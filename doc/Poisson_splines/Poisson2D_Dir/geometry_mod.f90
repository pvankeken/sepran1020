! note shape functions f0 etc. are for homogenous Dirichlet at all boundaries
module geometry
implicit none
integer, parameter :: NSPAN=4
! Gauss coordinates
real(kind=8) :: w(NSPAN*NSPAN),xg(NSPAN*NSPAN),yg(NSPAN*NSPAN) ! common pegauss
real(kind=8) :: w1d(NSPAN),xg1d(NSPAN) ! common peg1d
real(kind=8) :: sp0(NSPAN,NSPAN*NSPAN),sp1(NSPAN,NSPAN*NSPAN),sp2(NSPAN,NSPAN*NSPAN) ! common pespline
integer :: ngaus,ng1d
! Gauss integration points and weights
real(kind=8), parameter :: a1=0.0_8,h1=2.0_8
real(kind=8), parameter :: a2=0.577350269189626_8,h2=1.0_8
real(kind=8), parameter :: a31=0.774596669241483_8,h31=0.5555555555555555_8
real(kind=8), parameter :: a32=0.0_8,h32=0.888888888888888888_8
real(kind=8), parameter :: a41=0.861136311594053_8,h41=0.347854845137454_8
real(kind=8), parameter :: a42=0.339981043584856_8,h42=0.652145154862546_8

contains

  real(kind=8) function f0(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then 
     ! element is outside span of this shapefunction
     f0=0
     return
  endif
  call shap0(y,x,NSPAN)
  if (no==1) then
     ! modify for Dirichlet b.c.
     if (iel == 1) then
        f0 = y(2)-y(4)   
     else 
        f0 = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        f0 = y(3)-y(1)
     else
        f0 = y(iel-no+2)
     endif
     return
  endif
  f0 = y(iel-no+2)
   
  end function f0

  real(kind=8) function df1(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  !write(6,'(''df1: '',2i5,2f12.3,$)') no,iel,x
  if (no-2 >= iel .or. iel >= no+3) then
     ! element is outside span of this shapefunction
     df1=0
     !write(6,*) 'outside'
     return
  endif

  !y(1:4)=-99.0_8
  call shap1(y,x,NSPAN)
  if (no==1) then
     if (iel==1) then
        df1 = y(2)-y(4)
     else
        df1 = y(iel+1)
     endif
     !write(6,*) 'iel1: ',df1
     return
  endif
  if (no==nex-1) then
     ! shapefunction nx-1: add nx+1 for natural b.c.
     if (iel==nex) then
        df1 = y(3)-y(1)
     else
        df1 = y(iel-no+2)
     endif

     !write(6,*) 'ielN: ',df1
     return
  endif
  df1 = y(iel-no+2)
  !write(6,*) df1,iel-no+2

  end function df1

  real(kind=8) function df2(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then
     ! element is outside span of this shapefunction
     df2=0
     return
  endif

  call shap2(y,x,NSPAN)
  !y(1:4)=-999.0_8
  if (no==1) then
     if (iel==1) then
        df2 = y(2)-y(4)
     else
        df2 = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        df2 = y(3)-y(1)
     else
        df2 = y(iel-no+2)
     endif
     return
  endif
  df2 = y(iel-no+2)
  end function df2

  real(kind=8) function df3(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  integer,parameter  :: NSPAN=4
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then
     ! element is outside span of this shapefunction
     df3=0
     return
  endif

  call shap3(y,x,NSPAN)
  if (no==1) then
     if (iel==1) then
        df3 = y(2)-y(4)
     else
        df3 = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        df3 = y(3)-y(1)
     else
        df3 = y(iel-no+2)
     endif
     return
  endif
  df3 = y(iel-no+2)
  end function df3

  ! shap0 = shape functions for 1D bicubic splines defined on 4 intervals of [-1,1] 
  ! shapN = their N-th derivatives 
  subroutine shap0(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: xi2 ,oneminusx
  oneminusx=1-xi
  y(1) = xi*xi*xi
  y(2) = 1+3*xi+3*xi*xi-3*xi*xi*xi
  y(3) = 1+3*oneminusx+3*oneminusx*oneminusx-3*oneminusx*oneminusx*oneminusx
  y(4) = oneminusx*oneminusx*oneminusx
  !! ? y(1:4) = y(1:4)*0.1
  end subroutine shap0
  subroutine shap1(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: xi2 ,oneminusx
  oneminusx=1-xi
  y(1) = 3*xi*xi
  y(2) = 3+6*xi       -9*xi*xi
  y(3) = -3-6*oneminusx+9*oneminusx*oneminusx
  y(4) = -3*oneminusx*oneminusx
  !y(1) = 3*xi2
  !y(2) = -9*xi2+6*xi+3
  !y(3) = 9*xi2-12*xi
  !y(4) = -3*(1.0_8-xi)*(1.0_8-xi)
  !! ? y(1:4) = 0.1_8*y(1:4)
  !y(1:4) = -99.0_8
  end subroutine shap1

  subroutine shap2(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: oneminusx

  oneminusx=1-xi
  y(1) = 6*xi
  y(2) = 6       -18*xi
  y(3) = +6-18*oneminusx
  y(4) = 6*oneminusx
  !y(1) = 6*xi
  !y(2) = -18*xi+6.0_8
  !y(3) = 18*xi+6.0_8
  !y(4) = 6.0_8-6*xi
  !! ? y(1:4) = y(1:4)*0.1_8

  end subroutine shap2

  subroutine shap3(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y

  y(1) = 6.0_8
  y(2) = -18.0_8
  y(3) = 18.0_8
  y(4) = -6.0_8
  !! ? y(1:4) = y(1:4)*0.1_8

  end subroutine shap3




end module geometry
