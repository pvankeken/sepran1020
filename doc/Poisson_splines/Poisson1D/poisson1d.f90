! Poisson1D
! Fortran90 version of code in "Solving the Poisson equation using spline shapefunctions" PvK August 16, 1992
! 
! Non-optimized assembly & solution
! PvK lovingly restored January 2022
program poisson1d
use geometry
implicit none
integer,parameter :: NMAX=10
real(kind=8) :: amat(NMAX,NMAX),indx(NMAX,NMAX),b(NMAX),c(NMAX)
real(kind=8) :: sol_ana(5),w1,w2,g1,g2,g3,dx,d,xglob,xi,sol_num
integer :: nx,np,i,j,iel,k

sol_ana(1:5)=(/ 0.0_8, -0.21875_8, -0.375_8, -0.46875_8, -0.5_8 /)

! calculate weights and local coordinates for three point Gauss rule
w1=h31/2
w2=h32/2
g1=0.5_8-0.5_8*a31
g2=0.5_8
g3=0.5_8+0.5_8*a31
ng1d=3
xg1d(1)=g1
xg1d(2)=g2
xg1d(3)=g3
w1d(1)=w1
w1d(2)=w2
w1d(3)=w1

! calculate matrix coefficients
nx=NMAX
np=5
dx=1.0_8/nx
do j=1,nx
   do i=1,nx
     amat(i,j)=0.0_8
     do iel=1,nx
        do k=1,ng1d
           amat(i,j)=amat(i,j)+w1d(k)*df1x(j,iel,xg1d(k),nx)*df1x(i,iel,xg1d(k),nx)
        enddo
     enddo
   enddo
enddo
amat=amat*nx

! calculate load vector
do i=1,nx
   b(i)=0.0_8
   do iel=1,nx
      do k=1,ng1d
         b(i)=b(i)+w1d(k)*f0x(i,iel,xg1d(k),nx)
      enddo
   enddo
enddo
b = -b*dx

write(6,'(30x,''A'',29x,7x,''  b '')') 
do j=1,nx
   write(6,'(10f6.1,:,5x,f6.1)') (amat(j,i),i=1,nx),b(j)
enddo

! solve full matrix with NR subroutines (not efficient, of course)
call ludcmp(amat,nx,nx,indx,d)
call lubksb(amat,nx,nx,indx,b)

! output spline coefficients
write(6,*) 
write(6,'(''b: '',10f7.3)') (b(i),i=1,nx)
!b(1)=-0.016
!b(2)=-0.030
!b(3)=-0.043
!b(4)=-0.054
!b(5)=-0.063
!b(6)=-0.070
!b(7)=-0.076
!b(8)=-0.080
!b(9)=-0.083
!b(10)=-0.084

! output solution
write(6,'(3a12)') 'x','numerical','analytical'
do i=1,np
   xglob=(i-1)*1.0_8/(np-1)
   iel = xglob/dx + 1
   iel = min(iel,nx)
   xi = (xglob-(iel-1)*dx)/dx
   sol_num=0.0_8
   do j=1,nx
      sol_num=sol_num+b(j)*f0x(j,iel,xi,nx)
   enddo
   write(6,'(3f12.5)') xglob,sol_num,sol_ana(i)
enddo

end program poisson1D
