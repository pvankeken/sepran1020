! Poisson2D
! Fortran90 version of code in "Solving the Poisson equation using spline shapefunctions" PvK August 16, 1992
! 
! Optimized assembly and solve with linpack routines
! PvK lovingly restored January 2022
program poisson2d
use geometry
implicit none
integer,parameter :: NX=20,NY=20,NTOT=NX*NY,MBW=3*NY+3,LDA=MBW+1
real(kind=8) :: abd(LDA,NTOT),b(NTOT)
real(kind=8) :: sol_ana,w1,w2,g1,g2,g3,dx,dy,d,xglob,yglob,xi,eta,sol_num
real(kind=8) :: frow(500),dfrow(500)
integer :: i,j,k,npx,npy,jx,jy,ix,iy,ielx,iely,i1
integer :: ielxmin,ielxmax,ielymin,ielymax,info
real(kind=8) :: a1x,a1y,a2x,a2y,bfunc,bx,by,errmax,solmax,aij


! calculate weights and local coordinates for three point Gauss rule
w1=h31/2
w2=h32/2
g1=0.5_8-0.5_8*a31
g2=0.5_8
g3=0.5_8+0.5_8*a31
ng1d=3
xg1d(1)=g1
xg1d(2)=g2
xg1d(3)=g3
w1d(1)=w1
w1d(2)=w2
w1d(3)=w1

abd=0.0_8
b=0.0_8
write(6,*) 'matrix is lda = ',lda,' ntot =',ntot,lda*ntot

! finite element grid spacing
dx=1.0_8/NX
dy=1.0_8/NX
! Number of test grid points
npx=2*NX
npy=2*NY
! calculate matrix coefficients
do j=1,NTOT
   i1=max(1,j-mbw)
   do i=i1,j
     jy=(j-1)/NY+1
     jx=j-(jy-1)*NX
     iy=(i-1)/NY+1
     ix=i-(iy-1)*NX
     if ( abs(jy-iy)>3 .or. abs(jx-ix)>3) then
        ! splines are 0 by definition
        aij=0.0
     else
        aij=0.0
        ! determine in which elements the shapefunctions have non-zero contribution
        ! (mmmmm, this should be caught by if statement above, no?)
        ielxmax=min(ix,jx)+2
        ielxmin=min(ix,jx)-1+abs(ix-jx)
        ielymax=min(iy,jy)+2
        ielymin=min(iy,jy)-1+abs(iy-jy)
        do ielx=max(1,ielxmin),min(ielxmax,nx)
           do iely=max(1,ielymin),min(ny,ielymax)
              a1x=0.0_8
              a1y=0.0_8
              a2x=0.0_8
              a2y=0.0_8
              do k=1,ng1d
                 a1x=a1x+w1d(k)*df1(jx,ielx,xg1d(k),nx)*df1(ix,ielx,xg1d(k),nx)
                 a1y=a1y+w1d(k)* f0(jy,iely,xg1d(k),ny)* f0(iy,iely,xg1d(k),ny)
                 a2x=a2x+w1d(k)* f0(jx,ielx,xg1d(k),nx)* f0(ix,ielx,xg1d(k),nx)
                 a2y=a2y+w1d(k)*df1(jy,iely,xg1d(k),ny)*df1(iy,iely,xg1d(k),ny)
              enddo ! k
              aij=aij + dy/dx*a1x*a1y + dx/dy*a2x*a2y
           enddo ! iely
        enddo ! ielx
        ! find index in matrix
        k=i-j+MBW+1
        abd(k,j)=aij
     endif
   enddo !i
enddo !j

! calculate load vector
do i=1,NTOT
   b(i)=0.0_8
   iy=(i-1)/ny+1
   ix=i-(iy-1)*ny
   ielxmax=ix+2
   ielxmin=ix-1
   ielymax=iy+2
   ielymin=iy-1
   do ielx=max(1,ielxmin),min(nx,ielxmax)
      do iely=max(1,ielymin),min(ny,ielymax)
         bx=0.0_8
         by=0.0_8
         yglob=0.0 !!!!
         ! integrate over x
         do k=1,ng1d
            xglob=(ielx-1)*dx + xg1d(k)*dx
            bx=bx+bfunc(3,xglob,yglob)*w1d(k)*f0(ix,ielx,xg1d(k),nx)
         enddo
         ! integrate over y
         do k=1,ng1d
            yglob=(iely-1)*dy+xg1d(k)*dy
            by=by+bfunc(4,xglob,yglob)*w1d(k)*f0(iy,iely,xg1d(k),ny)
         enddo
         b(i)=b(i)+bx*by
       enddo ! iely
   enddo ! iely
enddo
b = -b*dx*dy

! solve positive definitive band matrix with linpack
call dpbfa(abd,lda,ntot,mbw,info)
if (info/=0) then
   write(6,*) 'singular matrix info = ',info
   stop
endif
call dpbsl(abd,lda,ntot,mbw,b)

! output spline coefficients
!write(6,*)
!write(6,*) 'spline coefficients'
!write(6,'(''b: '',9e12.3)') (b(i),i=1,nx*ny)
!write(6,*) 

! output solution
solmax=0.0_8
errmax=0.0_8
!write(6,'(5a12)') 'x','y','numerical','analytical','error'
do iy=npy,1,-1
   do ix=1,npx
      xglob=(ix-1)*1.0_8/(npx-1)
      yglob=(iy-1)*1.0_8/(npx-1)
      ielx = xglob/dx + 1
      ielx = min(ielx,nx)
      iely = yglob/dy + 1
      iely = min(iely,ny)
      xi  = (xglob-(ielx-1)*dx)/dx
      eta = (yglob-(iely-1)*dy)/dy
      sol_num=0.0_8
      sol_ana=bfunc(2,xglob,yglob,yglob)
      do j=1,ntot
         jy=(j-1)/ny+1
         jx=j-(jy-1)*ny
         sol_num=sol_num+b(j)*f0(jx,ielx,xi,nx)*f0(jy,iely,eta,ny)
      enddo
      !write(6,'(5f12.5)') xglob,yglob,sol_num,sol_ana,sol_ana-sol_num
      solmax=max(abs(sol_ana),solmax)
      errmax=max(abs(sol_ana-sol_num),errmax)
   enddo ! ix
enddo !iy 

write(6,*) 
write(6,'(''Maximum error: '',f11.6,'' = '',f11.6,'' %'')') errmax,errmax/solmax*100.0_8

end program poisson2D

real(kind=8) function bfunc(ichoice,x,y,z)
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8), parameter :: pi=3.141592653_8

if (ichoice==1) then
   bfunc=1.0_8
else if (ichoice==2) then
   ! analytical solution
   bfunc=sin(pi*0.5_8*x)*sin(pi*0.5_8*y)
else if  (ichoice==3) then
   ! x part of load vector
   bfunc=-0.5*pi*pi*sin(pi*0.5_8*x)
else if (ichoice==4) then
   bfunc=sin(pi*0.5*y)
endif

end function bfunc
