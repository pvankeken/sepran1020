c *************************************************************
c *   FUN
c *************************************************************
      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      if (ichois.eq.1) then
         func = -sin(pi*x)*sin(pi*y)
      else if (ichois.eq.2) then
         func = -pi*sin(pi*x)*cos(pi*y)
      else if (ichois.eq.3) then
         func =  pi*cos(pi*x)*sin(pi*y)
      else if (ichois.eq.4) then
         func = pi*pi*sin(pi*x)*sin(pi*y)
      else if (ichois.eq.5) then
         func = pi*pi*sin(pi*x)*sin(pi*y)
      else if (ichois.eq.6) then
         func = pi*pi*pi*sin(pi*x)*cos(pi*y)
      else if (ichois.eq.7) then
         func = pi*pi*pi*sin(pi*x)*cos(pi*y)
      endif
      return
      end
