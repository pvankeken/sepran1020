c *************************************************************
c *   PRESP
c *
c *************************************************************
      program presP
      implicit double precision(a-h,o-z)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      dimension user(100),kmesh(100),kprob(100),isol(5),iu1(3)
      dimension istrm(5),ipress(5),iuser(100)

      kmesh(1) = 100
      kprob(1) = 100
      user(1)  = 100
      iuser(1)  = 100

      call start(0,0,0,0)
      open(9,file='mesh1')
      call meshrd(3,9,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      iu1(1) = 2
      iu1(2) = 3
      call creavc(0,1,ivec,isol,kmesh,kprob,iu1,u1,iu2,u2)
      iuser(6) = 7
      iuser(7) = -6
      iuser(8) = -7
      iuser(9) = 0
      iuser(10)= 0
      iuser(11)= 0
      iuser(12)= 0
      iuser(13)= 1
      iuser(14) = -7
      user(6) = 1d-6
      user(7) = 1d0

      call deriva(0,1,ix,jd,ivec,ipress,kmesh,kprob,
     v             isol,isol,iuser,user,i)
      call stream(1,ivec,istrm,0,psiphi,kmesh,kprob,isol)
      format = 1d1
      chheight = 0.3
      jsm = 0
      yfaccn=1
      fname = 'PLOT'
      call plwin(1,0d0,0d0)
      call plotc1(1,kmesh,kprob,istrm,contln,ncntln,format,yfaccn,jsm)
      call plwin(3,format+1d0,0d0)
      call plotc1(1,kmesh,kprob,ipress,contln,ncntln,format,yfaccn,jsm)
      end
