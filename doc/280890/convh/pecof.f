c *************************************************************
c *   PECOF
c *  
c *   Fill coefficients for Sepran problems in arrays user
c *
c *   Ichois=1    Stokes equation (primitive variables) type 400
c *
c *   Ichois=2    Temperature equation (type 100 etc)
c *
c *   PvK 160290
c *************************************************************
      subroutine pecof(ichois,kmesh,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),iuser(*),user(*)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
	
      npoint = kmesh(8)
      do 10 i=6,20
10       iuser(i) = 0
      do 20 i=6,10+2*npoint
20        user(i) = 0.

      if (ichois.eq.1) then
c        *** Stokes equation
c        *** eps, rho, MCONV, omega, f1, f2, MODELV, eta
	 iuser(6) = 7
	 iuser(7) = -6
	 iuser(8) = -7
	 iuser(9) = 0
	 iuser(10)= 0
	 iuser(11)= 0
	 iuser(12)= 2001
	 iuser(13)= 10
	 iuser(14)= 1
	  user(6) = 1d-6
	  user(7) = 1d0
	 call pecopy(0,isol,user,kprob,10,rayleigh)
	 if (visc0.le.0.) then
            iuser(15)= -7
         else
	    iuser(15)= 2001
	    iuser(16)= 10+npoint
	    call pevisc(kmesh,kprob,isol,user(10+npoint))
         endif

       else if (ichois.eq.2) then
c         *** Temperature equation
c         *** a11,a12,a22,u,v,b,f,iupwind
	  iuser(6) = 7
	  iuser(7) = -6
	  iuser(8) = 0
          iuser(9) = -6
	  iuser(10)= 2001
	  iuser(11)= 10
	  iuser(12)= 2001
	  iuser(13)= 10+npoint
	  iuser(14)= 0
	  iuser(15)= -7
	  iuser(16)= 2
	   user(6) = 1d0
	   user(7) = q
          call pecopy(2,isol,user,kprob,10,1d0)
	  call pecopy(3,isol,user,kprob,10+npoint,1d0)
       else
	  write(6,*) 'PERROR(pecof): unknown option: ',ichois
       stop
      endif

      return
      end

c *************************************************************
c *   PEVISC
c *
c *   Fill array user with the value of the temperature and depth 
c *   dependent viscosity in each nodal point
c *
c *   PvK 050390
c ************************************************************* 
      subroutine pevisc(kmesh,kprob,isol,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      call pecopy(0,isol,user,kprob,1,1d0)
      npoint = kmesh(8)
      kelmi  = kmesh(23)
      call ini050(kelmi,'pevisc')
      ikelmi = infor(1,kelmi)
      call pevsc1(ibuffr(ikelmi),user,npoint)

      return
      end

      subroutine pevsc1(coor,user,npoint)
      implicit double precision(a-h,o-z)
      dimension coor(2,*),user(*)
      common /peparam/ rayleigh,rlam,q,visc0,b,c

      do 10 i=1,npoint
	 temp = user(i)
	 z    = coor(2,i)
	 visc = visc0*exp(-b*temp+c*(1-z))
	 user(i) = visc
10    continue
      
      return
      end

