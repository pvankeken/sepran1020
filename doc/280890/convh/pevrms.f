c ***************************************************
c *   PEVRMS
c *
c *   Calculates u*u + v*v per nodal point from the
c *   solution vector and places the result in array user.
c *
c *   PvK, 17-4-89
c *****************************************************************
      subroutine pevrms(isol,kmesh,kprob,iuser,user)
      implicit double precision(a-h,o-z)

      common ibuffr(1)
      dimension isol(*),kmesh(*),kprob(*),iuser(*),user(*)

      iprob = isol(4)/1000 
      iprob = iprob+1
      nprob = max(1,kprob(40))
      if (iprob.lt.1.or.iprob.gt.nprob) call ertrap(
     .   'pevrms',699,2,iprob,0,0)
      ipkprb = 1
      do 1 i=2,iprob
1          ipkprb = ipkprb + kprob(2+ipkprb)

c     *** Calculate pointer to solution vector in ibuffr
      itypvc = 0
      kfree  = 0
      call ini002(itypvc,isol,kmesh,kprob(ipkprb),kfree,nunkp,
     v            nusol,ipusol,ipkprf,ipkprbf,ipkprh,ikprbh,
     v            .false.,.false.,'pevrms',ivec)

c     *** Actual calculation
      npoint = kmesh(8)
      call pevrm1(npoint,ibuffr(ipusol),ibuffr(ipkprh),ikprbh,
     v            nunkp,user(6))

      iuser(6) = 7
      iuser(7) = 2001
      iuser(8) = 6

      return
      end
