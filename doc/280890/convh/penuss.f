c *************************************************************
c *   PENUSS
c *   
c *   Create files FTNAME with temperature profile T(y) in the
c *   middle of the box and FNNAME with Nusselt number profile Nu(y).
c *   Parameters:
c *       nx    i  Number of nodalpoints in the x direction
c *       ny    i  Number of nodalpoints in y direction
c *       kmesh i  Standard Sepran array
c *       user  i  User array, containing 
c *                -  the vertical velocity w per nodal point 
c *                   from position 10 to 9+npoint
c *                -  the temperature per nodal point 
c *                   from position 10+npoint to 9+2*npoint
c *                -  the temperature gradient dT/dy per nodal point
c *                   from position 10+2*npoint
c *
c *   The coordinates are extracted from the info in kmesh
c *   The nusselt number is calculated by the formula
c *
c *   Nu = average(wT) - average(dT/dy)
c *
c *   where the averages are taken horizontally by integrating
c *   wT and dT/dy by the trapezoid rule.
c * 
c *   PvK 080390
c *************************************************************
      subroutine penuss(nx,ny,kmesh,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      npoint = kmesh(8)
      kelmi  = kmesh(23)
      call ini050(kelmi,'penuss')
      ikelmi = infor(1,kelmi)
      call penus1(nx,ny,npoint,ibuffr(ikelmi),user)
      call penus2(nx,ny,npoint,ibuffr(ikelmi),user)
      return
      end

      subroutine penus1(nx,ny,npoint,coor,user)
      implicit double precision(a-h,o-z)
      common /pefname/ ftname,fnname
      character*80 ftname,fnname
      dimension coor(2,*),user(*)

      open(13,file=ftname)
      rewind(13)
      write(13,*) 2,ny
      write(6,*) nx,ny
      do 100 i=1,ny
	 k = 1+nx/2+(i-1)*nx
	 write(13,*) user(9+npoint+k),coor(2,k)
100   continue
      close(13)

      return
      end

      subroutine penus2(nx,ny,npoint,coor,user)
      implicit double precision(a-h,o-z)
      common /pefname/ ftname,fnname
      character*80 ftname,fnname
      dimension coor(*),user(*)

      open(13,file=fnname)
      rewind(13)
      write(13,*) 2,ny
      write(6,*) nx,ny
      do 100 i=1,ny
	 iof1 = 10+(i-1)*nx
	 iof2 = iof1+npoint
	 iof3 = iof2+npoint
	 iof4 = 1+2*(i-1)*nx
	 call penus3(nx,user(iof1),user(iof2),
     v                 user(iof3),coor(iof4),gnus)
	 write(13,*) coor(2+2*(i-1)*nx),gnus
100   continue
      close(13)

      return
      end

      subroutine penus3(nx,w,t,dtdz,coor,gnus)
      implicit double precision(a-h,o-z)
      dimension w(nx),t(nx),dtdz(nx),coor(2,nx)

      awt = 0d0
      adtdz = 0d0
      do 10 i=1,nx-1
	 dx = abs(coor(1,i+1)-coor(1,i))
	 awt = awt + 0.5*dx*(w(i)*t(i)+w(i+1)*t(i+1))
	 adtdz = adtdz + 0.5*dx*(dtdz(i)+dtdz(i+1))
10    continue
      gnus = awt - adtdz

      return
      end
