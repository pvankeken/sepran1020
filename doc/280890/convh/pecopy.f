c *******************************************************************
c *   PECOPY
c *
c *   Adaption of pecopy/er0035 to allow for scaling a solutionvector
c *
c *   PvK 7-11-89
c *******************************************************************
      subroutine pecopy(ichois,isol,user,kprob,istart,factor)
      implicit double precision (a-h,o-z)
      logical new
      dimension isol(*),kprob(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      if (ichois.eq.1) then
	 write(6,*) 'PERROR(pecopy): option 1 not available'
         stop
      endif
      iprob=isol(4)/1000+1
      if (iprob.gt.1) then
	 write(6,*) 'PERROR(pecopy): iprob>1'
	 stop
      endif
      call ini055(isol,kprob,indprf,indprh,indprp,nphys,'pecopy')
      iindprh = infor(1,indprh)
      iisol   = infor(1,isol(1))
      nusol   = kprob(5)

      call pecop1(ichois,nusol,ibuffr(iisol),ibuffr(iindprh),indprh,
     v       user(istart),factor)
      end

      subroutine pecop1(ichois,nusol,usol,kprobh,indprh,user,factor)
      implicit double precision (a-h,o-z)
      dimension usol(*),kprobh(*),user(*)

      if (factor.eq.0d0) factor=1d0
      if(ichois.eq.0) then
         if(indprh.eq.0) then
            do 100 i=1,nusol
100            user(i)=factor*usol(i)
         else
            do 110 i=1,nusol
110            user(i)=factor*usol(kprobh(i))
         endif
      else if(ichois.eq.2 .or. ichois.eq.3) then
         if(indprh.eq.0) then
            do 150 i=ichois-1,nusol,2
150            user((i+1)/2)=factor*usol(i)
         else
            do 160 i=ichois-1,nusol,2
160            user((i+1)/2)=factor*usol(kprobh(i))
         endif
      endif
      end
