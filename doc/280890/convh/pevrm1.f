c ************************************************************
c *   PEVRM1
c *   
c *   Calculates u*u+v*v from the solutionvector usol and stores
c *   the result in array user from position one.
c *
c *   Pvk 17-4-89
c ************************************************************
      subroutine pevrm1(npoint,usol,kprobh,ikprbh,nunkp,user)
      implicit double precision (a-h,o-z)
      dimension usol(*),kprobh(*),user(*)
     
      do 100 i=1,npoint
         ips1 = i*nunkp-1
         ips2 = ips1+1
         if (ikprbh.gt.0) then
            ips1 = kprobh(ips1)
            ips2 = kprobh(ips2)
         endif
         uv2   = usol(ips1)*usol(ips1) + usol(ips2)*usol(ips2)
100      user(i) = uv2
      return
      end 
