c **********************************************************
c *   Pois
c *
c *   PvK 090190
c **********************************************************************
      program pois
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      dimension kmesh(100),kprob(100),iuser(100),user(5000)
      dimension intmat(5),isol(5),islold(5),irhsd(5),matr(5)
      dimension iu1(2),u1(2),ivec1(5),ivec2(5),isolan(5)
      dimension icurvs(2),funcx(100),funcy(100)
      real t0,t1,t2

      kmesh(1)=100
      kprob(1)=100
      iuser(1)=100
       user(1)=5000
      funcx(1)=100
      funcy(1)=100
      call start(0,1,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(2,kmesh,kprob,intmat)

      read(5,*) nrule
      read(5,*) alpha,iu,iv
      call second(t0)
c     *** Fill part of USER for standard problem
      iuser(6)=7
      iuser(7)=-6
      iuser(9)=-6
      iuser(10)=iu
      iuser(11)=iv
      iuser(14)=iupwind
      user(6)=alpha
c     ***** Create velocity vector
      iu1(1)=iu
      call creavc(0,1,i,ivec1,kmesh,kprob,iu1,u1,iu2,u2)
      iu1(1)=iv
      call creavc(0,1,i,ivec2,kmesh,kprob,iu1,u1,iu2,u2)
      call plotvc(-1,-1,ivec1,ivec2,kmesh,kprob,10d0,factor,0d0)
      call bvalue(1,2,kmesh,kprob,isol,value,1,1,1,0) 
      value=0d0
      call bvalue(0,2,kmesh,kprob,isol,value,3,4,1,0)
c     ***** Coefficients for user defined problems 
c     ***** (stored in array USER from position 31)
      npoint=kmesh(8)
      user(31)=alpha
      iuser(31)=npoint
      call copyuv(0,ivec1,user,kprob,32)
      call copyuv(0,ivec2,user,kprob,32+npoint)

      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,iuser,user,
     v            user,islold,ielhlp)
      call second(t1)
      call solve(0,matr,isol,irhsd,intmat,kprob)
      call second(t2)
      write(6,*) 'System:  ',t1-t0
      write(6,*) 'Solve :  ',t2-t1
      call plotc1(number,kmesh,kprob,isol,contln,ncntln,10d0,
     v            1d0,jsmoot)

      iu1(1)=3
      call creavc(0,1,idum,isolan,kmesh,kprob,iu1,u1,iu2,u2)
      dif = anorm(0,3,1,kmesh,kprob,isol,isolan,ielhlp)
      write(6,*) dif
      call finish(0)
      end

      
