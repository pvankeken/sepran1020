c *************************************************************
c *   UPWDIF
c *   
c *   Add the upwind weighted diffusive terms to the stiffness
c *   matrix for the bilinear triangle. It is assumed that the
c *   Galerkin terms are already added. The integrals are calculated
c *   analytically.
c *
c *   PvK 220190
c *************************************************************
      subroutine upwdif(elemmt,a,b,alf,bet,alpha)
      implicit double precision(a-h,o-z)
      dimension elemmt(16)
   
      a1 = a/(4*b)*alf*alpha
      b1 = b/(4*a)*bet*alpha

      elemmt(1)  = elemmt(1)  + a1 + b1 
      elemmt(2)  = elemmt(2)  + a1 - b1
      elemmt(3)  = elemmt(3)  - a1 - b1
      elemmt(4)  = elemmt(4)  - a1 + b1 
      elemmt(5)  = elemmt(5)  + a1 - b1 
      elemmt(6)  = elemmt(6)  + a1 + b1
      elemmt(7)  = elemmt(7)  - a1 + b1 
      elemmt(8)  = elemmt(8)  - a1 - b1
      elemmt(9)  = elemmt(9)  - a1 - b1 
      elemmt(10) = elemmt(10) - a1 + b1 
      elemmt(11) = elemmt(11) + a1 + b1 
      elemmt(12) = elemmt(12) + a1 - b1
      elemmt(13) = elemmt(13) - a1 + b1
      elemmt(14) = elemmt(14) - a1 - b1
      elemmt(15) = elemmt(15) + a1 - b1
      elemmt(16) = elemmt(16) + a1 + b1

      return
      end
