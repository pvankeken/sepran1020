c *************************************************************
c *   FDERGAUS
c *
c *   Fill values of shapefunctions and derivatives of 
c *   shapefunctions in pegauss
c *
c *************************************************************
      subroutine fdergaus
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi

      if (nphi.ne.4) then
	 write(6,*) 'PERROR(fdergaus) nphi<>4 not yet implemented'
	 stop
      endif

      do 10 i=1,ngaus
	 x = xg(i)
	 y = yg(i)
	 phi(1,i) = (1-x)*(1-y)
	 phi(2,i) = x*(1-y)
	 phi(3,i) = x*y
	 phi(4,i) = (1-x)*y
	 dphidx(1,i) = y-1
	 dphidx(2,i) = 1-y
	 dphidx(3,i) = y
	 dphidx(4,i) = -y
	 dphidy(1,i) = x-1
	 dphidy(2,i) = -x
	 dphidy(3,i) = x
	 dphidy(4,i) = (1-x)
10    continue

      return
      end
