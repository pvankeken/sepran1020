      subroutine blrdif(elemmt,a,b,alpha) 
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      parameter(drie=0.33333333333333333d0,zes=0.16666666666666667d0)
      save ifirst
      dimension elemmt(*)
      data ifirst/0/

      ab=a/b*alpha
      ba=b/a*alpha
      if (nrule.eq.0) then
c        *** exact integration
         d1 = drie*ab + drie*ba
         d2 = zes*ab  - drie*ba
         d3 = -zes*ab - zes*ba
         d4 = -drie*ab+ zes*ba
         elemmt(1) = d1
         elemmt(2) = d2
         elemmt(3) = d3  
         elemmt(4) = d4
         elemmt(5) = d2
         elemmt(6) = d1
         elemmt(7) = d4
         elemmt(8) = d3
         elemmt(9) = d3
         elemmt(10)= d4
         elemmt(11)= d1
         elemmt(12)= d2
         elemmt(13)= d4
         elemmt(14)= d3
         elemmt(15)= d2
         elemmt(16)= d1
      else
c        *** numerical integration (symmetry!)
         do 20 j=1,nphi
	    do 20 i=j,4
	       ioff = (j-1)*4+i
	       sumx  = 0.
	       sumy  = 0.
	       do 30 k=1,ngaus
		  sumx = sumx+w(k)*dphidx(i,k)*dphidx(j,k)*ba
30                sumy = sumy+w(k)*dphidy(i,k)*dphidy(j,k)*ab
	       if (ifirst.eq.0) write(6,'(2i3,2f12.5)') i,j,sumx,sumy
               elemmt(ioff) = sumx+sumy
20       continue
	 elemmt(5) = elemmt(2)
	 elemmt(9) = elemmt(3)
	 elemmt(10)= elemmt(7)
	 elemmt(13)= elemmt(4)
	 elemmt(14)= elemmt(8)
	 elemmt(15)= elemmt(12)
	 ifirst = 1
      endif
      return
      end
