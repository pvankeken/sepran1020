c *************************************************************
c *   UPWGDIF
c *
c *   Calculate upwind corrected diffusion matrix
c *   Gauss integration
c *
c *   PvK 190390
c *************************************************************
      subroutine upwgdif(elemmt,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension elemmt(16)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2

      ab = a/b
      ba = b/a
      do 20 j=1,nphi
         do 20 i=1,nphi
            ioff = (i-1)*4+j
            sum  = 0
            do 30 k=1,ngaus
               sum = sum+w(k)*dwdx(i,1,k)*dphidx(j,k)*ba
30             sum = sum+w(k)*dwdx(i,2,k)*dphidy(j,k)*ab
            elemmt(ioff) = sum*alpha
20    continue

      return
      end
