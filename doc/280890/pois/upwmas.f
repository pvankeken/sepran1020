c *************************************************************
c *   UPWMAS
c *
c *   Built upwind weighted mass matrix for the bilinear triangles
c *
c *              1
c *   M     =   //  W N  dxdy   =
c *    ij       0    i j 
c *
c *         =   //  N N  dxdy   +
c *                  i j
c *
c *                        x y  
c *            alpha  //  F N  N dxdy +
c *                 i        i  j
c *   
c *
c *                        y x
c *            beta   //  F N  N dxdy +
c *                i         i  j
c *
c *                            x y 
c *            alpha beta  // F F N dxdy
c *                 i    i         j
c *
c *   PvK 300190
c *************************************************************
      subroutine upwmas(elemmt,alf,bet,a,b)
      implicit double precision (a-h,o-z)
      dimension elemmt(16),alf(4),bet(4)

      return
      end
