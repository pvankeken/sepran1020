c *************************************************************
c *   FWGAUS
c *
c *   Fill coordinates of gausspoints in arrays xg and yg
c *   Fill weights for the integration in array w
c *
c *   parameter ngaus has to be filled
c *
c ************************************************************* 
      subroutine fwgaus
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      data a1,h1/0,2/
      data a2,h2/0.577350269189626,1/
      data a31,h31/0.774596669241483,0.5555555555555555/
      data a32,h32/0,0.888888888888888888/
      data a41,h41/0.861136311594053,0.347854845137454/
      data a42,h42/0.339981043584856,0.652145154862546/

      if (ngaus.eq.4) then
	 g1    = 0.5 - 0.5*a2
	 g2    = 0.5 + 0.5*a2
	 xg(1) = g1
	 xg(2) = g2
	 xg(3) = g2
	 xg(4) = g1
	 yg(1) = g1
	 yg(2) = g1
	 yg(3) = g2
	 yg(4) = g2
	 do 10 i=1,4
            w(i) = 0.25
10       continue
      else if (ngaus.eq.9) then
	 g1    = 0.5 - 0.5*a31
	 g2    = 0.5
	 g3    = 0.5 + 0.5*a31
	 xg(1) = g1
	 xg(2) = g2
	 xg(3) = g3
	 xg(4) = g1
	 xg(5) = g2
	 xg(6) = g3
	 xg(7) = g1
	 xg(8) = g2
	 xg(9) = g3
	 yg(1) = g1
	 yg(2) = g1
	 yg(3) = g1
	 yg(4) = g2
	 yg(5) = g2
	 yg(6) = g2
	 yg(7) = g3
	 yg(8) = g3
	 yg(9) = g3
	 w1    = h31 / 2
	 w2    = h32 / 2
	 w(1)  = w1*w1
	 w(2)  = w1*w2
	 w(3)  = w1*w1
	 w(4)  = w2*w1
	 w(5)  = w2*w2
	 w(6)  = w2*w1
	 w(7)  = w1*w1
	 w(8)  = w1*w2
	 w(9)  = w1*w1
      endif

      return
      end
