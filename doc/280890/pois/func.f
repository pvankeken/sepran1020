      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      if (ichois.eq.3) func = funcbc(ichois,x,y,z)
      if (ichois.eq.1) func = sin(pi*y/2)*sin(pi*x/2)
      if (ichois.eq.2) func = cos(pi*x/2)*cos(pi*y/2)
      return
      end
