c *************************************************************
c *   UPWC4
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part IV
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc4(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = -alf*bet*b*(u(1)+u(2))/80
      u2 = -alf*bet*b*(u(3)+u(4))/80
      v1 = -alf*bet*a*(v(1)+v(4))/80
      v2 = -alf*bet*a*(v(2)+v(3))/80


      elemmt(1)  = elemmt(1) -3*u1-2*u2-3*v1-2*v2 
      elemmt(2)  = elemmt(2) +3*u1+2*u2-2*v1-3*v2
      elemmt(3)  = elemmt(3) +2*u1+3*u2+2*v1+3*v2
      elemmt(4)  = elemmt(4) -2*u1-3*u2+3*v1+2*v2

      elemmt(5)  = elemmt(5) -3*u1-2*u2-3*v1-2*v2 
      elemmt(6)  = elemmt(6) +3*u1+2*u2-2*v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+3*u2+2*v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-3*u2+3*v1+2*v2

      elemmt(9)  = elemmt(9) -3*u1-2*u2-3*v1-2*v2 
      elemmt(10) = elemmt(10)+3*u1+2*u2-2*v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+3*u2+2*v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-3*u2+3*v1+2*v2

      elemmt(13) = elemmt(13)-3*u1-2*u2-3*v1-2*v2 
      elemmt(14) = elemmt(14)+3*u1+2*u2-2*v1-3*v2
      elemmt(15) = elemmt(15)+2*u1+3*u2+2*v1+3*v2
      elemmt(16) = elemmt(16)-2*u1-3*u2+3*v1+2*v2

      return
      end
