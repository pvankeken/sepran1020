c *************************************************************
c *   UPWINIT
c *
c *   Calculate values of the linear basisfunctions N(x) and N(y),
c *   of the quadratic correction functions F(x) and F(y),
c *   their derivatives in the Gausspoints
c *
c *   phip(i,1,j)   Ni(x) in Gausspoint j
c *   phip(i,2,j)   Ni(y)
c *   dphip(i,1)    dNi(x)/dx
c *   dphip(i,2)    dNi(y)/dy
c *   fp(1,j)       F(x) 
c *   fp(2,j)       F(y)
c *   dfp(1,j)      dF(x)/dx
c *   dfp(2,j)      dF(y)/dy
c *   wp(i,j)       Wi(x,y)
c *   dwdx(i,1,j)   dWi(x,y)/dx
c *   dwdy(i,2,j)   dWi(x,y)/dy
c *   alpha1,alpha2 Upwind parameters in x-direction
c *   beta1,beta2   Upwind parameters in y-direction
c *
c *   PvK 190390
c *************************************************************
      subroutine upwinit
      implicit double precision(a-h,o-z)
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi

      do 10 i=1,ngaus
	 x = xg(i)
	 y = yg(i)
	 phip(1,1,i) = 1-x
	 phip(2,1,i) = x
	 phip(3,1,i) = x
	 phip(4,1,i) = 1-x
	 phip(1,2,i) = 1-y
	 phip(2,2,i) = 1-y
	 phip(3,2,i) = y
	 phip(4,2,i) = y
	 fp(1,i)   = 3*x*(1-x)
	 fp(2,i)   = 3*y*(1-y)
	 dfp(1,i)  = 3*(1-2*x)
	 dfp(2,i)  = 3*(1-2*y)
10    continue
      dphip(1,1) = -1.
      dphip(2,1) =  1.
      dphip(3,1) =  1.
      dphip(4,1) = -1.
      dphip(1,2) = -1.
      dphip(2,2) = -1.
      dphip(3,2) =  1.
      dphip(4,2) =  1.

      return
      end
