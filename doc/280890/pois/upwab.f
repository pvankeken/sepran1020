c *************************************************************
c *   UPWAB
c *
c *   Calculate the upwind factors alpha and beta defined by (i=1,4)
c *
c *   - alpha  = coth ( gamma  /2) - 2/gamma      
c *          i               i              i
c *
c *   - beta   = coth ( delta  /2) - 2/delta
c *         i                i              i
c *
c *   where gamma  = (u a)/kappa  and delta  = (v a)/kappa
c *              i     i                   i     i
c *
c *   The minus sign enters the equations because of the choice
c *   of F(x).
c *
c *   PvK 220190
c *************************************************************
      subroutine upwab(u,v,alf,bet,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension u(4),v(4)

      ug = 0.25*(u(1)+u(2)+u(3)+u(4))
      vg = 0.25*(v(1)+v(2)+v(3)+v(4))
      if (alpha.lt.1d-4) then
         alf=-1d0
         bet=-1d0
      else
         gamma = -abs(ug)*a/alpha
         delta = -abs(vg)*b/alpha
         if (abs(gamma).gt.1d-4) then
            alf = 1d0/tanh(gamma/2)-2d0/gamma
         else 
            alf = 0.
         endif
         if (abs(delta).gt.1d-4) then
            bet = 1d0/tanh(delta/2)-2d0/delta
         else
            bet = 0.
         endif
      endif
      return
      end

