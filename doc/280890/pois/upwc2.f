c *************************************************************
c *   UPWC2
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part II
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc2(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = alf*b*(u(1)+u(2))/48
      u2 = alf*b*(u(3)+u(4))/48
      v1 = alf*a*v(1)/120
      v2 = alf*a*v(2)/120
      v3 = alf*a*v(3)/120
      v4 = alf*a*v(4)/120

      elemmt(1)  = elemmt(1) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4 
      elemmt(2)  = elemmt(2) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(3)  = elemmt(3) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(4)  = elemmt(4) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4
 
      elemmt(5)  = elemmt(5) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4
      elemmt(6)  = elemmt(6) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(7)  = elemmt(7) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(8)  = elemmt(8) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4

      elemmt(9)  = elemmt(9) -  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(10) = elemmt(10)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(11) = elemmt(11)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4 
      elemmt(12) = elemmt(12)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4
 
      elemmt(13) = elemmt(13)-  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(14) = elemmt(14)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(15) = elemmt(15)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4
      elemmt(16) = elemmt(16)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4

      return
      end
