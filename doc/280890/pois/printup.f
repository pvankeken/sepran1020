c *************************************************************
c *    PRINTUP
c *************************************************************
      subroutine printup
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2
      write(6,*) 'Gauss points'
      do 10 i=1,ngaus
10        write(6,'(i2,2f12.5)') i,xg(i),yg(i)
      write(6,*) 'weights'
      do 20 i=1,ngaus
20       write(6,'(i2,f12.5)') i,w(i)

      write(6,*) 'phi'
      do 30 i=1,ngaus
30       write(6,'(i2,4f12.5)') i,(phi(j,i),j=1,4)
      write(6,*) 'dphidx'
      do 40 i=1,ngaus
40       write(6,'(i2,4f12.5)') i,(dphidx(j,i),j=1,4)
      write(6,*) 'dphidy'
      do 50 i=1,ngaus
50       write(6,'(i2,4f12.5)') i,(dphidy(j,i),j=1,4)
      write(6,*) 'f(x),f(y)'
      do 60 i=1,ngaus
60       write(6,'(i2,2f12.5)') i,(fp(j,i),j=1,2)

      write(6,*) 'df(x)/dx,df(y)/fy'
      do 70 i=1,ngaus
70       write(6,'(i2,2f12.5)') i,(dfp(j,i),j=1,2)
      write(6,*) 'alpha,beta'
      write(6,'(4f12.5)') alpha1,alpha2,beta1,beta2
      write(6,*) 'w'
      do 80 i=1,ngaus
80      write(6,'(i2,4f12.5)') i,(wp(j,i),j=1,4)

      return
      end
