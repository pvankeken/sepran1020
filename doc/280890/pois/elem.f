c *************************************************************
c *   ELEM
c *
c *   This version calculates element matrices for the
c *   convection-diffusion equation for the bilinear rectangle,
c *   with sides 1-2 and 3-4 in the x-direction (length a) and
c *   sides 2-3 and 4-1 in the y-direction (length b).
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *   iuser(31)           alpha        diffusivity
c *
c *    user(31)           u1           x-velocity in point 1
c *      ....             ....
c *    user(31+npoint)    uN           x-velocity in point N
c *
c *    user(32+npoint)    v1           y-velocity in point 1
c *      .....            ....
c *    user(31+2*npoint)  vN           y-velocity in point N
c *
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *        3          Galerkin weighting; exact integration
c *
c *        5          Upwind weighting according to scheme
c *                   of Huyakorn etc.; exact integration
c *       
c *        7          Pure convection; upwind weighting; 
c *                   exact integration
c *
c *        8          Mass matrix; exact integration
c * 
c *        9          Upwind weighted mass matrix; exact integration
c *
c *   PvK 150190-300190
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),u(4),v(4)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      if (ifirst.eq.1.and.(itype.eq.2.or.itype.eq.4)) return

      if (vector) then
         elemvc(1) = 0.
         elemvc(2) = 0.
         elemvc(3) = 0.
         elemvc(4) = 0.
      endif
     
      if (nrule.ne.0) then
c        *** Numerical integration
         if (nrule.gt.0) then
c           *** Gauss quadrature: 
c           *** fill values of shapefunctions and derivatives of
c           *** shapefunction in the nodalpoints.
c           *** Use common pegauss and peshape
	    if (nrule.eq.1) ngaus=4
	    if (nrule.eq.2) ngaus=9
	    if (nrule.eq.3) ngaus=16
            call fwgaus
	    nphi = 4
	    call fdergaus
         endif
      endif

c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
         iu = 31+index1(i)
         iv = iu+npoint
         u(i) = user(iu)
         v(i) = user(iv)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))
      alpha = user(31)

      if (matrix) then

         if (itype.eq.3) then
c        *** Stiffness matrix; Galerkin weighting
            call blrdif(elemmt,a,b,alpha)
            if (ifirst.eq.0) then
               print *,'Diffusion matrix 3'
               call printmat(elemmt)
            endif
            call blrconv(elemmt,a,b,u,v)
            if (ifirst.eq.0) then
               print *,'Element matrix 3'
               call printmat(elemmt)
            endif
	    ifirst=1
            return
         endif

c        *** Mass matrix
         if (itype.eq.8) then
            call blrmas(elemmt,a,b)
	    ifirst=1
            return
         endif

c        *** upwinding is used; initialize some parameters,
c        *** depending on integration rules
	 if (nrule.eq.0) then
            call upwab(u,v,alf,bet,a,b,alpha)            
         else if (nrule.gt.0) then 
            if (ifirst.eq.0) call upwinit
	    call upwgaus(u,v,a,b,alpha)
	    if (ifirst.eq.0) call printup
         endif

         if (itype.eq.5) then
	   if (nrule.eq.0) then
	      call blrdif(elemmt,a,b,alpha)
              call upwdif(elemmt,a,b,alf,bet,alpha)
           else if (nrule.gt.0) then
	      call upwgdif(elemmt,a,b,alpha)
	   endif
           if (ifirst.eq.0) then
               print *,'Diffusion matrix 5'
               call printmat(elemmt)
           endif
         endif
c        *** Convective terms 
	 if (nrule.eq.0) then
            call upwc2(elemmt,a,b,alf,bet,u,v)
            call upwc3(elemmt,a,b,alf,bet,u,v)
            call upwc4(elemmt,a,b,alf,bet,u,v)
         else if (nrule.gt.0) then
	    call upwgcon(elemmt,a,b,u,v)
	 endif
         if (ifirst.eq.0) then
             print *,'Element matrix 5'
             call printmat(elemmt)
         endif
      endif
c        if (itype.eq.9) then
c         if (nrule.eq.0) call upwab(u,v,alf,bet,a,b,alpha)      
c            if (ifirst.eq.0) then
c               print *
c               print *,'upwind factors'
c               write(6,'(2f12.3)') alf,bet
c            endif
c            call upwmas(elemmt,alf,bet,a,b)
c            if (ifirst.eq.0) then
c              print *,'Mass'
c              call printmat(elemmt)
c            endif
c	    ifirst=1
c            return
c         endif
            
      ifirst=1
      return
      end

      subroutine printmat(elemmt)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)
    
      write(6,*)
      write(6,10) 24*elemmt(1),24*elemmt(2),24*elemmt(3),
     v                24*elemmt(4)
      write(6,10) 24*elemmt(5),24*elemmt(6),24*elemmt(7),
     v                24*elemmt(8)
      write(6,10) 24*elemmt(9),24*elemmt(10),24*elemmt(11),
     v                24*elemmt(12)
      write(6,10) 24*elemmt(13),24*elemmt(14),24*elemmt(15),
     v                24*elemmt(16)
10    format(4f12.3)

      return
      end
 
 
 
 
