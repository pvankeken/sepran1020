c *************************************************************
c *   UPWGCON
c *   Calculate the upwind corrected convection matrix
c *   Gauss integration
c *
c *   PvK 190390
c *************************************************************
      subroutine upwgcon(elemmt,a,b,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(9,16),dphidx(9,16),dphidy(9,16),
     v                 d2phidx(9,16),d2phidy(9,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2

      save ifirst
      data ifirst/0/

      if (ifirst.eq.0) then
         write(6,*) 'upwgcon: u,v'
         write(6,'(4f12.3)') (u(i),i=1,4)
         write(6,'(4f12.3)') (v(i),i=1,4)
	 write(6,'(2f12.3)') a,b
      endif
      do 30 i=1,nphi
         do 30 j=1,nphi
            ioff = j+(i-1)*4
            sumx = 0.
	    sumy = 0.
            do 10 k=1,nphi
               tx = 0.
               ty = 0.
               do 20 m=1,ngaus
                  tx = tx + wp(i,m)*phi(k,m)*dphidx(j,m)*w(m)
20                ty = ty + wp(i,m)*phi(k,m)*dphidy(j,m)*w(m)
               sumx = sumx + u(k)*tx*b
	       sumy = sumy + v(k)*ty*a
10          continue
	    if (ifirst.eq.0) write(6,'(2i3,2f12.5)') i,j,sumx,sumy
	    elemmt(ioff) = elemmt(ioff)+sumx+sumy
30    continue
      ifirst=1
      return
      end
