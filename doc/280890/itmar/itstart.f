c *************************************************************
c *   ITSTART
c *
c *   Starts ITMAR
c *
c *   PvK 040490
c *************************************************************
      subroutine itstart(kmesh1,kprob1,isol1,intmt1,kmesh2,kprob2,
     v                   isol2,intmt2,coormark,y0,dm,nbufdef,npmax)
      implicit double precision(a-h,o-z)
      dimension kmesh1(*),kprob1(*),isol1(*),intmt1(*)
      dimension kmesh2(*),kprob2(*),isol2(*),intmt2(*),y0(*),dm(*)
      dimension coormark(*),iu1(3),u1(3),wavel(10),ainit(10),rho(11)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /bmint/ ityps,itypt,nruls,nrult
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /peiter/ tmax,dtout,dtcfl,tfac,tstepmax,tvalid,difcor,
     v                difcormax,nitermax,nout,ncor
      common /pelinqua/ itypel,ishape
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /vislo/ ivl,ivt,ivn
      common /bstore/ f2name
      common /peparam/ rayleigh,rl,q,visc0,b,c
      common /colmar/ icol(10),ivol(10)
      common /pecgsol/ cgeps,ncgmax,iprec,icgprint,itmet
      common /citer/ omega,alpha,beta,alam,niter,niter1,niter2
      character*80 f2name
      logical ivl,ivt,ivn
      character*80 fnre,fnt,header

      read(5,*) header
c     *** header     for identification only

      read(5,*) ityps,itypt,nruls,nrult
c     *** ityps      element type for Stokes equation
c     *** itypt      element type for temperature equation
c     *** nruls      integration rule for Stokes equation
c     *** nrult      integration rule for temperature equation

      read(5,*) ram,rat,ism,ist,ibm,ibt
c     *** ram        compositional Rayleigh number
c     *** rat        thermal Rayleigh number
c     *** ism        (1) Transport the markers
c     *** ist        (1) Solve the temperature equation
c     *** ibm        (1) Take compositional bouyancy forces into account
c     *** ibt        (1) Take thermal bouyancy forces into account

      read(5,*) iqchois,q
c     *** iqchois    Choice parameter for function describing heat prod
c     ***            0   Q = constant = q
c     *** q          Heat production constant
      read(5,*) nitermax,dtout,nout,ncor
c     *** nitermax   Maximumal number of iterations
c     *** dtout      Output at t=t0+i*dtout, i=1,....
c     *** nout       Output every nout timesteps
c     *** ncor       Number of corrector steps

      read(5,*) tfac,tmax,trestart,tstepmax,tvalid
c     *** tfac       Fraction of the CFL criterion that is used to
c     ***            limit the timestep
c     *** tmax       Maximum time for the calculation
c     *** trestart   The modeltime of the restart
c     *** tstepmax   Maximum timestep. Only applied if 0 < t < tvalid
c     *** tvalid     See tstepmax

      read(5,*) nochain
c     *** nochain    Number of markerchains

      read(5,*) (y0(i),wavel(i),ainit(i),dm(i),i=1,nochain)
c     *** y0         Initial depth of markerchain
c     *** wavel      Wavelength of initial disturbance
c     *** ainit      Amplitude of initial disturbance
c     *** dm         Maximal spacing between the markers

      read(5,*) itypv,ctd,cpd
c     *** itypv      Type of viscosity law (see subroutine prepvis)
c     *** ctd        Constant expressing the temperature dependence
c     *** cpd        Constant expressing the depth dependence

      read(5,*) (rho(i),i=1,nochain+1)
c     *** rho        Density of each layer

      read(5,*) (viscl(i),i=1,nochain+1)
c     *** visco      Viscosity of each layer 

      read(5,*) irestart,fnre,fnt
c     *** irestart   (1) the problem is restarted
c     *** fnre       file that contains the positions of the 
c     ***            markerchains for the restart
c     *** fnt        bsfile containing the temperature field for the
c     ***            restart
      
      read(5,*) nxpix,nypix,mooi,istart,iend,istride
c     *** nxpix,nypix Resolution of the pixelfile 
c     *** mooi        Indication if program has to make a filled
c     ***             contourplot of the composition (1) or just
c     ***             to outline the boundaries (0)
c     *** istart      Sequence number of first stored solution to
c     ***             be processed.
c     *** iend        same, for last
c     *** istride     Number of vectors to be skipped each time

      read(5,*) (icol(i),i=1,nochain+1)
c     *** icol        Color numbers for each layer. Only used
c     ***             in program postmar

      read(5,*) itmet,cgeps
c     *** itmet       Choice of iterative method:
c     ***          1  CG
c     ***          2  overrelaxation
c     *** cgeps       Accuracy of CG solver
c     *** ncgmax      Maximum number of iterations
c     *** iprec       Type of preconditioner
c     *** icgprint    Controls amount of output of CONGRD
      ncgmax = 200
      iprec  = 3
      icgprint = 0
      

      nl = nochain+1
      if (nl.gt.10) then
         write(6,*) 'PERROR(tstart): nl > 10 not implemented'
         stop
      endif
c     *** standard Sepran start
      call start(0,1,0,0)
      nbuffr = nbufdef
c     *** Prepare common /CITER/
      if (ic.eq.1) then
         niter1 = n1
         niter2 = n2
         alpha  = al
         beta   = be
      endif
      call permesh(kmesh1,kmesh2)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)
      if (itmet.eq.0) then
         jmetod = 1
      else if (itmet.eq.1) then
         jmetod = 5
      else if (itmet.eq.2) then
         jmetod = 9
      endif
      call commat(jmetod,kmesh1,kprob1,intmt1)
      call commat(2,kmesh2,kprob2,intmt2)
      nsup1 = kprob1(29)
      nsup2 = kprob2(29)
      kamt1= intmt1(5)
      kamt2= intmt2(5)
      write(6,'(''nsuper = '',2i10)') nsup1,nsup2
      write(6,'(''kamat  = '',2i10)') kamt1,kamt2
      if (nsup1.gt.0.or.nsup2.gt.0) stop
      npoint  = kmesh1(8)
      if (npoint.gt.npmax) then
         write(6,*) 'PERROR(tstart) npoint > npmax' 
         stop
      endif

c     *** determine type of viscosity law
      ivt = .false.
      ivl = .false.
      ivn = .false. 
      jtypv = abs(itypv)
      if (jtypv.eq.2.or.jtypv.eq.3.or.jtypv.eq.6.or.jtypv.eq.7) then
c        *** viscosity is temperature dependent
         ivt = .true.
      endif
      if (jtypv.eq.1.or.jtypv.eq.3.or.jtypv.eq.5.or.jtypv.eq.7) then
c        *** viscosity is layer dependent
         ivl = .true.
      endif
      if (jtypv.ge.4) then
         ivn = .true.
      endif


c     *** Determine aspect ratio
      ishape = 1
      call pefilxy(ishape,kmesh1,kprob1,isol1)
      rlam = xcmax-xcmin
      rl   = rlam
         
      if (irestart.eq.0) then
c        *** Initial condition is given by a sinusoidal disturbance
c        *** of the markerchains at depth y=y0(i) with amplitude
c        *** ainit(i) and wavelength wavel(i). The number of markers
c        *** on each chain is determined by the minimal spacing dm(i)
         if (ism.eq.1.or.ibm.eq.1) then
            t = 0d0
            ip = 0
            do 10 ichain=1,nochain
               imark(ichain) = rlam/dm(ichain) + 1
               nmark = imark(ichain)
               dm(ichain) = rlam/(nmark-1)
               piw = 3.1415926/wavel(ichain)
               do 20 i=1,nmark
                  x = dm(ichain)*(nmark-i)
                  y = y0(ichain) + ainit(ichain)*cos(piw*x)
                  coormark(ip+2*i-1) = x
                  coormark(ip+2*i)   = y
20             continue
               ip = ip + 2*nmark
10          continue
        endif
c       *** initial condition for streamfunction
         do 15 i=1,3
           iu1(i) = 0
            u1(i) = 0d0
15       continue
         call creavc(0,1,ivec,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
         if (ivt.or.ist.eq.1.or.ibt.eq.1) then
c           *** initial condition for the temperature
            iu1(1) = 2+ibt
            call creavc(0,1,ivec,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
         endif
      else
c        *** Restart
         if (ism.eq.1.or.ibm.eq.1) then
c           *** read markerchains from file 'fnre'
	    open(9,file=fnre,form='unformatted')
	    rewind(9)
	    read(9) t,nochain,(imark(ichain),ichain=1,nochain)
            ntot = 0
            do 25 ichain=1,nochain
25             ntot = ntot+imark(ichain)
            read(9) (coormark(i),i=1,2*ntot)
         endif
         if (ibt.eq.1.or.ist.eq.1) then
c            *** read temperature and streamfunction from file 'fnt'
             f2name = fnt
             call openf2(.false.)
             if (ivn.eq.1) call readbs(1,isol1,ihelp)
             call readbs(2,isol2,ihelp)
             t = trestart
         endif
      endif

c     *** Calculate weights
      do 35 i=2,nochain+1
         wm(i-1) = ram*(rho(i-1)-rho(i))
35    continue


      rayleigh = 1d0
      visc0 = 1d0
      b = 0
      c = 0
 
      open(12,file='times.output')
      rewind(12)
      open(20,file='tmar.data')
      rewind(20)
      open(21,file='tmar.der')
      rewind(21)
      open(9,file='markers.back',form='unformatted')
      rewind(9)
      write(9) t,nochain,(imark(ichain),ichain=1,nochain)
      ntot = 0
      do 18 ichain=1,nochain
18       ntot = ntot + imark(ichain)
      write(9) (coormark(i),i=1,2*ntot)
      write(6,*) 'Markers stored at t= ',t
      call flush(9)
      f2name = 'temp.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'isol1',isol1,ihelp)
      call writbs(0,2,numarr,'isol2',isol2,ihelp)
      call writb1
      write(12,*) 1,t

      return
      end
