      implicit double precision(a-h,o-z)
      dimension y0(10),wavel(10),ainit(10),dm(100),it(10)
      dimension coormark(10000),imark(10)
      read(5,*) nochain,rlam
      read(5,*) (y0(i),wavel(i),ainit(i),dm(i),it(i),i=1,nochain)

      ip = 0
      do 10 ichain=1,nochain
         imark(ichain) = rlam/dm(ichain) + 1
         nmark = imark(ichain)
         dm(ichain) = rlam/(nmark-1)
         piw = 3.1415926/wavel(ichain)
         if (it(ichain).eq.0) then
c           *** No perturbance
            do 15 i=1,nmark
               coormark(ip+2*i-1) = dm(ichain)*(nmark-i)
               coormark(ip+2*i) = y0(ichain)
15          continue
         else if (it(ichain).eq.1) then
c           *** Sinusoidal perturbance
            do 20 i=1,nmark
               x = dm(ichain)*(nmark-i)
               y = y0(ichain) + ainit(ichain)*cos(piw*x)
               coormark(ip+2*i-1) = x
               coormark(ip+2*i)   = y
20          continue
         else if (it(ichain).eq.2) then
c           *** triangular perturbance
            do 30 i=1,nmark
               x = dm(ichain)*(nmark-i)
               y = y0(ichain)
               coormark(ip+2*i-1) = x
               coormark(ip+2*i)   = y
30          continue 
            do 35 i=1,5
               y = ainit(ichain)*(6-i)*0.2 + y0(ichain)
               im = ip + 2*nmark - (i-1)*2
               coormark(im)  = y
35          continue
         endif
         ip = ip + 2*nmark
10      continue
      open(9,file='markers',form='unformatted')
      rewind(9)
      write(9) t,nochain,(imark(ichain),ichain=1,nochain)
      ntot = 0
      do 18 ichain=1,nochain
18       ntot = ntot + imark(ichain)
      write(9) (coormark(i),i=1,2*ntot)
      close(9)
      open(10,file='marker.form')
      rewind(10)
      write(10,*) t,nochain,(imark(ichain),ichain=1,nochain)
      ntot = 0
      ip = 0
      do 19 ichain=1,nochain
         do 21 i=1,imark(ichain)
            write(10,*) coormark(ip+2*i-1),coormark(ip+2*i)
21       continue
         ip = ip+2*imark(ichain)
         write(10,*)
19    continue
      
      end
