c *************************************************************
c *   REMARKER
c *   
c *   Regrid the markerchain. The length of the markerchain
c *   is computed and markers are distributed on the chain
c *   with a maximum distance DMAX. If more than NMAX markers
c *   are necessary this condition is relaxed and exactly NMAX
c *   markers are distributed on the chain.
c *   The position of the new markers is obtained by a linear
c *   interpolation of the old ones.
c *
c *   PvK 211189
c *************************************************************
      subroutine remarker(cm,cn,dmax,nmax)
      implicit double precision(a-h,o-z)
      dimension cm(2,*),cn(2,*),dmax(*),nmnew(10)
      common /c1mark/ wm(10),nochain,ichain,imark(10)

      ipm = 0
      ipn = 0
      do 100 ichain=1,nochain
         nmark = imark(ichain)
         rl = 0
         do 10 i=2,nmark
            dx = cm(1,ipm+i-1) - cm(1,ipm+i)
            dy = cm(2,ipm+i-1) - cm(2,ipm+i)
10       rl = rl + sqrt(dx*dx+dy*dy)
         nsegnew = rl/dmax(ichain)
         err  = rl-nsegnew*dmax(ichain)
         if (err.gt.dmax(ichain)) stop 'Foutje in err'
      
         nnew = nsegnew + 1
         if (nnew+1.gt.nmax) then
            write(6,*) 'PWARNING(remarker): nnew > nmax'
            nnew = nmax-1
            dnew = rl/nnew
         else 
            dnew = dmax(ichain)
         endif
      
         cn(1,ipn+1) = cm(1,ipm+1)
         cn(2,ipn+1) = cm(2,ipm+1)
         t = 0
         x1 = cm(1,ipm+1)
         y1 = cm(2,ipm+1)
         x2 = cm(1,ipm+2)
         y2 = cm(2,ipm+2)
         s1 = 0
         s2 = sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
         d = s2-s1
         i=1
         j=2
20       continue
           t = t+dnew
30         continue
           if (t.lt.s2) then
c             *** interpolate
              xt = x1 + (t-s1)/d*(x2-x1)
              yt = y1 + (t-s1)/d*(y2-y1)
              i = i+1
              cn(1,ipn+i) = xt
              cn(2,ipn+i) = yt
           else
c             *** new segment
              j = j+1
              x1 = x2
              y1 = y2
              x2 = cm(1,ipm+j)
              y2 = cm(2,ipm+j)
              s1 = s2
              s2 = s1 + sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
              d = s2-s1
              goto 30
           endif
           if (i.eq.nnew) then
              cn(1,ipn+nnew+1) = cm(1,ipm+nmark)
              cn(2,ipn+nnew+1) = cm(2,ipm+nmark)
              nmnew(ichain) = nnew+1
           else
              goto 20
           endif
           ipm = ipm+nmark
           ipn = ipn+nmnew(ichain)
100    continue
   
       do 200 ichain=1,nochain
          imark(ichain) = nmnew(ichain)
200    continue
       return 
       end

       
       
