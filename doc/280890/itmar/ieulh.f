c *****************************************************************
c *   IEULH
c *
c *   The temperature equation is solved using implicit euler
c *   Information concerning the coefficients of the equation
c *   are stored in (i)user and ivcold.
c * 
c *
c *   Pvk  160589/290889
c ****************************************************************
      subroutine ieulh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,matrm,irhsd)
      implicit double precision(a-h,o-z)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)
      common /bmint/ ityps,itypt,nruls,nrult
      common /peinterg/ nrule
      common /petime/ idia
      
      dimension kmesh(*),kprob(*),intmat(*),isol(*),user(*),iuser(*)
      dimension matrs(*),matrm(*),irhsd(*)
      dimension islold(*),ivec1(5),ivec2(5),ivec3(5),matr1(5)
      dimension ipser(100),pser(100)
      character*10 tname
      real t0,t1

      save ifirst
      data ifirst/0/

      nrule = nrult
      idia = 1
      call copyvc(isol,islold)
      if (idia.eq.2) then
         call chtype(1,kprob)
c        *** Built matrices and rhs vector at time t
c        *** Stiffness matrix
         call systm0(11,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         if (ifirst.eq.0) then
c           *** Non-diagonal mass matrix
            call chtype(2,kprob)
            ifirst=1
            ipser(1) = 100
             pser(1) = 100
            ipser(6) = 7
            ipser(7) = -6
             pser(6) = 1d0
            call systm0(11,matrm,intmat,kmesh,kprob,mrhsd,isol,
     v                  ipser,pser,islold,ielhlp)
         endif
      else
c        *** Stiffness matrix and lumped mass matrix
         if (ifirst.eq.0) then
	    ifirst= 1
            imas  = 1
         else
	    imas  = 0
         endif
	 call systm1(11,imas,matrs,intmat,kmesh,kprob,irhsd,matrm,
     v               isol,iuser,user,islold,ielhlp)
      endif

c     *** Built the system of equations
     
c     *** M*uold
      ichois=idia+3
      call maver(matrm,islold,ivec1,intmat,kprob,ichois)

c     *** tstep*f + M*uold
      call algebr(3,0,ivec1,irhsd,ivec3,kmesh,kprob,1d0,tstep,p,q,ip)

c     *** M + tstep*S
      call copymt(matrs,matr1,kprob)
      call addmat(kprob,matr1,matrm,intmat,tstep,alpha2,1d0,beta2)

c     *** ( M + tstep*S ) * usol(p)
      call maver(matr1,isol,ivec2,intmat,kprob,6)

c     *** tstep*f + M*uold - (M+tstep*S) * usol(p)
      call algebr(3,0,ivec3,ivec2,ivec1,kmesh,kprob,1d0,-1d0,p,q,ip)

c     *** Solve the system
c     write(6,*) 'IEULER: solve'

      call solve(0,matr1,isol,ivec1,intmat,kprob)

      return
      end
