c *************************************************************
c *   CHECKVOL
c *
c *   Check the volume of each layer by counting the number of
c *   pixels in each layer.
c *   Array ipix has to be filled
c * 
c *   PvK 060890
c *************************************************************
      subroutine checkvol
      implicit double precision(a-h,o-z)
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /colmar/ icol(10),ivol(10)
      dimension vol(10)

      do 10 il=1,nochain+1
         ivol(il) = 0
10    continue
      ntot = nxpix*nypix
      do 20 ip=1,ntot
         ivol(ipix(ip)) = ivol(ipix(ip))+1
20    continue
      do 30 il=1,nochain+1
         vol(il) = ivol(il)*1d0/ntot
30    continue
      write(6,'(''Volumes: '',10f6.3,:)') (vol(i),i=1,nochain+1)
      return
      end
