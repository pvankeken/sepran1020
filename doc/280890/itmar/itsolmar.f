c *************************************************************
c *   ITSOLMAR
c *
c *   Solves the biharmonical equation
c *
c *          4
c *      grad  psi  =   -Ram*dGAMMA/dx  + Rat*dT/dx
c *
c *   where GAMMA is a stepfunction, of which the position is 
c *   defined by a markerchain and T is the temperature
c *   Ram and Rat indicate the compositional and thermal Rayleigh
c *   numbers resp.
c *   THE nonconforming element is used
c *   The matrix equation is solved using the conjugate gradients
c *   method.
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for psi
c *       matr    o  Stiffness matrix (constant in this problem)
c *       imark   i  Integer information on markerchain
c *       coormark i Coordinates of markers (x1,y1,x2,y2,....)
c *
c *   common /c1solve/ rat,ram,ism,ist,ibm,ibt
c *       ram        Compositional Rayleigh number
c *       rat        Thermal rayleigh number
c *       ism        Flag: (1) advance markerchains
c *       ist        Flag: (1) convect temperature field
c *       ibm        Flag: (1) include compositional buoyancy forces
c *       ibt        Flag: (1) include thermal buouyancy forces
c *
c *   common /pecgsol/ cgeps,ncgmax,iprec,icgprint
c *       cgeps      Accuracy of method
c *       ncgmax     maximal number of iterations
c *       iprec      type of preconditioning
c *       icgprint   controls amount of output of CONGRD
c *       itmet      Choice of method: 1 = CG, 2 = SOR
c *
c *   PvK 040490/120890
c *************************************************************
      subroutine itsolmar(kmesh,kprob,istrm,intmat,matr,iuser,user,
     v                  coormark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),matr(*)
      dimension iuser(*),user(*),coormark(*),islold(5)
      dimension irhsd(5),ius(20),us(10),irhtus(5),matrback(5)
      dimension icontr(10)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
      common /bmint/ ityps,itypt,nruls,nrult
      common /peinterg/ nrule
      common /pecgsol/ cgeps,ncgmax,iprec,icgprint,itmet
      common /citer/ omega,alpha,beta,alam,niter,niter1,niter2
      save ifirst
      data ifirst/0/
      real t0,t1

      npoint = kmesh(8)
      nrule = nruls

c     *** *************** BUILD MATRIX *********************
c     *** Information concerning the viscosity is stored in 
c     *** array user (temperature) and/or commons cpix and
c     *** c1visc, depending on the type of viscosity ITYPV.
c     *** This information has been stored previously by
c     *** subroutine PREPVIS
c     *** ***************************************************
      if (ifirst.eq.0.or.itypv.gt.0) then
c        *** Build matrix; clear rhsd
         call copyvc(istrm,islold)
         call systm0(3,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v               user,islold,ielhlp)
         call copymt(matr,matrback,kprob)
      else
         call systm0(5,matrback,intmat,kmesh,kprob,irhsd,istrm,
     v               iuser,user,islold,ielhlp)
c        *** Don't build matrix; clear rhsd
      endif

c     *** **************** BUILD VECTOR *********************
c     *** A rhs vector is created for each source of buoyancy.
c     *** The final vector is a weighted sum of these vectors
c     *** following the formula
c     ***
c     ***              nochain  
c     ***   f  =  - Ra  sum   w(i)*dGamma(i)/dx  + Ra  dT/dx
c     ***             b i=1                          t
c     ***
c     *** ***************************************************
      if (ibm.eq.1) then
         icrhsd = 1
         iuser(6) = icrhsd
         ip = 0
         do 10 ichain=1,nochain
c           ***   Fill positions of markers in array USER, starting at
c           ***   6+npoint. Fill elementnumber for each marker in IUSER
c           ***   starting at 7.
            if (wm(ichain).eq.0d0) goto 11
            nmark = imark(ichain)
            if (npoint+2*nmark+7.gt.user(1)) then
               stop
            endif
            do 12 i=1,2*nmark
12             user(5+i+npoint) = coormark(i+ip)
c           *** determine the numbers of the elements in which 
c           *** the marker are
            do 20 i=1,nmark
               xm = coormark(ip+2*i-1)
               ym = coormark(ip+2*i)
               call pedetel(2,xm,ym,iel)
               iuser(7+i) = iel
20          continue

c           *** Built intermediate rhs vector and add
            call systm0(12,matr,intmat,kmesh,kprob,irhtus,istrm,iuser,
     v                  user,islold,ielhlp)
            aa = 1d0
            bb = wm(ichain)
            call algebr(3,1,irhsd,irhtus,irhsd,kmesh,kprob,aa,bb,p,q)
11          continue

            ip = ip+2*nmark
10       continue
      endif

      if (ibt.eq.1) then
c        *** The temperature is stored in user(6)...user(5+npoint)
         icrhsd = 3
         iuser(6) = icrhsd
         call systm0(12,matr,intmat,kmesh,kprob,irhtus,istrm,iuser,
     v               user,islold,ielhlp)
         aa = 1d0
         bb = rat
         call algebr(3,1,irhsd,irhtus,irhsd,kmesh,kprob,aa,bb,p,q)
         
      endif


      if (itmet.eq.1) then
c        *** Conjugate gradient method
         icontr(1) = 0
         icontr(2) = ifirst
         icontr(3) = 1
         icontr(4) = icgprint
         icontr(5) = iprec
c        if (ifirst.eq.0) icontr(5) = iprec
         call second(t0)
         call congrd(-1,icontr,matr,istrm,irhsd,intmat,kprob,cgeps,
     v               ncgmax,irestr)
         call second(t1)
         write(6,*) 'congrd: ',t1-t0
         if (icontr(6).ne.0) then
            write(6,*) 'PERROR(itsolmar): error in congrd = ',icontr(6)
            write(6,*) '                  niter           = ',icontr(1)
         endif
      else if (itmet.eq.2) then
         call second(t0)
         eps = -cgeps
         write(6,*) 'N1    = ',niter1
         write(6,*) 'N2    = ',niter2
         write(6,*) 'alpha = ',alpha 
         write(6,*) 'beta  = ',beta
         write(6,*) 'omega = ',omega
         call overrl(1,matr,irhsd,istrm,intmat,kmesh,kprob,eps,ncgmax)
         call second(t1)
         write(6,*) 'overrl: ',t1-t0
         write(6,*) 'omega:  ',omega
         write(6,*) 'niter:  ',niter
         write(6,*) 'alam:   ',alam
      else
         call second(t0)
         call solve(1,matr,istrm,irhsd,intmat,kprob)
         call second(t1)
         write(6,*) 'solve : ',t1-t0
      endif
      factor = 1d0
      npoint = kmesh(8)
      call pecopy(10,istrm,user,kprob,6,factor)
      call pecopy(11,istrm,user,kprob,6+npoint,factor)
      call pecopy(12,istrm,user,kprob,6+2*npoint,factor)

      ifirst = 1
      return
      end
