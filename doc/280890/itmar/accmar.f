c *************************************************************
c *   ACCMAR
c *
c *   ichois = 1   : maximum difference
c *   ichois = 2   : rms difference
c *
c *   PvK 231189
c *************************************************************
      function accmar(ichois,coor1,coor2)
      implicit double precision(a-h,o-z)
      dimension coor1(*),coor2(*)
      common /c1mark/ wm(10),nochain,ichain,imark(10)

    
      if (ichois.eq.1) then
         accmar = 0
         ip = 0
         do 100 ichain=1,nochain
            nmark=imark(ichain)
            do 10 i=1,2*nmark
               d = abs(coor1(ip+i)-coor2(ip+i))
10             accmar = max(accmar,d)
            ip = ip+2*nmark
100      continue
      else if (ichois.eq.2) then
         d = 0
         ip = 0
         do 110 ichain=1,nochain
            nmark = imark(ichain)
            do 20 i=1,2*nmark
              d = coor1(ip+i) - coor2(ip+i)
              sum = sum + d*d
20          continue
            ip = ip+2*nmark
110      continue
         accmar = sqrt(sum)
      else
         write(6,*) 'PERROR(accmar): unknown option ',ichois
         stop
      endif

      return
      end
