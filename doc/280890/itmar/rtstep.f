      subroutine rtstep(tstep)
      implicit double precision (a-h,o-z)
      rlogje = log10(tstep)
      if (rlogje.ge.0) then
         magn = rlogje
         itus = tstep/10.**magn
         tstep  = itus*1.0*(10.**magn)
      else
         magn = rlogje - 1
         tmagn = 10.**magn
         itus = int(tstep/tmagn)
         tstep = itus*1.0*tmagn
      endif
      end
