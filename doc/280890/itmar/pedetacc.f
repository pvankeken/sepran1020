c *************************************************************
c *   PEDETACC
c *
c *   Determine accuracy of PC-scheme using formula 14 of
c *   Christensen and Yuen 1984 JGR 84 4392
c *************************************************************
      function pedetacc(kmesh,kprob,islold,isol1,isol2)
      implicit double precision (a-h,o-z)
      common /carray/ iinfor,infor(3,1500)
      common ibuffr(1)
      dimension kmesh(*),kprob(*),islold(*),isol1(*),isol2(*)
   
      call ini050(isol1(1),'pedetacc: isol1')
      call ini050(isol2(1),'pedetacc: isol2')
      npoint = kmesh(8)
      iisol1 = infor(1,isol1(1))
      iisol2 = infor(1,isol2(1))
      call pedet1(npoint,ibuffr(iisol1),ibuffr(iisol2),difpc,difno)

      dif  = difpc/difno

c     write(6,100) dif
100   format(' | Tc - Tp | / | Tc | = ',f12.7)
      pedetacc=dif
      return
      end

      subroutine pedet1(npoint,usol1,usol2,difpc,difno)
      implicit double precision(a-h,o-z)
      dimension usol1(npoint),usol2(npoint)

      difpc = abs(usol1(1)-usol2(1))
      difno = abs(usol2(1))
      do 10 i=2,npoint
	 difpc = max(difpc,abs(usol1(i)-usol2(i)))
	 difno = max(difno,abs(usol2(i)) )
10    continue
      return
      end

