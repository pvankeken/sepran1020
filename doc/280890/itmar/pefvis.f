c *************************************************************
c *   PEFVIS
c *
c *   Calculate depth and temperature dependent part of viscosity
c *
c *   PvK 210890
c *************************************************************
      function pefvis(t,z)
      implicit double precision(a-h,o-z)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
    
      if (z.gt.0.5d0) then
         vis = 1d0
      else
         vis = 10.**(2.*(1.-z/0.25d0))
      endif

      pefvis = vis

      return
      end
