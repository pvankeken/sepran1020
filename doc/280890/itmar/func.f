      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      if (ichois.eq.1) then
c        *** streamfunction
         func = 0
      endif
      if (ichois.eq.2.or.ichois.eq.3) then
c        *** temperature
         wpi = pi/rlam
         func = 0.01*(ichois-2)*sin(pi*y)*cos(wpi*x) + (1-y)
      endif
      return
      end
    
