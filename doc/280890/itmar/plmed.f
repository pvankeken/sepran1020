       subroutine plmed(screen, file, iopt)
      logical screen, file
      common / seppl1 / iway
      common / seppl2 / fname
      character*80 fname
      common /frcount/ ifrinit, ifrcntr, nbase
      character*4 chrbf
      screen = .false.
      file   = .false.
      if (ifrinit .ne. 1) then
           ifrinit = 1
           ifrcntr = 0
           nbase = lnblnk(fname)
           if(nbase .eq. 0) then
               fname = 'PLOT'
               nbase = lnblnk(fname)
           end if
      end if
      iopt   = 1
      if(iway .eq. 1) then
         screen = .true.
      else if(iway .eq. 2) then
         file   = .true.
      else if(iway .eq. 3) then
         screen = .true.
         file   = .true.
      else 
           iopt = 0
      end if
      if(file) then
          ifrcntr = ifrcntr + 1
          write(chrbf,'(i4)') 1000 + ifrcntr
          fname = fname(1:nbase)//'.'//chrbf(2:4)
      end if
      return
      end

      function lnblnk(fname)
      character*(*) fname
      nl = len(fname)
      do 10 i=nl,1,-1
         if (fname(i:i).ne.' ') then
            lnblnk = i
            return
         endif
10    continue
      lnblnk=0
      return
      end
