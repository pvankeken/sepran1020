c **********************************************************
c *   PLOTNC
c *
c *   Plots the shapefunctions of the non-conforming rectangular
c *   bending element (Zienkewiecz 234; implementation from
c *   Cheung p.19)
c * 
c *   PvK  140390
c ******************************************************************
      program heat
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),iuser(100),user(5000)
      dimension intmat(5),isol(5),islold(5),istold(5),irhsd(5)
      dimension matrs(5),matrm(5),u1(2),iu1(2),contln(15),ivec1(5)
      dimension ivec2(5)
      integer restart

      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ alpha,iupwind
      common /usertext/text,text1,text2
      common /pefil/idia
      character*80 text,text1,text2

      kmesh(1)=100
      kprob(1)=100
      iuser(1)=100
       user(1)=5000
      contln(1)=15

      call start(0,0,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)

      form1=5d0
      form2=5d0
      form3=8d0
      form4=12d0
      do 10 i=1,3

	 write(6,*) 'plot1',i
	 write(text,'(a14,2i2)') 'Shapefunction ',i,1
	 call frstack(0)
         iu1(1)=(i-1)*10+1
         call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
         call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)

	 write(6,*) 'plot2',i
c call plwin(2,0d0,form3)
c write(6,*) 'Na plwin'
	 write(text,'(a14,2i2)') 'Shapefunction ',i,2
         iu1(1)=(i-1)*10+2
         call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
	 call frstack(0)
         call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)

	 write(6,*) 'plot3',i
	 write(text,'(a14,2i2)') 'Shapefunction ',i,3
c call plwin(2,form4,-form3)
         iu1(1)=(i-1)*10+3
         call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
	 call frstack(0)
         call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)

	 write(6,*) 'plot4',i
	 write(text,'(a14,2i2)') 'Shapefunction ',i,4
c        call plwin(3,0d0,form3)
         iu1(1)=(i-1)*10+4
         call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
	 call frstack(0)
         call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)
10    continue
      call frstack(1)

      end

      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      dimension xk(4),yk(4)
      dimension xik(4),etak(4),rn1(4),rn2(4),rn3(4)
      data (xk(i),i=1,4)/0,1,1,0/
      data (yk(i),i=1,4)/0,0,1,1/


      shapeche = 0
      a = xk(2)-xk(1)
      b = yk(3)-yk(2)
      do 10 i=1,4
         xik(i) = xk(i)/a
	 etak(i)= yk(i)/b
10    continue
      xi = x/a
      eta= y/b
      if (ichois.eq.1) then
        func = 1-xi*eta-(3-2*xi)*xi*xi*(1-eta)-(1-xi)*(3-2*eta)*eta*eta
      else if (ichois.eq.2) then
        func = (3-2*xi)*xi*xi*(1-eta)+xi*eta*(1-eta)*(1-2*eta)
      else if (ichois.eq.3) then
        func = (3-2*xi)*xi*xi*eta-xi*eta*(1-eta)*(1-2*eta)
      else if (ichois.eq.4) then
        func = (1-xi)*(3-2*eta)*eta*eta+xi*(1-xi)*(1-2*xi)*eta

      else if (ichois.eq.11) then
         func = (1-xi)*eta*(1-eta)*(1-eta)*b
      else if (ichois.eq.12) then
         func = xi*eta*(1-eta)*(1-eta)*b
      else if (ichois.eq.13) then
         func = -xi*(1-eta)*eta*eta*b
      else if (ichois.eq.14) then
         func = -(1-xi)*(1-eta)*eta*eta*b

      else if (ichois.eq.21) then
         func = -xi*(1-xi)*(1-xi)*(1-eta)*a
      else if (ichois.eq.22) then
         func = (1-xi)*xi*xi*(1-eta)*a
      else if (ichois.eq.23) then
         func = (1-xi)*xi*xi*eta*a
      else if (ichois.eq.24) then
         func = -xi*(1-xi)*(1-xi)*eta*a
      endif

      return
      end
c *************************************************************
c *    Shapefunction of the non-conforming cubic element
c *    with 4 nodalpoints and unknowns (psi,u,v) 
c *
c *    Formulation of Cheung
c *************************************************************
       function shapeche(xk,yk,x,y,psi,u,v)
       implicit double precision(a-h,o-z)
       dimension xk(4),yk(4),psi(4),u(4),v(4)
       dimension xik(4),etak(4)
       dimension rn1(4),rn2(4),rn3(4)

       shapeche = 0
       a = xk(2)-xk(1)
       b = yk(3)-yk(2)
       do 10 i=1,4
	 xik(i) = xk(i)/a
	 etak(i)= yk(i)/b
10     continue
       xi = x/a
       eta= y/b
       rn1(1) = 1-xi*eta-(3-2*xi)*xi*xi*(1-eta)-(1-xi)*(3-2*eta)*eta*eta
       rn2(1) = (1-xi)*eta*(1-eta)*(1-eta)*b
       rn3(1) = -xi*(1-xi)*(1-xi)*(1-eta)*a
       rn1(2) = (1-xi)*(3-2*eta)*eta*eta+xi*(1-xi)*(1-2*xi)*eta
       rn2(2) = -(1-xi)*(1-eta)*eta*eta*b
       rn3(2) = -xi*(1-xi)*(1-xi)*eta*a
       rn1(3) = (3-2*xi)*xi*xi*eta-xi*eta*(1-eta)*(1-2*eta)
       rn2(3) = -xi*(1-eta)*eta*eta*b
       rn3(3) = (1-xi)*xi*xi*eta*a
       rn1(4) = (3-2*xi)*xi*xi*(1-eta)+xi*eta*(1-eta)*(1-2*eta)
       rn2(4) = xi*eta*(1-eta)*(1-eta)*b
       rn3(4) = (1-xi)*xi*xi*(1-eta)*a
       r1=0
       r2=0
       r3=0
       do 20 i=1,4
	 write(6,'(3f12.4)') rn1(i),rn2(i),rn3(i)
	 r1 = rn1(i)+r1
	 r2 = rn2(i)+r2
	 r3 = rn3(i)+r3
	 shapeche = shapeche + rn1(i)*psi(i) + rn2(i)*u(i) + rn3(i)*v(i)
20     continue
       write(6,*) '----------------------------------------------'
       write(6,'(3f12.4)') r1,r2,r3
       write(6,*)
       return
       end
