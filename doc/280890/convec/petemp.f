c ****************************************************************
c *   PETEMP
c *
c *   Stores the solution vector in array usol in common /pectmp/
c *   If the solution vector corresponds to the temperature
c *   it may be used to calculate temperature dependent viscosity
c *   by funcfl.
c *
c *   PvK, 8-5-89
c *****************************************************************
      subroutine petemp(isol,kmesh,kprob)
      implicit double precision(a-h,o-z)

      common ibuffr(1)
      dimension isol(*),kmesh(*),kprob(*)

      iprob = isol(4)/1000 
      iprob = iprob+1
      nprob = max(1,kprob(40))
      if (iprob.lt.1.or.iprob.gt.nprob) call ertrap(
     .   'petemp',699,2,iprob,0,0)
      ipkprb = 1
      do 1 i=2,iprob
1          ipkprb = ipkprb + kprob(2+ipkprb)

c     *** Calculate pointer to solution vector in ibuffr
      itypvc = 0
      kfree  = 0
      call ini002(itypvc,isol,kmesh,kprob(ipkprb),kfree,nunkp,
     v            nusol,ipusol,ipkprf,ipkprbf,ipkprh,ikprbh,
     v            .false.,.false.,'petemp',ivec)

c     *** Actual calculation
      npoint = kmesh(8)
      call petmp1(npoint,ibuffr(ipusol),ibuffr(ipkprh),ikprbh,
     v            nunkp)


      return
      end
