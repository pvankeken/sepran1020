c ********************************************************
c *   PRINTALLES
c *
c ********************************************************
      subroutine printalles(ichois,name,length,iarray,rarray)
      implicit double precision(a-h,o-z)
      dimension iarray(*),rarray(*)
      character*(*) name
      
      write(6,*) 'PRINTALLES: ',name
      if (length.gt.0) then
         irest = length - length/10
         do 10 i=1,length,10
            n=i-1
            write(6,'(i3,a2,10i5)') n,':',(iarray(n+j),j=1,10)
10       continue
         n = iarray(3)-i-j
20       write(6,*) (iarray(i+j),j=1,n)
      else
         length = -length
         irest = length - length/5
         do 30 i=1,length,5
            n=i-1
            write(6,'(i3,a2,5e10.3)') n,':',(rarray(n+j),j=1,5)
30       continue
         n = rarray(3)-i-j
40       write(6,*) (rarray(j+i),j=1,n)
      endif

      return
      end
