c **********************************************************
c *   CONVEC
c *
c *   Solving the impulsequation and temperature equation
c *   decoupled.
c *   A viscous fluid layer is heated from below. Symmetry
c *   conditions are imposed at the vertical boundaries at
c *   x=0 and x=1. The boundaries are stress free.
c *
c *   Two dimensional arrays are used for this purpose:
c *   array(x,1) contains information concerning the impulsequation
c *   array(x,2) contains information concerning the temp equation
c *   PvK 020389
c ******************************************************************
c *   Used files:
c *      convec.in      Standard input file. Contains Sepran input
c *                     and extra parameters.
c *      convec.back    File for backingstorage. Contains the
c *                     solution vector.
c *      meshoutput     Output of program sepmesh
c *      convec.data    Output file: information concerning the
c *                     number of iterations and used cpu time
c *      convec.stop    File used to control restart. After each
c *                     iteration the contents of this file are
c *                     inspected. The file must contain three
c *                     (evt. dummy) parameter: interrup,initer,cpui
c *                     At the start of the program interrup has the
c *                     following meaning: 0=new situation 1=restart
c *                     with same parameters 2=restart with new 
c *                     parameters: use solution from backing storage
c *                     as initial condition. During the program 
c *                     execution interrup may be changed: interrup=1
c *                     forces the program to exit after the current
c *                     iteration.
c **********************************************************************
      program convec
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(200),iuser(100,2),user(10000,2)
      dimension matr(5,2),irhsd(5,2),contln(100),indcol(100)
      dimension intmat(5,2),isol(5,2),iu1(10),u1(10),islold(5,2)
      dimension massmt(5,2),idum(5),funcx(100),funcy(100)
      dimension ivcold(5,2),matrback(5)
      integer restart

      common /ctime/ theta,deltat,rtime(7),nstep,itime(9)
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,
     v               jkader,jtimes
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /usertext/ text,text1,text2
      common /seppl1/ iway
      common /seppl2/ fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /bstore/ f2name
      common /devlib/ device
      common /pevisc/ visc0,b,c,rayleigh,wavelength
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      character*80 text,text1,text2,fname,f2name,device,datime
      character*80 line1,line2

      kmesh(1)=100
      kprob(1)=200
      iuser(1,1)=100
       user(1,1)=10000
      iuser(1,2)=100
       user(1,2)=10000
      contln(1)=100
      indcol(1)=100
       funcx(1)=100
       funcy(1)=100
       f2name = 'convec.back'

      call sepstr(kmesh,kprob,intmat)
      call presdf(kmesh,kprob,isol)
c     call printalles(ichois,'kprob',100,kprob,rdum)

      read(5,*) iway
      read(5,*) format,chheight,yfaccn,jsmoot
      read(5,*) nplax,npltax,ncolchois
      read(5,*) maxitr,eps,relax
      read(5,*) rayleigh,visc0,b,c,wavelength
      read(5,'(a)') line1
      read(5,'(a)') line2

      write(6,1000) datime
      write(6,1100) f2name
      write(6,1200) old
      write(6,1300) iway
      write(6,1400) format,chheight,yfaccn,jsmoot
      write(6,1500) nplax,npltax,ncolchois
      write(6,1600) maxitr,eps,relax
      write(6,1650) rayleigh,visc0,b,c,wavelength
   
      open(7,file='convec.stop')
      rewind 7
      read (7,*,err=7) interrup,initer,cpui
      goto 71
c     **** convec.stop didn't exist
7     continue
         write(6,*) 'PERROR(convec): convec.data does not exist'
         interrup=0
         initer=0
         cpui=0
71    continue
      rewind (7)
      write (7,*) 0,0,0
      close(7)
      if (interrup.ne.0) then
         restart = 1
      else
         restart = 0
      endif
      open(9,file='convec.data')
      if (interrup.ne.1) then
         write(9,'(a)') line1
         write(9,'(a)') line2
         write(9,'(''Nsuper = '',i10)') kprob(29)
      endif
      close(9)

      if (restart.eq.0) then
c        *** Create initial temperature field
         do 10 i=1,2
            iu1(i) = 0
             u1(i) = 0d0
10       continue
         call creavc(0,1,idum,isol(1,1),kmesh,kprob,iu1,u1,iu2,u2)
         iu1(1)=1
         call creavc(0,1001,idum,isol(1,2),kmesh,kprob,iu1,u1,iu2,u2)
      else
         call openf2(.false.)
         call readbs(1,isol(1,1),ihelp)
         call readbs(2,isol(1,2),ihelp)
      endif

      if (visc0.gt.0) then
c        *** Depth and temperature dependent viscosity are used
c        *** Store the actual value of the temperature in the nodes in
c        *** common /pectmp/tsol(10000)
         call petemp(isol(1,2),kmesh,kprob)
      endif

c     *** Fill coefficients
      call filcof(iuser(1,1),user(1,1),kprob,kmesh,1)
      call filcof(iuser(1,2),user(1,2),kprob,kmesh,2)


c     *** Start iteration
      do 101 i=1,5
         ivcold(i,1)=isol(i,1)
101      ivcold(i,2)=isol(i,2)
      istop = 0
      difmax = 0d0
      niter = 0
      call second(t0)
      if (interrup.eq.1) then
         t0 = t0 - cpui
         niter = initer
      endif
      tnew = t0
100   continue
        difold = difmax
        niter = niter+1

c       **************** IMPULS ***************************
c       *** Solve the impuls equation where T is given in isol(1,2)

        call copyvc(isol(1,1),islold(1,1))
        call copyvc(isol(1,2),islold(1,2))
        ichois = 1
c       *** In the isoviscous case only the large vector has to
c       *** be updated!
        call second(tis)
        if (niter.eq.1.or.visc0.gt.0d0) then
          call systm2(1,0,1,matr(1,1),intmat(1,1),kmesh,kprob,
     v                 irhsd(1,1),massmt,isol(1,1),iuser(1,1),
     v                 user(1,1),2,ivcold,ielhlp)
          call copymt(matr(1,1),matrback,kprob)
          call solve(1,matr(1,1),isol(1,1),irhsd(1,1),intmat(1,1),kprob)
        else
          call systm2(2,0,1,matrback,intmat(1,1),kmesh,kprob,
     v                 irhsd(1,1),massmt,isol(1,1),iuser(1,1),
     v                 user(1,1),2,ivcold,ielhlp)
          call solve(1,matr(1,1),isol(1,1),irhsd(1,1),intmat(1,1),kprob)
        endif
        call second(tie)

        call second(tts)
c       ****************** TEMPERATURE ******************************
c       *** Solve the temp equation where (u,v) are given in isol(1,1)
        do 102 i =1,5
           ivcold(i,1) = isol(i,1)
102        ivcold(i,2) = isol(i,2)
        call systm2(1,0,2,matr(1,2),intmat(1,2),kmesh,kprob,irhsd(1,2),
     v               massmt,isol(1,2),iuser(1,2),user(1,2),2,
     v               ivcold,ielhlp)
        call solve(1,matr(1,2),isol(1,2),irhsd(1,2),intmat(1,2),kprob)
        call second(tte)


c       *** relaxation: u(n) = relax*u(n)+(1-relax)*u(n-1)
        if (relax.ne.0.and.relax.ne.1) then
           aa = 1-relax
           ba = relax
           call algebr(3,0,islold(1,1),isol(1,1),isol(1,1),kmesh,
     v                 kprob,aa,ba,0d0,0d0,ipoint)
           call algebr(3,0,islold(1,2),isol(1,2),isol(1,2),kmesh,
     v                 kprob,aa,ba,0d0,0d0,ipoint)
        endif

        if (visc0.gt.0d0) then
c          *** Temperature and depth dependent viscosity are used
c          *** Store the actual value of the temperature in the nodes in
c          *** common /pectmp/tsol(10000)
           call petemp(isol(1,2),kmesh,kprob)
c          *** Update the information in array user
           call pepupd(kmesh,iuser(1,1),user(1,1),100)
c          call printalles(ichois,'iuser',40,iuser,rdum)
c          call printalles(ichois,'user',-100,idum,user)
        endif
       
      dti = tie-tis
      dtt = tte-tts
      write(6,'(''(cpui,cput) = '',2f12.3)') dti,dtt
c     *** Continue iteration?
      call diffvc(0,isol(1,1),islold(1,1),kprob,difmax1)
      call diffvc(0,isol(1,2),islold(1,2),kprob,difmax2)
      rmax1 = anorm(1,3,0,kmesh,kprob,isol(1,1),ihelp,ihelp)
      call copyvc(isol(1,2),idum)
      rmax2 = anorm(1,3,0,kmesh,kprob,idum,ihelp,ihelp)
      difmax1 = difmax1/rmax1
      difmax2 = difmax2/rmax2
      difmax = max(difmax1,difmax2)
      if (niter.gt.1) then
         if (difmax.gt.difold) then
            ndiv = ndiv + 1
            if (ndiv.ge.5) istop = 4
         else
            ndiv = 0
         endif
      endif
      if (niter.ge.maxitr) istop = 1
      if (difmax.lt.eps)   istop = 2

c     *** Check on interrupts
      open(7,file='convec.stop')
      rewind 7
      read (7,*) interrup
      if (interrup.eq.1) istop=3
      close(7)
      
      tlast = tnew
      call second(tnew)
      dcpu = tnew - tlast
      open(9,file='convec.data')
      if (istop.eq.0) then
c        **** continue iteration
         write(6,2200) niter,difmax
         write(9,*) niter,difmax,dcpu
         close(9)
         goto 100
      endif 
      if (istop.eq.1) then
c        **** Maximum number of iterations exceeded.
         write(6,2000) niter,difmax
         write(9,2000) niter,difmax
      endif
      if (istop.eq.2) then
c        **** Convergence
         write(6,2100) niter,difmax
         write(9,2200) niter,difmax
      endif
      if (istop.eq.3) then
c        *** Interrupt
         open(7,file='convec.stop')
         rewind 7
         write(7,*) 1,niter,tnew-t0
         write(9,*) niter,difmax,dcpu
         write(6,2200) niter,difmax
      endif
     
      if (istop.eq.4) then
         write(6,2300) niter,difmax
         write(9,2300) niter,difmax
      endif
      write(6,2500) niter,tnew-t0

      if (interrup.ne.1) then
         write(9,2550) tnew-t0,(tnew-t0)/niter
      endif
2550  format('Total time used ',f10.3,' Av. time per iteration ',f10.3)
      close(9)
c     *** Write solution to backingstorage
      if (restart.eq.0) then
         call openf2(.true.)
      endif
      call writbs(0,1,numarr,'velocity',isol(1,1),ihelp)
      call writbs(0,2,numarr,'temperature',isol(1,2),ihelp)

c     *** Plot velocity and temperature
      ifrinit=0
      jkader=-4
      call plwin(1,0d0,0d0)
      fname='CPLOT'
      text = 'Velocity'
      call plotvc(1,2,isol(1,1),isol(1,1),kmesh,kprob,format,yfaccn,
     v             0d0)
      x = format+1d0
      call plwin(3,x,0d0)
      text = 'Temperature'
      call plotc1(number,kmesh,kprob,isol(1,2),contln,ncntln,format,
     v            yfaccn,jsmoot)

      call finish(1)

1000  format(//'PROGRAM CONVEC: runtime        ',a,1x,a)
1100  format(  'Filename for backingstorage      ',a)
1200  format(  'Old                              ',i10)
1300  format(  'Plots:                           ',/,
     v         '   output medium                 ',i10)
1400  format(  '   plotsize (cm)                 ',e10.3,/,
     v         '   text symbol size (cm)         ',e10.3,/,
     v         '   y factor                      ',e10.3,/,
     v         '   jsmoot                        ',i10)
1500  format(  '   nplax                         ',i10,/,
     v         '   npltax                        ',i10,/,
     v         '   ncolchois                     ',i10,/,
     v         '   iprint                        ',i10,/,
     v         '   nplot                         ',i10)
1600  format(  'Iteration:                       ',/,
     v         '   Maximum number of iterations  ',i10,/,
     v         '   Accuracy factor               ',e10.3,/,
     v         '   Relaxation parameter          ',e10.3)
1650  format(  'Viscosity parameters:            ',/,
     v         '   Rayleigh                      ',e10.3,/,
     v         '   Visc0                         ',e10.3,/,
     v         '   b                             ',e10.3,/,
     v         '   c                             ',e10.3,/,
     v         '   wavelength of perturbation    ',e10.3)
1700  format(  '   Restart                       ',i10)
2000  format(  'Iter: maximum number of iterations exceeded',/,
     v         '      Number of iterations = ',i5,/,
     v         '      Difmax               = ',e10.3)
2100  format(  'Iter: convergence after ',i5,' iterations',/,
     v         '      difmax          = ',e10.3)
2200  format(  'Iter: ',i5,'  difmax = ',e10.3)
2300  format(  'Iter: divergence after ',i5,' iterations',/,
     v         '      difmax         = ',e10.3)
2500  format(  'Time consumed in ',i5,' iterations: ',e10.3)
      end

      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      common /pevisc/ visc0,b,c,rayleigh,wavelength
      parameter(pi=3.1415926)
    
      wpi = wavelength*pi
      func = 0.1*sin(wpi*y)*cos(wpi*x) + (1-y)
  
      return
      end
