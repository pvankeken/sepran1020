cdccelflr6
      subroutine elflr6( number, user, coeff, index1, x, xgauss, mdecl,
     .                   ndecl, phi, vecold, work, ichois, index,
     .                   index3, index4, numold, iuser, ugauss )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 2.0    date   03-04-89 (Revised version, inludes more
c                                        possibilities)
c        version 1.0    date   01-02-89 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c     This subroutine fills function values in array coeff for the
c     integration points, where either ind(number) < 3000
c
c     Number corresponds to the number th coefficient in common block celiar
c
c ********************************************************************
c
c
c                INPUT / OUTPUT PARAMETERS
c

cimp      implicit none

      integer number, mdecl, ndecl, ichois, index(ndecl),index1(mdecl),
     .        numold, index3(numold,*), index4(numold,*), iuser(*)
      double precision user(*), coeff(*), x(mdecl,*), xgauss(mdecl,*),
     .                 phi(ndecl,*), vecold(*), work(*), ugauss(mdecl)

c
c     coeff   o      Double precision output array of length m in which the
c                    values of the coefficient in the integration points are
c                    stored
c
c     ichois  i      Choice parameter to be used when 1000<= ind(number) < 2000
c                    Possible values:
c
c                    1:   In each node there are exactly ncomp unknowns
c
c                    2:   The number of unkowns per point may vary
c                         Array index contains the number of nodes per point
c                         accumulated. Hence
c                         number of unkowns in node 1 is index(1)
c                         number of unkowns in node i is index(i)-index(i-1)
c
c     index   i      Index array to be used if 1000<=ind(number)<2000 and
c                    ichois = 2
c
c     index1  i      Array of length inpelm containing the point numbers
c                    of the nodal points in the element
c
c     index3  i      Two-dimensional integer array of length NUMOLD x NINDEX
c                    containing the positions of the "old" solutions in array
c                    VECOLD with respect to the present element
c
c                    For example VECOLD(INDEX3(i,j)) contains the j th
c                    unknown with respect to the i th old solution vector.
c                    The number i refers to the i th vector corresponding to
c                    IVCOLD in the call of SYSTM2 or DERIVA
c
c     index4  i      Two-dimensional integer array of length NUMOLD x INPELM
c                    containing the number of unknowns per point accumulated
c                    in array VECOLD with respect to the present element.
c
c                    For example INDEX4(i,1) contains the number of unknowns
c                    in the first point with respect to the i th vector stored
c                    in VECOLD.
c                    The number of unknowns in the j th point with respect to
c                    i th vector in VECOLD is equal to
c                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
c
c     mdecl   i      Number of integration points
c
c     ndecl   i      Number of nodal points
c
c     number  i      Parameter indicating which coefficient in common block
c                    celiar must be considered
c
c     numold  i      Number of old vectors that are stored in VECOLD
c
c     user    i      Real user array to pass user information from
c                    main program to subroutine. See STANDARD PROBLEMS
c
c     vecold  i      Old solution
c
c     x       i      Double precision array of length ndecl x ndim in which the
c                    co-ordinates of the local nodal points are stored
c                    according to:
c
c                    x  = x (i,1);  y  = x (i,2);  z  = x (i,3)
c                     i              i              i
c
c     xgauss  i      Double precision array of length mdecl x ndim in which the
c                    co-ordinates of the integration points are stored
c                    according to:
c
c                    x  = x (i,1);  y  = x (i,2);  z  = x (i,3)
c                     i              i              i
c
c     work           Work array of length ncomp
c
c     iuser   i      Integer user array to pass user information from
c                    main program to subroutine. See STANDARD PROBLEMS
c
c     ugauss  i      Preceding solution in Gauss points
c
c ********************************************************************
c
c
c                PARAMETERS IN COMMON BLOCKS
c
      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint
c
c                 /cactl/
c    Contains element dependent information for the various element
c    subroutines. cactl is used to transport information from main
c    subroutines to element subroutines
c
c    ielem    i   Element number
c    itype    i   Type number of element
c    ielgrp   i   Element group number
c    inpelm   i   Number of nodes in element
c    icount   i   Number of unknowns in element
c    ifirst   i   Indicator if the element subroutine is called for the
c                 first time in a series (0) or not (1)
c    notmat  i/o  Indicator if the element matrix is zero (1) or not (0)
c    notvc   i/o  Indicator if the element vector is zero (1) or not (0)
c    irelem   i   Relative element number with respect to element group
c                 number ielgrp
c    nusol    i   Number of degrees of freedom in the mesh
c    nelem    i   Number of elements in the mesh
c    npoint   i   Number of nodes in the mesh
c

      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

c                       / celiar /
c         Contains integer information of the various coefficients of the
c         differential equation.
c
c
c     ind       i    In this array of length 50, of each coefficient it
c                    is stored whether the coefficient is 0 (ind(i) = 0),
c                    constant (ind(i)<0) or variable (ind(i)>0) See elusr0
c
c                    If ind(i) should be 2003, then ind(i) is set equal to
c                    3000+ivec, where ivec refers to the vector to be used
c                    in array VECOLD
c
c                    If ind(i) should be 2004, then ind(i) is set equal to
c                    4000 + ipos, where ipos refers to the first relevant
c                    position in array iuser. In that case ich is also used
c
c     ich       i    In this array of length 50 information concerning the
c                    coefficients is stored for the case IND(I)=3000 + IVEC
c                    The value of ich(i) has the following meaning:
c
c                    1:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all nodal points
c
c                    2:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all nodal points
c
c                    3:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    4:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    5:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all nodes
c
c                    6:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all vertices but not in all nodes
c                         Two-dimensional elements only
c
c     mst       i    In this array of length 50 the start position of the
c                    real information of the ith coefficient in array user
c                    is stored. See elusr3
c                    If ist(i) = 3000+IVEC then mst(i) contains the degree of
c                    freedom j that must be used for this coefficient
c
c     ist       i    In this array of length 50 it is indicated if a coefficient
c                    is a copy of preceding coefficient (>0) or not (=0)
c                    If ist(i) > 0 the value of ist(i) indicates the coefficient
c                    number from which the coefficient must be copied.
c


      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, idum,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, idum
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     kstep    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (1)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (2)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn=0) or variable (jdercn=1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12=a13=a23=0;  a11=a22=a33   a11 and w constant
c
c                    2   a12=a13=a23=0;  a11=a22=a33
c
c                    3   a12#0 or a13#0 or a23#0   a11=a22=a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     metupw    i    Type of upwinding method to be chosen. Possible values:
c
c                    0:  No upwinding
c                    1:  Classical upwind scheme
c                    2:  Il'in scheme
c                    3:  Doubly assymptotic scheme
c                    4:  Critical approximation
c
c     nvert     i    Number of vertices in the element
c
c     idum      -    Dummy, not yet used

 
c
c ********************************************************************
c
c               LOCAL PARAMETERS
c

      integer i, j, istart, ifunc

c
c    i,j           Counting variables
c
c    istart        Starting address in array VECOLD
c
c    ifunc         Output parameter of elflr8 to be used if ind(number)>4000
c                  It indicates if the parameter which with the function
c                  is multiplied is a function (>0) or a constant (<0)
c
c
c ********************************************************************
c
c               SUBROUTINES CALLED
c
c     USER SUBROUTINE
c
      double precision funccf, funcc1

c
c     FUNCC1:  User function subroutine to compute the value of a coefficient
c              in a point depending on a preceding solution
c     FUNCCF:  User function subroutine to compute the value of a coefficient
c              in a point
c
c     BLAS subroutines
c
      double precision ddot
c
c     DDOT   (CDOT)     Inner product of two vectors
c
c ********************************************************************
c
      if ( ind(number).ge.3000 ) then

c      *  ind(number) > 3000, special treatment

         call elflr8 ( number, coeff, vecold, index3, index4, numold,
     .                 work, phi, ndecl, iuser, ifunc )
         if ( ind(number).gt.4000 ) then

c         *  ind(number) > 4000, coefficient must be multiplied

            if ( ifunc.lt.0 ) then

c            *  ifunc < 0, multiplication factor is constant

               do  50 i = kstep, m, kstep
                  coeff(i) = coeff(i) * user(-ifunc)
 50            continue

            else

c            *  ifunc > 0, multiplication factor is a function

               if ( jdiag.eq.1 ) then

c               *  jdiag = 1,  Nodal points

                  do 60 i = kstep, m, kstep
                     coeff(i) = coeff(i) * funccf ( ifunc, x(i,1),
     .                          x(i,2), x(i,3) )
 60               continue

               else

c               *  jdiag = 2,  Integration points

                  do  70 i = 1, m
                     coeff(i) = coeff(i) * funccf ( ifunc, xgauss(i,1),
     .                                   xgauss(i,2), xgauss(i,3) )
 70               continue

               end if

            end if

         end if

      else if ( ist(number).gt.0 ) then

c      * ist(i) > 0, copy coefficient from preceding one

         do  90 i = kstep, m, kstep
            coeff(i) = coeff(i+(ist(number)-number)*m)
 90      continue

      else if ( ind(number) .le. 1000 ) then

c      *  ind(number) < 1000,  coefficient is a function of space alone

         if ( jdiag.eq.1 ) then

c         *  jdiag = 1,  Nodal points

            do 100 i = kstep, m, kstep
               coeff(i) = funccf ( ind(number), x(i,1), x(i,2), x(i,3) )
100         continue

         else

c         *  jdiag = 2,  Integration points

            do 110 i = 1, m
               coeff(i) = funccf ( ind(number), xgauss(i,1),
     .                             xgauss(i,2), xgauss(i,3) )
110         continue

         end if

      else if ( ind(number) .le. 2000 ) then

c      *  ind(number) < 2000,  coefficient is a function of space and a
c                              preceding solution

         if ( ichois.eq.1 ) then

c         * ichois = 1,  Constant number of degrees of freedom per point
c                        Store unknowns per point in work

            if ( jdiag.eq.1 ) then

c            *  jdiag = 1,  Nodal points

               do 130 i = kstep, m, kstep
                  do 120 j = 1, ncomp
                     work(j) = ugauss(i+(j-1)*m)
120               continue
                  coeff(i) = funcc1 ( ind(number), x(i,1), x(i,2),
     .                                x(i,3), work )
130            continue

            else

c            *  jdiag = 2,  Integration points


               do 150 i = 1, m
                  do 140 j = 1, ncomp
                     work(j) = ugauss(i+(j-1)*m)
140               continue
                  coeff(i) = funcc1 ( ind(number), xgauss(i,1),
     .                                xgauss(i,2), xgauss(i,3), work )
150            continue

            end if

         else

c         * ichois = 2,  Variable number of degrees of freedom per point
c                        Store unknowns per point in work

            istart = 1
            if ( jdiag.eq.1 ) then

c            *  jdiag = 1,  Nodal points

               do 170 i = kstep, m, kstep
                  do 160 j = istart, index(i)
                     work(j-istart+1) = ugauss(j)
160               continue
                  istart = istart + index(i)+1
                  coeff(i) = funcc1 ( ind(number), x(i,1), x(i,2),
     .                                x(i,3), work )
170            continue

            else

c            *  jdiag = 2,  Integration points


               do 190 i = kstep, m, kstep
                  do 180 j = istart, index(i)
                     work(j-istart+1) = ugauss(j)
180               continue
                  istart = istart + index(i)+1
                  coeff(i) = funcc1 ( ind(number), xgauss(i,1),
     .                                xgauss(i,2), xgauss(i,3), work )
190            continue

            end if

         end if

      else if ( ind(number) .eq. 2001 ) then

c      *  ind(number) = 2001,  coefficient is a function of space alone
c                              it is given in all nodes

         if ( jdiag.eq.1 ) then

c         *  jdiag = 1

            do 210 i = kstep, m, kstep
               coeff(i) = user ( mst(number) + index1(i) )
210         continue
            if ( inpelm.ne.n ) then

c            *  n # inpelm, i.e. 6-point triangle with unkowns in centroid

c ******************************************************************
c *     Quadratic interpolation may lead to negative values
c *     of quantities
c *     if (the viscosity if filled (nr 7) then use a linear
c *     interpolation with values of the three secondary nodal points
c ******************* **********************************************
               if (number.eq.7) then
                  coeff(7) = (coeff(2)+coeff(4)+coeff(6))/3d0
               else
                  coeff(7) = ( -coeff(1)-coeff(3)-coeff(5)
     .                        +4d0 * (coeff(2)+coeff(4)+coeff(6)) )/9d0
               endif
            end if

         else

c        *  jdiag = 2

            do 220 i = 1, n
               work(i) = user ( mst(number) + index1(i) )
220         continue
            if ( inpelm.ne.n ) then

c            *  n # inpelm, i.e. 6-point triangle with unkowns in centroid

               work(7) = ( -work(1)-work(3)-work(5)
     .                      +4d0 * (work(2)+work(4)+work(6)) )/9d0
            end if
            do 230 i = 1, m
               coeff(i) = ddot ( n, work, 1, phi(1,i), 1 )
230         continue

         end if

      else

c      *  ind(number) = 2002,  coefficient is a function of space alone
c                              it is constant per element

         do 240 i = kstep, m, kstep
            coeff(i) = user ( mst(number) + irelem )
240      continue


      end if

      end
cdc*eor
