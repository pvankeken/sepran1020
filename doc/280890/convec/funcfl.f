c ****************************************************************
c *   FUNCFL
c *
c *   User subroutine to fill a part of array USER for each node.
c *   Possibilities:
c *   ichois = 1 : at output user contains the effective viscosity
c *                for each node as a function of temperature and
c *                depth. It is assumed that the temperature is
c *                stored in /pectmp/. This can be achieved by
c *                subroutine petemp.
c *****************************************************************
      subroutine funcfl(coor,npoint,ichois,user)
      implicit double precision(a-h,o-z)
      dimension coor(*),user(*)

      common /pectmp/ tsol(10000)
      common /pevisc/ visc0, b, c, rayleigh, wavelength

      if (ichois.eq.1) then
         open(9,file='funcfl.out')
         rewind(9)
         write(9,*) npoint
         do 10 i=1,npoint
            temp = tsol(i)
            z    = coor(2*i)
            viscos =  visc0 * exp( -b*temp + c*(1-z)) 
            write(9,*) i,viscos,temp,z
            user(i) = viscos
10       continue
         close(9)
      endif

      return
      end
            
      
