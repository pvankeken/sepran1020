c ************************************************************
c *   PETMP1
c *   
c *   Stores the solution vector in common /pectmp/
c *
c *   Pvk 17-4-89
c ************************************************************
      subroutine petmp1(npoint,usol,kprobh,ikprbh,nunkp,user)
      implicit double precision (a-h,o-z)
      
      common /pectmp/ tsol(10000)
      dimension usol(*),kprobh(*),user(*)
     
      if (npoint.gt.10000) then
         write(6,*) 'PERROR(petmp1): Npoint too large in petmp1'
         write(6,*) '                npoint = ',npoint
         stop
      endif
         
      do 100 i=1,npoint
         ips = i
         if (ikprbh.gt.0) then
            ips = kprobh(i)
         endif
         tsol(i) = usol(ips)
100   continue
      return
      end 
