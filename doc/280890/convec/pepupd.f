c *****************************************************************
c *   PEPUPD
c *
c *   Updates the values of coefficients in array (I)USER if these
c *   are given per nodal point by calls of FUNCFL.
c *   Array iuser is scanned from position 6 to 5+imax.
c *
c *   It is assumed that the values of the coefficients 
c *   in the case iuser=2001 are calculated by funcfl and that the
c *   reference number 1,2,3,... are used sequentially.
c *
c *
c *****************************************************************
      subroutine pepupd(kmesh,iuser,user,imax)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),iuser(*),user(*)

      common ibuffr(1)
      common /carray/ iinfor, infor(3,1500)

      iref = 1
      icount = 5
      npoint = kmesh(8)
c     *** Make sure that the information of the coordinates is in
c     *** blank common.
      call ini050(kmesh(23),'pepud')

10    continue
         icount = icount + 1
         if (iuser(icount).eq.2002) then
c           *** Coefficient given per element (skip)
            icount = icount +1
         else if (iuser(icount).eq.2001) then
            istart = iuser(icount+1)
            call funcfl(ibuffr(infor(1,kmesh(23))),npoint,iref,
     v                  user(istart))
            iref = iref+1
            icount = icount + 1
         else if (iuser(icount).eq.2003) then
c           *** Coefficient given in old solution array
            icount = icount + 2
         else if (iuser(icount).eq.2004) then
c           *** Coefficient 
            icount = icount + 2
         endif
      if (icount.lt.imax) goto 10

      return
      end
