#include <pixrect/pixrect_hs.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>

stringit_(IX,IY,NX,NY,string,pix)
int *NX,*NY,*IX,*IY;
char string[];
int pix[];
{
  Pixrect *mem;
  Pixfont *pf;
  struct rasterfile *rh;
  int type = RT_STANDARD;
  colormap_t *colormap = 0;
  int copy_flag = 1;
  int ix,iy,nx,ny;

  ix = *IX;
  iy = *IY;
  nx = *NX;
  ny = *NY;

  pf = pf_default();
  mem = mem_create(nx,ny,8);

  pr_ttext(mem,ix,iy,PIX_SET,pf,string);
  for(iy=0;iy<ny;iy++)
     for(ix=0;ix<nx;ix++)
        pix[iy*nx+ix] =  pr_get(mem,ix,iy);

  pr_close(mem);
  pf_close(pf);
}

#include <stdio.h>
#include <sys/types.h>
#include <sys/file.h>
#include <math.h>
#include <string.h>

static  char name[80];

rasout_(NTOT,PIX,fname)
int *NTOT;
int PIX[];
char fname[];
{
  int n,isc,i;
  unsigned char fj;
  FILE *fp, *fopen();

  
/* Copy Fortran name to C character array to get rid of the spaces */
  for (i=0;i<80;i++){
      if (fname[i] != 32){ 
              name[i]=fname[i];
      }
      else {
         name[i]=0;
         break;
      }
  }
  fp = fopen(name,"w");
  n  = *NTOT;
  for (i=0;i<n;i++){
      fj = (unsigned char) PIX[i];
      putc(fj,fp);
  }
  fclose(fp);
}
