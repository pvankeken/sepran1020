      program imatext
      dimension ipix(18000),t(200)
      character*80 string,fname,command
      character*4 ext
      
      read(5,*) ichois,nx,ny
      ntx = nx
      nty = 40
      open(9,file='times.output')
      rewind(9)
      i=1
100   continue
      read(9,*,end=200) n,t(i)
      i=i+1
      goto 100
200   niter = i-1
      write(6,*) niter
      idy = 20

      do 10 i=1,niter
         fname = 'TEXT'
         write(string,'(i3,'' t = '',f12.3)') i,t(i)
         call stringit(0,10,ntx,nty,string,ipix)
         ntot = ntx*nty
         call rasout(ntot,ipix,fname)
 
         write(command,1000) ntx,nty,nx,ny+idy+nty,ny+idy
         write(6,*) command
         call system(command)
1000     format('mfilt -i',i3.3,'x',i3.3,' -o',i3.3,'x',
     v           i3.3,' -ot0x',i3.3,' TEXT TUS')
         if (ichois.eq.2) then
           write(command,1002) nx,ny,nx,ny+idy+nty,i
1002       format('mfilt -i',i3.3,'x',i3.3,' -o',i3.3,'x',
     v             i3.3,' -u TMAGE.',i3.3,' TUS')
         else
           write(command,1001) nx,ny,nx,ny+idy+nty,i
1001       format('mfilt -i',i3.3,'x',i3.3,' -o',i3.3,'x',
     v             i3.3,' -u IMAGE.',i3.3,' TUS')
         endif
         
         write(6,*) command
         call system(command)
         write(command,1003) i
1003     format('mv TUS TOTMAG.',i3.3)
         call system(command)
10    continue
  
      end
