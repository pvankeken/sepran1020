c *****************************************************************
c *   FILTIM
c *
c *   Extension of subroutine filtim. The mass and stiffness matrix
c *   are calculated at time level t
c *
c *
c *   Pvk 160589
c *****************************************************************
      subroutine filtim(kmesh,kprob,intmat,isol,matrs,matrm,irhsd,
     v                  islold,user,iuser)
      implicit double precision (a-h,o-z)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ alpha,iupwind
      common /pefil/ idia

      dimension kmesh(*),kprob(*),intmat(*),isol(*),matrs(*)
      dimension matrm(*),irhsd(*),islold(*),user(*),iuser(*)
      dimension work(20),iwork(20)
      save ifirst
      if (ifirst.eq.0) then
            ifirst=1
	    if (idia.eq.1) goto 1000

            call chtype(1,kprob)
            iuser(6)=7
            iuser(7)=-6
            iuser(9)=-6
            iuser(10)=1
            iuser(11)=2
            iuser(14)=iupwind
             user(6)=alpha
            call systm0(11,matrs,intmat,kmesh,kprob,irhsd,
     v                  isol,iuser,user,islold,ihelp)
            call chtype(2,kprob)
            if (iupwind.eq.0) then
               iuser(6)=7
               iuser(7)=-6
               user(6)=1d0
            else
               iuser(14) =-7
                user(7)  =1d0
               iuser(15) = iupwind
            endif
            call systm0(14,matrm,intmat,kmesh,kprob,mrhsd,
     v                  isol,iuser,user,islold,ihelp)
	    return
1000        continue
            iuser(6)=7
            iuser(7)=-6
            iuser(9)=-6
            iuser(10)=1
            iuser(11)=2
            iuser(14)=-7
	     user(7)=1d0
             user(6)=alpha
            call systm1(11,1,matrs,intmat,kmesh,kprob,irhsd,matrm,
     v                  isol,iuser,user,islold,ihelp)
      endif

      return
      end
