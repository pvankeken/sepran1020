c *************************************************************
c *   ELEM
c *
c *   This version calculates element matrices for the
c *   convection-diffusion equation for the bilinear rectangle,
c *   with sides 1-2 and 3-4 in the x-direction (length a) and
c *   sides 2-3 and 4-1 in the y-direction (length b).
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *   iuser(31)           alpha        diffusivity
c *
c *    user(31)           u1           x-velocity in point 1
c *      ....             ....
c *    user(31+npoint)    uN           x-velocity in point N
c *
c *    user(32+npoint)    v1           y-velocity in point 1
c *      .....            ....
c *    user(31+2*npoint)  vN           y-velocity in point N
c *
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *        3          Galerkin weighting; exact integration
c *
c *        5          Upwind weighting according to scheme
c *                   of Huyakorn etc.; exact integration
c *       
c *        7          Pure convection; upwind weighting; 
c *                   exact integration
c *
c *        8          Mass matrix; exact integration
c * 
c *        9          Upwind weighted mass matrix; exact integration
c *
c *   PvK 150190-300190
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),u(4),v(4)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint

      if (ifirst.eq.1.and.(itype.eq.2.or.itype.eq.4)) return

      if (vector) then
         elemvc(1) = 0.
         elemvc(2) = 0.
         elemvc(3) = 0.
         elemvc(4) = 0.
      endif
     
c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
         iu = 31+index1(i)
         iv = iu+npoint
         u(i) = user(iu)
         v(i) = user(iv)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))
      alpha = user(31)

      if (matrix) then

c        *** Mass matrix
         if (itype.eq.8) then
            call blrmas(elemmt,a,b)
            return
         endif
         if (itype.eq.9) then
            call upwab(u,v,alf,bet,a,b,alpha)            
            if (ifirst.eq.0) then
               print *
               print *,'upwind factors'
               write(6,'(2f12.3)') alf,bet
            endif
            call upwmas(elemmt,alf,bet,a,b)
            if (ifirst.eq.0) then
              print *,'Mass'
              call printmat(elemmt)
            endif
            return
         endif

c        *** Stiffness matrix: 
c        *** Diffusive terms
         if (itype.le.5) then
            call blrdif(elemmt,a,b,alpha)
            if (ifirst.eq.0) then
               print *,'DifI'
               call printmat(elemmt)
            endif
         else
            do 5 i=1,16
5              elemmt(i) = 0d0
         endif
c        *** Add convective terms
         call blrconv(elemmt,a,b,u,v)
         if (ifirst.eq.0) then
           print *,'ConvI'
           call printmat(elemmt)
         endif
         if (itype.lt.5) return

c        *** Bilinear triangle: 
c        *** Calculate upwind factors alf and bet
         call upwab(u,v,alf,bet,a,b,alpha)            
         if (ifirst.eq.0) then
            print *
            print *,'upwind factors'
            write(6,'(2f12.3)') alf,bet
         endif
c        *** Diffusive terms
         if (itype.eq.5) then
c           call upwdif(elemmt,a,b,alf,bet,alpha)
c           if (ifirst.eq.0) then
c               print *,'DifII'
c               call printmat(elemmt)
c           endif
         endif
c        *** Convective terms 
         call upwc2(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvII'
             call printmat(elemmt)
         endif
         call upwc3(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvIII'
             call printmat(elemmt)
         endif
         call upwc4(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvIV'
             call printmat(elemmt)
         endif
      endif
            

      return
      end

      subroutine printmat(elemmt)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)
    
      write(6,*)
      write(6,10) 24*elemmt(1),24*elemmt(2),24*elemmt(3),
     v                24*elemmt(4)
      write(6,10) 24*elemmt(5),24*elemmt(6),24*elemmt(7),
     v                24*elemmt(8)
      write(6,10) 24*elemmt(9),24*elemmt(10),24*elemmt(11),
     v                24*elemmt(12)
      write(6,10) 24*elemmt(13),24*elemmt(14),24*elemmt(15),
     v                24*elemmt(16)
10    format(4f12.3)

      return
      end
 
 
 
 
