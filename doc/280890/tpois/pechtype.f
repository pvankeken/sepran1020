c *****************************************************************
c *   PECHTYPE
c *
c *   Adapts chtype to more problems.
c *****************************************************************
      subroutine pechtype(iprob,iserie,kprob)
      
      nprob  = max ( 1, kprob(40) )
      if ( iprob .lt. 1 .or. iprob .gt. nprob )  call ertrap( 'chtype',
     .     699, 2, iprob, 0, 0)
      ipkprb = 1
      do 100 k = 2, iprob
100      ipkprb = ipkprb + kprob ( 2+ipkprb )
      call chtype(iserie,kprob(ipkprb))
 
      return
      end
