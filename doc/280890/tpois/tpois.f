c **********************************************************
c *   HEAT
c *
c *   Solves the time dependent heat equation.
c *   PvK 160589
c ******************************************************************
      program heat
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),iuser(100),user(5000)
      dimension intmat(5),isol(5),islold(5),istold(5),irhsd(5)
      dimension matrs(5),matrm(5),u1(2),iu1(2),contln(15),ivec1(5)
      dimension ivec2(5)
      integer restart

      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ alpha,iupwind
      common /usertext/text,text1,text2
      common /pefil/idia
      character*80 text,text1,text2

      kmesh(1)=100
      kprob(1)=100
      iuser(1)=100
       user(1)=5000
      contln(1)=15

      read(5,*) tend,tstep,noutstep,imet
      read(5,*) iupwind,alpha,idia

      call start(0,0,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(2,kmesh,kprob,intmat)
      call presdf(kmesh,kprob,isol)

c     *** Create initial temperature field
      iu1(1)=1
      call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
      iu1(1)=11
      call creavc(0,1,idum,ivec1,kmesh,kprob,iu1,u1,iu2,u2)
      iu1(1)=12
      call creavc(0,1,idum,ivec2,kmesh,kprob,iu1,u1,iu2,u2)
      npoint = kmesh(8)
      user(31) = alpha
      iuser(31) = npoint
      call copyuv(0,ivec1,user,kprob,32)
      call copyuv(0,ivec2,user,kprob,32+npoint)
      ncntln=10
      do 9 i=1,10
9        contln(i+5)=i*1d-1
      ifrinit=0
      form1 = 8d0
      form2 = 10d0
      call plotvc(-1,-1,ivec1,ivec2,kmesh,kprob,form1,yfaccn,factor)
      call plwin(1,0d0,0d0)
      text = 'Initial condition'
      call plotc1(number,kmesh,kprob,isol,contln,ncntln,form1,
     v            yfaccn,jsmoot)
      call plwin(3,form1+2d0,0d0)
      call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)

      t=0d0
      dt = tend/noutstep
      nloop = noutstep
      do 10 i=1,noutstep
         iloop = i
         tout = dt*i
         angl = -90d0 + 360d0*tout/(6.283185)
         if (imet.lt.10) then
           call soltim(1,imet,kmesh,kprob,intmat,isol,epsusr,user,iuser)
         else
c          call ieuler(kmesh,kprob,intmat,isol,islold,user,iuser,
c    v                 numold,ivcold,matrs,matrm,irhsd)
c          call corrcn(kmesh,kprob,intmat,isol,islold,user,iuser,
c    v                 numold,ivcold,matrs,matrm,irhsd)
         endif
         call algebr(6,1,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
c        *** Plot temperature
         if (i.eq.noutstep) then
           write(text,99) 't = ',t,' met = ',imet,
     v                    ' dt = ',tstep
99         format(a,f10.5,a,i3,a,f10.7)
           call plwin(1,x,y)
           call plotc1(number,kmesh,kprob,isol,contln,ncntln,form1,
     v                 yfaccn,jsmoot)
           call plwin(3,form1+2d0,0d0)
           write(text,'(a)') ' '
           call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)
         endif
         call algebr(6,1,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
         write(6,98) t,a,b
98       format(f12.7,f10.3,f10.3)
10    continue

      call finish(0)

      end
