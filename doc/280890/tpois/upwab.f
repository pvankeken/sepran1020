c *************************************************************
c *   UPWAB
c *
c *   Calculate the upwind factors alpha and beta defined by (i=1,4)
c *
c *   alpha  = coth ( gamma  /2) - 2/gamma      
c *        i               i              i
c *
c *   beta   = coth ( delta  /2) - 2/delta
c *       i                i              i
c *
c *   where gamma  = (u a)/kappa  and delta  = (v a)/kappa
c *              i     i                   i     i
c *
c *
c *   PvK 220190
c *************************************************************
      subroutine upwab(u,v,alf,bet,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension u(4),v(4)

      ug = abs(0.25*(u(1)+u(2)+u(3)+u(4)))
      vg = abs(0.25*(v(1)+v(2)+v(3)+v(4)))
      alf = -1d0
      bet = -1d0
      if (ug.lt.1d-7) alf = 0.
      if (vg.lt.1d-7) bet = 0.

      return
      end

