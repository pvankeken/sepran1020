c **********************************************************
c *   HEAT
c *
c *   Solves the time dependent heat equation.
c *   PvK 160589
c ******************************************************************
      program heat
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),iuser(100),user(5000)
      dimension intmat(5),isol(5),islold(5),istold(5),irhsd(5)
      dimension matrs(5),matrm(5),u1(2),iu1(2),contln(15),ivec1(5)
      dimension ivec2(5)
      integer restart

      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ alpha,iupwind
      common /usertext/text,text1,text2
      common /pefil/idia
      character*80 text,text1,text2

      kmesh(1)=100
      kprob(1)=100
      iuser(1)=100
       user(1)=5000
      contln(1)=15

      read(5,*) tend,tstep,noutstep,imet
      read(5,*) iupwind,alpha,idia

      call start(0,0,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(2,kmesh,kprob,intmat)

c     *** Create initial temperature field
      iu1(1)=1
      call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2)
      iu1(1)=11
      call creavc(0,1,idum,ivec1,kmesh,kprob,iu1,u1,iu2,u2)
      iu1(1)=12
      call creavc(0,1,idum,ivec2,kmesh,kprob,iu1,u1,iu2,u2)
      npoint = kmesh(8)
      user(31) = alpha
      iuser(31) = npoint
      call copyuv(0,ivec1,user,kprob,32)
      call copyuv(0,ivec2,user,kprob,32+npoint)
      ncntln=10
      do 9 i=1,10
9        contln(i+5)=i*1d-1
      ifrinit=0
      form1 = 8d0
      form2 = 10d0
      call plotvc(-1,-1,ivec1,ivec2,kmesh,kprob,form1,yfaccn,factor)
      call plwin(1,0d0,0d0)
      text = 'Initial condition'
      call plotc1(number,kmesh,kprob,isol,contln,ncntln,form1,
     v            yfaccn,jsmoot)
      call plwin(3,form1+2d0,0d0)
      call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)

      t=0d0
      dt = tend/noutstep
      nloop = noutstep
      do 10 i=1,noutstep
         iloop = i
         tout = dt*i
         angl = -90d0 + 360d0*tout/(6.283185)
         if (imet.lt.10) then
           call soltim(1,imet,kmesh,kprob,intmat,isol,epsusr,user,iuser)
         else
           call ieuler(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                 numold,ivcold,matrs,matrm,irhsd)
           call corrcn(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                 numold,ivcold,matrs,matrm,irhsd)
         endif
         call algebr(6,1,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
c        *** Plot temperature
         if (i.eq.noutstep) then
           write(text,99) 't = ',t,' met = ',imet,
     v                    ' dt = ',tstep
99         format(a,f10.5,a,i3,a,f10.7)
           call plwin(1,x,y)
           call plotc1(number,kmesh,kprob,isol,contln,ncntln,form1,
     v                 yfaccn,jsmoot)
           call plwin(3,form1+2d0,0d0)
           write(text,'(a)') ' '
           call plot3d(1,kmesh,kprob,isol,form1,form2,45d0,13)
         endif
         call algebr(6,1,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
         write(6,98) t,a,b
98       format(f12.7,f10.3,f10.3)
10    continue

      call finish(0)

      end
      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      
      if (ichois.gt.2) then
         func = funccf(ichois-10,x,y,z)
      else
	yc = -0.5
	xc = 0
	yr = 0.1
	xr = -0.4
        r = sqrt( x*x + (y-yc)*(y-yc) )
        if (r.lt.0.4) then
	   s1 = sin(pi*(x+xr)/0.8)
	   s2 = sin(pi*(-y-yr)/0.8)
	   func = s1*s1*s2*s2
        else 
           func = 0d0
        endif
      endif

      return
      end
      function funccf(ichois,x,y,z)
      parameter(pi=3.1415926)
      implicit double precision(a-h,o-z)
      if (ichois.eq.1) then
         funccf = -pi*y
      else
         funccf = pi*x
      endif
      return
      end
c *************************************************************
c *   ELEM
c *
c *   This version calculates element matrices for the
c *   convection-diffusion equation for the bilinear rectangle,
c *   with sides 1-2 and 3-4 in the x-direction (length a) and
c *   sides 2-3 and 4-1 in the y-direction (length b).
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *   iuser(31)           alpha        diffusivity
c *
c *    user(31)           u1           x-velocity in point 1
c *      ....             ....
c *    user(31+npoint)    uN           x-velocity in point N
c *
c *    user(32+npoint)    v1           y-velocity in point 1
c *      .....            ....
c *    user(31+2*npoint)  vN           y-velocity in point N
c *
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *        3          Galerkin weighting; exact integration
c *
c *        5          Upwind weighting according to scheme
c *                   of Huyakorn etc.; exact integration
c *       
c *        7          Pure convection; upwind weighting; 
c *                   exact integration
c *
c *        8          Mass matrix; exact integration
c * 
c *        9          Upwind weighted mass matrix; exact integration
c *
c *   PvK 150190-300190
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),u(4),v(4)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint

      if (ifirst.eq.1.and.(itype.eq.2.or.itype.eq.4)) return

      if (vector) then
         elemvc(1) = 0.
         elemvc(2) = 0.
         elemvc(3) = 0.
         elemvc(4) = 0.
      endif
     
c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
         iu = 31+index1(i)
         iv = iu+npoint
         u(i) = user(iu)
         v(i) = user(iv)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))
      alpha = user(31)

      if (matrix) then

c        *** Mass matrix
         if (itype.eq.8) then
            call blrmas(elemmt,a,b)
            return
         endif
         if (itype.eq.9) then
            call upwab(u,v,alf,bet,a,b,alpha)            
            if (ifirst.eq.0) then
               print *
               print *,'upwind factors'
               write(6,'(2f12.3)') alf,bet
            endif
            call upwmas(elemmt,alf,bet,a,b)
            if (ifirst.eq.0) then
              print *,'Mass'
              call printmat(elemmt)
            endif
            return
         endif

c        *** Stiffness matrix: 
c        *** Diffusive terms
         if (itype.le.5) then
            call blrdif(elemmt,a,b,alpha)
            if (ifirst.eq.0) then
               print *,'DifI'
               call printmat(elemmt)
            endif
         else
            do 5 i=1,16
5              elemmt(i) = 0d0
         endif
c        *** Add convective terms
         call blrconv(elemmt,a,b,u,v)
         if (ifirst.eq.0) then
           print *,'ConvI'
           call printmat(elemmt)
         endif
         if (itype.lt.5) return

c        *** Bilinear triangle: 
c        *** Calculate upwind factors alf and bet
         call upwab(u,v,alf,bet,a,b,alpha)            
         if (ifirst.eq.0) then
            print *
            print *,'upwind factors'
            write(6,'(2f12.3)') alf,bet
         endif
c        *** Diffusive terms
         if (itype.eq.5) then
c           call upwdif(elemmt,a,b,alf,bet,alpha)
c           if (ifirst.eq.0) then
c               print *,'DifII'
c               call printmat(elemmt)
c           endif
         endif
c        *** Convective terms 
         call upwc2(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvII'
             call printmat(elemmt)
         endif
         call upwc3(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvIII'
             call printmat(elemmt)
         endif
         call upwc4(elemmt,a,b,alf,bet,u,v)
         if (ifirst.eq.0) then
             print *,'ConvIV'
             call printmat(elemmt)
         endif
      endif
            

      return
      end

      subroutine printmat(elemmt)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)
    
      write(6,*)
      write(6,10) 24*elemmt(1),24*elemmt(2),24*elemmt(3),
     v                24*elemmt(4)
      write(6,10) 24*elemmt(5),24*elemmt(6),24*elemmt(7),
     v                24*elemmt(8)
      write(6,10) 24*elemmt(9),24*elemmt(10),24*elemmt(11),
     v                24*elemmt(12)
      write(6,10) 24*elemmt(13),24*elemmt(14),24*elemmt(15),
     v                24*elemmt(16)
10    format(4f12.3)

      return
      end
 
 
 
 
c *************************************************************
c *   BLRCONV
c *
c *   Compute element integrals for the convective terms.
c *   These are of the form 
c *
c *        s(i,j)= // N(i) u dN(j)/dx dxdy + // N(i) v dN(j)/dy dxdy
c *
c *   The integrals are calculated by expressing (u,v) as the sum of
c *   basisfunctions 
c *
c *        u = sum(k)   u(k)N(k)
c *
c *   so 
c *
c *        s(i,j)= sum(k)  u(k) // N(i)N(k) dN(j)/dx  dxdy +
c *
c *                sum(k)  v(k) // N(i)N(k) dN(j)/dy  dxdy
c *
c *   The integrals are evaluated exactly, yielding a weighted sum
c *   over the velocity components.
c *
c *
c *   PvK 110190
c *************************************************************
      subroutine blrconv(elemmt,a,b,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      a72 = a/72
      b72 = b/72
      u1  = u(1)*b72
      u2  = u(2)*b72
      u3  = u(3)*b72
      u4  = u(4)*b72
      v1  = v(1)*a72
      v2  = v(2)*a72
      v3  = v(3)*a72
      v4  = v(4)*a72
      u12 = 2*u1
      u13 = 3*u1
      u16 = 6*u1
      u22 = 2*u2
      u23 = 3*u2
      u26 = 6*u2
      u32 = 2*u3
      u33 = 3*u3
      u36 = 6*u3
      u42 = 2*u4
      u43 = 3*u4
      u46 = 6*u4
      v12 = 2*v1
      v13 = 3*v1
      v16 = 6*v1
      v22 = 2*v2
      v23 = 3*v2
      v26 = 6*v2
      v32 = 2*v3
      v33 = 3*v3
      v36 = 6*v3
      v42 = 2*v4
      v43 = 3*v4
      v46 = 6*v4

      elemmt(1) = elemmt(1) -u16-u23-u3 -u42 -v16-v22-v3 -v43
      elemmt(2) = elemmt(2) +u16+u23+u3 +u42 -v12-v22-v3 -v4
      elemmt(3) = elemmt(3) +u12+u2 +u3 +u42 +v12+v22+v3 +v4
      elemmt(4) = elemmt(4) -u12-u2 -u3 -u42 +v16+v22+v3 +v43

      elemmt(5) = elemmt(5) -u13-u26-u32-u4  -v12-v22-v3 -v4 
      elemmt(6) = elemmt(6) +u13+u26+u32+u4  -v12-v26-v33-v4
      elemmt(7) = elemmt(7) +u1 +u22+u32+u4  +v12+v26+v33+v4
      elemmt(8) = elemmt(8) -u1 -u22-u32-u4  +v12+v22+v3 +v4 
 
      elemmt(9) = elemmt(9) -u1 -u22-u32-u4  -v1 -v2 -v32-v42
      elemmt(10)= elemmt(10)+u1 +u22+u32+u4  -v1 -v23-v36-v42
      elemmt(11)= elemmt(11)+u1 +u22+u36+u43 +v1 +v23+v36+v42
      elemmt(12)= elemmt(12)-u1 -u22-u36-u43 +v1 +v2 +v32+v42
  
      elemmt(13)= elemmt(13)-u12-u2 -u3 -u42 -v13-v2 -v32-v46
      elemmt(14)= elemmt(14)+u12+u2 +u3 +u42 -v1 -v2 -v32-v42
      elemmt(15)= elemmt(15)+u12+u2 +u33+u46 +v1 +v2 +v32+v42
      elemmt(16)= elemmt(16)-u12-u2 -u33-u46 +v13+v2 +v32+v46

      return
      end
      subroutine blrdif(elemmt,a,b,alpha) 
      implicit double precision(a-h,o-z)
      parameter(drie=0.33333333333333333d0,zes=0.16666666666666667d0)
      dimension elemmt(*)

      ab=a/b*alpha
      ba=b/a*alpha
      d1 = drie*ab + drie*ba
      d2 = zes*ab  - drie*ba
      d3 = -zes*ab - zes*ba
      d4 = -drie*ab+ zes*ba
      elemmt(1) = d1
      elemmt(2) = d2
      elemmt(3) = d3  
      elemmt(4) = d4
      elemmt(5) = d2
      elemmt(6) = d1
      elemmt(7) = d4
      elemmt(8) = d3
      elemmt(9) = d3
      elemmt(10)= d4
      elemmt(11)= d1
      elemmt(12)= d2
      elemmt(13)= d4
      elemmt(14)= d3
      elemmt(15)= d2
      elemmt(16)= d1
     
      return
      end
c *************************************************************
c *   BLRMAS
c *
c *   Built Galerking weighted mass matrix for the bilinear triangles
c *
c *              1
c *   M     =   //  N N  dxdy   =
c *    ij       0    i j 
c *
c *   PvK 300190
c *************************************************************
      subroutine blrmas(elemmt,a,b)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)

      a1 = a*b/36
      a2 = a*b/18
      a4 = a*b/9

      elemmt(1) = a4 
      elemmt(2) = a2 
      elemmt(3) = a1 
      elemmt(4) = a2 
      elemmt(5) = a2 
      elemmt(6) = a4 
      elemmt(7) = a2 
      elemmt(8) = a1 
      elemmt(9) = a1 
      elemmt(10)= a2 
      elemmt(11)= a4 
      elemmt(12)= a2 
      elemmt(13)= a2 
      elemmt(14)= a1 
      elemmt(15)= a2 
      elemmt(16)= a4 

      return
      end
c *************************************************************
c *   UPWC2
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part II
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc2(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = alf*b*(u(1)+u(2))/48
      u2 = alf*b*(u(3)+u(4))/48
      v1 = alf*a*v(1)/120
      v2 = alf*a*v(2)/120
      v3 = alf*a*v(3)/120
      v4 = alf*a*v(4)/120

      elemmt(1)  = elemmt(1) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4 
      elemmt(2)  = elemmt(2) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(3)  = elemmt(3) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(4)  = elemmt(4) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4
 
      elemmt(5)  = elemmt(5) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4
      elemmt(6)  = elemmt(6) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(7)  = elemmt(7) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(8)  = elemmt(8) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4

      elemmt(9)  = elemmt(9) -  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(10) = elemmt(10)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(11) = elemmt(11)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4 
      elemmt(12) = elemmt(12)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4
 
      elemmt(13) = elemmt(13)-  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(14) = elemmt(14)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(15) = elemmt(15)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4
      elemmt(16) = elemmt(16)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4

      return
      end
c *************************************************************
c *   UPWC3
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part III
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc3(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = bet*b*u(1)/120
      u2 = bet*b*u(2)/120
      u3 = bet*b*u(3)/120
      u4 = bet*b*u(4)/120
      v1 = bet*a*(v(1)+v(4))/48
      v2 = bet*a*(v(2)+v(3))/48

      elemmt(1)  = elemmt(1) -6*u1-3*u2-2*u3-4*u4 -3*v1-  v2  
      elemmt(2)  = elemmt(2) +6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(3)  = elemmt(3) +4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(4)  = elemmt(4) -4*u1-2*u2-3*u3-6*u4 +3*v1+  v2

      elemmt(5)  = elemmt(5) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(6)  = elemmt(6) +3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(9)  = elemmt(9) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(10) = elemmt(10)+3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(13) = elemmt(13)-6*u1-3*u2-2*u3-4*u4 -3*v1-  v2
      elemmt(14) = elemmt(14)+6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(15) = elemmt(15)+4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(16) = elemmt(16)-4*u1-2*u2-3*u3-6*u4 +3*v1+  v2


      return
      end
c *************************************************************
c *   UPWC4
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part IV
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc4(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = -alf*bet*b*(u(1)+u(2))/80
      u2 = -alf*bet*b*(u(3)+u(4))/80
      v1 = -alf*bet*a*(v(1)+v(4))/80
      v2 = -alf*bet*a*(v(2)+v(3))/80


      elemmt(1)  = elemmt(1) -3*u1-2*u2-3*v1-2*v2 
      elemmt(2)  = elemmt(2) +3*u1+2*u2-2*v1-3*v2
      elemmt(3)  = elemmt(3) +2*u1+3*u2+2*v1+3*v2
      elemmt(4)  = elemmt(4) -2*u1-3*u2+3*v1+2*v2

      elemmt(5)  = elemmt(5) -3*u1-2*u2-3*v1-2*v2 
      elemmt(6)  = elemmt(6) +3*u1+2*u2-2*v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+3*u2+2*v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-3*u2+3*v1+2*v2

      elemmt(9)  = elemmt(9) -3*u1-2*u2-3*v1-2*v2 
      elemmt(10) = elemmt(10)+3*u1+2*u2-2*v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+3*u2+2*v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-3*u2+3*v1+2*v2

      elemmt(13) = elemmt(13)-3*u1-2*u2-3*v1-2*v2 
      elemmt(14) = elemmt(14)+3*u1+2*u2-2*v1-3*v2
      elemmt(15) = elemmt(15)+2*u1+3*u2+2*v1+3*v2
      elemmt(16) = elemmt(16)-2*u1-3*u2+3*v1+2*v2

      return
      end
c *************************************************************
c *   UPWAB
c *
c *   Calculate the upwind factors alpha and beta defined by (i=1,4)
c *
c *   alpha  = coth ( gamma  /2) - 2/gamma      
c *        i               i              i
c *
c *   beta   = coth ( delta  /2) - 2/delta
c *       i                i              i
c *
c *   where gamma  = (u a)/kappa  and delta  = (v a)/kappa
c *              i     i                   i     i
c *
c *
c *   PvK 220190
c *************************************************************
      subroutine upwab(u,v,alf,bet,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension u(4),v(4)

      ug = abs(0.25*(u(1)+u(2)+u(3)+u(4)))
      vg = abs(0.25*(v(1)+v(2)+v(3)+v(4)))
      alf = -1d0
      bet = -1d0
      if (ug.lt.1d-7) alf = 0.
      if (vg.lt.1d-7) bet = 0.

      return
      end

c *****************************************************************
c *   FILTIM
c *
c *   Extension of subroutine filtim. The mass and stiffness matrix
c *   are calculated at time level t
c *
c *
c *   Pvk 160589
c *****************************************************************
      subroutine filtim(kmesh,kprob,intmat,isol,matrs,matrm,irhsd,
     v                  islold,user,iuser)
      implicit double precision (a-h,o-z)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ alpha,iupwind
      common /pefil/ idia

      dimension kmesh(*),kprob(*),intmat(*),isol(*),matrs(*)
      dimension matrm(*),irhsd(*),islold(*),user(*),iuser(*)
      dimension work(20),iwork(20)
      save ifirst
      if (ifirst.eq.0) then
            ifirst=1
	    if (idia.eq.1) goto 1000

            call chtype(1,kprob)
            iuser(6)=7
            iuser(7)=-6
            iuser(9)=-6
            iuser(10)=1
            iuser(11)=2
            iuser(14)=iupwind
             user(6)=alpha
            call systm0(11,matrs,intmat,kmesh,kprob,irhsd,
     v                  isol,iuser,user,islold,ihelp)
            call chtype(2,kprob)
            if (iupwind.eq.0) then
               iuser(6)=7
               iuser(7)=-6
               user(6)=1d0
            else
               iuser(14) =-7
                user(7)  =1d0
               iuser(15) = iupwind
            endif
            call systm0(14,matrm,intmat,kmesh,kprob,mrhsd,
     v                  isol,iuser,user,islold,ihelp)
	    return
1000        continue
            iuser(6)=7
            iuser(7)=-6
            iuser(9)=-6
            iuser(10)=1
            iuser(11)=2
            iuser(14)=-7
	     user(7)=1d0
             user(6)=alpha
            call systm1(11,1,matrs,intmat,kmesh,kprob,irhsd,matrm,
     v                  isol,iuser,user,islold,ihelp)
      endif

      return
      end
