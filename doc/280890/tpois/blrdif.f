      subroutine blrdif(elemmt,a,b,alpha) 
      implicit double precision(a-h,o-z)
      parameter(drie=0.33333333333333333d0,zes=0.16666666666666667d0)
      dimension elemmt(*)

      ab=a/b*alpha
      ba=b/a*alpha
      d1 = drie*ab + drie*ba
      d2 = zes*ab  - drie*ba
      d3 = -zes*ab - zes*ba
      d4 = -drie*ab+ zes*ba
      elemmt(1) = d1
      elemmt(2) = d2
      elemmt(3) = d3  
      elemmt(4) = d4
      elemmt(5) = d2
      elemmt(6) = d1
      elemmt(7) = d4
      elemmt(8) = d3
      elemmt(9) = d3
      elemmt(10)= d4
      elemmt(11)= d1
      elemmt(12)= d2
      elemmt(13)= d4
      elemmt(14)= d3
      elemmt(15)= d2
      elemmt(16)= d1
     
      return
      end
