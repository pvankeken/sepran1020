c *************************************************************
c *   UPWC3
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part III
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc3(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = bet*b*u(1)/120
      u2 = bet*b*u(2)/120
      u3 = bet*b*u(3)/120
      u4 = bet*b*u(4)/120
      v1 = bet*a*(v(1)+v(4))/48
      v2 = bet*a*(v(2)+v(3))/48

      elemmt(1)  = elemmt(1) -6*u1-3*u2-2*u3-4*u4 -3*v1-  v2  
      elemmt(2)  = elemmt(2) +6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(3)  = elemmt(3) +4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(4)  = elemmt(4) -4*u1-2*u2-3*u3-6*u4 +3*v1+  v2

      elemmt(5)  = elemmt(5) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(6)  = elemmt(6) +3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(9)  = elemmt(9) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(10) = elemmt(10)+3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(13) = elemmt(13)-6*u1-3*u2-2*u3-4*u4 -3*v1-  v2
      elemmt(14) = elemmt(14)+6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(15) = elemmt(15)+4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(16) = elemmt(16)-4*u1-2*u2-3*u3-6*u4 +3*v1+  v2


      return
      end
