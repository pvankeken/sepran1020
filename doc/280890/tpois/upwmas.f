c *************************************************************
c *   UPWMAS
c *
c *   Built upwind weighted mass matrix for the bilinear triangles
c *
c *              1
c *   M     =   //  W N  dxdy   =
c *    ij       0    i j 
c *
c *         =   //  N N  dxdy   +
c *                  i j
c *
c *                        x y  
c *            alpha  //  F N  N dxdy +
c *                 i        i  j
c *   
c *
c *                        y x
c *            beta   //  F N  N dxdy +
c *                i         i  j
c *
c *                            x y 
c *            alpha beta  // F F N dxdy
c *                 i    i         j
c *
c *   PvK 300190
c *************************************************************
      subroutine upwmas(elemmt,alf,bet,a,b)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)

      a1 = a*b/36
      a2 = a*b/18
      a4 = a*b/9
      b1 = -a*b/24*alf
      c1 = -a*b/24*bet
      d1 = a*b/16*alf*bet

      elemmt(1) = a4 + 2*b1 + 2*c1 + d1
      elemmt(2) = a2 + 2*b1 +   c1 + d1
      elemmt(3) = a1 +   b1 +   c1 + d1
      elemmt(4) = a2 +   b1 + 2*c1 + d1
      elemmt(5) = a2 + 2*b1 +   c1 + d1
      elemmt(6) = a4 + 2*b1 + 2*c1 + d1
      elemmt(7) = a2 +   b1 + 2*c1 + d1
      elemmt(8) = a1 +   b1 +   c1 + d1
      elemmt(9) = a1 +   b1 +   c1 + d1
      elemmt(10)= a2 +   b1 + 2*c1 + d1
      elemmt(11)= a4 + 2*b1 + 2*c1 + d1
      elemmt(12)= a2 + 2*b1 +   c1 + d1
      elemmt(13)= a2 +   b1 + 2*c1 + d1
      elemmt(14)= a1 +   b1 +   c1 + d1
      elemmt(15)= a2 + 2*b1 +   c1 + d1
      elemmt(16)= a4 + 2*b1 + 2*c1 + d1

      return
      end
