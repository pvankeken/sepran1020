      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      
      if (ichois.gt.2) then
         func = funccf(ichois-10,x,y,z)
      else
	x1 = -0.25
	x2 = 0.25
        if (x.lt.x1) then
	   func=1d0
        else if (x.gt.x2) then
	   func=0d0
        else
	   arg = (x-x1)/(x2-x1)*pi*0.5
	   func = cos(arg)*cos(arg)
        endif
      endif

      return
      end
