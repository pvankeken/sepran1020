      function funccf(ichois,x,y,z)
      parameter(pi=3.1415926)
      implicit double precision(a-h,o-z)
      if (ichois.eq.1) then
         funccf = -pi*cos(pi/2*x)*sin(pi/2*y)
      else
         funccf = pi*cos(pi/2*y)*sin(pi/2*x)
      endif
      return
      end
