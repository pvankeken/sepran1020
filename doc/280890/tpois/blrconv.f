c *************************************************************
c *   BLRCONV
c *
c *   Compute element integrals for the convective terms.
c *   These are of the form 
c *
c *        s(i,j)= // N(i) u dN(j)/dx dxdy + // N(i) v dN(j)/dy dxdy
c *
c *   The integrals are calculated by expressing (u,v) as the sum of
c *   basisfunctions 
c *
c *        u = sum(k)   u(k)N(k)
c *
c *   so 
c *
c *        s(i,j)= sum(k)  u(k) // N(i)N(k) dN(j)/dx  dxdy +
c *
c *                sum(k)  v(k) // N(i)N(k) dN(j)/dy  dxdy
c *
c *   The integrals are evaluated exactly, yielding a weighted sum
c *   over the velocity components.
c *
c *
c *   PvK 110190
c *************************************************************
      subroutine blrconv(elemmt,a,b,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      a72 = a/72
      b72 = b/72
      u1  = u(1)*b72
      u2  = u(2)*b72
      u3  = u(3)*b72
      u4  = u(4)*b72
      v1  = v(1)*a72
      v2  = v(2)*a72
      v3  = v(3)*a72
      v4  = v(4)*a72
      u12 = 2*u1
      u13 = 3*u1
      u16 = 6*u1
      u22 = 2*u2
      u23 = 3*u2
      u26 = 6*u2
      u32 = 2*u3
      u33 = 3*u3
      u36 = 6*u3
      u42 = 2*u4
      u43 = 3*u4
      u46 = 6*u4
      v12 = 2*v1
      v13 = 3*v1
      v16 = 6*v1
      v22 = 2*v2
      v23 = 3*v2
      v26 = 6*v2
      v32 = 2*v3
      v33 = 3*v3
      v36 = 6*v3
      v42 = 2*v4
      v43 = 3*v4
      v46 = 6*v4

      elemmt(1) = elemmt(1) -u16-u23-u3 -u42 -v16-v22-v3 -v43
      elemmt(2) = elemmt(2) +u16+u23+u3 +u42 -v12-v22-v3 -v4
      elemmt(3) = elemmt(3) +u12+u2 +u3 +u42 +v12+v22+v3 +v4
      elemmt(4) = elemmt(4) -u12-u2 -u3 -u42 +v16+v22+v3 +v43

      elemmt(5) = elemmt(5) -u13-u26-u32-u4  -v12-v22-v3 -v4 
      elemmt(6) = elemmt(6) +u13+u26+u32+u4  -v12-v26-v33-v4
      elemmt(7) = elemmt(7) +u1 +u22+u32+u4  +v12+v26+v33+v4
      elemmt(8) = elemmt(8) -u1 -u22-u32-u4  +v12+v22+v3 +v4 
 
      elemmt(9) = elemmt(9) -u1 -u22-u32-u4  -v1 -v2 -v32-v42
      elemmt(10)= elemmt(10)+u1 +u22+u32+u4  -v1 -v23-v36-v42
      elemmt(11)= elemmt(11)+u1 +u22+u36+u43 +v1 +v23+v36+v42
      elemmt(12)= elemmt(12)-u1 -u22-u36-u43 +v1 +v2 +v32+v42
  
      elemmt(13)= elemmt(13)-u12-u2 -u3 -u42 -v13-v2 -v32-v46
      elemmt(14)= elemmt(14)+u12+u2 +u3 +u42 -v1 -v2 -v32-v42
      elemmt(15)= elemmt(15)+u12+u2 +u33+u46 +v1 +v2 +v32+v42
      elemmt(16)= elemmt(16)-u12-u2 -u33-u46 +v13+v2 +v32+v46

      return
      end
