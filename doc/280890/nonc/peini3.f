c ************************************************************* 
c *  PEINI3
c *
c *  Adaption of subroutine ini003
c *  write arrays with priority=1 to file 3
c *  Compress ibuffr
c *
c *  PvK  120290
c *************************************************************
      subroutine peini3(iwork)
      dimension iwork(*)
      common /carray/ iinfor,infor(4500)
      common ibuffr(1)
      common /carbac/ ibufst,inback(1500),lenbac(1500)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree

      do 100 i=1,iinfor
         iprio=infor(3*i)
         if(iprio.eq.-100) then
	     call ini100(i)
	     infor(3*i) = 9
         endif
100   continue
c
c   *   make ibuffr compact
c       first sort starting positions
c
      call ini102(iwork,ninf)
      istart=ibufst
      do 200 i=1,ninf
         l=iwork(i)
         jstart=infor(3*l-2)
         length=infor(3*l-1)
         if(jstart.gt.istart) then
            call ini000(ibuffr,ibuffr,jstart,istart,length,1)
            infor(3*l-2)=istart
         endif
         istart=istart+length
         if(intlen.eq.2 .and. mod(istart,2).eq.0) istart=istart+1
200   continue
      ibfree=istart
      end
