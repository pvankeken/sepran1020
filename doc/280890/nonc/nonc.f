      program nonc
      implicit double precision(a-h,o-z)
      parameter(NPUS=8010,NPUS2=16010)
      parameter(NFUNC=500)
      parameter(NBUFDEF= 1 000 000)
c **********************************************************
c *   nonc
c *
c *   Modelling stationary convection in a square box.
c *
c *   A viscous fluid layer is heated from below. Symmetry
c *   conditions are imposed at the vertical boundaries at
c *   x=0 and x=1. The boundaries are stress free.
c *
c *   The momentum equation is solved with a nonconforming
c *   rectangular element with 12 degrees of freedom.
c *   The heat equation is solved using linear triangles.
c *   User arrays are used to transport solution vectors between
c *   the problems.
c *
c *   PvK 050390
c ******************************************************************
c *   Files:
c *        mesh1    Contains information on the mesh of rectangular
c *                 elements for biharmonical equation
c *        mesh2    Contains information on the mesh of elements
c *                 for the convection-diffusion equation
c **********************************************************************
      dimension kmesh1(100),kmesh2(100),kprob1(100),kprob2(100)
      dimension iuser(100),user(NPUS2),matr1(5),matr2(5)
      dimension irhs1(5),irhs2(5),intmt1(5),intmt2(5),isol1(5),isol2(5)
      dimension islol1(5),islol2(5),iu1(10),u1(10),icurvs(3),istrm(5)
      dimension funcx(NFUNC),funcy(NFUNC),igradt(5)
      dimension contln(100),indcol(100)
      dimension ipredsol(5),ivser(100),vser(NPUS)
      real t0,t1,tnew

      common ibuffr(NBUFDEF)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,
     v               jkader,jtimes
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /usertext/ text,text1,text2
      common /frcount/ ifrinit,ifrcntr,nbase
      common /bstore/ f2name
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      common /seppl2/ fname
      character*80 text,text1,text2,f2name,fname,pname
      common /pefname/ ftname,fnname
      character*80 ftname,fnname

      kmesh1(1)=100
      kmesh2(1)=100
      kprob1(1)=100
      kprob2(1)=100
      iuser(1)=100
       user(1)=NPUS2
      ivser(1)=100
       vser(1)=NPUS
      contln(1)=100
      indcol(1)=100
       funcx(1)=NFUNC
       funcy(1)=NFUNC
       f2name = 'nonc.back'
       ftname = 'temp.prof'
       fnname = 'nuss.prof'

      call start(1,1,0,0)
      nbuffr=NBUFDEF
      open(9,file='mesh1')
      rewind(9)
      call meshrd(2,9,kmesh1)
      open(9,file='mesh2')
      rewind(9)
      call meshrd(2,9,kmesh2)
      close(9)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
      call commat(1,kmesh1,kprob1,intmt1)
      call commat(2,kmesh2,kprob2,intmt2)
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)
      nsup1 = kprob1(29)
      nsup2 = kprob2(29)
      write(6,*) 'NSUPER: ',nsup1,nsup2
      if (nsup1.ne.0.or.nsup2.ne.0) stop
      npoint = kmesh1(8)
      if (npoint.gt.NPUS-10) then
	 write(6,*) 'PERROR: npoint too large for user arrays'
	 write(6,*) 'npoint = ',npoint
	 stop
      endif

      read(5,*) nitermax,eps,relax,irestart
      read(5,*) rayleigh,rlam,q,visc0,b,c

      iu1(1)=2
      iu1(2)=3
      iu1(3)=4
      call creavc(0,1,idum,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
      if (irestart.gt.0) then
	 f2name='restart.back'
	 call openf2(.false.)
	 call readbs(1,isol2,ihelp)
      else
         iu1(1)=1
         call creavc(0,1,idum,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
      endif
      iway=2
      format=7d0
      chheight=0.3d0
      ifrinit=0
      jkader=-4
      call plwin(1,0d0,0d0)
      fname='CPLOT'
      text = 'streamfunction'
      call plotc1(number,kmesh1,kprob1,isol1,contln,ncntln,format,
     v            yfaccn,jsmoot)
      x = format+1d0
      call plwin(2,x,0d0)
      text = 'Velocity'
      call plotvc(2,3,isol1,isol1,kmesh1,kprob1,format,yfaccn,
     v             0d0)
      call plwin(3,x,0d0)
      text = 'Temperature'
      call plotc1(number,kmesh2,kprob2,isol2,contln,ncntln,format,
     v            yfaccn,jsmoot)

      stop
      istop = 0
      difmax = 0d0
      niter = 0
      t = 0d0
      call second(t0)
      tnew = t0
100   continue
        niter = niter+1
	call copyvc(isol1,islol1)
	call copyvc(isol2,islol2)

	call pecof(1,kmesh1,kprob2,isol2,iuser,user)
        call pestoh(kmesh1,kprob1,intmt1,isol1,islol1,user,
     v              iuser,matr1,irhs1)

	call chtype(1,kprob2)
	call pecof(2,kmesh2,kprob1,isol1,iuser,user)
        call peheat(kmesh2,kprob2,intmt2,isol2,islol2,user,iuser,
     v              matr2,irhs2)

c       dif1 = anorm(0,3,0,kmesh1,kprob1,isol1,islol1,ielhlp)
c       umax=anorm(1,3,0,kmesh1,kprob1,isol1,islol1,ielhlp)
	dif2 = anorm(0,3,0,kmesh2,kprob2,isol2,islol2,ielhlp)
	tmax = anorm(1,3,0,kmesh2,kprob2,isol2,islol2,ielhlp)
	difmax = dif2/tmax
	call second(t1)
	call chtype(2,kprob2)
        call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v            isol2,iuser,user,ielhlp)  
	temp1  = bounin(1,1,1,1,kmesh2,kprob2,1,1,isol2,iuser,
     v           user)
        gnus1  = -bounin(2,1,1,1,kmesh2,kprob2,3,3,igradt,iuser,
     v             user)/temp1
        gnus2  = bounin(2,1,1,1,kmesh2,kprob2,1,1,igradt,iuser,
     v             user)/rlam
        call pevrms(isol1,kmesh1,kprob1,ivser,vser)
        vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v               vser,ihelp)/rlam
        vrms = sqrt(vrms2)
        icurvs(1) = -1
        icurvs(2) = 3
        icurvs(3) = 3
        call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
        nunkp=funcy(5)
        q2 = -funcy(6)
        q1 = -funcy(5+nunkp)
        icurvs(2) = 1
        icurvs(3) = 1
        call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
        q4 = -funcy(6)
        q3 = -funcy(5+nunkp)

	cpu = t1-t0
	write(6,11) niter,difmax,cpu,gnus1,vrms,q1,q2
11      format(i4,f12.8,f8.2,f12.7,f12.4,2f12.7)
	call flush(6)
	
	if (difmax.gt.eps.and.niter.lt.nitermax)  then
	    if (relax.gt.0.and.relax.lt.1) then
	       call algebr(3,0,islol1,isol1,isol1,kmesh1,kprob1,
     v                     1-relax,relax,0,0,ip)
	       call algebr(3,0,islol2,isol2,isol2,kmesh2,kprob2,
     v                     1-relax,relax,0,0,ip)
	    endif
	    f2name='restart.back'
	    call openf2(.true.)
	    call writbs(0,1,numarr,'temp',isol2,ihelp)
	    call writb1
	    goto 100
        endif

        
      call chtype(2,kprob2)
      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v            isol2,iuser,user,ielhlp)  
      gnus1 = -bounin(2,3,1,1,kmesh2,kprob2,3,3,igradt,iuser,user)/temp1
      gnus2 = bounin(2,3,1,1,kmesh2,kprob2,1,1,igradt,iuser,user)/rlam
      giso1 = bounin(2,3,1,1,kmesh2,kprob2,2,2,igradt,iuser,user)
      giso2 = bounin(2,3,1,1,kmesh2,kprob2,4,4,igradt,iuser,user)

      call pevrms(isol1,kmesh1,kprob1,ivser,vser)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v             vser,ihelp)/rlam
      vrms = sqrt(vrms2)
      icurvs(1) = -1
      icurvs(2) = 3
      icurvs(3) = 3
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      nunkp=funcy(5)
      q2 = -funcy(6)
      q1 = -funcy(5+nunkp)
      icurvs(2) = 1
      icurvs(3) = 1
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      q4 = -funcy(6)
      q3 = -funcy(5+nunkp)
      nx = nunkp
      ny = npoint/nunkp

      write(6,*)
      write(6,'(6f12.7)') gnus1,vrms,q1,q2,q3,q4
      write(6,'(''heatflow: '',6f12.7)') gnus1,gnus2,giso1,giso2
c     *** Plot velocity and temperature
      iway=2
      format=10d0
      chheight=0.3d0
      ifrinit=0
      jkader=-4
      fname='PLOT'
      call plwin(1,0d0,0d0)
      text = 'Streamfunction'
      call stream(1,ivec,istrm,0,psiphi,kmesh1,kprob1,isol1)
      call plotc1(number,kmesh1,kprob1,istrm,contln,ncntln,format,
     v            yfaccn,jsmoot)
      f2name='restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'temp',isol2,ihelp)
      call writb1


      x = format+1d0
      call plwin(3,x,0d0)
      text = 'Temperature'
      call plotc1(number,kmesh2,kprob2,isol2,contln,ncntln,format,
     v            yfaccn,jsmoot)
      f2name='restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'temp',isol2,ihelp)
      call writb1

c     *** Depth profiles of T and Nu 
      call deriva(1,1,2,1,1,igradt,kmesh2,kprob2,isol2,
     v            isol2,iuser,user,ielhlp)  
      call pecopy(3,isol1,user,kprob1,10,fac)
      call pecopy(0,isol2,user,kprob2,10+npoint,fac)
      call pecopy(0,igradt,user,kprob2,10+2*npoint,fac)
      call penuss(nx,ny,kmesh2,user)

      pname='pplot'
      call plotp(kmesh2,kprob2,isol2,pname)
      call finish(0)

      end

      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
    
      if (ichois.eq.1) then
         wpi = pi/rlam
         func = 0.01*sin(pi*y)*cos(wpi*x) + (1-y)
      else if (ichois.eq.2) then
c        ** psi
	 func = -sin(pi*x)*sin(pi*y)
      else if (ichois.eq.3) then
	 func = -pi*sin(pi*x)*cos(pi*y)
      else if (ichois.eq.4) then
	 func = pi*cos(pi*x)*sin(pi*y)
      endif
  
      return
      end
