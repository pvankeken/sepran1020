c *************************************************************
c *   PEHEAT
c *
c *   Solves the heat equation
c *************************************************************
      subroutine peheat(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      implicit double precision(a-h,o-z)
      dimension matrs(*),intmat(*),kmesh(*),kprob(*),isol(*)
      dimension iuser(*),user(*)
      dimension irhsd(*)

      call systm0(1,matrs,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matrs,isol,irhsd,intmat,kprob)

      return
      end
      
