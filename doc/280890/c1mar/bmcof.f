c *************************************************************
c *   BMCOF
c *
c *   PvK 120490
c *************************************************************
      subroutine bmcof(ichois,kmesh1,kmesh2,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh1(*),kmesh2(*),kprob(*),isol(*),iuser(*),user(*)
      common /bmint/ ityps,itypt,nruls,nrult

      if (ichois.eq.1) then
c        *** Stokes equation
	 if (ityps.eq.400) then
c            *** primitive variables, quadratic triangle
	     call pecof(1,kmesh1,kprob,isol,iuser,user)
         else if (ityps.eq.11) then
c            *** streamfunction, nonconforming element
             call c1cof(1,kmesh1,kmesh2,kprob,isol,iuser,user)
         else if (ityps.eq.100.or.ityps.eq.104) then
c            *** streamfunction, vorticity
	     call strcof(1,kmesh1,kmesh2,kprob,isol,iuser,user)
         endif
      endif

      if (ichois.eq.2) then
c        *** Temperature equation
	 ich = 2
	 if (itypt.eq.3.or.itypt.eq.5) ich = 3
	 if (ityps.eq.11) then
	    call c1cof(ich,kmesh1,kmesh2,kprob,isol,iuser,user)
	 else if (ityps.eq.100.or.ityps.eq.104) then
	    call strcof(ich,kmesh1,kmesh2,kprob,isol,iuser,user)
         else if (ityps.eq.400) then
	    call pecof(ich,kmesh2,kprob,isol,iuser,user)
         endif
      endif

      return
      end

c *************************************************************
c *   STRCOF
c *
c *
c *   1  Stokes equation  Type 100/104
c *
c *   2  Temperature equation  Type 100/104
c *
c *   PvK 120490
c *************************************************************
      subroutine strcof(ichois,kmesh1,kmesh2,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      common /bmint/ ityps,itypt,nruls,nrult
      dimension kmesh1(*),kmesh2(*),kprob(*),isol(*),iuser(*),user(*)
      dimension igrad(5),ivelx(5),ively(5)

      if (ichois.eq.1) then
	 npoint = kmesh1(8)
	 if (npoint+7.gt.user(1)) then
	    write(6,*) 'PERROR(strcof): array USER too small'
	    stop
         endif
	 iuser(6)  =  7
	 iuser(7)  = -6
	 iuser(8)  =  0
	 iuser(9)  = -6
	 iuser(10) = 0
	 iuser(11) = 0
	 iuser(12) = 0
	 iuser(13) = 2001
	 iuser(14) = 7
	  user(6)  = 1d0
         idc = 1
	 if (itypt.eq.104) idc = 2
         call deriva(idc,1,1,1,1,igrad,kmesh2,kprob,isol,isol,
     v               iuser,user,i)
         call pecopy(0,igrad,user,kprob,7,-rayleigh) 

      else if (ichois.eq.2) then
c        *** Heat equation

         npoint = kmesh2(8)
         if (2*npoint+7.gt.user(1)) then 
	    write(6,*) 'PERROR(strcof): array USER too small'
	    stop
	 endif
	 iuser(6)  = 7
	 iuser(7)  = -6
	  user(6)  = 1d0
         iuser(8)  = 0
	 iuser(9)  = -6
	 iuser(10) = 2001
	 iuser(11) = 7
	 iuser(12) = 2001
	 iuser(13) = 7+npoint
	 iuser(14) = 0
	 iuser(15) = 0
	 idc = 1
	 if (ityps.eq.104) idc=2
c        *** linear elements for streamfunction
         call deriva(idc,1,2,1,1,ivelx,kmesh1,kprob,isol,isol,
     v               iuser,user,i)
	 call deriva(idc,1,1,1,1,ively,kmesh1,kprob,isol,isol,
     v               iuser,user,i)
         factor = 1d0
         call pecopy(0,ivelx,user,kprob,7,factor)
         factor = -1d0
         call pecopy(0,ively,user,kprob,7+npoint,factor)
      endif


      return
      end


c *************************************************************
c *   C1COF
c *  
c *   Fill coefficients for Sepran problems in arrays user
c *
c *   Ichois=1    Stokes equation (streamfunction) type 11
c *
c *   Ichois=2    Temperature equation (type 100 etc); 
c *               velocity components derived from type 11 solution
c *
c *   Ichois=3    Temperature equation (types 3 and 5)
c *
c *   PvK 110490
c *************************************************************
      subroutine c1cof(ichois,kmesh1,kmesh2,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh1(*),kmesh2(*),kprob(*),isol(*),iuser(*),user(*)
      dimension igrad(5),igrad2(5)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
	
      npoint = kmesh2(8)
      do 10 i=6,20
10       iuser(i) = 0
      do 20 i=6,10+3*npoint
20        user(i) = 0.

      if (ichois.eq.1) then
c        *** Stokes equation
c        *** iuser(6) = icrhsd = 3
c        *** user(6),user(7).... = Ra*T
	 iuser(6) = 3
	 call pecopy(0,isol,user,kprob,6,rayleigh)
	 if (visc0.le.0.) then
c           iuser(15)= -7
         else
c   write(6,*) 'PERROR(c1cof)  eta <> constant not yet impl'
c   stop
c   iuser(16)= 10+npoint
c   call pevisc(kmesh2,kprob,isol,user(10+npoint))
         endif

       else if (ichois.eq.2) then
c         *** Temperature equation
c         *** a11,a12,a22,u,v,b,f,iupwind
	  iuser(6) = 7
	  iuser(7) = -6
	  iuser(8) = 0
          iuser(9) = -6
	  iuser(10)= 2001
	  iuser(11)= 10
	  iuser(12)= 2001
	  iuser(13)= 10+npoint
	  iuser(14)= 0
	  iuser(15)= -7
	  iuser(16)= 2
	   user(6) = 1d0
	   user(7) = q
          call pecopy(11,isol,user,kprob,10,1d0)
	  call pecopy(12,isol,user,kprob,10+npoint,1d0)
       else if (ichois.eq.3) then
	  user(31) = 1d0
	  call pecopy(11,isol,user,kprob,32,1d0)
	  call pecopy(12,isol,user,kprob,32+npoint,1d0)
       else
	  write(6,*) 'PERROR(c1cof): unknown option: ',ichois
       stop
      endif

      return
      end

c *************************************************************
c *   PECOF
c *  
c *   Fill coefficients for Sepran problems in arrays user
c *
c *   Ichois=1    Stokes equation (primitive variables) type 400
c *
c *   Ichois=2    Temperature equation (type 100 etc)
c *
c *   PvK 160290
c *************************************************************
      subroutine pecof(ichois,kmesh,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),iuser(*),user(*)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
	
      npoint = kmesh(8)
      do 10 i=6,20
10       iuser(i) = 0
      do 20 i=6,10+2*npoint
20        user(i) = 0.

      if (ichois.eq.1) then
c        *** Stokes equation
c        *** eps, rho, MCONV, omega, f1, f2, MODELV, eta
	 iuser(6) = 7
	 iuser(7) = -6
	 iuser(8) = -7
	 iuser(9) = 0
	 iuser(10)= 0
	 iuser(11)= 0
	 iuser(12)= 2001
	 iuser(13)= 10
	 iuser(14)= 1
	  user(6) = 1d-6
	  user(7) = 1d0
	 call pecopy(0,isol,user,kprob,10,rayleigh)
	 if (visc0.le.0.) then
            iuser(15)= -7
         else
	    iuser(15)= 2001
	    iuser(16)= 10+npoint
	    call pevisc(kmesh,kprob,isol,user(10+npoint))
         endif

       else if (ichois.eq.2) then
c         *** Temperature equation
c         *** a11,a12,a22,u,v,b,f,iupwind
	  iuser(6) = 7
	  iuser(7) = -6
	  iuser(8) = 0
          iuser(9) = -6
	  iuser(10)= 2001
	  iuser(11)= 10
	  iuser(12)= 2001
	  iuser(13)= 10+npoint
	  iuser(14)= 0
	  iuser(15)= -7
	  iuser(16)= 2
	   user(6) = 1d0
	   user(7) = q
          call pecopy(2,isol,user,kprob,10,1d0)
	  call pecopy(3,isol,user,kprob,10+npoint,1d0)
       else if (ichois.eq.3) then
	   user(31) = 1d0
	   call pecopy(2,isol,user,kprob,32,1d0)
	   call pecopy(3,isol,user,kprob,32+npoint,1d0)
       else
	  write(6,*) 'PERROR(pecof): unknown option: ',ichois
       stop
      endif

      return
      end

c *************************************************************
c *   PEVISC
c *
c *   Fill array user with the value of the temperature and depth 
c *   dependent viscosity in each nodal point
c *
c *   PvK 050390
c ************************************************************* 
      subroutine pevisc(kmesh,kprob,isol,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      call pecopy(0,isol,user,kprob,1,1d0)
      npoint = kmesh(8)
      kelmi  = kmesh(23)
      call ini050(kelmi,'pevisc')
      ikelmi = infor(1,kelmi)
      call pevsc1(ibuffr(ikelmi),user,npoint)

      return
      end

      subroutine pevsc1(coor,user,npoint)
      implicit double precision(a-h,o-z)
      dimension coor(2,*),user(*)
      common /peparam/ rayleigh,rlam,q,visc0,b,c

      do 10 i=1,npoint
	 temp = user(i)
	 z    = coor(2,i)
	 visc = visc0*exp(-b*temp+c*(1-z))
	 user(i) = visc
10    continue
      
      return
      end

