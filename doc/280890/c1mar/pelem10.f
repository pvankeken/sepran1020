c *************************************************************
c *   PELEM10
c *
c *   Element matrices for THE nonconforming element
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *
c *    iuser(6)           icrhsd    Choice of rhsd vector
c *                                 0: no righthand side vector built
c *                                 1: buoyancy effect of markerchain
c *                                 2: continuous function
c *    iuser(7)           nmark     number of markers
c *    iuser(8)           elem(1)   number of elements in which the
c *      ..                ..       markers are positioned
c *    iuser(7+nmark)     elem(nmark)
c *    iuser(8+nmark)     ncont     number of points on the contour
c *
c *    if (icrhsd=1): array user contains from position 6 the
c *                   coordinates of the markers in the sequence
c *                   x(1),y(1),x(2),y(2),....,x(nmark),y(nmark)
c *                   The next positions are taken by points
c *                   to close the contour (totalling ncont points):
c *                   xa(1),ya(1),xa(2),ya(2)......,x(1),y(1)
c *
c *
c *    if (ichrsd=2): array user contains from position 6 the
c *                   nodalpoint values of the continuous function g:
c *                   from 6 to 5+npoint:          g(i)
c *                   from 6+npoint to 5+2npoint:  dg(i)/dy
c *                   from 6+2npoint to 5+3npoint: -dg(i)/dx
c *
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *       10          Equation of Poisson
c *       11          Biharmonical equation 
c *
c *   PvK 030490
c *************************************************************
      subroutine pelem10(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                   vector,index1,index2)
      implicit double precision(a-h,o-z)
      parameter(done=1d0)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),g(12),visco(16),temp(4)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      nphi = 12
      if (ifirst.eq.0) then
         if (nrule.ne.0) then
c           *** Numerical integration
            if (nrule.gt.0) then
c              *** Gauss quadrature: 
c              *** fill values of shapefunctions and derivatives of
c              *** shapefunction in the nodalpoints.
c              *** Use common pegauss and peshape
               if (nrule.eq.1) ngaus=4
               if (nrule.eq.2) ngaus=9
               if (nrule.eq.3) ngaus=16
               call fwgaus
               call c1dgaus
            else
               write(6,*) 'PERROR(elem-030490)  nrule <= 0'
            stop
            endif
         endif
      endif

c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
         ih = index1(i)
         temp(i) = user(5+ih)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))

      if (vector) then
      icrhsd = iuser(6)
      if (icrhsd.eq.0) then
         do 12 i=1,nphi
12             elemvc(i) = 0.
      else if (icrhsd.eq.1) then
         x0 = x(1)
         y0 = y(1)
         call c1rhmar(elemvc,iuser,user,x0,y0,a,b)
      else if (icrhsd.eq.2) then
         do 14 i=1,10,3
            ih     = index1((i+2)/3)
            g(i)   = user(5+ih+npoint)
            g(i+1) = user(5+2*npoint+ih)
            g(i+2) = user(5+3*npoint+ih)
14       continue
         call c1rhcon(elemvc,g,a,b)
      else if (icrhsd.eq.3) then
c           *** rhsd = Ra*dT/dx; Ra*T is given in user(6),user(7)...
         do 15 i=1,4
            ih = index1(i)
            g(i) = user(5+ih)
15          continue
            call c1rhdtx(elemvc,g,a,b)
      else
         write(6,*) 'PERROR(elem-030490) icrhsd has wrong value'
         write(6,*) '                    icrhsd = ',icrhsd
         stop
         endif
      endif
     


      if (matrix) then

         if (itype.eq.10) then
c        *** Stiffness matrix; Galerkin weighting
            call c1dif(elemmt,a,b,done)
            return
         else if (itype.eq.11) then
            call fgvisc(iuser,user,visco,x(1),y(1),a,b,temp)
            call c1bihi(elemmt,a,b,visco)
         else
            write(6,*) 'PERROR(elem): itype <> 10,11 not implemented'
            write(6,*) 'itype = ',itype
            stop
         endif
      endif
      return
      end

c *************************************************************
c *   FWGAUS
c *
c *   Fill coordinates of gausspoints in arrays xg and yg
c *   Fill weights for the integration in array w
c *
c *   parameter ngaus has to be filled
c *
c ************************************************************* 
      subroutine fwgaus
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      data a1,h1/0,2/
      data a2,h2/0.577350269189626,1/
      data a31,h31/0.774596669241483,0.5555555555555555/
      data a32,h32/0,0.888888888888888888/
      data a41,h41/0.861136311594053,0.347854845137454/
      data a42,h42/0.339981043584856,0.652145154862546/

      if (ngaus.eq.4) then
         g1    = 0.5 - 0.5*a2
         g2    = 0.5 + 0.5*a2
         xg(1) = g1
         xg(2) = g2
         xg(3) = g2
         xg(4) = g1
         yg(1) = g1
         yg(2) = g1
         yg(3) = g2
         yg(4) = g2
         do 10 i=1,4
            w(i) = 0.25
10       continue
      else if (ngaus.eq.9) then
         g1    = 0.5 - 0.5*a31
         g2    = 0.5
         g3    = 0.5 + 0.5*a31
         xg(1) = g1
         xg(2) = g2
         xg(3) = g3
         xg(4) = g1
         xg(5) = g2
         xg(6) = g3
         xg(7) = g1
         xg(8) = g2
         xg(9) = g3
         yg(1) = g1
         yg(2) = g1
         yg(3) = g1
         yg(4) = g2
         yg(5) = g2
         yg(6) = g2
         yg(7) = g3
         yg(8) = g3
         yg(9) = g3
         w1    = h31 / 2
         w2    = h32 / 2
         w(1)  = w1*w1
         w(2)  = w1*w2
         w(3)  = w1*w1
         w(4)  = w2*w1
         w(5)  = w2*w2
         w(6)  = w2*w1
         w(7)  = w1*w1
         w(8)  = w1*w2
         w(9)  = w1*w1
      endif

      return
      end

c *************************************************************
c *   c1dgaus
c *
c *   Fill values of shapefunctions and derivatives of 
c *   shapefunctions in pegauss
c *
c *************************************************************
      subroutine c1dgaus
      implicit double precision(a-h,o-z)
      parameter(done=1d0)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension fp(16)

      if (nphi.ne.12) then
         write(6,*) 'PERROR(c1dgaus) nphi<>12 not implemented'
         stop
      endif

      do 10 i=1,ngaus
         x = xg(i)
         y = yg(i)
         call fphi(fp,x,y,done,done)
         do 11 j=1,nphi
11          phi(j,i) = fp(j)
         call fdpdx(fp,x,y,done,done)
         do 12 j=1,nphi
12          dphidx(j,i) = fp(j)
         call fdpdy(fp,x,y,done,done)
         do 13 j=1,nphi
13          dphidy(j,i) = fp(j)
         call fd2pdx(fp,x,y,done,done)
         do 14 j=1,nphi
14          d2phidx(j,i) = fp(j)
         call fd2pdy(fp,x,y,done,done)
         do 15 j=1,nphi
15          d2phidy(j,i) = fp(j)
         call fd2pxy(fp,x,y,done,done)
         do 16 j=1,nphi
16          d2phixy(j,i) = fp(j)
10    continue

      return
      end

      subroutine fphi(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1) = 1-x*y -(3-2*y)*y*y*(1-x) -(1-y)*(3-2*x)*x*x
      fp(2) = y*(1-y)*(1-y)*(1-x)*b
      fp(3) = -(1-y)*x*(1-x)*(1-x)*a
      fp(4) = (1-y)*(3-2*x)*x*x + y*(1-y)*(1-2*y)*x
      fp(5) = y*(1-y)*(1-y)*x*b
      fp(6) = (1-y)*(1-x)*x*x*a
      fp(7) = (3-2*y)*y*y*x - y*x*(1-x)*(1-2*x)
      fp(8) = -(1-y)*y*y*x*b
      fp(9) = x*x*(1-x)*y*a
      fp(10)= (3-2*y)*y*y*(1-x)+x*y*(1-x)*(1-2*x)
      fp(11)= -(1-y)*y*y*(1-x)*b
      fp(12)= -x*y*(1-x)*(1-x)*a

      return
      end

      subroutine fdpdx(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -y+(3-2*y)*y*y - (1-y)*(6*x-6*x*x)
      fp(2)  = -y*(1-y)*(1-y)*b
      fp(3)  = -(1-y)*(3*x*x-4*x+1)*a
      fp(4)  = (1-y)*(6*x-6*x*x)+y*(1-y)*(1-2*y)
      fp(5)  = y*(1-y)*(1-y)*b
      fp(6)  = (1-y)*(2*x-3*x*x)*a
      fp(7)  = (3-2*y)*y*y-y*(1-6*x+6*x*x)
      fp(8)  = -(1-y)*y*y*b
      fp(9)  = (2*x-3*x*x)*y*a
      fp(10) = -(3-2*y)*y*y + y*(1-6*x+6*x*x)
      fp(11) = (1-y)*y*y*b
      fp(12) = -y*(3*x*x-4*x+1)*a

      return
      end

      subroutine fdpdy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -x - (6*y-6*y*y)*(1-x) + (3-2*x)*x*x
      fp(2)  = (3*y*y-4*y+1)*(1-x)*b
      fp(3)  = x*(1-x)*(1-x)*a
      fp(4)  = (2*x-3)*x*x + (6*y*y-6*y+1)*x
      fp(5)  = (3*y*y-4*y+1)*x*b
      fp(6)  = (x-1)*x*x*a
      fp(7)  = (6*y-6*y*y)*x - x*(1-x)*(1-2*x)
      fp(8)  = -(2*y-3*y*y)*x*b
      fp(9)  = x*x*(1-x)*a
      fp(10) = (6*y-6*y*y)*(1-x)+x*(1-x)*(1-2*x)
      fp(11) = -(2*y-3*y*y)*(1-x)*b
      fp(12) = -x*(1-x)*(1-x)*a

      return
      end

      subroutine fd2pdx(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -(1-y)*(6-12*x)
      fp(2)  = 0.
      fp(3)  = -(1-y)*(6*x-4)*a
      fp(4)  = (1-y)*(6-12*x)
      fp(5)  = 0.
      fp(6)  = (1-y)*(2-6*x)*a
      fp(7)  = -y*(12*x-6)
      fp(8)  = 0.
      fp(9)  = (2-6*x)*y*a
      fp(10) = y*(12*x-6)
      fp(11) = 0.
      fp(12) = -y*(6*x-4)*a

      return
      end

      subroutine fd2pdy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = (12*y-6)*(1-x)
      fp(2)  = (6*y-4)*(1-x)*b
      fp(3)  = 0.
      fp(4)  = (12*y-6)*x
      fp(5)  = (6*y-4)*x*b
      fp(6)  = 0.
      fp(7)  = (6-12*y)*x
      fp(8)  = (6*y-2)*x*b
      fp(9)  = 0.
      fp(10) = (6-12*y)*(1-x)
      fp(11) = (6*y-2)*(1-x)*b
      fp(12) = 0.

      return
      end

      subroutine fd2pxy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = 6*y-6*y*y+6*x-6*x*x-1
      fp(2)  = -(3*y*y-4*y+1)*b
      fp(3)  = (3*x*x-4*x+1)*a
      fp(4)  = 6*y*y-6*y+6*x*x-6*x+1
      fp(5)  = (3*y*y-4*y+1)*b
      fp(6)  = (3*x*x-2*x)*a
      fp(7)  = 6*y-6*y*y+6*x-6*x*x-1
      fp(8)  = (3*y*y-2*y)*b
      fp(9)  = (2*x-3*x*x)*a
      fp(10) = 6*y*y-6*y+6*x*x-6*x+1
      fp(11) = (2*y-3*y*y)*b
      fp(12) = -(3*x*x-4*x+1)*a

      return
      end

c *************************************************************
c *   C1DIF
c * 
c *************************************************************
      subroutine c1dif(elemmt,a,b,alpha)
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension elemmt(*)
      dimension f1(12)
     
      do 10 i=1,10,3
         f1(i)   = 1d0
         f1(i+1) = b
         f1(i+2) = a
10    continue
      ab = a/b*alpha
      ba = b/a*alpha
      if (nrule.gt.0) then
       do 20 j=1,nphi
         do 20 i=j,nphi
            ioff = (j-1)*nphi+i
            sumx = 0.
            sumy = 0.
            do 30 k=1,ngaus
             sumx = sumx + w(k)*dphidx(i,k)*dphidx(j,k)
30                sumy = sumy + w(k)*dphidy(i,k)*dphidy(j,k)
               elemmt(ioff) = (ab*sumx + ba*sumy)*f1(i)*f1(j)
            iof2 = (i-1)*nphi+j
            if (i.gt.j) elemmt(iof2) = elemmt(ioff)
20       continue
      endif

      return
      end


c *************************************************************
c *   C1BIHI
c * 
c *   PvK 050490
c *************************************************************
      subroutine c1bihi(elemmt,a,b,visco)
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension elemmt(*),visco(*)
      dimension f1(12)
     
      do 10 i=1,10,3
         f1(i)   = 1d0
      f1(i+1) = b
         f1(i+2) = a
10    continue
      fa1 = 1d0/(a*a*a*a)
      fa2 = -1d0/(a*a*b*b)
      fa3 = fa2
      fa4 = 1d0/(b*b*b*b)
      fa5 = -4*fa2
      if (nrule.gt.0) then
      do 20 j=1,nphi
         do 20 i=j,nphi
            ioff = (j-1)*nphi+i
            sum1 = 0.
            sum2 = 0.
            sum3 = 0.
            sum4 = 0.
            sum5 = 0.
            do 30 k=1,ngaus
             sum1 = sum1 + visco(k)*w(k)*d2phidx(i,k)*d2phidx(j,k)
             sum2 = sum2 + visco(k)*w(k)*d2phidy(i,k)*d2phidx(j,k)
             sum3 = sum3 + visco(k)*w(k)*d2phidx(i,k)*d2phidy(j,k)
             sum4 = sum4 + visco(k)*w(k)*d2phidy(i,k)*d2phidy(j,k)
             sum5 = sum5 + visco(k)*w(k)*d2phixy(i,k)*d2phixy(j,k)
30             continue 
            sum = fa1*sum1+fa2*sum2+fa3*sum3+fa4*sum4+fa5*sum5
               elemmt(ioff) = a*b*sum*f1(i)*f1(j)
            iof2 = (i-1)*nphi+j
            if (i.gt.j) elemmt(iof2) = elemmt(ioff)
20       continue
      endif

      return
      end


c *************************************************************
c *   C1RHCON
c *
c *   Calculate the element vector for a continuous load function g:
c *
c *    e           e           12           e  e
c *   f  =  //  g N   de   =  sum  // g(j) N  N   de 
c *    i    e      i          j=1           j  i
c *
c *         12  ngaus             e      e
c *      = sum   sum   g(j) w(k) N (k)  N (k)
c *         j     k               j      i
c *
c *   PvK 040490
c *************************************************************
      subroutine c1rhcon(f,g,a,b)
      implicit double precision(a-h,o-z)
      dimension f(*),g(*),f1(12)

      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      
      do 5  i=1,10,3
      f1(i)   = 1d0
      f1(i+1) = b
      f1(i+2) = a
5     continue

      do 10 i=1,nphi
         sum = 0
      do 20 j=1,12
         sumk = 0.
         do 30 k=1,ngaus
            sumk = sumk + w(k)*phi(j,k)*phi(i,k)
30          continue
         sum = sum + sumk*g(j)*f1(i)*f1(j)
20       continue
      f(i) = sum*a*b
10    continue
  
      return
      end


c *************************************************************
c *   C1RHMAR
c *
c *   Built element vector according to formula (4) in
c *   Christensen, GJRAS, 68, 487-497 (1982).
c *    e
c *   f   = sum of phi (xm,ym)*[ y(m+1) - y(m-1) ]*0.5
c *    i              i
c *         over all markers (xm,ym) in element e
c *
c *   The element is THE C1 nonconforming element with length a
c *   and height b.
c *   Information on the positions of the markers is stored in
c *   IUSER and USER.
c *
c *   PvK 040490
c *************************************************************
      subroutine c1rhmar(elemvc,iuser,user,x0,y0,a,b)
      implicit double precision(a-h,o-z)
      dimension elemvc(*),iuser(*),user(*)
      dimension fp(12),f(12)

      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      common /c1mark/ nochain,ichain,imark(10)

c     *** Loop over the markers. If the marker is in the element
c     *** it will contribute to the element vector
      nmark = imark(1)
      do 5 i=1,nphi
5        f(i) = 0. 
      do 10 i=1,nmark
        if (iuser(7+i).eq.ielem) then
         xm = user(4+2*i+npoint)
         ym = user(5+2*i+npoint)
         if (i.eq.1) then
            yfac = user(7+2*i+npoint)-ym
         else if (i.eq.nmark) then
            yfac = ym-user(3+2*nmark+npoint)
         else
            yfac = 0.5*(user(7+2*i+npoint)-user(3+2*i+npoint))
         endif
c        *** calculate the basisfunctions in the marker
         xi  = (xm-x0)/a
         eta = (ym-y0)/b
         call fphi(fp,xi,eta,a,b)
         do 20 j=1,nphi
20          f(j) = fp(j)*yfac + f(j)
        endif
10    continue

      do 30 i=1,nphi
         elemvc(i) = -f(i)
30    continue

      return
      end

c *************************************************************
c *   C1RHDTX
c *
c *   Calculate the righthand side vector
c *                               ngaus
c *   f    =  // Ra dT/dx N  de  = sum  w  Ra dT/dx(x ) N (x ) 
c *    i      e            i       k=1   k           k      k
c *
c *   Ra*T for each nodalpoint is given in array g.
c *
c *   PvK  170490
c *************************************************************
      subroutine c1rhdtx(f,g,a,b)
      implicit double precision(a-h,o-z)
      dimension f(*),g(*)
      dimension f1(12),dtx(16)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

      do 5 i=1,10,3
      f1(i)   = 1d0
      f1(i+1) = b
      f1(i+2) = a
5     continue
      do 6 i=1,ngaus
c        *** Ra*dT/dx = sum Ra*T(i)*dN(i)/dx  in the Gauss points
      eta    = yg(i)
      dtx(i) = 1d0/a*(eta*(g(1)-g(2)+g(3)-g(4)) - g(1)+g(2))
6     continue

      do 10 i=1,nphi
      sum = 0
      do 20 k=1,ngaus
         sum = sum + w(k)*dtx(k)*phi(i,k)
20       continue
         f(i) = f1(i)*sum*a*b
10    continue

      return
      end

c *************************************************************
c *   FGVISC
c *
c *   Determine the value of viscosity in the Gauss points
c *   Information on the viscosity is stored in arrays IUSER,USER
c *   and common /c1visc/ viscl,ctd,cpd,itypv,nl
c *   where
c *          viscl(10)   Array with viscosity in layers 1..10
c *          nl          Number of layers
c *          ctd,cpz     Coefficients indicating the temperature 
c *                      and depth dependence 
c *          itypv       Type of viscosity distribution:
c *                      0  : constant viscosity
c *                           visco = 1
c *                      1  : layered viscosity distribution
c *                           visco = visl
c *                      2  : p,T dependence according to
c *                           visco = vist =   exp(ctd*T + cpz*z)
c *                      4  : non-Newtonian viscosity
c *                           visco = visn
c *                      Combinations are possible, e.g.
c *                      7  : visco = visl*vist*visn
c *         
c *   PvK 230490
c *************************************************************
      subroutine fgvisc(iuser,user,visco,x0,y0,a,b,temp)
      implicit double precision(a-h,o-z)
      dimension iuser(*),user(*),visco(*),temp(*)
      dimension visl(16),vist(16),visn(16)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      save ifirst
      data ifirst/0/

      do 10 i=1,ngaus
            visco(i) = 1d0
10    continue
      if (itypv.eq.0) then
c        *** viscosity is constant
         return
      endif
      if (ivl) then
c        *** Viscosity depends on position of point with respect to
c        *** markerchain
         if (nl.gt.2) then
c           *** Number of layers too large
            write(6,*) 'PERROR(pelem10:fgvisc) number of layers '
            write(6,*) '       too large: ',nl
            stop
         endif
         if (nxpix.eq.0) then
            write(6,*) 'PERROR(pelem10:fgvisc) nxpix=0'
            stop
         endif
c        *** Information on the layer dependent properties is
c        *** stored in the pixelarray ipix(nxpix,nypix)
c        *** (see Creation of rasterfiles from markerchain models,
c        ***      PvK June 4 1990)
         do 21 k=1,ngaus
            x = a*xg(k)+x0
            y = b*yg(k)+y0
            ix = x/rlam*(nxpix-1)+1
            iy = (1-y)*(nypix-1)+1
            ip = (iy-1)*nxpix + ix
            ival = ipix(ip)
            if (ival.eq.1) then
               visco(k) = visco(k)*viscl(1)
            else
               visco(k) = visco(k)*viscl(2)
            endif
21       continue
      endif
      if (ivt) then
c        *** viscosity is dependent on temperature and depth
c        *** temperature in the nodalpoints is stored in temp
c        *** Bilinear interpolation is used to obtain the
c        *** temperature in the gaussian points
         do 22 k=1,ngaus
            x = xg(k)
            y = yg(k)
            z = 1-y0-b*yg(k)
            t = (1-x)*(1-y)*temp(1) + x*(1-y)*temp(2)+
     v          x*y*temp(3) + (1-x)*y*temp(4)
            visco(k) = visco(k)*exp(cpd*z+ctd*t)
22       continue
      endif
      do 23 k=1,ngaus
23       write(93,'(3f12.3)') x0+a*xg(k),y0+b*yg(k),visco(k)
      return
      end

c *************************************************************
c *   CUTIT
c *
c *   Determine number of crossing of the line piece (0,0)-(xg,yg)
c *   with a closed contour defined by the markerchain CM
c * 
c *   PvK 230490
c *************************************************************
      function cutit(cm,ncont,xg,yg)
      implicit double precision(a-h,o-z)
      real xa,ya,xb,yb,x,y
      real rl,rm
      dimension cm(2,ncont)
      icut = 0
      do 10 i=1,ncont-1
      xa = cm(1,i)-0.5
      ya = cm(2,i)+0.1
      xb = cm(1,i+1)-0.5
      yb = cm(2,i+1)+0.1
      x  = xg-0.5
      y  = yg+0.1
      rn = y*(xb-xa)-x*(yb-ya)
         if (rn.eq.0) then
c           *** Lines are parallel
            rl = 1000
            rm = 1000
         else
            rm = (x*ya-y*xa)/rn
         if (rm.lt.0.or.rm.gt.1) goto 10
            if (y.ne.0) then 
               rl = (ya + rm*(yb-ya))/y
            else
               rl = (xa + rm*(xb-xa))/x
            endif
         endif

c        call snij(xa,ya,xb,yb,x,y,rl,rm)
         if (rl.ge.0.and.rl.le.1.) icut=icut+1
10    continue
      cutit = icut
      return
      end

c *************************************************************
c *   SNIJ
c *
c *   Determine the coefficients rl and rm of the point where 
c *   the lines
c *  
c *   x1 = rl*x1p    and
c *
c *   x2 = x2org + rm*(x2p-x2org)
c *   
c *   intersect.
c *
c *   x2org  = (xa,ya)    x2p = (xb,yb)    x1p = (xg,yg)
c *   
c *   PvK 230490
c *************************************************************
      subroutine snij(xa,ya,xb,yb,xg,yg,rl,rm)
      implicit double precision(a-h,o-z)
c     real xa,ya,xb,yb,xg,yg
c     real rl,rm

      if ((yg*(xb-xa)-xg*(yb-ya)).eq.0) then
c        *** Lines are parallel
         rl = 1000
         rm = 1000
      else
         rm = (xg*ya-yg*xa)/( yg*(xb-xa)-xg*(yb-ya) )
         if (yg.ne.0) then 
            rl = ya/yg + rm*(yb-ya)/yg
         else
            rl = xa/xg + rm*(xb-xa)/xg
         endif
      endif

      return
      end
c *************************************************************
c *   CAUCHINT
c *
c *   Determine the position of the point (xg,yg) with respect to
c *   the closed contour cm by evaluating the Cauchy integral
c *
c *      /  1/(z-zg)  dz   = 0    if (xg,yg) in the contour
c *
c *   where z=(xg,yg) and the integral is taken over the contour
c *
c *   PvK 230490
c *************************************************************
      function cauchint(cm,ncont,xg,yg)
      implicit double precision(a-h,o-z)
      dimension cm(2,ncont)
      complex z1,z2,z0,valint,r1,r2

      z0 = cmplx(xg,yg)
      valint = (0.,0.)
      do 10 i=ncont,2,-1
         x1 = cm(1,i-1)
         x2 = cm(1,i)
         y1 = cm(2,i-1)
         y2 = cm(2,i)
         z1 = cmplx(x1,y1)
         z2 = cmplx(x2,y2)
         r1 = (z1-z0)*(z1-z2)
         r2 = (z2-z0)*(z1-z2)
      if (r1.eq.(0,0).or.r2.eq.(0,0)) then
         cauchint = 1
         return
         endif
         valint = valint + (0.5,0)/r1 + (0.5,0)/r2
10    continue
      if (cabs(valint).lt.0.01) then
         cauchint = 1
      else
         cauchint = 0
      endif
      return
      end
