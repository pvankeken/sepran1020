c *************************************************************
c *   c1dgaus
c *
c *   Fill values of shapefunctions and derivatives of 
c *   shapefunctions in pegauss
c *
c *************************************************************
      subroutine c1dgaus
      implicit double precision(a-h,o-z)
      parameter(done=1d0)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension fp(16)

      if (nphi.ne.12) then
	 write(6,*) 'PERROR(c1dgaus) nphi<>12 not implemented'
	 stop
      endif

      do 10 i=1,ngaus
	 x = xg(i)
	 y = yg(i)
	 call fphi(fp,x,y,done,done)
	 do 11 j=1,nphi
11          phi(j,i) = fp(j)
	 call fdpdx(fp,x,y,done,done)
	 do 12 j=1,nphi
12          dphidx(j,i) = fp(j)
         call fdpdy(fp,x,y,done,done)
	 do 13 j=1,nphi
13          dphidy(j,i) = fp(j)
	 call fd2pdx(fp,x,y,done,done)
	 do 14 j=1,nphi
14          d2phidx(j,i) = fp(j)
         call fd2pdy(fp,x,y,done,done)
	 do 15 j=1,nphi
15          d2phidy(j,i) = fp(j)
	 call fd2pxy(fp,x,y,done,done)
         do 16 j=1,nphi
16          d2phixy(j,i) = fp(j)
10    continue

      return
      end

      subroutine fphi(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1) = 1-x*y -(3-2*y)*y*y*(1-x) -(1-y)*(3-2*x)*x*x
      fp(2) = y*(1-y)*(1-y)*(1-x)*b
      fp(3) = -(1-y)*x*(1-x)*(1-x)*a
      fp(4) = (1-y)*(3-2*x)*x*x + y*(1-y)*(1-2*y)*x
      fp(5) = y*(1-y)*(1-y)*x*b
      fp(6) = (1-y)*(1-x)*x*x*a
      fp(7) = (3-2*y)*y*y*x - y*x*(1-x)*(1-2*x)
      fp(8) = -(1-y)*y*y*x*b
      fp(9) = x*x*(1-x)*y*a
      fp(10)= (3-2*y)*y*y*(1-x)+x*y*(1-x)*(1-2*x)
      fp(11)= -(1-y)*y*y*(1-x)*b
      fp(12)= -x*y*(1-x)*(1-x)*a

      return
      end

      subroutine fdpdx(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -y+(3-2*y)*y*y - (1-y)*(6*x-6*x*x)
      fp(2)  = -y*(1-y)*(1-y)*b
      fp(3)  = -(1-y)*(3*x*x-4*x+1)*a
      fp(4)  = (1-y)*(6*x-6*x*x)+y*(1-y)*(1-2*y)
      fp(5)  = y*(1-y)*(1-y)*b
      fp(6)  = (1-y)*(2*x-3*x*x)*a
      fp(7)  = (3-2*y)*y*y-y*(1-6*x+6*x*x)
      fp(8)  = -(1-y)*y*y*b
      fp(9)  = (2*x-3*x*x)*y*a
      fp(10) = -(3-2*y)*y*y + y*(1-6*x+6*x*x)
      fp(11) = (1-y)*y*y*b
      fp(12) = -y*(3*x*x-4*x+1)*a

      return
      end

      subroutine fdpdy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -x - (6*y-6*y*y)*(1-x) + (3-2*x)*x*x
      fp(2)  = (3*y*y-4*y+1)*(1-x)*b
      fp(3)  = x*(1-x)*(1-x)*a
      fp(4)  = (2*x-3)*x*x + (6*y*y-6*y+1)*x
      fp(5)  = (3*y*y-4*y+1)*x*b
      fp(6)  = (x-1)*x*x*a
      fp(7)  = (6*y-6*y*y)*x - x*(1-x)*(1-2*x)
      fp(8)  = -(2*y-3*y*y)*x*b
      fp(9)  = x*x*(1-x)*a
      fp(10) = (6*y-6*y*y)*(1-x)+x*(1-x)*(1-2*x)
      fp(11) = -(2*y-3*y*y)*(1-x)*b
      fp(12) = -x*(1-x)*(1-x)*a

      return
      end

      subroutine fd2pdx(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = -(1-y)*(6-12*x)
      fp(2)  = 0.
      fp(3)  = -(1-y)*(6*x-4)*a
      fp(4)  = (1-y)*(6-12*x)
      fp(5)  = 0.
      fp(6)  = (1-y)*(2-6*x)*a
      fp(7)  = -y*(12*x-6)
      fp(8)  = 0.
      fp(9)  = (2-6*x)*y*a
      fp(10) = y*(12*x-6)
      fp(11) = 0.
      fp(12) = -y*(6*x-4)*a

      return
      end

      subroutine fd2pdy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = (12*y-6)*(1-x)
      fp(2)  = (6*y-4)*(1-x)*b
      fp(3)  = 0.
      fp(4)  = (12*y-6)*x
      fp(5)  = (6*y-4)*x*b
      fp(6)  = 0.
      fp(7)  = (6-12*y)*x
      fp(8)  = (6*y-2)*x*b
      fp(9)  = 0.
      fp(10) = (6-12*y)*(1-x)
      fp(11) = (6*y-2)*(1-x)*b
      fp(12) = 0.

      return
      end

      subroutine fd2pxy(fp,x,y,a,b)
      implicit double precision(a-h,o-z)
      dimension fp(12)

      fp(1)  = 6*y-6*y*y+6*x-6*x*x-1
      fp(2)  = -(3*y*y-4*y+1)*b
      fp(3)  = (3*x*x-4*x+1)*a
      fp(4)  = 6*y*y-6*y+6*x*x-6*x+1
      fp(5)  = (3*y*y-4*y+1)*b
      fp(6)  = (3*x*x-2*x)*a
      fp(7)  = 6*y-6*y*y+6*x-6*x*x-1
      fp(8)  = (3*y*y-2*y)*b
      fp(9)  = (2*x-3*x*x)*a
      fp(10) = 6*y*y-6*y+6*x*x-6*x+1
      fp(11) = (2*y-3*y*y)*b
      fp(12) = -(3*x*x-4*x+1)*a

      return
      end
