c *************************************************************
c *   C1DETVEL
c *
c *   PvK 040490
c *************************************************************
      subroutine c1detvel(kmesh,kprob,user,imark,coormark,velmark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),user(*),imark(*),coormark(*)
      dimension velmark(*),up(5)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      common /pelinqua/ itypel,ishape

      call ini050(kmesh(23),'detvel: coordinates')
      call ini050(kmesh(17),'detvel: nodalpoints')
      npoint = kmesh(8)
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))
 
      nmark = imark(1)
      do 10 i=1,nmark
         xm = coormark(2*i-1)
         ym = coormark(2*i)
	 call pedetel(2,xm,ym,iel)
	 call peint04(ibuffr(ikelmc),ibuffr(ikelmi),user,xm,ym,
     v                iel,up,npoint)
         velmark(2*i-1) = up(1)
         velmark(2*i)   = up(2)
10    continue     
      return
      end

c *************************************************************
c *   PEINT04
c *
c *   Find interpolated velocity in point (xm,ym) which lies in 
c *   element IEL. The nonconforming element is used; the velocity
c *   is part of the solution
c *  
c *   PvK 040490
c *************************************************************
      subroutine peint04(kmeshc,coor,user,xm,ym,iel,up,npoint)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),user(*),up(*)
      dimension nodno(4),x(4),y(4),psi(12)
      dimension fp(12),fpx(12),fpy(12)

c     *** Nodalpoint numbers are given in kmeshc
      inpelm = 4
      ip = (iel-1)*inpelm
      do 10 i=1,inpelm
10       nodno(i) = kmeshc(ip+i)
c     *** Coordinates in coor
      do 20 i=1,inpelm
         x(i) = coor(1,nodno(i))
20       y(i) = coor(2,nodno(i))
c     *** Solution in user
      do 30 i=1,4
	 ioff = (i-1)*3
	 ip   = nodno(i)
	 i1   = 5 + ip
         i2   = 5 + ip + npoint
	 i3   = 5 + ip + 2*npoint
         psi(ioff+1) = user(i1)
	 psi(ioff+2) = user(i2)
	 psi(ioff+3) = user(i3)
30    continue
      a = x(2)-x(1)
      b = y(4)-y(1)
      xi  = (xm-x(1))/a
      eta = (ym-y(1))/b
      call fphi(fp,xi,eta,a,b)
      call fdpdx(fpx,xi,eta,a,b)
      call fdpdy(fpy,xi,eta,a,b)
      psint = 0.
      uint = 0.
      vint = 0.
      do 40 i=1,12
	 psint = psint + psi(i) * fp(i)
	 uint  = uint  + psi(i) * fpy(i) / b
	 vint  = vint  - psi(i) * fpx(i) / a
40    continue
c     *** Bilinear interpolation of velocity
      u2 = psi(2)*(1-xi)*(1-eta) + psi(5)*xi*(1-eta)
      u2 = u2 + psi(8)*xi*eta + psi(11)*(1-xi)*eta
      v2 = psi(3)*(1-xi)*(1-eta) + psi(6)*xi*(1-eta)
      v2 = v2 + psi(9)*xi*eta + psi(12)*(1-xi)*eta

      up(1) = uint
      up(2) = vint
      up(3) = psint
      up(4) = u2
      up(5) = v2
      return
      end
