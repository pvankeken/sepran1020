c *************************************************************
c *   C1BIHI
c * 
c *   PvK 050490
c *************************************************************
      subroutine c1bihi(elemmt,a,b,visco)
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension elemmt(*)
      dimension f1(12)
     
      do 10 i=1,10,3
         f1(i)   = 1d0
	 f1(i+1) = b
         f1(i+2) = a
10    continue
      fa1 = 1d0/(a*a*a*a)
      fa2 = -1d0/(a*a*b*b)
      fa3 = fa2
      fa4 = 1d0/(b*b*b*b)
      fa5 = -4*fa2
      if (nrule.gt.0) then
	 do 20 j=1,nphi
	    do 20 i=j,nphi
	       ioff = (j-1)*nphi+i
	       sum1 = 0.
	       sum2 = 0.
	       sum3 = 0.
	       sum4 = 0.
	       sum5 = 0.
	       do 30 k=1,ngaus
		  sum1 = sum1 + w(k)*d2phidx(i,k)*d2phidx(j,k)
		  sum2 = sum2 + w(k)*d2phidy(i,k)*d2phidx(j,k)
		  sum3 = sum3 + w(k)*d2phidx(i,k)*d2phidy(j,k)
		  sum4 = sum4 + w(k)*d2phidy(i,k)*d2phidy(j,k)
		  sum5 = sum5 + w(k)*d2phixy(i,k)*d2phixy(j,k)
30             continue 
	       sum = fa1*sum1+fa2*sum2+fa3*sum3+fa4*sum4+fa5*sum5
               elemmt(ioff) = visco*a*b*sum*f1(i)*f1(j)
	       iof2 = (i-1)*nphi+j
	       if (i.gt.j) elemmt(iof2) = elemmt(ioff)
20       continue
      endif

      return
      end

