c *************************************************************
c *   C1START
c *
c *   Starts C1MAR
c *
c *   PvK 040490
c *************************************************************
      subroutine c1start(kmesh,kprob,istrm,intmat,imark,
     v                   coormark,y0,dm,nbufdef)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),imark(*)
      dimension coormark(*),iu1(1),u1(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peinterg/ nrule
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pelinqua/ itypel,ishape
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      character*80 fnre

      read(5,*) itypel,nrule
      read(5,*) nmark,y0,wavel,ainit,dm
      read(5,*) nitermax,nout,ncor
      read(5,*) tfac,tmax,tstepmax,tvalid
      read(5,*) viscl(1),viscl(2),ctd,cpd,itypv
      read(5,*) irestart,trestart,fnre
      nl = 2
c     *** standard Sepran start
      call start(0,1,0,0)
      nbuffr = nbufdef
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call presdf(kmesh,kprob,istrm)
      call commat(1,kmesh,kprob,intmat)
      nsuper = kprob(29)
      write(6,'(''nsuper = '',i2)') nsuper
      if (nsuper.gt.0) stop

c     *** initial condition for markerchain and streamfunction
      iu1(1) = 0
       u1(1) = 0
      call creavc(0,1,ivec,istrm,kmesh,kprob,iu1,u1,iu2,u2)
      if (itypel.eq.5) then
c        *** rectangular element with four nodalpoints
	 ishape = 1
      endif
      call pefilxy(ishape,kmesh,kprob,istrm)

      if (irestart.eq.0) then
	 t = 0d0
         rm1 = (xcmax-xcmin)/(nmark-1)
         piw = 3.1415926/wavel
         do 20 i=1,nmark
            x = rm1*(nmark-i)
            y = y0 + ainit*cos(piw*x)
            coormark(2*i-1) = x
            coormark(2*i)   = y
20       continue
         imark(1) = nmark
      else
	 open(9,file=fnre)
	 rewind(9)
	 read(9,*) itype,imark(1)
	 do 30 i=1,imark(1)
30          read(9,*) coormark(2*i-1),coormark(2*i)
	 t = trestart
      endif

      return
      end
