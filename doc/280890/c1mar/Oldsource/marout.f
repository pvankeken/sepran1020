c *************************************************************
c *   MAROUT
c *
c *   Post processing of solution vectors obtained by STRMAR
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marout(kmesh,kprob,istrm,imark,
     v                  coormark,iuser,user,velmark)
      parameter(NUM=2000)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),imark(*),coormark(*)
      dimension iuser(*),user(*),xmc(NUM),ymc(NUM),velmark(*)
      character pname*80,chrbf*4
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax

      nmark = imark(1)
      if (nmark.gt.NUM) then
         write(6,*) 'PERROR(marout): nmark too large'
         stop
      endif
 
c     *** Plot preparations
      do 10 i=1,nmark
         xmc(i) = coormark(2*i-1)
10       ymc(i) = coormark(2*i)
      iway=2
      fname='PLOT'
      yfaccn = 1
      jkader=-4
      format=6d0
      chheight = 0.3d0
c     *** Plot streamfunction and markers
c     write(tittext,'(''S: t= '',f12.3)') t
c     call plwin(1,0d0,0d0)
c     call peframe(1)
c     call plotc1(1,kmesh,kprob,istrm,contln,0,format,yfaccn,jsmoot)
c     call peframe(3)
c     call pecurvep(nmark,xmc,ymc,format,yfaccn)
c     call plwin(3,format+1d0,0d0)
      call frstack(0)
      write(tittext,'(''T: t= '',f12.3)') t
      call peframe(1)
      call plotvc(2,3,istrm,istrm,kmesh,kprob,format,yfaccn,0d0)
      call peframe(3)
      call pecurvep(nmark,xmc,ymc,format,yfaccn)

      write(6,1000) velxmax,velymax,strmax
1000  format(/,'vxmax = ',f15.7,/,'vymax = ',f15.7,/,
     v         'strmax= ',f15.7)
 
      write(chrbf,'(i4)') 1000+ifrcntr
      pname='p.'//chrbf(2:4)
      open(9,file=pname)
      rewind(9)
      write(9,*) 2,imark(1)
      do 20 i=1,nmark
20       write(9,*) coormark(2*i-1),coormark(2*i)
      pname = 'v.'//chrbf(2:4)
      open(9,file=pname)
      rewind(9)
      write(9,*) 2,imark(1)
      do 30 i=1,imark(1)
30       write(9,*) velmark(2*i-1),velmark(2*i)
      close(9)
      
      
      return
      end
