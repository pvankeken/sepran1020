c *************************************************************
c *   C1SOLMAR
c *
c *   Solves the biharmonical equation
c *
c *          4
c *      grad  psi  =   -dGAMMA/dx
c *
c *   where GAMMA is a stepfunction, of which the position is 
c *   defined by a markerchain.
c *   THE nonconforming element is used
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for psi
c *       matr    o  Stiffness matrix (constant in this problem)
c *       imark   i  Integer information on markerchain
c *       coormark i Coordinates of markers (x1,y1,x2,y2,....)
c *
c *   PvK 040490
c *************************************************************
      subroutine c1solmar(kmesh,kprob,istrm,intmat,matr,iuser,user,
     v                  imark,coormark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),matr(*)
      dimension iuser(*),user(*),imark(*),coormark(*)
      dimension irhsd(5),ius(20),us(10)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      save ifirst
      data ifirst/0/

      nmark = imark(1)
      if (nmark+7.gt.user(1)) then
         write(6,*) 'PERROR(c1solmar): array USER too small'
         stop
      endif

c     *** Calculate rhs vector
c     ***   fill positions of markers in array USER
c     ***   fill elementnumber for each marker in IUSER
      nmark = imark(1)
      icrhsd = 1
      iuser(6) = icrhsd
      iuser(7) = nmark
      do 10 i=1,2*nmark
10       user(5+i) = coormark(i)
c     **** Close contour; we will need this for the determination of
c     **** viscosity in the Gausspoints
      ip = 5+2*nmark
      user(ip+1) = coormark(2*nmark-1)
      user(ip+2) = 1d0
      user(ip+3) = coormark(1)
      user(ip+4) = 1d0
      user(ip+5) = coormark(1)
      user(ip+6) = coormark(2)
      ncont = nmark + 3
c     *** determine the numbers of the elements in which the marker are
      do 20 i=1,nmark
         xm = coormark(2*i-1)
         ym = coormark(2*i)
         call pedetel(2,xm,ym,iel)
         iuser(7+i) = iel
20    continue
      iuser(8+nmark) = ncont

      if (ifirst.eq.0.or.itypv.ne.0) then
c        *** Built matrix 
         ifirst = 1
         call systm0(13,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v               user,islold,ielhlp)
      endif

      call systm0(2,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v            user,islold,ielhlp)
      call solve(1,matr,istrm,irhsd,intmat,kprob)

      factor = 1d0
      npoint = kmesh(8)
      call pecopy(10,istrm,user,kprob,6,factor)
      call pecopy(11,istrm,user,kprob,6+npoint,factor)
      call pecopy(12,istrm,user,kprob,6+2*npoint,factor)
      return
      end
