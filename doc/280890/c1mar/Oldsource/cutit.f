      subroutine cutit(cm,ncont,xg,yg)
      implicit double precision(a-h,o-z)
      dimension cm(2,ncont)
      icut = 0
      do 10 i=1,ncont-1
	 xa = cm(1,i)
	 ya = cm(2,i)
	 xb = cm(1,i+1)
	 yb = cm(2,i+1)
         call snij(xa,ya,xb,yb,xg,yg,rl,rm)
         if (rl.ge.0.and.rl.le.1.and.rm.ge.0.and.rm.le.1) icut=icut+1
10    continue
      write(6,'(2f12.3,i4)') xg,yg,icut
      return
      end

        subroutine snij(xa,ya,xb,yb,xg,yg,rl,rm)
	implicit double precision(a-h,o-z)

	if ((yg*(xb-xa)-xg*(yb-ya)).eq.0) then
	   rl = 1000
	   rm = 1000
        else
           rm = (xg*ya-yg*xa)/( yg*(xb-xa)-xg*(yb-ya) )
           if (yg.ne.0) then 
	      rl = ya/yg + rm*(yb-ya)/yg
	   else
              rl = xa/xg + rm*(xb-xa)/xg
           endif
        endif

	return
	end
