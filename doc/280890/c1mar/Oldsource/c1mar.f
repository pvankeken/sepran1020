      program c1mar
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF= 3 000 000)
c **********************************************************
c *   C1MAR
c *
c *   Solve the equation of motion for a RT instability modelled
c *   with the markerchain method, in the streamfunction 
c *   formulation using THE C1 nonconforming element.
c *
c *   PvK 040490
c **********************************************************************
      parameter(NUM1=2010,NUM2=16005,NUM3=NUM1-10)
      common ibuffr(NBUFDEF)
      dimension kmesh(100),kprob(100),istrm(5)
      dimension intmat(5),isol(5),islold(5),irhsd(5),matr(5)
      dimension iu1(2),u1(2),ivec1(5),ivec2(5),isolan(5)
      dimension icurvs(2),funcx(100),funcy(100)
      dimension iuser(NUM1),user(NUM2)
      dimension imark(1),coormark(2,NUM3),coornewm(2,NUM3)
      dimension velmark(2,NUM3),velnewm(2,NUM3),cooroldm(2,NUM3)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      real t0,t1,t2
      data iuser(1),user(1),kmesh(1),kprob(1)/NUM1,NUM2,2*100/

      call c1start(kmesh,kprob,istrm,intmat,imark,
     v             coormark,y0,dm,NBUFDEF)
      nmark = imark(1)
      niter = 0
      inout = 0
      call second(t0)
      open(11,file='c1mar.data')
      rewind(11)
100   continue
	 niter = niter+1
	 inout = inout+1
	 call second(t1)

c        ************** PREDICTOR ***************
	 call c1solmar(kmesh,kprob,istrm,intmat,matr,iuser,user,imark,
     v                coormark)
	 call second(t2)
	 call c1detvel(kmesh,kprob,user,imark,coormark,velmark)
	 rk = velmark(2,nmark)/(coormark(2,nmark)-y0)
	 cpu1 = t2-t1
         call pedetcfl(imark,velmark)
         tstep = dtcfl*tfac
	 if (niter.eq.1) write(6,'(''rk,cpu '',f12.9,f12.2)') rk,cpu1
	 if (tstep.gt.tstepmax.and.t.lt.tvalid) tstep=tstepmax
         call predcoor(coormark,velmark,coornewm,imark)

c        ************** CORRECTOR ****************
         do 10 i=1,ncor
           call c1solmar(kmesh,kprob,istrm,intmat,matr,iuser,user,imark,
     v                   coornewm)
           call c1detvel(kmesh,kprob,user,imark,coornewm,velnewm)
           call copmar(imark,coornewm,cooroldm)
           call corcoor(imark,coormark,coornewm,velmark,velnewm)
	   difcor1=accmar(1,imark,coornewm,cooroldm)
	   difcor2=accmar(2,imark,coornewm,cooroldm)
           write(6,11) t+tstep,ncor,difcor1,difcor2
11         format('corrector: ',f12.2,i3,2f12.7)
10       continue
	 call copmar(imark,coornewm,coormark)

	 if ((niter/5)*5.eq.niter) then
           call remarker(imark,coormark,coornewm,dm,NUM3)
           call copmar(imark,coornewm,coormark)
	 endif
	 call second(t2)
	 dcpu = t2-t1
	 t = t+tstep
	 nmark=imark(1)
	 h = coormark(2,nmark)
	 write(11,'(i3,f12.2,f12.7,f12.7,f12.2)') niter,t,h-y0,rk,dcpu
	 call flush(11)

	 if (inout.eq.nout) then
	    inout = 0
	    call marout(kmesh,kprob,istrm,imark,coormark,
     v                  iuser,user,velnewm)
         endif
	 if (niter.lt.nitermax.and.t.lt.tmax) goto 100
	 write(6,'(''Total CPU-time: '',f12.2)') t2-t0
      close(11)
      call frstack(1)
      call finish(0)
      end
