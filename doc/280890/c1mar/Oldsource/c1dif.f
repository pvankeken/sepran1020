c *************************************************************
c *   C1DIF
c * 
c *************************************************************
      subroutine c1dif(elemmt,a,b,alpha)
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension elemmt(*)
      dimension f1(12)
     
      do 10 i=1,10,3
         f1(i)   = 1d0
	 f1(i+1) = b
         f1(i+2) = a
10    continue
      ab = a/b*alpha
      ba = b/a*alpha
      if (nrule.gt.0) then
	 do 20 j=1,nphi
	    do 20 i=j,nphi
	       ioff = (j-1)*nphi+i
	       sumx = 0.
	       sumy = 0.
	       do 30 k=1,ngaus
		  sumx = sumx + w(k)*dphidx(i,k)*dphidx(j,k)
30                sumy = sumy + w(k)*dphidy(i,k)*dphidy(j,k)
               elemmt(ioff) = (ab*sumx + ba*sumy)*f1(i)*f1(j)
	       iof2 = (i-1)*nphi+j
	       if (i.gt.j) elemmt(iof2) = elemmt(ioff)
20       continue
      endif

      return
      end

