c *************************************************************
c *   COPMAR
c *
c *   PvK 231189
c *************************************************************
      subroutine copmar(imark,coor1,coor2)
      implicit double precision(a-h,o-z)
      dimension imark(*),coor1(2,*),coor2(2,*)
      do 10 i=1,imark(1)
         coor2(1,i) = coor1(1,i)
10       coor2(2,i) = coor1(2,i)
      return
      end
