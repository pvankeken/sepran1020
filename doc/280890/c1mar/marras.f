c *************************************************************
c *   MARRAS
c *
c *   Create a rasterfile in which the value of the pixels 
c *   indicate the layer in which it is positioned.
c *   The layerboundaries are represented by the boundaries
c *   of the geometry ([0,rlam]x[0,1]) and the markerchain
c *   (common c1mark)
c *   The algorithm is described in PVK 040690
c *
c *   PvK 050690
c *************************************************************
      subroutine marras(coormark,rname,iout)
      implicit double precision (a-h,o-z)
      dimension coormark(*),icy(500)
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ nochain,ichain,imark(10)
      character*80 rname
      
      if (nxpix.eq.0) return
      if (nochain.gt.1) then
         write(6,*) 'PERROR(marras): nochain > 1 not yet implemented'
         stop
      endif
      rlam =  coormark(1)
      nmark = imark(1)
c     *** Loop over columns
      do 20 ix=1,nxpix
c        *** Determine in which pixels the markerchain intersects the column
c        *** Start with upper boundary (y=1,iy=1) and finish with the 
c        *** lower one (y=0,iy=nypix) 
c        ***(because of the fantastic choice of the imagetools origin)
         xm = (0.5+(ix-1))*rlam/nxpix
         icut=1
         icy(1)=1
         do 30 im=2,nmark
            x1 = coormark(2*im-3) 
            x2 = coormark(2*im-1) 
            if (x1.le.xm.and.x2.gt.xm.or.x1.ge.xm.and.x2.lt.xm) then
               y1 = coormark(2*im-2)
               y2 = coormark(2*im)
               ym = y1+(xm-x1)/(x2-x1)*(y2-y1)
               icut = icut+1
               icy(icut) = 1+(1-ym)*(nypix-1)
            endif
30       continue
         icut=icut+1
         icy(icut)=nypix
c        *** sort this array
         do 40 i=2,icut-1
            do 40 j=3,icut-1
               if (icy(j-1).gt.icy(j)) then
                  it = icy(j-1)
                  icy(j-1) = icy(j)
                  icy(j) = it
               endif
40          continue
c        *** Fill the intermediate positions; the first,
c        *** upper layer has value lu.
         do 50 iregio=1,icut-1,2
            do 60 iy=icy(iregio),icy(iregio+1)
               ip = (iy-1)*nxpix + ix
60             ipix(ip)=2
            do 70 iy=icy(iregio+1),icy(iregio+2)
               ip = (iy-1)*nxpix + ix
70             ipix(ip)=1
50       continue
20    continue

      if (iout.eq.-1) then
         ntot = nxpix*nypix
c        *** Represents the layers 1,2,3 ... with colors 6,7,8 ...
c        *** (Sun reserves the first four colorindices)
         do 100 i=1,ntot
100         ipix(ip) = ipix(ip)+5
         call rasout(ntot,ipix,rname)
      endif
       
      end
