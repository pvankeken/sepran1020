c *************************************************************
c *   C1START
c *
c *   Starts C1MAR
c *
c *   PvK 040490
c *************************************************************
      subroutine c1start(kmesh,kprob1,isol1,intmt1,kprob2,
     v                   isol2,intmt2,coormark,y0,dm,nbufdef)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob1(*),isol1(*),intmt1(*)
      dimension kprob2(*),isol2(*),intmt2(*),y0(*),dm(*)
      dimension coormark(*),iu1(3),u1(3)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peinterg/ nrule
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pelinqua/ itypel,ishape
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ nochain,ichain,imark(10)
      character*80 fnre

      read(5,*) itypel,nrule
      read(5,*) nmark,y0(1),wavel,ainit,dm(1)
      read(5,*) nitermax,nout,ncor
      read(5,*) tfac,tmax,tstepmax,tvalid
      read(5,*) viscl(1),viscl(2),ctd,cpd,itypv
      read(5,*) irestart,trestart,fnre
      read(5,*) nxpix,nypix
      nl = 2
c     *** standard Sepran start
      call start(0,1,1,0)
      nbuffr = nbufdef
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob1,kmesh,iinput)
      call probdf(0,kprob2,kmesh,iinput)
      call presdf(kmesh,kprob1,isol1)
      call presdf(kmesh,kprob2,isol2)
      call commat(1,kmesh,kprob1,intmt1)
      call commat(2,kmesh,kprob2,intmt2)
      nsup1 = kprob1(29)
      nsup2 = kprob2(29)
      write(6,'(''nsuper = '',2i3)') nsup1,nsup2
      if (nsup1.gt.0.or.nsup2.gt.0) stop

c     *** initial condition for markerchain and streamfunction
c     *** you'd better put all three dof's to zero!!!!!
      do 15 i=1,3
         iu1(i) = 0
         u1(i) = 0
15    continue
      call creavc(0,1,ivec,isol1,kmesh,kprob1,iu1,u1,iu2,u2)
c     *** initial condition for the temperature
      iu1(1) = 1
      call creavc(0,1,ivec,isol2,kmesh,kprob2,iu1,u1,iu2,u2)
      if (itypel.eq.5) then
c        *** rectangular element with four nodalpoints
	 ishape = 1
      endif
c     *** Determine aspect ratio
      call pefilxy(ishape,kmesh,kprob1,isol1)
      rlam = xcmax-xcmin
      imark(1) = nmark
         
      if (irestart.eq.0) then
	 t = 0d0
         rm1 = (xcmax-xcmin)/(nmark-1)
         piw = 3.1415926/wavel
         do 20 i=1,nmark
            x = rm1*(nmark-i)
            y = y0(1) + ainit*cos(piw*x)
            coormark(2*i-1) = x
            coormark(2*i)   = y
20       continue
         imark(1) = nmark
      else
	 open(9,file=fnre)
	 rewind(9)
	 read(9,*) itype,imark(1)
	 do 30 i=1,imark(1)
30          read(9,*) coormark(2*i-1),coormark(2*i)
	 t = trestart
      endif

      return
      end
