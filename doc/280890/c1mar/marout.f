c *************************************************************
c *   MAROUT
c *
c *   Post processing of solution vectors obtained by STRMAR
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marout(kmesh,kprob,istrm,coormark,iuser,user,
     v                  velmark,iplot)
      parameter(NUM=2000)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),coormark(*)
      dimension iuser(*),user(*),xmc(NUM),ymc(NUM),velmark(*)
      character pname*80,chrbf*4
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /c1mark/ nochain,ichain,imark(10)
      save ifrno
      data ifrno/1/

      nmark = imark(1)
      if (nmark.gt.NUM) then
         write(6,*) 'PERROR(marout): nmark too large'
         stop
      endif
 
c     *** Plot preparations
      if (iplot.eq.1) then
        do 10 i=1,nmark
           xmc(i) = coormark(2*i-1)
10         ymc(i) = coormark(2*i)
        iway=2
        fname='PLOT'
        yfaccn = 1
        jkader=-4
        format=6d0
        chheight = 0.3d0
c       *** Plot streamfunction and markers
        write(tittext,'(''S: t= '',f12.3)') t
        call plwin(1,0d0,0d0)
        call peframe(1)
        call plotc1(1,kmesh,kprob,istrm,contln,0,format,yfaccn,jsmoot)
        call peframe(3)
        call pecurvep(nmark,xmc,ymc,format,yfaccn)
        call plwin(3,format+1d0,0d0)
        write(tittext,'(''T: t= '',f12.3)') t
        call peframe(1)
        call plotvc(2,3,istrm,istrm,kmesh,kprob,format,yfaccn,0d0)
        call peframe(3)
        call pecurvep(nmark,xmc,ymc,format,yfaccn)
        call plot(0.,0.,999)
      endif

      write(chrbf,'(i4)') 1000+ifrno
      ifrno = ifrno+1
      pname='p.'//chrbf(2:4)
      open(9,file=pname)
      rewind(9)
      write(9,*) 2,imark(1)
      do 20 i=1,nmark
20       write(9,*) coormark(2*i-1),coormark(2*i)
      
      
      return
      end
