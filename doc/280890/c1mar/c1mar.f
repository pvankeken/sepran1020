      program c1mar
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF= 3 000 000)
c **********************************************************
c *   C1MAR
c *
c *   Solve the equation of motion for a RT instability modelled
c *   with the markerchain method, in the streamfunction 
c *   formulation using THE C1 nonconforming element.
c *
c *   PvK 040490
c **********************************************************************
      parameter(NUM1=2010,NUM2=16005,NUM3=NUM1-10)
      common ibuffr(NBUFDEF)
      dimension kmesh(100),kprob1(100),isol1(5),intmt1(5)
      dimension kprob2(100),intmt2(5),isol2(5)
      dimension islold(5),irhs2(5),irhs1(5),matr1(5),matr2(5)
      dimension iu1(2),u1(2),ivec1(5),ivec2(5),isolan(5)
      dimension icurvs(2),funcx(100),funcy(100)
      dimension iuser(NUM1),user(NUM2)
      dimension coormark(2*NUM3),coornewm(2*NUM3)
      dimension velmark(2*NUM3),velnewm(2*NUM3),cooroldm(2*NUM3)
      dimension y0(10),dm(10)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ nochain,ichain,imark(10)
      character*80 rname
      data iuser(1),user(1),kmesh(1),kprob1(1)/NUM1,NUM2,2*100/
      data kprob2(1)/100/
      real t0,t1,t2
      call c1start(kmesh,kprob1,isol1,intmt1,kprob2,isol2,intmt2,
     v             coormark,y0,dm,NBUFDEF)
      nmark = imark(1)
      niter = 0
      inout = 0
      call second(t0)
      open(11,file='c1mar.data')
      rewind(11)
100   continue
	 niter = niter+1
	 inout = inout+1
	 call second(t1)

c        ************** PREDICTOR ***************
         call prepvis(kmesh,kprob1,isol1,kmesh2,kprob2,isol2,coormark,
     v                iuser,user)
	 call c1solmar(kmesh,kprob1,isol1,intmt1,matr1,iuser,user,
     v                coormark)
	 call second(t2)
	 call c1detvel(kmesh,kprob1,user,coormark,velmark)
	 rk = velmark(2*nmark)/(coormark(2*nmark)-y0(1))
	 cpu1 = t2-t1
         call pedetcfl(velmark)
         tstep = dtcfl*tfac
	 if (niter.eq.1) write(6,'(''rk,cpu '',f12.9,f12.2)') rk,cpu1
	 if (tstep.gt.tstepmax.and.t.lt.tvalid) tstep=tstepmax
         call predcoor(coormark,velmark,coornewm)

c        ************** CORRECTOR ****************
         do 10 i=1,ncor
           call prepvis(kmesh,kprob1,isol1,kmesh2,kprob2,isol2,coormark,
     v                  iuser,user)
           call c1solmar(kmesh,kprob1,isol1,intmt1,matr1,iuser,user,
     v                   coornewm)
           call c1detvel(kmesh,kprob1,user,coornewm,velnewm)
           call copmar(coornewm,cooroldm)
           call corcoor(coormark,coornewm,velmark,velnewm)
	   difcor1=accmar(1,coornewm,cooroldm)
	   difcor2=accmar(2,coornewm,cooroldm)
           write(6,11) t+tstep,ncor,difcor1,difcor2
11         format('corrector: ',f12.2,i3,2f12.7)
10       continue
	 call copmar(coornewm,coormark)

	 if ((niter/5)*5.eq.niter) then
           call remarker(coormark,coornewm,dm,NUM3)
           call copmar(coornewm,coormark)
	 endif
	 call second(t2)
	 dcpu = t2-t1
	 t = t+tstep
	 h = coormark(2*nmark)
	 write(11,'(i3,f12.2,f12.7,f12.7,f12.2)') niter,t,h-y0(1),rk,dcpu

         if (inout.eq.nout) then
            iplot=1
            inout=0
         else
            iplot=0
         endif
         call marout(kmesh,kprob1,isol1,coormark,
     v                  iuser,user,velnewm,iplot)
	 if (niter.lt.nitermax.and.t.lt.tmax) goto 100
	 write(6,'(''Total CPU-time: '',f12.2)') t2-t0
      close(11)
      call frstack(1)
      call finish(0)
      end

