c *************************************************************
c *   PREPVIS
c *
c *   Prepare information for determination of viscosity in the
c *   Gauss points. Depending on the type of viscosity create
c *   a pixelarray with layerdependent information, or store
c *   information on temperature and velocity in the nodalpoints
c *   in array USER. Common block c1visc is used: 
c *
c *   common /c1visc/ viscl(10),ctd,cpd,itypv,nl
c *
c *   itypv      The viscosity is calculated by a combination of the
c *              following choices:
c *              0     constant viscosity (== 1)
c *              1     viscosity is layerdependent: 
c *                    viscosity in layer i is viscl(i)
c *              2     viscosity depends on temperature and pressure
c *                    according to visc = exp(ctd*T+cpd*z)
c *              4     Non-newtonian viscosity
c *
c *   user       Array to transport information to the elementroutines
c *              the contents depend on itypv. If the viscosity is 
c *              temperature dependent then user(6)..user(5+npoint)
c *              are filled with the temperature in the nodalpoints. 
c *
c *  common /c1mark/ nochain,ichain,imark(10)
c * 
c *  nochain     Number of markerchains
c *  imark       Array containing the number of nodalpoint on each 
c *              markerchain
c *
c *  common /vislo/ ivl,ivt,ivn
c *  logical ivl,ivt,ivn
c *
c *  ivl         true if viscosity is layerdepenent
c *  ivt         true if viscosity is temperature dependent
c *  ivn         true if viscosity is nonNewtonian
c *  PvK 120690
c *************************************************************
      subroutine prepvis(kmesh,kprob1,isol1,kmesh2,kprob2,isol2,
     v                   coormark,iuser,user)
      implicit double precision(a-h,o-z) 
      dimension kmesh(*),kprob1(*),isol1(*),kmesh2(*),kprob2(*)
      dimension coormark(*),iuser(*),user(*),isol2(*)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ nochain,ichain,imark(10)
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
      character*80 named

      ivt = .false.
      ivl = .false.
      ivn = .false. 

      if (itypv.eq.2.or.itypv.eq.3.or.itypv.eq.6.or.itypv.eq.7) then
c        *** viscosity is temperature dependent
         ivt = .true.
         factor = 1d0
         call pecopy(0,isol2,user,kprob2,6,factor)
      endif
      if (itypv.eq.1.or.itypv.eq.3.or.itypv.eq.5.or.itypv.eq.7) then
c        *** viscosity is layer dependent: fill pixelarray
         ivl = .true.
         call marras(coormark,named,1)
      endif
      if (itypv.ge.4) then
c        *** viscosity is non Newtonian
         ivn = .true.
      endif
      return
      end
