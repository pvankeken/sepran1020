c *************************************************************
c *   C1SOLMAR
c *
c *   Solves the biharmonical equation
c *
c *          4
c *      grad  psi  =   -dGAMMA/dx
c *
c *   where GAMMA is a stepfunction, of which the position is 
c *   defined by a markerchain.
c *   THE nonconforming element is used
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for psi
c *       matr    o  Stiffness matrix (constant in this problem)
c *       imark   i  Integer information on markerchain
c *       coormark i Coordinates of markers (x1,y1,x2,y2,....)
c *
c *   PvK 040490
c *************************************************************
      subroutine c1solmar(kmesh,kprob,istrm,intmat,matr,iuser,user,
     v                  coormark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),matr(*)
      dimension iuser(*),user(*),coormark(*)
      dimension irhsd(5),ius(20),us(10)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ nochain,ichain,imark(10)
      save ifirst
      data ifirst/0/

      if (nochain.gt.1) then
         write(6,*) 'PERROR(c1solmar): nochain>1 not yet implemented'
      endif
      nmark = imark(1)
      npoint = kmesh(8)
      if (npoint+2*nmark+7.gt.user(1)) then
         write(6,*) 'PERROR(c1solmar): array USER too small'
         stop
      endif

c     *** Calculate rhs vector
c     ***   Fill positions of markers in array USER, starting at
c     ***   6+npoint. Fill elementnumber for each marker in IUSER
c     ***   starting at 6.
      icrhsd = 1
      iuser(6) = icrhsd
      do 10 i=1,2*nmark
10       user(5+i+npoint) = coormark(i)
c     *** determine the numbers of the elements in which the marker are
      do 20 i=1,nmark
         xm = coormark(2*i-1)
         ym = coormark(2*i)
         call pedetel(2,xm,ym,iel)
         iuser(6+i) = iel
20    continue

      d1max = anorm(1,3,1,kmesh,kprob,istrm,istrm,ielhlp)
      d2max = anorm(1,3,2,kmesh,kprob,istrm,istrm,ielhlp)
      d3max = anorm(1,3,3,kmesh,kprob,istrm,istrm,ielhlp)
      write(6,*) 'd1,d2,d3 : ',d1max,d2max,d3max
      if (ifirst.eq.0.or.itypv.ne.0) then
c        *** Built matrix 
         ifirst = 1
         call copyvc(istrm,islold)
         open(93,file='visco.gaus')
         rewind(93) 
         call systm0(-13,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v               user,islold,ielhlp)
         close(93)
      endif

      call systm0(2,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v            user,islold,ielhlp)
      rhmax = anorm(1,3,1,kmesh,kprob,irhsd,irhsd,ielhlp)
      write(6,*) 'rhmax : ',rhmax
      call solve(1,matr,istrm,irhsd,intmat,kprob)
      d1max = anorm(1,3,1,kmesh,kprob,istrm,istrm,ielhlp)
      d2max = anorm(1,3,2,kmesh,kprob,istrm,istrm,ielhlp)
      d3max = anorm(1,3,3,kmesh,kprob,istrm,istrm,ielhlp)
      write(6,*) 'd1,d2,d3 : ',d1max,d2max,d3max

      factor = 1d0
      npoint = kmesh(8)
      call pecopy(10,istrm,user,kprob,6,factor)
      call pecopy(11,istrm,user,kprob,6+npoint,factor)
      call pecopy(12,istrm,user,kprob,6+2*npoint,factor)
      return
      end
