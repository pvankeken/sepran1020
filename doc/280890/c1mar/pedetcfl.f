c *************************************************************
c *   PEDETCFL
c *
c *   Determine value of maximum timestep according to the
c *   CFL-criterium. 
c *
c *   dt(cfl) = min(dx/vx,dy/vy)
c *
c *************************************************************
      subroutine pedetcfl(velmark)
      implicit double precision(a-h,o-z)
      dimension velmark(2,*)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pexymin/ dxmin,dymin
      common /c1mark/ nochain,ichain,imark(10)
  
      nmark = imark(1)
      vxmax = abs(velmark(1,1))
      vymax = abs(velmark(2,1))
      do 10 im=2,nmark
         vxmax = max(vxmax,abs(velmark(1,im)))
10       vymax = max(vymax,abs(velmark(2,im)))
      dtcfl = min(dxmin/vxmax,dymin/vymax)
     
      return
      end
