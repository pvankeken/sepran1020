c ************************************************************
c *   PEFILXY
c *
c *   Fill the coordinates of the primary nodes of a rectangular
c *   mesh in common /pexcyc/
c *   
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine pefilxy(ishape,kmesh,kprob,isol)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),funcx(105),icurvs(2)
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /pexymin/ dxmin,dymin

      funcx(1) = 105
      icurvs(1)=0
      icurvs(2)=1
      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (ishape.eq.1) then
c        *** linear elements
         nx = funcx(5)/2
         do 9 i=1,nx
9           xc(i) = funcx(4+2*i)
      else if (ishape.eq.2) then
c        *** Quadratic elements
         nx = funcx(5)/4+1
         do 10 i=1,nx
            xc(i) = funcx(2+4*i)
10          continue
      endif

      icurvs(1)=0
      icurvs(2)=2
      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (ishape.eq.1) then
c        *** linear elements
         ny = funcx(5)/2
         do 19 i=1,ny
19          yc(i) = funcx(5+2*i)
      else if (ishape.eq.2) then
c        *** Quadratic elements
         ny = funcx(5)/4+1
         do 20 i=1,ny
20          yc(i) = funcx(3+4*i)
      endif
      
      xcmin = xc(1)
      ycmin = yc(1)
      xcmax = xc(nx)
      ycmax = yc(ny)

      dxmin = xc(2)-xc(1)
      do 30 ic=2,nx-1
         dx = xc(ic+1)-xc(ic)
         dxmin = min(dxmin,dx)
30    continue
 
      dymin = yc(2)-yc(1)
      do 40 ic=2,ny-1
         dy = yc(ic+1)-yc(ic)
         dymin = min(dymin,dy)
40    continue
      
      if (ishape.eq.2) then 
         dxmin = dxmin/2 
         dymin = dymin/2
      endif

      return
      end
