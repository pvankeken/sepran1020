      program stmar
      implicit double precision(a-h,o-z)
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /c1colmar/ icol(10),ivol(10)
      dimension icr(10),xz(10),yz(10),coormark(10000)
      dimension toutput(500),cr(10)
      character*80 fmar

      read(5,*) fmar
      read(5,*) istart,iend,istride
      read(5,*) nxpix,nypix,rlam

      open(9,file=fmar,form='unformatted')
      rewind(9)

      open(12,file='times.output')
      rewind(12)
      i = 1
10      read(12,*,end=11) iter,toutput(i) 
        i = i+1
        goto 10
11    continue
      niter = i-1
      close(12)
      read(9) t,nochain,(imark(ichain),ichain=1,nochain)
      ntot = 0
      do 20 ichain=1,nochain
20          ntot = ntot+imark(ichain)
      read(9) (coormark(i),i=1,2*ntot)
      write(6,*) 'nochain = ',nochain
      call marras(coormark,rname,1)
      nl = nochain+1
      write(6,*) 'ivol: ',(ivol(ic),ic=1,nl)
      do 30 ic=1,nl
         xz(ic) = 0
         yz(ic) = 0
30    continue
      
      do 40 iy=1,nypix
         do 45 ic=1,nl
45          icr(ic) = 0
         do 50 ix=1,nxpix
            ip = ix + nxpix*(iy-1)
            x  = rlam*(ix-0.5)/nxpix
            y  = (iy-0.5)*1d0/nypix
            ic = ipix(ip)
            icr(ic) = icr(ic)+1
            xz(ic) = xz(ic)+x
            yz(ic) = yz(ic)+y
50      continue
        do 55 ic=1,nl
           if (ivol(ic).gt.0) then
              cr(ic) = icr(ic)*1d2/ivol(ic)
           else
              cr(ic) = 0
           endif
55      continue
        write(6,*) y,(cr(ic),ic=1,nl)
40    continue
       
      do 60 ic=1,nl
         if (ivol(ic).gt.0) then
             xz(ic) = xz(ic)/ivol(ic)
             yz(ic) = yz(ic)/ivol(ic)
         else
             xz(ic) = 0d0
             yz(ic) = 0d0
         endif
         write(6,'(i3,2f12.7)') ic,xz(ic),yz(ic)
60    continue

      end
      
