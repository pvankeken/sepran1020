cdccel4028
      subroutine el4028(ichois,name,itcoor,n,m,ndim,
     .                  modelv,etha,cn,cl,param,work,
     .                  xg,u,dudx,etheff,secinv,detdsc)
      implicit real (a-h,o-z)
      character *(*) name
      dimension etha(*),param(*),work(*),xg(*),u(*),dudx(*),
     .          etheff(*),secinv(*),detdsc(*)
      dimension gradv(9)
cdc      level 2,etha,param,work,xg,u,dudx,etheff,secinv,detdsc
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     .                notvec,irelem,nusol,inelem,npoint
      common /cmcdpr/ epsmac,sqreps,rinfin
      save m2,m3,m4,m5,m6,m7,m8
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 3    date   29-05-85
c                     date   11-03-1986 (addition of modelv=102)
c                     date   19-01-1987 (save)
c        version 3.3  date   13-01-1989 (itcoor=3 for modelv=102)
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c
c     this subroutine tests and computes model-dependent
c     quantities for a generalized newtonian liquid
c                    and plastico-viscous liquids
c     available are :  modelv=1, newtonian model
c                      modelv=2, power law model
c                      modelv=3, 3 parameter carreau model
c                      modelv=4, 3 parameter plastico-viscous liquid
c                               cn=1 : bingham model
c                               cn=2 : casson model
c                      modelv=100 , user defined effective viscosity
c                                   based on gradv
c                      modelv=101 , user defined effective viscosity
c                                   based on second invariant
c                      modelv=102 , user defined effective viscosity
c                                   based on x,u,second invariant
c
c            modelv=2,3 initiated by hans damsteegt (1-10-1983)
c
c***********************************************************************
c
c     parameters
c
c     etha(i)   viscosity parameter etha0 in integration points
c     cn        power (law model) parameter n
c     cl        carreau model parameter lambda
c     cl        binham (plastico-viscous) model parameter s
c     etheff    computed effective viscosity
c     secinv    computed second invariant of tensor a1
c     detdsc    derivative of second invarint for newton linearization
c               (for modelv=2,3 only)
c
c
c*  computation of effective viscosity
c
      if (modelv.eq.1) then
c
c*  newtonian shear-independent viscosity (modelv=1)
c
         do 5 i=1,m
            etheff(i)=etha(i)
5        continue
         return
      else if ((modelv.ne.100).and.(modelv.ne.101).and.(modelv.ne.102))
     .  then
c
c*  store parameter etha in work array for other models
c
         do 8 i=1,m
            work(i)=etha(i)
8        continue
      endif
c
c*  computation of effective viscosity from first kinematic tensor
c               or grad(v) (for modelv=100), or secinv (modelv=101,102)
c
      if (ifirst.eq.1) then
         m2=m+m
         m3=m2+m
         if (itcoor.eq.3) then
            m4=m3+m
            m5=m4+m
         else if (ndim.eq.3) then
            m4=m3+m
            m5=m4+m
            m6=m5+m
            m7=m6+m
            m8=m7+m
         endif
      endif
      if ((itcoor.eq.0).and.(ndim.eq.2)) then
c
c*   2-dim cartesian coordinates
c
         do 20 k=1,m
            gradv(1)=dudx(k)
            gradv(2)=dudx(m2+k)
            gradv(3)=dudx(m+k)
            gradv(4)=dudx(m3+k)
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(4)*gradv(4))
     .                   +(gradv(2)+gradv(3))*(gradv(2)+gradv(3))
            endif
20       continue
      else if ((itcoor.eq.1).and.(ndim.eq.2)) then
c
c*   axisymmetrical coordinates (no swirl)
c
         do 21 k=1,m
            gradv(1)=dudx(k)
            gradv(2)=dudx(m2+k)
            gradv(3)=dudx(m+k)
            gradv(4)=dudx(m3+k)
            gradv(5)=u(k)/xg(k)
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(4)*gradv(4)
     .                     +gradv(5)*gradv(5))
     .                   +(gradv(2)+gradv(3))*(gradv(2)+gradv(3))
            endif
21       continue
      else if ((itcoor.eq.2).and.(ndim.eq.2)) then
c
c*   2-dim polar coordinates
c
         do 22 k=1,m
            xgk=xg(k)
            gradv(1)=dudx(k)
            gradv(2)=dudx(m2+k)
            gradv(3)=(dudx(m+k)-u(m+k))/xgk
            gradv(4)=(dudx(m3+k)+u(k))/xgk
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(4)*gradv(4))
     .                   +(gradv(2)+gradv(3))*(gradv(2)+gradv(3))
            endif
22       continue
      else if ((itcoor.eq.3).and.(ndim.eq.2)) then
c
c*   axisymmetrical coordinates (with swirl)
c
         do 23 k=1,m
            xgk=xg(k)
            gradv(1)=dudx(k)
            gradv(2)=dudx(m4+k)
            gradv(3)=dudx(m2+k)
            gradv(4)=-u(m2+k)/xgk
            gradv(5)=u(k)/xgk
            gradv(6)=0.0e0
            gradv(7)=dudx(m+k)
            gradv(8)=dudx(m5+k)
            gradv(9)=dudx(m3+k)
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(5)*gradv(5)
     .                     +gradv(9)*gradv(9))
     .                   +(gradv(2)+gradv(4))*(gradv(2)+gradv(4))
     .                   +(gradv(3)+gradv(7))*(gradv(3)+gradv(7))
     .                   + gradv(8)*gradv(8)
            endif
23       continue
      else if ((itcoor.eq.0).and.(ndim.eq.3)) then
c
c*   3-dim cartesian coordinates
c
         do 30 k=1,m
            gradv(1)=dudx(k)
            gradv(2)=dudx(m3+k)
            gradv(3)=dudx(m6+k)
            gradv(4)=dudx(m+k)
            gradv(5)=dudx(m4+k)
            gradv(6)=dudx(m7+k)
            gradv(7)=dudx(m2+k)
            gradv(8)=dudx(m5+k)
            gradv(9)=dudx(m8+k)
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(5)*gradv(5)
     .                     +gradv(9)*gradv(9))
     .                    +(gradv(2)+gradv(4))*(gradv(2)+gradv(4))
     .                    +(gradv(3)+gradv(7))*(gradv(3)+gradv(7))
     .                    +(gradv(6)+gradv(8))*(gradv(6)+gradv(8))
            endif
30       continue
      else if ((itcoor.eq.4).and.(ndim.eq.3)) then
c
c*   3-dim cylindrical coordinates
c
         do 31 k=1,m
            xgk=xg(k)
            gradv(1)=dudx(k)
            gradv(2)=dudx(m3+k)
            gradv(3)=dudx(m6+k)
            gradv(4)=(dudx(m+k)-u(m+k))/xgk
            gradv(5)=(dudx(m4+k)+u(k))/xgk
            gradv(6)=dudx(m7+k)/xgk
            gradv(7)=dudx(m2+k)
            gradv(8)=dudx(m5+k)
            gradv(9)=dudx(m8+k)
            if (modelv.eq.100) then
               etheff(k)=fnv000(gradv)
               if (etheff(k).le.0) call ertrap(name,234,15,itype,
     .            ielem,0)
            else
               secinv(k)=2*(gradv(1)*gradv(1)+gradv(5)*gradv(5)
     .                     +gradv(9)*gradv(9))
     .                    +(gradv(2)+gradv(4))*(gradv(2)+gradv(4))
     .                    +(gradv(3)+gradv(7))*(gradv(3)+gradv(7))
     .                    +(gradv(6)+gradv(8))*(gradv(6)+gradv(8))
            endif
31       continue
      endif
      if (modelv.eq.101) then
         do 51 k=1,m
            secnd=secinv(k)
            etheff(k)=fnv001(secnd)
            if (etheff(k).le.0) call ertrap(name,234,15,itype,ielem,0)
51       continue
      else if (modelv.eq.102) then
         do 52 k=1,m
            secnd=secinv(k)
            x1=xg(k)
            x2=xg(m+k)
            if (ndim.eq.3) x3=xg(m2+k)
            u1=u(k)
            u2=u(m+k)
            if ((itcoor.eq.3).and.(ndim.eq.2)) then
               u2=u(m2+k)
               u3=u(m+k)
            else if (ndim.eq.3) then
               u3=u(m2+k)
            endif
            etheff(k)=fnv002(x1,x2,x3,u1,u2,u3,secnd)
            if (etheff(k).le.0) call ertrap(name,234,15,itype,ielem,0)
52    continue
      endif
c
c     * ichois = 0,1 computation of etheff
c     * ichois = 2, computation of etheff and detdsc
c
      if ((modelv.eq.100).or.(modelv.eq.101).or.(modelv.eq.102)) return
      goto (500,120,130,140),modelv
c
c     * powerlaw model
c
c
c     * ichois=0,1,...
c
120   expn=(cn-1)/2
      if (expn.le.0) then
         do 121 i=1,m
            if (secinv(i).lt.epsmac) secinv(i)=epsmac
121      continue
      endif
      do 122 i=1,m
         etheff(i)=work(i)*(secinv(i)**expn)
122   continue
      if (ichois.ne.2) return
c
c     * ichois=2
c
      expn=(cn-3)/2
      if (expn.le.0) then
         do 126 i=1,m
            if (secinv(i).lt.epsmac) secinv(i)=epsmac
126      continue
      endif
      do 127 i=1,m
         cfac=work(i)*(cn-1)
         detdsc(i)=cfac*(secinv(i)**expn)
127   continue
      return
c
c     * 3 parameter carreau liquid
c
c
c     * ichois=0,1,...
c
130   expn=(cn-1)/2
      do 131 i=1,m
         term=1+cl*secinv(i)
      etheff(i)=work(i)*(term**expn)
131   continue
      if (ichois.ne.2) return
c
c     * ichois=2
c
      expn=(cn-3)/2
      cfac=work(i)*(cn-1)*cl
      do 132 i=1,m
         term=1+cl*secinv(i)
         detdsc(i)=cfac*(term**expn)
132   continue
      return
c
c     * 3 parameter plastico-viscous liquid
c
c     * ichois=0,1,...
c
140   if (ifirst.eq.0) then
         expn=0.5e0/cn
         epsbng=100.0e0*epsmac
      endif
      do 141 i=1,m
         if (secinv(i).lt.epsbng) then
            if (secinv(i).gt.2*epsmac) then
               secinv(i)=(secinv(i)-epsmac)*(secinv(i)-epsmac)
     .                   /(2*(epsbng-epsmac))
            else
               secinv(i)=epsmac
            endif
         endif
         term=(cl*cl/(work(i)*work(i)*secinv(i)))**expn
         etheff(i)=work(i)*(1+term)**cn
141   continue
500   return
      end
cdc*eor
