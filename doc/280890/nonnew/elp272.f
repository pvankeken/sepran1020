cdccelp272
      subroutine elp272(coor,index1,n,x,y,curved)
cimp  implicit none
      double precision coor,x,y,curved
ce    real             coor,x,y,curved
      integer index1,n
      dimension coor(*),index1(*),x(*),y(*)
cdc      level 2,coor,index1,x,y
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   19-08-85
c                            14-01-87 (save)
c        version 2    date   18-11-1987 (nlast)
c                2.1  date   10-03-1989 (common cactl)
c                2.2  date   10-03-1989 (criterion for curved elements)
c                2.3  date   10-04-1989 (criterion for curved elements)
c                2.4  date   01-06-1989 (initialize nlast)
c
c
c   copyright (c) 1984,1987,1989  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     fill coordinates and test whether element is curved
c          (quadratic or extended quadratic triangle)
c          (quadratic or extended quadratic quadrilateral)
c
c     compute coordinates of 7'th point
c
c ********************************************************************
c
      common /cactl/ ielem ,itype ,ielgrp,inpelm,icount,ifirst,
     .               notmat,notvec,irelem,nusol ,inelem,npoint
      integer        ielem ,itype ,ielgrp,inpelm,icount,ifirst,
     .               notmat,notvec,irelem,nusol ,inelem,npoint
      save /cactl/
c
c*  ielem   actual element number
c   itype   actual type number
c   ielgrp  actual element group number
c   inpelm  number of nodal points in element ielem
c   icount  number of degrees of freedom in ielem with itype
c   ifirst  =0 first element of a group
c           =1 not the first element of a group
c   notmat  =0 element matrix not equal to zero
c           =1 element matrix equal to zero
c   notvec  =0 element vector equal to zero
c           =1 element vector not equal to zero
c   irelem  relative element number in ielgrp
c   nusol   total number of degrees of freedom in the mesh
c   inelem  number of elements in the group ielgrp
c*  npoint  total number of nodal points in the mesh
c
      integer i     ,ih    ,ind   ,indi  ,indi1 ,indi2 ,ipp   ,nlast
      double precision eps   ,xh    ,x7    ,yh    ,y7
ce    real             eps   ,xh    ,x7    ,yh    ,y7
      dimension ind(10)
      save ind,nlast,eps
      data nlast/0/
      if ((ifirst.eq.0).or.(n.ne.nlast)) then
c
c*  fill ind
c
         if ((n.eq.7).or.(n.eq.9)) then
            ipp=n
         else
            ipp=n+1
         endif
         do 1 i=1,n
            ind(i)=i
1        continue
         ind(ipp)=1
         nlast=n
         eps=1.0d-2
      endif
      if (n.eq.7) then
         ipp=6
      else
         ipp=n
      endif
      do 10 i=1,ipp
         ih=2*index1(i)
         x(i)=coor(ih-1)
         y(i)=coor(ih)
10    continue
      curved=0.0d0
      if (n.lt.6) return
c
c ********************************************************************
c
c      *  check whether boundary is curved or straight
c
      if (n.eq.9) ipp=8
      do 15 i=1,ipp,2
         indi=ind(i)
         indi1=ind(i+1)
         indi2=ind(i+2)
         if (max(abs(x(indi)-2*x(indi1)+x(indi2)),
     .           abs(y(indi)-2*y(indi1)+y(indi2))) .gt.
     .       max(abs(x(indi)-x(indi2)),abs(y(indi)-y(indi2)))*eps )
     .       curved=2.0d0
15    continue
      if (n.eq.7) then
         x7=x(1)+x(3)+x(5)
         y7=y(1)+y(3)+y(5)
         xh=x(2)+x(4)+x(6)
         yh=y(2)+y(4)+y(6)
         if (curved.lt.1) then
c
c    * triangle with straight sides  ==>  7'th point
c
            x(7)=x7/3
            y(7)=y7/3
         else
c
c   * triangle with curved boundary  ==>  7'th point
c
            x(7)=(4*xh-x7)/9
            y(7)=(4*yh-y7)/9
         endif
      endif
      end
cdc*eor
