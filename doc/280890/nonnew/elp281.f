cdccelp281
      subroutine elp281 (baricc,phig,phigt,
     v                   dptdks,dptdet,dpdks1,dpdet1)
      implicit double precision (a-h,o-z)
      dimension baricc(*),phig(*),phigt(*),dptdks(*),dptdet(*),
     v          dpdks1(*),dpdet1(*)
      dimension ind(9)
      save al1,al2,bl1,bl2,ind
cdc      level 2,baricc,phig,phigt,dptdks,dptdet,dpdks1,dpdet1
      data ind/0,7,14,0,7,11,0,4,11/
      data al1,al2/ 0.79742 69853 53087 d0, 0.10128 65073 23456 d0/
      data bl1,bl2/ 0.47014 20641 05115 d0, 0.05971 58717 89770 d0/
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 2    date   17-01-87
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     fill barycentric coordinates,
c          basis functions for extended quadratic triangle,
c          derivatives of transformation functions, and
c          derivatives of shape functions on reference triangle
c
c ********************************************************************
c
c
c*  store the bariccentric coordinates
c
      do 10 i=1,3
         indi=ind(i)
         indi3=ind(i+3)
         indi6=ind(i+6)
         baricc(1+indi)=al1
         baricc(2+indi3)=al2
         baricc(3+indi6)=al2
         baricc(4+indi)=bl1
         baricc(5+indi3)=bl1
         baricc(6+indi6)=bl2
         baricc(18+i)=0.33333 33333 33333 d0
10    continue
      dldks1=-1.0d0
      dldks2= 1.0d0
      dldks3= 0.0d0
      dldet1=-1.0d0
      dldet2= 0.0d0
      dldet3= 1.0d0
      kk=1
      ik=1
      do 20 k=1,7
         zeta1=baricc(kk  )
         zeta2=baricc(kk+1)
         zeta3=baricc(kk+2)
         zz=3*zeta1*zeta2*zeta3
c
c*  compute transformation functions in integration points
c
c*  compute basis/shape functions in integration points
c
         phigt(ik   )=zeta1*(2*zeta1-1)
         phig (ik   )=phigt(ik   )+zz
         phigt(ik+14)=zeta2*(2*zeta2-1)
         phig (ik+14)=phigt(ik+14)+zz
         phigt(ik+28)=zeta3*(2*zeta3-1)
         phig (ik+28)=phigt(ik+28)+zz
         phigt(ik+ 7)=4*zeta1*zeta2
         phig (ik+ 7)=phigt(ik+ 7)-4*zz
         phigt(ik+21)=4*zeta2*zeta3
         phig (ik+21)=phigt(ik+21)-4*zz
         phigt(ik+35)=4*zeta3*zeta1
         phig (ik+35)=phigt(ik+35)-4*zz
         phig (ik+42)=9*zz
c
c*  compute derivatives of shape functions in integration points
c
         dphi7x=dldks1*zeta2*zeta3
     v         +dldks2*zeta1*zeta3
     v         +dldks3*zeta1*zeta2
         dpdks1(ik   )=dldks1*(4*zeta1-1)+3*dphi7x
         dpdks1(ik+14)=dldks2*(4*zeta2-1)+3*dphi7x
         dpdks1(ik+28)=dldks3*(4*zeta3-1)+3*dphi7x
         dpdks1(ik+7 )=4*(dldks1*zeta2+dldks2*zeta1)-12*dphi7x
         dpdks1(ik+21)=4*(dldks2*zeta3+dldks3*zeta2)-12*dphi7x
         dpdks1(ik+35)=4*(dldks3*zeta1+dldks1*zeta3)-12*dphi7x
         dpdks1(ik+42)=27*dphi7x
         dphi7y=dldet1*zeta2*zeta3
     v         +dldet2*zeta1*zeta3
     v         +dldet3*zeta1*zeta2
         dpdet1(ik   )=dldet1*(4*zeta1-1)+3*dphi7y
         dpdet1(ik+14)=dldet2*(4*zeta2-1)+3*dphi7y
         dpdet1(ik+28)=dldet3*(4*zeta3-1)+3*dphi7y
         dpdet1(ik+7 )=4*(dldet1*zeta2+dldet2*zeta1)-12*dphi7y
         dpdet1(ik+21)=4*(dldet2*zeta3+dldet3*zeta2)-12*dphi7y
         dpdet1(ik+35)=4*(dldet3*zeta1+dldet1*zeta3)-12*dphi7y
         dpdet1(ik+42)=27*dphi7y
c
c*  compute derivatives of transformation functions in integration points
c
         dptdks(ik   )=dldks1*(4*zeta1-1)
         dptdks(ik+14)=dldks2*(4*zeta2-1)
         dptdks(ik+28)=dldks3*(4*zeta3-1)
         dptdks(ik+7 )=4*(dldks1*zeta2+dldks2*zeta1)
         dptdks(ik+21)=4*(dldks2*zeta3+dldks3*zeta2)
         dptdks(ik+35)=4*(dldks3*zeta1+dldks1*zeta3)
         dptdet(ik   )=dldet1*(4*zeta1-1)
         dptdet(ik+14)=dldet2*(4*zeta2-1)
         dptdet(ik+28)=dldet3*(4*zeta3-1)
         dptdet(ik+7 )=4*(dldet1*zeta2+dldet2*zeta1)
         dptdet(ik+21)=4*(dldet2*zeta3+dldet3*zeta2)
         dptdet(ik+35)=4*(dldet3*zeta1+dldet1*zeta3)
         kk=kk+3
         ik=ik+1
20    continue
      end
cdc*eor
