cdccelflr8
      subroutine elflr8( number, coeff, vecold, index3, index4, numold,
     .                   work, phi, ndecl, iuser, ifunc )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 2.1    date   25-05-89 (More unknowns per element for type 116)
c        version 2.0    date   04-04-89 (Removel of renum and sequen)
c        version 1.0    date   01-02-89 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c     This subroutine fills array coeff with a coefficient, where the
c     coefficient is equal to a solution or derivative of a preceding
c     computation. The value is only stored in all nodal points.
c     If the old solution is not available for all nodes, then it is
c     computed in the missing points by interpolation.
c     At this moment it is assumed that the old solution is at least
c     available in all vertex points
c
c     Number corresponds to the number th coefficient in common block celiar
c
c ********************************************************************
c
c
c                INPUT / OUTPUT PARAMETERS
c

cimp      implicit none

      integer number, numold, index3(numold,*), index4(numold,*), ndecl,
     .        iuser(*), ifunc
      real coeff(*), vecold(*), work(*), phi(ndecl,*)

c
c     number  i      Parameter indicating which coefficient in common block
c                    celiar must be considered
c
c     coeff   o      Double precision output array of length n in which the
c                    values of the coefficient in the nodal points are
c                    stored
c
c     vecold  i      In this array all preceding solutions are stored, i.e.
c                    all solutions that have been computed before and
c                    have been carried to system or deriva by the parameter
c                    islold in the parameter list of these main subroutines
c
c     index3  i      Two-dimensional integer array of length NUMOLD x NINDEX
c                    containing the positions of the "old" solutions in array
c                    VECOLD with respect to the present element
c
c                    For example VECOLD(INDEX3(i,j)) contains the j th
c                    unknown with respect to the i th old solution vector.
c                    The number i refers to the i th vector corresponding to
c                    IVCOLD in the call of SYSTM2 or DERIVA
c
c     index4  i      Two-dimensional integer array of length NUMOLD x INPELM
c                    containing the number of unknowns per point accumulated
c                    in array VECOLD with respect to the present element.
c
c                    For example INDEX4(i,1) contains the number of unknowns
c                    in the first point with respect to the i th vector stored
c                    in VECOLD.
c                    The number of unknowns in the j th point with respect to
c                    i th vector in VECOLD is equal to
c                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
c
c     numold  i      Number of old vectors that are stored in VECOLD
c
c     work           Two-dimensional work array of length max(n+1,m)
c
c     iuser   i      Standard array containing integer information of the
c                    coefficients
c
c     ifunc   o      Output parameter to be used if ind(number)>4000
c                    It indicates if the parameter which with the function
c                    is multiplied is a function (>0) or a constant (<0)
c
c ********************************************************************
c
c
c                PARAMETERS IN COMMON BLOCKS
c

      integer irefwr, irefre, irefer
      common /cmcdpi/ irefwr, irefre, irefer
c
c                 /cmcdpi/
c    Unit numbers to use for certain standard in- and output files.
c
c    irefwr   i   Unit number to use for "normal" writes
c    irefre   i   Same for standard reads (mostly keyboard input)
c    irefer   i   Same for error messages
c

      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

c                       / celiar /
c         Contains integer information of the various coefficients of the
c         differential equation.
c
c
c     ind       i    In this array of length 50, of each coefficient it
c                    is stored whether the coefficient is 0 (ind(i) = 0),
c                    constant (ind(i)<0) or variable (ind(i)>0) See elusr0
c                    If ind(i) should be 2003, then ind(i) is set equal to
c                    3000+ivec, where ivec refers to the vector to be used
c                    in array VECOLD
c
c                    If ind(i) should be 2004, then ind(i) is set equal to
c                    4000 + ipos, where ipos refers to the first relevant
c                    position in array iuser. In that case ich is also used
c
c     ich       i    In this array of length 50 information concerning the
c                    coefficients is stored for the case IND(I)=3000 + IVEC
c                    The value of ich(i) has the following meaning:
c
c                    0:   Array VECOLD(IVEC) corresponds to a vector of type
c                         116, i.e. with a constant number of unknowns
c                         per element
c
c                    1:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all nodal points
c
c                    2:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all nodal points
c
c                    3:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    4:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    5:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all nodes
c
c                    6:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all vertices but not in all nodes
c                         Two-dimensional elements only
c
c     mst       i    In this array of length 50 the start position of the
c                    real information of the ith coefficient in array user
c                    is stored. See elusr3
c                    If ist(i) = 3000+IVEC then mst(i) contains the degree of
c                    freedom j that must be used for this coefficient
c
c     ist       i    In this array of length 50 it is indicated if a coefficient
c                    is a copy of preceding coefficient (>0) or not (=0)
c                    If ist(i) > 0 the value of ist(i) indicates the coefficient
c                    number from which the coefficient must be copied.
c


      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint
c
c                 /cactl/
c    Contains element dependent information for the various element
c    subroutines. cactl is used to transport information from main
c    subroutines to element subroutines
c
c    ielem    i   Element number
c    itype    i   Type number of element
c    ielgrp   i   Element group number
c    inpelm   i   Number of nodes in element
c    icount   i   Number of unknowns in element
c    ifirst   i   Indicator if the element subroutine is called for the
c                 first time in a series (0) or not (1)
c    notmat  i/o  Indicator if the element matrix is zero (1) or not (0)
c    notvc   i/o  Indicator if the element vector is zero (1) or not (0)
c    irelem   i   Relative element number with respect to element group
c                 number ielgrp
c    nusol    i   Number of degrees of freedom in the mesh
c    nelem    i   Number of elements in the mesh
c    npoint   i   Number of nodes in the mesh
c


      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, idum,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, idum
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     kstep    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (1)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (2)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn=0) or variable (jdercn=1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12=a13=a23=0;  a11=a22=a33   a11 and w constant
c
c                    2   a12=a13=a23=0;  a11=a22=a33
c
c                    3   a12#0 or a13#0 or a23#0   a11=a22=a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     metupw    i    Type of upwinding method to be chosen. Possible values:
c
c                    0:  No upwinding
c                    1:  Classical upwind scheme
c                    2:  Il'in scheme
c                    3:  Doubly assymptotic scheme
c                    4:  Critical approximation
c
c     nvert     i    Number of vertices in the element
c
c     idum      -    Dummy, not yet used

c
c ********************************************************************
c
c               LOCAL PARAMETERS
c

      integer ivec, idegfd, i, ndegfd

c
c    ivec          Sequence number of vector in array VECOLD.
c                  Hence array VECOLD(IVEC) is considered
c
c    idegfd        Degree of freedom in VECTOR(IVEC) that must be used
c
c    i             Counting variable
c
c    ndegfd        Number of degrees of freedom per point in array VECOLD(IVEC)
c
c ********************************************************************
c
c               SUBROUTINES CALLED
c
c
c
c     BLAS  subroutines
c

      real sdot

c
c     DDOT   (CDOT)     Inner product of two vectors
c
c
c ********************************************************************
c
      if ( ind(number).ge.4000 ) then

c      *  ind(number) > 4000, special situation old solution must be
c                             multiplied by a coefficient
c                             The multiplication is carried out in elflr6

         ipos  = ind(number) - 4000
         ivec  = iuser(ipos)
         ifunc = iuser(ipos+2)
      else

c      *  ind(number) < 4000

         ivec   = ind(number) - 3000

      end if
      idegfd = mst(number)

      if ( ich(number) .eq. 0 ) then

c      *  ich(number) = 0, constant number of unknowns per element

         do 50 i = kstep, m, kstep
            coeff(i) = vecold(index3(ivec,idegfd))
 50      continue
         return

      else if ( ich(number) .eq. 1 ) then

c     *  in each point there is exactly one unknown

         do 100 i = 1, inpelm
            work(i) = vecold ( index3(ivec,i) )
100      continue

      else if ( ich(number) .eq. 2 ) then

c     *  in each point the number of unknowns is constant

         ndegfd = index4(ivec,1)
         do 110 i = 1, inpelm
            work(i) = vecold ( index3(ivec,(i-1)*ndegfd+idegfd) )
110      continue

      else if ( ich(number) .eq. 3 ) then

c     *  There is exactly one unknown in the vertices only

         do 120 i = 1, nvert*2, 2
            work(i) = vecold ( index3(ivec,i) )
120      continue
            
      else if ( ich(number) .eq. 4 ) then

c     *  in each vertex the number of unknowns is constant
c        Outside the vertices there are no unknowns

         ndegfd = index4(ivec,1)
         do 130 i = 1, nvert*2 ,2
            work(i) = vecold ( index3(ivec,(i-1)*ndegfd+idegfd) )
130      continue

      else if ( ich(number) .eq. 5 ) then

c     *  The number of unknowns is variable
c        The unknown is present in all nodes

         work(1) = vecold ( index3(ivec,idegfd) )
         do 140 i = 2, inpelm
            work(i) = vecold ( index3(ivec,index4(ivec,i-1)+idegfd) )
140      continue

      else

c     *  The number of unknowns is variable
c        The unknown is present in all vertices, but not in all nodes

         work(1) = vecold ( index3(ivec,idegfd) )

         if ( inpelm.eq.6 .or. inpelm.eq.7 .or. inpelm.eq.9 ) then

c         *  inpelm = 6, 7 or 9:  quadratic two-dimensional elements

            do 150 i = 3, inpelm-1, 2
               work(i) = vecold ( index3(ivec, index4(ivec,2*i-2)
     .                                           +idegfd))
150         continue

         else

c        *  This possibility has not yet been implemented in ELFLR8

            write(irefwr,180)
180         format(///' ERROR in ELFLR8'///
     .             ' The possibility unknown not present in all nodes',
     .             ' in combination with three-dimensional elements'/
     .             ' for the computation of coefficients as function of'
     .            ,' a preceding solution has not yet been implemented',
     .             //' Please contact Guus Segal')
            call instop

         end if                

      end if

      if ( ich(number).eq.3 .or. ich(number).eq.4 .or.
     .     ich(number).eq.6 ) then

c      *  The coefficients have only been computed in the vertices
c         Interpolate to the other nodes

         if ( n.eq.6 .or. n.eq.7 .or. n.eq.9 ) then

c         *  n = 6, 7 or 9:  quadratic two-dimensional elements

            work(2*nvert+1) = work(1)
            do 200 i = 1, nvert
               work(2*i) = 0.5e0 * ( work(2*i-1) + work(2*i+1) )
200         continue

            if ( n .eq. 7 ) then

c            *  n = 7, fill 7 th point

               work(n) = ( work(1) + work(3) + work(5) ) / 3e0

            else if ( n .eq. 9 ) then

c            *  n = 9, fill 9 th point

               work(n) = ( work(1) + work(3) + work(5) + work(7) )
     .                      * .25e0

            end if

         else

c        *  This possibility has not yet been implemented in ELFLR8

            write(irefwr,180)
            call instop

         end if

      else if ( inpelm.lt.n ) then
     
c      *  inpelm < n, special situation
c                     At this moment only 6 point triangles with dummy
c                     centroid are allowed

         work(n) = ( -work(1)-work(3)-work(5) +
     .               4e0 * ( work(2)+work(4)+work(6) ) ) / 9e0

      end if

      if ( jdiag .eq. 2 ) then

c      *  jdiag = 2, compute coefficients in Gauss points
c                    Store result in coeff

         do 310 i = 1, m
            coeff(i) = sdot ( n, work, 2, phi(1,i), 1 )
310      continue

      else

c     *  jdiag = 1, copy coefficients in coeff

         do 320 i = kstep, m, kstep
            coeff(i) = work(i)
320      continue

      end if                        

      end
cdc*eor
