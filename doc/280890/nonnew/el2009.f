cdccel2009
      subroutine el2009(ichois,elemmt,elemvc,s,f,ind,n,jind)
      implicit real (a-h,o-z)
      logical matr,vectr,mattra
cdc      level 2,elemmt,elemvc,f,s,ind
      dimension elemmt(*),elemvc(*),f(*),s(*),ind(*)
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     v                notvec,irelem,nusol,nelem,npoint
c
c ********************************************************************
c
c
c        programmer   Guus Segal
c        version 2    date   15-05-87
c
c
c
c   copyright (c) 1985  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c    *  the element matrix s (nxn) is transformed to a new element matrix s
c       (nxn;  new sequence)
c       the element vector f (1xn) is transformed to a new vector f
c       the new numbering is stored in array ind according to:
c
c       elemmt(i,j) = s(ind(i),ind(j))
c       f(i) = f(ind(i))
c
c       Both arrays are supposed to be stored as one-dimensional arrays
c       If s is stored as two-dimensional array then s transpose must be used
c
c ********************************************************************
c
c                            input/output
c
c      ichois    i   choice parameter. Possibilities:
c
c           0        both element matrix and vector are transformed
c
c           1        only the element matrix is transformed
c
c           2        only the element vector is transformed
c
c           10       both element matrix and vector are transformed
c                    The element matrix is transposed  (s is stored 2-d)
c
c           11       only the element matrix is transformed
c                    The element matrix is transposed  (s is stored 2-d)
c
c      elemmt    o   the element matrix in the new sequence
c
c      elemvc    o   the element vector in the new sequence
c
c      s         i   the element matrix in the old sequence
c
c      f         i   the element vector in the old sequence
c
c      ind      i/o  index array containing the new sequence
c
c      n         i   number of rows in s
c
c      jind      i   indication whether array ind is filled by the user
c                    jind=0, or must be filled by el2009 for ifirst=0
c                    jind=2,3
c
c ********************************************************************
c
      save m
      if(ifirst.eq.0) then
c
c      * ifirst = 0
c
         if(jind.gt.0) then
c
c         *  jind > 0    fill ind
c
           m=n/jind
           do 50 l=1,jind
              lm=(l-1)*m
              do 50 i=1,m
 50              ind(jind*(i-1)+l)=i+lm
         endif
      endif
      matr=.true.
      vectr=.true.
      mattra=.false.
      if(ichois.eq.1) then
         vectr=.false.
      else if(ichois.eq.2) then
         matr=.false.
      else if(ichois.eq.10) then
         matr=.false.
         mattra=.true.
      else if(ichois.eq.11) then
         matr=.false.
         mattra=.true.
         vectr=.false.
      endif
c
      if(matr) then
c
c      * change matrix  standard sequence
c
         in=0
         do 110 i=1,n
            i1=(ind(i)-1)*n
            do 100 j=1,n
100            elemmt(in+j)=s(i1+ind(j))
110         in=in+n
      else if(mattra) then
c
c      * change matrix  transposed sequence
c
         in=0
         do 130 i=1,n
            do 120 j=1,n
120            elemmt(in+j)=s((ind(j)-1)*n+ind(i))
130         in=in+n
      endif
      if(vectr) then
         do 200 i=1,n
200         elemvc(i)=f(ind(i))
      endif
      end
cdc*eor
