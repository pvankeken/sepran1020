cdccelind0
      subroutine elind0( index4, islold, numold, isub, isup, iuser )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 2.1    date   25-05-89 (Removel of error 786)
c        version 2.0    date   08-04-89 (Extra parameter iuser)
c        version 1.0    date   01-02-89 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c     This subroutine fills array ich in common block celiar for coefficients
c     that depend on a preceding solution
c
c ********************************************************************
c
c
c                INPUT / OUTPUT PARAMETERS
c

cimp      implicit none

      integer numold, index4(numold,*), islold(5,numold), isub, isup,
     .        iuser(*)

c
c     index4  i      Two-dimensional integer array of length NUMOLD x INPELM
c                    containing the number of unknowns per point accumulated
c                    in array VECOLD with respect to the present element.
c
c                    For example INDEX4(i,1) contains the number of unknowns
c                    in the first point with respect to the i th vector stored
c                    in VECOLD.
c                    The number of unknowns in the j th point with respect to
c                    i th vector in VECOLD is equal to
c                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
c
c     islold  i      User input array in which the user puts information
c                    of all preceding solutions
c                    Integer array of length 5 x numold
c
c     isub    i      Integer help parameter for do-loops to indicate the
c                    smallest coefficient number that is dependent on space
c
c     isup    i      Integer help parameter for do-loops to indicate the
c                    largest coefficient number that is dependent on space
c
c     iuser   i      Integer user array to pass user information from
c                    main program to subroutine. See STANDARD PROBLEMS
c
c
c     numold  i      Number of old vectors that are stored in VECOLD
c
c ********************************************************************
c
c
c                PARAMETERS IN COMMON BLOCKS
c

      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

c                       / celiar /
c         Contains integer information of the various coefficients of the
c         differential equation.
c
c
c     ind       i    In this array of length 50, of each coefficient it
c                    is stored whether the coefficient is 0 (ind(i) = 0),
c                    constant (ind(i)<0) or variable (ind(i)>0) See elusr0
c                    If ind(i) should be 2003, then ind(i) is set equal to
c                    3000+ivec, where ivec refers to the vector to be used
c                    in array VECOLD
c
c                    If ind(i) should be 2004, then ind(i) is set equal to
c                    4000 + ipos, where ipos refers to the first relevant
c                    position in array iuser. In that case ich is also used
c
c     ich       i    In this array of length 50 information concerning the
c                    coefficients is stored for the case IND(I)=3000 + IVEC
c                    The value of ich(i) has the following meaning:
c
c                    0:   Array VECOLD(IVEC) corresponds to a vector of type
c                         116, i.e. with a constant number of unknowns
c                         per element
c
c                    1:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all nodal points
c
c                    2:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all nodal points
c
c                    3:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    4:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    5:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all nodes
c
c                    6:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all vertices but not in all nodes
c                         Two-dimensional elements only
c
c     mst       i    In this array of length 50 the start position of the
c                    real information of the ith coefficient in array user
c                    is stored. See elusr3
c                    If ist(i) = 3000+IVEC then mst(i) contains the degree of
c                    freedom j that must be used for this coefficient
c
c     ist       i    In this array of length 50 it is indicated if a coefficient
c                    is a copy of preceding coefficient (>0) or not (=0)
c                    If ist(i) > 0 the value of ist(i) indicates the coefficient
c                    number from which the coefficient must be copied.
c

      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint
c
c                 /cactl/
c    Contains element dependent information for the various element
c    subroutines. cactl is used to transport information from main
c    subroutines to element subroutines
c
c    ielem    i   Element number
c    itype    i   Type number of element
c    ielgrp   i   Element group number
c    inpelm   i   Number of nodes in element
c    icount   i   Number of unknowns in element
c    ifirst   i   Indicator if the element subroutine is called for the
c                 first time in a series (0) or not (1)
c    notmat  i/o  Indicator if the element matrix is zero (1) or not (0)
c    notvc   i/o  Indicator if the element vector is zero (1) or not (0)
c    irelem   i   Relative element number with respect to element group
c                 number ielgrp
c    nusol    i   Number of degrees of freedom in the mesh
c    nelem    i   Number of elements in the mesh
c    npoint   i   Number of nodes in the mesh
c
c ********************************************************************
c
c               LOCAL PARAMETERS
c

      integer i, idegfd, inunkp, itypvc, ivec, jnunkp, k
      logical variab, allpnt

c
c    allpnt        Indication if all points contain the degree of freedom (true)
c                  or not (false)
c
c    i             Counting variable
c
c    idegfd        Degree of freedom in VECTOR(IVEC) that must be used
c
c    inunkp        Number of degrees of freedom in first point
c
c    itypvc        Indication of the type of solution vector:
c                  110: solution vector
c                  115, 116: vectors of special structure
c
c    ivec          Sequence number of vector in array VECOLD.
c                  Hence array VECOLD(IVEC) is considered
c
c    jnunkp        Number of degrees of freedom in second point
c
c    k             Counting variable
c
c    variab        Indication if the number of degrees of freedom per point is
c                  constant (false) or not (true)
c
c ********************************************************************
c
c               SUBROUTINES CALLED
c
c
c     SEPRAN service subroutines:
c
c     ERTRAP:  Gives error message
c
c ********************************************************************
c
c               ERROR MESSAGES
c
c      785  fatal error   Coefficient refers to preceding solution vector
c                         that has not been provided  to the subroutine
c      787  fatal error   Coefficient refers to preceding solution vector that 
c                         does not contain the required degree of freedom in
c                         any point
c      788  fatal error   Coefficient refers to preceding solution vector but
c                         the degree of freedom is not present in the first 
c                         point
c
c ********************************************************************
c
c    *  Loop over all relevant coefficients
c
      do 200 i = isub, isup

         if ( ind(i) .gt. 3000 ) then

c         *  ind(i) > 3000, coefficient depends on preceding solution

            if ( ind(i).gt.4000 ) then

c             *  ind(i) > 4000, information is stored in iuser

               ivec = iuser(ind(i)-4000)

            else

               ivec   = ind(i) - 3000
            end if

            idegfd = mst(i)
            if ( ivec .gt. numold ) then

c           *  ivec > numold: error 785

               call ertrap ('system  (elind0)', 785, 51, ielgrp, i, 0)
               call instop

            end if

            itypvc = islold(2,ivec)
            if ( itypvc .eq. 116 ) then

c             *  itypvc = 116, vector of special structure

               ich(i) = 0

            else

c             *  itypvc = 115 or 116
c                Check number of degrees of freedom per point

               variab = .false.
               inunkp = index4(ivec,1)
               do 100 k = 2, inpelm
                  if ( index4(ivec,k) - index4(ivec,k-1) .ne. inunkp )
     .                     then

c                  *  variable degrees of freedom per point

                     variab = .true.
                     go to 110

                  end if
100            continue

110            if ( .not. variab ) then

c               *  constant number of degrees of freedom

                  if ( idegfd .gt. inunkp ) call ertrap (
     .                  'system  (elind0)', 787, 51, ielgrp, i, 0)
                  if ( inunkp .eq. 1 ) then
                     ich(i) = 1
                  else
                     ich(i) = 2
                  end if

               else

c              *  Variable number of degrees of freedom per point

                  if ( idegfd .gt. inunkp ) call ertrap (
     .                  'system  (elind0)', 788, 51, ielgrp, i, 0)
                  allpnt = .true.
                  do 120 k = 2, inpelm
                     if ( index4(ivec,k) - index4(ivec,k-1) .lt.
     .                       idegfd ) then

c                      *  Degree of freedom idegfd not in this point

                        allpnt = .false.
                        go to 130

                     end if
120               continue

130               if ( allpnt ) then

c                  *  allpnt = true,  degree of freedom in all nodes

                     ich(i) = 5

                  else

c                  *  allpnt = false,  degree of freedom not in all nodes

                     jnunkp = index4(ivec,2) - index4(ivec,1)
                     if ( jnunkp .eq. 0 ) then

c                      *  Degree of freedom in vertices, but not in
c                         the mid points

                        if ( inunkp .eq. 1 ) then
                           ich(i) = 3
                        else
                           ich(i) = 4
                        end if

                     else

c                      *  Variable situation

                        ich(i) = 6

                     end if

                  end if

               end if

            end if

         end if

200   continue

      end
cdc*eor
