cdccel4025
      subroutine el4025 (itcoor,ichint,iadd,s,sp,sigma,isign,delta,
     v     e1,w,xg,phig,dphidx,n,m,ncomp,lrow)
      implicit real (a-h,o-z)
      dimension s(*),sp(*),e1(*),w(*),xg(*),phig(*),dphidx(*)
      dimension ind(3)
cdc      level 2,s,sp,e1,w,xg,phig,dphidx
      save ind
      data ind/4,6,2/
c
c*********************************************************************
c
c    the penalty sp(2n)*sp(2n)*sigma is added
c        to matrix (lrow)*(lrow)
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   08-01-85
c        version 2    date   08-05-85 (simplified parameter list)
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     for variables see elp270 , elm400 , el4000
c
c     iadd    iadd=0 : no addition of penalty matrix to matrix
c     s       element matrix to be supplemented
c     sp      penalty vector
c     m          number of integration points
c
c********************************************************************
c
      nm=n*m
      if (((n.eq.6).or.(n.eq.7))
     v    .and.(ichint.eq.0).and.(itcoor.eq.0).and.(ncomp.eq.2)) then
c
c*    for cartesian coordinates and a straight triangle the gauss
c             theorem is used to compute sp from a line integral
c
         h=isign/6.0e0
         ij=1
         do 10 i=1,3
            e1i=e1(i)*h
            e2i=e1(3+i)*h
            ii=ind(i)
            sp(ij)=e1i
            sp(ii)=-4*e1i
            sp(ij+n)=e2i
            sp(ii+n)=-4*e2i
            ij=ij+2
10       continue
         if (n.eq.7) then
            sp(7)=0.0e0
            sp(14)=0.0e0
         endif
c
c*    for curved elements div(u) is to be integrated
c
       else if ((ncomp.eq.2).and.(itcoor.eq.0)) then
c
c*  cartesian coordinates (itcoor=0)
c
         ik=1
         do 120 i=1,n
            sum1=0.0e0
            sum2=0.0e0
            do 110 k=1,m
               sum1=sum1+w(k)*dphidx(ik)
               sum2=sum2+w(k)*dphidx(nm+ik)
               ik=ik+1
110         continue
            sp(i)=sum1
            sp(i+n)=sum2
120      continue
      else if ((ncomp.eq.2).and.(itcoor.eq.1)) then
c
c*  axis-symmetrical coordinates (itcoor=1) , gauss integration
c
         ik=1
         do 220 i=1,n
            sum1=0.0e0
            sum2=0.0e0
            do 210 k=1,m
               sum1=sum1+w(k)*(dphidx(ik)+phig(ik)/xg(k))
               sum2=sum2+w(k)*dphidx(nm+ik)
               ik=ik+1
210         continue
            sp(i)=sum1
            sp(i+n)=sum2
220      continue
      else if ((ncomp.eq.2).and.(itcoor.eq.2)) then
c
c*  polar coordinates (itcoor=2) , gauss integration
c
         ik=1
         do 320 i=1,n
            sum1=0.0e0
            sum2=0.0e0
            do 310 k=1,m
               sum1=sum1+w(k)*(dphidx(ik)+phig(ik)/xg(k))
               sum2=sum2+w(k)*dphidx(nm+ik)/xg(k)
               ik=ik+1
310         continue
            sp(i)=sum1
            sp(i+n)=sum2
320      continue
      endif
c
c*    add penalty matrix to element matrix s
c
      if (iadd.eq.0) return
      h=2*sigma/delta
      n2=n+n
      ilrow=0
      do 1020 i=1,n2
         do 1010 j=1,n2
            s(ilrow+j)=s(ilrow+j)+h*sp(i)*sp(j)
1010     continue
         ilrow=ilrow+lrow
1020  continue
      end
cdc*eor
