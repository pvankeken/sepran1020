cdccel4031
      subroutine el4031 (ichint,fg,phig,rho,w,vector,nstart,n,m,lr)
      implicit real (a-h,o-z)
      dimension fg(*),phig(*),w(*),vector(*)
cdc      level 2,fg,phig,w,vector
c
c*******************************************************************
c
c     add external (driving) forces to vector
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   11-01-85
c        version 2    date   14-05-85 (simplified parameter list)
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c*    initialize element vector
c
c     fill it with body forces of two (momentum) equations
c             multiplied by rho
c
c     set other part of vector equal to zero
c
c*********************************************************************
c
      if ((ichint.eq.0).and.(n.eq.m)) then
c
c*  newton-cotes integration  /  n=m !
c
         do 10 i=nstart,n
            rho1=rho*w(i)
            vector(i)=fg(i)*rho1
            vector(i+n)=fg(n+i)*rho1
10       continue
      else
c
c*   gauss integration
c
         do 50 i=nstart,n
            sum1=0.0e0
            sum2=0.0e0
            ik=(i-1)*m+1
            do 40 k=1,m
               h=w(k)*phig(ik)
               sum1=sum1+h*fg(k)
               sum2=sum2+h*fg(m+k)
               ik=ik+1
40          continue
            vector(i)=rho*sum1
            vector(i+n)=rho*sum2
50       continue
      endif
      i1=n+n+1
      if (lr.ge.i1) then
         do 100 i=i1,lr
            vector(i)=0.0e0
100      continue
      endif
      end
cdc*eor
