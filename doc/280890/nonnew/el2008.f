cdccel2008
      subroutine el2008(s,sh,n,lrow)
      implicit real (a-h,o-z)
cdc      level 2,s,sh
      dimension s(*),sh(*)
c
c ********************************************************************
c
c
c        programmer   guus segal
c        version 1    date   30-11-84
c
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c    *  add matrix sh to matrix s, where sh is of smaller size than s
c
c ********************************************************************
c
c                            input/output
c
c      s        i/o  lrow x lrow matrix
c                    s := s + sh
c
c      sh        i   n x n matrix  n <= lrow
c                    the matrix sh is put from position 1 in s
c                    hence:  sh(i,j) = s(i,j)   i,j,=1(1)n
c
c      n         i   number of rows in sh
c
c      lrow      i   number of rows in s
c
c ********************************************************************
c
      in=0
      il=0
      do 100 i=1,n
         do 50 j=1,n
 50         s(il+j)=s(il+j)+sh(in+j)
         in=in+n
100      il=il+lrow
      end
cdc*eor
