cdccelcomm
      subroutine ELCOMM
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 1.0    date   10-06-88 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1988  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c     This subroutine initializes the common blocks CELINT and CELP
c     to get a standard set parameters
c
c ********************************************************************
c
c
c                Parameters in common blocks
c
cimp      implicit none

c                       / celp /
c         Contains some choice parameters for the various elp subroutines
c

      integer iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .        interp, isecdv
      common /celp/   iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .                interp, isecdv

c
c     iweigh    o    Indication if weights must be computed (1) or not (0)
c
c     ideriv    o    Indication if derivatives must be computed (1) or not (0)
c
c     irule     o    Indication of the type of integration rule to be applied.
c                    Possibilities:
c
c                    1:  Newton-Cotes four-point rule
c
c     igausp    o    Indication if Gauss points must be computed (1) or not (0)
c                    Is only applied when irule > 1
c
c     iphi      o    Indication if array phi must be filled (1) or not (0)
c                    Is only applied when irule > 1
c
c     jcart     o    Indication of the type of co-ordinates to be used in the
c                    element. Possible values:
c
c                    1:  Cartesian co-ordinates
c                    2:  Axi-symmetric co-ordinates
c                    3:  Polar co-ordinates
c
c     icoor     o    Indication if array x must be filled (1) or not (0)
c                    In general icoor = 0 is only permitted if array x has
c                    been filled before
c
c     interp    o    Indication if the value of the basis functions in a
c                    specific point must be computed (interpolation) (1)
c                    or not (0)
c
c     isecdv    -    Not used
c

      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, nunkp,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, nunkp
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     kstep    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (1)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (2)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn=0) or variable (jdercn=1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12=a13=a23=0;  a11=a22=a33   a11 and w constant
c
c                    2   a12=a13=a23=0;  a11=a22=a33
c
c                    3   a12#0 or a13#0 or a23#0   a11=a22=a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     metupw    i    Type of upwinding method to be chosen. Possible values:
c
c                    0:  No upwinding
c                    1:  Classical upwind scheme
c                    2:  Il'in scheme
c                    3:  Doubly assymptotic scheme
c                    4:  Critical approximation
c
c     nvert     i    Number of vertices in the element
c
c     nunkp     i    Maximal number of degrees of freedom per point


      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint
c
c                 /cactl/
c    Contains element dependent information for the various element
c    subroutines. cactl is used to transport information from main
c    subroutines to element subroutines
c
c    ielem    i   Element number
c    itype    i   Type number of element
c    ielgrp   i   Element group number
c    inpelm   i   Number of nodes in element
c    icount   i   Number of unknowns in element
c    ifirst   i   Indicator if the element subroutine is called for the
c                 first time in a series (0) or not (1)
c    notmat  i/o  Indicator if the element matrix is zero (1) or not (0)
c    notvc   i/o  Indicator if the element vector is zero (1) or not (0)
c    irelem   i   Relative element number with respect to element group
c                 number ielgrp
c    nusol    i   Number of degrees of freedom in the mesh
c    nelem    i   Number of elements in the mesh
c    npoint   i   Number of nodes in the mesh
c
c
c ********************************************************************
c
c     CELP:
c
      iweigh = 1
      ideriv = 1
      irule  = 1
      igausp = 0
      iphi   = 0
      jcart  = 1
      icoor  = 1
      interp = 0
      isecdv = 0

c     CELINT:

      ncomp  = 1
      nunkp  = 1
      jdiag  = 1
      ndim   = 2
      idim   = 1
      jsol   = 1
      jderiv = 0
      jtrans = 0
      jind   = 1
      jconsf = 2
      kstep  = 1
      jadd   = 1
      npsi   = 0
      jdercn = 1
      jzero  = 2
      jsecnd = 4
      istarw = 0

c    *  Compute nvert

      if ( inpelm.eq.6 .or. inpelm.eq.7 ) then
         nvert = 3
      else if ( inpelm .eq. 9 ) then
         nvert = 4
      else if ( inpelm .eq. 10 ) then
         nvert = 4
      else if ( inpelm .eq. 24 ) then
         nvert = 8
      else
         nvert = inpelm
      end if
      end
cdc*eor
