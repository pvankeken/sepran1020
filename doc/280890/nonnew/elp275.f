cdccelp275
      subroutine elp275(itcoor,x,y,e1,e2,sign,curved,
     v           w,x1g,x2g,phig,dphidx,dphidy,truc,trvc,n,mint)
      implicit double precision (a-h,o-z)
      dimension x(*),y(*),e1(*),e2(*),w(*),x1g(*),x2g(*),phig(*),
     v          dphidx(*),dphidy(*),truc(*),trvc(*)
      dimension ind(3)
cdc      level 2,x,y,e1,e2,w,x1g,x2g,phig,dphidx,dphidy,truc,trvc
      save ind
      data ind/4,6,2/
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   17-01-87
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c
c     compute transformation matrices truc, trvc for elimination
c             of uc,vc from integral((x-xc)*div(u)dx)=0
c             for straight or curved triangle
c             (itcoor=0,1,2,3)
c
c ********************************************************************
c
      xc=x(n)
      yc=y(n)
      wc=w(n)
c
c         in    = ( i - 1) * mint
c         (i,k) = ( i - 1 ) * mint + k = in + k
c         (n,k) = nn1 + k
c
      n1=n-1
      n12=n1+n1
      nn1=n*n1
      if (itcoor.eq.0) then
c
c*  cartesian coordinates / newton-cotes integration
c
      if ((curved.lt.1).and.(n.eq.7)) then
c
c*  triangle which is not curved : line integral
c
         h=sign/(6*wc)
         do 10 i=1,3
            i1=i+i-1
            i2=ind(i)
            x1=x(i1)-xc
            y1=y(i1)-yc
            e1i=h*e1(i)
            e2i=h*e2(i)
            w1=w(i1)/wc
            truc(i1)=x1*e1i-w1
            trvc(i1)=y1*e1i
            truc(i1+6)=x1*e2i
            trvc(i1+6)=y1*e2i-w1
            x1=4*(x(i2)-xc)
            y1=4*(y(i2)-yc)
            w1=w(i2)/wc
            truc(i2)=-x1*e1i-w1
            trvc(i2)=-y1*e1i
            truc(i2+6)=-x1*e2i
            trvc(i2+6)=-y1*e2i-w1
10       continue
         return
      else
c
c*   curved triangle or no triangle (but straight)
c
      ik=1
      do 60 i=1,n1
         sum1=0.0d0
         sum2=0.0d0
         sum3=0.0d0
         sum4=0.0d0
         do 50 k=1,mint
            w1=w(k)*(x(k)-xc)
            h1=dphidx(ik)
            h2=dphidy(ik)
            sum1=sum1+w1*h1
            sum2=sum2+w1*h2
            w1=w(k)*(y(k)-yc)
            sum3=sum3+w1*h1
            sum4=sum4+w1*h2
            ik=ik+1
50       continue
         truc(i)=sum1
         trvc(i)=sum3
         truc(i+n1)=sum2
         trvc(i+n1)=sum4
         ik=ik+1
60    continue
      a11=0.0d0
      a12=0.0d0
      a21=0.0d0
      a22=0.0d0
      ik=nn1+1
      do 70 k=1,mint
         w1=w(k)*(x(k)-xc)
         h1=dphidx(ik)
         h2=dphidy(ik)
         a11=a11+w1*h1
         a12=a12+w1*h2
         w1=w(k)*(y(k)-yc)
         a21=a21+w1*h1
         a22=a22+w1*h2
         ik=ik+1
70    continue
      endif
      else
c
      if (itcoor.eq.1) then
c
c*        itcoor=1   axisymmetric coordinates
c
         ik=1
         do 120 i=1,n1
            tru1=0.0d0
            tru2=0.0d0
            trv1=0.0d0
            trv2=0.0d0
            do 110 k=1,mint
               tru1=tru1+w(k)*(x1g(k)-xc)*(dphidx(ik)+phig(ik)/x1g(k))
               tru2=tru2+w(k)*(x1g(k)-xc)*dphidy(ik)
               trv1=trv1+w(k)*(x2g(k)-yc)*(dphidx(ik)+phig(ik)/x1g(k))
               trv2=trv2+w(k)*(x2g(k)-yc)*dphidy(ik)
               ik=ik+1
110         continue
            truc(i)=tru1
            truc(i+n1)=tru2
            trvc(i)=trv1
            trvc(i+n1)=trv2
120      continue
         a11=0.0d0
         a12=0.0d0
         a21=0.0d0
         a22=0.0d0
         ik=nn1+1
         do 130 k=1,mint
            a11=a11+w(k)*(x1g(k)-xc)*(dphidx(ik)+phig(ik)/x1g(k))
            a12=a12+w(k)*(x1g(k)-xc)*dphidy(ik)
            a21=a21+w(k)*(x2g(k)-yc)*(dphidx(ik)+phig(ik)/x1g(k))
            a22=a22+w(k)*(x2g(k)-yc)*dphidy(ik)
            ik=ik+1
130      continue
      endif
      if (itcoor.eq.2) then
c
c*        itcoor=2   polar coordinates
c
         ik=1
         do 220 i=1,n1
            tru1=0.0d0
            tru2=0.0d0
            trv1=0.0d0
            trv2=0.0d0
            do 210 k=1,mint
               tru1=tru1+w(k)*(x1g(k)-xc)*(dphidx(ik)+phig(ik)/x1g(k))
               tru2=tru2+w(k)*(x1g(k)-xc)*dphidy(ik)/x1g(k)
               trv1=trv1+w(k)*(x2g(k)-yc)*(dphidx(ik)+phig(ik)/x1g(k))
               trv2=trv2+w(k)*(x2g(k)-yc)*dphidy(ik)/x1g(k)
               ik=ik+1
210         continue
            truc(i)=tru1
            truc(i+n1)=tru2
            trvc(i)=trv1
            trvc(i+n1)=trv2
220      continue
         a11=0.0d0
         a12=0.0d0
         a21=0.0d0
         a22=0.0d0
         ik=nn1+1
         do 230 k=1,mint
            a11=a11+w(k)*(x1g(k)-xc)*(dphidx(ik)+phig(ik)/x1g(k))
            a12=a12+w(k)*(x1g(k)-xc)*dphidy(ik)/x1g(k)
            a21=a21+w(k)*(x2g(k)-yc)*(dphidx(ik)+phig(ik)/x1g(k))
            a22=a22+w(k)*(x2g(k)-yc)*dphidy(ik)/x1g(k)
            ik=ik+1
230      continue
      endif
c
      if (itcoor.eq.3) then
c
c*        itcoor=3   cartesian coordinates / gauss integration
c
         ik=1
         do 320 i=1,n1
            tru1=0.0d0
            tru2=0.0d0
            trv1=0.0d0
            trv2=0.0d0
            do 310 k=1,mint
               tru1=tru1+w(k)*(x1g(k)-xc)*dphidx(ik)
               tru2=tru2+w(k)*(x1g(k)-xc)*dphidy(ik)
               trv1=trv1+w(k)*(x2g(k)-yc)*dphidx(ik)
               trv2=trv2+w(k)*(x2g(k)-yc)*dphidy(ik)
               ik=ik+1
310         continue
            truc(i)=tru1
            truc(i+n1)=tru2
            trvc(i)=trv1
            trvc(i+n1)=trv2
320      continue
         a11=0.0d0
         a12=0.0d0
         a21=0.0d0
         a22=0.0d0
         ik=nn1+1
         do 330 k=1,mint
            a11=a11+w(k)*(x1g(k)-xc)*dphidx(ik)
            a12=a12+w(k)*(x1g(k)-xc)*dphidy(ik)
            a21=a21+w(k)*(x2g(k)-yc)*dphidx(ik)
            a22=a22+w(k)*(x2g(k)-yc)*dphidy(ik)
            ik=ik+1
330      continue
      endif
      endif
         h=a11*a22-a21*a12
         do 500 i=1,n12
            h1=truc(i)
            h2=trvc(i)
            truc(i)=(a12*h2-a22*h1)/h
            trvc(i)=(a21*h1-a11*h2)/h
500      continue
      end
cdc*eor
