      subroutine elm400 ( coor, elemmt, elemvc, iuser, user,index1, 
     .                    index3, index4, numold, vecold, islold,
     .                    work )


      real elemmt(*), elemvc(*), coor(2,*), user(*),
     .                 vecold(*), work(*)

      integer numold, iuser(*), index1(*), index3(numold,*),
     .        index4(numold,*), islold(5,numold)
      integer iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .        interp, isecdv
      common /celp/   iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .                interp, isecdv



      integer ielem, itype, ielgrp, inpelm, icount, ifirst, notmat,
     .        notvec, irelem, nusol, nelem, npoint
      common /cactl/  ielem, itype, ielgrp, inpelm, icount, ifirst,
     .                notmat, notvec, irelem, nusol, nelem, npoint


      real x, w, dphidx, xgauss, phi, array, amasps,
     .                 u, ugauss, un, dudx, trans, transl, qphixx,
     .                 etaeff, secinv, dvisd2, omega, sigma, rho, cn,
     .                 clam, f, eij, dlamdx, psi, dpsidx, dum, dum1
      common /celwrk/ x(14), w(7), dphidx(98), xgauss(14), phi(49),
     .                array(56), u(21), ugauss(21), un(21), dudx(42),
     .                trans(24), transl, qphixx(147), etaeff(7),
     .                secinv(7), dvisd2(7), omega, sigma, rho,
     .                cn, clam, psi(42), f(21), dpsidx(84), amasps(36), 
     .                dum1(276), eij(6), dlamdx(6), dum(488)


      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, nunkp,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, nunkp
      logical matrix, vector, oldsol
      integer notmas
      common /cellog/ matrix, vector, oldsol, notmas
      save /cellog/
      character * 80 chars
      character * 200 name
      common /cmessc/ chars(10), name
      save /cmessc/

      integer ints, lennam
      real reals
      common /cmessg/ reals(10), ints(10), lennam
      save /cmessg/

      real theta, dt, ratime
      integer iatime
      common /ctime/ theta, dt, ratime(8), iatime(10)


      integer induv(12)
      integer method, npuv, npuv2, nstart, itime, mconv,
     .        irotat, modelv, iche, itcoor, jelp, i, j,
     .        ichint, ichiv, ichl, icurve, iptim, ksign,
     .        isub, isup, kderiv, ipwork
      real delta, rhoh, phiphi

      save method, itcoor, npuv, npuv2, nstart, iptim, ipwork,
     .     induv, itime, mconv, irotat, modelv, isub, isup,
     .     rhoh, iche, ichl
      data induv/1, 7, 2, 8, 3, 9, 4, 10, 5, 11, 6, 12/
      call eropen ( 'elm400' )
      if ( ifirst.eq.0 ) then


         call el0400 ( iuser, user, jelp, isub, isup, method, mconv, 
     .                 itime, modelv, irotat )
         ipwork = ( (inpelm+1)*2 ) ** 2 + 1


         call elind0 ( index4, islold, numold, isub, isup, iuser )


         npuv=inpelm+1
         npuv2=npuv+npuv
         lrow=npuv2
         nstart=1
         itcoor = jcart - 1
         iptim = npuv*npuv+1

         iche = 1
         ichl = itcoor+30
         if (method.eq.0) ichl = ichl+40

      end if


      call elp220 ( ichl, ichint, coor, index1, name(1:lennam), ksign,
     .              icurve, delta, x, xgauss, eij, dlamdx, w, phi,
     .              dphidx, trans, transl, qphixx )
      if ( itcoor.ne.0 .or. icurve.ne.0 ) jdiag = 2



      do 150 i = isub, isup

         if ( ind(i).gt.0 .or. ist(i).gt.0 ) then


            call elflr6 ( i, user, array(1+(i-1)*m), index1, x, xgauss,
     .                    m, n, phi, vecold, work, 1, index1, index3,
     .                    index4, numold, iuser, ugauss )
            if ( i.eq.7 ) then


                do 110 j = 1, m
                   if ( array(j+(i-1)*m).le.0 ) then


                      ints(1) = i
                      ints(2) = itype
                      ints(3) = ielgrp
                      ints(4) = ielem
                      reals(1) = array(j+(i-1)*m)
                      chars(1) = 'eta (0)'
                      call errsub ( 1304, 4, 1, 1 )

                   end if

110             continue

             end if

         end if

150   continue

      if (ind(7).ne.0) then
         ichiv = 1
      else
         ichiv = ichint
      endif
      if (matrix .or. modelv.gt.1 .and. modelv.lt.4 .and. itime.ne.3)
     .   call el4028 ( iche, name(1:lennam), itcoor, npuv, npuv, ndim,
     .                 modelv, array(1+6*m), cn, clam, work(ipwork), 
     .                 work(ipwork+npuv), xgauss, ugauss, dudx,
     .                 etaeff, secinv, dvisd2 )
      if (.not.matrix) goto 2000
      call el4022 ( ichiv, itcoor, etaeff, work, w, xgauss, phi, dphidx,
     .              nstart, npuv, npuv, lrow, phi, dphidx, inpelm,
     .              dlamdx, qphixx, delta, f )
      if ((modelv.eq.2).and.(cn.gt.1)) then
         call el4036 ( itcoor, w, w(1+npuv), xgauss, phi, dphidx, 
     .                 ugauss, dudx, dvisd2, work(ipwork), ndim, npuv, 
     .                 npuv, phi, dphidx, inpelm )
         call el2008(work, work(ipwork), npuv2, lrow)
      endif
      call el4024(1, work, work(ipwork), trans, work(ipwork),
     .            ndim, npuv, lrow)
      call el2009( 1, elemmt, elemvc, work, work(ipwork),
     .             induv, icount, 2 )
2000  if (.not.vector) go to 3000
      call el4031 ( ichint, array(1+4*m), phi, rho, w, work(ipwork), 
     .              nstart, npuv, npuv, lrow )
      if ((modelv.eq.2).and.(cn.gt.1))
     .   call el4037 ( itcoor, w, work, xgauss, phi, dphidx, ugauss, 
     .                 dudx, secinv, dvisd2, work(ipwork), ndim, npuv, 
     .                 npuv, phi, dphidx, inpelm )
      if (mconv.eq.2 .or. mconv.eq.4)
     .   call el4033 ( ichint, itcoor, rho, ugauss, dudx, w, xgauss, 
     .                 phi, work(ipwork), nstart, npuv, npuv, ndim, phi,
     .                 inpelm )
      if(itime.eq.1 .or. itime.eq.4) call saxpy(npuv2, 1e0, u, 1,
     .    work(ipwork), 1 )
      call el4024(2, work, work(ipwork), trans, work, ndim, npuv, lrow)
      call el2009(2, elemmt, elemvc, work, work(ipwork), induv, icount,
     .            2)
3000  call erclos ( 'elm400' )
      end
