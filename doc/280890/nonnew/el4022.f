cdccel4022
      subroutine el4022 (ichvis,itcoor,etha,stress,w,xg,phig,dphidx,
     v                   nstart,n,m,lrow,psig,dpsidx,n1,
     v                   dldx,qdphix,delta,work)
      implicit real (a-h,o-z)
      dimension etha(*),stress(*),w(*),xg(*),phig(*),dphidx(*),
     v    psig(*),dpsidx(*),dldx(*),qdphix(*),work(*)
cdc      level 2,etha,stress,w,xg,phig,dphidx,psig,dpsidx,
cdc     v        dldx,qdphix,work
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     v                notvec,irelem,nusol,nelem,npoint
      save n2,nm,nm2,nln,nln1
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   08-01-85
c        version 2    date   06-05-85 (simplified parameter list)
c        version 3    date   20-05-85 (itcoor=3)
c                            19-01-87 (save)
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c    the element matrix due to viscous stresses is computed
c        for various models
c
c
c ********************************************************************
c
c     for variables see elp270 , elm400 , el4000
c
c     stress   element matrix to be initialised with viscous stresses
c
c     work     help array for intermediate storage of weights
c
c     nstart     number of test function to start with integration
c                (1 : navier-stokes; 7 : pressure gradient)
c
c     n          number of basis functions
c
c     lrow       total length of stress element matrix
c
c********************************************************************
c
c
c     the matrix stress is filled only with the viscous stress part
c        if there are other variables it has to be supplemented
c
c     stress(i,j) (lrow*lrow) = 2*sum1(i,j) (n*n) + sum2 + sum3 +2*sum4
c                           (for itcoor=0)
c
c     stress(i,j) (lrow*lrow) = similar + 2*sum5 (for itcoor=1)
c
c     in   = (i-1)*n     irow  = (i-1)*lrow      n2=n+n
c     jn   = (j-1)*n     nln  = n*lrow
c                        nln1 = nln+n
      if (ifirst.eq.0) then
         n2=n+n
         nm=n*m
         nm2=nm+nm
         nln=n*lrow
         nln1=n*(lrow+1)
      endif
      if (ichvis.eq.0) then
         h=delta*etha(1)
      else
         do 1 i=1,m
            work(i)=etha(i)*w(i)
1        continue
      endif
      if ((ichvis.eq.0).and.(itcoor.eq.0)
     v    .and.((n.eq.6).or.(n.eq.7)).and.(n.eq.m)) then
c****************************************************************
c
c*    special case : newtonian liquid with constant viscosity
c     and straigth triangles for cartesian coordinates
c
c
c*    dksidx = d ksi / dx  ,  dksidy = d ksi / dy
c     detadx = d eta / dx  ,  detady = d eta / dy
c
c     dkxkx=delta*etha*dkx*dkx, etc.
c     dkxey=delta*etha*dkx*dey, etc.
c
c     matrix stress(i,j) for viscous stresses is composed of
c
c     2*etha*dphi(i)/dx * du(j)/dx  i=1,..,n;j=1,..,n
c       ==> qdphixi,j)
c
c     etha*(dphi(i)/dy+dphi(i+n)/dx) * du(j)/dy
c       ==> qdphix(nm2+i,j)+qdphix(nm+i,j)
c       (remark that phi(i+n)=phi(i) and that stress(i,j) is lrow*lrow)
c
c     etha*(dphi(i+7)/dx + dphi(i)/dy) * dv(j+7)/dx
c       ==> qdphix(i,j) + qdphix(nm+(j,i))
c       (remark that u(j+n)=v(j) --> phi(j) )
c
c     2*viscous*dphi(i+n)/dy*du(j+n)/dy
c       ==> qdphix(nm2+(i,j))
c
c     qdphix(i,j)=integr(dphi(i)/dx * dphi(j)/dx) on the reference elem.
c
         dksidx=dldx(2)
         dksidy=dldx(5)
         detadx=dldx(3)
         detady=dldx(6)
         dkxkx=h*dksidx*dksidx
         dkxky=h*dksidx*dksidy
         dkxex=h*dksidx*detadx
         dkxey=h*dksidx*detady
         dkyky=h*dksidy*dksidy
         dkyex=h*dksidy*detadx
         dkyey=h*dksidy*detady
         dexex=h*detadx*detadx
         dexey=h*detadx*detady
         deyey=h*detady*detady
         do 20 i=nstart,n
            in=(i-1)*m
            irow=(i-1)*lrow
            jn=0
            jrow=0
            do 10 j=1,i
               q11=qdphix(in+j)
               q12=qdphix(nm+in+j)
               q21=qdphix(nm+jn+i)
               q22=qdphix(nm2+in+j)
               hq=q12+q21
               sum1=dkxkx*q11+dkxex*hq+dexex*q22
               sum2=dkyky*q11+dkyey*hq+deyey*q22
               sum3=dkxky*q11+dexey*q22
               sum4=sum3+dkxey*q12+dkyex*q21
               sum3=sum3+dkyex*q12+dkxey*q21
               q11=2*sum1+sum2
               q22=2*sum2+sum1
               stress(irow+j)=q11
               stress(irow+j+nln1)=q22
               stress(irow+j+n)=sum3
               stress(irow+j+nln)=sum4
c
c*   if i.eq.j prevent refilling the matrix stress
c
               if (j.ne.i) then
                  stress(jrow+i)=q11
                  stress(jrow+i+nln1)=q22
                  stress(jrow+i+nln)=sum3
                  stress(jrow+i+n)=sum4
               endif
               jn=jn+m
               jrow=jrow+lrow
10          continue
20       continue
      else if (itcoor.eq.0) then
c
c*    viscosity not constant, or element is curved (itcoor=0),
c               or element is not a triangle
c
      do 120 i=nstart,n
         in=(i-1)*m
         irow=(i-1)*lrow
         jn=0
         jrow=0
         do 110 j=1,i
            sum1=0.0e0
            sum2=0.0e0
            sum3=0.0e0
            sum4=0.0e0
            do 105 k=1,m
               dpxik=dphidx(in+k)
               dpxjk=dphidx(jn+k)
               dpyik=dphidx(nm+in+k)
               dpyjk=dphidx(nm+jn+k)
               h=work(k)
               sum1=sum1+h*dpxik*dpxjk
               sum2=sum2+h*dpyik*dpyjk
               sum3=sum3+h*dpyik*dpxjk
               sum4=sum4+h*dpxik*dpyjk
105         continue
            q11=2*sum1+sum2
            q22=2*sum2+sum1
            stress(irow+j)=q11
            stress(irow+j+nln1)=q22
            stress(irow+j+n)=sum3
            stress(irow+j+nln)=sum4
c
c*   if i.eq.j prevent refilling the matrix stress
c
            if (j.ne.i) then
               stress(jrow+i)=q11
               stress(jrow+i+nln1)=q22
               stress(jrow+i+nln)=sum3
               stress(jrow+i+n)=sum4
            endif
            jn=jn+m
            jrow=jrow+lrow
110      continue
120   continue
      else if ((ichvis.eq.1).and.((itcoor.eq.1).or.(itcoor.eq.3))) then
c
c*    axisymmetrical coordinates (no swirl part)
c
      do 220 i=nstart,n
         in=(i-1)*m
         irow=(i-1)*lrow
         jn=0
         jrow=0
         do 210 j=1,i
            sum1=0.0e0
            sum2=0.0e0
            sum3=0.0e0
            sum4=0.0e0
            sum5=0.0e0
            do 205 k=1,m
               dpxik=dphidx(in+k)
               dpxjk=dphidx(jn+k)
               dpyik=dphidx(nm+in+k)
               dpyjk=dphidx(nm+jn+k)
               h=work(k)
               sum1=sum1+h*dpxik*dpxjk
               sum2=sum2+h*dpyik*dpyjk
               sum3=sum3+h*dpyik*dpxjk
               sum4=sum4+h*dpxik*dpyjk
               sum5=sum5+h*phig(in+k)*phig(jn+k)/(xg(k)*xg(k))
205         continue
            q11=2*(sum1+sum5)+sum2
            q22=2*sum2+sum1
            stress(irow+j)=q11
            stress(irow+j+nln1)=q22
            stress(irow+j+n)=sum3
            stress(irow+j+nln)=sum4
c
c*   if i.eq.j prevent refilling the matrix stress
c
            if (j.ne.i) then
               stress(jrow+i)=q11
               stress(jrow+i+nln1)=q22
               stress(jrow+i+nln)=sum3
               stress(jrow+i+n)=sum4
            endif
            jn=jn+m
            jrow=jrow+lrow
210      continue
220   continue
      if ((itcoor.eq.3).and.(nstart.eq.1)) then
c
c*    axisymmetrical coordinates (swirl part)
c
         in=0
         irow=n2*lrow+n2
         jrow0=irow
         n1m=n1*m
         do 260 i=1,n1
         jrow=jrow0
            jn=0
            do 250 j=1,i
            sum1=0.0e0
               do 245 k=1,m
                  dpxik=dpsidx(in+k)
                  dpxjk=dpsidx(jn+k)
                  dpyik=dpsidx(n1m+in+k)
                  dpyjk=dpsidx(n1m+jn+k)
                  h=work(k)
                  xk=xg(k)
                  sum1=sum1+h*((dpxik-psig(in+k)/xk)
     v                        *(dpxjk-psig(jn+k)/xk)
     v                        +dpyik*dpyjk)
245            continue
               stress(irow+j)=sum1
c
c*   if i.eq.j prevent refilling the matrix stress
c
               if (j.ne.i) then
                  stress(jrow+i)=sum1
               endif
               jn=jn+m
               jrow=jrow+lrow
250         continue
            in=in+m
            irow=irow+lrow
260      continue
      endif
      else if ((ichvis.eq.1).and.(itcoor.eq.2)) then
c
c*    polar coordinates
c
      do 320 i=nstart,n
         in=(i-1)*m
         irow=(i-1)*lrow
         jn=0
         jrow=0
         do 310 j=1,i
            sum1=0.0e0
            sum2=0.0e0
            sum3=0.0e0
            sum4=0.0e0
            sum5=0.0e0
            sum6=0.0e0
            sum7=0.0e0
            sum8=0.0e0
            sum9=0.0e0
            do 305 k=1,m
               dpxik=dphidx(in+k)
               dpxjk=dphidx(jn+k)
               xk=xg(k)
               dpyik=dphidx(nm+in+k)/xk
               dpyjk=dphidx(nm+jn+k)/xk
               phik=phig(in+k)/xk
               phjk=phig(jn+k)/xk
               h=work(k)
               sum1=sum1+h*dpxik*dpxjk
               sum2=sum2+h*dpyik*dpyjk
               sum3=sum3+h*dpyik*dpxjk
               sum4=sum4+h*dpxik*dpyjk
               sum5=sum5+h*phik*phjk
               sum6=sum6+h*phik*dpxjk
               sum7=sum7+h*dpxik*phjk
               sum8=sum8+h*phik*dpyjk
               sum9=sum9+h*dpyik*phjk
305         continue
            q11=2*(sum1+sum5)+sum2
            q22=2*sum2+sum1+sum5-sum6-sum7
            stress(irow+j)=q11
            stress(irow+j+nln1)=q22
            stress(irow+j+n)=sum3+2*sum8-sum9
            stress(irow+j+nln)=sum4-sum8+2*sum9
c
c*   if i.eq.j prevent refilling the matrix stress
c
            if (j.ne.i) then
               stress(jrow+i)=q11
               stress(jrow+i+nln1)=q22
               stress(jrow+i+nln)=sum3+2*sum8-sum9
               stress(jrow+i+n)=sum4-sum8+2*sum9
            endif
            jn=jn+m
            jrow=jrow+lrow
310      continue
320   continue
      endif
c
c**  set elements of stress outside 2n rows of length 2n equal to 0
c                  on rows (   1 : n2   ) and colums (     n2 : lrow)
c
      if (nstart.ne.1) return
      len=lrow-n2
      if (len.eq.0) return
      irow=n2
      do 1000 i=1,n2
         do 990 j=1,len
            stress(irow+j)=0.0e0
990      continue
         irow=irow+lrow
1000  continue
      if (itcoor.eq.3) then
c
c*  axisymmetrical coordinates with swirl (extra zeros)
c                  on rows (n2+1 : n2+n1) and colums (      1 : n2)
c                                         and colums (n2+n1+1 : lrow)
c
         len=lrow-n2-n1
         irow=n2*lrow
         j1=n2+n1+1
         do 1030 i=1,n
            do 1020 j=1,n2
               stress(irow+j)=0.0e0
1020        continue
            if (len.gt.0) then
               do 1021 j=j1,lrow
                  stress(irow+j)=0.0e0
1021           continue
            endif
            irow=irow+lrow
1030     continue
      endif
      if (len.gt.0) then
c
c*  zeros on all columns of rows (n2+n1+1 : lrow)
c
         i1=(lrow-len)*lrow+1
         i2=lrow*lrow
         do 1040 i=i1,i2
            stress(i)=0.0e0
1040     continue
      endif
      end
cdc*eor
