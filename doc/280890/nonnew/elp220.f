cdccelp220
      subroutine elp220 (ichois,ichint,coor,index1,name,isg,icrv,delta,
     .                  x,xg,e1,dldx1,w,
     .                  phi,dphidx,truc,trpsut,qphixx)
      implicit real (a-h,o-z)
      character *(*) name
      dimension coor(*),index1(*),x(*),xg(*),e1(*),dldx1(*),
     .          w(*),phi(*),dphidx(*),truc(*),trpsut(*),
     .          qphixx(*)
      dimension dpdks0(49),dpdet0(49),wnc(7),wng(7),detjac(7),
     .          dpdks1(49),dpdet1(49),phigt(42),dptdks(42),dptdet(42),
     .          dxdksi(7),dydksi(7),dxdeta(7),dydeta(7),
     .          baricc(21),phinc(49),phig(49)
cdc      level 2,coor,index1,x,xg,e1,dldx1,w,
cdc     .        phi,dphidx,truc,trpsut,qphixx,
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     .                notvc,irelem,nusol,nelem,npoint
      common /cmcdpi/ irefwr,irefre,irefer
      save inum,itcoor,jchint,ideriv,itruv,itrps,
     .     wnc,wng,phig,phigt,phinc,
     .     dpdks0,dpdet0,dpdks1,dpdet1,dptdks,dptdet,baricc
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 1    date   04-01-85 (as elp270)
c        version 2    date   04-04-85 (itcoor=0 ==> itcoor=3 for
c                                      curved elements)
c        version 3    date   04-01-85 (simplified parameter list)
c                3.1  date   14-01-87 (save)
c                3.2  date   08-03-1989 (improved wnc(3) with d0)
c                3.3  date   03-04-1989 (if itruv=1 also ideriv=1)
c
c   copyright (c) 1984,1989  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c
c       input and output parameters
c
c      ichois   choice parameter, possibilities:
c
c             0:  cartesian coordinates      |  icoor=0
c             1:  axi-symmetric coordinates  |       =1
c             2:  polar coordinates          |       =2
c             3:  cartesian coord/gauss integ|       =3
c             for curved elements gauss integration is used !!
c                 unless itruv=0 and itrps=0
c
c
c                 isg,icrv,x,w,qphixx are stored
c                 wnc/wng , dpdks0/dpdks1 , ... are computed once
c
c           +10:  + dphidx            |  ideriv=1
c
c           +20:  + truc              |  itruv=1 (and also ideriv=1)
c
c           +40:  + trpsut            |  itrps=1
c
c     coor        array containing the coordinates
c
c     index1      see to0000
c
c     name        name of calling subroutine (literal constant)
c
c     isg        isg of the jacobian (anti-clockwise : >0 )
c
c     icrv        icrv=0,2 (triangle is not curved or is curved)
c
c     delta      jacobian (2*area)
c
c     x(14)      arrays containing the coordinates of the nodal points
c
c     e1(6)      differences (see lecture notes van kan/segal)
c                    (only for icrv=0)
c     dldx1(6)   derivatives of linear baricentric functions
c                    (only for icrv=0)
c     w(7)       array containing the weights for the integration
c
c     xg(14)     value of x(1:7),y(1:7) (x1,x2) in integration points
c
c     phig(49)    value of basis functions in integration points
c
c     dphidx(98), array containing the values of the derivatives of the
c                 basis functions
c
c     truc(24),   transformation matrices for elimination of u7,v7
c                 with help of integral((x-x7)*div(u)dx),...
c
c     trpsut(36)  transformation matrix for divergence-free
c                 element (from u,v at mid-side nodes to
c                 psi at vertices and u.t at mid-side nodes)
c                 not yet used
c
c     qphixx(1:49)  integral of dphi(i)/dksi * dphi(j)/dksi on reference
c                   triangle
c     qphixx(50:98)  integral of dphi(i)/dksi * dphi(j)/deta
c     qphixx(99:147) integral of dphi(i)/deta * dphi(j)/deta
c
c
c ********************************************************************
c
c    *local parameters
c
c     baricc(21)  baricentric coordinates of integration points
c                 (itcoor>0)
c
c    itcoor  = 0,1,2,3   type of coordinates (see ichois)
c    ideriv  = 0,1   computation of dphidx    (=1) or not (=0)
c    itruv   = 0,1   computation of truc,truc(13) (=1) or not (=0)
c    itrps   = 0,1   computation of trpsut    (=1) or not (=0)
c
c    dpdks0(49)  = derivatives of basis functions in reference triangle
c    dpdet0(49)  = derivatives of basis functions in reference triangle
c
c    dpdks1(49)  = derivatives of basis functions in integration points
c    dpdet1(49)  = derivatives of basis functions in integration points
c
c    phigt(42)   = transformation functions in integration points
c    dptdks(42)  = derivatives of transformation in integration points
c    dptdet(42)  = derivatives of transformation in integration points
c
c    dxdksi(7)  = derivative of x with respect to ksi
c    dydksi(7)
c    dxdeta(7)
c    dydeta(7)
c    detjac(7)  = determinant of jacobi in nodal points
c
c ********************************************************************
c
c
c    * initialize dpdks0,dpdet0  ==  dphi(i)/dksi in nodal points j=1,.,7
c
c         1     2     3      4     5      6          7
      data dpdks0/
     1 -3.e0,-1.e0, 1.e0, .25e0, 1.e0,-.25e0,-0.3333333333333333e0,
     2  4.d0, 0.d0,-4.d0,  1.d0, 0.d0, -1.d0, 0.d0,
     3 -1.e0, 1.e0, 3.e0, .25e0,-1.e0,-.25e0, 0.3333333333333333e0,
     4  0.e0, 0.e0, 0.e0,  5.e0, 4.e0, -1.e0, 1.3333333333333333e0,
     5  0.e0, 0.e0, 0.e0,-.75e0, 0.e0, .75e0, 0.e0,
     6  0.e0, 0.e0, 0.e0,  1.e0,-4.e0, -5.e0,-1.3333333333333333e0,
     7  0.e0, 0.e0, 0.e0,-6.75e0,0.e0,6.75e0, 0.e0/
c
c         1     2     3      4     5      6          7
      data dpdet0/
     1 -3.e0,-.25e0, 1.e0, .25e0, 1.e0,-1.e0,-0.3333333333333333e0,
     2  0.e0, -5.e0,-4.e0,  1.e0, 0.e0, 0.e0,-1.3333333333333333e0,
     3  0.e0, .75e0, 0.e0,-.75e0, 0.e0, 0.e0, 0.e0,
     4  0.e0, -1.e0, 4.e0,  5.e0, 0.e0, 0.e0, 1.3333333333333333e0,
     5 -1.e0,-.25e0,-1.e0, .25e0, 3.e0, 1.e0, 0.3333333333333333e0,
     6  4.d0, -1.d0, 0.d0,  1.d0,-4.d0, 0.d0, 0.d0,
     7  0.e0,6.75e0, 0.e0,-6.75e0,0.e0, 0.e0, 0.e0/
c
c*   initialize wnc : weights for newton-cotes integration on
c                     reference triangle with area 0.5
      data wnc/.025e0,.0666666666666666e0,.025e0,.0666666666666666e0,
     .         .025e0,.0666666666666666e0,.225e0/
c
c*   initialize wng : weights for gauss integration on
c                     reference triangle with area 0.5
      data wng/.06296 95902 72413 d0, .06619 70763 94253 d0,
     .         .06296 95902 72413 d0, .06619 70763 94253 d0,
     .         .06296 95902 72413 d0, .06619 70763 94253 d0,
     .         .11250 00001 5e0/
c
c*   initialize phinc : basis functions in nodal points
c
      data phinc/1.0e0,7*0.0e0,1.0e0,7*0.0e0,1.0e0,7*0.0e0,
     .           1.0e0,7*0.0e0,1.0e0,7*0.0e0,1.0e0,7*0.0e0,
     .           1.0e0/
c
c*******************************************************************
c
c*   ifirst=0  compute parameters integrals qphixx,qphixx(50),qphixx(99),
c              or  (itcoor>0) baricc,phig,dptdks,...
c
      if (ifirst.eq.0) then
c
c*   ifirst=0  compute parameters itcoor,ideriv,itruv,itrps
c
         inum=ichois/10
         itcoor=ichois-10*inum
         if (itcoor.eq.0) then
            jchint=0
         else
            jchint=1
         endif
         ideriv=0
         itruv=0
         itrps=0
         if (inum.ne.0) then
            goto (11,12,12,14,15,16,17),inum
17          itrps=1
16          itruv=1
11          ideriv=1
            goto 20
12          ideriv=1
            itruv=1
            goto 20
15          ideriv=1
14          itrps=1
20       endif
         if ((itcoor.ne.0).and.(itruv.eq.1)) ideriv=1
         call elp271(dpdks0,dpdet0,wnc,qphixx,qphixx(50),qphixx(99),7)
         call elp281(baricc,phig,phigt,
     .               dptdks,dptdet,dpdks1,dpdet1)
      endif
c
c*    fill coordinates in x,x(8)  determine whether element is curved
c
      call elp272(coor,index1,7,x,x(8),curved)
      icrv=curved+1.0d-7
      if (icrv.lt.1) then
c ********************************************************************
c
c
c*    boundaries of element are straight : special treatment
c
c         compute e1,dldx1,w,isg,delta,xg
c                 dphidx    if ideriv=1
c
c ********************************************************************
         if (itcoor.eq.0) then
            call elp273(itcoor,name,x,x(8),wnc,e1,e1(4),dldx1,dldx1(4),
     .                  sign,delta,w,xg,xg(8),baricc,7)
         else
            call elp273(itcoor,name,x,x(8),wng,e1,e1(4),dldx1,dldx1(4),
     .                  sign,delta,w,xg,xg(8),baricc,7)
         endif
         if (ideriv.eq.1) then
            call elp274(itcoor,dldx1,dphidx,baricc)
            call elp274(itcoor,dldx1(4),dphidx(50),baricc)
         endif
      else
c ********************************************************************
c
c
c*    boundaries of element are curved
c
c         compute w,isg,delta,xg,
c                 dphidx    if ideriv=1
c
c ********************************************************************
         if (itcoor.eq.0) itcoor=3
         call elp277(itcoor,x,x(8),dxdksi,dydksi,dxdeta,dydeta,
     .               xg,xg(8),phigt,dptdks,dptdet,7)
         call elp278(itcoor,name,wng,dxdksi,dydksi,dxdeta,dydeta,
     .               xg,detjac,sign,delta,w,7)
         if (ideriv.eq.1) then
            call elp279(dxdksi,dydksi,dxdeta,dydeta,detjac,
     .                  dpdks1,dpdet1,dphidx,dphidx(50),7,7)
         endif
      endif
c ********************************************************************
c
c*        compute truc        if itruv=1
c                 trpsut      if itrps=1
c
c ********************************************************************
      if (itruv.eq.1) call elp275(itcoor,x,x(8),e1,e1(4),sign,curved,
     .    w,xg,xg(8),phig,dphidx,dphidx(50),truc,truc(13),7,7)
      if (itrps.eq.1) then
c
c***   no transformation to divergence free basis functions avialable
c
         write(irefwr,'('' 7-p triangle : transformation'',
     .    '' to divergence free elm not yet ready'')')
         call instop
c        if (itrps.eq.1) call elp276(itcoor,e1,e1(4),trpsut)
      endif
c
c*  set ichint and fill phi
c
      isg=sign+1.0d-7
      if ((itcoor.eq.0).and.((icrv.eq.0).or.((itruv.eq.0).and.
     .    (itrps.eq.0)))) then
         ichint=0
         if ((ifirst.eq.0).or.(ichint.ne.jchint)) then
            jchint=0
            do 100 i=1,49
               phi(i)=phinc(i)
100         continue
         endif
         do 101 i=1,14
            xg(i)=x(i)
101      continue
      else
         ichint=1
         if ((ifirst.eq.0).or.(ichint.ne.jchint)) then
            jchint=1
            do 110 i=1,49
               phi(i)=phig(i)
110         continue
         endif
      endif
      end
cdc*eor
