cdccel4024
      subroutine el4024 (ichois,sm,sv,truc,b,k,n,lrow)
      implicit real (a-h,o-z)
      dimension sm(*),sv(*),truc(*),b(*)
cdc      level 2,sm,sv,truc,b
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     v                notvec,irelem,nusol,nelem,npoint
      save n1,n2,m,n21,n22,lr2
c
c*********************************************************************
c
c    the element matrix sm (lrow)*(lrow) is transformed to
c        a matrix (lrow-2)*(lrow-2) ; i.e the velocities in the
c        centroid of the element are eliminated (ichois=1,3)
c
c    the element vector sv (k*n+m) is transformed to s (k*n-k+m)
c        if required (ichois=2,3)
c
c ********************************************************************
c
c        programmer   jaap van der zanden
c        version 2    date   04-04-85
c                            19-01-87 (save)
c
c
c   copyright (c) 1984  "ingenieursbureau sepra"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "ingenieursbureau sepra".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     for variables see elp220 , elm400 , el4000
c
c     sm      element matrix to be transformed
c     sv      element vector to be transformed
c     truc    two rows of transformation matrix
c     b       help matrix for intermediate storage
c
c     lrow    total number of degrees of freedom
c
c********************************************************************
c
      if (ifirst.eq.0) then
         n1=n-1
         n2=n+n
         m=lrow-n2
         n21=n2-1
         n22=n2-2
         lr2=lrow-2
      endif
      if (ichois.eq.2) goto 100
c
c*    first : b (lrow)*(lrow-2) = sm (lrow)*(lrow) * r (lrow)*(lrow-2)
c             r(n,j)=truc(j) , r(2n,j)=truc(n22+j)
c
      ilr=0
      ilr2=0
      do 20 i=1,lrow
         sinn1=sm(ilr+n)
         sinn2=sm(ilr+n2)
         do 10 j=1,n1
            b(ilr2+j)=sm(ilr+j)+sinn1*truc(j)+sinn2*truc(n22+j)
10       continue
         ilr1=ilr+1
         do 11 j=n,n22
            b(ilr2+j)=sinn1*truc(j)+sm(ilr1+j)+sinn2*truc(n22+j)
11       continue
         if (m.ne.0) then
            ilr1=ilr1+1
            do 12 j=n21,lr2
               b(ilr2+j)=sm(ilr1+j)
12          continue
         endif
         ilr=ilr+lrow
         ilr2=ilr2+lr2
20    continue
c
c*    second : sm (lrow-2)*(lrow-2) = r(transp) * b (lrow)*(lrow-2)
c
      nlr2=n1*lr2
      n2lr2=n21*lr2
      ilr2=0
      do 40 i=1,n1
         trui=truc(i)
         trvi=truc(n22+i)
         do 30 j=1,lr2
            sm(ilr2+j)=b(ilr2+j)+trui*b(nlr2+j)+trvi*b(n2lr2+j)
30       continue
         ilr2=ilr2+lr2
40    continue
      do 60 i=n,n22
         trui=truc(i)
         trvi=truc(n22+i)
         ilr3=ilr2+lr2
         do 50 j=1,lr2
            sm(ilr2+j)=trui*b(nlr2+j)+b(ilr3+j)+trvi*b(n2lr2+j)
50       continue
         ilr2=ilr2+lr2
60    continue
      if (m.gt.0) then
         do 80 i=n21,lr2
            ilr3=ilr2+2*lr2
            do 70 j=1,lr2
               sm(ilr2+j)=b(ilr3+j)
70          continue
            ilr2=ilr2+lr2
80       continue
      endif
      if (ichois.eq.1) return
c
c*   transform vector  s = r (transp) * s
c
100   vn=sv(n)
      vn2=sv(n2)
      do 110 i=1,n1
         sv(i)=sv(i)+truc(i)*vn+truc(n22+i)*vn2
         sv(i+n1)=truc(i+n1)*vn+sv(i+n)+truc(n22+i+n1)*vn2
110   continue
c
c*    shift last elements if present
c
      if (m.eq.0) return
      do 120 i=n21,lrow
         sv(i)=sv(i+2)
120   continue
      end
cdc*eor
