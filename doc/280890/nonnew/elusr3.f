cdccelusr3
      subroutine elusr3 ( number, value, iuser, user )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 1.2    date   05-04-89 (Extension with 2004)
c        version 1.1    date   13-03-89 (Error correction: set nuserh=0)
c        version 1.0    date   01-02-89 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c      this subroutine is used to fill constants from array user and
c      iuser as help quantities for element subroutines
c
c ********************************************************************
c
c
c                INPUT / OUTPUT PARAMETERS
c

cimp      implicit none

      real user(*), value(*)
      integer iuser(*), number

c
c     number   i     Sequence number of coefficient to be filled with respect
c                    to the arrays in common block celiar
c
c     value    o     Array of length m in which the value of the coefficient
c                    is stored if the coefficient is a constant
c
c     iuser    i     User array in which integer information of the
c                    coefficients is stored (See Standard Problems)
c
c     user     i     User array in which real information of the
c                    coefficients is stored (See Standard Problems)
c
c ********************************************************************
c
c
c                PARAMETERS IN COMMON BLOCKS
c


      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint

      common /cactl/  ielem, itype, ielgrp, inpelm, icount, ifirst,
     .                notmat, notvec, irelem, nusol, nelem, npoint

c                       / cactl /
c
c     Contains element dependent information for the various element
c     subroutines. cactl is used to transport information from main
c     subroutines to element subroutines
c
c
c     ielem    i     Element number
c
c     itype    i     Type number of element
c
c     ielgrp   i     Element group number
c
c     inpelm   i     Number of nodes in element
c
c     icount   i     Number of unknowns in element
c
c     ifirst   i     Indication if the element subroutine is called for the
c                    first time in a series (0) or not (1)
c
c     notmat  i/o    Indication if the element matrix is zero (1) or not (0)
c
c
c     notvc  i/o    Indication if the element vector is zero (1) or not (0)
c
c
c     irelem  i     Relative element number with respect to element group
c                   number ielgrp
c
c     nusol   i     Number of degrees of freedom in the mesh
c
c     nelem   i     Number of elements in the mesh
c
c     npoint  i     Number of nodes in the mesh
c
c

      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, ispec,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, idum
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                ispec, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                idum(3)
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     ispec    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (0)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (1)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn = 0) or variable (jdercn = 1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12 = a13 = a23 = 0;  a11 = a22 = a33   a11 and w 
c                        constant
c
c                    2   a12 = a13 = a23 = 0;  a11 = a22 = a33
c
c                    3   a12#0 or a13#0 or a23#0   a11 = a22 = a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     idum      -    Dummy, not yet used
c

      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

c                       / celiar /
c         Contains integer information of the various coefficients of the
c         differential equation.
c
c
c     ind       i    In this array of length 50, of each coefficient it
c                    is stored whether the coefficient is 0 (ind(i) = 0),
c                    constant (ind(i)<0) or variable (ind(i)>0) See elusr0
c                    If ind(i) should be 2003, then ind(i) is set equal to
c                    3000+ivec, where ivec refers to the vector to be used
c                    in array VECOLD
c
c                    If ind(i) should be 2004, then ind(i) is set equal to
c                    4000 + ipos, where ipos refers to the first relevant
c                    position in array iuser. In that case ich is also used
c
c     ich       i    In this array of length 50 information concerning the
c                    coefficients is stored for the case IND(I)=3000 + IVEC
c                    The value of ich(i) has the following meaning:
c
c                    1:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all nodal points
c
c                    2:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all nodal points
c
c                    3:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    4:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    5:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all nodes
c
c                    6:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all vertices but not in all nodes
c                         Two-dimensional elements only
c
c     mst       i    In this array of length 50 the start position of the
c                    real information of the ith coefficient in array user
c                    is stored. See elusr3
c                    If ist(i) = 3000+IVEC then mst(i) contains the degree of
c                    freedom j that must be used for this coefficient
c
c     ist       i    In this array of length 50 it is indicated if a coefficient
c                    is a copy of preceding coefficient (>0) or not (=0)
c                    If ist(i) > 0 the value of ist(i) indicates the coefficient
c                    number from which the coefficient must be copied.
c
c
c ********************************************************************
c
c               LOCAL PARAMETERS
c

      integer i, nuserh

c
c    nuserh        Help variable to compute nuser
c
c    i             Counting variable
c
c ********************************************************************
c
c               SUBROUTINES CALLED
c
c     ELUSR3 calls the following subroutines:
c
c     ERTRAP:  Error messages
c
c ********************************************************************
c
      ind(number) = iuser(ipos)
      mst(number) = 0
      nuserh = 0
      ipos = ipos+1
      if ( ind(number) .eq. 0 ) then

c      * ind(number)  =  0

        do 100 i = 1,m
          value(i) = 0e0
100     continue

      else if ( ind(number) .lt. 0 ) then

c      * ind(number)<0

        if ( ind(number) .ge. -5 ) call ertrap('system   (elusr3)',42,
     .       16, ipos-1, ind(number), 0 )
        nuserh = -ind(number)
        do 110 i = 1,m
           value(i) = user(nuserh)
110     continue

      else

c      * ind(number) > 0

         if ( ind(number) .eq. 2003 ) then

c         *  ind(number) = 2003, coefficient is equal to a preceding vector

            ind(number) = 3000 + iuser(ipos)
            mst(number) = iuser(ipos+1)
            if ( iuser(ipos).le.0 .or. iuser(ipos).ge.1000 ) call 
     .           ertrap ('SYSTEM  (elusr3)',795,16, ipos-1, iuser(ipos),
     .                   0 ) 
            ipos = ipos + 2

         else if ( ind(number) .eq. 2004 ) then

c         *  ind(number) = 2004, coefficient is equal to a preceding vector
c                                multiplied by a constant or function

            ind(number) = 4000 + ipos
            mst(number) = iuser(ipos+1)
            if ( iuser(ipos).le.0 .or. iuser(ipos).ge.1000 ) call 
     .           ertrap ('SYSTEM  (elusr3)',795,16, ipos-1, iuser(ipos),
     .                   0 ) 
            if ( iuser(ipos+2).ge.1000 ) call ertrap (
     .           'SYSTEM  (elusr3)',796,16, ipos-1, iuser(ipos+2), 0 ) 
            if ( iuser(ipos+2).le.0 ) then

c             *  iuser(ipos+2) < 0,  multiply by constant

               if ( iuser(ipos+2).ge.-5) call ertrap (
     .           'SYSTEM  (elusr3)',797,16, ipos-1, iuser(ipos+2), 0 ) 
            end if
            ipos = ipos + 3

         else if ( ind(number) .ge. 2000 ) then

c         *  ind(number) = 2001 or 2002

            mst(number) = iuser(ipos)-1
            if ( ind(number) .eq. 2001 ) nuserh = mst(number)+npoint
            if ( ind(number) .eq. 2002 ) nuserh = mst(number)+nelem
            ipos = ipos+1
         end if
      endif
      nuser = max( nuser, nuserh )
c
      end
cdc*eor
