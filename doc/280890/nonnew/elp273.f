cdccelp273
      subroutine elp273(itcoor,name,x,y,wint,e1,e2,dldx1,dldx2,sign,
     .                  delta,w,x1g,x2g,baricc,mint)
      implicit double precision (a-h,o-z)
      character *(*) name
      dimension x(*),y(*),wint(*),e1(*),e2(*),dldx1(*),dldx2(*),w(*),
     .          x1g(*),x2g(*),baricc(*)
cdc      level 2,x,y,wint,e1,e2,dldx1,dldx2,w,x1g,x2g,baricc
      common /cactl/  ielem,itype,ielgrp,inpelm,icount,ifirst,notmat,
     .                notvc,irelem,nusol,nelem,npoint
      common /consta/ pi,dum(19)
c
c ********************************************************************
c
c        programmer   Jaap van der Zanden
c        version 3    date   28-02-86
c        version 4    date   15-02-88 (Factor 2 pi for axisymmetric co-ord)
c
c
c   copyright (c) 1984,1988  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c             straight triangle with mint integration points
c                               and either 6 or 7 nodal points
c
c     compute derivative of baricentric coordinates (itcoor=0)
c     revise weights
c     compute x1g,x2g  (itcoor > 0)
c
c     for itcoor see elp220 or elp221
c
c ********************************************************************
c
      e1(1)=y(3)-y(5)
      e1(2)=y(5)-y(1)
      e1(3)=y(1)-y(3)
      e2(1)=x(5)-x(3)
      e2(2)=x(1)-x(5)
      e2(3)=x(3)-x(1)
      delta=e1(3)*e2(1)-e1(1)*e2(3)
c
      if(delta) 30,20,40
c
c *error 105:  delta=0
c
20    call ertrap(name,105,15,itype,ielem,0)
      return
c
c  *delta < 0
c
30    sign=-1
      ddelta=-delta
      goto 50
c
c  *delta > 0
c
40    sign=1
      ddelta=delta
c
c ********************************************************************
c
c
50    do 60 i=1,3
         dldx1(i)=e1(i)/delta
         dldx2(i)=e2(i)/delta
60    continue
      delta=ddelta
      if (itcoor.eq.0) then
c
c*  newton-cotes integration
c
         do 70 i=1,mint
            w(i)=wint(i)*delta
70       continue
      else
c
c*  gauss integration
c
         kk=1
         h=delta
         if ( itcoor.eq.1 ) h = h * 2d0* pi
         do 90 i=1,mint
            zeta1=baricc(kk)
            zeta2=baricc(kk+1)
            zeta3=baricc(kk+2)
            x1g(i)=zeta1*x(1)+zeta2*x(3)+zeta3*x(5)
            x2g(i)=zeta1*y(1)+zeta2*y(3)+zeta3*y(5)
            if (itcoor.eq.3) then
               w(i)=wint(i)*h
            else
               w(i)=wint(i)*x1g(i)*h
            endif
            kk=kk+3
90       continue
      endif
      end
cdc*eor
