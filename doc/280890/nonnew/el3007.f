cdccel3007
      subroutine el3007( usol, u, dudx, phi, dphidx, mdecl, ndecl, 
     .                   nddim, ndcomp, index3, ind, trans, ugauss,
     .                   jdtran, numold, number )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 2.0a   date   26-06-89 (Ad-hoc extension for temporary error
c                                        correction elm400)
c        version 2.0    date   04-04-89 (New parameter list)
c        version 1.0    date   10-06-88 (Developed at Toshiba PC)
c
c
c
c   copyright (c) 1988,1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c    *  Compute a local vector and its derivatives from a known vector.
c       the values are evaluated in the integration points or in
c       the nodal points
c
c
c     EL3007 is called by several types of element subroutines
c            It must be preceded by ELP subroutines of the 6-series
c
c ********************************************************************
c
c
c                input / output parameters
c

cimp      implicit none

      integer mdecl, ndecl, nddim, ndcomp, numold, index3(numold,*),
     .        ind(ndecl,ndcomp), number,jdtran

      real  usol(*), u(ndecl,ndcomp), phi(ndecl,mdecl),
     .                  dudx(mdecl,nddim,ndcomp), dphidx(ndecl,mdecl,
     .                  nddim), trans(ndecl-jdtran,ndcomp*ndcomp), 
     .                  ugauss(mdecl,ndcomp)

c
c     usol     i     Known solution vector from which the local vector
c                    must be evaluated
c
c     u        o     Real n x ncomp array in which the values of the solution
c                    vector in the nodal points of the element are stored in
c                    the sequence:
c
c                    u  (x ) = u(i,j)  j = 1(1)n, i = 1 (1) ncomp
c                     i   j
c
c                    Array u is always filled
c
c     dudx     o     Real m x ndim x ncomp array in which the values of the
c                    derivatives of the solution vector in the integration
c                    points are stored in the sequence:
c
c                    du / dx  (x ) = dudx ( k, j, i )
c                      i    j   k
c
c                    If jdercn = 0 in common block celint, then only
c                    dudx (1,*,*) is filled because dudx is then a constant
c                    per element
c
c                    dudx is only filled if jderiv > 0
c
c
c     phi      i     Array of length n x m containing the values of the basis
c                    functions in the integration points if a Gauss rule is
c                    used.
c
c                    phi  (xg ) = phi (i,k)
c                       i    k
c
c     dphidx   i     Array of length n x m x 3  containing the derivatives
c                    of the basis functions in the sequence:
c
c                    d phi / dx (xg ) = dphidx (i,k,1);
c                         i        k
c
c
c                    d phi / dy (xg ) = dphidx (i,k,2);
c                         i        k
c
c
c                    d phi / dz (xg ) = dphidx (i,k,3);
c                         i        k
c
c                    If the element is a linear tetrahedron (n=4), then the
c                    derivatives are constant and only k = 1 is filled.
c
c     m        i     Number of integration points
c
c     n        i     Number of basis functions
c
c     ndim     i     Dimension of the space. Indicates the number of
c                    derivatives of the basis functions implicitly
c
c     ncomp    i     Number of components of the solution vector u
c
c     index3  i      Two-dimensional integer array of length NUMOLD x NINDEX
c                    containing the positions of the "old" solutions in array
c                    USOL with respect to the present element
c
c                    For example USOL(INDEX3(i,j)) contains the j th
c                    unknown with respect to the i th old solution vector.
c                    The number i refers to the i th vector corresponding to
c                    ISLOLD in the call of SYSTM2 or DERIVA
c
c
c     ind      i     Index array that is used if jind <= 0, to store the
c                    way the vector u must be extracted from usol. See jind
c
c     trans    i     Array of length n-jtrans x ncomp**2 to store the
c                    transformation matrix with which the n - jtrans to n 
c                    components of the vector u must be computed. Is only
c                    used if jtrans > 0
c
c     ugauss    o     array of length m x ncomp in which the values of
c                    u in the integration points are stored the sequence:
c
c                    u  (x ) = u(i,j)  j = 1(1)m, i = 1 (1) ncomp
c                     i   j
c
c                    ugauss is onlu filled if jdiag > 1 in common celint
c
c     jdtran    i    Value of jtrans in calling subroutine
c
c     numold    i    Number of old vectors that are stored in USOL
c
c     number    i    The value of number indicates which of the solution
c                    vectors must be used (1 <= number <= numold)
c
c ********************************************************************
c
c
c                Parameters in common blocks

      integer irefwr, irefre, irefer
      common /cmcdpi/ irefwr, irefre, irefer

c
c                 /cmcdpi/
c    Unit numbers to use for certain standard in- and output files.
c
c    irefwr   i   Unit number to use for "normal" writes
c    irefre   i   Same for standard reads (mostly keyboard input)
c    irefer   i   Same for error messages
c
c
c
c                       / cactl /
c
c         Information of the elements is transported from subroutine VOLINT
c         to ELI208 by means of common CACTL
c


      integer ielem, itype, ielgrp, inpelm, icount, ifirst, notmat,
     .        notvec, irelem, nusol, nelem, npoint
      common /cactl/  ielem, itype, ielgrp, inpelm, icount, ifirst,
     .                notmat, notvec, irelem, nusol, nelem, npoint

c
c     ielem   i     Element number
c
c     itype   i     Type number of element
c
c     ielgrp  i     Element group number
c
c     inpelm  i     Number of nodal points in the element
c
c     icount  i     Number of degrees of freedom in an element
c
c     ifirst  i     Indication of the element is called for the first time
c                   in the global subroutine (ifirst=0) or not (1)
c
c     notmat  i/o    Indication if the element matrix is zero (0) or not (1)
c
c     notvec  i/o    Indication if the element vector is zero (0) or not (1)
c
c     irelem  i     Relative element number with respect to standard element
c                   sequence number ielgrp
c
c     nusol   i     Number of degrees of freedom in the mesh
c
c     nelem   i     Number of elements in the mesh
c
c     npoint  i     Number of nodal points in the mesh
c
      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, nunkp,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, nunkp
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     kstep    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (1)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (2)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn=0) or variable (jdercn=1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12=a13=a23=0;  a11=a22=a33   a11 and w constant
c
c                    2   a12=a13=a23=0;  a11=a22=a33
c
c                    3   a12#0 or a13#0 or a23#0   a11=a22=a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     metupw    i    Type of upwinding method to be chosen. Possible values:
c
c                    0:  No upwinding
c                    1:  Classical upwind scheme
c                    2:  Il'in scheme
c                    3:  Doubly assymptotic scheme
c                    4:  Critical approximation
c
c     nvert     i    Number of vertices in the element
c
c     nunkp     i    Maximal number of degrees of freedom per point

c
c ********************************************************************
c
c               Local parameters
c

      integer i, j, k, l, lstep, lsub, lsup
      integer kderiv
      save lstep, lsub, lsup

c
c    i, j, k, l    Counting variables
c
c    lstep         Step variable for vertices. Usually lstep = 1,
c                  but when the solution must be computed in the vertices
c                  and the element is quadratic then lstep = 2
c
c    lsub          Under index for the direction of the derivative to be
c                  computed
c
c    lsup          Upper index for the direction of the derivative to be
c                  computed
c
c ********************************************************************
c
c               SUBROUTINES CALLED
c
c
c
c     BLAS  subroutines
c

      real sdot

c
c     DDOT   (CDOT)     Inner product of two vectors
c
c ********************************************************************
c
c   *  first set some constants
c
      if ( ifirst .eq. 0 ) then

c       *  ifirst = 0:  First element

         kderiv = abs(jderiv)
         if ( kderiv.eq.1 .or. kderiv.eq.5 ) then

c        *  kderiv = 1 or 5:  The gradient must be computed
c                             Components 1 to ndim

            lsub=1
            lsup=ndim

         else if ( kderiv .gt. 1 ) then

c        *  kderiv 2, 3 or 4:  Component kderiv-1 must be computed

            lsub=kderiv-1
            lsup=lsub

         end if
         if ( kderiv.gt.1 .and. ndim.eq.2 .and. n.ge.6 ) then

c         *  kderiv > 1, hence computation in vertices
c                        Furthermore quadratic 2D element
c                        Skip points between

            lstep=2
         else
            lstep=1
         end if
         if ( kderiv.gt.1 .and. ndim.eq.3 .and. (n.ne.4 .and. n.ne.8) )
     .      then
               write(irefwr,10)
 10            format(' This possibility has not yet been implemented',
     .                ' in el3007')
         end if

      end if

c    *  Fill array u with solution in nodes

      if ( jind .eq. 0 ) then

c      *  jind = 0, use array ind to find unknowns

         do 100 j = 1, ncomp
         do 100 i = 1, n - jtrans
            u ( i,j ) = usol ( index3 ( number, ind ( i,j ) ) )
100      continue

      else if ( jind .eq. -1 ) then

c      *  jind = -1, use array ind partly to find unknowns

         if ( nunkp .eq. 1 ) then

c         *  nunkp = 1

            do 110 i = 1, n - jtrans
               u ( i,1 ) = usol ( index3 ( number, ind (1,1)+i-1 ) )
110         continue

         else

c         *  nunkp > 1

            do 120 j = 1, ncomp
            do 120 i = 1, n - jtrans
               u ( i,j ) = usol ( index3 ( number,
     .                     ind (j,1)-nunkp + i*nunkp ) )
120         continue

         end if

      else

c      *  jind = >0, use jind to find unknowns

         if ( nunkp .eq. 1 ) then

c         *  nunkp = 1

            do 130 i = 1, n - jtrans
               u ( i,1 ) = usol ( index3 ( number, i ) )
130         continue

         else

c         *  nunkp > 1

            do 140 j = 1, ncomp
            do 140 i = 1, n - jtrans
               u ( i,j ) = usol ( index3 ( number,
     .                     jind-1+j-nunkp + i*nunkp ) )
140         continue

         end if

      end if

      if ( jtrans .eq. 1 ) then

c      *  jtrans = 1  Compute extra unknowns by transformation
c                  n-1
c         u (n) =  sum  trans (l)  u (l)
c          k       l=1       k      k

         do 220 j = 1, ncomp
            u ( n,j ) = 0e0
            do 210 i = 1, ncomp
               u(n,j) = u(n,j) + sdot ( n-jtrans, 
     .                  trans(1,i+(j-1)*ncomp), 1, u(1,i), 1 )
210         continue
220      continue

      end if

      if ( jsol .eq. 1 .and. jdiag .ne. 1 ) then

c      *  Compute solution in Gauss points
c                   n
c         ug (k) = sum   u (i) phi (k)
c           l      i=1    l       i

         do 250 j = 1, ncomp
            do 240 i = 1, n
               ugauss ( i,j ) = sdot ( n, u(1,j), 1, phi(1,i), 1 )
240         continue
250      continue

      else if ( jsol.eq.1 ) then

c      *  jsol = 1,  Copy u into ugauss

         do 270 j = 1, ncomp
            do 260 i = 1, n
               ugauss(i,j) = u(i,j)
260         continue
270      continue

      end if

      if ( jderiv .gt. 0 ) then

c      *  jderiv > 0,  compute derivatives

         if ( jdercn .eq. 0 ) then

c         *  Derivatives of basis functions are constant, hence dudx is constant
c
c                      n
c            du/dx =  sum  u   d phi  / dx
c                 k   i=1   i       i     k

            do 300 j = 1, ncomp
               do 290 k = lsub, lsup
                  dudx ( 1, k+1-lsub, j ) = sdot ( n, u(1,j), 1,
     .                                      dphidx(1,1,k), 1 )
290            continue
300         continue

         else

c         *  dudx is variable
c

            do 350 j = 1, ncomp
               do 340 k = lsub, lsup
                  do 330 l = 1, m, lstep
                     dudx ( l, k+1-lsub, j ) = sdot ( n, u(1,j), 1,
     .                                         dphidx(1,l,k), 1 )
330               continue
340            continue
350         continue

         end if

      else if ( jderiv .lt. 0 ) then

c      *  jderiv < 0,  compute derivatives in a special way
c         This possibility is temporarily and is only meant to be used in
c         combination with old basis functions subroutines elp...
c         In these subroutines the sequence of dphidx is different from the
c         new one in the sense that we have:
c
c         dphidx ( k,i,l );  k=1(1)m, i=1(1)n, l=1(1)ndim
c
c         This possibility works only in the case m=n!!!
c


         do 390 j = 1, ncomp
            do 380 k = lsub, lsup
               do 370 l = 1, m, lstep
                  dudx ( l, k+1-lsub, j ) = sdot ( n, u(1,j), 1,
     .                                      dphidx(l,1,k), m )
370            continue
380         continue
390      continue

      end if

      end
cdc*eor
