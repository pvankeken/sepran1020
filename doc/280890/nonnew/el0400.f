      subroutine el0400( iuser, user, jelp, isub, isup, method,
     .                   mconv, itime, modelv, irotat )


      real user(*)

      integer iuser(*), jelp, isub, isup, method, mconv, itime, modelv,
     .        irotat

      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint

      common /cactl/  ielem, itype, ielgrp, inpelm, icount, ifirst,
     .                notmat, notvec, irelem, nusol, nelem, npoint


      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)


      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, nunkp,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, nunkp


      integer iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .        interp, isecdv
      common /celp/   iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .                interp, isecdv

      integer ierror, nerror
      common /cconst/ ierror, nerror


      logical matrix, vector, oldsol
      integer notmas
      common /cellog/ matrix, vector, oldsol, notmas


      real x, w, dphidx, xgauss, phi, array, amasps,
     .                 u, ugauss, un, dudx, trans, transl, qphixx,
     .                 etaeff, secinv, dvisd2, omega, sigma, rho, cn,
     .                 clam, f, eij, dlamdx, psi, dpsidx, dum, dum1
      common /celwrk/ x(14), w(7), dphidx(98), xgauss(14), phi(49),
     .                array(56), u(21), ugauss(21), un(21), dudx(42),
     .                trans(24), transl, qphixx(147), etaeff(7),
     .                secinv(7), dvisd2(7), omega, sigma, rho,
     .                cn, clam, psi(42), f(21), dpsidx(84), amasps(36), 
     .                dum1(276), eij(6), dlamdx(6), dum(488)

      character * 80 chars
      character * 200 name
      common /cmessc/ chars(10), name
      save /cmessc/

      integer ints, lennam
      real reals
      common /cmessg/ reals(10), ints(10), lennam
      save /cmessg/

      integer i, j, isubcf, isupcf, nparm

      call eropen ( 'el0400' )
      call elcomm
      ncomp = 2
      nunkp = 2
      ind(50) = 0


      if ( itype.eq.400 .or. itype.eq.402 .or. itype.eq.404
     .     .or. itype.eq.406 ) then


         n      =  7
         m      =  7
         mn     = 49
         jelp   =  1
         method =  1
         jtrans =  1
         if ( itype.eq.406 ) then
            ncomp = 3
         else
            ncomp = 2
         end if
         if ( inpelm .ne. n-1 ) then


            ints(1) = inpelm
            ints(2) = itype
            ints(3) = n-1
            ints(4) = ielgrp
            ints(5) = ielem
            call errsub ( 1300, 5, 0, 0 )
         end if
         if (icount .ne. ncomp*(n-1) ) then


            ints(1) = icount
            ints(2) = itype
            ints(3) = ncomp*(n-1)
            ints(4) = ielgrp
            ints(5) = ielem
            call errsub ( 1301, 5, 0, 0 )
         end if
         if ( itype .eq. 400 ) then


            jcart = 1

         else if ( itype .eq. 402 ) then


            jcart = 3

         else if ( itype.eq.404 .or. itype.eq.406 ) then


            jcart = 2

         end if

      end if


      ipos = iuser ( ielgrp + 5 )
      if ( ipos.lt.7 ) then


         ints(1) = ielgrp+5
         ints(2) = ipos
         ints(3) = itype
         ints(4) = ielgrp
         ints(5) = ielem
         call errsub ( 1302, 5, 0, 0 )
      end if
      nuser = 0
      nparm = ncomp+5
      do 100 i = 1, nparm
         call elusr3 ( i, array(1+(i-1)*m), iuser, user )
100   continue



      if ( ind(1).gt.0 ) then


         ints(1) = 1
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         chars(1) = 'epsilon'
         call errsub ( 1303, 4, 0, 1 )

      else if ( array(1).le.0e0 ) then


         ints(1) = 1
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         reals(1) = array(1)
         chars(1) = 'epsilon'
         call errsub ( 1304, 4, 1, 1 )
      else

         sigma = 1e0 / array(1)

      end if


      if ( ind(2).gt.0 ) then


         ints(1) = 2
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         chars(1) = 'rho'
         call errsub ( 1303, 4, 0, 1 )

      else if ( array(1+m).le.0e0 ) then


         ints(1) = 2
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         reals(1) = array(1+m)
         chars(1) = 'rho'
         call errsub ( 1304, 4, 1, 1 )
      else

         rho = array(1+m)

      end if


      mconv = ind(3)
      if ( mconv .ge. 100 ) then
         mconv = mconv - 100
         itime = 4
      else if ( mconv.ge.30) then
         itime = 3
         mconv = 0
      else if ( mconv.ge.10) then
         mconv = mconv-10
         itime = 1
         if ( mconv.ge.10) then
           mconv = mconv-10
           itime = 2
           if ( mconv.ge.5) then
              mconv = mconv-5
              itime = -2
           end if
         end if
      end if
      if ( mconv.gt.0 ) ind(50) = 1

      if ( mconv.lt.0 .or. mconv.gt.4 ) then


         ints(1) = 3
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         ints(5) = mconv
         chars(1) = 'mconv'
         call errsub ( 1305, 5, 0, 1 )

      end if


      if ( ind(4).gt.0 ) then


         ints(1) = 4
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         chars(1) = 'omega'
         call errsub ( 1303, 4, 0, 1 )

      else

         omega = array(1+3*m)
         if ( omega.eq.0e0 ) then
            irotat = 0
         else
            irotat = 1
            if ( itype.eq.404 ) then


               ints(1) = ielgrp
               ints(2) = ielem
               call errsub ( 238, 2, 0, 0 )

            end if

         end if

      end if

      modelv = ind(nparm)
      ind(nparm) = 0
      if ( modelv.le.0 .or. modelv.gt.4 .and. modelv.lt.100 .or.
     .     modelv.gt.102 ) then


         ints(1) = nparm
         ints(2) = itype
         ints(3) = ielgrp
         ints(4) = ielem
         ints(5) = modelv
         chars(1) = 'modelv'
         call errsub ( 1305, 5, 0, 1 )

      else if ( modelv.le.4 ) then


         call elusr3 ( nparm, array(1+(nparm-1)*m), iuser, user )
         if ( ind(nparm).eq.0 .or. ind(nparm).lt.0 .and.
     .        array(1+(nparm-1)*m).lt.0 ) then


            ints(1) = nparm
            ints(2) = itype
            ints(3) = ielgrp
            ints(4) = ielem
            reals(1) = array(1+(nparm-1)*m)
            chars(1) = 'eta (0)'
            call errsub ( 1304, 4, 1, 1 )

         end if


         if ( modelv.ge.2 ) then


            ind(50) = 1
            call elusr3 ( 1, array, iuser, user )
            cn = array(1)
            if ( ind(1).gt.0 ) then


               ints(1) = nparm+1
               ints(2) = itype
               ints(3) = ielgrp
               ints(4) = ielem
               chars(1) = 'n (Power)'
               call errsub ( 1303, 4, 0, 1 )

            end if
            if ( cn.le.0e0 .or. cn.gt.5e0 ) then


               ints(1) = nparm+1
               ints(2) = itype
               ints(3) = ielgrp
               ints(4) = ielem
               reals(1) = cn
               chars(1) = 'n (Power)'
               call errsub ( 1305, 4, 1, 1 )

            end if

            if ( modelv.ge.3 ) then


               call elusr3 ( 1, array, iuser, user )
               clam = array(1)
               if ( ind(1).gt.0 ) then


                  ints(1) = nparm+2
                  ints(2) = itype
                  ints(3) = ielgrp
                  ints(4) = ielem
                  chars(1) = 'lambda'
                  call errsub ( 1303, 4, 0, 1 )

               end if
               if ( clam.lt.0 ) then


                  ints(1) = nparm+2
                  ints(2) = itype
                  ints(3) = ielgrp
                  ints(4) = ielem
                  reals(1) = clam
                  chars(1) = 'lambda'
                  call errsub ( 1305, 4, 1, 1 )

               end if

            end if

         end if

      end if
      if ( modelv.gt.1 .or. mconv.gt.0 ) jderiv = 1


      call prinal ( name(1:lennam), 'iuser', ipos, user, iuser )
      call prinal ( name(1:lennam), 'user', -nuser, user, iuser )


      if ( itime.eq.3 ) then
         isub = nparm+1
         isup = nparm
      else
         if ( matrix ) then
            isupcf = nparm
         else
            isupcf = nparm-1
         end if
         if ( vector ) then
            isubcf = 5
         else
            isubcf = 6
         end if

         do 150 i = isubcf , isupcf
            isub = i


            if ( ind(i) .gt. 0 ) goto 170

150      continue

         isub = isupcf + 1

170      isup = isupcf
         do 180 i = isupcf, isub , -1
            isup = i
            if ( ind(i) .gt. 0 ) goto 190

180      continue

      end if


190   do 200 i = isub, isup
         ist(i) = 0
200   continue
      do 220 i = isub, isup
         if ( ind(i).gt.0 .and. ind(i).lt.2000 ) then


            do 210 j = i+1, isup
               if ( ind(j).eq.ind(i) ) then
                  ist(j) = i
                  ind(j) = 0
               end if
210         continue
         if ( ind(i).gt.1000 ) ind(50) = 1
         end if
220   continue

      if ( ierror .ne. 0 ) call instop
      call erclos ( 'el0400' )

      end
