c **********************************************************************
c *   MKIMAGE
c *
c *   Create Imagetool file from Sepran solution vector.
c *
c *   At present the maximum number of pixels in either direction is 500
c *   The Sepran vectors should contain only one degree of freedom.
c *
c *   Common /cimage/ xmin,xmax,ymin,ymax,ipxmax
c *           xmax   Maximum x coordinate value
c *           ymax   Maximum y coordinate value
c *           ipmax  Maximum number of pixels in any direction
c *
c *   PvK 170590
c **********************************************************************
      program mkimage
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),isol(5),iu1(1),u1(1)
      common /cimage/ xmin,xmax,ymin,ymax,ipmax
      character*80 nimage,rname
      common /bstore/ fname
      character*80 fname
      data kmesh(1),kprob(1)/2*100/
      character*4 ext

c     *** Input of:
c     ***  FNAME - bsfile containing solutionvectors
c     ***  NSOL  - number of vectors to be displayed
c     ***  XMAX  - horizontal dimension of mesh
c     ***  YMAX  - vertical dimension of mesh
c     ***  IPMAX - Maximum number of pixels in direction with
c     ***          largest dimension
c     ***  MCHOIS- Choice of mesh input: Possibilities
c     ***          0     Meshdefinition is read from stdin
c     ***         -1     Mesh is read from file 'mesh2'
c     ***         >0     KMESH is read from bsfile record MCHOIS
c     ***  KCHOIS- Choice of input of  problem definition: Possibilities
c     ***          0     Definition is read from stdin
c     ***         >0     KPROB is read from bsfile record KCHOIS
c     ***  ISKIP - Number of records to be skipped in bsfile
      read(5,*) fname,rname
      read(5,*) nsol
      read(5,*) xmax,ymax,ipmax
      read(5,*) mchois,kchois
      read(5,*) iskip
c     *** Standard Sepran start and input
      call start(0,0,0,0)
      call openf2(.false.)
c     *** Mesh definition
      if (mchois.eq.0) then
         call mesh(0,iinput,rinput,kmesh)
      else if (mchois.lt.0) then
         open(9,file='mesh2')
         call meshrd(3,9,kmesh)
      else if (mchois.gt.0) then
         call readbs(mchois,kmesh,ihelp)
      endif
c     *** Problem definition
      if (kchois.eq.0) then
         call probdf(0,kprob,kmesh,iinput)
      else if (kchois.gt.0) then
         call readbs(kchois,kprob,kmesh)
      endif
c     **** Process solution vectors
      do 10 i=1,nsol
         nc = lnblnk(rname)
         write(ext,'(i3.3)') i
         nimage = rname(1:nc) // '.' // ext
         write(6,*) nimage
         nrec = i+iskip
         call readbs(nrec,isol,ihelp)
         call image(kmesh,kprob,isol,nimage)
10    continue
      end
