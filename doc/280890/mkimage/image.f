c *************************************************************
c *   IMAGE
c *   
c *   Create Imagetool rasterfile from Sepran solution vector
c *   
c *   Limitations: 
c *      Triangles with 3 nodalpoints and linear shapefunctions are 
c *      used. Maximum number of pixels in either direction is 500
c *      Solution has to contain only one degree of freedom
c *************************************************************
      subroutine image(kmesh,kprob,isol,nimage)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),ipix(160000)
      dimension a(3),b(3),c(3),iinvec(10),iresvc(10)
      dimension xn(3),yn(3),tn(3),phi(3)
      character*80 nimage
      common /cimage/ xmin,xmax,ymin,ymax,ipmax
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      logical out

      

c     *** Find information on the nodalpoint numbers of the
c     *** elements (KMESH part c), the coordinates of the
c     *** nodalpoints (KMESH part i) and the numbering of
c     *** of dof's of the solutionvector (KPROB part h)
      call ini050(kmesh(23),'image: coordinates')
      call ini050(kmesh(17),'image: nodalpoints')
      call ini055(isol,kprob,indprf,indprh,indprp,nphys,'image')
c     *** Find the pointers in ibuffr
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))
      iusol  = infor(1,isol(1))
      if (indprh.gt.0) then
c        *** the dof's are renumbered. 
         iindprh = infor(1,indprh)
      else
         iindprh = 1
      endif
      nelem = kmesh(9)

c     *** determine x and y dimensions of the mesh. The longest side
c     *** will be discretized with the ipmax pixels 
      npoint = kmesh(8)
      call imag02(ibuffr(ikelmi),npoint)
      print *,'xmin,xmax,ymin,ymax',xmin,xmax,ymin,ymax
      rlam = abs((xmax-xmin)/(ymax-ymin))
      if (rlam.lt.1.0) then
         npy = ipmax
         npx = ipmax*rlam
      else
         npx = ipmax
         npy = ipmax*1.0/rlam
      endif
c     *** total number of pixels
      ntot = npx*npy
      write(6,*) 'npx,npy = ',npx,npy

c     *** determine minimum and maximum (TMIN,TMAX) of solution vector
      call algebr(6,1,isol,i1,i1,kmesh,kprob,tmin,tmax,p,q,ipoint)
      print *,'tmin , tmax = ',tmin,tmax

c     *** determine number of pixels per unit length
      rlx = (npx-1)*1./(xmax-xmin)
      rly = (npy-1)*1./(ymax-ymin)
      do 10 ielem=1,nelem
c        *** determine coordinates of nodalpoints (xn,yn)
c        *** and nodalpoint values of isol (tn)
         call imag01(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
     v               ibuffr(iindprh),indprh,xn,yn,tn,ielem)
c        *** determine smallest rectangular area around element
c        *** Only the pixels in this area are scanned. If the
c        *** pixel is in the element a linear interpolation is
c        *** performed to determine the pixel value of the 
c        *** solutionvector
         xcmin = min(xn(1),xn(2),xn(3))
         ycmin = min(yn(1),yn(2),yn(3))
         xcmax = max(xn(1),xn(2),xn(3))
         ycmax = max(yn(1),yn(2),yn(3))
c        *** Translate this to pixelvalues
         ipxmin = (xcmin-xmin)*rlx + 1.5
         ipymin = (ycmin-ymin)*rly + 1.5
         ipxmax = (xcmax-xmin)*rlx + 1.5
         ipymax = (ycmax-ymin)*rly + 1.5
         if (ipxmin.lt.1.or.ipymin.lt.1) then
            write(6,*) 'PERROR(image) ipmin < 1'
            write(6,*) 'ipxmin,ipymin  ',ipxmin,ipymin
            stop
         endif
         if (ipymax.gt.ipmax.or.ipxmax.gt.ipmax) then
c           write(6,*) 'PERROR(image) pxmax > max'
            write(6,*) 'ipxmax,ipymax  ',ipxmax,ipymax
            stop
         endif
c        *** determine coefficients a,b,c of shapefunctions
         call trilin(xn,yn,a,b,c)
c        write(6,101) xcmin,xcmax,ipxmin,ipxmax,rlx
c        write(6,101) ycmin,ycmax,ipymin,ipymax,rly
101      format(2f12.3,2i10,f12.3)
c        *** Loop over the pixels in the rectangular area
         do 20 ipy = ipymin,ipymax
            do 20 ipx = ipxmin,ipxmax
               x = xmin + (ipx-1)/rlx
               y = ymin + (ipy-1)/rly
               out = .true.
c              *** If the pixel is positioned in the element
c              *** then the values of all basisfunctions is 
c              *** larger or equal than 0
               do 30 i=1,3
                  phi(i) = a(i)+b(i)*x+c(i)*y                
                  out = phi(i).ge.0.and.out
30             continue
c              write(6,'(''x   = '',3f12.3,i10)') x,xcmin,xcmax,ipx
c              write(6,'(''y   = '',3f12.3,i10)') y,ycmin,ycmax,ipy
c              write(6,'(''phi = '',3f12.3)') (phi(i),i=1,3)
c              write(6,*)
               if (out) then
c              *** the pixel is in the element
                  t = tn(1)*phi(1)+tn(2)*phi(2)+tn(3)*phi(3)
c                 ip = ipx + (ipy-1)*npx
c                 ** Imagetools origin is positioned in the
c                 ** upper left corner; we need to transform
c                 ** the coordinates
                  ip = ntot - ipy*npx + ipx
                  ipix(ip) = (t-tmin)/(tmax-tmin)*240+10
c                 write(6,*) 'x,y,ipix ',x,y,ipix(ip)
                  if (t.gt.1.) t=1.
                  if (t.lt.0.) t=0.
               endif
20        continue
10    continue
c     *** Now dump the pixelvalues to rasterfile
      call rasout(ntot,ipix,nimage)
      return
      end
