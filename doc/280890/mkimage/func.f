      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      a = ichois*0.5
      func = 1-y+0.1*a*sin(pi*y)*cos(pi*x)
      return
      end
