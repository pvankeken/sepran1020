c *************************************************************
c *   IMAG01
c * 
c *   Determines nodalpoints values, coordinates and solution in
c *   the nodalpoints of element iel
c *
c *   PvK 170590
c *************************************************************
      subroutine imag01(kmeshc,coor,usol,kprobh,indprh,xn,yn,tn,ielem)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),usol(*),kprobh(*),xn(*),yn(*),tn(*)
      dimension nodno(3)
 
c     print *,'IN imag01'
c     print *,'kmeshc(1) = ',kmeshc(1)
      inpelm = 3
      ip = (ielem-1)*inpelm
c     *** nodalpoint numbers in kmesh part c
      do 10 i=1,inpelm
10        nodno(i) = kmeshc(ip+i)
c     print *,(nodno(i),i=1,3)
c     *** coordinates in coor
      do 20 i=1,inpelm
         xn(i) = coor(1,nodno(i))
20       yn(i) = coor(2,nodno(i))
c     print *,'x: ',(xn(i),i=1,3)
c     print *,'y: ',(yn(i),i=1,3)
c     *** solution in usol
      do 30 i=1,inpelm
         if (indprh.eq.0) then
            tn(i) = usol(nodno(i))
         else 
            ip = kprobh(nodno(i))
            tn(i) = usol(ip)
         endif
30     continue

       return
       end
