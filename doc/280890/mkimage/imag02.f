c *************************************************************
c * IMAG02
c * 
c * Determine extrema of x- and y-coordinates
c *
c * PvK 260590
c *************************************************************
      subroutine imag02(coor,npoint)
      implicit double precision(a-h,o-z)    
      dimension coor(2,npoint)
      common /cimage/ xmin,xmax,ymin,ymax,ipmax
      xmin = coor(1,1)
      xmax = coor(1,1)
      ymin = coor(2,1)
      ymax = coor(2,1)
      do 10 i=2,npoint
         x = coor(1,i)
         y = coor(2,i)
         if (x.gt.xmax) xmax=x
         if (x.lt.xmin) xmin=x
         if (y.lt.ymin) ymin=y
         if (y.gt.ymax) ymax=y
10    continue
      return
      end
