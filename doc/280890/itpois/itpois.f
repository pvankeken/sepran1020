      program poisson
      implicit double precision (a-h, o-z)
      dimension kmesh(100),kprob(100),iwork(10),work(10)
      dimension intmat(5),matr(5),isol(5),isolan(5),iu1(1)
      dimension irhsd(5),iuser(100),user(100),contln(20)
      dimension icontr(6)
      real t0,t1

      kmesh(1)  = 100
      kprob(1)  = 100
      contln(1) =  20
      iuser(1)  = 100
      user(1)   = 100

      call start(0,0,+1,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call second(t0)
      call commat(1,kmesh,kprob,intmat)
      call bvalue(1,2,kmesh,kprob,isol,value,1,4,1,0) 
      nparm = 7 
      do 100 iparm = 1, nparm
         iwork(iparm) = 0
         work(iparm)  = 0
100   continue
      work(1) = 1
      work(3) = 1
      call fil100(1,iuser,user,kprob,nparm,iwork,work)
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol, 
     1            iuser,user,islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      iu1(1)=1
      call creavc(0,1,idum,isolan,kmesh,kprob,iu1,u1,iu2,u2)
      dif = anorm(0,3,1,kmesh,kprob,isol,isolan,ielhlp)
      call second(t1)
      write(6,'(''Direct solver:    '',f12.7,f12.4)') dif,t1-t0

      call second(t0)
      call commat(9,kmesh,kprob,intmat)
      call bvalue(1,2,kmesh,kprob,isol,value,1,4,1,0) 
      nparm = 7 
      do 101 iparm = 1, nparm
         iwork(iparm) = 0
         work(iparm)  = 0
101   continue
      work(1) = 1
      work(3) = 1
      call fil100(1,iuser,user,kprob,nparm,iwork,work)
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      icontr(1) = 0
      icontr(2) = 0
      icontr(3) = 1
      icontr(4) = 2
      iprec = 4
      icontr(5) = iprec
      eps = 1e-6
      nmax = 100
c     call congrd(-1,icontr,matr,isol,irhsd,intmat,kprob,
c    v            eps,nmax,irestr)
      call overrl(0,matr,irhsd,isol,intmat,kmesh,kprob,eps,nmax)
      iu1(1)=1
      call creavc(0,1,idum,isolan,kmesh,kprob,iu1,u1,iu2,u2)
      dif = anorm(0,3,1,kmesh,kprob,isol,isolan,ielhlp)
      call second(t1)
      write(6,'(''overrl : '',f12.7,f12.4)') dif,t1-t0
      end

      function funcbc(ichois,x,y,z)
      implicit double precision (a-h, o-z)
      parameter (pi=3.1415926532, n=1)
      funcbc = sin(n*pi*x) * exp(-n*pi*y)
      return
      end

      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      func = funcbc(ichois,x,y,z)
      return
      end
