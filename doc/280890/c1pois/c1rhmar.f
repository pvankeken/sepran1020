c *************************************************************
c *   C1RHMAR
c *
c *   Built element vector according to formula (4) in
c *   Christensen, GJRAS, 68, 487-497 (1982).
c *    e
c *   f   = sum of phi (xm,ym)*[ y(m+1) - y(m-1) ]*0.5
c *    i              i
c *         over all markers (xm,ym) in element e
c *
c *   The element is THE C1 nonconforming element with length a
c *   and height b.
c *   Information on the positions of the markers is stored in
c *   IUSER and USER.
c *
c *   PvK 040490
c *************************************************************
      subroutine c1rhmar(elemvc,iuser,user,x0,y0,a,b)
      implicit double precision(a-h,o-z)
      dimension elemvc(*),iuser(*),user(*)
      dimension fp(12),f(12)

      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

c     *** Loop over the markers. If the marker is in the element
c     *** it will contribute to the element vector
      nmark = iuser(7)
      do 5 i=1,nphi
5        f(i) = 0. 
      do 10 i=1,nmark
	 if (iuser(7+i).eq.ielem) then
	    xm = user(4+2*i)
	    ym = user(5+2*i)
	    if (i.eq.1) then
	       yfac = user(7+2*i)-ym
            else if (i.eq.nmark) then
	       yfac = ym-user(3+2*nmark)
            else
	       yfac = 0.5*(user(7+2*i)-user(3+2*i))
            endif
c           *** calculate the basisfunctions in the marker
	    xi  = (xm-x0)/a
	    eta = (ym-y0)/b
            call fphi(fp,xi,eta,a,b)
	    do 20 j=1,nphi
20             f(j) = fp(j)*yfac + f(j)
         endif
10    continue

      do 30 i=1,nphi
	 elemvc(i) = f(i)
30    continue

      return
      end
