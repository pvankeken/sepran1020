c *************************************************************
c *   C1RHCON
c *
c *   Calculate the element vector for a continuous load function g:
c *
c *    e           e           12           e  e
c *   f  =  //  g N   de   =  sum  // g(j) N  N   de 
c *    i    e      i          j=1           j  i
c *
c *         12  ngaus             e      e
c *      = sum   sum   g(j) w(k) N (k)  N (k)
c *         j     k               j      i
c *
c *   PvK 040490
c *************************************************************
      subroutine c1rhcon(f,g,a,b)
      implicit double precision(a-h,o-z)
      dimension f(*),g(*),f1(12)

      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      
      do 5  i=1,10,3
	 f1(i)   = 1d0
	 f1(i+1) = b
	 f1(i+2) = a
5     continue

      do 10 i=1,nphi
         sum = 0
	 do 20 j=1,nphi
	    sumk = 0.
	    do 30 k=1,ngaus
	       sumk = sumk + w(k)*phi(j,k)*phi(i,k)
30          continue
	    sum = sum + sumk*g(j)*f1(i)*f1(j)
20       continue
	 f(i) = sum*a*b
10    continue
  
      return
      end
