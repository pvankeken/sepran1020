c *************************************************************
c *   ELEM
c *
c *   Element matrices for THE nonconforming element
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *
c *    iuser(6)           icrhsd    Choice of rhsd vector
c *                                 0: no righthand side vector built
c *                                 1: buoyancy effect of markerchain
c *                                 2: continuous function
c *    iuser(7)           nmark     number of markers
c *    iuser(8)           elem(1)   number of elements in which the
c *      ..                ..       markers are positioned
c *    iuser(7+nmark)     elem(nmark)
c *
c *    if (icrhsd=1): array user contains from position 6 the
c *                   coordinates of the markers in the sequence
c *                   x(1),y(1),x(2),y(2),....,x(nmark),y(nmark)
c *
c *    if (ichrsd=2): array user contains from position 6 the
c *                   nodalpoint values of the continuous function g:
c *                   from 6 to 5+npoint:          g(i)
c *                   from 6+npoint to 5+2npoint:  dg(i)/dy
c *                   from 6+2npoint to 5+3npoint: -dg(i)/dx
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *       10          Equation of Poisson
c *       11          Biharmonical equation 
c *
c *   PvK 030490
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      parameter(done=1d0)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),g(12)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      nphi = 12
      if (ifirst.eq.0) then
         if (nrule.ne.0) then
c           *** Numerical integration
            if (nrule.gt.0) then
c              *** Gauss quadrature: 
c              *** fill values of shapefunctions and derivatives of
c              *** shapefunction in the nodalpoints.
c              *** Use common pegauss and peshape
   	       if (nrule.eq.1) ngaus=4
               if (nrule.eq.2) ngaus=9
	       if (nrule.eq.3) ngaus=16
               call fwgaus
	       call c1dgaus
	    else
               write(6,*) 'PERROR(elem-030490)  nrule <= 0'
	       stop
            endif
         endif
      endif

c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))

      if (vector) then
	 icrhsd = iuser(6)
	 if (icrhsd.eq.0) then
	    do 12 i=1,nphi
12             elemvc(i) = 0.
	 else if (icrhsd.eq.1) then
	    x0 = x(1)
	    y0 = y(1)
	    call c1rhmar(elemvc,iuser,user,x0,y0,a,b)
         else if (icrhsd.eq.2) then
	    do 14 i=1,10,3
	       ih     = index1((i+2)/3)
               g(i)   = user(5+ih)
	       g(i+1) = user(5+npoint+ih)
	       g(i+2) = user(5+2*npoint+ih)
14          continue
	    call c1rhcon(elemvc,g,a,b)
	 else
	    write(6,*) 'PERROR(elem-030490) icrhsd has wrong value'
	    write(6,*) '                    icrhsd = ',icrhsd
	    stop
         endif
      endif
     


      if (matrix) then

         if (itype.eq.10) then
c        *** Stiffness matrix; Galerkin weighting
            call c1dif(elemmt,a,b,done)
            return
	 else if (itype.eq.11) then
	    call c1bihi(elemmt,a,b,done)
	 else
	    write(6,*) 'PERROR(elem): itype <> 10,11 not implemented'
	    write(6,*) 'itype = ',itype
	    stop
         endif
      endif
      return
      end
