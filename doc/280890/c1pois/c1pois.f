c **********************************************************
c *   C1pois
c *
c *   Solve the Poisson equation with THE nonconforming element
c *
c *   PvK 030490
c **********************************************************************
      program c1pois
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension kmesh(100),kprob(100),iuser(100),user(5000)
      dimension intmat(5),isol(5),islold(5),irhsd(5),matr(5)
      dimension iu1(2),u1(2),ivec1(5),ivec2(5),isolan(5)
      dimension icurvs(2),funcx(100),funcy(100)
      real t0,t1,t2

      kmesh(1)=100
      kprob(1)=100
      iuser(1)=100
       user(1)=5000
      funcx(1)=100
      funcy(1)=100
      call start(0,1,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(1,kmesh,kprob,intmat)
      call presdf(kmesh,kprob,isol)

      read(5,*) nrule
      read(5,*) alpha
      call second(t0)
      iuser(6)=0

      user(31)=alpha
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,iuser,user,
     v            user,islold,ielhlp)
      call second(t1)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      call second(t2)
      write(6,*) 'System:  ',t1-t0
      write(6,*) 'Solve :  ',t2-t1
      x = 0d0
      y = 0d0
      format=7d0
      call plwin(1,x,y)
      x = format+1d0
      call plotc1(1,kmesh,kprob,isol,contln,ncntln,format,
     v            1d0,jsmoot)
      call plwin(2,x,y)
      call plotc1(2,kmesh,kprob,isol,contln,ncntln,format,
     v            1d0,jsmoot)
      call plwin(3,x,y)
      call plotc1(3,kmesh,kprob,isol,contln,ncntln,format,
     v            1d0,jsmoot)

      iu1(1)=1
      iu1(2)=2
      iu1(3)=3
      call creavc(0,1,idum,isolan,kmesh,kprob,iu1,u1,iu2,u2)
      npoint = kmesh(8)
      call pecopy(10,isol,user,kprob,6,1d0)
      call pecopy(11,isol,user,kprob,6+npoint,1d0)
      call pecopy(12,isol,user,kprob,6+2*npoint,1d0)
      open(10,file='user.out')
      rewind(10)
      do 99 i=1,npoint
	 i1 = i+npoint
	 i2 = i1 + npoint
	 write(10,'(3f12.3)') user(5+i),user(5+i1),user(5+i2) 
99    continue

      dif1 = anorm(0,3,1,kmesh,kprob,isol,isolan,ielhlp)
      dif2 = anorm(0,3,2,kmesh,kprob,isol,isolan,ielhlp)
      dif3 = anorm(0,3,3,kmesh,kprob,isol,isolan,ielhlp)
      write(6,*) dif1,dif2,dif3
      x = 0d0
      y = 0d0
      format=7d0
      call plwin(1,x,y)
      x = format+1d0
      call plotc1(1,kmesh,kprob,isolan,contln,ncntln,format,
     v            1d0,jsmoot)
      call plwin(2,x,y)
      call plotc1(2,kmesh,kprob,isolan,contln,ncntln,format,
     v            1d0,jsmoot)
      call plwin(3,x,y)
      call plotc1(3,kmesh,kprob,isolan,contln,ncntln,format,
     v            1d0,jsmoot)
      call finish(0)
      end

       function funcbc(ichois,x,y,z)
       implicit double precision(a-h,o-z)
       parameter(pi=3.1415926)
 
       if (ichois.eq.1) funcbc = x*y
       if (ichois.eq.2) funcbc = x
       if (ichois.eq.3) funcbc = -y
c      if (ichois.eq.1) funcbc = sin(pi*x)*sin(pi*y)
c      if (ichois.eq.2) funcbc = pi*sin(pi*x)*cos(pi*y)
c      if (ichois.eq.3) funcbc = -pi*cos(pi*x)*sin(pi*y)
 
       return
       end
 
       function func(ichois,x,y,z)
       implicit double precision(a-h,o-z)
       func = funcbc(ichois,x,y,z)
       return
       end
