c *************************************************************
c *   PIPEFLOW
c *
c *************************************************************
      program pipeflow
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),iuser(100),user(100)
      dimension matr(5),isol(5),irhsd(5),ipress(5),iwork(10),work(10)
      common /peinterg/ nrule

      kmesh(1) = 100
      kprob(1) = 100
      iuser(1) = 100
       user(1) = 100
      read(5,*) itertp,maxitr,eps,nrule
      call start(0,0,0,0)
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(2,kmesh,kprob,intmat)
      call bvalue(0,0,kmesh,kprob,isol,value,icrv1,icrv2,
     v            idegfd,jstap)
      call bvalue(0,2,kmesh,kprob,isol,1d0,3,3,1,0)
      iprint = 1
      ichois = -1
      call nonlin(itertp,maxitr,eps,iprint,iwork,work,intmat,
     v            ichois,iuser,user,kmesh,kprob,isol,ielhlp)
      vmax = anorm(1,3,1,kmesh,kprob,isol,isol,ihelp)
      vaver = anorm(1,4,1,kmesh,kprob,isol,isol,ielhlp)
      vaver2 = bounin(1,3,1,1,kmesh,kprob,2,2,isol,iuser,user)
      format=12d0
      call plotvc(1,2,isol,isol,kmesh,kprob,format,yfaccn,0d0)
      write(6,*) 'vmax = ',vmax
      write(6,*) 'vaver = ',vaver
      write(6,*) 'vaver2 = ',vaver2
      end

      subroutine filnln(niter,iuser,user,kmesh,kprob,isol,
     v                  realx,differ)
      implicit double precision(a-h,o-z)
      dimension iuser(*),user(*),kmesh(*),kprob(*),isol(*),
     v          iwork(10),work(10)
      relax = 0d0
      nparam = 8
      if (niter.eq.0) then
         open(9,file='flow.param') 
         rewind(9)
         do 100 i=1,nparam
100         read(9,*) iwork(i),work(i)
      else
         iwork(3) = min(2,niter)
      endif
      call fil100(1,iuser,user,kprob,nparam,iwork,work)
      return
      end


