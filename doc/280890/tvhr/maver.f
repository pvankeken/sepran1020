cdccmaver
      subroutine maver ( matr, ix, iy, intmat, kprob, ichois )
c
c ********************************************************************
c
c        programmer     Niek Praagman
c        version 8.6    date   29-09-89 (Extension for periodical boundary
c                                        conditions)
c        version 8.5    date   02-06-89 (Ad-hoc extension for Stokes iterative)
c        version 8.4    date   08-03-89 (Extension to more problems)
c        version 8.3    date   18-01-89 (Small error coreections)
c        version 8.2    date   03-10-88 (Extension of carray)
c        version 8.1    date   13-04-88
c
c
c
c   copyright (c) 1984,1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c  *  perform matrix vector multiplication :
c
c           amat * x = y
c
c     depending on the value of intmat(1) one of the subroutines
c     er0013, er0014, er0015, er0016 is called
c     the effect of boundary conditions is taken into account (ichois=odd)
c     or not (ichois=even) (subroutine er0017, er0018)
c     when matr is of type isol, the matrix is supposed to be a
c     diagonal matrix and intmat is not used
c
c ********************************************************************
c
c                       INPUT/OUTPUT
c
cimp      implicit none
      integer matr(*), ix(*), iy(*), intmat(*), kprob(*), ichois
c
c     ichois   i  choice parameter. Possibilities:
c
c       0         boundary conditions are not taken into account
c                 boundary conditions in y are left unchanged
c
c       1         boundary conditions are taken into account
c                 boundary conditions in y are left unchanged
c
c       2         boundary conditions are not taken into account
c                 boundary conditions in y are copied from x
c
c       3         boundary conditions are taken into account
c                 boundary conditions in y are copied from x
c
c       4         boundary conditions are not taken into account
c                 boundary conditions in y are made equal to 0
c
c       5         boundary conditions are taken into account
c                 boundary conditions in y are made equal to 0
c
c       6         the vector y is made equal to the boundary conditions
c                 without the matrix-vector multiplication.
c                 the positions corresponding to the boundary conditions
c                 get the value 0
c
c     intmat   i  standard sepran array containing information
c                 concerning the storage of the matrix
c                 intmat is only used when matr corresponds to a
c                 large matrix
c
c     ix       i  standard sepran array containing information of the
c                 vector x
c
c     iy       o  sepran array containing information of the vector y
c
c     kprob    i  standard sepran array containing information of the
c                 problem
c
c     matr     i  standard sepran array containing information
c                 concerning the matrix
c                 matr may be of type isol.
c                 then the matrix is supposed to be diagonal.
c
c ********************************************************************
c
c                      COMMON BLOCKS
c

      integer iinfor, infor
      common /carray/ iinfor, infor(3,1500)
c
c                 /carray/
c    Information of arrays in IBUFFR
c
c    iinfor   -   Number of types that is stored in INFOR.
c                 0 <= IINFOR <= 1500
c    infor   i/o  Information concerning the storage of arrays in IBUFFR
c                 is stored in the following way:
c     Pos. (1,i)  If > 0: array i is stored in IBUFFR from position
c                         stored in infor(1,i)
c                 If < 0: array i is stored in file 3 from record
c                         number -infor(1,i)
c     Pos. (2,i)  Length of the array in words
c     Pos. (3,i)  Priority number. Is used to decide which arrays
c                 are put to file 3. 0 <= prio <= 10

      integer ielem, itype, ielgrp, inpelm, icount, ifirst
      integer notmat, notvec, irelem, nusol, nelem, npoint
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint
c
c                 /cactl/
c    Contains element dependent information for the various element
c    subroutines. cactl is used to transport information from main
c    subroutines to element subroutines
c
c    ielem    i   Element number
c    itype    i   Type number of element
c    ielgrp   i   Element group number
c    inpelm   i   Number of nodes in element
c    icount   i   Number of unknowns in element
c    ifirst   i   Indicator if the element subroutine is called for the
c                 first time in a series (0) or not (1)
c    notmat  i/o  Indicator if the element matrix is zero (1) or not (0)
c    notvc   i/o  Indicator if the element vector is zero (1) or not (0)
c    irelem   i   Relative element number with respect to element group
c                 number ielgrp
c    nusol    i   Number of degrees of freedom in the mesh
c    nelem    i   Number of elements in the mesh
c    npoint   i   Number of nodes in the mesh
c
      integer nbuffr, kbuffr, intlen, ibfree
      common /cbuffr/ nbuffr, kbuffr, intlen, ibfree
c
c                 /cbuffr/
c    Several variables related to the common buffer ibuffr
c
c    nbuffr   i   Declared length of array ibuffr
c    kbuffr   -   Last position used in ibuffr
c    intlen   -   Length of a real variable divided by the length of
c                 an integer variable
c    ibfree  i/o  Next free position in array ibuffr
c
      integer ierror, nerror
      common /cconst/ ierror, nerror
c
c                 /cconst/
c    Error codes and number of errors to print
c
c    ierror  i/o  Error number of last error occurred
c        0        No error present
c       >0        Last run-time error
c    nerror   -   Number of error messages printed
c
      integer ifree, ioutp, itime
      common /cinout/ ifree, ioutp, itime
c
c                 /cinout/
c    Contains parameters to indicate how much SEPRAN output is required
c
c    ifree    -   Not yet used
c    ioutp    i   Indication of the amount of print output that must
c                 be performed by SEPRAN. Possibilities:
c      -1         No SEPRAN output except for error messages and
c                 output explicitly required.
c       0         Minimum output
c       1         Information of dimensions etc. is printed
c    itime    i   Indication if the CPU-time must be printed for each
c                 main subroutine (1) or not (0)
c
      integer ibuffr
      common ibuffr(1)

c
c                 Blank common
c    Working storage for all kinds of operations
c
c    ibuffr  i/o  General SEPRAN buffer array
c
c
c ********************************************************************
c
c                     LOCAL PARAMETERS
c
      logical compl, diag, compvc
      integer IPRIO, IPROB, JMETOD, NPROB, IPKPRB, K, LENGUS, NBOUND,
     .        NPBOUN, N, JCHOIS, KFREE, IPYST, LENG, IPXST, IPAMAT,
     .        NSUPER, MINNUM, LENGTH, MAXNUM, IPKPRK, IPMTRP, KAMATA,
     .        LENGPB, LENGPA, IPINT1, N1, N2, ISUPER, IRECNR, IPINT2,
     .        IPMAT3, IPKPRS, index

c
c ********************************************************************
c
c                     METHOD (PSEUDO CODE)
c
c
c
c *********************************************************************
c
c      ASKCLK
c      ER0013
c      ER0014
c      ER0015
c      ER0016
c      ER0017
c      ER0018
c      ER0021
c      ER0022
c      ER0031
c      ER0032
c      ER0052
c      ER0053
c      ER0054
c      ER0055
c      ER0056
c      ERMV01
c      ERMV02
c      ERTRAP
c      INI000
c      INI001
c      INI006
c      INI050
c      INI051
c      INIPRI
c      INSTOP
c      PRINBF
c      TOST03:  Compute effect of periodical boundary conditions (real case)
c      TOST04:  Compute effect of periodical boundary conditions (complex case)
c      READFL
c
c *********************************************************************
c
c  *  tests
c
      iprio=10
      compl=.false.
      if(ichois.lt.0 .or. ichois.gt.6) call ertrap('maver',1,2,ichois,
     .   0,0)
      if(kprob(2).ne.101) call ertrap('maver',26,1,0,0,0)
      if(ix(2).ne.110) call ertrap('maver',281,1,0,0,0)
      if(matr(2).eq.103) then
c
c      *  matrix is a large matrix
c
         iprob = intmat(1)/1000
         if ( matr(1)/1000 .ne. iprob ) call ertrap('maver',44,1,0,0,0 )
         jmetod=mod ( intmat(1),100 )
c
c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c
c        Ad-hoc extension for Stokes iteratief
c
c     jmetod = 41
c     ichois = 0:   y = B x
c
c     ichois = 1:   y = Bt x
c
         if ( jmetod .eq. 41 ) then
 
           call ini050(-matr(3),'maver: matrix')
           call ini050(-intmat(3),'maver: intmat')
           call ini050(-ix(3),'maver: x')
           nusol = kprob(5)
           if ( ichois .eq. 0 ) then
              call ini051(iy(1),nelem,'maver')
              call ermv01(ibuffr(infor(1,matr(3))),
     .           ibuffr(infor(1,intmat(3))),nelem,8,
     .           ibuffr(infor(1,ix(1))),nusol,
     .           ibuffr(infor(1,iy(1))) )
              iy(2) = 115
              iy(3) = 0
              iy(4) = 1
              iy(5) = nelem
           else
              call ini051(iy(1),nusol,'maver')
              call ermv02(ibuffr(infor(1,matr(3))),
     .           ibuffr(infor(1,intmat(3))),nelem,8,
     .           ibuffr(infor(1,ix(1))),nusol,
     .           ibuffr(infor(1,iy(1))) )
              iy(2) = 110
              iy(3) = 0
              iy(4) = 0
              iy(5) = nusol
           end if
           return
         end if
c
c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c
         if(jmetod.lt.1.or.jmetod.gt.10) call ertrap('maver',94,2,jmetod
     .       ,0,0)
         if(jmetod.eq.3 .or. jmetod.eq.4) compl=.true.
         diag=.false.
         if(intmat(2).ne.102) call ertrap('maver',89,1,0,0,0)
         if(mod(matr(1),1000).ne.jmetod) call ertrap('maver',44,1,0,
     .        0,0)
      else if (matr(2).eq.110) then
c
c      *  matrix is diagonal matrix
c
         diag=.true.
         if(matr(3).eq.1) compl=.true.
         iprob = matr(4) / 1000
      else
         call ertrap('maver',95,1,0,0,0)
      endif
      iprob  = iprob + 1
      nprob  = max ( 1, kprob(40) )
      if ( iprob .lt. 1 .or. iprob .gt. nprob )  call ertrap( 'maver',
     .     699, 2, iprob, 0, 0)
      ipkprb = 1
      do 50 k = 2, iprob
 50      ipkprb = ipkprb + kprob ( 2+ipkprb )
c
c   *  check if the vector corresponding to ix is complex
c      If so the result is also complex
c      The matrix may be real
c
      if(ix(3).ne.0) then
c
c      *  ix(3) = 1  complex vector
c
         compvc=.true.
      else
c
c      *  ix(3) = 0  real vector
c
         compvc=.false.
      endif
      if ( ix(4)/1000 .ne. iprob-1 ) call ertrap('maver',799,1,0,0,
     .     0)
c
      if(compl .and. .not. compvc) call ertrap('maver',282,1,0,0,
     .     0)
c
c   *  Set lengus:  lengus = 2, real matrix;  lengus = 3,  complex one.
c
      if(compl) then
         lengus=3
      else
         lengus=2
      endif
      nusol=kprob(4+ipkprb)
      nbound=kprob(9+ipkprb)
      npboun=kprob(33+ipkprb)
      if(ix(5).ne.nusol) call ertrap('maver',282,1,0,0,0)
      if(iy(2).eq.110 .and. (ix(3).ne.iy(3) .or. iy(5).ne.nusol))iy(2)=0
      if(ierror.ne.0) call instop
c
c    *  initialisations;   computing starting positions
c
      n=nusol-nbound-npboun
c
c   *  Set jchois:  jchois = 2, real vector;  jchois = 3,  complex one.
c
      if(compvc) then
         jchois=3
      else
         jchois=2
      endif
      kfree=0
      call ini001(jchois,.true.,iy,2,110,1,nusol,ipyst,index,kfree,
     .   'maver',iprio)
      call ini001(1,.false.,ix,2,110,1,leng,ipxst,index,kfree,
     .   'maver',iprio)
c
      if(ichois.eq.6) then
c
c       *  ichois = 6  special case
c
         n=nusol
         if(compl) n=2*n
         call ini006(ibuffr(ipyst),n)
         if(diag) return
         goto 100
      endif
c
      if(diag) then
c
c      *  diagonal matrix
c
         call ini001(1,.false.,matr,2,110,1,leng,ipamat,index,kfree,
     .      'maver',iprio)
      else
c
c     *  large matrix
c
         nsuper=kprob(28+ipkprb)
         minnum=1
         if(nsuper.eq.0) then
            call ini001(lengus,.false.,matr,2,103,3,length,ipamat,index,
     .         kfree,'maver',iprio)
            maxnum=n
         else
            call ini001(1,.false.,kprob(ipkprb),2,101,24,length,ipkprk,
     .         index,kfree,'maver',iprio)
            call ini001(1,.false.,matr,2,103,3,length,ipmtrp,index,
     .         kfree,'maver',iprio)
            kamata=intmat(5)
            lengpb=0
            if(intmat(4).gt.0) then
               lengpb=infor(2,intmat(4))-nbound-1
            endif
            kamata=kamata-lengpb
            lengpa=(kamata*intlen*(lengus-1))/2
            ipamat=max(kfree,ibfree)
            kfree=ibfree+lengpa
            call prinbf('maver',kfree)
         endif
         call ini001(1,.false.,intmat,2,102,3,leng,ipint1,index,kfree,
     .      'maver',iprio)
      endif
      call prinbf('maver',ibfree)
c
      length=1
      if(compvc) length=2
      n1=n*intlen*length
      n2=nbound*length
      if(ichois.gt.3) then
c
c      *  ichois = 4 or 5      clear end of array y
c
         call ini006(ibuffr(ipyst+n1),n2)
      else if (ichois.ge.2) then
c
c      *  ichois = 2 or 3      copy end of array x in y
c
         call ini000(ibuffr,ibuffr,ipxst+n1,ipyst+n1,n2*intlen,0)
      endif
c
      if(diag) then
c
c      *  diagonal matrix
c
         if(compl) then
c
c          *  complex matrix and vector
c
            call er0022(ibuffr(ipamat),ibuffr(ipxst),ibuffr(ipyst),n)
         else if (compvc) then
c
c          *  real matrix and complex vector
c
            call er0052(ibuffr(ipamat),ibuffr(ipxst),ibuffr(ipyst),n)
         else
c
c          *  real matrix and vector
c
            call er0021(ibuffr(ipamat),ibuffr(ipxst),ibuffr(ipyst),n)
         endif
      else
c
c     *  large matrix
c
         if(jmetod.le.4) then
            do 45 isuper=1,max(1,nsuper)
               if(nsuper.gt.0) then
c
c                * nsuper > 0
c
                  maxnum=ibuffr(ipkprk+isuper-1)
                  irecnr=ibuffr(ipmtrp+2*isuper-2)
                  length=ibuffr(ipmtrp+2*isuper-1)
                  call readfl(irecnr,ipamat,ipamat+length-1,1,3,ibuffr)
               endif
c
               goto (10,20,30,40) jmetod
c
c               *  jmetod = 1
c
 10               if(compvc) then
c
c                 *  real matrix and complex vector
c
                     call er0053(ibuffr(ipamat),ibuffr(ipxst),
     .                           ibuffr(ipyst),ibuffr(ipint1),n,minnum,
     .                           maxnum)
                  else
c
c                 *  real matrix and real vector
c
                     call er0013(ibuffr(ipamat),ibuffr(ipxst),
     .                           ibuffr(ipyst),ibuffr(ipint1),n,minnum,
     .                           maxnum)
                  endif
                  goto 45
c
c               *  jmetod = 2
c
 20               if(compvc) then
c
c                 *  real matrix and complex vector
c
                     call er0054(ibuffr(ipamat),ibuffr(ipxst),
     .                           ibuffr(ipyst),ibuffr(ipint1+n),
     .                           ibuffr(ipint1),n,minnum,maxnum)
                  else
c
c                 *  real matrix and real vector
c
                     call er0014(ibuffr(ipamat),ibuffr(ipxst),
     .                           ibuffr(ipyst),ibuffr(ipint1+n),
     .                           ibuffr(ipint1),n,minnum,maxnum)
                  endif
                  goto 45
c
c               *  jmetod = 3
c
30                call er0015(ibuffr(ipamat),ibuffr(ipxst),ibuffr(ipyst)
     .               ,ibuffr(ipint1),n,minnum,maxnum)
                  goto 45
c
c              *  jmetod = 4
c
40                call er0016(ibuffr(ipamat),ibuffr(ipxst),ibuffr(ipyst)
     .              ,ibuffr(ipint1+n),ibuffr(ipint1),n,minnum,maxnum)
45             minnum=maxnum+1
         else if(jmetod.lt.7 .or. jmetod.eq.9) then
c
c         *  jmetod = 5,6,9  real compact matrix
c
            if(compvc) then
c
c             *  real matrix and complex vector
c
               call er0055(jmetod,ibuffr(ipamat),ibuffr(ipamat+n*intlen)
     .                    ,ibuffr(ipamat+intlen*(n+ibuffr(ipint1+n))),
     .                     ibuffr(ipxst),ibuffr(ipyst),n,ibuffr(ipint1),
     .                     ibuffr(ipint1+n+1))
            else
c
c             *  real matrix and real vector
c
               call er0031(jmetod,ibuffr(ipamat),ibuffr(ipamat+n*intlen)
     .                    ,ibuffr(ipamat+intlen*(n+ibuffr(ipint1+n))),
     .                     ibuffr(ipxst),ibuffr(ipyst),n,ibuffr(ipint1),
     .                     ibuffr(ipint1+n+1))
            endif
         else
c
c     *  jmetod = 7,8,10   complex compact matrix
c
            call er0032(jmetod,ibuffr(ipamat),ibuffr(ipamat+n*intlen*2),
     .       ibuffr(ipamat+intlen*2*(n+ibuffr(ipint1+n))),ibuffr(ipxst),
     .       ibuffr(ipyst),n,ibuffr(ipint1),ibuffr(ipint1+n+1))
         endif
      endif
c
c  *  boundary conditions :
c
100   if (nbound.gt.0 .and. (ichois.eq.1 .or. ichois.eq.3 .or.
     .     ichois.ge.5) ) then
c
c       *  nbound > 0    effect of boundary conditions
c
         call ini001(1,.false.,intmat,2,102,4,leng,ipint2,index,
     .      kfree,'maver',iprio)
         kfree=ibfree
         call ini001(1,.false.,matr,2,103,5,leng,ipmat3,index,
     .      kfree,'maver',iprio)
         call prinbf('maver',ibfree)
c
         if(compl) then
c
c         *  complex case
c
            call er0018(ibuffr(ipint2),ibuffr(ipmat3),ibuffr(ipxst),
     .                ibuffr(ipyst),nusol,nbound)
         else
c
c         *  real case
c
            if(compvc) then
c
c             *  real matrix and complex vector
c
            call er0056(ibuffr(ipint2),ibuffr(ipmat3),ibuffr(ipxst),
     .                     ibuffr(ipyst),nusol,nbound)
c
            else
c
c             *  real matrix and real vector
c
               call er0017(ibuffr(ipint2),ibuffr(ipmat3),ibuffr(ipxst),
     .                     ibuffr(ipyst),nusol,nbound)
            endif
         endif
      endif
      if (npboun.gt.0) then

c        *  npboun > 0; compute effect of periodical boundary conditions

         call ini050(-matr(5),'maver')
         call ini050(-intmat(4),'maver')
         call ini050(-kprob(ipkprb+37),'maver')
         ipint2 = infor(1,intmat(4))
         ipkprs = infor(1,kprob(ipkprb+37))
         ipyst = infor(1,iy(2))
         ipmat3 = infor(1,matr(5))
         if (compl) then

c           *  complex case

            call tost04(npboun,nusol-nbound-npboun+1,
     .                  ibuffr(ipkprs+intlen*npboun),
     .                  ibuffr(ipint2+nbound),
     .                  ibuffr(ipint2+nbound+npboun+1),ibuffr(ipmat3),
     .                  ibuffr(ipyst),ibuffr(ipkprs),1)

         else

c           *  real case

            call tost03(npboun,nusol-nbound-npboun+1,
     .                  ibuffr(ipkprs+intlen*npboun),
     .                  ibuffr(ipint2+nbound),
     .                  ibuffr(ipint2+nbound+npboun+1),ibuffr(ipmat3),
     .                  ibuffr(ipyst),ibuffr(ipkprs),1)

         endif
      endif
      do 500 k = 2,5
         iy(k)=ix(k)
500   continue
      if(itime.eq.1) call askclk('maver')
      call inipri
      end
cdc*eor
