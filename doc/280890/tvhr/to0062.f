cdccto0062
      subroutine to0062 ( islold, istold, kprob )
c
c ********************************************************************
c
c
c        programmer     Guus Segal
c        version  1.0   date   01-09-88
c        version  2.0   date   20-10-88 (Complete revision; new parameter list)
c
c
c   copyright (c) 1988  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c
c ********************************************************************
c
c     Fill array istold with information of old solution vectors.
c     Reserve space for the necessary arrays
c
c     Help subroutine for TO0050  (SYSTEM)
c
c ********************************************************************
c
c                 input / output
c

cimp      implicit none
      integer islold, istold, kprob
      dimension kprob(*), islold(5,2), istold(9,2)
c
c   islold  i   This array is used if ichois < 0, or numold > 0
c               When ichois < 0, it is supposed to consist of one vector
c               only, which must be of the same structure as the solution
c               vector
c               When numold > 0, islold contains numold vectors which may
c               be solution vectors of several problems or vectors of
c               special structure (type 115) resulting of several problems
c
c   istold  i   Array of length numold x 9, in which some information
c               concerning the various arrays in islold is stored
c               Contents
c
c               istold(1,k)   Starting address of array in IBUFFR
c               istold(2,k)   Indication of the type of vector
c                             Possible values:
c
c                             0   Vector is of the same type as the sol. vector
c                            -1   Vector has not been filled
c                             j   Vector has been filled and has the same
c                                 structure as the jth vector in islold
c
c               istold(3,k)   ipkprf for specific array
c               istold(4,k)   ikprbf for specific array
c               istold(5,k)   ipkprh for specific array
c               istold(6,k)   ikprbh for specific array
c               istold(7,k)   Problem number jprob for this array
c               istold(8,k)   ipkprb for specific problem number jprob
c               istold(9,k)   index in the sense of INI001 for specific array
c
c   kprob   i   Standard SEPRAN array containing information of the problem
c
c ********************************************************************
c
c
c                      Parameters in common blocks
c

      integer         jmetod, numns, nrusol, ichnun, nunkp, nbound,
     .                npltra, ndim, nelgrp, invmat, kprobo, nbcons,
     .                lengrl, ipkprb, iprob, numold, nsuper, lengpa,
     .                lengpb, nprob, nsol, imas, kelgrp, numnat, nindex,
     .                kelma, ipoint, idummy
      common /csubwk/ jmetod, numns, nrusol, ichnun, nunkp, nbound,
     .                npltra, ndim, nelgrp, invmat, kprobo, nbcons,
     .                lengrl, ipkprb, iprob, numold, nsuper, lengpa,
     .                lengpb, nprob, nsol, imas, kelgrp, numnat, nindex,
     .                kelma, ipoint(50,2), idummy(74)

c
c                       /csubwk/
c     Integer work space of length 200 for top subroutines in SEPRAN
c     With this local common, local parameters may be transported from
c     top to lower level subroutines
c     The meaning of csubwk may vary from subroutine to subroutine
c
c     jmetod   -   type of storage used for the large matrix
c     numns    -   number of element groups (including boundary elements)
c     nrusol   -   number of non-prescribed degrees of freedom
c     ichnun   -   parameter ichois for subroutine to0000
c     nunkp    -   maximal number of degrees of freedom in a nodal point
c     nbound   -   number of prescribed degrees of freedom
c     npltra   i   Number of points with local transformations
c     ndim     -   Dimension of space
c     nelgrp   -   Number of element groups
c     invmat   i   Indication if the inverse of the element matrix must be
c                  computed and assembled (1) or not (0)
c     kprobo   -   Start address of kprob part o in array KPROB (if > 0)
c     nbcons   -   Number of element groups with boundary conditions of the
c                  type u = constant
c     lengrl   -   The length of one variable in the matrix or right-hand side
c                  expressed in words (=integers)
c     ipkprb   i   Start address in array kprob for problem iprob
c     iprob    i   Actual problem number
c     numold   i   Number of vectors stored in islold
c     nsuper   i   number of super elements (If zero, no super elements are
c                  used)
c     lengpa   i   Length in integers required for first part of matrix
c     lengpb   i   Length in integers required for second part of matrix
c     nprob    i   Number of problems that may be solved in this program
c     nsol     i   Number of degrees of freedom expressed in reals
c     kelgrp   i   Number of nodes in standard elements accumulated
c     numnat   i   Number of standard boundary elements
c     nindex   i   Help parameter for the length of array index3
c     kelma    o   Indication if the number of nodes in the elements is
c                  constant (kelma=0) or not (kelma>0)
c     ipoint   o   Work array of length 50 x 2 to store pointers for ibuffr
c
c                  ipoint is filled as follows
c                  Reference to permanent arrays:
c
c                  k     (k,1)      (k,2)      Meaning of array
c
c                   1    ipkprf     ikprbf    KPROB part f
c                   2    ipkprh     ikprbh    KPROB part h
c                   3    ipcoor     ikcoor    Array COOR containing co-ordinates
c                   4    ipint1     ikint1    Array INTMAT part 1
c                   5    ipint2     ikint2    Array INTMAT part 2
c                   6    ipusol     ikusol    Solution array USOL
c                   7    ipmatr     ikmatr    Array MATR for super elements
c                   8    ipmat      ikmat     First part of matrix (NSUPER = 0)
c                   9    ipmatb     ikmatb    Secnd part of matrix (NSUPER = 0)
c                  10    ipmatc     ikmatc    Third part of matrix (NSUPER = 0)
c                                             This part corresponds to the
c                                             matrix built from inverted element
c                                             matrices
c                  11
c                  12    ipkprn     ikkprn    KPROB part n
c                  13    ipolsl     ikolsl    ISLOLD  (ICHOIS<0)
c                  14    iprhsd     ikrhsd    IRHSD Right-hand-side vector
c                  15    ipkpri     ikkpri    KPROB part i
c                  16    ipkmsc     ikkmsc    KMESH part c
c                  17    ipkmsm     ikkmsm    KMESH part m
c                  18    ipmass     ikmass    Mass matrix
c                  19    ipkprm     ikkprm    KPROB part m (NSUPER>0)
c                  20    ipkprk     ikkprk    KPROB part k (NSUPER>0)
c                  21    ipkprl     ikkprl    KPROB part l (NSUPER>0)
c                  22
c                  23
c                  24
c                  25
c
c
c                            Local pointers
c
c                       Pointer    Length         meaning
c
c                  31    ipiwtr     nusol     Work array to indicate unknowns
c                                             to be transformed (npltra>0)
c                  32    ipstld    9*numold   Array containing start positions
c                                             and other information concerning
c                                             the old solution vectors
c                  33    ipmat1  lengpa*intlen  First block of super element
c                                               matrix
c                        ipmat2 = ipmat1 + lengpa/2   Second block  (nsuper>0)
c                  34    ipmatb  lengpb*intlen  Part of matrix corresponding to
c                                               boundary conditions (nsuper>0)
c                  35    ipkpmh   see kprobm  Actual part of KPROB m (nsuper>0)
c                  36    iparr     larray     Local real work array ARRAY
c                  37    ipiarr    liarry     Local integer work array IARRAY
c
c                                      2
c                  38    ipelem  icount * lengrl  Element matrix
c                  39    ipelvc  icount * lengrl  Element vector
c                  40    ipelms  icount * lengrl  Element mass matrix
c                  41    ipind1    inpelm     Array INDEX1
c                  42    ipind2    icount     Array INDEX2
c                  43    ipind3  numold*nindex  Array INDEX3 (numloc>0)
c                  44    ipind4  numold*inpelm  Array INDEX4 (numloc>0)
c                  45    ipindx    icount     Array
c                  46    ipinds    2*icount     Array
c                        ipindt=ipinds+icount   Array
c
c                                      2
c                  47    ipelmp  icount * lengrl  Copy of element matrix
c                  48    ipiwrk    - - -      Integer work array
c                  49
c                  50
c
c     idummy   -   Dummy work array
c     imas     i   Choice parameter for the vector corresponding to massmt
c                  imas = 0 means massmt is not created, imas = 1 means it will
c                  be created
c
c

c
c ********************************************************************
c
c                      Subroutines called
c
c     SEPRAN subroutines
c
c     SERVICE:
c
c     ERWARN:   Warnings
c     INI001:   Compute starting addresses of arrays in IBUFFR
c
c
c ********************************************************************
c
c                      Local parameters

      integer k, itypvc, length, kfree, iprio, jprob, j, kpivec,
     .        kcopy, jpkprb

c
c    k, j     Loop variables
c
c    itypvc   Type number of vector
c
c    iprio     Priority parameter for arrays in array IBUFFR
c
c    kfree     Help parameter to compute starting positions in ibuffr
c
c    length    Output parameter of subroutine INI001 that is not actually used
c
c    jprob     Problem number corresponding to specific vector
c
c    kcopy     Actual value of j that indicates the vector that must be copied
c
c    jpkprb    Actual value of ipkprb corresponding to a specific vector
c
c    kpivec    Starting position in KPROB with respect to number of unknowns
c              for array of special stucture of type 115
c
c ********************************************************************
c
c    *  Loop over all solution vectors
c
      iprio=10
      do 200 k = 1, numold

         itypvc = islold(2,k)
         if ( itypvc .ne. 110 .and. itypvc .ne. 115
     .        .and. itypvc .ne. 116 ) then

c         *  itypvc # 110, 115 or 116:  this type of array is not supported

            call erwarn ( 'system  (to0050,to0062)', 720, 2, k, 0, 0 )
            istold ( 1,k ) = 0
            istold ( 2,k ) = -1
            go to 200

         end if

c       *  Find starting position of array islold

         call ini001 ( 112, .false.,islold(1,k), 2, itypvc, 1, length,
     .                 istold(1,k), istold(9,k), kfree,
     .                 'system (to0050,to0062)', iprio )
         jprob = islold(4,k) / 1000 + 1
         istold ( 7,k ) = jprob

c      *  Test if array has a new structure or an old one

         if ( jprob.eq.iprob .and. itypvc.eq.110 ) then

c         *  jprob = iprob  and itypvc = 110, so index3 = index2

            istold ( 2,k ) = 0
            istold ( 3,k ) = ipoint(1,1)
            istold ( 4,k ) = ipoint(1,2)
            istold ( 5,k ) = ipoint(2,1)
            istold ( 6,k ) = ipoint(2,2)
            istold ( 8,k ) = ipkprb
c
         else

c         *  jprob # iprob  check if new problem number is found or that old
c                           must be copied

            kcopy = 0
            do 100 j = 1, k-1
               if ( istold ( 7,j ) .eq. jprob ) then

c               *  Problem numbers are identical, now check type of vector

                  if ( islold(k,2) .eq. islold(j,2) .and.
     .                 islold(k,4) .eq. islold(j,4) ) then
                     kcopy = j
                     goto 110
                  end if
               end if
100         continue

110         if ( kcopy .eq. 0 ) then

c            *  kcopy = 0, hence a new array has been found

               istold ( 2,k ) = k
               jpkprb = 1
               do 120 j = 2, jprob
120               jpkprb = jpkprb + kprob ( jpkprb + 2 )

c            *  Fill array istold and compute starting positions of
c               corresponding arrays kprobf and kprobh

               istold ( 8,k ) = jpkprb
               if ( itypvc .eq. 110 ) then

c               *  itypvc = 110, standard situation

                  call ini001 ( 111, .false., kprob(jpkprb), 2, 101, 19,
     .                          length, istold(3,k), istold(4,k), kfree,
     .                          'system (to0050,to0062)', iprio )
                  call ini001 ( 1, .false., kprob(jpkprb), 2, 101, 21,
     .                          length, istold(5,k), istold(6,k), kfree,
     .                          'system (to0050,to0062)', iprio )

               else if ( itypvc .eq. 115 ) then

c               *  itypvc = 115, Array of special structure 115

                  kpivec = kprob(jpkprb+17) - 1 + kelgrp *
     .                     kprob(5+jpkprb) + mod ( islold(4,k),1000 )
                  if ( kprob(jpkprb-1+kpivec ) .lt. 0 ) then

c                  *  Constant number of degrees of freedom, no kprobf

                     istold(3,k) = 1
                     istold(4,k) = 0

                  else

c                 *  Number of degrees of freedom not constant
c                    Compute starting positions for kprobh

                     call ini001 ( 111, .false., kprob(jpkprb), 2, 101,
     .                             kpivec, length, istold(3,k),
     .                             istold(4,k), kfree,
     .                             'system (to0050,to0062)', iprio )
                  end if

c               *  For arrays of special structure part h has not been filled

                  istold(5,k) = 1
                  istold(6,k) = 0

               else

c               *  itypvc = 116,  special vector

                  istold(3,k) = 1
                  istold(4,k) = 0
                  istold(5,k) = 1
                  istold(6,k) = 0

               end if
c
            else

c            *  kcopy # 0, copy information from array kcopy

               do 150 j = 2, 8
150               istold (j,k) = istold ( j,kcopy )

            end if

         end if

200   continue
      end
cdc*eor
