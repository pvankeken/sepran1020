c *************************************************************
c *   CORRH
c *
c *   Solves    .
c *           M T  + S T  = f
c *   using a CN scheme. The system is non-linear, S depends on time.
c *************************************************************
c *   Parameters:
c *      kmesh     i   Standard Sepran array
c *      kprob     i   Standard Sepran array
c *      intmat    i   Information of the large matrix for this problem
c *      isol      o   Solution vector T(n+1)
c *      islold    i   Old solution vector T(n): output of predictorstep
c *      (i)user   i   Contains information on coefficients
c *      numold    i   number of solutionvectors in ivcold
c *      ivcold    i   'old solution array' used to transport 
c *                    information between problems
c *      matrs     i   Stiffness matrix S(n)
c *      matrm     i   Mass matrix (assumed to be constant in time)
c *      irhsd     i   Righthandside vector f(n)
c * 
c *   290889 PvK
c *************************************************************
      subroutine corrh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,matrm,irhsd)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),isol(*),islold(*),user(*)
      dimension iuser(*),matrs(*),matrm(*),irhsd(*)
      dimension matrs1(5),irhs1(5),ivec1(5),ivec2(5),ivec3(5)
      real t2,t3
      character*10 tname

      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peparam/ rayleigh,rlam,idia

      t2step = tstep*0.5d0
c     *** Built S and f at time t(n+1) using predicted velocity
      call systm0(11,matrs1,intmat,kmesh,kprob,irhs1,isol,
     v            iuser,user,islold,ielhlp)

c     *** If irhsd is time-dependent  do something with f(n)
c     *** irhsd = dt/2*(irhsd+irhs1)
c     call algebr(3,0,irhsd,irhs1,irhsd,kmesh,kprob,t2step,t2step,p,q,i)
     
c     *** ivec1 = M*uold  add it to irhsd*dt
      ichois=idia+3
      call maver(matrm,islold,ivec1,intmat,kprob,ichois)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,tstep,1d0,p,q,i)

c     *** S1 = M + dt/2*S1
      call addmat(kprob,matrs1,matrm,intmat,t2step,alpha2,1d0,beta2)

c     *** ivec1 = S1*usol(p) ; add it to irshd
      call maver(matrs1,isol,ivec1,intmat,kprob,6)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,-1d0,p,q,i)

c     *** ivec1 = S*uold ; add it to irhsd
      call maver(matrs,islold,ivec1,intmat,kprob,5)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,-t2step,p,q,i)

c     *** solve the system
      call solve(0,matrs1,isol,irhsd,intmat,kprob)

c     *** Delete matrices from memory
      call pein56(matrs1(3),0)
      call pein56(matrs1(4),0)
      call pein56(matrs1(5),0)
      call peini3(ibuffr)

      return
      end
