      program tvhr
      implicit double precision(a-h,o-z)
      parameter(NPUS=8010,NPUS2=16010)
      parameter(NFUNC=500)
      parameter(NBUFDEF= 5 000 000)
c **********************************************************
c *   TVH
c *
c *   Modelling time dependent convection using a predictor
c *   corrector approach.
c *
c *   A viscous fluid layer is heated from below. Symmetry
c *   conditions are imposed at the vertical boundaries at
c *   x=0 and x=1. The boundaries are stress free.
c *
c *   The equations are solved on different meshes; 
c *   User arrays are used to transport information
c *   PvK 160589
c ******************************************************************
c *   Used files:
c *      tvh.in      Standard input file. Contains Sepran input
c *                   and extra parameters.
c *      tvh.back    File for backingstorage. Contains the
c *                   solution vector.
c *      tvh.data    Output file: information concerning the
c *                   number of iterations and used cpu time
c **********************************************************************
      dimension kmesh1(100),kmesh2(100),kprob1(100),kprob2(100)
      dimension iuser(100),user(NPUS2),matr1(5),matr2(5),matrm(5)
      dimension irhs1(5),irhs2(5),intmt1(5),intmt2(5),isol1(5),isol2(5)
      dimension islol1(5),islol2(5),iu1(10),u1(10),icurvs(3),istrm(5)
      dimension funcx(NFUNC),funcy(NFUNC),igradt(5)
      dimension contln(100),indcol(100)
      dimension ipredsol(5),ivser(100),vser(NPUS)
      real t0,t1,tnew
      integer restart

      common ibuffr(NBUFDEF)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,
     v               jkader,jtimes
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /usertext/ text,text1,text2
      common /frcount/ ifrinit,ifrcntr,nbase
      common /bstore/ f2name
      common /peparam/ rayleigh,rlam,idia
      common /peheat/ q
      common /margins/ xmargin,ymargin
      common /seppl2/ fname
      common /pegrid/ dxm,dym
      character*80 text,text1,text2,f2name,fname,pname
      character*10 tname

      icount=0
      kmesh1(1)=100
      kmesh2(1)=100
      kprob1(1)=100
      kprob2(1)=100
      iuser(1)=100
       user(1)=NPUS2
      ivser(1)=100
       vser(1)=NPUS
      contln(1)=100
      indcol(1)=100
       funcx(1)=NFUNC
       funcy(1)=NFUNC
       f2name = 'tvhr.back'

      tname = 'main'
      call prtime(tname)
      call start(1,1,1,0)
      nbuffr=NBUFDEF
      open(9,file='mesh1')
      rewind(9)
      call meshrd(2,9,kmesh1)
      open(9,file='mesh2')
      rewind(9)
      call meshrd(2,9,kmesh2)
      close(9)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
      call commat(1,kmesh1,kprob1,intmt1)
      call commat(2,kmesh2,kprob2,intmt2)
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)
      nsup1 = kprob1(29)
      nsup2 = kprob2(29)
      write(6,*) 'NSUPER: ',nsup1,nsup2
      if (nsup1.ne.0.or.nsup2.ne.0) stop
      npoint = kmesh1(8)
      if (npoint.gt.NPUS-10) then
	 write(6,*) 'PERROR: npoint too large for user arrays'
	 write(6,*) 'npoint = ',npoint
	 stop
      endif
      read(5,*) tend,tstmin,rayleigh,rlam,q
      read(5,*) nitermax,ncor,noutput,nback,npout
      read(5,*) accmin
      read(5,*) idia,nfac
      read(5,*) format

      iu1(1)=0
      iu1(2)=0
      call creavc(0,1,idum,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
      f2name='restart.back'
      call openf2(.false.)
      call readbs(1,isol2,ihelp)
      fname='RPLOT'
      ifrinit=0



c     *** Start  time integration
      istop = 0
      difmax = 0d0
      niter = 0
      open(7,file='tvh.restart')
      rewind(7)
      read(7,*) t
      close(7)
      call second(t0)
      tnew = t0
      open(9,file='tvhr.der')
      rewind(9)
      open(7,file='tvhr.data')
      rewind(7)
      f2name='tvhr.back'
      call openf2(.true.)
      open(10,file='back.info')
      rewind(10)
      tname='init'
      call prtime(tname)
100   continue
        niter = niter+1

c *******************************************************************
c *                   PREDICTOR
c *******************************************************************
	call pecof(1,kmesh1,kprob2,isol2,iuser,user)
        call pestoh(kmesh1,kprob1,intmt1,isol1,islol1,user,
     v              iuser,matr1,irhs1)
c       *** determine timestep

c       **** Predict the temperature field at time t+dt by an implicit
c       **** Euler step
c       call pedets(kmesh1,kprob1,isol1,dtcf1)
	call pecof(2,kmesh2,kprob1,isol1,iuser,user)
	call pedtcf(kmesh2,user,dtcfl)
cdtcfl = min(dtcf2,dtcf1)
	if (dtcfl.ge.tstmin) then
	   tstep = dtcfl/nfac
        else
	   tstep = tstmin
        endif
        call ieulh(kmesh2,kprob2,intmt2,isol2,islol2,user,
     v              iuser,matr2,matrm,irhs2)
        call copyvc(isol2,ipredsol)

c *********************************************************************
c *                   CORRECTOR
c *********************************************************************
999     continue
c       *** Calculate the velocity from the predicted temperature field
        call pecof(1,kmesh1,kprob2,isol2,iuser,user)
        call pestoh(kmesh1,kprob1,intmt1,isol1,islol1,user,
     v              iuser,matr1,irhs1)
c       *** Apply CN to obtain the corrected temperature
        call pecof(2,kmesh2,kprob1,isol1,iuser,user)
        call corrh(kmesh2,kprob2,intmt2,isol2,islol2,
     v          user,iuser,matr2,matrm,irhs2)
        acc = pedetacc(kmesh2,kprob2,islol2,ipredsol,isol2)
        if (acc.gt.accmin) then
	     call copyvc(isol2,ipredsol)
	     write(6,*) 'Correction: acc= ',acc
	     goto 999
	endif
        write(tname,'(''niter '',i4)') niter
	call prtime(tname)
        if ((niter/noutput)*noutput.eq.niter) then
          call frstack(0)
          write(text,105) t
105       format('T: t = ',f12.5)
106       format('S: t = ',f12.5)
	  jkader=-4
	  xmargin=1d0
          call plotc1(number,kmesh2,kprob2,isol2,contln,ncntln,format,
     v                yfaccn,jsmoot)
	  call frstack(0)
	  write(text,106) t
	  call stream(1,ivec,istrm,0,psiphi,kmesh1,kprob1,isol1)
	  call plotc1(number,kmesh1,kprob1,istrm,contln,ncntln,format,
     v                yfaccn,jsmoot)
          f2name='restart.back'
          call openf2(.true.)
          call writbs(0,1,numarr,'temperature',isol2,ihelp)
	  call writb1
          open(11,file='tvh.restart')
          rewind(11)
          write(11,*) t
	  close(11)
        endif
        if ((niter/npout)*npout.eq.niter) then
	    icount=icount+1
            write(pname,'(''pplot.'',i3.3)') icount
            call plotp(kmesh2,kprob2,isol2,pname)
c    call pewrit(kmesh,kprob,isol2)
	    write(10,*) t
	    call flush(10)
	endif
 
c       *************************************************************
c       **** Derivation of Nu,Vrms,q1,q2,q3,q4
c       *************************************************************
	call chtype(3,kprob2)
        call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v               isol2,iuser,user,ielhlp)  
c       *** Nusselt number
	temp1 = bounin(1,1,1,1,kmesh2,kprob2,1,1,isol2,iuser,user)
	gnusselt = -bounin(2,3,1,1,kmesh2,kprob2,3,3,igradt,iuser,
     v             user)/temp1
c       *** Vrms
	call pevrms(isol1,kmesh1,kprob1,ivser,vser)
	vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v                 vser,ihelp)/rlam
	vrms = sqrt(vrms2)
c       **** q1-q4
        icurvs(1) = -1
	icurvs(2) = 3
	icurvs(3) = 3
	call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
	nunkp=funcy(5)
	q2 = -funcy(6)
	q1 = -funcy(5+nunkp)
	icurvs(2) = 1
	icurvs(3) = 1
	call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
	q4 = -funcy(6)
	q3 = -funcy(5+nunkp)
1001    format(f10.7,': ',4f15.7)	
	write(9,*) t,gnusselt,vrms,q1,q2,q3,q4
        call flush(9)

        if (t.ge.tend.or.niter.ge.nitermax) istop=1
      open(8,file='tvh.stop')
      rewind(8)
      read(8,*) nstop
      close(8)
      if (nstop.gt.0) istop=1
      tlast = tnew
      call second(tnew)
      dcpu = tnew - tlast
      open(8,file='run.info')
      rewind(8)
      write(8,*) niter,t,tnew-t0
      close(8)
      write(7,'(i4,3f12.7,f12.9,f12.2)') 
     v          niter,t,dtcfl,tstep,acc,dcpu
      call flush(7)
      if (istop.eq.0) goto 100
     
      close(9)
      close(7)

      call frstack(1)
c     *** Plot velocity and temperature
      iway=2
      format=10d0
      chheight=0.3d0
      ifrinit=0
      jkader=-4
      call plwin(1,0d0,0d0)
      fname='CPLOT'
      text = 'Velocity'
      call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,yfaccn,
     v             0d0)
      x = format+1d0
      call plwin(3,x,0d0)
      text = 'Temperature'

      call plotc1(number,kmesh2,kprob2,isol2,contln,ncntln,format,
     v            yfaccn,jsmoot)
      f2name='restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'temperature',isol2,ihelp)
      call writb1
      open(11,file='tvhr.restart')
      rewind(11)
      write(11,*) t
      call finish(1)
      call second(t1)
c     write(6,1000) t1-t0
1000  format('CPU: ',f12.4)

      end

      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      common /peparam/ rayleigh,rlam,idia
    
      if (ichois.eq.1) then
         wpi = pi/rlam
         func = 0.01*sin(pi*y)*cos(wpi*x) + (1-y)
      else if (ichois.eq.2) then
	 func = -y
      else if (ichois.eq.3) then
	 func = x
      endif
  
      return
      end
