c *************************************************************
c *   BLRMAS
c *
c *   Built Galerking weighted mass matrix for the bilinear triangles
c *
c *              1
c *   M     =   //  N N  dxdy   =
c *    ij       0    i j 
c *
c *   PvK 300190
c *************************************************************
      subroutine blrmas(elemmt,a,b)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)

      a1 = a*b/36
      a2 = a*b/18
      a4 = a*b/9

      elemmt(1) = a4 
      elemmt(2) = a2 
      elemmt(3) = a1 
      elemmt(4) = a2 
      elemmt(5) = a2 
      elemmt(6) = a4 
      elemmt(7) = a2 
      elemmt(8) = a1 
      elemmt(9) = a1 
      elemmt(10)= a2 
      elemmt(11)= a4 
      elemmt(12)= a2 
      elemmt(13)= a2 
      elemmt(14)= a1 
      elemmt(15)= a2 
      elemmt(16)= a4 

      return
      end
