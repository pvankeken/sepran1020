c *************************************************************
c *   PRTIME
c *
c *   Print elapsed time, system time and user time
c *
c *   PvK 150290
c *************************************************************
      subroutine prtime(name)
      character*10 name
      common /petime/ et0,st0,ut0
      dimension mtime(3)
      data ifirst/0/

      if (ifirst.eq.0) then
	 call timeus(ut0)
	 call timesys(st0)
	 call itime(mtime)
	 et0 = mtime(1)*3600+mtime(2)*60+mtime(3)
	 write(6,*) name,'Start of timing'
	 ifirst=1
      else
	 call timeus(ut1)
	 call timesys(st1)
	 call itime(mtime)
	 et1 = mtime(1)*3600+mtime(2)*60+mtime(3)
	 udt = ut1-ut0
	 sdt = st1-st0
	 edt = et1-et0
	 write(6,'(''Time: '',a10,3f12.3)') name,udt,sdt,edt
	 call flush(6)
      endif
      return
      end
	 
       


