cdccini050
      subroutine ini050 ( index, name )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 1.0  date   27-12-88
c        version 2.0  date   12-01-89 ( Reduction of parameter list )
c
c
c   copyright (c) 1988,1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     Check if an already existing array is positioned in array IBUFFR or not
c     If the array is available in IBUFFR, then the priority is set equal
c     to 10, and the subroutine returns
c
c     When the array is only available on backing storage, and not in IBUFFR
c     then the array is read from file 3 and space is reserved in array IBUFFR
c
c     if no space is available in array IBUFFR, INI050 tries to
c     create space in the following ways:
c
c       -  by writing low priority arrays to backing storage
c
c       -  if this is not sufficient by compressing
c          array ibuffr after writing of all arrays with priority
c          smaller than 10 to backing storage
c
c     INI050 replaces the old subroutine INI001 (for existing arrays)
c
c ********************************************************************
c
c        input / output
c
cimp      implicit none
      integer index
      character * (*) name
c
c     index   i  The absolute value of index refers to array INFOR in common
c                block CARRAY.
c                This means that the positions (i,|INDEX|) (i=1,2,3) in INFOR
c                contain all the information about the storage of the array
c
c                If index is positive then it is supposed that the array is
c                changed in the calling subroutine, otherwise (index<0)
c                the arrays remains unchanged
c
c     name    i  name of calling subroutine
c
c ********************************************************************
c
c                    COMMON BLOCKS
c

      integer ibufst, inback, lenbac
      common /carbac/ ibufst, inback(1500), lenbac(1500)
c
c                 /carbac/
c    Help common block for the organization of arrays in IBUFFR and on
c    backing storage
c
c    ibufst   o   First free position to be used in IBUFFR
c    inback  i/o  Integer array of length 1500 in which of each array that
c                 is referenced in array INFOR in common CARRAY is given
c                 whether the last version is also stored on backing storage
c                 inback(i) > 0, or not inback(i) <= 0.
c                 When inback(i) > 0, the value of inback(i) gives the record
c                 number from which array i is stored,
c                 if inback(i) = 0, the array has never been stored on file 3,
c                 and if inback(i) < 0, then |inback(i)| gives the record
c                 number from which a preceding version of array has been stored
c    lenbac  i/o  Integer array of length 1500 corresponding to inback
c                 For each ARRAY i with INBACK(i)#0, LENBAC(i) contains the
c                 the number of records that are used by the last version of
c                 array i that has been written to file 3
c

      integer nbuffr, kbuffr, intlen, ibfree
      common /cbuffr/ nbuffr, kbuffr, intlen, ibfree
c
c                 /cbuffr/
c    Several variables related to the common buffer ibuffr
c
c    nbuffr   i   Declared length of array ibuffr
c    kbuffr   -   Last position used in ibuffr
c    intlen   -   Length of a real variable divided by the length of
c                 an integer variable
c    ibfree  i/o  Next free position in array ibuffr
c

      integer ibuffr
cdc      level 2, //
      common ibuffr(1)
c
c                 Blank common
c    Working storage for all kinds of operations
c
c    ibuffr  i/o  General SEPRAN buffer array
c

      integer iinfor, infor
      common /carray/ iinfor, infor(3,1500)
c
c                 /carray/
c    Information of arrays in IBUFFR
c
c    iinfor   -   Number of types that is stored in INFOR.
c                 0 <= IINFOR <= 1500
c    infor   i/o  Information concerning the storage of arrays in IBUFFR
c                 is stored in the following way:
c     Pos. (1,i)  If > 0: array i is stored in IBUFFR from position
c                         stored in infor(1,i)
c                 If < 0: array i is stored in file 3 from record
c                         number -infor(1,i)
c     Pos. (2,i)  Length of the array in words
c     Pos. (3,i)  Priority number. Is used to decide which arrays
c                 are put to file 3. 0 <= prio <= 10
c
c
c ********************************************************************
c
c                    LOCAL PARAMETERS
c

      integer indfor, length, number, ipstar
c
c     indfor    Help parameter to store the record number from which the
c               array has been stored in file 3
c
c     number    Absolute value of index
c
c     length    length of the array in words
c
c     ipstar    computed starting position in array IBUFFR
c
c ********************************************************************
c
c                              I/O
c
c     If necessary arrays are written to and/or read from file 3 (iref3)
c
c ********************************************************************
c
c                SUBROUTINES CALLED
c
c     SEPRAN service subroutines:
c
c     INI049:  Create space in IBUFFR by writing low priority arrays to file 3,
c              or to compress IBUFFR
c     ERTRAP:  Gives error message
c     READFL:  Read array from backing storage
c     PRINBF:  Print and check length of array IBUFFR
c
c ********************************************************************
c
c                Error Messages:
c
c      17:  Fatal error: too few space in array IBUFFR
c     802:  Fatal error: the array from which the start address must be found
c                        does not exist
c
c ********************************************************************
c
c                      Method
c
c     If ( INDEX = 0 or INDEX > IINFOR ) then
c
c         Error: Array does not exist;  STOP
c
c     Set priority
c
c     If ( array on file 3 ) then
c
c         If ( enough space in IBUFFR after IBFREE ) then
c
c             IPSTAR = IBFREE
c             Raise IBFREE
c
c         else
c
c             Create space by writing low priority arrays to file 3
c             or compressing IBUFFR
c
c         Read array from file 3
c
c     Set INBACK ( |INDEX| ) negative if nessary
c
c ********************************************************************
c
      number = abs (index)
      if ( index .eq. 0 .or. number.gt.iinfor ) then

c      *  index < 0: error

         call ertrap ( name, 802, 1, 0, 0, 0 )
         call instop

      end if

c    *  Set priority 10 in INFOR

      infor ( 3,number ) = 10
      length = infor ( 2,number )
      ipstar = infor ( 1,number )

      if ( ipstar .lt. 0 ) then
c
c         *  ipstar < 0  array in file 3
c
         indfor = -ipstar
         if ( ibfree+length-1 .le. nbuffr ) then

c         *  ibfree+length-1 <= nbuffr:  Read array in IBUFFR
c                                        compute ipstar and raise ibfree
c            For a 32-bits computer, ibfree must always be odd

            infor ( 1,number ) = ibfree
            ipstar = ibfree
            ibfree = ibfree+length
            if ( intlen.eq.2 .and. mod ( ibfree, 2 ).eq.0 ) ibfree =
     .               ibfree+1

         else
c
c         *  try to create free space by writing low priority arrays
c                to backing storage   ( file 3 ) or compressing ibuffr
c
            call ini049 ( length, ipstar, name )
c !!!!!!!!!!    
            infor(1,number) = ipstar
c !!!!!!!!!!
         endif

         call prinbf ( name, ibfree )
         call readfl ( indfor, ipstar, ipstar+length-1, 1, 3, ibuffr )

      endif


c    *
      if ( index.gt.0 .and. inback (number).gt.0 )
     .      inback ( number ) = -inback ( number )
      end
cdc*eor
