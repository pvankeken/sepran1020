c *****************************************************************
c *   IEULH
c *
c *   The temperature equation is solved using implicit euler
c *   Information concerning the coefficients of the equation
c *   are stored in (i)user and ivcold.
c * 
c *
c *   Pvk  160589/290889
c ****************************************************************
      subroutine ieulh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,matrm,irhsd)
      implicit double precision(a-h,o-z)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /peparam/ rayleigh,rlam,idia
      
      dimension kmesh(*),kprob(*),intmat(*),isol(*),user(*),iuser(*)
      dimension matrs(*),matrm(*),irhsd(*)
      dimension islold(*),ivec1(5),ivec2(5),ivec3(5),matr1(5)
      dimension ipser(10),pser(10)
      character*10 tname

      save ifirst
      data ifirst/0/

      call copyvc(isol,islold)
      if (idia.eq.2) then
         call chtype(1,kprob)
c        *** Built matrices and rhs vector at time t
c        *** Stiffness matrix
         call systm0(11,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         if (ifirst.eq.0) then
c           *** Non-diagonal mass matrix
            call chtype(2,kprob)
            ifirst=1
            call systm0(11,matrm,intmat,kmesh,kprob,mrhsd,isol,
     v                  iuser,user,islold,ielhlp)
         endif
      else
c        *** Stiffness matrix and lumped mass matrix
         if (ifirst.eq.0) then
	    ifirst= 1
            imas  = 1
         else
	    imas  = 0
         endif
	 call systm1(11,imas,matrs,intmat,kmesh,kprob,irhsd,matrm,
     v               isol,iuser,user,islold,ielhlp)
      endif

      t = t+tstep
c     *** Built the system of equations
     
c     *** M*uold
      ichois=idia+3
      call maver(matrm,islold,ivec1,intmat,kprob,ichois)

c     *** tstep*f + M*uold
      call algebr(3,0,ivec1,irhsd,ivec3,kmesh,kprob,1d0,tstep,p,q,ip)

c     *** M + tstep*S
      call copymt(matrs,matr1,kprob)
      call addmat(kprob,matr1,matrm,intmat,tstep,alpha2,1d0,beta2)

c     *** ( M + tstep*S ) * usol(p)
      call maver(matr1,isol,ivec2,intmat,kprob,6)

c     *** tstep*f + M*uold - (M+tstep*S) * usol(p)
      call algebr(3,0,ivec3,ivec2,ivec1,kmesh,kprob,1d0,-1d0,p,q,ip)

c     *** Solve the system
c     write(6,*) 'IEULER: solve'
      call solve(0,matr1,isol,ivec1,intmat,kprob)

c     *** Delete temporary array from datastructure
      call pein56(matr1(3),0)
      call pein56(matr1(4),0)
      call pein56(matr1(5),0)
      call peini3(ibuffr)

      return
      end
