c *************************************************************
c *   SOLSTR
c *
c *   Solves the Poisson equation
c *
c *      grad div Psi  = - Omega
c *
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for Psi
c *       itemp   i  Vector containing Omega in the nodal points
c *       matr    i  Stiffness matrix (constant in this problem)
c *
c *   PvK 021189
c *************************************************************
      subroutine solstr(kmesh,kprob,istrm,ivort,intmat,matr,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),ivort(*),intmat(*)
      dimension irhsd(5),iuser(*),user(*),matr(*)

   
      npoint = kmesh(8)
      if (npoint+7.gt.user(1)) then
         write(6,*) 'PERROR(solstr): array user too small'
         stop
      endif


c     *** Calculate rhs vector and solve the system
      iuser(6)  = 7
      iuser(7)  = -6
      iuser(8)  = 0
      iuser(9)  = -6
      iuser(10) = 0
      iuser(11) = 0
      iuser(12) = 0
      iuser(13) = 2001
      iuser(14) = 7
       user(6)  = 1d0
      factor = -1d0
      call pecopy(0,ivort,user,kprob,7,factor)
      call systm0(2,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v            user,islold,ielhlp)
      call solve(2,matr,istrm,irhsd,intmat,kprob)

      return
      end
