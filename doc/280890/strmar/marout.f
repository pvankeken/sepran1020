c *************************************************************
c *   MAROUT
c *
c *   Post processing of solution vectors obtained by STRMAR
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marout(kmesh,kprob,istrm,ivort,imark,
     v                  coormark,iuser,user)
      parameter(NUM=750)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),ivort(*),imark(*),coormark(*)
      dimension iuser(*),user(*),xmc(NUM),ymc(NUM),ivelx(5),ively(5)
      character pname*80,chrbf*4
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax

      nmark = imark(1)
      if (nmark.gt.NUM) then
         write(6,*) 'PERROR(marout): nmark too large'
         stop
      endif
 
c     *** Plot preparations
      do 10 i=1,nmark
         xmc(i) = coormark(2*i-1)
10       ymc(i) = coormark(2*i)
      iway=2
      fname='PLOT'
      tittext = 'vorticity'
      yfaccn = 2
      jkader=-4
      format=6d0
      chheight = 0.3d0
c     *** Plot vorticity and markers
      call plwin(1,0d0,0d0)
      call peframe(1)
      call plotc1(1,kmesh,kprob,ivort,contln,0,format,yfaccn,jsmoot)
      call peframe(3)
      call pecurvep(nmark,xmc,ymc,format,yfaccn)
c     *** Plot streamfunction and markers
      call plwin(2,format+1d0,0d0)
      tittext = 'streamfunction'
      call peframe(1)
      call plotc1(1,kmesh,kprob,istrm,contln,0,format,yfaccn,jsmoot)
      call peframe(3)
      call pecurvep(nmark,xmc,ymc,format,yfaccn)
c     *** Derive velocity from streamfunction      
      call deriva(1,1,2,1,0,ivelx,kmesh,kprob,istrm,istrm,iuser,user,i)
      call deriva(1,1,1,1,0,ively,kmesh,kprob,istrm,istrm,iuser,user,i)
      call algebr(3,1,ively,ively,ively,kmesh,kprob,-1d0,0d0,p,q,i)
      call plwin(3,format+1d0,0d0)
      tittext = 'velocity'
      call peframe(1)
      call plotvc(-1,-1,ivelx,ively,kmesh,kprob,format,yfaccn,0d0)
      call peframe(3)
      call pecurvep(nmark,xmc,ymc,format,yfaccn)
      velxmax = anorm(1,3,1,kmesh,kprob,ivelx,ivelx,ielhlp)
      velymax = anorm(1,3,1,kmesh,kprob,ively,ively,ielhlp)
      strmax  = anorm(1,3,1,kmesh,kprob,istrm,istrm,ielhlp)
      volmax  = anorm(1,3,1,kmesh,kprob,ivort,ivort,ielhlp)

      write(6,1000) velxmax,velymax,strmax,volmax
1000  format(/,'vxmax = ',f15.7,/,'vymax = ',f15.7,/,
     v         'strmax= ',f15.7,/,'volmax= ',f15.7)
 
      write(chrbf,'(i4)') 1000+ifrcntr
      pname='p.'//chrbf(2:4)
      open(9,file=pname)
      rewind(9)
      write(9,*) 2,imark(1)
      do 20 i=1,nmark
20       write(9,*) coormark(2*i-1),coormark(2*i)
      
      
      return
      end
