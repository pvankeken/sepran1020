c *************************************************************
c *   MARSTART
c *
c *   Starts the Sepran program STRMAR. 
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marstart(kmesh,kprob,istrm,ivort,intmat,imark,
     v                    coormark,y0,dm,nbufdef)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),ivort(*),intmat(*),imark(*)
      dimension coormark(*),iu1(1),u1(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pelinqua/ itypel,ishape

      read(5,*) itypel,istart
      read(5,*) nmark,y0,wavel,ainit,dm
      read(5,*) nitermax,nout,ncor
      read(5,*) tfac,tmax,tstepmax,tvalid

c     *** Standard Sepran start
      kmesh(1) = 100
      kprob(1) = 100
      call start(0,1,0,0)
      nbuffr = nbufdef
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call presdf(kmesh,kprob,istrm)
      call presdf(kmesh,kprob,ivort)
      call commat(1,kmesh,kprob,intmat)
      nsuper = kprob(29)
      write(6,'(''NSUPER = '',i2)') nsuper
      if (nsuper.ne.0) stop

c     *** Initial condition for streamfunction and markerchain
      iu1(1) = 0
       u1(1) = 0
      call creavc(0,1,ivec,istrm,kmesh,kprob,iu1,u1,iu2,u2)
      if (itypel.eq.3) then
c        *** linear triangle
         ishape = 1
      else if (itypel.eq.4) then
c        *** quadratic triangle
         ishape = 2
      endif
      call pefilxcyc(ishape,kmesh,kprob,isol)
        
      if (istart.eq.0) then
c        *** Initial run
         rm1 = (xcmax-xcmin)/(nmark-1)
         piw = 3.1415926/wavel
         do 20 i=1,nmark
            x = rm1*(nmark-i)
            y = y0 + ainit*cos(piw*x)
            coormark(2*i-1) = x
            coormark(2*i) = y
20       continue
         imark(1) = nmark
      else if (istart.eq.1) then
c     *** Read coordinates of markers from file 'marker.init' (p-format)
         open(9,file='marker.init')
         rewind(9)
         read(9,*) i,nmark
         do 30 i=1,nmark
30          read(9,*) coormark(2*i-1),coormark(2*i)
         imark(1) = nmark
      endif
      return
      end
