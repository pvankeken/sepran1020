c *************************************************************
c *   DETVEL
c *
c *   PvK 151189
c *************************************************************
      subroutine detvel(kmesh,kprob,istrm,imark,coormark,velmark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),imark(*),coormark(*)
      dimension velmark(*),up(2)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      common /pelinqua/ itypel,ishape

      call ini050(kmesh(23),'detvel: coordinates')
      call ini050(kmesh(17),'detvel: nodalpoints')
      call ini055(istrm,kprob,indprf,indprh,indprp,nphys,'detvel')
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))
      iusol  = infor(1,istrm(1))
      if (indprh.gt.0) then
         iindprh = infor(1,indprh)
      else
         iindprh = 1
      endif
 
      nmark = imark(1)
      do 10 i=1,nmark
         xm = coormark(2*i-1)
         ym = coormark(2*i)
         call pedetel(ishape,xm,ym,iel)
         if (itypel.eq.3) then
            call peint02(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
     v                   ibuffr(iindprh),indprh,xm,ym,iel,up)
         else if (itypel.eq.4) then
            call peint03(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
     v                   ibuffr(iindprh),indprh,xm,ym,iel,up)
         else
            write(6,*) 'PERROR(detvel): itypel <> 3,4 not implemented'
            stop
         endif
         velmark(2*i-1) = up(1)
         velmark(2*i)   = up(2)
10    continue     
      return
      end

c *************************************************************
c *   PEINT02
c *
c *   Find interpolated velocity in point (xm,ym) which lies in 
c *   element IEL. The element is linear and the velocity has to
c *   be derived from the streamfunction USOL.
c *  
c *   PvK 151189
c *************************************************************
      subroutine peint02(kmeshc,coor,usol,kprobh,indprh,xm,ym,iel,up)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),usol(*),kprobh(*),up(*)
      dimension nodno(3),x(3),y(3),psi(3)

c     *** Nodalpoint numbers are given in kmeshc
      inpelm = 3
      ip = (iel-1)*inpelm
      do 10 i=1,inpelm
10       nodno(i) = kmeshc(ip+i)
c     *** Coordinates in coor
      do 20 i=1,inpelm
         x(i) = coor(1,nodno(i))
20       y(i) = coor(2,nodno(i))
c     *** Solution is usol
      do 30 i=1,inpelm
         if (indprh.eq.0) then
            psi(i) = usol(nodno(i))
         else
            ip = kprobh(nodno(i))
            psi(i) = usol(ip)
         endif
30    continue

c     *** vx is given by   psi(1)*c1 + psi(2)*c2 + psi(3)*c3
c     *** vy is given by  -psi(1)*b1 - psi(2)*b2 - psi(3)*b3
      b1 = y(2)-y(3)
      b2 = y(3)-y(1)
      b3 = y(1)-y(2)
      c1 = x(3)-x(2)
      c2 = x(1)-x(3)
      c3 = x(2)-x(1)
      delta = -c3*b1 + b3*c1
      delinv = 1d0/delta
      up(1) = (psi(1)*c1 + psi(2)*c2 + psi(3)*c3)*delinv
      up(2) = -1d0*(psi(1)*b1 + psi(2)*b2 + psi(3)*b3)*delinv

      return
      end

c *************************************************************
c *   PEINT03
c *
c *   Find interpolated velocity in point (xm,ym) which lies in 
c *   element IEL. The element is quadratic and the velocity has to
c *   be derived from the streamfunction USOL.
c *  
c *   PvK 211189
c *************************************************************
      subroutine peint03(kmeshc,coor,usol,kprobh,indprh,xm,ym,iel,up)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),usol(*),kprobh(*),up(*)
      dimension nodno(6),x(6),y(6),psi(6)

c     *** Nodalpoint numbers are given in kmeshc
      inpelm = 6
      ip = (iel-1)*inpelm
      do 10 i=1,inpelm
10       nodno(i) = kmeshc(ip+i)
c     *** Coordinates in coor
      do 20 i=1,inpelm
         x(i) = coor(1,nodno(i))
20       y(i) = coor(2,nodno(i))
c     *** Solution is usol
      do 30 i=1,inpelm
         if (indprh.eq.0) then
            psi(i) = usol(nodno(i))
         else
            ip = kprobh(nodno(i))
            psi(i) = usol(ip)
         endif
30    continue

      delta = (x(3)-x(1))*(y(5)-y(3)) - (y(3)-y(1))*(x(5)-x(3)) 
      delinv = 1d0/delta
      b1 = (y(3)-y(5))*delinv
      b2 = (y(5)-y(1))*delinv
      b3 = (y(1)-y(3))*delinv
      c1 = (x(5)-x(3))*delinv
      c2 = (x(1)-x(5))*delinv
      c3 = (x(3)-x(1))*delinv
      a1 = -b1*x(5)-c1*y(5)
      a2 = -b2*x(5)-c2*y(5)
      a3 = -b3*x(1)-c3*y(1)

c     *** quadratic interpolation
      u =     psi(1)*(4*c1*c1*ym + 4*a1*c1-c1 + 4*b1*c1*xm) 
      u = u + psi(3)*(4*c2*c2*ym + 4*a2*c2-c2 + 4*b2*c2*xm) 
      u = u + psi(5)*(4*c3*c3*ym + 4*a3*c3-c3 + 4*b3*c3*xm) 
      u = u + 4*psi(2)*(2*c1*c2*ym + a1*c2+a2*c1 + b1*c2*xm+b2*c1*xm)
      u = u + 4*psi(4)*(2*c2*c3*ym + a2*c3+a3*c2 + b2*c3*xm+b3*c2*xm)
      u = u + 4*psi(6)*(2*c3*c1*ym + a3*c1+a1*c3 + b3*c1*xm+b1*c3*xm)

      v =   - psi(1)*(4*b1*b1*xm + 4*a1*b1-b1 + 4*b1*c1*ym)
      v = v - psi(3)*(4*b2*b2*xm + 4*a2*b2-b2 + 4*b2*c2*ym)
      v = v - psi(5)*(4*b3*b3*xm + 4*a3*b3-b3 + 4*b3*c3*ym)
      v = v - 4*psi(2)*(2*b1*b2*xm + a1*b2+b1*a2 + b2*c1*ym+b1*c2*ym)
      v = v - 4*psi(4)*(2*b2*b3*xm + a2*b3+b2*a3 + b3*c2*ym+b2*c3*ym)
      v = v - 4*psi(6)*(2*b3*b1*xm + a3*b1+b3*a1 + b1*c3*ym+b3*c1*ym)

      up(1) = u
      up(2) = v

      return
      end
