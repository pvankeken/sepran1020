c *************************************************************
c *   CORCOOR
c *
c *   Corrects the position of the markers
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine corcoor(imark,coormark,coornewm,velmark,velnewm)
      implicit double precision(a-h,o-z)
      dimension imark(*)
      dimension coormark(2,*),velmark(2,*),velnewm(2,*),coornewm(2,*)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny

      nmark = imark(1)
      do 10 i=1,nmark
         do 20 j=1,2
20          coornewm(j,i)=coormark(j,i) +
     v                    tstep*0.5d0*(velmark(j,i)+velnewm(j,i))
         if (coornewm(1,i).lt.xcmin) coornewm(1,i) = xcmin
         if (coornewm(1,i).gt.xcmax) coornewm(1,i) = xcmax
         if (coornewm(2,i).lt.ycmin) coornewm(2,i) = ycmin
         if (coornewm(2,i).gt.ycmax) coornewm(2,i) = ycmax
10    continue

      return
      end
