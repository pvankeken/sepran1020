      subroutine pecopy(ichpro,isol,user,kprob,istart,factor)
      implicit double precision (a-h,o-z)
      logical new
      dimension isol(*),kprob(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      iprob = ichpro / 1000
      ichois = ichpro - 1000*iprob
      if(ichois.ne.1) then
         new=.false.
         iprob = isol(4)/1000 + 1
      else
         new=.true.
         iprob = iprob + 1
      endif
      nprob = max ( 1,kprob(40) )
      if ( iprob .lt. 1 .or. iprob .gt. nprob )  call ertrap( 'pecopy',
     .     699, 2, iprob, 0, 0)
      ipkprb = 1
      do 100 k = 2, iprob
100      ipkprb = ipkprb + kprob ( 2+ipkprb )

      if (isol(2).eq.110) then
c        *** Vector isol is of type solution vector
         iprio=10
         kfree=0
         itypvc=110
         nusol=kprob(ipkprb+4)
         call ini001(1,.false.,kprob(ipkprb),2,101,21,length,ipkprh,
     v               ikprbh,kfree,'pecopy',iprio)
         call ini001(2,.true.,isol,2,itypvc,1,nusol,ipusol,index,
     v               kfree,'pecopy',iprio)
         call prinal('pecopy','user',-nusol-5,user,iuser)
         call pecop1(ichois,nusol,ibuffr(ipusol),ibuffr(ipkprh),ikprbh,
     v               user(istart),factor)
         if(ichois.eq.1) then
            isol(2)=110
            isol(3)=0
            isol(4) = (iprob-1)*1000
            isol(5)=nusol
         endif
      else if (isol(2).eq.115) then
c         *** vector of special structure: no renumbering
          if (ichois.ne.0) then
             write(6,*) 'PERROR(pecopy): ichois<>0 not yet implemented'
             write(6,*) '                for vector type 115'
          endif
          call ini050(isol(1),'pecopy: ivec')
          iisol = infor(1,isol(1))
          nusol = kprob(ikprb+4)
          call pecop2(nusol,ibuffr(iisol),user(istart),factor)
      endif
      return
      end
          
c *************************************************************
c *   PECOP2
c *   Copies usol into array user. usol is given only in the
c *   vertices, so (with the assumption that usol is a piecewise
c *   linear function) we have to average the solution in to
c *   neighbouring nodalpoints to obtain usol in the secondary points.
c *   For each nodalpoint the two numbers of the two neighbouring
c *   nodal points are given in file secmesh, which can be created
c *   with program MAKEMESH.
c *   PvK 081189
c *************************************************************
      subroutine pecop2(nusol,usol,user,factor)
      implicit double precision(a-h,o-z)
      dimension usol(*),user(*)
      data ifirst/0/

      open(9,file='secmesh')
      rewind(9)
      read(9,*) npoint
      if (factor.eq.0d0) factor=1d0
      j = 0
c     *** fill positions of primary nodalpoints
      do 100 i=1,npoint
         read(9,*) ip1,ip2
         if (ip1.eq.0) then
            j = j + 1
            user(i) = factor*usol(j)
         endif
100   continue
      rewind(9)
      read(9,*) npoint
      do 200 i=1,npoint
         read(9,*) ip1,ip2
         if (ip1.ne.0) then
            user(i) = 0.5*(user(ip1)+user(ip2))
         endif
200   continue
      close(9)
      return
      end

      subroutine pecop1(ichois,nusol,usol,kprobh,ikprbh,user,factor)
      implicit double precision (a-h,o-z)
      dimension usol(*),kprobh(*),user(*)

      if (factor.eq.0d0) factor=1d0
      if(ichois.eq.0) then
         if(ikprbh.eq.0) then
            do 100 i=1,nusol
100            user(i)=factor*usol(i)
         else
            do 110 i=1,nusol
110            user(i)=factor*usol(kprobh(i))
         endif
      else if(ichois.eq.2 .or. ichois.eq.3) then
         if(ikprbh.eq.0) then
            do 150 i=ichois-1,nusol,2
150            user((i+1)/2)=factor*usol(i)
         else
            do 160 i=ichois-1,nusol,2
160            user((i+1)/2)=factor*usol(kprobh(i))
         endif
      else
         if(ikprbh.eq.0) then
            do 200 i=1,nusol
200            usol(i)=factor*user(i)
         else
            do 210 i=1,nusol
210            usol(kprobh(i))=factor*user(i)
         endif
      endif
      end
