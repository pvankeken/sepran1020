c *************************************************************
c *   ELEM
c *
c *   In this version: creates the elementvector according to
c *   formula (4) in Christensen,GJRAS,68,487-497 (1982).
c * 
c *      e
c *     f   =  sum of   phi (xm,ym) * [ y(m+1) - y(m-1) ] * 0.5
c *      i                 i
c *            over all markers (xm,ym) in element e.
c *
c *************************************************************
c *   INPUT:
c *
c *   In iuser(6) is the number of markers NMARK stored.
c *   In iuser(7)..iuser(6+NMARK) is stored in which element
c *   each marker is positioned.
c *   In user(6)..user(5+NMARK) are the coordinates of the
c *   markers stored in the sequence xm1,ym1,xm2,ym2,....
c *
c *   PvK 141189
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,vector,
     v                index1,index2)
      implicit double precision(a-h,o-z)
      logical matrix,vector
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*),uold(*)
      dimension index1(*),index2(*)
      integer ielem, itype, ielgrp, inpelm, icount, ifirst,
     .        notmat, notvec, irelem, nusol, nelem, npoint
      common /pelinqua/ itypel,ishape
      common /cactl/ ielem, itype, ielgrp, inpelm, icount, ifirst,
     .               notmat, notvec, irelem, nusol, nelem, npoint

      if (matrix) then
         write(6,*) 'PERROR(elem): matrix = .true.'
         stop
      endif
      if (inpelm.ne.3.and.inpelm.ne.6) then
         write(6,*) 'PERROR(elem): only linear and quadratic'
         write(6,*) '              triangles implemented'
         stop
      endif

      nmark = iuser(6)

      if (itypel.eq.4) goto 1000


c     ************** LINEAR TRIANGLE ****************
c     *** Initialize elementvector and get coordinates of nodalpoints
      f1 = 0.
      f2 = 0.
      f3 = 0.
      ih = 2*index1(1)
      x1 = coor(ih-1)
      y1 = coor(ih)
      ih = 2*index1(2)
      x2 = coor(ih-1)
      y2 = coor(ih)
      ih = 2*index1(3)
      x3 = coor(ih-1)
      y3 = coor(ih)
    
c     *** Each basisfunction is given by  phi(x,y) = a+b*x+c*y
c     *** Calculate for each function a,b,c
      b1 = (y2-y3)
      b2 = (y3-y1)
      b3 = (y1-y2)
      c1 = (x3-x2)
      c2 = (x1-x3) 
      c3 = (x2-x1)
      delta = -c3*b1 + b3*c1
      delinv = 1d0/delta
      a1 = -b1*x3 - c1*y3
      a2 = -b2*x3 - c2*y3
      a3 = -b3*x1 - c3*y1



c     *** Loop over the markers. If the marker is in the element
c     *** it will contribute to the elementvector.
      do 20 i=1,nmark
         if (iuser(6+i).eq.ielem) then
            xm = user(4+2*i)
            ym = user(5+2*i)
            if (i.eq.1) then
                yfac = user(7+2*i)-ym
            else if (i.eq.nmark) then
                yfac = ym-user(3+2*nmark)
            else
                yfac = 0.5*(user(7+2*i)-user(3+2*i))
            endif
            ym1 = user(7+2*i)
            ym2 = user(3+2*i)
            ph1 = delinv*(a1+b1*xm+c1*ym)
            ph2 = delinv*(a2+b2*xm+c2*ym)
            ph3 = delinv*(a3+b3*xm+c3*ym)
            f1  = f1 + ph1*yfac
            f2  = f2 + ph2*yfac
            f3  = f3 + ph3*yfac
            if (ph1.lt.0d0.or.ph2.lt.0d0.or.ph3.lt.0d0) then
               write(6,*) 'PWARNING(elem): value of basisfunction'
               write(6,*) '                less than zero in a marker'
            endif
         endif
20    continue
         
      elemvc(1) = f1
      elemvc(2) = f2
      elemvc(3) = f3
      return


1000  continue
c     *************** QUADRATIC TRIANGLE **************
      f1 = 0.
      f2 = 0.
      f3 = 0.
      f4 = 0.
      f5 = 0.
      f6 = 0.
      ih = 2*index1(1)
      x1 = coor(ih-1)
      y1 = coor(ih)
      ih = 2*index1(3)
      x2 = coor(ih-1)
      y2 = coor(ih)
      ih = 2*index1(5)
      x3 = coor(ih-1)
      y3 = coor(ih)
    
c     *** Each linear basisfunction is given by  rl(x,y) = a+b*x+c*y
c     *** Calculate for each function a,b,c
      b1 = (y2-y3)
      b2 = (y3-y1)
      b3 = (y1-y2)
      c1 = (x3-x2)
      c2 = (x1-x3) 
      c3 = (x2-x1)
      delta = -c3*b1 + b3*c1
      delinv = 1d0/delta
      a1 = -b1*x3 - c1*y3
      a2 = -b2*x3 - c2*y3
      a3 = -b3*x1 - c3*y1
      do 30 i=1,nmark
         if (iuser(6+i).eq.ielem) then
            xm = user(4+2*i)
            ym = user(5+2*i)
            if (i.eq.1) then
                yfac = user(7+2*i)-ym
            else if (i.eq.nmark) then
                yfac = ym-user(3+2*nmark)
            else
                yfac = 0.5*(user(7+2*i)-user(3+2*i))
            endif
            rl1 = delinv*(a1+b1*xm+c1*ym)
            rl2 = delinv*(a2+b2*xm+c2*ym)
            rl3 = delinv*(a3+b3*xm+c3*ym)
            ph1 = rl1*(2*rl1-1)
            ph3 = rl2*(2*rl2-1) 
            ph5 = rl3*(2*rl3-1)
            ph2 = 4*rl1*rl2
            ph4 = 4*rl2*rl3
            ph6 = 4*rl3*rl1
            f1  = f1 + yfac*ph1
            f2  = f2 + yfac*ph2
            f3  = f3 + yfac*ph3
            f4  = f4 + yfac*ph4
            f5  = f5 + yfac*ph5
            f6  = f6 + yfac*ph6
         endif
30    continue
      elemvc(1) = f1
      elemvc(2) = f2
      elemvc(3) = f3
      elemvc(4) = f4
      elemvc(5) = f5
      elemvc(6) = f6
      return

      end
