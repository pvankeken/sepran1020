      program strmar
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF=4 000 000)
c *************************************************************
c *   STRMAR
c *
c *   Solves the Stokes equation for RT-instabilities using the
c *   streamfunction-vorticity formulation. 
c *
c *   Following equations are solved:
c *
c *       grad div Omega = dGAMMA/dx
c *
c *       grad div Psi   = - Omega
c *
c *
c *   where Omega is vorticity, Psi is streamfunction and
c *   GAMMA is a stepfunction GAMMA=GAMMA(x,z).
c *
c *   PvK 141189
c *************************************************************
      parameter(NUM1=1005,NUM2=16005,NUM3=NUM1-5)
      dimension kmesh(100),kprob(100),istrm(5),ivort(5)
      dimension isold(5),matr(5),iuser(NUM1),user(NUM2),cooroldm(2,NUM3)
      dimension intmat(5),imark(1),coormark(2,NUM3),coornewm(2,NUM3)
      dimension velmark(2,NUM3),velnewm(2,NUM3)
      common ibuffr(NBUFDEF)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      data iuser(1),user(1)/NUM1,NUM2/
      real t0,t1,t2,t3

      call marstart(kmesh,kprob,istrm,ivort,intmat,imark,coormark,
     v             y0,dm,NBUFDEF)
      t = 0d0
      niter = 0
      inout  = 0
      call second(t0)
100   continue
        niter = niter+1
        inout = inout+1
        call second(t1)

        call solmar(kmesh,kprob,ivort,intmat,matr,iuser,user,imark,
     v              coormark)
        call solstr(kmesh,kprob,istrm,ivort,intmat,matr,iuser,user)
        call second(t2)
        call detvel(kmesh,kprob,istrm,imark,coormark,velmark)
        cpu1 = t2-t1
        rk = velmark(2,imark(1))/(coormark(2,imark(1))-y0) 
        call pedetcfl(imark,velmark)
        tstep =  dtcfl*tfac
        if (niter.eq.1) write(6,'(''rk,cpu'',f12.9,f12.2)') rk,cpu1
        if (tstep.gt.tstepmax.and.t.lt.tvalid) tstep = tstepmax
        call predcoor(coormark,velmark,coornewm,imark)
        do 10 i=1,ncor
           call solmar(kmesh,kprob,ivort,intmat,matr,iuser,user,imark,
     v                 coornewm)
           call solstr(kmesh,kprob,istrm,ivort,intmat,matr,iuser,user)
           call detvel(kmesh,kprob,istrm,imark,coornewm,velnewm)
           call copmar(imark,coornewm,cooroldm)
           call corcoor(imark,coormark,coornewm,velmark,velnewm)
           difcor1 = accmar(1,imark,coornewm,cooroldm)
           difcor2 = accmar(2,imark,coornewm,cooroldm)
c          write(6,11) t+tstep,ncor,difcor1,difcor2
11      format('corrector: ',f12.2,i3,2f12.7)
10      continue
        call copmar(imark,coornewm,coormark)

        call second(t2)
        dcpu = t2-t1
        t = t+tstep
        h = coormark(2,imark(1))
        write(6,'(i3,f12.2,f12.7,f12.7,f12.2)') niter,t,h-y0,rk,dcpu
        call flush(6)
        if (inout.eq.nout) then
           inout=0
           call marout(kmesh,kprob,istrm,ivort,imark,coormark,
     v                 iuser,user)
        endif
        if ((niter/5)*5.eq.niter) then 
           call remarker(imark,coormark,coornewm,dm,NUM3)
           call copmar(imark,coornewm,coormark)
        endif
        open(9,file='strmar.stop')
        rewind(9)
        read(9,*) istop
        if (niter.ge.nitermax.or.t.ge.tmax) istop=1
        if (istop.eq.0) goto 100
      call second(t3)
      write(6,'('' Total CPU-time: '',f12.2)') t3-t0
      call crepf2(imark,coormark,'marker.p')

      call finish(0)
      end

c *************************************************************
c *   FITLKK
c *
c *   Determine growth factor from a least-squares fit
c *   PvK 231189
c *************************************************************
      subroutine fitlkk(niter,t,h)
      implicit double precision(a-h,o-z)
      dimension t(*),h(*)
      do 10 i=1,niter
10       h(i) = log(h(i))
      call lkk(t,h,niter,am,bm,chi2m,siga,sigb)
      write(6,20) am,siga
20    format('lkk:   am = ',f12.9,' +/- ',f12.9)
      return
      end

c **********************************************************
c *   LKK
c *
c *   Least square fitting to a straight line y=Ax+B
c *   Double precision version
c *   PvK  201288 (231189)
c **********************************************************
      subroutine lkk(x,y,N,A,B,chi2,sigA,sigB)
      implicit double precision(a-h,o-z)
      dimension x(*),y(*)
      double precision mu
      
      if (N.le.2) then
         write(6,*) 'LKK: Number of points too low'
      endif

c     *** Calculate average values for x,y,xx,yy,xy
      xav=0.
      yav=0.
      x2av=0.
      y2av=0.
      xyav=0.
      do 10 i=1,N
         xav = xav + x(i)
         yav = yav + y(i)
         x2av=x2av + x(i)*x(i)
         y2av=y2av + y(i)*y(i)
         xyav=xyav + x(i)*y(i)
10    continue
      xav = xav/N
      yav = yav/N
      x2av=x2av/N
      y2av=y2av/N
      xyav=xyav/N

c     *** Calculate variances
      delta   = x2av-xav*xav
      epsilon = y2av-yav*yav
      mu      = xyav - xav*yav
     
      if (delta.eq.0) then
         print *,'delta = 0' 
         return
      endif
c     *** Calculate coefficients
      A    = mu/delta
      B    = yav - mu/delta*xav
      chi2 = (epsilon-mu*mu/delta)*N/(N-2)

      if (chi2.lt.0)  then
         print *,'chi2 < 0'
         return
      endif
      if (delta.lt.0) then
         print *,'delta < 0'
         return
      endif
c     *** Calculate errors
      sigA = sqrt(chi2/N/delta)
      sigB = sqrt(chi2*x2av/N/delta)
      return
      end

      subroutine crepf2(imark,coormark,fname)
      implicit double precision(a-h,o-z)
      dimension imark(*),coormark(2,*)
      character*80 fname
  
      open(2,file=fname)
      rewind(2)
      write(2,*) 2,imark(1)
      do 10 i=1,imark(1)
10       write(2,*) coormark(1,i),coormark(2,i)
      close(2)
      return
      end
