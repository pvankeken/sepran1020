c *************************************************************
c *   SOLMAR
c *
c *   Solves the Poisson equation
c *
c *      grad div Omega  =   dGAMMA/dx
c *
c *   where GAMMA is a stepfunction, of which the position is 
c *   defined by a markerchain.
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       ivort   o  Solution for Omega
c *       matr    o  Stiffness matrix (constant in this problem)
c *       imark   i  Integer information on markerchain
c *       coormark i Coordinates of markers (x1,y1,x2,y2,....)
c *
c *   PvK 151189
c *************************************************************
      subroutine solmar(kmesh,kprob,ivort,intmat,matr,iuser,user,
     v                  imark,coormark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),ivort(*),intmat(*),matr(*)
      dimension iuser(*),user(*),imark(*),coormark(*)
      dimension irhsd(5),ius(20),us(10)
      save ifirst
      data ifirst/0/

      nmark = imark(1)
      if (nmark+7.gt.user(1)) then
         write(6,*) 'PERROR(solmar): array USER too small'
         stop
      endif

      if (ifirst.eq.0) then
c        *** Built matrix as for equation of Laplace.
         ifirst = 1
         call chtype(1,kprob)
         ius(1)  = 20
          us(1)  = 10
         ius(6)  = 7
         ius(7)  = -6
         ius(8)  = 0
         ius(9)  = -6
         ius(10) = 0
         ius(11) = 0
         ius(12) = 0
         ius(13) = 0
          us(6) = 1d0
         call systm0(13,matr,intmat,kmesh,kprob,irhsd,ivort,ius,
     v               us,islold,ielhlp)
      endif

c     *** Calculate rhs vector
c     ***   fill positions of markers in array USER
c     ***   fill elementnumber for each marker in IUSER
      call chtype(2,kprob)
      nmark = imark(1)
      iuser(6) = nmark
      do 10 i=1,2*nmark
10       user(5+i) = coormark(i)
      do 20 i=1,nmark
         xm = coormark(2*i-1)
         ym = coormark(2*i)
         call pedetel(1,xm,ym,iel)
         iuser(6+i) = iel
20    continue
      call systm0(2,matr,intmat,kmesh,kprob,irhsd,ivort,iuser,
     v            user,islold,ielhlp)
      call chtype(1,kprob)
      call solve(1,matr,ivort,irhsd,intmat,kprob)

      return
      end
