c *************************************************************
c *   PEDETEL
c *
c *   Determine in which element a point with given coordinates
c *   lies.
c *   ICHOIS
c *       1     Mesh is rectangular, coordinates of the primary
c *             nodal points are given by xc(nx) and yc(ny) 
c *             The element are triangles where the diagonal of
c *             the rectangle has an angle with the positive 
c *             x-axis larger then 90 degrees.
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine pedetel(ichois,xm,ym,iel)
      implicit double precision(a-h,o-z)
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny

c     *** Find (ix,iy) such that  xc(ix-1) <= xm < xc(ix)
c     ***                    and  yc(iy-1) <= ym < yc(iy)
      ix = 1
100   continue
         ix = ix + 1
         if (xc(ix-1).le.xm.and.xm.lt.xc(ix)) then
            continue
         else
            if (ix.ge.nx) then
               continue
            else
               goto 100
            endif
         endif
 
      iy = 1
200   continue
         iy = iy + 1
         if (yc(iy-1).le.ym.and.ym.lt.yc(iy)) then
            continue
         else
            if (iy.ge.ny) then
               continue
            else
               goto 200
            endif
         endif

c     *** The element number is given by iel2 or iel2+1
c     *** ix and iy are >= 2. The elements in the first row are
c     *** numbered from 1 .. 2*(nx-1), in the second row from
c     *** 2*(nx-1)+1 .. 4*(nx-1) etc.
      iel2 = 2*(nx-1)*(iy-2) + 2*(ix-1) - 1

c     *** Determine position of marker with respect to diagonal.
c     *** Here it is assumed that the diagonal has a negative
c     *** angle with the positive x-axis.
      y1 = yc(iy-1)
      y2 = yc(iy)
      x1 = xc(ix)
      x2 = xc(ix-1)
      ydia = y1 + (y2-y1)*(xm-x1)/(x2-x1)
      if (ym.lt.ydia) then
         iel = iel2
      else
         iel = iel2 + 1
      endif
c     write(6,*) 'PEDETEL: xm,ym,ix,iy,iel2,iel'
c     write(6,'(2f10.3,4i10)') xm,ym,ix,iy,iel2,iel
      return
      end
