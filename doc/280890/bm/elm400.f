cdccelm400
      subroutine elm400 ( coor, elemmt, elemvc, iuser, user,index1, 
     .                    index3, index4, numold, vecold, islold,
     .                    work )
c
c ********************************************************************
c
c        programmer     Jaap van der Zanden / Guus Segal
c        version 10.0a  date    24-08-90 Extension with Gauss integration (PvK)
c        version 10.0   date    25-01-90 (New parameter list)
c        version  9.0a   date   26-06-89 (Temporary correction el3007 )
c        version  9.0    date   04-04-89 (First step to a complete revision)
c        version  8.0    date   25-02-89 (Ad-hoc extension for iterative
c                                         time-method)
c        version  7.0    date   07-09-87
c
c
c   copyright (c) 1984, 1990  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     Fill element matrix and vector two-dimensional Navier-Stokes equations
c
c
c     ELM400 is a help subroutine for subroutine SYSTEM,
c     it is called through the intermediate subroutine ELMNS2
c
c     So:
c
c     SYSTEM
c       -  Loop over elements
c          -  ELMNS2
c             - ELM400
c
c     itype=400 :    cartesian coordinates   7-point triangle
c                    Penalty function approach
c
c     itype=402 :    polar coordinates       7-point triangle
c                    Penalty function approach
c
c     itype=404 :    axisymmetric coordinates  7-point triangle
c                    Penalty function approach
c
c ********************************************************************
c
c
c                INPUT / OUTPUT PARAMETERS

cimp     implicit none

      double precision elemmt(*), elemvc(*), coor(2,*), user(*),
     .                 vecold(*), work(*)

      integer numold, iuser(*), index1(*), index3(numold,*),
     .        index4(numold,*), islold(5,numold)
c
c     coor     i     array of length 2 x npoint containing the co-ordinates of
c                    the nodal points with respect to the global numbering
c
c                    x  = coor (1,i);  y  = coor (2,i);
c                     i                 i
c
c     elemmt   o     Element matrix to be computed
c                    At this moment the element is assumed to be zero
c
c     elemvc   o     Element vector to be computed
c
c     elemms   o     Element mass matrix to be computed (Diagonal matrix)
c
c     index1   i     Array of length inpelm containing the point numbers
c                    of the nodal points in the element
c
c     index3  i      Two-dimensional integer array of length NUMOLD x NINDEX
c                    containing the positions of the "old" solutions in array
c                    VECOLD with respect to the present element
c
c                    For example VECOLD(INDEX3(i,j)) contains the j th
c                    unknown with respect to the i th old solution vector.
c                    The number i refers to the i th vector corresponding to
c                    IVCOLD in the call of SYSTM2 or DERIVA
c
c     index4  i      Two-dimensional integer array of length NUMOLD x INPELM
c                    containing the number of unknowns per point accumulated
c                    in array VECOLD with respect to the present element.
c
c                    For example INDEX4(i,1) contains the number of unknowns
c                    in the first point with respect to the i th vector stored
c                    in VECOLD.
c                    The number of unknowns in the j th point with respect to
c                    i th vector in VECOLD is equal to
c                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
c
c     iuser    i     Integer user array to pass user information from
c                    main program to subroutine. See STANDARD PROBLEMS
c
c     islold  i      User input array in which the user puts information
c                    of all preceding solutions
c                    Integer array of length 5 x numold
c
c     numold  i      Number of old vectors that are stored in VECOLD
c
c     user     i     Real user array to pass user information from
c                    main program to subroutine. See STANDARD PROBLEMS
c
c     vecold  i      In this array all preceding solutions are stored, i.e.
c                    all solutions that have been computed before and
c                    have been carried to system or deriva by the parameter
c                    islold in the parameter list of these main subroutines
c
c     work           Work space for this subroutine
c                    At this moment 2*196 positions work space is sufficient
c
c ********************************************************************
c
c
c                PARAMETERS IN COMMON BLOCKS
c
      integer iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .        interp, isecdv
      common /celp/   iweigh, ideriv, irule, igausp, iphi, jcart, icoor,
     .                interp, isecdv

c                       / celp /
c         Contains some choice parameters for the various elp subroutines
c
c
c     iweigh    o    Indication if weights must be computed (1) or not (0)
c
c     ideriv    o    Indication if derivatives must be computed (1) or not (0)
c
c     irule     o    Indication of the type of integration rule to be applied.
c                    Possibilities:
c
c                    1:  Newton-Cotes four-point rule
c
c     igausp    o    Indication if Gauss points must be computed (1) or not (0)
c                    Is only applied when irule > 1
c
c     iphi      o    Indication if array phi must be filled (1) or not (0)
c                    Is only applied when irule > 1
c
c     jcart     o    Indication of the type of co-ordinates to be used in the
c                    element. Possible values:
c
c                    1:  Cartesian co-ordinates
c                    2:  Axi-symmetric co-ordinates
c                    3:  Polar co-ordinates
c
c     icoor     o    Indication if array x must be filled (1) or not (0)
c                    In general icoor = 0 is only permitted if array x has
c                    been filled before
c
c     interp    o    Indication if the value of the basis functions in a
c                    specific point must be computed (interpolation) (1)
c                    or not (0)
c
c     isecdv    -    Not used
c
c                       / cactl /
c
c         Information of the elements is transported from subroutine SYSTEM
c         to ELM100 by means of common CACTL
c


      integer ielem, itype, ielgrp, inpelm, icount, ifirst, notmat,
     .        notvec, irelem, nusol, nelem, npoint
      common /cactl/  ielem, itype, ielgrp, inpelm, icount, ifirst,
     .                notmat, notvec, irelem, nusol, nelem, npoint

c
c     ielem   i     Element number
c
c     itype   i     Type number of element
c
c     ielgrp  i     Element group number
c
c     inpelm  i     Number of nodal points in the element
c
c     icount  i     Number of degrees of freedom in an element
c
c     ifirst  i     Indication of the element is called for the first time
c                   in the global subroutine (ifirst=0) or not (1)
c
c     notmat  i/o    Indication if the element matrix is zero (0) or not (1)
c
c     notvec  i/o    Indication if the element vector is zero (0) or not (1)
c
c     irelem  i     Relative element number with respect to standard element
c                   sequence number ielgrp
c
c     nusol   i     Number of degrees of freedom in the mesh
c
c     nelem   i     Number of elements in the mesh
c
c     npoint  i     Number of nodal points in the mesh
c
c                     /celwrk/
c
c          Work common block to store local arrays
c          CELWRK contains exactly 1500 real positions, the last 500
c          are reserved for the ELP subroutines

      double precision x, w, dphidx, xgauss, phi, array, amasps,
     .                 u, ugauss, un, dudx, trans, transl, qphixx,
     .                 etaeff, secinv, dvisd2, omega, sigma, rho, cn,
     .                 clam, f, eij, dlamdx, psi, dpsidx, dum, dum1,
     .                 penal, amas
      common /celwrk/ x(14), w(7), dphidx(98), xgauss(14), phi(49),
     .                array(56), u(21), ugauss(21), un(21), dudx(42),
     .                trans(24), transl, qphixx(147), etaeff(7),
     .                secinv(7), dvisd2(7), omega, sigma, rho,
     .                cn, clam, psi(42), f(21), dpsidx(84), amasps(36),
     .                penal(14), amas(49), dum1(213), eij(6), dlamdx(6),
     .                dum(488)

c          The following parameters are used in CELWRK
c
c
c     amas           Mass matrix corresponding to basis funtions phi
c
c     amasps         Mass matrix corresponding to basis funtions psi
c
c     array          8 x m array to store the coefficients for each
c                    integration point
c                    Contents:
c
c                    Pos 1-m:       -
c                    Pos m+1-2m:    rho
c                    Pos 2m+1-3m:   -
c                    Pos 3m+1-4m:   omega
c                    Pos 4m+1-5m:   f 1
c                    Pos 5m+1-6m:   f 2
c                    Pos 6m+1-7m:   f 3
c                    Pos 7m+1-8m:   eta
c
c     clam           Parameter lambda for Generalised Newtonian fluids
c
c     cn             Parameter n for Generalised Newtonian fluids
c
c     dlamdx         Array of 6 positions containing the derivatives of the
c                    barycentric co-ordinates
c
c     dphidx         Array of length n x m x 2  containing the derivatives
c                    of the basis functions in the sequence:
c
c                    d phi / dx = dphidx (i, k, 1);  d phi / dy = dphidx (i, k, 2);
c                         i                             i
c
c                      in node k
c
c     dpsidx         Array of length n-1 x m x 2  containing the derivatives
c                    of the special basis functions corresponding to the
c                    nodal points in the sequence:
c
c                    d psi / dx = dpsidx (i, k, 1);  d psi / dy = dpsidx (i, k, 2);
c                         i                             i
c
c                      in node k
c
c     dudx           Array of length 4 x m containing the derivatives of the
c                    preceding velocity in the integration points in the
c                    sequence:
c
c                    1    - m:  du/dx
c                    m+1  - 2m: du/dy
c                    2m+1 - 3m: dv/dx
c                    3m+1 - 4m: dv/dy
c
c     dum            488 positions reserved for the ELP subroutines
c
c     dum1           work space free for later use
c
c     dvisd2         Array of length m to store the derivative of the viscosity
c                    with respect to the second invariant of the
c                    stress tensor in the integration points
c
c     eij            Array of 6 positions containing the factors e
c
c     etaeff         Array of length m to store the effective viscosity in
c                    the integration points
c
c     f              Array of length 3n to store the right-hand side vector
c                    temporarily
c
c     omega          Angular velocity of rotating system
c
c     penal          Help array to store the penalty vector
c
c     phi      o     Array of length n x m containing the values of the
c                    basis functions in the Gauss points in the sequence:
c
c                           k
c                    phi (xg ) = phi (i, k)
c                       i
c
c                    Array phi is only filled when ifirst = 0 and gauss
c                    integration is applied
c
c     psi            Array of length n-1 x m containing the values of the
c                    basis functions corresponding to the nodal points
c                    in the Gauss points in the sequence:
c
c                           k
c                    psi (xg ) = psi (i, k)
c                       i
c
c                    Array psi is only filled when ifirst = 0 and gauss
c                    integration is applied
c
c     qphixx         Array of length 3 x n x n to store the integrals
c                    corresponding to the second derivative terms on a
c                    reference element
c
c     rho            Density of the fluid
c
c     secinv         Array of length m to store the second invariant of the
c                    stress tensor in the integration points
c
c     sigma          Inverse of parameter epsilon in Penalty function method
c
c     trans          Transformation array for the velocity in the centroid
c                    length: 2 * inpelm
c
c     transl         Not yet used
c
c     x              array of length n x 2 containing the x and
c                    y-coordinates of the nodal points with respect to the
c                    local numbering in the sequence:
c
c                    x  = x (i, 1);  y  = x (i, 2);
c                     i              i
c
c     xgauss   o     Array of length m x 2 containing the co-ordinates of
c                    the gauss points.  Array xgauss is only filled when Gauss
c                    integration is applied.
c
c                    xg  = xgauss (i, 1);  yg  = xgauss (i, 2);
c                      i                    i
c
c     u              Array of length 3n to store the preceding velocity in
c                    the nodal points
c
c     ugauss         Array of length 3m to store the preceding velocity in
c                    the integration points
c
c     un             Array of length 3n to store the velocity at a preceding
c                    time level in the nodal points
c
c     w              array of length m containing the weights for the
c                    numerical integration
c - - - - - - - - - - - - - - - - - - - - - - - - -

      integer ind, mst, ist, ich
      common /celiar/ ind(50), ich(50), mst(50), ist(50)

c                       / celiar /
c         Contains integer information of the various coefficients of the
c         differential equation.
c
c
c     ind       i    In this array of length 50, of each coefficient it
c                    is stored whether the coefficient is 0 (ind(i) = 0),
c                    constant (ind(i)<0) or variable (ind(i)>0) See elusr0
c
c                    If ind(i) should be 2003, then ind(i) is set equal to
c                    3000+ivec, where ivec refers to the vector to be used
c                    in array VECOLD
c
c                    If ind(i) should be 2004, then ind(i) is set equal to
c                    4000 + ipos, where ipos refers to the first relevant
c                    position in array iuser. In that case ich is also used
c
c                    If one of the indices i is between 1000 and 2000, which
c                    means that a preceding solution must be filled in an
c                    array uold, then ind(50) gets the value 1, otherwise
c                    ind(50) = 0
c
c     ich       i    In this array of length 50 information concerning the
c                    coefficients is stored for the case IND(I)=3000 + IVEC
c                    The value of ich(i) has the following meaning:
c
c                    1:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all nodal points
c
c                    2:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all nodal points
c
c                    3:   Array VECOLD(IVEC) has exactly one degree of freedom
c                         in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    4:   Array VECOLD(IVEC) has a constant number of degrees
c                         of freedom in all vertices and none in the other nodes
c                         Two-dimensional elements only
c
c                    5:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all nodes
c
c                    6:   Array VECOLD(IVEC) contains the degree of freedom
c                         in all vertices but not in all nodes
c                         Two-dimensional elements only
c
c     mst       i    In this array of length 50 the start position of the
c                    real information of the ith coefficient in array user
c                    is stored. See elusr3
c                    If ist(i) = 3000+IVEC then mst(i) contains the degree of
c                    freedom j that must be used for this coefficient
c
c     ist       i    In this array of length 50 it is indicated if a coefficient
c                    is a copy of preceding coefficient (>0) or not (=0)
c                    If ist(i) > 0 the value of ist(i) indicates the coefficient
c                    number from which the coefficient must be copied.
c
c - - - - - - - - - - - - - - - - - - - - - - - - -
      integer ipos, nuser, n, m, ncomp, jdiag, ndim, idim, nunkp,
     .        lrow, mn, jsol, jderiv, jtrans, jind, jconsf, kstep,
     .        jadd, npsi, jdercn, jzero, jsecnd, istarw, metupw, nvert
      common /celint/ ipos, nuser, n, m, ncomp, jdiag, ndim, idim,
     .                lrow, mn, jsol, jderiv, jtrans, jind, jconsf,
     .                kstep, jadd, npsi, jdercn, jzero, jsecnd, istarw,
     .                metupw, nvert, nunkp
c
c                 /celint/
c         Contains information of some element dependent quantities
c         Is used to transport information from main element subroutines
c         to lower level subroutines
c
c     ipos     i     Position in array iuser
c
c     nuser    i     Computed length of array user
c
c     n        i     Number of nodes corresponding to type number
c
c     m        i     Number of integration points
c
c     ncomp    i     Number of components of the solution vector
c
c     jdiag    i     Indication if the basis functions satisfy:
c                            k                                         k
c                    phi i (x ) = delta    for each integration point x
c                                      ik
c
c                    True:  jdiag = 1,  false jdiag = 2
c
c     ndim     i     dimension of space, see dudx
c
c     idim     i     number of components of the velocity vector u that must
c                    be taken into account
c
c     lrow     i     size of the element matrix
c
c     mn       i     Number of positions the derivatives of the basis functions
c                    need for the derivative in one direction
c
c     jsol     i     Indication if the values of the local vector in the nodal
c                    points or the integration points must be computed (1)
c                    or not (0)
c
c     jderiv   i     Indication if the value of the derivatives must be
c                    computed.  Possibilities:
c
c                    0   No derivatives
c
c                    1   The gradient is computed in all integration points
c                        in the sequence given by dudx
c
c                    2   dudx in the vertices
c
c                    3   dudy in the vertices
c
c                    4   dudz in the vertices
c
c                    5   The gradient is computed in the vertices
c                        in the sequence given by dudx
c                                       th
c     jtrans   i     Indication if the n   point is computed with the aid
c                    of array TRANS (1) (NDIM = 2 or 3 only) or not (0)
c
c
c     jind     i     indication of how the local vector u must be found
c                    from usol. possibilities:
c
c                    0:  the value of u in the nodal points must be computed
c                        with the aid of array ind of length n x ncomp
c
c                   -1:  in each nodal point there are the same number of
c                        components of u. the components ind(1), ind(2), ...
c                        ind(ncomp) are stored
c
c                   >0:  in each nodal point there are the same number of
c                        components of u. the components jind, jind+1,...
c                        jind+ncomp-1 must be stored in u.
c
c     jconsf   i     Information of the weight function w and the function to
c                    be integrated g. Possibilities:
c
c                    1   g and w constant
c
c                    2   general
c
c     kstep    i     Indication if the elements must be treated normally
c                    i.e. all integration points are stored sequentially (1)
c                    or that a quadratic triangle with a 3-point
c                    newton cotes rule must be considered (2)
c
c     jadd     i     Indication if the matrix or vector must be added to an
c                    already existing matrix resp element vector (1) or not (0)
c
c     npsi     i     Number of basis functions psi
c
c     jdercn   i     Indication if the derivatives of the basis functions
c                    are constant (jdercn=0) or variable (jdercn=1)
c
c     jzero    i     Choice parameter for subroutine el3002
c                    Indicates the function to be integrated
c                    Possibilities:
c
c                    1   The function b and the weights w are constant
c
c                    2   General
c
c                    3   The function b is constant, the weights w are variable
c
c     jsecnd   i     choice parameter. Possibilities:
c
c                    1   a12=a13=a23=0;  a11=a22=a33   a11 and w constant
c
c                    2   a12=a13=a23=0;  a11=a22=a33
c
c                    3   a12#0 or a13#0 or a23#0   a11=a22=a33
c
c                    4   general
c
c     istarw    i    When istarw > 0 it indicates the starting row for some
c                    subroutines
c
c     metupw    i    Type of upwinding method to be chosen. Possible values:
c
c                    0:  No upwinding
c                    1:  Classical upwind scheme
c                    2:  Il'in scheme
c                    3:  Doubly assymptotic scheme
c                    4:  Critical approximation
c
c     nvert     i    Number of vertices in the element
c
c     nunkp     i    Maximal number of degrees of freedom per point
c - - - - - - - - - - - - - - - - - - - - - - - - -
      logical matrix, vector, oldsol
      integer notmas
      common /cellog/ matrix, vector, oldsol, notmas
      save /cellog/
c
c                 /cellog/
c
c     matrix   i     Indication if the element matrix must be computed (true)
c                    or not (false)
c
c     vector   i     Indication if the element vector must be computed (true)
c                    or not (false)
c
c     oldsol   i     Indication if array uold has been filled with a preceding
c                    solution (true) or not (false)
c
c     notmas   i     Indication if the mass matrix is equal to zero (1)
c                    or not (0)
c
c - - - - - - - - - - - - - - - - - - - - - - - - -
      character * 80 chars
      character * 200 name
      common /cmessc/ chars(10), name
      save /cmessc/

c                 /cmessc/
c    Contains characters for the error message subroutines
c    Must be used in cooporation with common block cmessg
c
c    chars     Array containing character information for error messages
c              At most 10 positions are available
c
c    name      Name of tree of subroutines concatenated with name of local
c              subroutine to be used for error messages
c              Only the last 80 characters are used in the error message 
c              The number of characters in name is given by lennam
c
c - - - - - - - - - - - - - - - - - - - - - - - - -
      integer ints, lennam
      double precision reals
      common /cmessg/ reals(10), ints(10), lennam
      save /cmessg/

c                 /cmessg/
c    Contains extra information for the error message subroutines
c    Must be used in cooporation with common block cmessc
c
c    ints      Array containing integer information for error messages
c
c    lennam    Length of parameter name
c
c    reals     Array containing real information for error messages
c - - - - - - - - - - - - - - - - - - - - - - - - -
      double precision theta, dt, ratime
      integer iatime
      common /ctime/ theta, dt, ratime(8), iatime(10)

      common /peinterg/ nrule

c
c ********************************************************************
c
c               LOCAL PARAMETERS

c
c    ratime    theta, dt, time, ....
c
c    iatime    istep
      integer induv(12)
      integer method, npuv, npuv2, nstart, itime, mconv,
     .        irotat, modelv, iche, itcoor, jelp, i, j,
     .        ichint, ichiv, ichl, icurve, iptim, ksign,
     .        isub, isup, kderiv, ipwork
      double precision delta, rhoh, phiphi

      save method, itcoor, npuv, npuv2, nstart, iptim, ipwork,
     .     induv, itime, mconv, irotat, modelv, isub, isup,
     .     rhoh, iche, ichl
      data induv/1, 7, 2, 8, 3, 9, 4, 10, 5, 11, 6, 12/
c
c    ipwork        Starting address of array f with respect to array work
c
c**********************************************************************
c
      call eropen ( 'elm400' )
      if ( ifirst.eq.0 ) then

c      *  ifirst = 0, compute element independent quantities

         call el0400 ( iuser, user, jelp, isub, isup, method, mconv, 
     .                 itime, modelv, irotat )
         ipwork = ( (inpelm+1)*2 ) ** 2 + 1

c      *  Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

c       *  Set jsol temporarily equal to -1 in order to indicate that the
c          old versions of the ELP subroutines are used

        jsol = -jsol


c       *  Set some constants, these correspond to the old version
c        npuv    number of points for velocity u1, u2 including centroid
c        nstart  number of test function to start with
c        lrow    total number of degrees of freedom in element
c

         npuv=inpelm+1
         npuv2=npuv+npuv
         lrow=npuv2
         nstart=1
         itcoor = jcart - 1
         iptim = npuv*npuv+1

         if(itime.eq.1 .or. itime.eq.4) then
            rhoh = rho/(theta*dt)
         else if(itime.eq.3) then
            rhoh = rho
         endif
         if ((modelv.eq.2).and.(cn.gt.1)) then
            iche = 2
         else
            iche = 1
         endif
         ichl = itcoor+30
         if (nrule.gt.0) ichl = ichl + 3
         if (method.eq.0) ichl = ichl+40

      end if

c    * Compute basis functions and weights for numerical integration

      call elp220 ( ichl, ichint, coor, index1, name(1:lennam), ksign,
     .              icurve, delta, x, xgauss, eij, dlamdx, w, phi,
     .              dphidx, trans, transl, qphixx )
      if ( itcoor.ne.0 .or. icurve.ne.0 ) jdiag = 2
      if (nrule.gt.0) jdiag = 2

c      * Fill variable coefficients if necessary
c        First fill uold if necessary

      if ( itime.eq.4 ) then

c      *  itime = 4, Iterative time-dependent method
c                    Both un and u n+1 * are necessary
 
         kderiv = jderiv
         jderiv = 0
         call el3007 ( vecold, un, work, phi, dphidx, m, n, ndim, ncomp,
     .                 index3, index1, trans, ugauss, jtrans, numold, 1)
         jderiv = -kderiv

         call el3007 ( vecold, u, dudx, phi, dphidx, m, n, ndim, ncomp,
     .                 index3, index1, trans, ugauss, jtrans, numold, 2)

      else if ( ind(50).eq.1 ) then

         jderiv = -jderiv
         call el3007 ( vecold, u, dudx, phi, dphidx, m, n, ndim, ncomp,
     .                 index3, index1, trans, ugauss, jtrans, numold, 1)
         jderiv = -jderiv
 
      end if

      do 150 i = isub, isup

         if ( ind(i).gt.0 .or. ist(i).gt.0 ) then

c         *  Coefficient is variable

            call elflr6 ( i, user, array(1+(i-1)*m), index1, x, xgauss,
     .                    m, n, phi, vecold, work, 1, index1, index3,
     .                    index4, numold, iuser, ugauss )
            if ( i.eq.7 ) then

c            *  Coefficient 7, i.e. eta
c               Test if eta is positive

                do 110 j = 1, m
                   if ( array(j+(i-1)*m).le.0 ) then

c                   * Error 1304: Eta is not positive

                      ints(1) = i
                      ints(2) = itype
                      ints(3) = ielgrp
                      ints(4) = ielem
                      reals(1) = array(j+(i-1)*m)
                      chars(1) = 'eta (0)'
                      call errsub ( 1304, 4, 1, 1 )

                   end if

110             continue

             end if

         end if

150   continue

      if (ind(7).ne.0) then
         ichiv = 1
      else
         ichiv = ichint
      endif
c
c*  compute effective viscosity
c
      if (matrix .or. modelv.gt.1 .and. modelv.lt.4 .and. itime.ne.3)
     .   call el4028 ( iche, name(1:lennam), itcoor, npuv, npuv, ndim,
     .                 modelv, array(1+6*m), cn, clam, work(ipwork), 
     .                 work(ipwork+npuv), xgauss, ugauss, dudx,
     .                 etaeff, secinv, dvisd2 )
      if(itime.ge.1) then
         call el2002(6, ichint+1, rhoh, elemmt, w, phi, npuv,
     .          npuv, npuv, work(ipwork), phiphi, delta, .true.)
         if(itime.eq.3) goto 1100
         call el2015(1, ichint+1, elemmt, u, 1d0, elemmt(iptim), npuv
     .          , npuv, 1)
         call el2015(1, ichint+1, elemmt, u(1+npuv), 1d0, elemmt
     .        (iptim+npuv), npuv, npuv, 1)
c
c       *  copy m uold in array from pos. ipu
c
         call dcopy(npuv2, elemmt(iptim), 1, u, 1)
      endif
c
c*   if (matrix) compute element matrix
c
      if (.not.matrix) goto 2000
c
c*    compute viscous stresses and initialize element matrix
c             s
c
      call el4022 ( ichiv, itcoor, etaeff, work, w, xgauss, phi, dphidx,
     .              nstart, npuv, npuv, lrow, phi, dphidx, inpelm,
     .              dlamdx, qphixx, delta, f )
c
c*    compute terms due to newton linearization of viscosity
c             s for special cases only
c
      if ((modelv.eq.2).and.(cn.gt.1)) then
         call el4036 ( itcoor, w, w(1+npuv), xgauss, phi, dphidx, 
     .                 ugauss, dudx, dvisd2, work(ipwork), ndim, npuv, 
     .                 npuv, phi, dphidx, inpelm )
         call el2008(work, work(ipwork), npuv2, lrow)
      endif
c
c*    compute penalty matrix if required, and add it
c
      if (method.eq.1) call el4025 ( itcoor, ichint, 1, work, 
     .                 work(ipwork), sigma, ksign, delta, eij, w, 
     .                 xgauss, phi, dphidx, npuv, npuv, ncomp, lrow )
c
c*    compute convective and/or coriolis acceleration if required
c
      if (mconv.gt.0 .and. mconv.ne.4 .or. irotat.eq.1 ) then
         call el4023 ( ichint, itcoor, mconv, rho, irotat, omega, 
     .                 ugauss, dudx, w, xgauss, phi, dphidx, work,
     .                 npuv, npuv, lrow, phi, dphidx, inpelm )
      endif
1100  if(itime.eq.1 .or. itime.eq.4 .and. theta.gt.0d0) then
         call el2016 ( 1, ichint+1, work, elemmt, 1d0, 1d0, npuv, lrow )
     .      
         call el2016 ( 1, ichint+1, work(1+npuv*lrow+npuv), elemmt, 1d0,
     .                 1d0, npuv, lrow )
      else if (itime.eq.3) then
         do 1110 i = 1, lrow*lrow
1110        work(i) = 0d0
         call el2016 ( 1, ichint+1, work, elemmt, 0d0, 1d0, npuv, lrow )
         call el2016 ( 1, ichint+1, work(1+npuv*lrow+npuv), elemmt, 0d0,
     .                 1d0, npuv, lrow )
      endif
c
c*    eliminate velocity from centroid (uc, vc) in point npuv
c               i.e. transform matrix : r(transp)*smatrix*r
c
      call el4024(1, work, work(ipwork), trans, work(ipwork),
     .            ndim, npuv, lrow)
c
c*    reorder the contents of the element matrix and put it into elemmt
c
      call el2009( 1, elemmt, elemvc, work, work(ipwork),
     .             induv, icount, 2 )
c
c*********************************************************************
c
c
c    compute element vector
c
c********************************************************************
c
2000  if (.not.vector) go to 3000
c
c*    fill (driving) external forces into vector (initialize it)
c
      call el4031 ( ichint, array(1+4*m), phi, rho, w, work(ipwork), 
     .              nstart, npuv, npuv, lrow )
c
c*    add terms due to newton linearization of viscosity
c             f
c
      if ((modelv.eq.2).and.(cn.gt.1))
     .   call el4037 ( itcoor, w, work, xgauss, phi, dphidx, ugauss, 
     .                 dudx, secinv, dvisd2, work(ipwork), ndim, npuv, 
     .                 npuv, phi, dphidx, inpelm )
c
c*    compute influence of convective acceleration for mconv = 2
c
      if (mconv.eq.2 .or. mconv.eq.4)
     .   call el4033 ( ichint, itcoor, rho, ugauss, dudx, w, xgauss, 
     .                 phi, work(ipwork), nstart, npuv, npuv, ndim, phi,
     .                 inpelm )
      if(itime.eq.1 .or. itime.eq.4) call daxpy(npuv2, 1d0, u, 1,
     .    work(ipwork), 1 )
c
c*    eliminate velocity basis functions from centroid from vector
c
      call el4024(2, work, work(ipwork), trans, work, ndim, npuv, lrow)
c
c*    reorder the contents of the element vector and put it into elemvc
c
      call el2009(2, elemmt, elemvc, work, work(ipwork), induv, icount,
     .            2)
3000  call erclos ( 'elm400' )
      end
cdc*eor


