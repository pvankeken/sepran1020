c *************************************************************
c *   OUTTUS
c * 
c *   Intermediate output for program BM
c *
c *   Output of iteration number, accuracy, Nusselt number
c *   rms velocity, local temperature gradients and cpu time
c *
c *   PvK 120490
c *************************************************************
      subroutine outtus(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                  islol1,islol2,iuser,user)
      implicit double precision(a-h,o-z)
      parameter(NFUNC=505)
      dimension kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      dimension isol2(*),islol1(*),islol2(*),iuser(*),user(*)
      dimension igradt(5),funcx(NFUNC),funcy(NFUNC),icurvs(3)
      common /bmint/ ityps,itypt,nruls,nrult
      common /bmiter/ eps,relax,dif1,dif2,dif,nitermax,niter
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2,cpu3
      real t0,t1,dcpu,cput,cpu1,cpu2,cpu3,t2

      funcx(1) = NFUNC
      funcy(1) = NFUNC
      call second(t2)
      dcpu = t2-t1
      cput = t2-t0
      t1   = t2

c     **** Relaxation
      if (relax.ne.0) then
	 call algebr(3,0,islol2,isol2,isol2,kmesh2,kprob2,
     v               1-relax,relax,0,0,ip)
      endif
      dif1 = anorm(0,3,0,kmesh1,kprob1,isol1,islol1,ielhlp)
      dif2 = anorm(0,3,0,kmesh2,kprob2,isol2,islol2,ielhlp)
      smax = anorm(1,3,0,kmesh1,kprob1,isol1,isol1,ielhlp)
      tmax = anorm(1,3,0,kmesh2,kprob2,isol2,isol2,ielhlp)
      dif1 = dif1/smax
      dif2 = dif2/tmax
      dif  = dif2

      call chtype(2,kprob2)
      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,isol2,
     v            iuser,user,ihelp)
      irule = 1
      if (itypt.eq.104) irule=3
      temp1 = bounin(1,irule,1,1,kmesh2,kprob2,1,1,isol2,iuser,user)
      gnus  = -bounin(2,irule,1,1,kmesh2,kprob2,3,3,igradt,iuser,
     v                user)/temp1
      icurvs(1) = -1
      icurvs(2) = 3
      icurvs(3) = 3
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      nunkp = funcy(5)
      q2 = -funcy(6)
      q1 = -funcy(5+nunkp)

      write(6,11) niter,dif,gnus,vrms,q1,q2,dcpu,cput
11    format(i4,f12.8,f12.7,f12.4,2f12.7,2f8.2)
      call flush(6)
      call chtype(1,kprob2)

      return
      end
