c *************************************************************
c *   BMHEAT
c *   
c *   PvK 120490
c *************************************************************
      subroutine bmheat(kmesh,kprob,intmat,iuser,user,isol,islold,matr)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),iuser(*),user(*),isol(*)
      dimension islold(*),matr(*),irhsd(5)
      common /peinterg/ nrule
      common /bmint/ ityps,itypt,nruls,nrult
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2,cpu3
      real t0,t1,dcpu,cput,cpu1,cpu2,cpu3,t2,t3

      call second(t2)
      nrule=nrult
      call copyvc(isol,islold)
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      call second(t3)
      cpu3 = t3-t2

      return
      end

