c *************************************************************
c *   PERMESH
c *
c *   Read mesh from file
c *
c *   PvK 120490
c *************************************************************
      subroutine permesh(kmesh1,kmesh2)
      implicit double precision(a-h,o-z)
      common /bmint/ ityps,itypt,nruls,nrult
      dimension kmesh1(*),kmesh2(*)
      character*80 fsname,ftname

      if (ityps.eq.400.or.ityps.eq.104) then
c        *** Triangle with six nodal points
	 fsname = 'mesh1'
      else if (ityps.eq.100) then
c        *** Triangle with three nodal points
	 fsname = 'mesh2'
      else if (ityps.eq.108.or.ityps.eq.11) then
c        *** Rectangular element with four nodal points	
	 fsname = 'mesh3'
      else
	 write(6,*) 'PERROR(permesh): element type unknown'
	 write(6,*) 'ityps = ',ityps
      endif

      if (itypt.eq.104) then
c        *** Triangle with six nodal points
	 ftname = 'mesh1'
      else if (itypt.eq.100.or.itypt.eq.120) then
c        *** Triangle with three nodal points
	 ftname = 'mesh2'
      else if (itypt.eq.108.or.itypt.eq.5.or.itypt.eq.3) then
c        *** Rectangular element with four nodal points	
	 ftname = 'mesh3'
      else
	 write(6,*) 'PERROR(permesh): element type unknown'
	 write(6,*) 'itypt = ',itypt
      endif

      open(9,file=fsname)
      rewind(9)
      call meshrd(3,9,kmesh1)
      open(9,file=ftname)
      rewind(9)
      call meshrd(3,9,kmesh2)
      close(9)

      return
      end
