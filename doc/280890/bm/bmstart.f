c *************************************************************
c *   BMSTART
c *
c *   PvK 120490
c *************************************************************
      subroutine bmstart(kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2,
     v                   matr1,matr2,iuser,user,isol1,isol2,nbufdef,num)
      implicit double precision(a-h,o-z)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /bmint/ ityps,itypt,nruls,nrult
      common /bmiter/ eps,relax,dif1,dif2,dif,nitermax,niter
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      common /c1visc/ viscl(10),ctd,cpz,itypv,nl
      common /bstore/ f2name
      character*80 f2name
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2
      real t0,t1,dcpu,cput,cpu1,cpu2
      dimension kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),intmt1(*)
      dimension intmt2(*),iuser(*),user(*),isol1(*),isol2(*)
      dimension iu1(3)

      kmesh1(1)  = 100
      kmesh2(1)  = 100
      kprob1(1)  = 100
      kprob2(1)  = 100
      iuser(1)   = 100
       user(1)   = num+5

      read(5,*) ityps,nruls
      read(5,*) itypt,nrult
      read(5,*) nitermax,eps,relax,irestart
      read(5,*) rayleigh,rlam,q,visc0,b,c

      call start(0,1,-1,0)
      nbuffr = nbufdef
      call permesh(kmesh1,kmesh2)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
      call commat(1,kmesh1,kprob1,intmt1)
      call commat(2,kmesh2,kprob2,intmt2)
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)

      nsup1  = kprob1(29)
      nsup2  = kprob2(29)
      npoint = kmesh1(8)
      if (nsup1.ne.0.or.nsup2.ne.0) then
	 write(6,*) 'NSUPER: ',nsup1,nsup2
	 stop
      endif
      if (npoint.gt.num) then
	 write(6,*) 'PERROR(bmstart): Number of points too large '
	 write(6,*) 'for user arrays. Npoint = ',npoint
         stop
      endif

      if (irestart.gt.0) then
	 f2name = 'restart.back'
	 call openf2(.false.)
         call readbs(1,isol1,ihelp)
	 call readbs(2,isol2,ihelp)
      else
	 iu1(1) = 1
	 call creavc(0,1,idum,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
      endif

      if (visc0.gt.0) then
c        *** Viscosity is temperature and depth dependent
	 itypv = 2
         ctd   = -b
	 cpz   = c
      endif
      call second(t0)
      return
      end
