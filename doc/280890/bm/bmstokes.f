c *************************************************************
c *   BMSTOKES
c *
c *   Solves the Stokes equation. 
c *
c *   PvK 120490
c *************************************************************
      subroutine bmstokes(kmesh,kprob,intmat,iuser,user,isol,islol,matr)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),iuser(*),user(*)
      dimension isol(*),islol(*),matr(*),irhsd(5),ivort(5)
      common /peinterg/ nrule
      common /bmint/ ityps,itypt,nruls,nrult
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2,cpu3
      real t0,t1,dcpu,cput,cpu1,cpu2,cpu3,t2,t3
      save ifirst
      data ifirst/0/

      call second(t2)
      nrule = nruls
      call copyvc(isol,islol)
      if (ityps.eq.400.or.ityps.eq.11) then
	 call pestoh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v               matr,irhsd)
      else if (ityps.eq.100.or.ityps.eq.104) then
	 call copyvc(isol,ivort)
	 call solvor(kmesh,kprob,ivort,intmat,matr,iuser,user)
	 call solstr(kmesh,kprob,isol,ivort,intmat,matr,iuser,user)
      endif
      call second(t3)
      if (ifirst.eq.0) then
	  ifirst = 1
	  cpu1 = t3-t1
      else
	  cpu2 = t3-t1
      endif
      return
      end

c *************************************************************
c *   SOLVOR
c *
c *   Solves the Poisson equation
c *
c *      grad div Omega  =   - Ra * dT/dx
c *
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       ivort   o  Solution for Omega
c *       matr    o  Stiffness matrix (constant in this problem)
c *
c *   PvK 021189
c *************************************************************
      subroutine solvor(kmesh,kprob,ivort,intmat,matr,iuser,user)
      implicit double precision(a-h,o-z)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      dimension kmesh(*),kprob(*),ivort(*),intmat(*),matr(*)
      dimension iuser(*),user(*),irhsd(5)
      save ifirst
      data ifirst/0/

      if (ifirst.eq.0) then
c        *** Built matrix as for equation of Laplace.
         ifirst = 1
         call systm0(13,matr,intmat,kmesh,kprob,irhsd,ivort,iuser,
     v               user,islold,ielhlp)
      endif

c     *** Compute derivative of temperature   
c     *** Calculate rhs vector and solve the system
      call systm0(2,matr,intmat,kmesh,kprob,irhsd,ivort,iuser,
     v            user,islold,ielhlp)
      call solve(1,matr,ivort,irhsd,intmat,kprob)

      return
      end


c *************************************************************
c *   SOLSTR
c *
c *   Solves the Poisson equation
c *
c *      grad div Psi  = - Omega
c *
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for Psi
c *       itemp   i  Vector containing Omega in the nodal points
c *       matr    i  Stiffness matrix (constant in this problem)
c *
c *   PvK 021189
c *************************************************************
      subroutine solstr(kmesh,kprob,istrm,ivort,intmat,matr,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),ivort(*),intmat(*)
      dimension irhsd(5),iuser(*),user(*),matr(*)

   
      npoint = kmesh(8)
      if (npoint+7.gt.user(1)) then
         write(6,*) 'PERROR(solstr): array user too small'
         stop
      endif


c     *** Calculate rhs vector and solve the system
      iuser(6)  = 7
      iuser(7)  = -6
      iuser(8)  = 0
      iuser(9)  = -6
      iuser(10) = 0
      iuser(11) = 0
      iuser(12) = 0
      iuser(13) = 2001
      iuser(14) = 7
       user(6)  = 1d0
      factor = -1d0
      call pecopy(0,ivort,user,kprob,7,factor)
      call systm0(2,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v            user,islold,ielhlp)
      call solve(2,matr,istrm,irhsd,intmat,kprob)

      return
      end

c *************************************************************
c *   PESTOH
c *
c *   Solves the Stokes equation
c *************************************************************
      subroutine pestoh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      implicit double precision(a-h,o-z)
      dimension matrs(*),intmat(*),kmesh(*),kprob(*),isol(*)
      dimension iuser(*),user(*),irhsd(*),matrback(5)
      common /peinterg/ nrule
      common /peparam/ rayleigh,rlam,q,visc0,b,c

      save ifirst
      data ifirst/0/

      if (visc0.gt.0.or.ifirst.eq.0) then
c        *** Build system; clear rhsd
         call systm0(3,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         call copymt(matrs,matrback,kprob)
      else
         call systm0(5,matrback,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
      endif
      nrule = 0
      call systm0(12,matrback,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matrs,isol,irhsd,intmat,kprob)

      ifirst=1

      return
      end
      
