      program bm
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF = 3 000 000)
      parameter(NUM = 20 000)
c *************************************************************
c *   BM
c *
c *   Stationary convection benchmark models
c *
c *   PvK 120490
c *************************************************************
      dimension kmesh1(100),kmesh2(100),kprob1(100),kprob2(100)
      dimension intmt1(5),intmt2(5),matr1(5),matr2(5)
      dimension iuser(105),user(NUM+5)
      dimension isol1(5),isol2(5),islol1(5),islol2(5)
      common ibuffr(NBUFDEF)
      common /bmiter/ eps,relax,dif1,dif2,dif,nitermax,niter
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2
      real t0,t1,dcpu,cput,cpu1,cpu2

      call bmstart(kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2,
     v             matr1,matr2,iuser,user,isol1,isol2,NBUFDEF,NUM)
      niter=0
100   continue
	 niter = niter +1
	 call bmcof(1,kmesh1,kmesh2,kprob2,isol2,iuser,user)
	 call bmstokes(kmesh1,kprob1,intmt1,iuser,user,
     v                 isol1,islol1,matr1)

	 call bmcof(2,kmesh1,kmesh2,kprob1,isol1,iuser,user)
	 call bmheat(kmesh2,kprob2,intmt2,iuser,user,
     v               isol2,islol2,matr2)

	 call outtus(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v               islol1,islol2,iuser,user)
   
	 if (niter.lt.nitermax.and.dif.gt.eps) goto 100

       call bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,iuser,user)

       end

