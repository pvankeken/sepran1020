      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
    
      if (ichois.eq.1) then
         wpi = pi/rlam
         func = 0.01*sin(pi*y)*cos(wpi*x) + (1-y)
      else if (ichois.eq.2) then
	 func = -y
      else if (ichois.eq.3) then
	 func = x
      endif
  
      return
      end
