c *************************************************************
c *   BMOUT
c *
c *   output of program BM
c *
c *   PvK 120490
c *************************************************************
      subroutine bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                 iuser,user)
      implicit double precision(a-h,o-z)
      parameter(NFUNC=505)
      dimension kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      dimension isol2(*),iuser(*),user(*),ivelx(5),ively(5)
      dimension igradt(5),funcx(NFUNC),funcy(NFUNC),icurvs(3),istrm(5)
      dimension kmesh3(100),kprob3(100),iinput(100)
      common /cinout/ifree,iout,itime
      common /bmiter/ eps,relax,dif1,dif2,dif,nitermax,niter
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      common /pecpu/ t0,t1,dcpu,cput,cpu1,cpu2,cpu3
      real t0,t1,dcpu,cput,cpu1,cpu2,cpu3,t2
      common /bstore/ f2name
      common /bmint/ ityps,itypt,nruls,nrult
      character*80 f2name

      kmesh3(1)= 100
      kprob3(1)= 100
      funcx(1) = NFUNC
      funcy(1) = NFUNC

      npoint = kmesh2(8)
      call chtype(2,kprob2)
      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,isol2,
     v            iuser,user,ihelp)
      ir = 1
      if (itypt.eq.104) ir=3
      temp1 = bounin(1,ir,1,1,kmesh2,kprob2,1,1,isol2,iuser,user)
      gnus1=-bounin(2,ir,1,1,kmesh2,kprob2,3,3,igradt,iuser,user)/temp1
      gnus2= bounin(2,ir,1,1,kmesh2,kprob2,1,1,igradt,iuser,user)/rlam
      gnus3= bounin(2,ir,1,1,kmesh2,kprob2,2,2,igradt,iuser,user)
      gnus4= bounin(2,ir,1,1,kmesh2,kprob2,4,4,igradt,iuser,user)

      icurvs(1) = -1
      icurvs(2) = 3
      icurvs(3) = 3
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      nunkp = funcy(5)
      q2 = -funcy(6)
      q1 = -funcy(5+nunkp)

      icurvs(2) = 1
      icurvs(3) = 1
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      q4 = -funcy(6)
      q3 = -funcy(5+nunkp)
      nx = nunkp
      ny = npoint/nunkp
      if (ityps.eq.11.and.itypt.ne.104) then
c        *** Another calculation of NU
         nxelem = nx-1
         call pecopy(0,isol2,user,kprob2,6,fac)
         call nusan(kmesh1,user,nxelem,gnusa)
      endif

c     **** Root mean square velocity ********
      if (ityps.eq.400) then
	 call pevrms(isol1,kmesh1,kprob1,iuser,user)
	 vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v                  user,ihelp)/rlam
      else if (ityps.eq.11) then
	 vrms2 = c1vrms(isol1,kmesh1,kprob1,iuser,user)/rlam
      else if (ityps.eq.100.or.ityps.eq.104) then
	 idc = 1
	 if (ityps.eq.104) idc=2
	 call deriva(idc,1,2,1,1,ivelx,kmesh1,kprob1,isol1,isol1,
     v               iuser,user,i)
	 call deriva(idc,1,1,1,1,ively,kmesh1,kprob1,isol1,isol1,
     v               iuser,user,i)
	 call vrms(ivelx,ively,kmesh1,kprob1,iuser,user)
	 if (ivelx(2).eq.110) then
	    vrms2 = volint(0,1,1,kmesh1,kprob1,ivelx,iuser,
     v                     user,ihelp)/rlam
	 else if (ivelx(2).eq.115) then
	    jout = iout
	    iout = -1
	    open(9,file='mesh2')
	    rewind(9)
	    call meshrd(0,9,kmesh3)
	    close(9)
	    do 10 i=1,30
10             iinput(i) = 0
            iinput(1) = 30
	    iinput(6) = 1
	    iinput(21)= 100
	    call probdf(1,kprob3,kmesh3,iinput)
	    vrms2 = volint(0,1,1,kmesh3,kprob3,isol1,iuser,
     v                     user,ihelp)/rlam
	    iout = jout
	 endif
      endif
      vrms1  = sqrt(vrms2)

c     **** Local temperature maximum in the middle of the box
      fac=1d0
      call pecopy(0,isol2,user,kprob2,6,fac)
c     if (itypt.ne.104) then
c       call teze(nx,ny,kmesh2,user,te1,ze1,te2,ze2)
c     endif

      write(6,*)
      write(6,'(''Niter  '',i12,f12.2)') niter,cput
      write(6,'(''cpu    '',3f12.2)') cpu1,cpu2,cpu3
      write(6,'(''Nu     '',4f12.7)') gnus1,gnus2,gnus3,gnus4
      write(6,'(''Nu     '',4f12.7)') gnusa
      write(6,'(''Vrms   '',f12.7)') vrms1
      write(6,'(''q1     '',2f12.7)') q1,q3
      write(6,'(''q2     '',2f12.7)') q2,q4
      write(6,'(''Te     '',2f12.7)') te1,te2
      write(6,'(''Ze     '',2f12.7)') ze1,ze2

      format = 10d0
      chheight = 0.3d0
      ifrinit = 0
      jkader = -4
      fname = 'PLOT'
      x=0d0
      y=0d0
      call plwin(1,x,y)
      text = 'Streamfunction'
      if (ityps.eq.400) then
	 call stream(1,ivec,istrm,0,psiphi,kmesh1,kprob1,isol1)
      else
	 call copyvc(isol1,istrm)
      endif
      call plotc1(1,kmesh1,kprob1,istrm,contln,ncntln,format,yfaccn,1)
      x = format+1d0
      call plwin(3,x,y)
      text = 'Temperature'
      call plotc1(1,kmesh2,kprob2,isol2,contln,ncntln,format,yfaccn,1)

      f2name = 'restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'velo',isol1,ihelp)
      call writbs(0,2,numarr,'temp',isol2,ihelp)
      call writb1

      return
      end


      subroutine nusan(kmesh,user,nxelem,gnusa)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      nelem = kmesh(9)
      call ini050(kmesh(23),'nusan: coordinates')
      call ini050(kmesh(17),'nusan: nodalpoints')
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))

      gnusa =  gnusa1(ibuffr(ikelmc),ibuffr(ikelmi),user,nxelem,nelem)

      return
      end

      function gnusa1(kmeshc,coor,user,nxelem,nelem)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),user(*),nodno(4),x(4),y(4),t(4)

      inpelm = 4
      sum = 0.
      do 10 iel = nelem-nxelem,nelem
	 ip = (iel-1)*inpelm
	 do 15 i=1,inpelm
	    nodno(i) = kmeshc(ip+i)
	    x(i)     = coor(1,nodno(i))
	    y(i)     = coor(2,nodno(i))
	    t(i)     = user(5+nodno(i))
15       continue
         a = x(2)-x(1)
	 b = y(4)-y(1)
	 sum = sum + 0.5*a/b*(t(3)+t(4)-t(1)-t(2))
10    continue
      gnusa1 = -sum

      return
      end
