cdccerrsub
      subroutine errsub ( mesnum, nints, nreal, nchar )
c
c ====================================================================
c
c     Programmer     S.Jansen / H.Hanzon
c     Version 2.0    Date   04-02-90 (Adapted to SEPRAN by Guus Segal)
c     Version 1.0    Date   01-01-90
c
c
c
c     Copyright (c) 1990 "Ingenieursbureau SEPRA"
c     Permission to copy or distribute this software or documentation
c     in hard copy or soft copy granted only by written license
c     obtained from "Ingenieursbureau SEPRA"
c     All rights reserved. No part of this publication may be
c     reproduced, stored in a retrieval system (e.g., in memory, disk,
c     or core) or be transmitted by any means, electronic, mechanical,
c     photocopy, recording, or otherwise, without written Permission
c     from the publisher.
c
c ********************************************************************
c
c     ERRSUB prints an error message.
c
c ********************************************************************
c
c     INPUT / OUTPUT PARAMETERS
c
cimp      implicit none
      integer mesnum, nints, nreal, nchar
c
c     mesnum   i    Error number.
c     nchar    i    Number of character strings.
c     nints    i    Number of integers.
c     nreal    i    Number of reals.
c
c ********************************************************************
c
c     COMMON BLOCKS
c

      integer ierror, nerror
      common /cconst/ ierror, nerror
      save /cconst/

c                 /cconst/
c    Error codes and number of errors to print
c
c    ierror  i/o  Error number of last error occurred
c        0        No error present
c       >0        Last run-time error
c    nerror   -   Number of error messages printed
c
c - - - - - - - - - - - - - - - - - - - - - - - - -

      integer irefwr, irefre, irefer
      common /cmcdpi/ irefwr, irefre, irefer
      save /cmcdpi/
c
c                 /cmcdpi/
c    Unit numbers to use for certain standard in- and output files.
c
c    irefwr     Unit number to use for "normal" writes
c    irefre     Same for standard reads (mostly keyboard input)
c    irefer     Same for error messages
c
c - - - - - - - - - - - - - - - - - - - - - - - - -
      character * 80 chars
      character * 200 name
      common /cmessc/ chars(10), name
      save /cmessc/

c                 /cmessc/
c    Contains characters for the error message subroutines
c    Must be used in cooporation with common block cmessg
c
c    chars     Array containing character information for error messages
c              At most 10 positions are available
c
c    name      Name of tree of subroutines concatenated with name of local
c              subroutine to be used for error messages
c              Only the last 80 characters are used in the error message 
c              The number of characters in name is given by lennam
c
c - - - - - - - - - - - - - - - - - - - - - - - - -
      integer ints, lennam
      double precision reals
      common /cmessg/ reals(10), ints(10), lennam
      save /cmessg/

c                 /cmessg/
c    Contains extra information for the error message subroutines
c    Must be used in cooporation with common block cmessc
c
c    ints      Array containing integer information for error messages
c
c    lennam    Length of parameter name
c
c    reals     Array containing real information for error messages
c - - - - - - - - - - - - - - - - - - - - - - - - -
c
c ********************************************************************
c
c     LOCAL PARAMETERS
c
c     None.
c
c ********************************************************************
c
c     I / O
c
c     ERRSUB writes to the default output device.
c
c ********************************************************************
c
c     SUBROUTINES CALLED
c
c     ERMESS   Prints a message, stored in the Error Text File.
c     INSTOP   Closes files and stops the program.
c
c ********************************************************************
c
c     ERROR MESSAGES
c
c     Number of errors = 10 , STOP.
c
c ********************************************************************
c
c     PSEUDO CODE
c
c     Increment NERROR (number of errors);
c     Call ERMESS;
c     if (number of errors = 10) then
c        Print message;
c        STOP;
c
c ====================================================================
c
      nerror = nerror + 1
c
c     --- Print the message, stored in the Error Text File.
c
      if ( lennam.le.80 ) then
          call ermess (name(1:lennam),mesnum,ints,nints,reals,nreal,
     *                 chars,nchar,'ERROR')
      else
          call ermess (name(lennam-79:lennam),mesnum,ints,nints,reals,
     *                 nreal, chars,nchar,'ERROR')
      end if
      ierror = mesnum
c
c     --- Stop if number of errors = 10 .
c
      if (nerror.eq.10) then
         write (irefwr,'(//1x,78(''*'')/)')
         write (irefwr,1500)
         write (irefwr,'(/1x,78(''*'')//)')
         call instop
      endif
c
 1500 format (' STOP : number of errors = 10 ')
c
      end
cdc*eor
