c *************************************************************
c *   PESTOH
c *
c *   Solves the Stokes equation
c *************************************************************
      subroutine pestoh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      implicit double precision(a-h,o-z)
      dimension matrs(*),intmat(*),kmesh(*),kprob(*),isol(*)
      dimension iuser(*),user(*),irhsd(*),matrback(5)
      common /peparam/ rayleigh,rlam,q,visc0,b,c

      save ifirst
      data ifirst/0/

      if (visc0.gt.0.or.ifirst.eq.0) then
         call systm0(1,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         call copymt(matrs,matrback,kprob)
      else
         call systm0(2,matrback,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
      endif
      call solve(1,matrs,isol,irhsd,intmat,kprob)

      ifirst=1

      return
      end
      
