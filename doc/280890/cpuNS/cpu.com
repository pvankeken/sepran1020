xl 20
yl 16
xa 0 400 2000
ya -50 100 700
nd 0 0
xt N
yt cpu
tt Stokes equation
fr
li 1
sy 1 0.1 10
pl buf3.p
leg 300K
dd 0.1
sy 2 0.1 10
pl buf5.p
leg 500K
dd 0.2
sy 3 0.1 10
pl buf10.p
leg 1M
pleg 1 15
