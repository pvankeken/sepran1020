c *************************************************************
c *   pefm2
c *   Fill arrays iinput and rinput to be used in the mesh
c *   creation. The mesh is built by meshgenerator RECTANGLE
c *   for a rectangular geometry ([0,xl]x[0,yl]). To include
c *   refinements in the 
c *************************************************************
      subroutine pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,ichois)
      implicit double precision(a-h,o-z)
      
      dimension iinput(*),rinput(*),fx(*),fy(*),nx(*),ny(*)
      dimension x(300),y(300)
      data ipsurf/0/
      save ipsurf

      if (xl.le.0d0.or.yl.le.0d0) then
         write(6,*) 'PERROR(pefm2): xl <= 0 or yl <= 0'
      endif
      if (ichois.eq.2.and.ipsurf.eq.0) then
         write(6,*) 'PERROR(pefm2): ichois=2 and ipsurf=0'
      endif

      nusx      = nx(1)+nx(2)+nx(3)
      nusy      = ny(1)+ny(2)+ny(3)
      nuspnt    = 4*nusx+4*nusy
      ncurvs    = 4
      nsurfs    = 1
      nsurel    = 1
      irenumber = 3

      iileng = nuspnt + 7 + ncurvs*4 + 20 + 13
      irleng = 7+2*nuspnt
      if (iinput(1).lt.iileng) then
         write(6,*) 'PERROR(pefm2): array iinput too small'
         write(6,*) '                 computed length = ',iileng
      endif
      if (rinput(1).lt.irleng) then
         write(6,*) 'PERROR(pefm2): array rinput too small'
         write(6,*) '                 computed length = ',irleng
      endif

      if (ichois.eq.1) then
c        *** Fill iinput and rinput for the first time.
c        *** Eight curves are defined by userpoints; RECTANGLE
c        *** creates one surface with quadratic triangles. 
         do 10 i=2,20
10          iinput(i) = 0
         iinput(6) = 2
         iinput(7) = nuspnt
         iinput(8) = ncurvs
         iinput(9) = nsurfs
         iinput(12)= nsurel
	 iinput(15)= irenumber

         ipcurv = 21
         ipbase = 1
         do 20 icrv = 1,ncurvs
c           *** pointer to curve information in iinput
            if (icrv.eq.1.or.icrv.eq.3) then
               n = 2*nusx
            else
               n = 2*nusy
            endif
            iinput(ipcurv)   = 2
            iinput(ipcurv+1) = 1
            iinput(ipcurv+2) = n
            iinput(ipcurv+3) = 0
            do 30 ip = 1,n+1
30             iinput(ipcurv+3+ip) = ipbase+ip-1
            ipcurv = ipcurv + 4 + n + 1
            ipbase = ipbase + n
20       continue
c        *** correct last user point number
         iinput(ipcurv-1) = 1        

c        *** surface information
         ipsurf = ipcurv
	 isurf = 1
	 ishape = 4
         iinput(ipsurf)   = isurf
         iinput(ipsurf+1) = ishape
         iinput(ipsurf+2) = ncurvs
         do 40 ic = 1,ncurvs
40          iinput(ipsurf+2+ic) = ic
         iinput(ipsurf+7) = nusx
         iinput(ipsurf+8) = nusy

c        *** Element group information
         ielgrp = 1
	 ifirst = 1
	 ilast  = 1
         iinput(ipsurf+9) = ishape
         iinput(ipsurf+10) = ielgrp
         iinput(ipsurf+11) = ifirst
         iinput(ipsurf+12) = ilast 

c        *** Fill RINPUT with the coordinates of the user points
	 dx = calcd(nx,fx,xl)
	 call calcoor(nx,fx,dx,x)
	 write(6,*) 'x-coor: '
	 do 11 i=3,2*(nx(1)+nx(2)+nx(3))+1,2
11          write(6,*) i,x(i),x(i)-x(i-2)
	 dy = calcd(ny,fy,yl)
	 call calcoor(ny,fy,dy,y)
	 write(6,*) 'y-coor: '
	 do 12 i=3,2*(ny(1)+ny(2)+ny(3))+1,2
12          write(6,*) i,y(i),y(i)-y(i-2)
           
         do 90 ip=2,7
90          rinput(ip)=0d0
         ipbase = 8
         do 100 ip=1,2*nusx+1
            xx = x(ip)
            yy = 0d0         
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
100      ipbase = ipbase+2
         do 120 ip=2,2*nusy+1
            xx = xl
            yy = y(ip)
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
120      ipbase = ipbase+2
         do 140 ip=2*nusx,1,-1
            xx = x(ip)
            yy = yl
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
140      ipbase = ipbase+2
         do 150 ip=2*nusy,2,-1
            yy = y(ip)
            xx = 0d0
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
150      ipbase = ipbase+2


      else if (ichois.eq.2) then
c        *** Use the same nodalpoint distribution and RECTANGLE to
c        *** create mesh with linear triangles
	 ishape = 3
         iinput(ipsurf+1)  = ishape
         iinput(ipsurf+7) = nusx*2
         iinput(ipsurf+8) = nusy*2
         iinput(ipsurf+9) = ishape
         iinput(ipsurf+10) = 1
         iinput(ipsurf+11) = 1
         iinput(ipsurf+12) = 1

      else if (ichois.eq.3) then
c        *** Use the same nodal point distribution and RECTANGLE to
c        *** create mesh with bilinear quadrilaterals
	 ishape = 5
         iinput(ipsurf+1)  = ishape
         iinput(ipsurf+7) = nusx*2
         iinput(ipsurf+8) = nusy*2
         iinput(ipsurf+9) = ishape
         iinput(ipsurf+10) = 1
         iinput(ipsurf+11) = 1
         iinput(ipsurf+12) = 1
      
      else
         write(6,*) 'PERROR(pefm2): unknown value for ICHOIS'
         write(6,*) '                 ICHOIS = ',ichois
      
      endif
         
      return
      end
