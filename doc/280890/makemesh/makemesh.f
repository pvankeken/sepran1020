      program makemesh
      implicit double precision(a-h,o-z)
      dimension iinput(1000),rinput(1000),kmesh(100)

      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      dimension nx(3),ny(3),fx(3),fy(3)
      character*80 fmame

      iinput(1)=1000
      rinput(1)=1000
      kmesh(1)=100
      
      call start(0,1,-1,0)
      yl=1
      read(5,*) xl,ichois
      read(5,*) (nx(i),i=1,3),(fx(i),i=1,2)
      read(5,*) (ny(i),i=1,3),(fy(i),i=1,2)
      call pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,1)
      call mesh(1,iinput,rinput,kmesh)
      open(9,file='mesh1')
      rewind(9)
      call meshwr(9,kmesh)
      call pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,2)
      call mesh(1,iinput,rinput,kmesh)
      open(9,file='mesh2')
      rewind(9)
      call meshwr(9,kmesh)
      call pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,3)
      call mesh(1,iinput,rinput,kmesH)
      open(9,file='mesh3')
      rewind(9)
      call meshwr(9,kmesh)
      close(9)

c     *** Create information on neighbouring nodalpoints of sec. np.
      nxt = nx(1)+nx(2)+nx(3)
      nyt = ny(1)+ny(2)+ny(3)
      call pecresec(nxt,nyt,'secmesh')
 
c     *** PLOT MESHES ***
      jmark    = 5
      iway     = 2
      fname    = 'MESH'
      jkader   = -4
      format   = 10d0
      chheight = 0.3
      yfact    = 1d0

      jkader=-4
      tittext  = 'mesh'
      call plotm2(0,kmesh,iuser,format,yfact)

      end
