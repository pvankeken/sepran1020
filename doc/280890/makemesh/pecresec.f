c *************************************************************
c *   PECRESEC
c *
c *   Create a file with nodalpoint numbers that are the
c *   neighbours of secondary nodal points.
c *
c *   PvK 081189
c *************************************************************
      subroutine pecresec(nx,ny,fname)
      character*80 fname
      dimension nbuf(2,10000)

      npx = 2*nx + 1
      npy = 2*ny + 1 
c     *** fill points of cat A
      do 10 i=1,ny+1
         do 10 j=2,npx-1,2
            ip = (i-1)*2*npx + j
            nbuf(1,ip) = ip-1
            nbuf(2,ip) = ip+1
c           write(6,*) 'A: ',ip,ip-1,ip+1
10    continue            
c     *** fill points of cat B            
      do 20 i=1,ny
         do 20 j=npx+1,2*npx,2
            ip = (i-1)*2*npx+j
            nbuf(1,ip) = ip-npx
            nbuf(2,ip) = ip+npx
c           write(6,*) 'B: ',ip,ip-npx,ip+npx
20    continue
c     *** fill points of cat C
      do 30 i=1,ny
         do 30 j=npx+2,2*npx-1,2      
            ip = (i-1)*2*npx+j
            nbuf(1,ip) = ip-npx+1
            nbuf(2,ip) = ip+npx-1
c           write(6,*) 'C: ',ip,ip-npx+1,ip+npx-1
30    continue

      open(9,file=fname)
      rewind(9)
      write(9,*) npx*npy
      do 40 i=1,npx*npy
         write(9,*) nbuf(1,i),nbuf(2,i)
40    continue
      close(9)
      return
      end
      
      
