	 subroutine calcoor(n,f,d,coor)
	 implicit double precision(a-h,o-z)
	 dimension n(*),f(*),coor(*)

	 n1 = n(1)
	 n2 = n(2)
	 n3 = n(3)
	 f1 = f(1)
	 f3 = f(2)
         coor(1) = 0.
         ip   = 2
	 do 10 i=2,n1
            coor(ip)   = coor(ip-1) + 0.5*d
	    coor(ip+1) = coor(ip-1) + d
	    d          = d*f1
	    ip         = ip+2
10       continue
         do 20 i=n1+1,n1+n2+1
	    coor(ip)   = coor(ip-1) + 0.5*d
	    coor(ip+1) = coor(ip-1) + d
	    ip         = ip+2
20       continue
         do 30 i=n1+n2+2,n1+n2+n3+1
	    coor(ip)   = coor(ip-1) + 0.5*d
	    coor(ip+1) = coor(ip-1) + d
	    ip         = ip+2
	    d          = d*f3
30       continue
	 return
	 end
