      program surface
      implicit double precision(a-h,o-z)
      dimension kmesh(100),kprob(100),iinput(100),rinput(100)
      dimension iuser(100),user(100),isol(5),intmat(5),matr(5)
      dimension irhsd(5),iu1(1),u1(1),ipres(5),iwork(10),work(10)
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,
     v               jkader,jtimes
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /usertext/ text,text1,text2
      common /frcount/ ifrinit,ifrcntr,nbase

      kmesh(1) = 100
      kprob(1) = 100
      iinput(1) = 100
      rinput(1) = 100
       user(1) = 100 
      iuser(1) = 100
     
      call start(0,1,0,0)
      call mesh(2,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call commat(1,kmesh,kprob,intmat)
      write(6,*) 'NSUPER: ',nsuper
      if (nsuper.ne.0) stop
      call presdf(kmesh,kprob,isol)
      call chanbn(-2,number,3,3,kmesh,kprob,isol,iinput,rinput,
     v            iuser,user)
      call mesh(3,iinput,rinput,kmesh)
      ifrinit = 0
      fname = 'PLOT'
      iway  = 2
      jmark = 5
      format = 10d0
      yfaccn = 1
      do 10 i=1,8
         iwork(i) = 0
10       work(i)  = 0d0
      iwork(7) = 1
      work(1)  = 1d-6
      work(2)  = 1d0
      work(6)  = -1d0
      work(8)  = 1d0
      iparm = 8
      call fil100(1,iuser,user,kprob,iparm,iwork,work)
      nsuper = kprob(29)
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,iuser,
     v            user,islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      
      call plwin(1,0d0,0d0)
      call plotm2(0,kmesh,iuser,format,yfaccn)
      call plwin(3,format+2d0,0d0)
      call plotvc(1,2,isol,isol,kmesh,kprob,format,yfaccn,0d0)

      end

      subroutine funccr(coorol,coornw,uold,inodp,ilocal,anorm,n)
      implicit double precision(a-h,o-z)
      dimension coorol(*),coornw(*),uold(*),anorm(*)

      xold = coorol(2*ilocal-1)
      yold = coorol(2*ilocal)
      ynew = yold + 0.1*cos(3.1415926*xold)
      xnew = xold
      coornw(2*ilocal-1) = xnew
      coornw(2*ilocal) = ynew
 
      return
      end
