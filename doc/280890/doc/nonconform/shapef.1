.sc
.po 15n
.he 'Sepran''nonconform'
.fo 'PvK 150390''%'
.EQ
delim $$
.EN
.sz +3
.ce
.u "Shapefunctions for a nonconforming element with $C sub 1$ continuity"

.ce
Peter van Keken, March 15th, 1990
.sz -3

Element matrices resulting from discretization of the biharmonical
equation by the finite element method contain second derivatives of
the shapefunctions, which have to be (at least) $C sub 1$ continuous.
For conforming elements this condition is difficult to impose.
We consider the shapefunctions of a non-conforming rectangular element
with four nodal points and 12 degrees of freedom ([1],[2]).

Consider a rectangular element $OMEGA~=~[0,a] times [0,b]$
with four nodalpoints in the corners of the element.
The nodal displacements are given by 
.EQ (1)
bold {a sub i} ~~=~~ left [  ~~~
   cpile { 
      psi sub i above 
      ({partial psi} over {partial y}) sub i above
      - ({partial psi} over {partial x}) sub i
    } ~~~right ]
.EN
The element displacement vector is given by
.EQ (2)
bold {a sup e} ~~=~~ left [~~~
   cpile {
      bold {a sub 1} above
      bold {a sub 2} above
      bold {a sub 3} above
      bold {a sub 4} 
   }~~~right ]
.EN
The displacement in the element is given by
.EQ (3)
psi ~~=~~ bold N bold {a sup e}
.EN
where $bold N$ is the $12 times 1$ matrix containing the shapefunctions.
The shapefunctions are derived using the polynomial expansion
.EQ (4)
psi ~~=~~ alpha sub 1 ~+~
          alpha sub 2 x ~+~
          alpha sub 3 y ~+~
          alpha sub 4 x sup 2 ~+~
          alpha sub 5 xy ~+~
          alpha sub 6 y sup 2 ~+~
          alpha sub 7 x sup 3 ~+~
          alpha sub 8 x sup 2 y ~+~
          alpha sub 9 x y sup 2 ~+~
          alpha sub 10 y sup 3 ~+~
          alpha sub 11 x sup 3 y ~+~
          alpha sub 12 x y sup 3
.EN
Explicit expressions are given by [2] and [3]. Following the
latter 
(where the coordinates of the nodalpoints are $bold x sub 1 ~=~ (0,0)$,
$bold x sub 2~=~(0,b)$, $bold x sub 3~=~(a,b)$ 
and $bold x sub 4~=~(a,0)$) 
we can write with $xi ~=~ x/a$ and $eta~=~y/b$ (note the typing errors!)

.EQ I (5)
bold N ~~~~=~~~~~~~~~~~~~size +2 [~~ mark
      1 - xi eta - (3-2 xi ) xi sup 2 (1- eta ) ~-~
            (1- xi ) ( 3 - 2 eta ) eta sup 2          ~~, 
.EN
.EQ I
lineup lpile {
        (1 - xi ) eta (1 - eta ) sup 2 b              ~~, above
        - xi ( 1 - xi ) sup 2 ( 1 - eta ) a           ~~, above

        (3-2 xi ) xi sup 2 ( 1- eta ) ~+~
        xi eta ( 1 - eta ) ( 1 - 2 eta )           ~~, above
        xi eta ( 1 - eta ) sup 2 b                    ~~, above
        (1 - xi ) xi sup 2 ( 1 - eta ) a              ~~, above

        (3-2 xi ) xi sup 2 eta ~-~
           xi eta ( 1 - eta ) ( 1 - 2 eta )          ~~, above
        - xi ( 1 - eta ) eta sup 2 b                  ~~, above
        (1 - xi ) xi sup 2 eta a                      ~~, above

        ( 1 - xi ) ( 3 - 2 eta ) eta sup 2 ~+~
            xi (1- xi )( 1 - 2 xi ) eta               ~~, above
        - ( 1 - xi ) ( 1 - eta ) eta sup 2 b          ~~, above
        - xi ( 1 - xi ) sup 2 eta a                   ~~size +2 ]
   }
.EN

A subroutine to calculate the basisfunctions and $psi$ for
variable $x$ and $y$ is appended. The formulation is tested
by calculating the shapefunctions, 
the nodalpoint displacements $bold {a sup e}$ 
and $psi$ thru (3) for
the function $psi sub 0$ given by (2) with $alpha sub i~=~1~,~~i=1,12$.
$psi$ is then an exact representation of $psi sub 0$.

Often the coordinates are chosen such that $bold x sub 1~=~(0,0)$,
$bold x sub 2~=~(a,0)$, $x sub 3~=~(a,b)$, $x sub 4~=~(0,b)$.
We can rewrite (5) using the relations
.EQ (6)
define xii '{ xi sub i }'
define etai '{ eta sub i}'
define dpdx '{ { partial psi } over { partial x }}'
define dpdy '{ { partial psi } over { partial y }}'
define N1i '{  N sub i sup 1 }'
define N2i '{ N sub i sup 2}'
define N3i '{ N sub i sup 3}'
psi ( xi , eta ) ~~=~~ 
  sum from i=1 to 4~ psi ( xii , etai ) N1i ( xi , eta ) ~~~+~~~
  sum from i=1 to 4~dpdy ( xii , etai ) N2i ( xi , eta ) ~~~+~~~
  sum from i=1 to 4~- dpdx ( xii , etai ) N3i ( xi , eta )
.EN
and 
.EQ (7)
psi ( eta , xi ) ~~=~~ 
  sum from i=1,4~ psi ( etai , xii ) N2i ( eta , xi ) ~~~+~~~
  sum from i=1,4~ - dpdy ( etai , xii ) ( - N3i ( eta , xi ) ) ~~~+~~~
  sum from i=1,4~ dpdx ( etai , xii ) ( - N2i ( eta , xi ) )
.EN
to
.EQ I (8)
bold N ~~~=~~~ size +2 [~~ mark
   1 - eta xi - ( 3 - 2 eta ) eta sup 2 ( 1 - xi ) ~-~
     (1 - eta ) ( 3 - 2 xi ) xi sup 2 ~~~~~~~~~~, 
.EN
.EQ I
   lineup lpile { eta ( 1 - eta ) sup 2 ( 1 - xi ) b ~~~~~~~~~~, above
   - ( 1 - eta ) xi ( 1- xi ) sup 2 a ~~~~~~~~~~, above

   ( 1 - eta ) ( 3 - 2 xi ) xi sup 2 ~+~
      eta ( 1 - eta ) ( 1 - 2 eta ) xi ~~~~~~~~~~, above
   eta ( 1 - eta ) sup 2 xi b          ~~~~~~~~~~, above
   (1 - eta ) ( 1 - xi ) xi sup 2 a    ~~~~~~~~~~, above

   ( 3 - 2 eta ) eta sup 2 xi ~-~
     eta xi ( 1 - xi ) ( 1 - 2 xi )    ~~~~~~~~~~, above
   - ( 1- eta ) eta sup 2 xi b         ~~~~~~~~~~, above
   xi sup 2 ( 1 - xi ) eta a         ~~~~~~~~~~, above

   ( 3 - 2 eta ) eta sup 2 ( 1 - xi ) ~+~
     eta xi ( 1 - xi ) ( 1 - 2 xi )    ~~~~~~~~~~, above
   - ( 1 - eta ) eta sup 2 ( 1- xi ) b ~~~~~~~~~~, above
   - xi eta ( 1 - xi ) sup 2 a         ~~~~~~~~~~size +2 ] }
.EN


.b "References"
.nr ii +10n
.ip [1]
Zienkiewicz, O.C. (1977), The finite element method, McGraw-Hill
.ip [2]
Hansen, U. and A. Ebel (1984), Numerical and dynamical stability
of convection cells in the Rayleigh number range $10 sup 3~-~
8 cdot 10 sup 5$.
.i "Annales Geophysicae",
.b 2 ,
291-302.
.ip [3]
Cheung, Y.K. and M.F. Yeo (1979), A practical introduction to
Finite element analysis, Pitman.
.bp
.lp
.(l
\fC
.sz -3
c *************************************************************
c *    Shapefunction of the non-conforming cubic element
c *    with 4 nodalpoints and unknowns (psi,u,v) 
c *
c *    Formulation of Cheung; x2=(0,b) x4=(a,0)
c *************************************************************
       function shapeche(xk,yk,x,y,psi,u,v)
       implicit double precision(a-h,o-z)
       dimension xk(4),yk(4),psi(4),u(4),v(4)
       dimension xik(4),etak(4)
       dimension rn1(4),rn2(4),rn3(4)

       shapeche = 0
       a = xk(3)-xk(2)
       b = yk(2)-yk(1)
       x0 = xk(1)
       y0 = yk(1)
       xi = (x-x0)/a
       eta= (y-y0)/b
       write(6,'(''x0,y0,a,b,xi,eta'',4f8.2)') x0,y0,a,b,xi,eta
       rn1(1) = 1-xi*eta-(3-2*xi)*xi*xi*(1-eta)-(1-xi)*(3-2*eta)*eta*eta
       rn2(1) = (1-xi)*eta*(1-eta)*(1-eta)*b
       rn3(1) = -xi*(1-xi)*(1-xi)*(1-eta)*a
       rn1(2) = (1-xi)*(3-2*eta)*eta*eta+xi*(1-xi)*(1-2*xi)*eta
       rn2(2) = -(1-xi)*(1-eta)*eta*eta*b
       rn3(2) = -xi*(1-xi)*(1-xi)*eta*a
       rn1(3) = (3-2*xi)*xi*xi*eta-xi*eta*(1-eta)*(1-2*eta)
       rn2(3) = -xi*(1-eta)*eta*eta*b
       rn3(3) = (1-xi)*xi*xi*eta*a
       rn1(4) = (3-2*xi)*xi*xi*(1-eta)+xi*eta*(1-eta)*(1-2*eta)
       rn2(4) = xi*eta*(1-eta)*(1-eta)*b
       rn3(4) = (1-xi)*xi*xi*(1-eta)*a
       r1=0
       r2=0
       r3=0
       do 20 i=1,4
	 r1 = rn1(i)+r1
	 r2 = rn2(i)+r2
	 r3 = rn3(i)+r3
	 write(6,'(3f12.4)') rn1(i),rn2(i),rn3(i)
	 shapeche = shapeche + rn1(i)*psi(i) + rn2(i)*u(i) + rn3(i)*v(i)
20     continue
       write(6,'(3f12.4)') r1,r2,r3

       return
       end
.sz +3
\fR
.)l
.bp
.(l
\fC
.sz -3
c *************************************************************
c *    Shapefunction of the non-conforming cubic element
c *    with 4 nodalpoints and unknowns (psi,u,v) 
c *
c *    Formulation of Cheung; x2=(a,0) x4=(0,b)
c *************************************************************
       function shapeche(xk,yk,x,y,psi,u,v)
       implicit double precision(a-h,o-z)
       dimension xk(4),yk(4),psi(4),u(4),v(4)
       dimension xik(4),etak(4)
       dimension rn1(4),rn2(4),rn3(4)

       shapeche = 0
       a = xk(2)-xk(1)
       b = yk(3)-yk(2)
       x0 = xk(1)
       y0 = yk(1)
       xi = (x-x0)/a
       eta= (y-y0)/b
       write(6,'(''x0,y0,a,b,xi,eta'',4f8.2)') x0,y0,a,b,xi,eta
       rn1(1) = 1-eta*xi-(3-2*eta)*eta*eta*(1-xi)-(1-eta)*(3-2*xi)*xi*xi
       rn1(2) = (1-eta)*(3-2*xi)*xi*xi+eta*(1-eta)*(1-2*eta)*xi
       rn1(3) = (3-2*eta)*eta*eta*xi-eta*xi*(1-xi)*(1-2*xi) 
       rn1(4) = (3-2*eta)*eta*eta*(1-xi)+eta*xi*(1-xi)*(1-2*xi) 
       rn2(1) = eta*(1-eta)*(1-eta)*(1-xi)*b
       rn2(2) = eta*(1-eta)*(1-eta)*xi*b
       rn2(3) = -(1-eta)*eta*eta*xi*b
       rn2(4) = -(1-eta)*eta*eta*(1-xi)*b
       rn3(1) = -(1-eta)*xi*(1-xi)*(1-xi)*a
       rn3(2) =  (1-eta)*(1-xi)*xi*xi*a
       rn3(3) =  eta*xi*xi*(1-xi)*a
       rn3(4) = -eta*xi*(1-xi)*(1-xi)*a
       do 20 i=1,4
	 shapeche = shapeche + rn1(i)*psi(i) + rn2(i)*u(i) + rn3(i)*v(i)
20     continue

       return
       end
\fR
.)l
