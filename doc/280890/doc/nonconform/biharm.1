.sc
.po 15n
.he 'Sepran''Nonconform'
.fo 'PvK 170490''%'
.EQ
delim $$
.EN
.sz +3
.ce
.u "Solving the biharmonical equation with the nonconforming element"

.ce
Peter van Keken, April 17, 1990
.sz -3




.sh 1 "Introduction"
.lp
A nonconforming element with $C sub 1$ continuity is used to solve
the biharmonical equation on a rectangular geometry. 
The element is described in [1]. Explicit formulae for the
shapefunctions are given in [2]. The element has been used to solve
the equation of Laplace. Programming examples of this are given
in [3]. The element will be used to solve the equation
.EQ (1)
grad sup 4 ~psi~~=~~ Ra {partial T} over { partial x}
.EN
that arises in mantle convection models using the streamfunction
formulation ([1]). Later we will consider the use of this element
for the modelling of Rayleigh-Taylor instabilities.

.sh 1 "Notation"
.lp
The element under consideration contains 4 nodalpoints and 12 
degrees of freedom. In each nodalpoint 3 degrees of freedom
$psi$, $u~=~{partial psi}/{partial y}$ 
and $v~=~-{partial psi}/{partial x}$ are specified. We will
denote the shapefunctions with $N sub i ~~(i=1,12)$ and the
degrees of freedom with $psi sub i  ~~(i=1,12)$, where
$psi sub 1~=~( psi ) sub 1$, $psi sub 2~=~(u) sub 1$, 
$psi sub 3~=~(v) sub 1$, $psi sub 4~=~( psi ) sub 2$ etc.
The displacement (or streamfunction) in the element is given
.EQ(2)
psi~~=~~sum from j=1 to 12~psi sub j ~N sub j
.EN

.sh 1 "The element matrix"
.lp
The coefficients of the element stiffness matrix are given by ([1])
.EQ (3)
define 2inte '{ size +2 {int int} from e}'
define p2 '{partial sup 2}'
define Ni '{N sub i}'
define Nj '{N sub j}'
define px2 '{partial x sup 2}'
define py2 '{partial y sup 2}'
define pxy '{partial x partial y}'
bold S sub ij ~~=~~ 2inte ~left [~
      left ( {p2 Ni} over px2~-~{p2 Ni} over py2 right ) cdot
      left ( {p2 Nj} over px2~-~{p2 Nj} over py2 right ) ~+~
      4 {p2 Ni} over pxy {p2 Nj} over pxy ~~right ]~dxdy
.EN
The element integral is calculated with 
a Gaussian quadrature rule. The Sepran element routine is 
programmed following the structure given in [4]. The values of
the second derivatives in the Gauss points are stored in
common \fCpeshape\fR. A listing of this subroutine \fCc1bihi\fR
is appended.


.sh 1 "The load vector"
.lp
The element load vector $bold f sub j$ is given by
.EQ (5)
bold f sub j ~~=~~ 2inte ~left [~Nj Ra {partial T} over {partial x}
~right ] dxdy
.EN
The temperature gradient can be derived in the nodalpoints. The
integral is then calculated by interpolating the nodalpoints values
of ${partial T}/{partial x}$ with the shapefunctions $N sub j$.
It has been found that this procedure yields inaccurate results
for convection models. A better approach is to use the nodalpoint
values of the temperature field itself in determining the temperature
gradient in the Gauss points, by writing
.EQ (6)
{partial T} over {partial x}~~=~~
  sum from j=1 to 4 ~T sub j  {partial L sub j} over { partial x}
.EN
where $L sub j$ are the basisfunctions for the temperature.
For a rectangular element with bilinear shapefunctions this leads
to
.EQ (7)
define T1 '{T sub 1}'
define T2 '{T sub 2}'
define T3 '{T sub 3}'
define T4 '{T sub 4}'
{partial T} over {partial x} ( bold x sub gauss )~~=~~
   {eta sub gauss} over a ( T1 - T2 + T3 - T4 ) ~+~ 1 over a ( T2 - T1 )
.EN
A practical implementation of this is given in the appended subroutine 
\fCc1rhdtx\fR.


.sh 1 "Derived quantities: rms velocity"
.lp
The root mean square velocity is given by
.EQ (8)
v sub rms sup 2 ~~=~~ sum from k=1 to {N sub elem} ~
    2inte ~( u sup 2 ~+~ v sup 2 ) ~~dxdy
.EN
In each element this integral is calculated with Gaussian quadrature.
The velocity components in the Gauss points are derived from the
streamfunction:
.EQ (9a)
u ( bold x sub gauss )~~=~~ {partial psi} over {partial y} ~~=~~
  sum from j=1 to 12 ~psi sub j {partial Nj ( bold x sub gauss) } 
  over {py}
.EN
and
.EQ (9b)
v ( bold x sub gauss )~~=~~ -{partial psi} over {partial y} ~~=~~
  -~sum from j=1 to 12 ~psi sub j {partial Nj ( bold x sub gauss) }
  over {px}
.EN

.sh 1 "Derived quantities: local temperature extrema"
.lp
.EQ
define z1 '{z sub 1}'
define z2 '{z sub 2}'
define z3 '{z sub 3}'
define lj '{l sub j}'
define zj '{z sub j}'
.EN
A temperature profile in the center of the box ($x= lambda / 2$)
is easily obtained for a rectangular and symmetrical mesh. For
comparison of numerical codes the position and value of the 
local temperature extrema are sometimes used ([5]).
If the temperature is approximated with linear shapefunction
the position and value of the maximum is determined by the
nodalpoint distribution. A way to circumvent this restriction
is to assume a quadratic interpolation of the temperature
profile. First we determine the nodalpoint with the local temperature
extremum, say $T2$ at depth $z2$, with neighbouring nodalpoints at
depth $z1$ and $z3$. We can then write
.EQ (10)
T(z)~~=~~sum from j=1 to 3~lj T ( zj )
.EN
which is the familiar Langrangian interpolation formula with
coefficients (e.g. [6])
.EQ (11)
l sub 1 ~~=~~ { (z- z2 )(z - z3 ) } over { ( z1 - z2 )( z1 - z3 ) }~~~~~
l sub 2 ~~=~~ { (z- z1 )(z - z3 ) } over { ( z2 - z1 )( z2 - z3 ) }~~~~~
l sub 3 ~~=~~ { (z- z1 )(z - z2 ) } over { ( z3 - z1 )( z3 - z2 ) }
.EN
The position of the extremum $z sub 0$ is given by
.EQ (12)
T prime ( z sub 0 ) ~~=~~ 
   sum from j=` to 3~ {l sub j} prime T ( zj ) ~~=~~ 0
.EN
From this we can solve for $z sub 0$:
.EQ (13)
z sub 0 ~~=~~ 1 over 2 cdot { ( z2 + z3 ) ( z2 - z3 ) T1 ~-~ 
   ( z1 + z3 ) ( z1 - z3 ) T2 ~+~
   ( z1 + z2 ) ( z1 - z2 ) T3 } over
    { ( z2 - z3 ) T1 ~-~ ( z1 - z3 ) T2 ~+~ ( z1 - z2 ) T3 }
.EN
Substitution of $z sub 0$ in (10) yields the value of the temperature
extremum.
.bp
.sh 1 "Results"
.lp
Preliminary results for mantle convection models show 
that the use of this nonconforming element
yields about as accurate results as the use of the standard Sepran
element type 400 for the penalty function method, 
but is more expensive.
A discussion of results for the benchmark comparison using different
approaches will be given in a later report.

.b "References"

.ip [1]
Hansen, U. and A. Ebel,
Numerical and dynamical stability of convection cells in the
Rayleigh number range $10 sup 3 - 8 cdot 10 sup 5$,
.i "Annales Geophysicae"
(1984)
.b 2 ,
291-302.
.ip [2]
Peter van Keken, March 15th, 1990, Shapefunctions for a nonconforming
element with $C sub 1$ continuity
.ip [3]
Peter van Keken, April 4, 1990, Solving the equation of Laplace with
the nonconforming element.
.ip [4]
Peter van Keken, March 20, 1990, Programming examples of element
routines
.ip [5]
B. Blankenbach et al.,
A benchmark comparison for mantle convection codes, 
.i "Geophys.J.Int."
(1989)
.b "98" ,
23-28.
.ip [6]
Ralston, A.,
A first course in numerical analysis,
1965,
McGraw-Hill.
.lp

