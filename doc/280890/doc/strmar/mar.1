.sc
.po 15n
.he 'Sepran''Markerchain method'
.fo 'PvK 201189''%'
.EQ
delim $$
define arrow '{ = back 20 > }'
define q1 '{q sub 1}'
define q2 '{q sub 2}'
define vrms '{v sub rms}'
define cput '{cpu sub t}'
define cpu1 '{cpu sub 1}'
define cpu2 '{cpu sub 2}'
.EN
.sz +2
.ce
.u "The markerchain method combined with the streamfunction-vorticity formulation in Sepran"
.sp
.ce
.u "I    The method"
.sp
.ce
Peter van Keken,    November 20, 1989.
.sz -2
.sp 2
.sh 1 "Introduction"
.lp
The use of the markerchain method for the modelling of Rayleigh-Taylor
instabilities ([1]) is studied. If primitive variables are used in 
the solution of the Stokes equation it is then necessary to calculate
the contribution of the buoyancy forces by calculating
surface-integrals over parts of the elements,
which creates a considerable geometrical problem ([2]).
This can be circumvented in the special case of an isoviscous model
by the streamfunction approach. In this case the contribution of
the bouyancy forces is calculated by a line integral over the
markerchain (section 2). 
The biharmonical equation can now be solved in Sepran using the
streamfunction-vorticity formulation ([3]). The following is
a description of the implementation of the markerchain method.

.sh 1 "Calculation of the streamfunction"
.lp
Consider a model of a Rayleigh-Taylor instability of two isoviscous
fluid layers with stress-free boundaries. 
If the variables are non-dimensionalized according to [4], then the
streamfunction is given by 
.EQ (1)
grad sup 4~ psi ~~=~~ -~{partial GAMMA} over {partial x}
.EN
where $GAMMA$ is a stepfunction
.EQ (2)
GAMMA ~~=~~ left { lpile { ~~0~roman {in~layer~1} above 
                           ~~1~roman {in~layer~2} 
                         }
.EN
In the numerical model the position of this stepfunction is approached
by a markerchain ($(x sub v ~,~y sub v ),~v=1 . . . NP$).
By introducing the vorticity $OMEGA$ we can write
.EQ (3)
grad sup 2 ~psi~~=~~-~OMEGA
.EN
.EQ (4)
grad sup 2 ~OMEGA ~~=~~{ partial GAMMA } over { partial x }
.EN
Equation (4) can be solved by introducing an approximating function
.EQ (5)
OMEGA bar ~~=~~ sum from i=1 to N ~omega sub i phi sub i
.EN
and using the method of Galerkin to obtain a system of equations
.EQ (6)
bold S~bold omega ~~=~~ bold f
.EN
where
.EQ
bold omega ~=~ (~omega sub 1~,~omega sub 2~,~. . . . ~,
~omega sub N ~) sup T
.EN
.EQ
s sub ij~=~int int ~grad phi sub i cdot grad phi sub j~dxdy
.EN
and
.EQ (7)
f sub i ~~=~~ int int ~phi sub i { partial GAMMA } over { partial x }~dxdy
.EN
.bp
The derivative of a stepfunction is a delta function, and
(7) can be approximated by ([5])
.EQ (8)
f sub i ~~ approx ~~ sum from v=1 to NP ~
     phi sub i ( x sub v ~,~y sub v ) ~ { y sub v+1 ~-~ y sub v-1 } over 2
.EN
$bold f$ is assembled elementwise.
For an elementvector $f sub i sup e$ is the contribution of (8) only
non-zero if marker $v$ is positioned in the element $e$. 

For a given position of the markerchain the vorticity can be calculated
by solving the system (6). The streamfunction is calculated by solving
equation (3) (see [3]).

.sh 1 "Advancing the markers"
.lp
The fluid velocity can be derived from the streamfunction. 
The markers are advected by the flow and the path of a marker is
given by the nonlinear differential equation
.EQ (8)
bold { x dot } (t) ~=~bold v [ bold x (t)~,~t ]  
.EN
To solve this equation a predictor-corrector method is used.
The position of a marker at time $t sup n+1$ is predicted 
shifting the markers from the positions at time $t sup n$
along the instantaneous streamlines
.EQ (9)
bold x sub 0 sup n+1~~=~~bold x sup n ~+~ 
    DELTA t ~bold v sup n ( bold x sup n )
.EN
From this position a prediction of the streamfunction at $t~+~DELTA t$
is derived, which is used to correct position of the markers
.EQ (10)
bold x sub 1 sup n+1 ~~=~~ bold x sup n ~+~
   {DELTA t} over 2 left [~ bold v sup n ( bold x sup n )~+~ 
     bold v sup n+1 ( bold x sub 0 sup n+1 ) ~right ]
.EN
This corrector step can be repeated.

The timestep $DELTA t$ is limited by the CFL-criterion
.EQ (11)
DELTA t ~<=~ min ( u / DELTA x , v / DELTA y )
.EN
where $DELTA x$ and $DELTA y$ refer to the local gridspacing and 
$u$ and $v$ to the local velocity components.


.sh 1 "Implementation in Sepran"  
.sp
Following algorithm can be employed to study the 
development of the model
.(l
\fI
create initial position for markerchain
for each timestep
    solve (3) and (4) for the streamfunction
    determine velocity in the markers
    determine timestep
    predict new position of the markers
    solve (3) and (4) with the new position
    determine predicted velocity in the markers
    correct the position of the markers
    If necessary, regrid the markerchain
\fR
.)l

This is implemented in program \fCstrmar\fR, of which the source code
is appended. The main program consists mainly of subroutine calls:
\fCmarstart\fR starts the program, creates the mesh, problem definition
and initial condition for the markers. It furthermore stores information
on the mesh in common \fCpexcyc\fR, which is needed by several 
subroutines (section 4.1).
\fCsolmar\fR solves equation (4) (see section 4.2).
\fCsolstr\fR solves equation (3) (see [3]).
\fCdetvel\fR interpolates the velocity for the markers (section 4.3).
\fCpedetcfl\fR determines the maximum timestep allowed by the CFL-criterion (11).
\fCpredcoor\fR predicts the new position of the markers following
equation (9); \fCcorcoor\fR corrects this position (equation (10)).
Each fifth timestep the markerchain is regridded by \fCremarker\fR.
By interpolation, markers are equidistantly distributed on the chain.
The post processing is handled by subroutines 
\fCmarout\fR and \fCcrepf\fR, which should be self explanatory.


.sh 2 "Information on the mesh"
.lp
It is necessary to be able to determine the element in which a 
marker is positioned, e.g. when interpolating the velocity or 
building the element vectors. The calculation is simplified enormously
when a regular element distribution is used. In this program it
is assumed that meshgenerator RECTANGLE is used to discretize a
rectangular region. In this case it is suffient to know the 
distribution of nodal points along the boundaries. The element
number is then easily calculated, by determining
the x- and y-interval in which the marker is positioned.
Note that refinements in the mesh are allowed.
Furthermore information on the local gridspacing is required when
calculation the maximum timestep.
.br
Subroutine \fCpefilxcyc\fR is used to obtain the information on the
the nodalpoint distribution. Subroutine \fCpedetel\fR can be used
to find the element number corresponding to a given marker.



.sh 2 "Calculating the element vectors for equation (4)"
.lp
The element vectors are created by the userwritten subroutine \fCelem\fR
which is called by the second call of \fCsystm0\fR in subroutine
\fCsolmar\fR. For each element it is necessary to know which markers
are positioned in the element, calculate the contribution to (8)
for each marker and add the result to obtain the elementvector.
Arrays \fCiuser\fR and \fCuser\fR are used to pass the information
on the positions of the markers. 
For each marker is the corresponding element number stored in \fCiuser\fR.
\fCuser\fR contains the coordinates of the markers.
The coordinates of the nodalpoints are obtained from array \fCcoor\fR
using \fCindex1\fR (see Sepran Programmer's Guide). 
If a marker is in the element then
the value of the basisfunctions in the marker are calculated and
the contribution to (8) is stored for each nodalpoint in the element
vector. If the marker is at the end of the chain an adaption to
the integration formula (8) is necessary.

.sh 2 "Determination of the velocity in the markers"
.lp
The velocity is obtained by differentiating the streamfunction. 
The streamfunction for a point in the element is given by
.EQ (12)
psi ~~=~~ psi sub 1 phi sub 1 ~+~ psi sub 2 phi sub 2 
   ~+~ psi sub 3 phi sub 3
.EN
For linear triangles this can be written as
.EQ (13)
psi ~~=~~ psi sub 1 ( a sub 1 + b sub 1 x + c sub 1 y ) ~+~
          psi sub 2 ( a sub 2 + b sub 2 x + c sub 2 y ) ~+~
          psi sub 3 ( a sub 3 + b sub 3 x + c sub 3 y )
.EN
(see [6] for a definition of the coefficients).
The velocity components are then given by
.EQ (14)
u ~~=~~ { partial psi } over { partial y } ~~=~~
   psi sub 1 c sub 1 ~+~ psi sub 2 c sub 2 ~+~ psi sub 3 c sub 3
.EN
and
.EQ (15)
v ~~=~~ -~{ partial psi } over { partial x } ~~=~~
   - psi sub 1 b sub 1 ~-~ psi sub 2 b sub 2 ~-~ psi sub 3 b sub 3
.EN
Similar expressions are derived for quadratic triangles (see appendix).

The velocity in each marker on the chain are calculated by subroutine
\fCdetvel\fR, which calls \fCpeint02\fR for linear and \fCpeint03\fR
for quadratic triangles.
.bp
.sh 1 "References"
.ip [1]
Woidt, W.D. (1978),
Finite element calculations applied to salt dome analysis,
.i Tectonophysics ,
.b 50 ,
369-386.
.ip [2]
Van Keken, P. (August 22, 1989),
Implementation of the markerchain method in SEPRAN.
.ip [3]
Van Keken, P. (November 7, 1989),
Use of the streamfunction-vorticity formulation in solving the
Stokes equation.
.ip [4]
Weijermars, R. and H. Schmeling (1986),
Scaling of Newtonian and non-Newtonian fluid dynamics without inertia
for quantitative modelling of rock flow due to gravity (including the
concept of rheological similarity),
.i "Phys.Earth Plan.Int" ,
.b 43 ,
316-330.
.ip [5]
Christensen, U.R. (1982),
Phase boundaries in finite amplitude mantle convection,
.i "Geophys.J.R.astr.Soc" ,
.b 68 ,
487-497.
.ip [6]
Cuvelier, C., A. Segal and A.A. van Steenhoven (1988),
Finite element methods and Navier-Stokes equations,
D. Reidel Publishing Company, Dordrecht.

.lp


.b "Appendix: derivation of velocity for quadratic triangles"

Shape functions for quadratic triangles are given by (see [6])
.EQ (A.1)
define l1 '{ lambda sub 1 }'
define l2 '{ lambda sub 2 }'
define l3 '{ lambda sub 3 }'
define s1 '{ psi sub 1 }'
define s2 '{ psi sub 2 }'
define s3 '{ psi sub 3 }'
define s4 '{ psi sub 4 }'
define s5 '{ psi sub 5 }'
define s6 '{ psi sub 6 }'
define p1 '{ phi sub 1 }'
define p2 '{ phi sub 2 }'
define p3 '{ phi sub 3 }'
define p4 '{ phi sub 4 }'
define p5 '{ phi sub 5 }'
define p6 '{ phi sub 6 }'
define a1 '{ a sub 1 }'
define a2 '{ a sub 2 }'
define a3 '{ a sub 3 }'
define b1 '{ b sub 1 }'
define b2 '{ b sub 2 }'
define b3 '{ b sub 3 }'
define c1 '{ c sub 1 }'
define c2 '{ c sub 2 }'
define c3 '{ c sub 3 }'
define b12 '{ b sub 1 sup 2 }'
define b22 '{ b sub 2 sup 2 }'
define b32 '{ b sub 3 sup 2 }'
define c12 '{ c sub 1 sup 2 }'
define c22 '{ c sub 2 sup 2 }'
define c32 '{ c sub 3 sup 2 }'
lpile { 
    p1 ~=~ l1 ( 2 l1 - 1 ) above
    p2 ~=~ 4 l1 l2 above
    p3 ~=~ l2 ( 2 l2 - 1 ) above
    p4 ~=~ 4 l2 l3 above
    p5 ~=~ l3 ( 2 l3 - 1 ) above
    p6 ~=~ 4 l3 l1 
}
.EN
where $lambda sub i$ are the linear functions
.EQ (A.2)
lpile {
    l1 ~=~ a1 + b1 x + c1 y above
    l2 ~=~ a2 + b2 x + c2 y above
    l3 ~=~ a3 + b3 x + c3 y 
}
.EN

For the velocity components we can now derive
.EQ 
u ~~=~~ { partial psi } over { partial y } ~~=~~ 
    partial  over { partial y } 
           ( sum from i=1 to 6 ~ phi sub i psi sub i )  ~~=~~
    sum from i=1 to 6 ~ 
        psi sub i { partial phi sub i } over { partial y } ~~=~~
.EN
.EQ (A.3) 
    s1 ~(~ 4 c12 y + 4 a1 c1 - c1 + 4 b1 c1 x ~) ~+~ 
    s3 ~(~ 4 c22 y + 4 a2 c2 - c2 + 4 b2 c2 x ~) ~+~
    s5 ~(~ 4 c32 y + 4 a3 c3 - c3 + 4 b3 c3 x ~) ~+~
.EN
.EQ 
  4 s2 ~(~ 2 c1 c2 y + a1 c2 + a2 c1 + b1 c2 x + b2 c1 x ~)~+~ 
  4 s4 ~(~ 2 c2 c3 y + a2 c3 + a3 c2 + b2 c3 x + b3 c2 x ~)~+~ 
.EN
.EQ
  4 s6 ~(~ 2 c3 c1 y + a3 c1 + a1 c3 + b3 c1 x + b1 c3 x ~)
.EN
and similarly
.EQ 
v ~~=~~
  -~ s1 ~(~ 4 b12 x + 4 a1 b1 - b1 + 4 b1 c1 y ~) 
  -~ s3 ~(~ 4 b22 x + 4 a2 b2 - b2 + 4 b2 c2 y ~)
  -~ s5 ~(~ 4 b32 x + 4 a3 b3 - b3 + 4 b3 c3 y ~)
.EN
.EQ (A.4)
  -~ 4 s2 ~(~ 2 b1 b2 x + a1 b2 + b1 a2 + b2 c1 y + b1 c2 y ~) 
  -~ 4 s4 ~(~ 2 b2 b3 x + a2 b3 + b2 a3 + b3 c2 y + b2 c3 y ~) 
.EN
.EQ
  -~ 4 s6 ~(~ 2 b3 b1 x + a3 b1 + b3 a1 + b1 c3 y + b3 c1 y ~) 
.EN

  
