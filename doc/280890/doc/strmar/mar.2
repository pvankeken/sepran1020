.sc
.po 15n
.he 'Sepran''Markerchain method'
.fo 'PvK 231189''%'
.EQ
delim $$
define arrow '{ = back 20 > }'
define q1 '{q sub 1}'
define q2 '{q sub 2}'
define vrms '{v sub rms}'
define cput '{cpu sub t}'
define cpu1 '{cpu sub 1}'
define cpu2 '{cpu sub 2}'
.EN
.sz +2
.ce
.u "The markerchain method combined with the streamfunction-vorticity formulation in Sepran"
.sp
.ce
.u "II  Results"
.sp
.ce
Peter van Keken,  November 23, 1989.
.sz -2
.sp 2
.sh 1 "Introduction"
.lp
The accuracy and efficiency of the method described in [1] are studied,
using a model of a Rayleigh-Taylor instability of two layers with equal
thicknesses,
with free slipboundaries, $DELTA eta~=~0$, $DELTA rho~=~1$.
The interface is initially disturbed with an sinusoidal displacement
with small amplitude and wavelength $lambda~=~2$. According to
linear stability analysis ([2]) the amplitude of the interface with 
initially grow exponentially, with a growth factor $kappa~=~0.0531307$.
Four distinct cases will be considered in this report
.np
Effect of discretization on the growth factor
.np
Effect of time integration on the growth factor
.np
Modelling of initial to intermediate stages.
.np
Modelling of the complete development of the instability.


.sh 1 "Effect of discretization on the growth factor"
.lp
The growth factor is determined from the calculated velocity at $t=0$:
.EQ (1)
v (t=0) ~~=~~ { partial w } over { partial t } | from t=0 ~~=~~ w sub 0 kappa
.EN
where $w(t)$ is the amplitude of the interface 
and $w sub 0$ the amplitude of the initial disturbance.
Results are presented for the markerchain method using linear and quadratic
elements and for the method of the deformable mesh as described in [3].
Equidistant meshes are employed and the relative errors in the growth
factor as a function of discretization is given in table I. The
discretization is given by the number of quadratic elements that are
or could be formed, i.e. $30 times 30$ is equivallent to $60 times 60$
linear elements. The quantity in brackets is the CPU-time needed
for the calculation (which consist mainly of building and solving
the system of algebraic equations) and it holds for the Gould PN9000.
For all cases $w sub 0~=~0.01$ and 250 markers were used.

.b "Table I    Relative error in growthfactor vs method and discretization"
.sz -1
.EQ
gsize -1
.EN
.TS
tab(#),center,box;
c || c s | c s | c s | c s | c s | c s 
l || n c | n c | n c | n c | n c | n c.
method#$10 times 10$#$16 times 16$#$20 times 20$#$24 times 24$#$25 times 25$#$30 times 30$
_

defmesh#0.074#(27.5)#-0.06#(170)#-0.07#(566)
linear#-0.26#(2.2)#-0.13#(7.8)#-0.11#(14.7)# # #-0.096#(29.5)#-0.08#(55)
quadratic#0.51#(2.2)#0.1#(9.2)#0.03#(21.4)#0.0002#(43)#0.31#(51)#-0.02#(103)
.TE
.sz +1
.EQ
gsize +1
.EN
There is a tendency for the numerical estimates of $kappa$ to be a little
on the low side (about 0.08 %), which is still significant. 
Note the difference in error made by calculations with the meshes 
$24 times 24$ and $25 times 25$ for quadratic elements. It will have
to do with the position of the marker in the element, but it is hard
to explain this satisfactory. For linear elements this increase in error
is not observed. 
Note that for "defmesh" the error is an order of magnitude smaller,
and the CPU-time more than an order of magnitude greater.


.bp
.sh 1 "Effect of time integration"
.lp
The chain of markers is advected by the velocityfield using following
formulae:
.EQ (2)
bold x sub 0 sup n+1 ~~=~~ bold x sup n ~+~ 
    DELTA t bold v sup n ( bold x sup n )
.EN
and
.EQ (3)
bold x sub i sup n+1 ~~=~~ bold x sup n ~+~ 
    { DELTA t } over 2 left [ ~bold v sup n ( bold x sup n ) ~+~
    bold v sup n+1 ( bold x sub i-1 sup n+1 ) ~right ] ~~~~~i=1, N sub cor
.EN

The markers were advected 5 steps in time using (2) and (3) with
$N sub cor$ corrector steps. The timestep was given by 10 % of the
value of the CFL-criterion. Again, $w sub 0~=~0.01$ and 250 markers
were distributed on the chain. $kappa$ was determined by a least
squares fit of $w(t)$ (see [3]); the relative errors are presented
in table II.

.b "Table IIa   Relative error in growth factor vs ncor and meshsize: linear elements"
.TS
box,tab(#),center;
c || c s | c s | c s | c s 
n || n c | n c | n c | n c.
$N sub cor$#$10 times 10$#$16 times 16$#$20 times 20$#$30 times 30$
_

1#-1.3#(2.6)#-0.8#(7.2)#-0.6#(12.6)#-0.36#(32)
2#-0.24#(4.3)#-0.09#(11.0)#-0.05#(17.5)#-0.04#(43)
3#-0.12#(5.7)#-0.02#(14.1)#-0.008#(24.2)#-0.02#(58)
.TE


.b "Table IIb   Relative error in growth factor vs ncor and meshsize: quadratic elements"
.TS
box,tab(#),center;
c || c s | c s | c s | c s 
n || n c | n c | n c | n c.
$N sub cor$#$10 times 10$#$16 times 16$#$20 times 20$#$30 times 30$
_

1#-1.1#(1.8)#-0.63#(4.7)#-0.47#(8.3)#-0.28#(24)
2#-0.04#(2.6)#0.07#(7.1)#0.06#(12.2)#0.04#(35)
3#0.08#(3.4)#0.13#(9.5)#0.11#(16.3)#0.06#(48)
.TE

The increase in accuracy with $N sub cor$ is well worth the small
increase in computertime. 
For linear elements the error decreases with both $N sub cor$ and
the number of elements. For quadratic elements a combined effect
of space discretization and time integration can be seen 
(for $N sub cor~>=~2$), 
but in the limit the error may well tend to zero.
This effect is further studied in section 4.
Note that for the same number of nodalpoints the use of quadratic
elements is more efficient. 
It takes longer to built the matrix (section 2),
but once this matrix is decomposed, it is faster in creating the
vectors and solving the system. 


.bp
.sh 1 "Modelling of initial and intermediate stages"
.lp
The timestep is limited by the value of the CFL-criterion 
($DELTA t sub cfl$), which is essentially
a upper bound of the timestep to ensure stability of explicit methods.
It can be argued, however, that the predictor-corrector method (2) and
(3) is in the limit $N sub cor ~- back 30 >~ inf$ an implicit, second
order Crank-Nicholson scheme. The time-step is then still limited
by the low order of the time integration and, more importantly, by
the limited accuracy of the velocity 
(in our case at most of order one).
It will be shown that this limits the value of the timestep well
below $DELTA t sub cfl$.
.br
In the initial stage the limiting factor of $DELTA t$ is the exponential
growth of the interface. It is therefore not surprising that other
workers have formulated extra conditions using the
growth factor. One example is ([4])
.EQ (4)
DELTA t ~~<~~ 0.5 over kappa
.EN
This condition is only useful in the initial stages of the development,
when the overall velocity still increases. When the overturn is
nearly complete it is not necessary to use condition (4) to limit
the timestep.
.br
For the model described in section one condition (4) implies 
$DELTA t~<~10$. 
The development of maximum amplitude of the interface $w(t)$ 
from $t~=~0$ to $t~=~60$ with 
$w sub 0~=~0.01$ for different time integration schemes is shown
in figure 1. Only quadratic elements were used and if the meshsize is
not explicitly stated, a mesh of $16 times 16$ elements was employed.
The solid line represents the analytical solution
.EQ (5)
w(t)~=~0.01 e sup {0.05313 t}
.EN
The dashed line is the numerical approximation which was obtained
by using $DELTA t~=~1$, $N sub cor~=~3$ and a mesh of $30 times 30$
quadratic elements. The size of this timestep is 10 % of condition
(4) and far below $DELTA t sub CFL$. This result is used as best
numerical estimate.

.lp
In figure 1.I three sets of datapoints are plotted for different
numbers of the correctorstep (3). 
The timestep is limited by (4) and $DELTA T sub cfl$. The latter
is smaller than (4) for $t~>~40$.
The iteration is sufficiently
converged for $N sub cor~=~3$. Increasing the number of steps to
4 shows no improvement. 
The numerical approximation to $w(t)$ shows to be too large for
this discretization, which will be a combined effect of both
time and space discretization, which are implicitly linked through
the velocity in the employed predictor-corrector scheme. 
Only increasing the number of elements is not sufficient. Figure 1.II
shows the approximation to $w(t)$ for a mesh with $30 times 30$
elements and $N sub cor~=~3$. The CPU-time is increased by a factor 10,
but the error is still large.
.br
The timestep is now further limited by demanding 
$DELTA t ~<~C DELTA t sub cfl$, where $C$ is chosen to be 0.5 and 0.25
(figure 1.III). It shows no considerable improvement, which is not
too surprisingly: the error is induced in the initial stages of the
development ($t~<~30$ for this model) and in this phase the timestep
is limited by condition (4).
.br
Figure 1.IV shows the result of the hardening of this condition by
imposing $DELTA t~<~C sub 2 / kappa$, where $C sub 2$ is 0.25 and 0.125.
The improvement is now clearly visible for $C sub 2~=~0.125$.
The prize to be paid is an increase in computertime by a factor of 4.

.bp
.sh 1 "Modelling of the complete development"
.lp
In figure 2 some snapshots of the development of the instability are
pictured, which were obtained with a mesh of $16 times 16$ quadratic
elements, $N sub cor~=~3$ and a timestep that was limited by both
$DELTA t~<~DELTA t sub cfl$ and $DELTA t~<~0.125 / kappa$.
The result is quite satisfying. The markers that are positioned at
the lateral boundaries should be driven into the corners
(which are stagnation points) and the following markers should
be driven against the upper and lower boundary resp. Due to the
not-so-high space accuracy this is not properly approximated. 
After the overturn is nearly completed ($t~>~500$) this incorrect
positions will lead to unphysical buoyancy forces, which will
destabilize the layering. The calculation therefore has to be
stopped at this stage.
.br
In figure 3 the same modelparameters are used for a mesh with
$32 times 32$ linear elements (i.e. the same number of nodalpoints)
The markerchain shows a considerable and unacceptable oscillation, 
probably due to the low accuracy of the velocity. The dynamics
are on the other hand very similar to the case with the quadratic
elements. The use of linear elements is less efficient than 
the employment of quadratic elements (section 3) and it is thus
not sensible to use linear elements for this approach.
.br
Figure 4 shows the development modelled with a mesh of $30 times 30$
quadratic elements. 

.sh 1 "Conclusions"
.lp
The markerchain method combined with the streamfunction-vorticity
formulation is a flexible and relatively cheap way to model 
isoviscous Rayleigh-Taylor instabilities with stress free boundaries.
The Sepran implementation requires more strict limitations on the
timestep than generally found in the literature, due to only first order
accuracy in the velocity. The time integration scheme is found to be
optimal if three iterations of the corrector step are performed and
the stepsize is limited by the CFL-criterion and 
(for the initial stages) $DELTA t ~<~ 0.125 / kappa$, where $kappa$
is the characteristic growth factor of the model.

.sh 1 "References"
.ip [1]
Van Keken, P. (November 20, 1989),
The markerchain method combined with the streamfunction-vorticity
formulation in Sepran: I The method.
.ip [2]
Van Keken, P. (November 20, 1989),
Derivation of growth factor for a RT instability with stress free boundaries
.ip [3]
Van Keken, P. (February 1989),
On the modelling of Rayleigh-Taylor instabilities (doctoraal scriptie)
.ip [4]
Schmeling, H. (1987),
On the relation between initial conditions and late stages of 
Rayleigh-Taylor instabilities.
.i Tectonophysics ,
.b 133 ,
65-80.
