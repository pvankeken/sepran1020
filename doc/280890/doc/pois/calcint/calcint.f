c *************************************************************
c *   CALCINT
c *
c *   Calculate integrals of the form
c *
c *     1 1    p  q      r      s       t       u
c *     / /   x  y  (1-x)  (1-y)  (1-2x)  (1-2y)  dx dy
c *     0 0
c *
c *
c *   PvK 220190
c **************************************************************
      program calcint
      implicit double precision(a-h,o-z)
      parameter(N=7,M=7)
      dimension mat(N,M)

1     continue
      read(5,*) ip,iq,ir,is,it,iu,fac
c     if (ip.eq.0.and.iq.eq.0.and.ir.eq.0) stop
      do 10 i=1,N
         do 10 j=1,M
10          mat(i,j)=0
      mat(1,1)=1
      call printmat(n,m,mat)

c     *** Multiply with x**p 
      do 20 k=1,ip
         do 30 j=1,M
            do 40 i=N,2,-1
40             mat(i,j) = mat(i-1,j)
30          mat(1,j) = 0
20    continue
      call printmat(n,m,mat)

c     *** Multiply with y**q
      do 50 k=1,iq
         do 60 i=1,N
            do 70 j=M,2,-1
70             mat(i,j) = mat(i,j-1)
60          mat(i,1) = 0
50    continue
      call printmat(n,m,mat)

c     *** Multiply with (1-x)**r
      do 80 k=1,ir
         do 90 j=1,M
            do 90 i=N,2,-1
90             mat(i,j) = mat(i,j)-mat(i-1,j)
80    continue
      call printmat(n,m,mat)
 
c     *** Multiply with (1-y)**s
      do 100 k=1,is
         do 110 i=1,N
            do 110 j=M,2,-1
110            mat(i,j) = mat(i,j)-mat(i,j-1)
100   continue
      call printmat(n,m,mat)

c     *** Multiply with (1-2x)**t
      do 120 k=1,it
         do 130 j=1,M
            do 130 i=N,2,-1
130            mat(i,j) = mat(i,j)-2*mat(i-1,j)
120   continue
      call printmat(n,m,mat)
        
c     *** Multiply with (1-2y)**u
      do 140 k=1,iu
         do 150 i=1,N
            do 150 j=M,2,-1
150            mat(i,j) = mat(i,j)-2*mat(i,j-1)
140   continue
      call printmat(n,m,mat)
c     *** Calculate integral
      wint = 0.
      do 200 i=1,N
         do 200 j=1,M
200         wint = wint + mat(i,j)*1d0/(i*j)
      wint = fac*wint

      print 1000,1,wint
      print 1000,12,wint*12
      print 1000,16,wint*16
      print 1000,20,wint*20
      print 1000,24,wint*24
      print 1000,30,wint*30
      print 1000,36,wint*36
      print 1000,48,wint*48
      print 1000,50,wint*50
1000  format(i2,': ',f10.7)
      goto 1

      end

      subroutine printmat(k,l,mat)
      character*1 cc
      dimension mat(k,l)
      return
      do 10 j=l,1,-1
10       print 100,(mat(i,j),i=1,k)
100   format(10(1x,i5),:)

      read(5,'(a)') cc
      return
      end
 

