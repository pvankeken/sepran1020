.po 15n
Temperature

Vorticity

Streamfunction

.EQ
Ra~=~10 sup 5
.EN

.EQ
Ra~=~10 sup 4
.EN

Figure 1. Results for benchmark models 1a and 1b using streamfunction-vorticity formulation.
.br
Mesh 32x32 linear elements, \fIf\fR = 0.85.
