.sc
.pn 2
.po 15n
.he 'Sepran''Streamfunction-vorticity'
.fo 'PvK 071189''%'
.EQ
delim $$
define arrow '{ = back 20 > }'
define q1 '{q sub 1}'
define q2 '{q sub 2}'
define vrms '{v sub rms}'
define cput '{cpu sub t}'
define cpu1 '{cpu sub 1}'
define cpu2 '{cpu sub 2}'
.EN
.sh 2 "The implementation in Sepran" 2 1
.lp
The elliptical equations (2)-(4) are part of the standard problem
package of Sepran and can readily be solved. Standard subroutine
DERIVA can be used to derive the temperature gradient and the
velocity components.
Main problem is the transport of information between the equations.

The equations are solved using the same element 
distribution ($arrow$ only one array KMESH is necessary).
Equation (2) and (3) are very similar. The large matrices which
are derived for these equations are equal and constant for each
iteration. This matrix has to be built only once and can be
stored in memory after decomposition. 
Furthermore, the matrix is positive definite.
The boundaryconditions are
equal as well and therefore the Sepran problem definition is
the same for these problems ($arrow$ only one array
KPROB is necessary). In each iteration only the right-hand-side
vector has to be built anew.
.br
The matrix which is derived from the heat equation has to be
built at each iteration, due to the changing convective terms.
The boundaryconditions are different from the first two problems
and therefore the problem definition is different ($arrow$ new
array KPROB). This creates the main problem: in general the
degrees of freedom are renumbered to put the prescribed dof's at
the end of a solution vector. This has to be taken into account
when copying a solutionvector from one problem to another.

Information on some coefficients in the equations are stored
as solutionvectors, which can be copied into user-arrays. These
are used in SYSTM0 to build the matrix and right-hand-side vector.

Appended is the Sepranprogram that is used to solve the equations
for the benchmark models. 
We will clarify some technicalities of the code in the next section.


.sh 2 "Program description: linear elements"
.lp
.u "Main program strvort"
.br
The algorithm from section 2 is implemented. Space for the transport
of information between the problems is allocated 
through array \fCuser\fR.
.br
The mesh is read from file. In general refinement in the boundary
layers is used to increase the numerical accuracy. In Sepran this
refinement is defined by rules of the form "each element is factor
$f$ smaller than the following" but this will lead to different
distributions of nodal points for linear and quadratic elements,
which inhibits direct comparison of numerical results. Meshes
with linear and quadratic elements using the same distribution of
nodal points can be made with program \fCmakemesh\fR (see elsewhere).
.br
The problem definitions are stored in 
arrays \fCkprob1\fR and \fCkprob2\fR. Boundary conditions are filled
and solution arrays are created. The iteration is done in the loop
defined by label 100, in which old solution vectors are stored,
the equations (3),(2) and (4) are solved (in \fCsolvor\fR, 
\fCsolstr\fR and \fCsoltmp\fR, respectively) and the convergence
of the iteration is checked.
Array \fCmatr\fR is declared in the main program because it is the
same matrix for both equation (3) and (2).

.u "Subroutine solvor"
.br
Equation (3) is solved for the vorticity. Only in the first call
of this subroutine the matrix is built (by \fCsystm0\fR) and decomposed
(by \fCsolve\fR). In each call the right-hand-side vector is built,
and the system of equations is solved. Information on the parameters
is stored in the user-arrays. Using the notation of Sepran's manual
of standard problems, section 3 (elliptical equations):
$alpha sub 11~=~1$,
$alpha sub 12~=~0$,
$alpha sub 22~=~1$,
$u~=~0$,
$v~=~0$,
$beta ~=~0$,
$f =~- Ra { partial T } over { partial x }$.
.br
$f$ can be derived in each nodalpoint by multiplication of the
Rayleigh-number and the derivative of $T$ (also given in the
nodalpoints) with respect to $x$. The derivative is calculated
by subroutine \fCderiva\fR, which yields for linear elements
an outputvector \fCigrad\fR, that has the same structure as the
solutionvector with the same number of degrees of freedom and the
prescribed degrees of freedom put at the
end of the vector. $f$ has to be specified nodalpointwise in
array \fCuser\fR and it is thus necessary to transform the
vector \fCigrad\fR before copying. This is done in subroutine
\fCpecopy\fR, which is here only an extension to the Sepran
routine \fCcopyuv\fR. It allows for multiplication of a constant
factor, which is in this case $- Ra$.
\fCkprob2\fR is used as actual parameter in the call of \fCpecopy\fR,
because \fCigrad\fR is renumbered according to this problem definition.

.u "Subroutine solstr"
.br
Equation (2) is solved to obtain the streamfunction. The matrix is
built and decomposed in routine \fCsolvor\fR and only the 
right-hand-side vector has to be built from the previously derived
vorticity.
The parameters are specified in the user-arrays,
\fCpecopy\fR is used to copy the solution, multiplied by $-1$,
into array \fCuser\fR and the system is solved.

.u "Subroutine soltmp"
.br
The convective-diffusion equation (4) is solved. The parameters
for this equation have to be specified in the sequence:
$alpha sub 11~=~1$,
$alpha sub 12~=~0$,
$alpha sub 22~=~1$,
$u~=~partial psi / partial y$,
$v~=~- partial psi / partial x$,
$beta~=~f~=~0$.
.br
The velocity components $u$ and $v$ can be derived from the 
streamfunction, which is given in the nodalpoints.

.u "Subroutine peoutp"
.br
Some post-processing is performed. Plots of the solution vectors
are made and numerical values of the Nusselt number $Nu$, the
local temperature gradient in the corners of the box $q sub 1$ and
$q sub 2$ and the root-mean-square velocity $v sub rms$ are
calculated. First the temperature gradient is calculated from
\fCitemp\fR using \fCderiva\fR. The Nusselt number is given
by the boundary integral 
.EQ (5)
int from {C sub 1}~grad T cdot bold n~dC
.EN
where $C$ is the boundary, $C sub 1$ the upper boundary and
$bold n$ the outward pointing normal on this curve.
.br
The root-mean-square velocity is given by 
.EQ (6)
v sub rms ~~=~~ int from S ~(~u sup 2 ~+~ v sup 2~)~dS
.EN
where $S$ is the surface on which the equations are solved.

.sh 2 "Results: linear elements"
.lp
Results are shown for the benchmark models 1a and 1b 
($Ra~=~10 sup 4$ and $Ra~=~10 sup 5$). These results are meant
for comparison with results obtained by numerical schemes using
the penalty function approach (quadratic elements) and linear
of quadratic elements for the heat equation ([3],[4]).
.br
Meshes with 2x20x20 and 2x32x32 linear triangles with different
amounts of refinement in the boundarylayers are employed. 
The amount of refinement is defined by a factor $f$, which expresses
the ratio of the length of each element with the length of the
preceding one. The given values of $f$ correspond to this definition
for quadratic elements, so for linear elements the size of each pair of
elements is $f$ times the preceding pair.
CPU-times hold for the Gould PN9000.


.b "Model 1a"
$Ra~=~10 sup 4$.

Mesh 20x20, $cput~=~45$, $cpu1$ = 5.7, $cpu2$ = 4.8, 
niter = 9, $delta~=~10 sup -3$.
.TS
center,tab(#),box;
l | c c c
l | n n n.
$f$#0.85#0.75#0.70
_
$Nu$#4.85591#4.89920#4.92065
$q1$#7.89786#8.02329#8.07902
$q2$#0.55409#0.56017#0.57435
$vrms$#43.48773#43.75665#43.95578
$psi$#18.13835#18.23323#18.30467
.TE
.bp
Mesh 32x32, $cput~=~174$, $cpu1$ = 23, $cpu2$ = 18, 
niter = 9, $delta~=~10 sup -3$.
.TS
center,tab(#),box;
l | c c c
l | n n n.
$f$#0.85#0.75#0.70
_
$Nu$#4.88867#4.91355#4.92760
$q1$#8.03918#8.09996#8.13040
$q2$#0.58032#0.58512#0.58610
$vrms$#43.18668#43.46425#43.67860
$psi$#18.00944#18.11430#18.19483
.TE


.b "Model 1b"
$Ra~=~10 sup 5$.

Mesh 20x20, $cput~=~50$, $cpu1$ = 23, $cpu2$ = 18, 
niter = 10, $delta~=~10 sup -3$.
.TS
center,tab(#),box;
l | c c c
l | n n n.
$f$#0.85#0.75#0.70
_
$Nu$#10.03000#10.34963#10.48535
$q1$#16.43849#17.76984#18.33753
$q2$#0.46201#0.58463#0.62835
$vrms$#198.52114#199.79426#200.68062
$psi$#77.65905#78.02215#78.27223
.TE

Mesh 32x32, $cput~=~210$, $cpu1$ = 23, $cpu2$ = 18, 
niter = 11, $delta~=~10 sup -3$.
.TS
center,tab(#),box;
l | c c c
l | n n n.
$f$#0.85#0.75#0.70
_
$Nu$#10.45760#10.60144#10.66011
$q1$#18.50087#19.07753#19.27266
$q2$#0.66560#0.7031#0.71068
$vrms$#197.08168#195.77152#198.16143
$psi$#77.10#76.69395#77.43847
.TE



Plots of the solutions at these Rayleigh numbers are given in
figure 1.
Note that meshes with strong refinements in the boundary layers 
yield less accurate solutions. This is probably a consequence of
the behaviour of the vorticity, which has pronounced local
maxima near the center of the box. The derived root-mean-square
velocity is not very accurate. The streamfunction is approximated
by a piecewise linear function which gives constant values for
the velocity in the elements. Subroutine DERIVA however, calculates
the velocity in one nodalpoint, by averaging the values in the
elements, which may introduce a serious error.
Compared with the penalty function approach, this method is
cheaper.

Conclusions:
.np
The implementation of the streamfunction-vorticity formulation
with linear elements is a little more efficient but less
accurate than the penalty function method.
.np
Strong grid refinements should be avoided,
which places a strong condition on the number of elements, especially
for higher Rayleigh numbers.
.lp

