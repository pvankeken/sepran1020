.sc
.he '09-89''predictor-corrector'
.fo ''%''
.po 15n
.EQ
delim $$
define Ton1 '{ bold T sub 0 sup n+1 }'
define Ton  '{ bold T sub 0 sup n }'
define dt   '{ DELTA t }'
define Sn   '{ bold S ( bold v sup n ) }'
define Tn   '{ bold T sup n }'
define Sn1  '{ bold S ( bold v sub 0 sup n+1 ) }'
define von1 '{ bold v sub o sup n+1 }'
define vin1 '{ bold v sub i sup n+1 }'
define Tin1 '{ bold T sub i sup n+1 }'
define vn   '{ bold v sup n }'
.EN
.sz +3
.ce
.u "Predictor-corrector scheme for convection models"
.sp 2
.ce
Peter van Keken     September 1, 1989
.sz -3
.sp 2
.sh 1 "Introduction"
.lp
The implementation of a second order predictor-corrector scheme in
SEPRAN is presented. This method is used for the time-integration
of the heat-equation that describes the evolution of convection models.
After space discretization a system of ordinary differential equations
is obtained
.EQ (1)
bold M bold T dot ~+~  bold S bold T ~~=~~bold f
.EN
where $bold T$ is the temperature in the nodalpoints, $bold M$ the
mass matrix (which is constant in time) and $bold S$ the stiffness 
matrix, which depends on time through the convection term. As a
consequence, the system in non-linear.
.br
The set of equations (1) is solved by a predictor-corrector scheme
following [1],[2].
By an (quasi) implicit step  the predictor $Ton1$ is obtained
.EQ (2)
bold M { Ton1 ~-~ Tn } over dt ~+~ Sn Ton1~~=~~ bold f
.EN
The velocity $von1$ is calculated using $Ton1$ and this is used
to obtain a corrector by
.EQ (3)
bold M { Tin1 ~-~ Tn } over dt ~+~ half Sn1 Tin1
~+~ half Sn Tn ~~=~~bold f ~~~~~~i~=~1
.EN
The velocity $vin1$ can be obtained from $Tin1$. The corrector step
may be repeated ($i=2,3,...$) until convergence.

.sh 1 "Implicit Euler"
.lp
Equation (1) can be rewritten
.EQ (4)
[~bold M ~+~ dt Sn~] Ton1 ~~=~~ dt bold f ~+~ bold M Tn
.EN
Incoorporating the prescribed degrees of freedom $bold T sub p$
leads to
.EQ (5)
[~bold M sub ff ~+~ dt bold S sub ff ( bold v sup n )~] 
    bold T sub of sup n+1 ~~=~~ dt bold f ~+~
    bold M sub ff bold T sub f sup n ~-~
    [~bold M sub fp ~+~ dt bold S sub fp ( bold v sup n ) ~] 
    bold T sub op sup n+1
.EN
An implementation of this algorithm is given in the listing of
subroutine \fCieuler\fR.

.sh 1 "Crank-Nicholson"
.lp
Equation (3) can be rewritten as
.EQ (6)
define dt2 '{ size -2 { dt over 2 }}'
[~bold M ~+~ dt2  Sn1~] Tin1 ~~=~~ dt bold f ~+~ bold T sup n
  ~-~ dt2 Sn bold T sup n
.EN
which leads to the system of algebraic equations
.EQ (7)
[~bold M sub ff ~+~ dt2 bold S sub ff ( bold v sub o sup n+1 )~]
  bold T sub {i f} sup n+1 ~~=~~ dt bold f ~+~ 
  bold M sub ff bold T sub f sup n ~+~ bold M sub fp  bold T sub p sup n
.EN
.EQ 
  ~-~ dt2 bold S sub ff ( bold v sup n ) bold T sub f sup n
  ~-~ dt2 bold S sub fp ( bold v sup n ) bold T sub p sup n
  ~-~ [~bold M sub fp ~+~ dt2 bold S sub fp ( bold v sub o sup n+1 ) ~]
  bold T sub ip sup n+1
.EN
This is implemented in subroutine \fCcorrcn\fR.

.bp
.(l
\fC
.sz -2
c *****************************************************************
c *   IEULER
c *
c *   The temperature equation is solved using implicit euler
c *   Information concerning the coefficients of the equation
c *   are stored in (i)user and ivcold.
c * 
c *
c *   Pvk  160589/290889
c ****************************************************************
      subroutine ieuler(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  numold,ivcold,matrs,matrm,irhsd)
      implicit double precision(a-h,o-z)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      
      dimension kmesh(*),kprob(*),intmat(*),isol(*),user(*),iuser(*)
      dimension matrs(*),matrm(*),ivcold(5,numold),irhsd(*)
      dimension islold(*),ivec1(5),ivec2(5),ivec3(5),matr1(5)

      save ifirst
      data ifirst/0/

      call copyvc(isol,islold)
c     *** Built matrices and rhs vector at time t
      call pechtype(2,1,kprob)
      call systm2(11,0,2,matrs,intmat,kmesh,kprob,irhsd,matrdum,isol,
     v            iuser,user,numold,ivcold,ielhlp)
      if (ifirst.eq.0) then
         ifirst=1
         call pechtype(2,2,kprob)
         call systm2(14,0,2,matrm,intmat,kmesh,kprob,mrhsd,matrdum,isol,
     v               iuser,user,numold,ivcold,ielhlp)
      endif


      t = t+tstep
c     *** Built the system of equations
     
c     *** M*uold
      call maver(matrm,islold,ivec1,intmat,kprob,5)

c     *** tstep*f + M*uold
      call algebr(3,0,ivec1,irhsd,ivec3,kmesh,kprob,1d0,tstep,p,q,ip)

c     *** M + tstep*S
      call copymt(matrs,matr1,kprob)
      call addmat(kprob,matr1,matrm,intmat,tstep,alpha2,1d0,beta2)

c     *** ( M + tstep*S ) * usol(p)
      call maver(matr1,isol,ivec2,intmat,kprob,6)

c     *** tstep*f + M*uold - (M+tstep*S) * usol(p)
      call algebr(3,0,ivec3,ivec2,ivec1,kmesh,kprob,1d0,-1d0,p,q,ip)

c     *** Solve the system
      call solve(0,matr1,isol,ivec1,intmat,kprob)

      return
      end

c *************************************************************
.sz +2
\fR
.)l

.bp
.(l
\fC
.sz -2
c *************************************************************
c *   CORRCN
c *
c *   Solves    .
c *           M T  + S T  = f
c *   using a CN scheme. The system is non-linear, S depends on time.
c *************************************************************
c *   Parameters:
c *      kmesh     i   Standard Sepran array
c *      kprob     i   Standard Sepran array
c *      intmat    i   Information of the large matrix for this problem
c *      isol      o   Solution vector T(n+1)
c *      islold    i   Old solution vector T(n): output of predictorstep
c *      (i)user   i   Contains information on coefficients
c *      numold    i   number of solutionvectors in ivcold
c *      ivcold    i   'old solution array' used to transport 
c *                    information between problems
c *      matrs     i   Stiffness matrix S(n)
c *      matrm     i   Mass matrix (assumed to be constant in time)
c *      irhsd     i   Righthandside vector f(n)
c * 
c *   290889 PvK
c *************************************************************
      subroutine corrcn(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  numold,ivcold,matrs,matrm,irhsd)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),isol(*),islold(*),user(*)
      dimension iuser(*),ivcold(5,numold),matrs(*),matrm(*),irhsd(*)
      dimension matrs1(5),irhs1(5),ivec1(5),ivec2(5),ivec3(5)

      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)

      t2step = tstep*0.5d0
c     *** Built S and f at time t(n+1) using predicted velocity
      call pechtype(2,1,kprob)
      call systm2(11,0,2,matrs1,intmat,kmesh,kprob,irhs1,matrdum,isol,
     v            iuser,user,numold,ivcold,ielhlp)

c     *** If irhsd is time-dependent  do something with f(n)
c     *** irhsd = dt/2*(irhsd+irhs1)
c     call algebr(3,0,irhsd,irhs1,irhsd,kmesh,kprob,t2step,t2step,p,q,i)
     
c     *** ivec1 = M*uold  add it to irhsd*dt
      call maver(matrm,islold,ivec1,intmat,kprob,5)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,tstep,1d0,p,q,i)

c     *** S1 = M + dt/2*S1
      call addmat(kprob,matrs1,matrm,intmat,t2step,alpha2,1d0,beta2)

c     *** ivec1 = S1*usol(p) ; add it to irshd
      call maver(matrs1,isol,ivec1,intmat,kprob,6)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,-1d0,p,q,i)

c     *** ivec1 = S*uold ; add it to irhsd
      call maver(matrs,islold,ivec1,intmat,kprob,5)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,-t2step,p,q,i)

c     *** solve the system
      call solve(0,matrs1,isol,irhsd,intmat,kprob)

         
      return
      end
.sz +2
\fR
.)l

.bp
.sh 1 References
.lp
.ip [1]
Hansen U. and A. Ebel. 
Time-dependent thermal convection - a possible explanation for a 
multiscale flow in the Earth's mantle.
.i "Geophysical Journal",
1988,
.b 94 ,
181-191.

.ip [2]
Christensen U.
Convection with pressure- and temperature-dependent non-Newtonian rheology.
.i "Geophys.J.R.astr.Soc",
1984,
.b 77 ,
343-384.



