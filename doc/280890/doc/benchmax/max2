.sc
.po 15n
.he 'Benchmark''case 2'
.fo 'PvK 080390''%'
.EQ
delim $$
.EN
.sz +3
.ce
.u "Benchmark case 2: temperatuur en diepte afhankelijke viscositeit"

.ce
Peter van Keken, 8 maart 1990.
.sz -3




Geval 2 van de benchmark vergelijking\**
.(f
\**
A benchmark comparison for mantle convection codes,
Blankenbach et al., \fIGeophys.J.Int (1989)\fR \fB98\fR, 23-28
.)f
is doorgerekend met Sepran.

.sh 1 "Model en implementatie in Sepran"
.lp
Stationaire convectie is gemodelleerd in een rechthoekige geometrie.
De randen zijn spanningsvrij; de temperatuur aan de onderkant is
constant 1, aan de bovenkant constant 0 en de zijkanten zijn
geisoleerd.  De viscositeit varieert volgens
.EQ
nu~=~ exp ~ [ ~- bT~+~ c (1-z)~]
.EN
Hierbij ligt de onderkant van de doos op $z=0$, de bovenkant op $z=1$.
.br
We onderscheiden twee gevallen:
.br
Geval 2a: aspect ratio 1, $Ra sub 0~=~10 sup 4$, $b~=~ln (1000)$, $c~=0$.
.br
Geval 2b: aspect ratio 2.5, $Ra sub 0~=~10 sup 4$, $b~=~ln (16384)$,
$c~=~ln (64)$.

Programma \fCconvh\fR (zie\**) is gebruikt voor het oplossen van
.(f
\**
Het oplossen van gekoppelde problemen op verschillende meshes in Sepran,
Peter van Keken, 7 maart 1990.
.)f
de stationaire vergelijkingen. De Stokes vergelijking wordt opgelost
met de penalty functie methode op een mesh van kwadratische driehoeken.
De warmtevergelijking wordt opgelost een mesh van lineaire driehoeken,
waarbij dezelfde knooppuntenverdeling als bij de Stokes vergelijking
wordt gebruikt. De berekening van de viscositeit benodigt enige
technische toelichting: de viscositeit is als functie van temperatuur
en diepte per knooppunt gegeven en wordt via de user-arrays 
doorgegeven. Voor de berekening is dan de temperatuur en de diepte
nodig, welke uit de oplossingsvector van de warmtevergelijking,
resp. de mesh definitie kunnen worden opgevist. De implementatie
hiervan in Sepran wordt verzorgd door subroutine \fCpecof\fR,
waarvan de sourcelisting is bijgevoegd.
.br
In deze gevallen met variabele viscositeit is het noodzakelijk
onderrelaxatie te gebruiken om de iteratie te laten convergeren.
Voor geval 2a is een relaxatie parameter $OMEGA~=~0.5$ gebruikt,
voor geval 2b was het noodzakelijk een sterkere demping bij de
eerste 7 iteraties toe passen ($OMEGA~=~0.3$). 
.br
In het algemeen werd de iteratie beeindigd als het maximale verschil
tussen twee opeenvolgende oplossing voor de temperatuur beneden
de grens $delta~=~10 sup -4$ was gekomen.
.br
De warmtevergelijking wordt opgelost met standaard (Bubnov-)Galerkin
weging (Ke1) en met gebruik van upwind gecorrigeerde weegfuncties (Ke2).
De Il'in variatie van de "Brookes and Hughes type" upwinding is gebruikt
(zie Sepran manual Standard Problems).


.sh 1 "Vergelijking met andere codes"
.lp
De resultaten worden getabuleerd naast de uitkomsten van de codes
van Christensen (Ch) en Hansen (Ha). 
Christensen gebruikt bicubic splines
voor de stroomfunctie en biquadratic splines voor de temperatuur
met upwind correctie. De gegeven CPU-tijden gelden voor de
Cray XMP in Munchen. Hansen gebruikt een niet conform vierde
orde element (met vier knooppunten) voor de stroomfunctie en
bilinear element (met de Heinrich-upwinding) voor de temperatuur.
De CPU-tijden gelden voor een Cyber 205.
De berekeningen met Sepran zijn gedaan op de Multimax 510 in de
configuratie van maart 1990. Uit tests gedaan met Sepran op de
Cyber 205 van SARA in Amsterdam is de verhouding van rekensnelheid
tussen de 205 en de MM510 ongeveer 20.
Verschillende gradaties van verfijningen in de grenslagen zijn gebruikt.
De meshes zijn gemaakt met programma \fCmakemesh\fR (zie referentie 2),
waarbij voor de factoren \fCfx\fR en \fCfy\fR de volgende waarden
zijn gebruikt:

.TS
tab(#),center;
c c c
l n n .
 #\fCfx\fR#\fCfy\fR

a#0.7#0.7
b#0.85#0.85
c#0.775#0.775
d#1#0.7
e#0.85#0.7
f#0.9#0.8
.TE




.sh 1 "Geval 2a: resultaten"


.sz -1
.TS
tab(#),center;
c c c c c c c c
l l n n n n n n n.
Code#Grid#Nu#$v sub rms$#$q sub 1$#$q sub 2$#$q sub 3$#$q sub 4$#CPU

Ch#18x18 #10.06681 #480.7714 #17.53909#1.015290#26.79242#0.497214#2.94
  #24x24 #10.06738 #480.3088 #17.52025#1.008742#26.82805#0.497334#5.82
  #36x36 #10.065863#480.38728#17.52924#1.008509#26.81372#0.497356#19.4
  #48x48 #10.065907#480.41558#17.53059#1.008513#26.81125#0.497373#47.4
  #72x72 #10.065949#480.42971#17.53116#1.008511#26.80979#0.497379#190
  #ext.  #10.065995#480.43342#17.53136#1.008509#26.80846#0.497380#

Ha#24x24 # 9.93096 #478.232  #17.8744 #1.01904 #25.5240 #0.487654#8.45
  #48x48 #10.0379  #480.620  #17.6282 #1.01095 #26.5374 #0.494881#40.9
  #72x72 #10.0543  #480.622  #17.5759 #1.00956 #26.6941 #0.496243#133
  #ext   #10.0667  #480.524  #17.5325 #1.00847 #26.8124 #0.49733 #

Ke1#20x20a#10.065518#466.06792#17.26420#1.00703#26.681070#0.503348#1235
   #20x20b# 9.941131#471.2219 #17.15707#1.00912#25.655017#0.469283#1272
   #20x20c# 9.641577#472.6910 #16.75444#1.01283#23.406489#0.386856#1246

   #24x24a#10.092118#467.25931#17.33904#1.00673#26.924512#0.509106#2294
   #24x24b#10.034097#472.87264#17.33206#1.00804#26.473209#0.492519#2305
   #24x24c# 9.865760#475.07544#17.12069#1.01036#25.079479#0.448515#2277

   #28x28a#10.098508#467.81814#17.36609#1.00661#26.984672#0.510116#3819
   #28x28b#10.065808#473.70696#17.40203#1.00763#26.753667#0.499993#3830
   #28x28c# 9.971596#476.31778#17.30189#1.00918#25.969233#0.475435#3814

   #30x30a#10.082655#472.36403#17.41094#1.00732#26.888432#0.504587#5358
   #30x30c#10.001877#476.7054 #17.35523#1.00881#26.231669#0.482857#5000

   #34x34a#10.100669#468.16450#17.37989#1.00663#27.007119#0.510216#7719
   #34x34b#10.079698#474.29229#17.44073#1.00742#26.874370#0.502894#7703

   #40x40a#10.088069#472.85272#17.43407#1.00721#26.936189#0.505268#15496


Ke2#20x20a#10.125992#476.3072 #17.78429#1.01536#27.128160#0.531892#1330
   #20x20b# 9.975399#479.39722#17.53320#1.01727#25.959848#0.507737#1333
   #20x20c# 9.629473#480.26488#17.00783#1.02492#23.342131#0.447309#1330

   #24x24a#10.156506#476.55952#17.83940#1.01296#27.379649#0.529759#2387
   #24x24b#10.078572#479.91087#17.68972#1.01236#26.825170#0.514564#2379
   #24x24c# 9.883650#480.87349#17.36009#1.01647#25.252600#0.485236#2268

   #28x28a#10.163580#476.5344 #17.85339#1.01223#27.438417#0.527931#3921
   #28x28b#10.112458#480.0894 #17.74481#1.01027#27.111827#0.515592#3744
   #28x28c# 9.998737#481.19586#17.52724#1.01239#26.202592#0.497686#3787

   #34x34a#10.165525#476.36033#17.85590#1.01209#27.454382#0.526563#
   #34x34b#10.126270#480.0712 #17.76710#1.00928#27.229248#0.514899#7780

.TE
.sz +1


.bp
.sh 1 "Geval 2b: resultaten"


.sz -1
.TS
tab(#),center;
c c c c c c c c c
l l n n n n n n n.
Code#Grid#Nu#$v sub rms$#$q sub 1$#$q sub 2$#$q sub 3$#$q sub 4$#CPU

Ch#36x18 #6.958129#172.9798 #        #        #        #        #5.0
  #48x24 #6.933153#171.89687#18.50291#0.177529#14.16516#0.617438#10.7
  #72x36 #6.930056#171.76533#18.48383#0.177462#14.17037#0.617670#37.5
  #96x48 #6.929978#171.76540#18.48420#0.177443#14.17015#0.617689#91
  #ext   #6.929913#171.76540#18.48774#0.177426#14.17005#0.617700

Ha#32x32 #6.861873#167.718  #18.7519 #0.173788#14.0444 #0.606305#54
  #48x48 #6.89925 #169.951  #18.6016 #0.175805#14.11423#0.612699#195
  #72x72 #6.91611 #170.950  #18.5360 #0.176701#14.1444 #0.615494#480
  #ext   #6.92970 #171.753  #18.4842 #0.177417#14.1682 #0.617708#

Ke1#26x20d#6.851376#168.17092#18.65930#0.169178#14.14096#0.236337#1992
   #36x18f#6.862503#170.19690#17.35493#0.183883#13.89489#0.600522#2771
   #36x22e#6.886040#169.3610 #18.2909 #0.181476#14.06251#0.623086#3720
   #36x30e#6.890272#169.5503 #18.37289#0.180950#14.09767#0.621256#7500
   #48x24f#6.898718#170.72226#18.12007#0.180852#14.07861#0.617135#7614
   #42x36f#6.910219#170.83277#18.35231#0.178910#14.14158#0.610294#13525
   #46x36f#6.911685#170.91440#18.38120#0.179136#14.14349#0.614039#18022

Ke2#26x20f#6.892632#172.84040#17.82238#0.181992#14.09624#0.548757#1796
   #36x18f#6.874164#172.17361#17.62931#0.186626#13.95092#0.620068#2775
   #36x30f#6.904974#172.38943#18.86677#0.183598#14.16091#0.631784#8576
   #48x24f#6.907742#172.22181#18.38478#0.182326#14.11049#0.626564#7769
   #48x36f#6.920749#172.37121#18.64870#0.180412#14.16739#0.620773#14355
.TE
.sz +1
