.po 15n
.sc
.fo ''%''
.he '''sepran'
.EQ
delim $$
define bT '{bold T}'
define bM '{bold M}'
define bS '{bold S}'
define bf '{bold f}'
define Mn '{bM sup n}'
define Sn '{bS sup n}'
define fn '{bf sup n}'
define dt '{DELTA t}'
define Tf '{bT sub f}'
define Tp '{bT sub p}'
define Tn '{bT sup n}'
define Tn1 '{bT sup n+1}'
define Mff '{bM sub ff}'
define Mfp '{bM sub fp}'
define Mpf '{bM sub pf}'
define Mpp '{bM sub pp}'
define Tfn '{bT sub f sup n}'
define Tpn '{bT sub p sup n}'
define Tfn1 '{bT sub f sup n+1}'
define Tpn1 '{bT sub p sup n+1}'
define Sff '{bS sub ff}'
define Sfp '{bS sub fp}'
.EN
.sz +3
.ce
.u "Time stepping algorithms using Sepran"
.sz -3
.sp 2
.lp
Application of the FEM to time dependent equations leads in general
to a system of differential equations of the form
.EQ (1)
bM cdot bT dot ~~+~~ bS cdot bT ~~~=~~~ bf
.EN
where $bM$ is the mass matrix, $bS$ the stiffness matrix and $bT$
contains the unknown degrees of freedom. $bT dot$ denotes the
time derivative of $bT$.
This equation can be solved in time by standard time integration
techniques.

.sh 1 "Implicit Euler"
.lp
The solution to (1) is in this case approximated by
.EQ (2)
Mn cdot { Tn1 ~-~Tn } over dt ~~+~~ Sn cdot Tn1 ~~~=~~~ fn
.EN
where $n$ denotes the known vectors and matrices at time $t$ and
$n+1$ the unknown vectors at time $t~+~dt$.
.sp
The vectors contain prescribed degrees of freedom: 
$bT~=~left ( pile { Tf above Tp } right )$.
.sp
Equation (2) is solved for $Tf$ only. The matrices can be
partitioned:
.EQ
bM ~~=~~ left ( matrix { 
                  ccol { Mff above Mpf }
                  ccol { Mfp above Mpp }
                } right )   ~~~~~~~~ roman { etc. }
.EN
and (2) can be rewritten as
.EQ (3)
Mff Tfn1 ~+~ Mfp Tpn1 ~-~ Mff Tfn ~-~ Mfp Tpn ~+~dt Sff Tfn1 
 ~+~ dt Sfp Tpn1 ~~=~~ dt fn
.EN
or
.EQ (4)
(~Mff ~+~ dt Sff ~) Tfn1 ~~=~~dt fn ~-~ dt Sfp Tpn1 ~-~
   Mfp Tpn1 ~+~ Mfp Tpn ~+~ Mff Tfn
.EN
If the prescribed degrees of freedom are time independent we find
.EQ (5)
(~Mff ~+~ dt Sff ~) Tfn1 ~~=~~ dt fn ~-~ dt Sfp Tpn1 ~+~ Mff Tfn
.EN


.ip "Note:"
In Sepran MAVER can be used to do matrix-vector multiplications.
ADDMAT can be used to create linear combinations of matrics;
ALGEBR (a.o.) for linear combinations of vectors.
