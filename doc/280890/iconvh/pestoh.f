c *************************************************************
c *   PESTOH
c *
c *   Solves the Stokes equation
c *************************************************************
      subroutine pestoh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      implicit double precision(a-h,o-z)
      dimension matrs(*),intmat(*),kmesh(*),kprob(*),isol(*)
      dimension iuser(*),user(*),irhsd(*),matrback(5),icontr(6)
      common /peparam/ rayleigh,rlam,q,visc0,b,c
      real t0,t1
      common /pecgsol/ cgeps,ncgmax,iprec,icgprint

      save ifirst
      data ifirst/0/

      if (visc0.gt.0.or.ifirst.eq.0) then
         call systm0(1,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         call copymt(matrs,matrback,kprob)
      else
         call systm0(2,matrback,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
      endif

      icontr(1) = 0
      icontr(2) = 1
      icontr(3) = 1
      icontr(4) = icgprint
      icontr(5) = iprec
      call second(t0)
      call congrd(1,icontr,matrs,isol,irhsd,intmat,kprob,cgeps,ncgmax)
      call second(t1)
      write(6,*) 'congrd: ',t1-t0
c     call solve(1,matrs,isol,irhsd,intmat,kprob)

      ifirst=1

      return
      end
      
