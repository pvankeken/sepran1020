c *************************************************************
c *   PEIN56
c *
c *   User routine to delete arrays with index INDEX with respect
c *   to INFOR from the datastructure (ICHOIS=0) 
c *   or to write the arrays to the scratch file (ICHOIS=-1).
c *   This routine should be used in conjunction with PEINI3
c *   where the actual removal/writing is performed.
c *   
c *   PvK 13-2-90
c ************************************************************* 
      subroutine pein56(index,ichois)
      common /carray/ iinfor,infor(3,1500)
      if (ichois.eq.-1.and.index.gt.0) infor(3,index) = -100
      if (ichois.eq.0) call ini056(index,'pein56')
      return
      end
