c *************************************************************
c *   POSTMAR
c *  
c *   Plots solutionvectors in Postscript
c *
c *   PvK 030890
c *************************************************************
      program postmar
      implicit double precision(a-h,o-z)
      parameter(NCMAX=2,NMARMAX=4000,NPMAX=10000)
      parameter(NUM1=NMARMAX+10,NUM2=2*NPMAX+10,NUM3=NCMAX*NMARMAX)
c     *** NCMAX = Maximum number of markerchains
c     *** NMARMAX = Maximum number of markers on one chain
c     *** NPMAX   = Maximum number of nodalpoints
c     *** NUM1    = length of array iuser   = 10 + NMARMAX
c     *** NUM2    = length of array  user   = 10 + 2*NPMAX
c     *** NUM3    = total number of markers = NCMAX*NMARMAX
      dimension kmesh1(100),kprob1(100),isol1(5)
      dimension kmesh2(100),kprob2(100),isol2(5)
      dimension icurvs(2),funcx(100),funcy(100),igradt(5)
      dimension iuser(NUM1),user(NUM2)
      dimension coormark(2*NUM3)
      dimension y0(10),dm(10),rk(10),h(10),igradt(5)
      dimension toutput(100),ainit(10),wavel(10),rho(10)
      dimension xmc(NUM3),ymc(NUM3),x0(10),y0(10)

      common /bmint/ ityps,itypt,nruls,nrult
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /pelinqua/ itypel,ishape
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /bstore/ f2name
      character*80 f2name
      common /colmar/ icol(10)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtout,dtcfl,tfac,tstepmax,tvalid,difcor,
     v                difcormax,nitermax,nout,ncor
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn,plotmarkers
      common /peparam/ rayleigh,rl,visc0,b,c,q
      character*80 rname,fimage
      character*80 fnre,fnt,fmar,fsol
      character*80 modtext
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /devlib/ device
      character*80 device
      real sx,sy

      data iuser(1),user(1),kmesh1(1),kprob1(1)/NUM1,NUM2,2*100/
      data kmesh2(1),kprob2(1)/2*100/



      read(5,*) modtext
      read(5,*) ityps,itypt,nruls,nrult
      read(5,*) ram,rat,ism,ist,ibm,ibt
      read(5,*) iqchois,q
      read(5,*) nitermax,dtout,nout,ncor
      read(5,*) tfac,tmax,trestart,tstepmax,tvalid
      read(5,*) nochain
      read(5,*) (y0(i),wavel(i),ainit(i),dm(i),i=1,nochain)
      read(5,*) itypv,ctd,cpd
      read(5,*) (rho(i),i=1,nochain+1)
      read(5,*) (viscl(i),i=1,nochain+1)
      read(5,*) irestart,fnre,fnt
      read(5,*) nxpix,nypix,mooi,istart,iend,istride
      read(5,*) (icol(i),i=1,nochain+1)
      read(5,*) imet,cgeps

c     *** standard Sepran start
      call start(0,1,0,0)
      call permesh(kmesh1,kmesh2)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)

      iway=2
      if (device.eq.'postscript') then
         fname='POST'
      else if (device.eq.'tektronix') then
         fname='TEK'
      endif
      yfaccn = 1
      jkader=-4
      chheight = 0.3d0

c     *** Determine aspect ratio
      ishape = 1
      call pefilxy(ishape,kmesh1,kprob1,isol1)
      rlam = xcmax-xcmin
      rl   = rlam

c     *** Determine number of solutions NSOL in one window
c     *** If rlam>5 then NSOL = 1 else
c     *** 3 frames + text + space have to fit in the y-direction (18 cm)
c     *** so  format/rlam = 4.8 and  y0=0 y1=6 y2=12
c     *** Let's put  5 <= format <= 10
c     *** Then we have (with paperlength 24 cm)
c     ***
c     ***   NSOL     FORMAT   X-origin:
c     ***    2         10        0   12
c     ***    3          7        0    8   16
c     ***    4          5        0    6   12   18
      teksc = 1d0
      if (device.eq.'tektronix') teksc=0.75
      form = 5*rlam
      print *,'form, rlam = ',form,rlam
      if (device.eq.'tektronix') then
         dy = 6d0*teksc + 3*chheight
      else
         dy = 6d0*teksc
      endif
      nsol = 4
      dx = 6d0*teksc
      if (form.ge.5d0.and.form.lt.7d0) then
         nsol = 4
         format = 5d0*teksc
         dx = 6d0*teksc
      else if (form.ge.7d0.and.form.lt.10d0) then
         nsol = 3
         format = 7d0*teksc
         dx = 8d0*teksc
      else if (form.ge.10d0.and.form.lt.20d0) then
         nsol = 2
         format = 10d0*teksc
         dx = 12d0*teksc
      else
         nsol = 2
         format = 20d0*teksc
         dx = 0
      endif
      write(6,*) 'NSOL, FORMAT: ',nsol,format

c     **** Open bsfiles
      if (irestart.eq.-1) then
         fmar = fnre
         fsol = fnt
      else
c        *** Standard filenames
         fmar = 'markers.back'
         fsol = 'temp.back'
      endif
      f2name = fsol
      call openf2(.false.)
      open(9,file=fmar,form='unformatted')
      rewind(9)

c     *** Find the output times
      open(12,file='times.output')
      rewind(12)
      i = 1
10      read(12,*,end=11) iter,toutput(i) 
        i = i+1
        goto 10
11    continue
      niter = i-1
      close(12)
      if (iend.gt.niter) iend=niter
c     *** Determine picture header
      nc = lnblnk(modtext)
      write(modtext(nc+2:nc+17),'(''  Ra = '',f8.1)') rat
      nc = lnblnk(modtext)
      write(modtext(nc+1:nc+16),'(''  Rb = '',f8.1)') ram
      nc = lnblnk(modtext)
      write(modtext(nc+1:nc+16),'(''  rl = '',f8.3)') rl
      write(6,*) 'modtext = ',modtext
      ncharmod = lnblnk(modtext)
      iw = 1
      if (ism.eq.1.or.ibm.eq.1) then
         do 28 itus = 1,istart
            read(9) t,nochain,(imark(ichain),ichain=1,nochain)
            ntot = 0
            do 27 ichain=1,nochain
27             ntot = ntot+imark(ichain)
            read(9) (coormark(i),i=1,2*ntot)
28       continue
      endif
      plotmarkers = (ism.eq.1.or.ibm.eq.1).and.ist.eq.0.
      write(6,*) 'PLOTMARKERS: ',plotmarkers
      do 100 iter=istart,iend,istride
c       *** Read the information from bsfiles
        if (ism.eq.1.or.ibm.eq.1) then
              read(9) t,nochain,(imark(ichain),ichain=1,nochain)
              ntot = 0
              do 25 ichain=1,nochain
25               ntot = ntot+imark(ichain)
              read(9) (coormark(i),i=1,2*ntot)
        endif
        iseq = iter*2+1
        call readbs(iseq,isol1,ihelp)
        call readbs(iseq+1,isol2,ihelp)
        if (t.ne.toutput(iter+1)) 
     v       write(6,*) 't <> toutput!!!',t,toutput(iter+1)
        t = toutput(iter+1)
c       *** determine some outputparameters: Nu,strmax
        if (ist.eq.1) then
           call chtype(1,kprob2)
           call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v                 isol2,iuser,user,ielhlp)  
           temp1  = bounin(1,1,1,1,kmesh2,kprob2,1,1,isol2,iuser,
     v                     user)
           gnus1  = -bounin(2,1,1,1,kmesh2,kprob2,3,3,igradt,iuser,
     v                      user)/temp1
        endif
        vrms2 = c1vrms(isol1,kmesh1,kprob1,iuser,user)/rlam
        vrms = sqrt(vrms2)
c       *** determine maximum amplitude of psi, but keep sign
        call algebr(6,1,isol1,isol1,isol1,kmesh1,kprob1,aa,bb,p,q)
        if (abs(aa).gt.abs(bb)) then
           strmax = aa
        else 
           strmax = bb
        endif

        write(6,'(''t     = '',f12.3)') t
        write(6,'(''Nu    = '',f12.5)') gnus1
        write(6,'(''strmax= '',f12.5)') strmax
        write(6,'(''vrms  = '',f12.5)') vrms

c       *** 
        if (iw.eq.1) then
           dxorg = 0
           dyorg = 0
           i1 = 1
           i2 = 2
           i3 = 2
           if (nsol.eq.1) i3=3   
           if (ism.eq.0.and.ibm.eq.0.and.nsol.eq.1) i2=3
        else if (iw.eq.nsol) then
           dxorg = dx
           dyorg = -2*dy
           i1 = 2
           i2 = 2
           i3 = 3
           if (ism.eq.0.and.ibm.eq.0) i2=3
        else 
           dxorg = dx
           dyorg = -2*dy
           i1 = 2
           i2 = 2
           i3 = 2
        endif
        if (iter.eq.iend) i3=3
     
        write(6,*) 'N,t,xor,i1,i2,i3',iter,t,xor,i1,i2,i3
        iw = iw+1
        if (iw.gt.nsol) iw=1
   
c       *** Plot streamfunction and markers
        if (i1.eq.1) then
c          *** write header
           call peframe(0)
           call plwin(i1,dxorg,dyorg)
           call plafp6(1,x,y)
           xchar = 0d0 + 2.5d0
           ychar = 17.5d0 + 2.5d0
           angle = 0d0
           chmod = 0.5d0 
           call plafp7(xchar,ychar,modtext,angle,chmod,ncharmod)
           call plafp6(5,x,y)
        endif
        
        call plwin(i2,dxorg,dyorg)
        call peframe(0)
        write(tittext,'(''S: t= '',f12.3,''  psm = '',f12.3)') t,strmax
        if (nochain.gt.0.and.plotmarkers) call peframe(1)
        call plotc1(1,kmesh1,kprob1,isol1,contln,0,format,yfaccn,jsmoot)
        if (plotmarkers) then
c          *** Plot markers
           ip = 0
           do 12 ichain=1,nochain
              call peframe(2+ichain/nochain)
              nmark = imark(ichain)
              do 13 i=1,nmark
                xmc(i) = coormark(ip+2*i-1)
13              ymc(i) = coormark(ip+2*i)
             call pecurp(nmark,xmc,ymc,format,yfaccn)
             ip = ip+2*nmark
12         continue
        endif

c       *** Plot temperature or velocity and markers
        call plwin(i2,0d0,dy)
        if (nochain.gt.0.and.plotmarkers) call peframe(1)
        if (ist.eq.1) then
           write(tittext,'(''T: t= '',f12.3,''   Nu = '',f12.5)') 
     v           t,gnus1
           call plotc1(1,kmesh2,kprob2,isol2,contln,0,format,
     v                 yfaccn,jsmoot)
        else
           call plotvc(2,3,isol1,isol1,kmesh1,kprob1,format,yfaccn,0d0)
           if (plotmarkers) then
              ip = 0
              do 19 ichain=1,nochain
                 call peframe(2+ichain/nochain)
                 nmark = imark(ichain)
                 do 14 i=1,nmark
                   xmc(i) = coormark(ip+2*i-1)
14                 ymc(i) = coormark(ip+2*i)
                  ip = ip+2*nmark
                 call pecurp(nmark,xmc,ymc,format,yfaccn)
19            continue
            endif
        endif
c       *** Plot composition
        if (ism.eq.1.or.ibm.eq.1) then
           call plwin(i3,0d0,dy)
           call peframe(1)
           call plafp6(1,x,y)
           if (mooi.eq.1.and.device.eq.'postscript') then
              call marras(coormark,fimage,1)
              call checkvol
              do 50 i=1,nxpix*nypix
50               ipix(i) = icol(ipix(i))
              sx = format
              sy = format/rlam
              ntot = nxpix*nypix
              fimage = 'IMAGE'
              call rasout(ntot,ipix,fimage)
              call postim(nxpix,nypix,sx,sy,ipix)
           endif
           ip = 0
           call peframe(2)
           xmc(1)=0
           ymc(1)=0
           xmc(2)=rl
           ymc(2)=0
           xmc(3)=rl
           ymc(3)=1
           xmc(4)=0
           ymc(4)=1
           xmc(5)=0
           ymc(5)=0
           call pecurp(5,xmc,ymc,format,yfaccn)
           do 59 ichain=1,nochain
              call peframe(2+ichain/nochain)
              nmark = imark(ichain)
              do 54 i=1,nmark
                xmc(i) = coormark(ip+2*i-1)
54              ymc(i) = coormark(ip+2*i)
              ip = ip+2*nmark
              call pecurp(nmark,xmc,ymc,format,yfaccn)
59         continue
        endif
        if ((ism.eq.1.or.ibm.eq.1)) then
           do 61 itus = 2,istride
              read(9,end=63) t,nochain,(imark(ichain),ichain=1,nochain)
              ntot = 0
              do 62 ichain=1,nochain
62               ntot = ntot+imark(ichain)
              read(9) (coormark(i),i=1,2*ntot)
61         continue
63         continue
        endif

100   continue
      end
c ************************************************************
c *   PECUR2
c *
c *   Plots a curve. The points are given in meshcoordinates
c *   Differs from pecurp in the calls of plafp6
c *************************************************************
      subroutine pecur2(nr,xmc,ymc,format,yfact)
      implicit double precision(a-h,o-z) 
      parameter(xoff=2.5d0,yoff=2.5d0)
      dimension xmc(*),ymc(*)

      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes


      if (format.le.0) then
         write(6,*) 'PERROR(pecurvep): format <= 0'
	 return
      endif
      if (nr.lt.2) then
         write(6,*) 'PERROR(pecurvep): nr of points too low'
	 return
      endif
      if (xmin.eq.xmax.or.ymin.eq.ymax) then
         write(6,*) 'PERROR(pecurvep): xmin=xmax or ymin=ymax'
	 return
      endif

      yfaccn = yfact
      if (yfact.eq.0d0) yfaccn = 1d0
      if ((xmax-xmin).ge.(ymax-ymin)) then
         xscale = format/(xmax-xmin)
         yscale = xscale*yfaccn
      else 
         yscale = format/(ymax-ymin)
         xscale  = yscale/yfaccn
      endif

      x = xoff + (xmc(1)-xmin)*xscale
      y = yoff + (ymc(1)-ymin)*yscale
      call plafp6(3,x,y)
      do 10 ip=2,nr
         x = xoff + (xmc(ip)-xmin)*xscale
         y = yoff + (ymc(ip)-ymin)*yscale
         call plafp6(2,x,y)
10    continue

      return
      end
    
