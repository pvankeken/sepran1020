#include "postscript.h"
#include <stdio.h>

postim_(NX,NY,SX,SY,PIX)
int *NX,*NY;   /* dimensions of pixelarry */
float *SX,*SY;   /* size of plot (in cm) */
int PIX[];     /* the image */
{
  int nx,ny,ix,iy;
  unsigned char ch;
  float sx,sy;

  nx = *NX;
  ny = *NY;
  sx = (*SX)/2.54;
  sy = (*SY)/2.54;

  sprintf(itext,"\/picstr %d string def",nx);
  flshbu(itext);
  sprintf(itext,"/inch {300 mul} def");
  flshbu(itext);
  sprintf(itext,"/invinch {300 div} def");
  flshbu(itext);
  sprintf(itext,"\n/imageit");
  flshbu(itext);
  sprintf(itext,"{  %d %d 8 [%d 0 0 -%d 0 %d]",nx,ny,nx,ny,ny);
  flshbu(itext);
  sprintf(itext,"  {  currentfile picstr readhexstring pop }");
  flshbu(itext);
  sprintf(itext,"  image");
  flshbu(itext);
  sprintf(itext,"} def");
  flshbu(itext);
  sprintf(itext,"%f inch %f inch translate",sy/2,sy/2);
  flshbu(itext);
  sprintf(itext,"%f inch %f inch scale",sx,sy);
  flshbu(itext);
  sprintf(itext,"\nimageit");
  flshbu(itext);

  for(iy=0;iy<ny;iy++) 
     for(ix=0;ix<nx;ix++) {
        ch = (unsigned char) PIX[ix+iy*nx];
        sprintf(itext,"%02x",ch);
        flshbu(itext);
     }
/* Restore origin and scale */
  sprintf(itext,"\n%f invinch %f invinch scale",1./sx,1./sy);
  flshbu(itext);
  sprintf(itext,"%f inch %f inch translate",-sy/2,-sy/2);
  flshbu(itext);
}
  
#include <stdio.h>
FILE *fd;
#include "postscript.h"

/*
   ******************
   ***** flshbu *****
   ******************
 
   parameters : cstring
 
   purpose    : output routine for postscript library
 
   special considerations :
   Output is written to file or to "stdout", dependent on the initiation
   that is performed in routine "plots".
   All questions and error messages from postscript library routines are
   written to "stderr", so if output to "stdout" is chosen, it can be
   redirected.
   Apparently, for characters with ASCII code > 127 the most significant
   bit is stripped by the lineprinter deamon, so those characters are
   send to postscript as an octal ASCII code, escaped by a backslash.
*/ 

flshbu(cstring)
unsigned char *cstring;

{
int i;
char nl=10;
if(fd == 0)
     {while(*cstring != NULL)
	  {if(*cstring < 128)
	        {putchar(*cstring);}
	   else
		{printf("\\%03o",*cstring);}
	   cstring++;
          }
      putchar(nl);
     }
else
     {while(*cstring != NULL)
	  {if(*cstring < 128)
	        {putc(*cstring,fd);}
	   else
		{fprintf(fd,"\\%03o",*cstring);}
	   cstring++;
          }
      putc(nl,fd);
     }
}
