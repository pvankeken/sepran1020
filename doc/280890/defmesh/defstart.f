      subroutine defstart(kmesh,kprob,intmat,isol,iuser,user,
     v                    iinput,rinput,nbufdef)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),isol(*),iuser(*),user(*)
      dimension iinput(*),rinput(*),iu1(2),u1(2),iwork(10),work(10)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor,niter
      common /defc1/ wnod1,zl1,ain1,rl1

      kmesh(1) = 100
      kprob(1) = 200
      iuser(1) = 100
       user(1) = 100
      iinput(1)= 100
      rinput(1)= 100

      call start(0,0,0,0)
      read(5,*) rl1,ain1
      read(5,*) nitermax,nout,ncor
      read(5,*) tfac,tmax,tstepmax,tvalid

      call mesh(6,iinput,rinput,kmesh)
      call mesh(1,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      iu1(1) = 0
      iu1(2) = 0
       u1(1) = 0d0
       u1(2) = 0d0
      call creavc(0,1,idum,isol,kmesh,kprob,iu1,u1,iu2,u2) 
      call chanbn(-2,num,3,3,kmesh,kprob,isol,iinput,rinput,iuser,user)
      call mesh(4,iinput,rinput,kmesh)

      

      eps    = 1d-6
      f2     = -1d0
      MODELV = 1
      vis1   = 1d0
      vis2   = 1d2
      rho1   = 1d0
      rho2   = 2d0

      nparam = 8
      do 10 i=1,nparam
        iwork(i) = 0
10      work(i) = 0d0
      work(1) = eps
      work(2) = rho1
      work(6) = f2
      iwork(7)= MODELV
      work(8) = vis1
      call fil100(1,iuser,user,kprob,nparam,iwork,work)

      work(2) = rho2
      work(8) = vis2
      call fil100(2,iuser,user,kprob,nparam,iwork,work)

      nsuper = kprob(29)
      if (nsuper.gt.0) then
	 write(6,*) 'NSUPER = ',nsuper
	 stop
      endif
      fname = 'MESH'
      ifrinit = 0
      iway = 2
      format = 10d0
      jkader= -4
      jmark=5
      call plotm2(0,kmesh,iuser,format,yfaccn)

      open(11,file='defmesh.data')
      rewind(11)
      write(11,'(i5,f12.3,f12.7,f12.3)') 0,0d0,(wnod1-zl1),0d0
      call flush(11)

      call commat(1,kmesh,kprob,intmat)
      call bvalue(0,0,kmesh,kprob,isol,value,icrv1,icrv2,idegfd,jstap)

      return
      end
