      subroutine outvel(kmesh,kprob,isol)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*)
      dimension icurvs(2),funcx(200),funcy(200)

      funcx(1) = 200
      funcy(1) = 200
      icurvs(1) = 0
      icurvs(2) = 3
      call compcr(0,kmesh,kprob,isol,2,icurvs,funcx,funcy)
      xp = funcx(6)
      yp = funcx(7)
      vx = funcy(6)
      vy = funcy(7)

      write(6,'(2f10.3,2f10.5)') xp,yp,vx,vy
      return
      end
