      subroutine deffilc(ichois,kprob,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kprob(*),iuser(*),user(*),iwork(10),work(10)

      eps    = 1d-6
      f2     = 1d0
      MODELV = 1
      vis1   = 1d0
      vis2   = 1d0
      rho1   = 1d0
      rho2   = 2d0

      nparam = 8
      do 10 i=1,nparam
	 iwork(i) = 0
10       work(i) = 0d0
       work(1) = eps
       work(2) = rho1
       work(6) = f2
       iwork(7)= MODELV
       work(8) = vis1
       call fil100(1,iuser,user,kprob,nparam,iwork,work)

       work(2) = rho2
       work(8) = vis2
       call fil100(2,iuser,user,kprob,nparam,iwork,work)

       return
       end
