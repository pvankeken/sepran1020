      subroutine defcoor(kmesh,kprob,intmat,isol,iinput,rinput,
     v                   iuser,user)
      implicit double precision(a-h,o-z)
      parameter(half=0.5d0)
      dimension kmesh(*),kprob(*),intmat(*),isol(*),iinput(*),rinput(*)
      dimension iuser(*),user(*),isltus(5),irhsd(5),matr(5),kmesh(100)


      call copyvc(isol,isltus)
      call chanbn(-2,number,3,3,kmesh,kprob,isol,iinput,rinput,
     v            iuser,user)
      call mesh(5,iinput,rinput,kmesh)

      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
	  
      call algebr(3,0,isol,isltus,isltus,kmesh,kprob,half,-half,
     v            p,q,ipoint)
      call chanbn(-2,number,3,3,kmesh,kprob,isltus,
     v            iinput,rinput,iuser,user)
	 
      call mesh(4,iinput,rinput,kmesh)
      write(6,*) 'un+1,vn+1: '
      call outvel(kmesh,kprob,isol)
  
      return
      end
