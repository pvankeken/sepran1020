      program defmesh
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF = 1 000 000)
      parameter(half=0.5d0)
c *************************************************************
c *   DEFMESH
c *
c *   Modelling Rayleigh-Taylor instabilities with a deformable
c *   mesh.
c *
c *   Predictor-corrector approach
c * 
c *   PvK 190490
c *************************************************************
      dimension kmesh(100),kprob(200),iuser(100),user(100)
      dimension matr(5),irhsd(5),intmat(5),isol(5),islold(5)
      dimension iinput(100),rinput(100),isltus(5)
      dimension iu1(10),u1(10),icurvs(2),funcx(200),funcy(200)


      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor,niter
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /defc1/ wnod1,zl1,ain1,rl1
      real t0,t1,t2
      data funcx(1),funcy(1)/200,200/

      call defstart(kmesh,kprob,intmat,isol,iuser,user,iinput,
     v              rinput,NBUFDEF)

c     *********** MAIN LOOP
      niter = 0
      t     = 0d0
      cpu   = 0d0
      call second(t0)
200   continue
         call second(t1)
	 niter = niter+1
	 call defpred(kmesh,kprob,intmat,isol,iinput,rinput,iuser,user)

	 if (niter.eq.1) then
	    icurvs(1) = 0
	    icurvs(2) = 3
	    call compcr(0,kmesh,kprob,isol,2,icurvs,funcx,funcy)
	    vpmax = -funcy(7)/ain1
	    call second(t2)
	    write(6,'(''vpmax = '',f12.7,f12.3)') vpmax,t2-t1
         endif

         tstep = tstepmax
	 t     = t+tstep
	 call defcoor(kmesh,kprob,intmat,isol,iinput,rinput,iuser,user)

	 call second(t2)
	 dcpu = t2 - t1
	 write(11,'(i5,f12.3,f12.7,f12.3)') niter,t,(wnod1-zl1),dcpu
	 call flush(11)

      if (niter.lt.nitermax.and.t.lt.tmax) goto 200

      write(6,'(''This took: '',f12.3)') t2-t0

      call defout(kmesh,kprob,isol)

      end
