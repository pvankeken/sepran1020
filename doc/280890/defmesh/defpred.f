      subroutine defpred(kmesh,kprob,intmat,isol,iinput,rinput,
     v                    iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),intmat(*),isol(*),iinput(*),rinput(*)
      dimension iuser(*),user(*),irhsd(5),matr(5)

      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,iuser,user,
     v            islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      write(6,*) 'un, vn: '
      call outvel(kmesh,kprob,isol)

      return
      end
