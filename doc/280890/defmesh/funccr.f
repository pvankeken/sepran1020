      subroutine funccr(coorol,coornw,uold,inodp,ilocal,anorm,n)
      implicit double precision(a-h,o-z)
      dimension coorol(*),coornw(*),uold(*),anorm(*)
      parameter(pi=3.1415926)

      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /defc1/ wnod1,zl1,ain1,rl1
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor,niter

      xold = coorol(2*ilocal-1)
      yold = coorol(2*ilocal)
      xnorm = anorm(2*ilocal-1)
      ynorm = anorm(2*ilocal)
      u1 = uold(1)
      u2 = uold(2)

      if (niter.eq.0) then
	 xnew = xold
	 zl1  = yold
	 ynew = yold + ain1*cos(pi*xold/rl1)
      else
	 xnew = xold + tstep*uold(1)
	 ynew = yold + tstep*uold(2)
      endif
      if (ilocal.eq.N) then
	 wnod1 = ynew
      endif

      coornw(2*ilocal-1) = xnew
      coornw(2*ilocal)   = ynew

      return
      end
