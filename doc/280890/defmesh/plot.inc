      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
