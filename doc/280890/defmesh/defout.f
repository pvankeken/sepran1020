      subroutine defout(kmesh,kprob,isol)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*)
      dimension icurvs(2),funcx(200),funcy(200)

      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      data funcx(1),funcy(1)/2*200/

      jmark = 5
      jkader = -4
      x=0d0
      y=0d0
      call plwin(1,x,y)
      fname = 'MESH'
      format = 10d0
      call plotm2(0,kmesh,iuser,format,yfaccn)
      x = format+1d0
      call plwin(3,x,y)
      call plotvc(1,2,isol,isol,kmesh,kprob,format,yfaccn,0d0)
      icurvs(1) = 0
      icurvs(2) = -3
      call compcr(-1,kmesh,kprob,ivectr,number,icurvs,funcx,funcy)
      open(12,file='funccr.out')
      rewind(12)
      ncoor = funcx(5)
      write(12,*) 2,ncoor/2
      do 300 i=1,ncoor,2
300      write(12,*) funcx(5+i),funcx(5+i+1)
      close(12)

      return
      end

