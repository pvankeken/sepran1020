c *************************************************************
c *   Determine pressure by integration  of
c *
c *                   3         3
c *           dp     d psi     d psi
c * - eta *   --  =  ------ +  -----
c *           dx       2          3
c *                  dx dy      dy
c *
c *   Use 1D Gaussian integration. Integration is performed over
c *   element iel, at y-level y0.
c *************************************************************
      subroutine c1prsx(kmeshc,coor,iel,kprobp,usol,npoint,y0,x1,x2)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),usol(*),kprobp(npoint,3)
      dimension nodno(4),x(4),y(4),psi(12),fp1(12),fp2(12)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peg1d/ w1d(4),xg1d(4),ng1d
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

      inpelm = 4
      ip = inpelm*(iel-1)
      do 10 i=1,inpelm
10       nodno(i) = kmeshc(ip+i)
      do 20 i=1,inpelm
         x(i) = coor(1,nodno(i))
20       y(i) = coor(2,nodno(i))
      do 30 i=1,inpelm
	 ioff = (i-1)*3
	 ip   = nodno(i)
         do 35 j=1,3
35          psi(ioff+j) = usol(kprobp(ip,j))
30    continue
      x1= x(1)
      x2= x(2)
      a = x(2)-x(1)
      b = y(4)-y(1)
      eta0 = (y0-y(1))/b
      a2b = a*a*b
      b3  = b*b*b
      if (eta0.lt.0.or.eta0.gt.1) then
         write(6,*) 'PERROR(c1prsx):  y0 is not in element'
      endif
      if (nrule.gt.0) then
c        *** Gauss integration
         sum = 0
         do 40 k=1,ng1d
            dp1 = 0
            dp2 = 0
	    xi = xg1d(k)
	    eta= eta0
	    call fd3pdx2y(fp1,xi,eta,a,b)
	    call fd3pdy(fp2,xi,eta,a,b)
	    do 50 i=1,nphi
	       dp1 = dp1 + psi(i)*fp1(i)/a2b
	       dp2 = dp2 + psi(i)*fp2(i)/b3
50          continue
            xc = x(1)+a*xg1d(k)
            write(8,*) xc,dp1+dp2
	    sum = sum+w1d(k)*(dp1+dp2)
40       continue
      else if (nrule.eq.-1) then
c        *** Newton Cotes
         write(6,*) 'PERROR(c1prsx): NC not yet implemented'
      endif
      c1prsx = sum*a

      return
      end
 
