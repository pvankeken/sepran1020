c *************************************************************
c *   PRES
c *
c *************************************************************
      program pres
      implicit double precision(a-h,o-z)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peg1d/ w1d(4),xg1d(4),ng1d
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension user(10000),kmesh(100),kprob(100),isol(5),iu1(3)
      dimension lu1(9),lu2(9),rnum(10000),ana(10000)

      kmesh(1) = 100
      kprob(1) = 100
      user(1)  = 10000

      read(5,*) ielstart,ielend,y0
      call start(0,0,0,0)
      open(9,file='mesh3')
      call meshrd(3,9,kmesh)
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))
      call probdf(0,kprob,kmesh,iinput)
      iu1(1) = 1
      iu1(2) = 2
      iu1(3) = 3
      call creavc(0,1,ivec,isol,kmesh,kprob,iu1,u1,iu2,u2)
      call ini055(isol,kprob,indprf,indprh,indprp,nphys,'isol')
      iusol = infor(1,isol(1))
      iindprp = infor(1,indprp)
      nphi = 12
      nrule = 2
      ngaus = 4
      call fwgaus
      psum = 0
      npoint = kmesh(8)
      nelem = kmesh(9)
c     open(8,file='dpsdx.n')
c     rewind(8)
c     open(9,file='dpsdx.a')
c     rewind(9)
c     lu1(1) = 8
c     lu2(1) = 9
c     call pdpsdx(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
c    v          ibuffr(iindprp),nelem,npoint,lu1,lu2)
c     rewind(8)
c     rewind(9)
c     read(8,*) (rnum(i),i=1,ngaus*nelem)
c     read(9,*) (ana(i),i=1,ngaus*nelem)
c     d2 = 0
c     d= 0
c     do 10 i=1,ngaus*nelem
c        d1= rnum(i)-ana(i)
c        d = d+d1*d1
c        d2 = max(abs(d1),d2)
c0    continue
c     write(6,*) 'rms error: ',sqrt(d)
c     write(6,*) 'L1 error: ',d2
c     close(8)
c     close(9)
      do 10 iel=ielstart,ielend
         p1 = c1prsx(ibuffr(ikelmc),ibuffr(ikelmi),iel,
     v               ibuffr(iindprp),ibuffr(iusol),npoint,y0,x1,x2)
         write(6,1000) x1,x2,p1,psum
         psum = psum + p1
10    continue
1000  format(2f12.4,3x,2f12.3)
      end

      subroutine pdpsdx(kmeshc,coor,usol,kprobp,nelem,npoint,lu1,lu2)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),psi(12),usol(*)
      dimension kprobp(npoint,3),nodno(4),x(4),y(4),fp(12),lu1(9),lu2(9)
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      
      inpelm = 4
      do 100 iel=1,nelem
         ip = inpelm*(iel-1)
         do 10 i=1,inpelm
10          nodno(i) = kmeshc(ip+i)
         do 20 i=1,inpelm
            x(i) = coor(1,nodno(i))
20          y(i) = coor(2,nodno(i))
         do 30 i=1,inpelm
            ioff = (i-1)*3
            ip = nodno(i)
            do 35 j=1,3
35             psi(ioff+j) = usol(kprobp(ip,j))
30       continue
         a = x(2)-x(1)
         b = y(4)-y(1)
         do 40 k=1,4
            call fd3pdy(fp,xg(k),yg(k),a,b)
            ps = 0
            do 50 i=1,12
50             ps = ps + fp(i)*psi(i)/(b*b*b)
            x1 = x(1)+a*xg(k)
            y1 = y(1)+b*yg(k)
            ps2 = func(6,x1,y1,z1)
            write(lu1(1),*) x(1)+a*xg(k),y(1)+b*yg(k),ps
            write(lu2(1),*) x(1)+a*xg(k),y(1)+b*yg(k),ps2
40       continue
100   continue
      return
      end
      subroutine ppsi(kmeshc,coor,usol,kprobp,nelem,npoint,lu1,lu2)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),psi(12),usol(*)
      dimension kprobp(npoint,3),nodno(4),x(4),y(4),fp(12),lu1(9),lu2(9)
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /peshape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      
      inpelm = 4
      do 100 iel=1,nelem
         ip = inpelm*(iel-1)
         do 10 i=1,inpelm
10          nodno(i) = kmeshc(ip+i)
         do 20 i=1,inpelm
            x(i) = coor(1,nodno(i))
20          y(i) = coor(2,nodno(i))
         do 30 i=1,inpelm
            ioff = (i-1)*3
            ip = nodno(i)
            do 35 j=1,3
35             psi(ioff+j) = usol(kprobp(ip,j))
30       continue
         a = x(2)-x(1)
         b = y(4)-y(1)
         do 40 k=1,4
            call fphi(fp,xg(k),yg(k),a,b)
            ps = 0
            do 50 i=1,12
50             ps = ps + fp(i)*psi(i)
            x1 = x(1)+a*xg(k)
            y1 = y(1)+b*yg(k)
            ps2 = func(1,x1,y1,z1)
            write(lu1(1),*) x(1)+a*xg(k),y(1)+b*yg(k),ps
            write(lu2(1),*) x(1)+a*xg(k),y(1)+b*yg(k),ps2
40       continue
100   continue
      return
      end
