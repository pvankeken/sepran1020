c *************************************************************
c *   FUN
c *************************************************************
      function func(ichois,x,y,z)
      implicit double precision(a-h,o-z)
      parameter(pi=3.1415926)
c     if (ichois.eq.1) then
c        func = x*x*y+x*x*x*y+x*y*y*y
c     else if (ichois.eq.2) then
c        func = x*x + x*x*x + x*y*y*3
c     else if (ichois.eq.3) then
c        func = -(2*x*y+3*x*x*y+y*y*y)
c     else if (ichois.eq.4) then
c        func = 2*y+6*x*y
c     else if (ichois.eq.5) then
c        func = 6*x*y
c     else if (ichois.eq.6) then
c        func = 2+6*x
c     else if (ichois.eq.7) then
c        func = 6*x
c     endif
      if (ichois.eq.1) then
         func = -sin(pi*x)*sin(pi*y)
      else if (ichois.eq.2) then
         func = -pi*sin(pi*x)*cos(pi*y)
      else if (ichois.eq.3) then
         func =  pi*cos(pi*x)*sin(pi*y)
      else if (ichois.eq.4) then
         func = pi*pi*sin(pi*x)*sin(pi*y)
      else if (ichois.eq.5) then
         func = pi*pi*sin(pi*x)*sin(pi*y)
      else if (ichois.eq.6) then
         func = pi*pi*pi*sin(pi*x)*cos(pi*y)
      else if (ichois.eq.7) then
         func = pi*pi*pi*sin(pi*x)*cos(pi*y)
      endif
      return
      end
