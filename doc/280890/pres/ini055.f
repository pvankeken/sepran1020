cdccini055
      subroutine ini055 ( isol, kprob, indprf, indprh, indprp, nphys,
     .                    name )
c
c ********************************************************************
c
c        programmer   Guus Segal
c        version 1.0  date   17-02-89
c
c
c   copyright (c) 1989  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c     General memory management subroutine which simplifies the computation 
c     of the starting addresses of an existing SEPRAN solution array and its
c     corresponding arrays KPROB parts f, h and p
c
c     If these arrays exist, it is checked if they are positioned in
c     in array IBUFFR (Blank common) or not.
c     If one or more of these arrays are not available
c     these array are read from file 3 and space is reserved in array IBUFFR.
c     The priority number is made equal to 10.
c
c     if no space is available in array IBUFFR, INI055 tries to
c     create space in the following ways:
c
c       -  by writing low priority arrays to backing storage
c
c       -  if this is not sufficient by compressing
c          array ibuffr after writing of all arrays with priority
c          smaller than 10 to backing storage
c
c     INI055 replaces the old subroutine INI103 or INI002 (for existing arrays)
c
c     If array KPROB part f (or its corresponding counterpart) does exist,
c     INDPRF gets the correct index number referring to array INFOR in
c     common block CARRAY.
c     Afterwards the starting address of KPROB part f is given by
c     INFOR(1,INDFOR)
c
c     In the same way INDPRH and INDPRP refer to KPROB parts h and p
c     respectively
c
c ********************************************************************
c
c                     INPUT / OUTPUT
c

cimp      implicit none
      integer isol(5), kprob(*), indprf, indprh, indprp, nphys
      character * (*) name

c
c     isol    i  The standard SEPRAN solution array or array of special
c                structure of type 115 from which the starting positions
c                must be computed and the relevant information found.
c                Other types of arrays are not allowed
c
c     kprob   i  Standard SEPRAN array containing information of the problems
c                to be solved.
c
c     indprf  o  At output INDPRF contains information concerning the KPROB
c                part f corresponding to ISOL.
c                If no KPROB part f corresponds to ISOL, INDPRF = 0, otherwise
c                (INDPRF > 0) the starting address of KPROB part f is given
c                by INFOR(1,INDPRF)
c
c     indprh  o  At output INDPRH contains information concerning the KPROB
c                part h corresponding to ISOL.
c                If no KPROB part h corresponds to ISOL, INDPRH = 0, otherwise
c                (INDPRH > 0) the starting address of KPROB part h is given
c                by INFOR(1,INDPRH)
c
c     indprp  o  At output INDPRP contains information concerning the KPROB
c                part p corresponding to ISOL.
c                If no KPROB part p corresponds to ISOL, INDPRP = 0, otherwise
c                (INDPRP > 0) the starting address of KPROB part p is given
c                by INFOR(1,INDPRP)
c
c     nphys   o  At output NPHYS contains the maximal number of degrees of
c                freedom per point or the number of physical variables if
c                defined
c                If ISOL is a vector of special structure and the number of
c                degrees of freedom per point is not constant then NPHYS = 0
c
c     name    i  name of calling subroutine
c
c ********************************************************************
c
c                    COMMON BLOCKS
c
c     none
c
c ********************************************************************
c
c                    LOCAL PARAMETERS
c

       integer i, itypvc, iprob, ipkprb, ivec, kpivec
c
c     kpivec      Starting address of information concerning arrays of
c                 special structure in KPROB
c
c     i           Loop variable
c
c     ipkprb      Starting address of part of KPROB with respect to
c                 a complete problem in array KPROB (minus one)
c
c     iprob       Problem number
c
c     itypvc      Type number of solution vector (110=solution vector,
c                 115=vector of special structure)
c
c     ivec        Indication of the structure of a vector of special structure
c
c ********************************************************************
c
c                              I/O
c
c     none
c
c ********************************************************************
c
c                SUBROUTINES CALLED
c
c     SEPRAN service subroutines:
c
c     ERTRAP:  Gives error message
c     INI050:  Compute start address of existing vector in array IBUFFR
c
c ********************************************************************
c
c                METHOD
c
c     Compute problem number (IPROB), type of solution vector (ITYPVC)
c     and for a vector of special structure the vector number (IVEC)
c
c     Read solution array in IBUFFR (INI050)
c
c     If ( ISOL is of type 110 ) then
c
c        if ( KPROB part f has been filled ) then
c           Set INDPRF and read KPROB part f in IBUFFR
c
c        if ( KPROB part h has been filled ) then
c           Set INDPRH and read KPROB part h in IBUFFR
c
c        if ( KPROB part p has been filled ) then
c           Set INDPRP and read KPROB part p in IBUFFR
c
c     else if ( ISOL is of type 115 ) then
c
c        if ( KPROB part f has been filled ) then
c           Set INDPRF and read KPROB part f in IBUFFR
c
c     else
c
c        wrong type of vector found: error
c
c ********************************************************************
c
c                ERROR MESSAGES:
c
c     744:  Fatal error: Only arrays of type 110 and 115 are permitted
c
c ********************************************************************
c
      indprf = 0
      indprh = 0
      indprp = 0
      itypvc = isol(2)

c   *  Put ISOL into IBUFFR

      call ini050 ( isol(1), name )

c   *  Compute IPROB and offset for array KPROB

      iprob  = max ( 1, isol(4)/1000 )
      ipkprb = 1
      do 100 i = 2, iprob
         ipkprb = ipkprb + kprob ( 2+ipkprb )
100   continue

      if ( itypvc .eq. 110 ) then

c      *  ISOL is of type 110:  solution vector

         nphys = max ( kprob(ipkprb+3), kprob(ipkprb+32) )
         indprf = kprob(ipkprb+18)
         indprh = kprob(ipkprb+20)
         indprp = kprob(ipkprb+34)
         if ( indprf .gt. 0 ) call ini050 ( -indprf, name )
         if ( indprh .gt. 0 ) call ini050 ( -indprh, name )
         if ( indprp .gt. 0 ) call ini050 ( -indprp, name )

      else if ( itypvc .eq. 115 ) then

c      *  ISOL is of type 115:  array of special structure

         ivec   = mod(isol(4),1000)
         kpivec = kprob(ipkprb+17)-1+kprob(39)*kprob(6)+ivec + ipkprb-1
         nphys  = kprob(kpivec)
         if ( nphys .lt. 0 ) then

c         *  nphys < 0, constant number of degrees of freedom per point

            nphys = -nphys

         else

c         *  nphys > 0, number of degrees of freedom per point not constant

            indprf = nphys
            nphys  = 0
            call ini050 ( -indprf, name )

         end if   

      else

c       *  wrong type of vector

         call ertrap ( name, 744, 1, 0, 0, 0 )

      end if
      end
cdc*eor
