      subroutine exqint(tn,phi,nphys,user)
      implicit double precision(a-h,o-z)
      dimension tn(nphys,*),phi(*),user(nphys),p(6)

      p123 = phi(1)*phi(2)*phi(3)
      p(1) = phi(1)*(2*phi(1)-1) + 3*p123
      p(2) = 4*phi(1)*phi(2) - 12*p123
      p(3) = phi(2)*(2*phi(2)-1)+ 3*p123
      p(4) = 4*phi(2)*phi(3)- 12*p123
      p(5) = phi(3)*(2*phi(3)-1)+ 3*p123
      p(6) = 4*phi(1)*phi(3)- 12*p123
      do 10 idof=1,nphys
         t = 0
         do 20 ikn=1,6
            t  = t + tn(idof,ikn)*p(ikn)
20       continue
         user(idof) = t
10    continue
      return
      end
