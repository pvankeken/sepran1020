      subroutine quadrint(tn,phi,nphys,user)
      implicit double precision(a-h,o-z)
      dimension tn(nphys,*),phi(*),user(nphys),p(6)

      p(1) = phi(1)*(2*phi(1)-1) 
      p(2) = 4*phi(1)*phi(2)
      p(3) = phi(2)*(2*phi(2)-1)
      p(4) = 4*phi(2)*phi(3)
      p(5) = phi(3)*(2*phi(3)-1)
      p(6) = 4*phi(1)*phi(3)
      do 10 idof=1,nphys
         t = 0
         do 20 ikn=1,6
            t  = t + tn(idof,ikn)*p(ikn)
20       continue
         user(idof) = t
10    continue
      return
      end
