c *************************************************************
c *    TRILIN
c *
c *    Determine coefficients of shapefunctions of linear triangle
c *    
c *    PvK 170590
c *************************************************************
       subroutine trilin(x,y,a,b,c)
       implicit double precision(a-h,o-z)
       dimension x(3),y(3),a(3),b(3),c(3)
 
       a(1) = x(2)*y(3) - y(2)*x(3)
       a(2) = -x(1)*y(3) + y(1)*x(3)
       a(3) = x(1)*y(2) - y(1)*x(2)
       b(1) = y(2)-y(3)
       b(2) = y(3)-y(1)
       b(3) = y(1)-y(2)
       c(1) = x(3)-x(2)
       c(2) = x(1)-x(3)
       c(3) = x(2)-x(1)
       delta = -c(3)*b(1) + b(3)*c(1)
       if (delta.eq.0) then
          write(6,*) 'PERROR(trilin): surface of element is zero'
          stop
       endif
       do 10 i=1,3
          a(i) = a(i)/delta
          b(i) = b(i)/delta
          c(i) = c(i)/delta
10     continue
       return
       end

