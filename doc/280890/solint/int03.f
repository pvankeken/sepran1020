c *************************************************************
c *   INT03
c * 
c *   Determines nodalpoints values, coordinates and solution in
c *   the nodalpoints of element iel
c *   For elements with more than one degree of freedom per
c *   nodalpoint
c *
c *   PvK 170590
c *************************************************************
      subroutine int03(kmeshc,coor,usol,kprobp,xn,yn,
     v                  tn,ielem,nphys,inpelm,npoint)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,npoint),kprobp(npoint,nphys),xn(*)
      dimension usol(*),tn(nphys,6),yn(6)
      dimension nodno(6)
 
      
      ip = (ielem-1)*inpelm
c     write(6,*) 'ip,nphys,inpelm: ',ip,nphys,inpelm
c     *** nodalpoint numbers in kmesh part c
      do 10 i=1,inpelm
10        nodno(i) = kmeshc(ip+i)
c     print *,(nodno(i),i=1,3)
c     *** coordinates in coor
      do 20 i=1,inpelm
         xn(i) = coor(1,nodno(i))
20       yn(i) = coor(2,nodno(i))
c     print *,'x: ',(xn(i),i=1,3)
c     print *,'y: ',(yn(i),i=1,3)
c     *** solution in usol
      do 30 i=1,inpelm
         do 31 it=1,nphys
            tn(it,i) = usol(kprobp(nodno(i),it))
31       continue
30     continue
       return
       end
