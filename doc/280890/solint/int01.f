      subroutine int01(coor,kmesh,kprob,isol,user,np2,ichois)
      implicit double precision(a-h,o-z)
      dimension coor(2,*),kmesh(*),kprob(*),isol(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)
      logical linear,quadra,exqua
      dimension xn(6),yn(6),tn(12),a(3),b(3),c(3),phi(3),xl(6),yl(6)
      
      linear = ichois.eq.1
      quadra = ichois.eq.2
      exqua  = ichois.eq.3
      inpelm = 3
      if (ichois.gt.1) inpelm = 6
      write(6,*) 'linear,quadratic',linear,quadra
      call ini050(kmesh(23),'int01: coordinates')
      call ini050(kmesh(17),'int01: nodalpoints')
      call ini055(isol,kprob,indprf,indprh,indprp,nphys,'int01')
      ikelmc = infor(1,kmesh(17))
      ikelmi = infor(1,kmesh(23))
      iusol  = infor(1,isol(1))
      if (indprh.gt.0) then
         iindprh = infor(1,indprh)
      else
         iindprh = 1
      endif
      iindprp = infor(1,indprp)
      npoint = kmesh(8)
      nelem = kmesh(9)
      write(6,*) 'nelem,np2,nphys: ',nelem,np2,nphys
      ieasy = 1
      do 10 ikn = 1,np2
         if ((ikn/10)*10.eq.ikn) write(6,*) ikn
         x = coor(1,ikn)
         y = coor(2,ikn)
5        continue
         call pedetel(1,x,y,iel)
         if (iel.gt.nelem) then
            write(6,*) 'PERROR(int01): iel > nelem'
            write(6,*) 'iel = ',iel
            write(6,*) 'ikn = ',ikn
            write(6,*) 'x,y = ',x,y
            stop
         endif
c        *** determine coordinates and dof's of the element
         if (linear) then
           call int02(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
     v                ibuffr(iindprh),indprh,xn,yn,tn,iel,nphys,inpelm)
         else
           call int03(ibuffr(ikelmc),ibuffr(ikelmi),ibuffr(iusol),
     v                ibuffr(iindprp),xn,yn,tn,iel,nphys,inpelm,npoint)
         endif
c        *** Calculate coefficients of the basisfunctions
         if (linear) then
            do 21 i=1,3
               xl(i) = xn(i)
21             yl(i) = yn(i)
         else
            do 22 i=1,3
               xl(i) = xn(2*i-1)
               yl(i) = yn(2*i-1)
22          continue
         endif
         call trilin(xl,yl,a,b,c)
         do 30 i=1,3
            phi(i) = a(i)+b(i)*x+c(i)*y
30       continue
c        *** All basisfunctions are positive if the point is
c        *** in the element
         eps = -1e-7
         if (phi(1).lt.eps.or.phi(2).lt.eps.or.phi(3).lt.eps) then
            write(6,*) 'Point is not in this  element '
            write(6,*) (phi(i),i=1,3)
         endif
         ip = 6 + (ikn-1)*nphys 
         if (linear) then
c           *** Linear triangle
            call linint(tn,phi,nphys,user(ip))
c           if (ikn/10*10.eq.ikn)  then
c               write(6,'(i5,2f12.3,i5)') ikn,x,y,nphys
c               write(6,'(5x,3f12.3)') (phi(i),i=1,3)
c               write(6,'(5x,3f12.3)') (tn(i),i=1,3)
c               write(6,'(5x,3f12.3,:)') (user(ip+i-1),i=1,nphys)
c           endif
         else if (quadra) then
c           *** Quadratic triangle: 
            call quadrint(tn,phi,nphys,user(ip))
c           write(6,'(a,4f12.3)') 'x,y,u,v: ',x,y,user(ip),user(ip+1)
         else if (exqua) then
c           *** extended quadratic triangle
            call exqint(tn,phi,nphys,user(ip))
c           write(6,'(a,4f12.3)') 'x,y,u,v: ',x,y,user(ip),user(ip+1)
         endif
10    continue
      return
      end
