c *************************************************************
c *   SOLINT
c *
c *   Interpolation of a solutionvector from one mesh to another.
c *   
c *   Input:
c *        ICHOIS         Choice parameter:
c *                       1  Linear triangles, 1 dof
c *                       2  Quadratic triangles, 2 dof's
c *        F2BACK         Name of the bsfile containing the
c *                       solution vector to be interpolated
c *        NREC           Record number of solution vector on F2BACK
c *        FMSH1          File containing the mesh on which the
c *                       vector is defined
c *        FMSH2          File containing the mesh on which the
c *                       solutionvector has to be interpolated
c *        F2RES          Name of bsfile on which the output is
c *                       written
c *
c *   PvK 260690
c *************************************************************
      program solint
      implicit double precision(a-h,o-z)
      dimension kmesh1(100),kmesh2(100),kprob1(100),kprob2(100)
      dimension isol1(5),isol2(5),iu1(2),u1(2)
      character*80 f2back,fmsh1,fmsh2,f2res
      common /bstore/f2name
      character*80 f2name
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      data kmesh1(1),kmesh2(1),kprob1(1),kprob2(1)/4*100/

      read(5,*) jchois
      read(5,*) f2back,nrec1
      read(5,*) fmsh1,fmsh2
      read(5,*) f2res,nrec2

      ichois = jchois - (jchois/10)*10

      call start(0,1,0,0)
      open(9,file=fmsh1)
      call meshrd(3,9,kmesh1)
      open(9,file=fmsh2)
      call meshrd(3,9,kmesh2)
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
     
      f2name = f2back
      call openf2(.false.)
      call readbs(nrec1,isol1,ihelp)
      if (ichois.eq.1) then
         ipchois = 1
      else
         ipchois = 2
      endif
      call pefilxy(ipchois,kmesh1,kprob1,isol1)
      iu1(1) = 0
      iu1(2) = 0
      u1(1) = 0d0
      u1(2) = 0d0
      call creavc(0,1,ivec,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
     
      if (jchois.lt.10) then
        call intit(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,ichois)
      else
        call intmsh(0,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2)
      endif
 


      iway=2
      fname='INTERP'
      yfaccn = 1
      jkader=-4
      format=1d1
      chheight = 0.3d0
      if (ichois.eq.1) then
        call plwin(1,0d0,0d0)
        call plotc1(1,kmesh1,kprob1,isol1,contln,0,format,yfaccn,jsmoot)
        call plwin(3,format+2d0,0d0)
        call plotc1(1,kmesh2,kprob2,isol2,contln,0,10d0,1d0,0)
      else
        call plwin(1,0d0,0d0) 
        call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,yfaccn,0d0)
        call plwin(3,format+2d0,0d0)
        call plotvc(1,2,isol2,isol2,kmesh2,kprob2,format,yfaccn,0d0)
      endif
      f2name = f2res
      if (nrec2.eq.1) then
        call openf2(.true.)
      else
        call openf2(.false.)
      endif
      call writbs(0,nrec2,numarr,'isol2',isol2,ihelp)
      call writb1

      end

