c *******************************************************************
c *   PECOPY
c *
c *   Adaption of pecopy/er0035 to allow for scaling a solutionvector
c *
c *   PvK 7-11-89
c *
c *   Extension with ichois >= 10:
c *         10   copy first dof 
c *         11   copy second dof
c *         12   copy third dof
c *******************************************************************
      subroutine pecopy(ichois,isol,user,kprob,istart,factor)
      implicit double precision (a-h,o-z)
      logical new
      dimension isol(*),kprob(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      if (ichois.eq.1) then
	 write(6,*) 'PERROR(pecopy): option 1 not available'
         stop
      endif
      iprob=isol(4)/1000+1
      if (iprob.gt.1) then
	 write(6,*) 'PERROR(pecopy): iprob>1'
	 stop
      endif
      call ini055(isol,kprob,indprf,indprh,indprp,nphys,'pecopy')
      iindprp = infor(1,indprp)
      iindprh = infor(1,indprh)
      iisol   = infor(1,isol(1))
      nusol   = kprob(5)
      nunkp   = kprob(4)
      npoint  = nusol/nunkp
      if (ichois.eq.21) then
        call pecop3(ichois,nusol,ibuffr(iisol),ibuffr(iindprp),
     v              user(istart),factor,npoint)
      else if (ichois.lt.10) then
        call pecop1(ichois,nusol,ibuffr(iisol),ibuffr(iindprh),indprh,
     v              user(istart),factor,nunkp)
      else
	  nphys  = kprob(33)
	  nusol  = kprob(5)
	  if (ichois-9.gt.nphys.or.nphys.le.1) then
	     write(6,*) 'PERROR(pecopy): ichois too large'
          endif
	  call ini050(kprob(35),'pecopy: kprob p')
	  ikprbp = infor(1,kprob(35))
	  call pecop2(ichois-10,ibuffr(iisol),ibuffr(ikprbp),
     v                user(istart),factor,nusol,nphys)
       endif
      end

      subroutine pecop2(ichois,usol,kprobp,user,factor,nusol,nphys)
      implicit double precision(a-h,o-z)
      dimension usol(*),kprobp(*),user(*)

      npoint = nusol/nphys
      do 10 i=1,npoint
	 iof = ichois*npoint + i
	 user(i) = factor*usol(kprobp(iof))
10    continue

      return
      end

      subroutine pecop1(ichois,nusol,usol,kprobh,indprh,user,
     v                  factor,nunkp)
      implicit double precision (a-h,o-z)
      dimension usol(*),kprobh(*),user(*)

      if (factor.eq.0d0) factor=1d0
      if(ichois.eq.0) then
         if(indprh.eq.0) then
            do 100 i=1,nusol
100            user(i)=factor*usol(i)
         else
            do 110 i=1,nusol
110            user(i)=factor*usol(kprobh(i))
         endif
      else if(ichois.eq.2 .or. ichois.eq.3) then
         if(indprh.eq.0) then
            do 150 i=ichois-1,nusol,2
150            user((i+1)/2)=factor*usol(i)
         else
            do 160 i=ichois-1,nusol,2
160            user((i+1)/2)=factor*usol(kprobh(i))
         endif
      else if (ichois.eq.21) then
         if (indprh.eq.0) then
            do 170 i=1,nusol
170                usol(i) = user(i)
         else 
            do 180 i=1,nusol/2
               ip = 2*(kprobh(i)-1)
               usol(1+ip) = user((i-1)*2+1)
               usol(2+ip) = user(i*2)
180         continue
         endif
      else if (ichois.ge.10) then
	 if (indprh.eq.0) then
            do 200 i=ichois-9,nusol,nunkp
200            user((i+nunkp-1)/nunkp)=factor*usol(i)
         else
	    do 210 i=ichois-9,nusol,nunkp
210            user((i+nunkp-1)/nunkp)=factor*usol(kprobh(i))
         endif
      endif
      end

      subroutine pecop3(ichois,nusol,usol,kprobp,user,factor,npoint)
      implicit double precision(a-h,o-z)
      dimension usol(*),kprobp(npoint,2),user(2,*)
    
      do 10 i=1,npoint
         do 20 it=1,2
            usol(kprobp(i,it)) = user(it,i)
20       continue
10    continue

      return
      end
