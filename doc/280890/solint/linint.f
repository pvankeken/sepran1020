      subroutine linint(tn,phi,nphys,user)
      implicit double precision(a-h,o-z)
      dimension tn(nphys,*),phi(*),user(nphys),p(6)

      do 10 idof=1,nphys
         t = 0
         do 20 ikn=1,3
            t  = t + tn(idof,ikn)*phi(ikn)
20       continue
         user(idof) = t
10    continue
      return
      end
