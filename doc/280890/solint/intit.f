      subroutine intit(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,ichois)
      implicit double precision(a-h,o-z)
      parameter(NMAX=10000)
      dimension kmesh1(*),kmesh2(*),kprob1(*),kprob2(*)
      dimension isol1(*),isol2(*),user(NMAX)
      
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)

      user(1) = NMAX
      call ini050(kmesh2(23),'intit: coordinates')
      call ini055(isol1,kprob1,indprf,indprh,indprp,nphys,'intit')
      npnt2  = kmesh2(8)
      nphys  = kprob2(33)
      if (nphys*npnt2.gt.NMAX) then
         write(6,*) 'PERROR(intit): number of points in second'
         write(6,*) 'mesh too large: ',npnt2
         stop
      endif
      ikelmi = infor(1,kmesh2(23))
      call int01(ibuffr(ikelmi),kmesh1,kprob1,isol1,user,npnt2,ichois)
      if (nphys.eq.1) then
         call copyuv(1,isol2,user,kprob2,6)
      else if (nphys.eq.2) then
         call pecopy(21,isol2,user,kprob2,6,1d0)
      endif

      return 
      end

