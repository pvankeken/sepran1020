c *************************************************************
c *   INT02
c * 
c *   Determines nodalpoints values, coordinates and solution in
c *   the nodalpoints of element iel
c *
c *   PvK 170590
c *************************************************************
      subroutine int02(kmeshc,coor,usol,kprobh,indprh,xn,yn,
     v                  tn,ielem,nphys,inpelm)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),kprobh(*),xn(*),yn(*),tn(*)
      dimension usol(nphys,*),tn(nphys,*)
      dimension nodno(6)
 
      
      ip = (ielem-1)*inpelm
c     write(6,*) 'ip,nphys,inpelm: ',ip,nphys,inpelm
c     *** nodalpoint numbers in kmesh part c
      do 10 i=1,inpelm
10        nodno(i) = kmeshc(ip+i)
c     print *,(nodno(i),i=1,3)
c     *** coordinates in coor
      do 20 i=1,inpelm
         xn(i) = coor(1,nodno(i))
20       yn(i) = coor(2,nodno(i))
c     print *,'x: ',(xn(i),i=1,3)
c     print *,'y: ',(yn(i),i=1,3)
c     *** solution in usol
      do 30 i=1,inpelm
         if (indprh.eq.0) then
            do 31 it = 1,nphys
               tn(it,i) = usol(it,nodno(i))
31          continue
         else 
            do 32 it=1,nphys
               ip = kprobh(nodno(i))
               tn(it,i) = usol(it,ip)
32          continue
         endif
30     continue
       return
       end
