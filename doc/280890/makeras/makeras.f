      program makeras
      implicit double precision(a-h,o-z)
      parameter(NMARX=5000,NMAX=160000)
      dimension coormark(2*NMARX)
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /colmar/ icol(10)
      character*80 rname,fname,ext
      
      read(5,*) fname,rname
c     **** FNAME      file containing the coordinates of the
c     ****            markerchains
c     **** RNAME      basename of outputfiles. Images are 
c     ****            generated for each timestep and written
c     ****            to file RNAME.001, RNAME.002 etc.

      read(5,*) istart,iend,istride,nxpix
c     **** NOIMAGE    Number of images to be created
c     **** NXPIX      Dimension of image in x-direction
c     **** NYPIX      Dimension of image in y-direction

      read(5,*) nl,(icol(j),j=1,nl)
c     **** NL         Number of layers (= nochain+1)
c     **** ICOL       Array containing the color numbers for each layer

      if (nxpix*nypix.gt.NMAX) then
         write(6,*) 'PERROR(makeras): no of pixels'
         write(6,*) 'too large: ',nxpix*nypix
         stop
      endif
      open(9,file=fname,form='unformatted')
      rewind(9)
      do 5 im=1,istart-1
         read(9) t,nochain,(imark(ichain),ichain=1,nochain)
         ntot =0
         do 6 ichain=1,nochain
6           ntot = ntot+imark(ichain)
         read(9) (coormark(i),i=1,2*ntot)
5     continue
      do 10 im=istart,iend,istride
         read(9) t,nochain,(imark(ichain),ichain=1,nochain)
         write(6,*) t,nochain,imark(1)
         write(ext,'(i4)') 1000+im
         nc = lnblnk(rname)
         fname = rname(1:nc) // '.' // ext(2:4)
         ntot = 0
         do 11 ichain=1,nochain
            ntot = ntot+imark(ichain)
11       continue
         write(6,*) ntot
         read(9) (coormark(i),i=1,2*ntot)
         rlam = coormark(1)
         nypix = nxpix/rlam
         write(6,*) fname,nxpix,nypix
         call marras(coormark,fname,-1)
         if (im+istride.gt.iend) stop
         do 20 ik=1,istride-1
           read(9) t,nochain,(imark(ichain),ichain=1,nochain)
           ntot =0
           do 21 ichain=1,nochain
21             ntot = ntot+imark(ichain)
             read(9) (coormark(i),i=1,2*ntot)
20       continue
10    continue
      end

      function lnblnk(fname)
      character*(*) fname
  
      nl = len(fname)
      do 10 i=nl,1,-1
         if (fname(i:i).ne.' ') then
            lnblnk = i
            return
         endif
10    continue
      lnblnk = 1
      end
