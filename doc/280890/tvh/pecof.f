c *************************************************************
c *   PECOF
c *  
c *   Fill coefficients for Sepran problems in arrays user
c *
c *   Ichois=1    Stokes equation (primitive variables) type 400
c *
c *   Ichois=2    Temperature equation (type 100 etc)
c *
c *   PvK 160290
c *************************************************************
      subroutine pecof(ichois,kmesh,kprob,isol,iuser,user)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*),iuser(*),user(*)
      common /peparam/ rayleigh,rlam,idia
      common /peheat/ q
	
      npoint = kmesh(8)
      do 10 i=6,20
10       iuser(i) = 0
      do 20 i=6,10+2*npoint
20       user(i)  = 0d0

      if (ichois.eq.1) then
c        *** Stokes equation
c        *** eps, rho, MCONV, omega, f1, f2, MODELV, eta
	 iuser(6) = 7
	 iuser(7) = -6
	 iuser(8) = -7
	 iuser(9) = 0
	 iuser(10)= 0
	 iuser(11)= 0
	 iuser(12)= 2001
	 iuser(13)= 10
	 iuser(14)= 1
	 iuser(15)= -7
	  user(6) = 1d-6
	  user(7) = 1d0
	  call pecopy(0,isol,user,kprob,10,rayleigh)
       else if (ichois.eq.2) then
c         *** Temperature equation
c         *** a11,a12,a22,u,v,b,f,rhocp
	  iuser(6) = 7
	  iuser(7) = -6
	  iuser(8) = 0
          iuser(9) = -6
	  iuser(10)= 2001
	  iuser(11)= 10
	  iuser(12)= 2001
	  iuser(13)= 10+npoint
	  iuser(14)= 0
	  iuser(15)= -7
	  iuser(16)= -6
	   user(6) = 1d0
	   user(7) = q
          call pecopy(2,isol,user,kprob,10,1d0)
	  call pecopy(3,isol,user,kprob,10+npoint,1d0)
       else
	  write(6,*) 'PERROR(pecof): unknown option: ',ichois
	  stop
       endif

       return
       end

