c *************************************************************
c *   PEDTCF
c *
c *   Determines CFL timestep
c *   PvK 040390
c *************************************************************
      subroutine pedtcf(kmesh,user,dtcfl)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),user(*)
      common ibuffr(1)
      common /carray/ iinfor,infor(3,1500)

      kelmc = kmesh(17)
      kelmi = kmesh(23)
      nelem = kmesh(9)
      npoint = kmesh(8)
      call ini050(kelmc,'pedtcf')
      call ini050(kelmi,'pedtcf')
      ikelmc = infor(1,kelmc)
      ikelmi = infor(1,kelmi)
      dtcfl = 1e30
      call pedtc1(nelem,ibuffr(ikelmc),ibuffr(ikelmi),user(10),
     v            user(10+npoint),dtcfl)
      return
      end

      subroutine pedtc1(nelem,kmeshc,coor,xvel,yvel,dtcfl)
      implicit double precision(a-h,o-z)
      dimension kmeshc(*),coor(2,*),xvel(*),yvel(*)
      dimension u(4),v(4),index(4)

      do 10 i=1,nelem
         ipc = 3*(i-1)+1
	 do 20 j=1,3
20          index(j) = kmeshc(ipc+j-1)
         dx1 = abs(coor(1,index(1)) - coor(1,index(2)))
         dy1 = abs(coor(2,index(2)) - coor(2,index(3)))
         dx2 = abs(coor(1,index(2)) - coor(1,index(3)))
         dy2 = abs(coor(2,index(1)) - coor(2,index(2)))
	 dx = max(dx1,dx2)
	 dy = max(dy1,dy2)
	 do 30 j=1,3
            u(j) = xvel(index(j))
	    v(j) = yvel(index(j))
	    x = coor(1,index(j))
	    y = coor(2,index(j))
	    if (abs(u(j)).gt.1e-7.and.abs(v(j)).gt.1e-7) then
	        udx = dx/abs(u(j))
	        vdy = dy/abs(v(j))
                dtcfl = min(dtcfl,udx,vdy)
            endif
30       continue
10    continue
      return
      end

