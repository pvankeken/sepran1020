c *************************************************************
c *   PEDETS
c *
c *   Determine CFL criterion from velocity field and grid
c *   parameters
c *
c *   Pvk 160290
c ************************************************************** 
      subroutine pedets(kmesh,kprob,isol,dtcfl)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),isol(*)
      common /pegrid/ dxm,dym

      call algebr(6,1,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
      umax = max(abs(a),abs(b))
      tx = umax/dxm
      call algebr(6,2,isol,idum,idum,kmesh,kprob,a,b,p,q,i)
      vmax = max(abs(a),abs(b))
      ty = vmax/dym
      txy = max(tx,ty)
      dtcfl = 1d0/txy

      return
      end
