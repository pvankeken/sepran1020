c *************************************************************
c *   DETPER
c *
c *   Determine times and extrema in a timeseries  a=a(t)
c *
c *************************************************************
      program detper
      implicit double precision(a-h,o-z)
      parameter(NUM=10000)
      dimension t(NUM),a(NUM)
      
      read(5,*) itype,ny
      read(5,*) (t(i),a(i),i=1,ny)

      do 10 i=1,ny-2
         dt1 = a(i+1) - a(i) 
         dt2 = a(i+2) - a(i+1)
         if (dt1*dt2.eq.0) then
            write(6,'(2f15.7)') 0.5*(t(i)+t(i+1)),a(i+1)
            i=i+1
         endif
         if (dt1*dt2.lt.0) then
            call quadet(t(i),a(i),te,ae)
            write(6,'(2f15.7)') te,ae
         endif
10    continue

      return
      end

c *************************************************************
c *   QUADET
c *
c *   Determine maximum of y(x) using quadratic interpolation
c *   The position of y'(x) = 0 is determined
c *************************************************************
      subroutine quadet(x,y,xm,ym)
      implicit double precision(a-h,o-z)
      dimension x(3),y(3)

      a1 = x(1)
      a2 = x(2)
      a3 = x(3)
      f1 = y(1)
      f2 = y(2)
      f3 = y(3)

      rt = (a2+a3)*(a2-a3)*f1 - (a1+a3)*(a1-a3)*f2 + (a1+a2)*(a1-a2)*f3
      rn = 2*(a2-a3)*f1 - 2*(a1-a3)*f2 + 2*(a1-a2)*f3
      xm = rt/rn

      rl1 = (xm-a2)*(xm-a3)/((a1-a2)*(a1-a3))
      rl2 = (xm-a1)*(xm-a3)/((a2-a1)*(a2-a3))
      rl3 = (xm-a1)*(xm-a2)/((a3-a1)*(a3-a2))

      ym = rl1*f1 + rl2*f2 + rl3*f3

      return
      end
