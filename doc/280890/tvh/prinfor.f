c *************************************************************
c *   PRINFOR
c *
c *   writes information on arrays stored in the Sepran datastructure
c *
c *************************************************************
      subroutine prinfor(index)
      implicit double precision(a-h)
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)

      jindex = abs(index)
      if (jindex.eq.0) return
      if (jindex.gt.iinfor) then
         write (6,10) nbuffr,kbuffr,intlen,ibfree
10       format('cbuffr: ',4i10)
         write (6,11) iinfor
11       format('carray: number of arrays = ',i10)
         do 100 i=1,iinfor
            write (6,12) i,infor(1,i),infor(2,i),infor(3,i)
100      continue
      else
	 write(6,12) jindex,infor(1,jindex),infor(2,jindex),
     v               infor(3,jindex)
      endif
12    format(i4,': ',3i10)
      return
      end
