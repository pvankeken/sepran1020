      subroutine pewrst(iref)
      common /peback/ irefp,istartp,nrec,nsol
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      open(iref,file='bsfile',access='direct',recl=1000)
      irefp=iref
      istartp=1
      nrec = 125
      nsol = 0
      return
      end

      subroutine pewrit(kmesh,kprob,isol)
      dimension kmesh(*),kprob(*),isol(*)
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)
      npoint = kmesh(8)
      nphys = kprob(4)
      call ini050(isol(1),'pewrit')
      istart = infor(1,isol(1))
      call pewrt1(npoint,ibuffr(istart),nphys)
      return
      end

      subroutine pewrt1(npoint,usol,nphys)
      common /peback/ irefp,istartp,nrec,nsol
      dimension usol(nphys*npoint)
      n = nphys*npoint/nrec +1
      iho=1
      do 100 i=1,n
	iht = min(nphys*npoint,iho+nrec-1)
	write(irefp,rec=istartp+i-1)
     v        (usol(k),k=iho,iht),((j-j),j=iht-iho+2,nrec)
	iho = iht+1
100   continue
      istartp = istartp+n
      nsol = nsol+1
      return
      end


      subroutine peread(kmesh,kprob,isol,nr)
      dimension kmesh(*),kprob(*),isol(*)
      common ibuffr(1)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/ iinfor,infor(3,1500)

      if (nr.gt.nsol) then
	 write(6,*) 'PERROR(peread): nr > nsol'
	 return
      endif
      npoint = kmesh(8)
      nphys = kprob(4)
      length = npoint*nphys
      call ini051(isol(1),length,'peread')
      istart = infor(1,isol(1))
c     *** first record
      istartp = (nr-1)*(npoint*nphys/nrec+1)
      call perd1(npoint,ibuffr(istart),nphys)
      return
      end

      subroutine perd1(npoint,usol,nphys)
      common /peback/ irefp,istartp,nrec,nsol
      dimension usol(nphys*npoint)
      n = nphys*npoint/nrec +1
      iho=1
      do 100 i=1,n
       iht = min(nphys*npoint,iho+nrec-1)
       irec = istartp+i-1
       read(irefp,rec=irec) (usol(k),k=iho,iht)
       iho = iht+1
100   continue
      return
      end
