/* Multimax version; time is given in 50 tics per second */
#include <sys/types.h>
#include <sys/times.h>
timeus_(ctime) 
float *ctime;
{
	struct tms buffer;
        int utime, stime;
	times(&buffer);
	utime = buffer.tms_utime;
        *ctime = utime/50.;
}

timesys_(ctime) 
float *ctime;
{
	struct tms buffer;
        int utime, stime;
	times(&buffer);
	stime = buffer.tms_stime;
        *ctime = stime/50.;
}
