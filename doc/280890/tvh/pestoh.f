c *************************************************************
c *   PESTOH
c *
c *   Solves the Stokes equation
c *************************************************************
      subroutine pestoh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      dimension matrs(1),intmat(1),kmesh(1),kprob(1),isol(1)
      dimension iuser(1),user(1),matrback(5)
      dimension irhsd(5)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /carray/iinfor,infor(3,1500)
      common ibuffr(1)
      character*10 tname

      save ifirst

      data ifirst/0/

      if (ifirst.eq.0) then
         call systm0(1,matrs,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
         call copymt(matrs,matrback,kprob)
      else
         call systm0(2,matrback,intmat,kmesh,kprob,irhsd,isol,
     v               iuser,user,islold,ielhlp)
      endif
12    format(3i10)
      call solve(1,matrs,isol,irhsd,intmat,kprob)

      if (ifirst.eq.0) then
         call pein56(matrback(3),0)
         call peini3(ibuffr)
	 ifirst=1
      endif

      return
      end
      
