c *************************************************************
c *   REMARKER
c *   
c *   Regrid the markerchain. The length of the markerchain
c *   is computed and markers are distributed on the chain
c *   with a maximum distance DMAX. If more than NMAX markers
c *   are necessary this condition is relaxed and exactly NMAX
c *   markers are distributed on the chain.
c *   The position of the new markers is obtained by a linear
c *   interpolation of the old ones.
c *
c *   PvK 211189
c *************************************************************
      subroutine remarker(imark,cm,cn,dmax,nmax)
      implicit double precision(a-h,o-z)
      dimension imark(*),cm(2,*),cn(2,*)
      common /marker/ nmchain

      if (nmchain.gt.1) return

      nmark = imark(1)
      rl = 0
      do 10 i=2,nmark
         dx = cm(1,i-1) - cm(1,i)
         dy = cm(2,i-1) - cm(2,i)
10       rl = rl + sqrt(dx*dx+dy*dy)
      nsegnew = rl/dmax
      err  = rl-nsegnew*dmax
      if (err.gt.dmax) stop 'Foutje in err'
      
      nnew = nsegnew + 1
      if (nnew+1.gt.nmax) then
         write(6,*) 'PWARNING(remarker): nnew > nmax'
         nnew = nmax-1
         dnew = rl/nnew
      else 
         dnew = dmax
      endif
      
      cn(1,1) = cm(1,1)
      cn(2,1) = cm(2,1)
      t = 0
      x1 = cm(1,1)
      y1 = cm(2,1)
      x2 = cm(1,2)
      y2 = cm(2,2)
      s1 = 0
      s2 = sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
      d = s2-s1
      i=1
      j=2
20    continue
        t = t+dnew
30      continue
        if (t.lt.s2) then
c          *** interpolate
           xt = x1 + (t-s1)/d*(x2-x1)
           yt = y1 + (t-s1)/d*(y2-y1)
           i = i+1
           cn(1,i) = xt
           cn(2,i) = yt
        else
c          *** new segment
           j = j+1
           x1 = x2
           y1 = y2
           x2 = cm(1,j)
           y2 = cm(2,j)
           s1 = s2
           s2 = s1 + sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
           d = s2-s1
           goto 30
        endif
        if (i.eq.nnew) then
           cn(1,nnew+1) = cm(1,nmark)
           cn(2,nnew+1) = cm(2,nmark)
           imark(1) = nnew+1
        else
           goto 20
        endif
   
        return 
        end

       
       
