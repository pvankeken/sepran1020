c *************************************************************
c *   ELEM
c *
c *   Element matrices 
c *************************************************************
      subroutine elem(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      parameter(done=1d0)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint

      if (itype.ge.3.and.itype.le.9) then
         call pelem3(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      else if (itype.ge.10.and.itype.le.11) then
         call pelem10(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      endif

      return
      end
