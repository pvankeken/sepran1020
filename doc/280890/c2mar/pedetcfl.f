c *************************************************************
c *   PEDETCFL
c *
c *   Determine value of maximum timestep according to the
c *   CFL-criterium. 
c *
c *   dt(cfl) = min(dx/vx,dy/vy)
c *
c *************************************************************
      subroutine pedetcfl(imark,velmark)
      implicit double precision(a-h,o-z)
      dimension imark(*),velmark(2,*)
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pexymin/ dxmin,dymin
      common /marker/ nmchain
  
      do 30 k=1,nmchain
	 if (k.eq.1) then
	    iof = 0
         else 
	    iof = iof+imark(k-1)
         endif
         nmark = imark(k)
         vxmax = abs(velmark(1,iof+1))
         vymax = abs(velmark(2,iof+1))
         do 10 im=2,nmark
            vxmax = max(vxmax,abs(velmark(1,iof+im)))
10          vymax = max(vymax,abs(velmark(2,iof+im)))
         dtcfl = min(dxmin/vxmax,dymin/vymax)
30    continue
     
      return
      end
