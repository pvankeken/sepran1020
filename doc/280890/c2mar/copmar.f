c *************************************************************
c *   COPMAR
c *
c *   PvK 231189
c *************************************************************
      subroutine copmar(imark,coor1,coor2)
      implicit double precision(a-h,o-z)
      dimension imark(*),coor1(2,*),coor2(2,*)
      common /marker/ nmchain
      do 30 k=1,nmchain
	 if (k.eq.1) then
	    iof=0 
         else
	    iof = iof+imark(k-1)
         endif
         do 10 i=iof+1,iof+imark(k)
            coor2(1,i) = coor1(1,i)
10          coor2(2,i) = coor1(2,i)
30    continue

      return
      end
