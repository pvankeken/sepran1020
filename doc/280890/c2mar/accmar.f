c *************************************************************
c *   ACCMAR
c *
c *   ichois = 1   : maximum difference
c *   ichois = 2   : rms difference
c *
c *   PvK 231189
c *************************************************************
      function accmar(ichois,imark,coor1,coor2)
      implicit double precision(a-h,o-z)
      dimension imark(*),coor1(*),coor2(*)

    
      nmark=imark(1)
      if (ichois.eq.1) then
         accmar = 0
         do 10 i=1,2*nmark
            d = abs(coor1(i)-coor2(i))
10          accmar = max(accmar,d)
      else if (ichois.eq.2) then
         d = 0
         do 20 i=1,2*nmark
           d = coor1(i) - coor2(i)
           sum = sum + d*d
20       continue
         accmar = sqrt(sum)
      else
         write(6,*) 'PERROR(accmar): unknown option ',ichois
         stop
      endif
      return
      end
         
