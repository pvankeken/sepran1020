c *************************************************************
c *   C2START
c *
c *   Starts C2MAR
c *
c *   PvK 040490
c *************************************************************
      subroutine c2start(kmesh,kprob,istrm,intmat,imark,
     v                   coormark,y0,dm,nbufdef)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),imark(*)
      dimension coormark(*),iu1(1),u1(1),y0(*),dm(*),wavel(2),ainit(2)
      common /cbuffr/ nbuffr,kbuffr,intlen,ibfree
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peinterg/ nrule
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /peiter/ tmax,dtcfl,tfac,tstepmax,tvalid,difcor,difcormax,
     v                nitermax,nout,ncor
      common /pelinqua/ itypel,ishape
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1dens/ dens(10)
      common /marker/ nmchain
      character*80 fnre(2)

      read(5,*) itypel,nrule
      read(5,*) nmchain
      do 9 i=1,nmchain
9        read(5,*) imark(i),y0(i),wavel(i),ainit(i),dm(i)
      nl = nmchain+1
      read(5,*) nitermax,nout,ncor
      read(5,*) tfac,tmax,tstepmax,tvalid
      read(5,*) (viscl(i),i=1,nl)
      read(5,*) (dens(i),i=1,nl)
      read(5,*) ctd,cpd,itypv
      read(5,*) irestart,trestart,(fnre(i),i=1,nmchain)

c     *** standard Sepran start
      call start(0,1,0,0)
      nbuffr = nbufdef
      call mesh(0,iinput,rinput,kmesh)
      call probdf(0,kprob,kmesh,iinput)
      call presdf(kmesh,kprob,istrm)
      call commat(1,kmesh,kprob,intmat)
      nsuper = kprob(29)
      write(6,'(''nsuper = '',i2)') nsuper
      if (nsuper.gt.0) stop

c     *** initial condition for markerchain and streamfunction
      iu1(1) = 0
       u1(1) = 0
      call creavc(0,1,ivec,istrm,kmesh,kprob,iu1,u1,iu2,u2)
      if (itypel.eq.5) then
c        *** rectangular element with four nodalpoints
	 ishape = 1
      endif
      call pefilxy(ishape,kmesh,kprob,istrm)

      if (irestart.eq.0) then
	 t = 0d0
	 do 10 i=1,nmchain
	   if (i.eq.1) iof=0
	   if (i.eq.2) iof=imark(1)
           rm1 = (xcmax-xcmin)/(imark(i)-1)
           piw = 3.1415926/wavel(i)
           do 20 j=1,nmark
              x = rm1*(imark(i)-j)
              y = y0(i) + ainit(i)*cos(piw*x)
              coormark(2*j+iof-1) = x
              coormark(2*j+iof)   = y
20         continue
10       continue
      else
	 do 11 i=1,nmchain
	    if (i.eq.1) iof=0
	    if (i.eq.2) iof=imark(1)
            open(9,file=fnre(i))
            rewind(9)
            read(9,*) itype,imark(i)
            do 30 j=1,imark(i)
30             read(9,*) coormark(2*j+iof-1),coormark(2*j+iof)
11       continue
         t = trestart
      endif

      return
      end
