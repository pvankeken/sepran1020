c *************************************************************
c *   MAROUT
c *
c *   Post processing of solution vectors obtained by STRMAR
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marout(kmesh,kprob,istrm,imark,
     v                  coormark,iuser,user)
      parameter(NUM=2000)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),imark(*),coormark(*)
      dimension iuser(*),user(*),xmc(NUM),ymc(NUM)
      character pname*80,chrbf*4
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /marker/ nmchain

      nmark = imark(1)
      if (nmark.gt.NUM) then
         write(6,*) 'PERROR(marout): nmark too large'
         stop
      endif
 
c     *** Plot preparations
      iway=2
      fname='PLOT'
      yfaccn = 1
      jkader=-4
      format=6d0
      chheight = 0.3d0
      call frstack(0)
      write(tittext,'(''t= '',f12.3)') t
      call peframe(1)
c     **** Plot velocity
      call plotvc(2,3,istrm,istrm,kmesh,kprob,format,yfaccn,0d0)
      do 14 k=1,nmchain
	 if (k.eq.1) then
	    iof = 0
	    ic  = 2
         else if (k.gt.1.and.k.lt.nmchain) then
	    iof = iof+imark(k-1)
	    ic  = 2
         else if (k.eq.nmchain) then
	    iof = iof+imark(k-1)
	    ic  = 3
         endif
         do 10 i=1,imark(k)
            xmc(i) = coormark(iof+2*i-1)
10          ymc(i) = coormark(iof+2*i)
         call peframe(ic)
         call pecurvep(nmark,xmc,ymc,format,yfaccn)
14    continue
      velxmax = anorm(1,3,2,kmesh,kprob,istrm,istrm,ielhlp)
      velymax = anorm(1,3,3,kmesh,kprob,istrm,istrm,ielhlp)
      strmax  = anorm(1,3,1,kmesh,kprob,istrm,istrm,ielhlp)

      write(6,1000) velxmax,velymax,strmax
1000  format(/,'vxmax = ',f15.7,/,'vymax = ',f15.7,/,
     v         'strmax= ',f15.7)
 
      write(chrbf,'(i4)') 1000+ifrcntr
      pname='p.'//chrbf(2:4)
      open(9,file=pname)
      rewind(9)
      write(9,*) 2,imark(1)
      do 20 i=1,imark(1)
20       write(9,*) coormark(2*i-1),coormark(2*i)
      
      
      return
      end
