      program tmar
      implicit double precision(a-h,o-z)
      parameter(NBUFDEF= 2 000 000)
c **********************************************************
c *   TMAR
c *
c *   Solve the equation of motion for a RT instability modelled
c *   with the markerchain method, in the streamfunction 
c *   formulation using THE C1 nonconforming element.
c *   An initial temperature field is convected.
c *
c *   PvK 040490
c **********************************************************************
      parameter(NCMAX=2,NMARMAX=4000,NPMAX=10000)
      parameter(NUM1=NMARMAX+10,NUM2=2*NPMAX+10,NUM3=NCMAX*NMARMAX)
c     *** NCMAX = Maximum number of markerchains
c     *** NMARMAX = Maximum number of markers on one chain
c     *** NPMAX   = Maximum number of nodalpoints
c     *** NUM1    = length of array iuser   = 10 + NMARMAX
c     *** NUM2    = length of array  user   = 10 + 2*NPMAX
c     *** NUM3    = total number of markers = NCMAX*NMARMAX
      common /carray/ iinfor,infor(1,1500)
      common ibuffr(NBUFDEF)
      dimension kmesh1(100),kprob1(100),isol1(5),intmt1(5)
      dimension kmesh2(100),kprob2(100),isol2(5),intmt2(5)
      dimension matrm(5),islol2(5)
      dimension islold(5),irhs2(5),irhs1(5),matr1(5),matr2(5)
      dimension iu1(2),u1(2),ivec1(5),ivec2(5),isolan(5),ipredsol(5)
      dimension icurvs(2),funcx(100),funcy(100)
      dimension iuser(NUM1),user(NUM2)
      dimension coormark(2*NUM3),coornewm(2*NUM3)
      dimension velmark(2*NUM3),velnewm(2*NUM3),cooroldm(2*NUM3)
      dimension y0(10),dm(10),rk(10),h(10),igradt(5)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /peiter/ tmax,dtout,dtcfl,tfac,tstepmax,tvalid,difcor,
     v                difcormax,nitermax,nout,ncor
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
      common /peparam/ rayleigh,rlam,visc0,b,c,q
      character*80 rname
      real t0,t1,t2
      data iuser(1),user(1),kmesh1(1),kprob1(1)/NUM1,NUM2,2*100/
      data kmesh2(1),kprob2(1)/2*100/

      call tstart(kmesh1,kprob1,isol1,intmt1,kmesh2,kprob2,isol2,intmt2,
     v             coormark,y0,dm,NBUFDEF,NPMAX)
      write(6,'(''nochain     = '',i3)') nochain
      write(6,'(''imark       = '',10i3,:)' ) (imark(i),i=1,nochain)
      write(6,'(''Rb, Ra      = '',2f12.3)') ram,rat
      write(6,'(''ism,ist     = '',2i5)') ism,ist
      write(6,'(''ibm,ibt     = '',2i5)') ibm,ibt
      write(6,'(''wm          = '',10f8.3,:)') (wm(i),i=1,nochain)
      write(6,'(''itypv       = '',i5)') itypv
      write(6,*) '   ivl,ivt,ivn = ', ivl,ivt,ivn
      write(6,'(''visco       = '',10f8.3,:)') (viscl(i),i=1,nochain)
      write(6,'(''t0          = '',f12.5)') t
      write(6,'(''output every  '',f12.5)') dtout


      niter = 0
      inout = 0
      tout = t+dtout
      call second(t0)
100   continue
	 niter = niter+1
	 inout = inout+1
         call second(t1)
c        ************** PREDICTOR ***************
c        ******* Predict velocity field  ********
         call prepvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,coormark,
     v                iuser,user)
         call tsolmar(kmesh1,kprob1,isol1,intmt1,matr1,iuser,user,
     v                coormark)

         call tdetvel(kmesh1,kprob1,user,coormark,velmark)
c        *** determine growth factor at t=0
         if (ism.eq.1.or.ibm.eq.1) then
           ip = 0
           do 101 ichain = 1,nochain
                 nmark = imark(ichain)
                 rk(ichain) = velmark(ip+2*nmark)/(coormark(ip+2*nmark)
     v                       -y0(ichain))
                 ip = ip+2*nmark
101         continue
            cpu1 = t2-t1
	    if (niter.eq.1) write(6,'(''rk1,cpu '',f12.9,f12.2)') 
     v                            rk(1),cpu1
         endif
c        ********* Determine timestep ***********
         call tmstep(kmesh1,kmesh2,kprob1,isol1,coormark,velmark,
     v               iuser,user,dtm,dtt)

c        ****************** PREDICTOR ***************************
         if (ism.eq.1) then
c            ***** Predict positions of markers *****
             call predcoor(coormark,velmark,coornewm)
         endif
         if (ist.eq.1) then
c            ****** Predict temperature field *******
             call ieulh(kmesh2,kprob2,intmt2,isol2,islol2,user,
     v                  iuser,matr2,matrm,irhs2)
             call copyvc(isol2,ipredsol)
         endif


c        ************** CORRECTOR ****************
         do 10 i=1,ncor
c          ******* Correct velocity field ********
          call prepvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,coormark,
     v                 iuser,user)
          call tsolmar(kmesh1,kprob1,isol1,intmt1,matr1,iuser,user,
     v                  coornewm)
           if (ism.eq.1) then
c             ******* correct position of markers ***
              call tdetvel(kmesh1,kprob1,user,coornewm,velnewm)
              call copmar(coornewm,cooroldm)
              call corcoor(coormark,coornewm,velmark,velnewm)
	      difcor1=accmar(1,coornewm,cooroldm)
	      difcor2=accmar(2,coornewm,cooroldm)
           endif
           if (ist.eq.1) then
c             ****** correct temperature field ******
              call bmcof(2,kmesh1,kmesh2,kprob1,isol1,iuser,user)
              call corrh(kmesh2,kprob2,intmt2,isol2,islol2,
     v                   user,iuser,matr2,matrm,irhs2)
              difcor3=pedetacc(kmesh2,kprob2,islol2,ipredsol,isol2)
           endif
10       continue
         write(20,*) t+tstep,dtm,dtt,difcor1,difcor3
         call flush(20)

	 if (ism.eq.1.or.ibm.eq.1) call copmar(coornewm,coormark)
c        **** REMARKER *******
         istr = 1
	 if ((ism.eq.1.or.ibm.eq.1).and.(niter/istr)*istr.eq.niter) then
           call remarker(coormark,coornewm,dm,NMARMAX)
           call copmar(coornewm,coormark)
           write(6,*) 'remarker: ',nochain,(imark(i),i=1,nochain)
           write(6,*) 'cm      : ',coormark(1),coormark(2*imark(1)-1)
	 endif
	 call second(t2)
	 dcpu = t2-t0

	 t = t+tstep

c        **** OUTPUT ********
         if (ist.eq.1) then
            call chtype(1,kprob2)
            call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v                  isol2,iuser,user,ielhlp)  
            temp1  = bounin(1,1,1,1,kmesh2,kprob2,1,1,isol2,iuser,
     v                   user)
            gnus1  = -bounin(2,1,1,1,kmesh2,kprob2,3,3,igradt,iuser,
     v              user)/temp1
         endif
         strmax = anorm(1,3,1,kmesh1,kprob1,isol1,isol1,ielhlp)
         vrms2 = c1vrms(isol1,kmesh1,kprob1,iuser,user)/rlam
         vrms = sqrt(vrms2)
         h(1) = coormark(2*nmark)
         if (ism.eq.1) then
            write(6,1000) niter,t,h(1)-y0(1),rk(1),dcpu
         endif
         if (ist.eq.1) then
            write(6,1100) niter,t,gnus1,dcpu
         endif
         write(21,*) t,gnus1,strmax,vrms
         call flush(21)

1000     format('M: ',i3,f12.5,f12.7,f12.7,f12.2)
1100     format('T: ',i3,f12.5,f12.7,f12.2)
         if (t.ge.tout) then
            iplot=1
            tout = tout+dtout
         else
            iplot=0
         endif
         call marout(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,coormark,
     v                  iuser,user,velnewm,iplot)
	 if (niter.lt.nitermax.and.t.lt.tmax) goto 100
	 write(6,'(''Total CPU-time: '',f12.2)') t2-t0
      call frstack(1)
      call finish(0)
      end
