c *************************************************************
c *   CORCOOR
c *
c *   Corrects the position of the markers
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine corcoor(coormark,coornewm,velmark,velnewm)
      implicit double precision(a-h,o-z)
      dimension coormark(2,*),velmark(2,*),velnewm(2,*),coornewm(2,*)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /pexcyc/ xc(200),yc(200),xcmin,xcmax,ycmin,ycmax,nx,ny
      common /c1mark/ wm(10),nochain,ichain,imark(10)

      ip = 0
      do 100 ichain=1,nochain
         nmark = imark(ichain)
         do 10 j=1,nmark
            i = ip+j
            do 10 k=1,2
               coornewm(k,i) = coormark(k,i) + 
     v                tstep*0.5d0*(velmark(k,i)+velnewm(k,i))
            if (coornewm(1,i).lt.xcmin) coornewm(1,i) = xcmin
            if (coornewm(1,i).gt.xcmax) coornewm(1,i) = xcmax
            if (coornewm(2,i).lt.ycmin) coornewm(2,i) = ycmin
            if (coornewm(2,i).gt.ycmax) coornewm(2,i) = ycmax
10       continue
         coornewm(1,ip+1) = xcmax
         coornewm(1,ip+nmark) = xcmin
         ip = ip+nmark
100   continue
 

      return
      end
