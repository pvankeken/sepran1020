c *************************************************************
c *   COPMAR
c *
c *   PvK 231189
c *************************************************************
      subroutine copmar(coor1,coor2)
      implicit double precision(a-h,o-z)
      dimension coor1(*),coor2(*)
      common /c1mark/ wm(10),nochain,ichain,imark(10)

      ntot = 0
      do 10 ichain=1,nochain
10       ntot = ntot + imark(ichain)
      do 100 i=1,2*ntot
         coor2(i) = coor1(i)
100   continue

      return
      end
