c *************************************************************
c *   TSOLMAR
c *
c *   Solves the biharmonical equation
c *
c *          4
c *      grad  psi  =   -Ram*dGAMMA/dx  + Rat*dT/dx
c *
c *   where GAMMA is a stepfunction, of which the position is 
c *   defined by a markerchain and T is the temperature
c *   Ram and Rat indicate the compositional and thermal Rayleigh
c *   numbers resp.
c *   THE nonconforming element is used
c *************************************************************
c *   Parameters:
c *       kmesh   i  Standard sepran array
c *       kprob   i  Standard sepran array: 
c *       intmat  i  Standard sepran array
c *       istrm   o  Solution for psi
c *       matr    o  Stiffness matrix (constant in this problem)
c *       imark   i  Integer information on markerchain
c *       coormark i Coordinates of markers (x1,y1,x2,y2,....)
c *
c *   common /c1solve/ rat,ram,ism,ist,ibm,ibt
c *       ram        Compositional Rayleigh number
c *       rat        Thermal rayleigh number
c *       ism        Flag: (1) advance markerchains
c *       ist        Flag: (1) convect temperature field
c *       ibm        Flag: (1) include compositional buoyancy forces
c *       ibt        Flag: (1) include thermal buouyancy forces
c *
c *   PvK 040490
c *************************************************************
      subroutine tsolmar(kmesh,kprob,istrm,intmat,matr,iuser,user,
     v                  coormark)
      implicit double precision(a-h,o-z)
      dimension kmesh(*),kprob(*),istrm(*),intmat(*),matr(*)
      dimension iuser(*),user(*),coormark(*),islold(5)
      dimension irhsd(5),ius(20),us(10),irhtus(5),matrback(5)
      common /c1visc/ viscl(10),ctd,cpd,itypv,nl
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
      common /bmint/ ityps,itypt,nruls,nrult
      common /peinterg/ nrule
      save ifirst
      data ifirst/0/
      real t0,t1

      npoint = kmesh(8)
      nrule = nruls

c     *** *************** BUILD MATRIX *********************
c     *** Information concerning the viscosity is stored in 
c     *** array user (temperature) and/or commons cpix and
c     *** c1visc, depending on the type of viscosity ITYPV.
c     *** This information has been stored previously by
c     *** subroutine PREPVIS
c     *** ***************************************************
      if (ifirst.eq.0.or.itypv.ne.0) then
c        *** Build matrix; clear rhsd
         ifirst = 1
         call copyvc(istrm,islold)
         call systm0(3,matr,intmat,kmesh,kprob,irhsd,istrm,iuser,
     v               user,islold,ielhlp)
         call copymt(matr,matrback,kprob)
      else
         call systm0(5,matrback,intmat,kmesh,kprob,irhsd,istrm,
     v               iuser,user,islold,ielhlp)
c        *** Don't build matrix; clear rhsd
      endif

c     *** **************** BUILD VECTOR *********************
c     *** A rhs vector is created for each source of buoyancy.
c     *** The final vector is a weighted sum of these vectors
c     *** following the formula
c     ***
c     ***              nochain  
c     ***   f  =  - Ra  sum   w(i)*dGamma(i)/dx  + Ra  dT/dx
c     ***             b i=1                          t
c     ***
c     *** ***************************************************
      if (ibm.eq.1) then
         icrhsd = 1
         iuser(6) = icrhsd
         ip = 0
         do 10 ichain=1,nochain
c           ***   Fill positions of markers in array USER, starting at
c           ***   6+npoint. Fill elementnumber for each marker in IUSER
c           ***   starting at 7.
            if (wm(ichain).eq.0d0) goto 11
            nmark = imark(ichain)
            if (npoint+2*nmark+7.gt.user(1)) then
               stop
            endif
            do 12 i=1,2*nmark
12             user(5+i+npoint) = coormark(i+ip)
c           *** determine the numbers of the elements in which 
c           *** the marker are
            do 20 i=1,nmark
               xm = coormark(ip+2*i-1)
               ym = coormark(ip+2*i)
               call pedetel(2,xm,ym,iel)
               iuser(7+i) = iel
20          continue

c           *** Built intermediate rhs vector and add
            call systm0(12,matr,intmat,kmesh,kprob,irhtus,istrm,iuser,
     v                  user,islold,ielhlp)
            aa = 1d0
            bb = wm(ichain)
            call algebr(3,1,irhsd,irhtus,irhsd,kmesh,kprob,aa,bb,p,q)
11          continue

            ip = ip+2*nmark
10       continue
      endif

      if (ibt.eq.1) then
c        *** The temperature is stored in user(6)...user(5+npoint)
         icrhsd = 3
         iuser(6) = icrhsd
         call systm0(12,matr,intmat,kmesh,kprob,irhtus,istrm,iuser,
     v               user,islold,ielhlp)
         aa = 1d0
         bb = rat
         call algebr(3,1,irhsd,irhtus,irhsd,kmesh,kprob,aa,bb,p,q)
         
      endif


      call solve(1,matr,istrm,irhsd,intmat,kprob)
      factor = 1d0
      npoint = kmesh(8)
      call pecopy(10,istrm,user,kprob,6,factor)
      call pecopy(11,istrm,user,kprob,6+npoint,factor)
      call pecopy(12,istrm,user,kprob,6+2*npoint,factor)
      return
      end
