c *************************************************************
c *   MAROUT
c *
c *   Post processing of solution vectors obtained by STRMAR
c *
c *   PvK 15-11-89
c *************************************************************
      subroutine marout(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                  coormark,iuser,user,velmark,iplot)
      parameter(NUM=4000)
      implicit double precision(a-h,o-z)
      dimension kmesh1(*),kprob1(*),isol1(*)
      dimension kmesh2(*),kprob2(*),isol2(*),coormark(*)
      dimension iuser(*),user(*),xmc(NUM),ymc(NUM),velmark(*)
      character pname*80,chrbf*4,fimage*80
      common /cplot/ xmin,xmax,ymin,ymax,zmin,zmax,jmax,jmark,jkader,
     v               jtimes
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /seppl1/ iway
      common /seppl2/ fname
      character*80 fname
      common /frcount/ ifrinit,ifrcntr,nbase
      common /usertext/ tittext,xaxtext,yaxtext
      character*80 tittext,xaxtext,yaxtext
      common /userplot/ format,frheight,chheight,nplax,npltax
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /bstore/ f2name
      character*80 f2name
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      save ifrno
      data ifrno/1/

 
c     *** Plot preparations
      if (iplot.eq.1) then
        iway=2
        fname='PLOT'
        yfaccn = 1
        jkader=-4
        format=6d0
        chheight = 0.3d0

c       *** Plot streamfunction and markers
        write(tittext,'(''S: t= '',f12.3)') t
        call plwin(1,0d0,0d0)
        if (nochain.gt.0) call peframe(1)
        call plotc1(1,kmesh1,kprob1,isol1,contln,0,format,yfaccn,jsmoot)
        if (ism.eq.1.or.ibm.eq.1) then
c          *** Plot markers
           ip = 0
           do 12 ichain=1,nochain
              call peframe(2+ichain/nochain)
              nmark = imark(ichain)
              if (nmark.gt.NUM) then
                 write(6,*) 'PERROR(marout): nmark too large'
                 stop
              endif
              do 10 i=1,nmark
                xmc(i) = coormark(ip+2*i-1)
10              ymc(i) = coormark(ip+2*i)
             call pecurp(nmark,xmc,ymc,format,yfaccn)
             ip = ip+2*nmark
12         continue
        endif

c       *** Plot temperature or velocity and markers
        call plwin(3,format+1d0,0d0)
        if (nochain.gt.0) call peframe(1)
        if (ist.eq.1) then
           write(tittext,'(''T: t= '',f12.3)') t
           call plotc1(1,kmesh2,kprob2,isol2,contln,0,format,
     v                 yfaccn,jsmoot)
        else
           call plotvc(2,3,isol1,isol1,kmesh1,kprob1,format,yfaccn,0d0)
        endif
        if (ism.eq.1.or.ibm.eq.1) then
           ip = 0
           do 13 ichain=1,nochain
              call peframe(2+ichain/nochain)
              nmark = imark(ichain)
              if (nmark.gt.NUM) then
                 write(6,*) 'PERROR(marout): nmark too large'
                 stop
              endif
              do 14 i=1,nmark
                xmc(i) = coormark(ip+2*i-1)
14              ymc(i) = coormark(ip+2*i)
              ip = ip+2*nmark
             call pecurp(nmark,xmc,ymc,format,yfaccn)
13         continue
        endif
c       *** Output to backingstorage files
        f2name = 'temp.back'
        call openf2(.false.)
        call writbs(0,-1,numarr,'isol1',isol1,ihelp)
        call writbs(0,-1,numarr,'isol2',isol2,ihelp)
        call writb1

        if (ism.eq.1.or.ibm.eq.1.) then
           write(9) t,nochain,(imark(ichain),ichain=1,nochain)
           ntot = 0
           do 15 ichain=1,nochain
15            ntot = ntot + imark(ichain)
           write(9) (coormark(i),i=1,2*ntot)
        endif
        write(12,*) ifrno,t
        call flush(12)

c       *** Check volumes
        call marras(coormark,rname,1)
        call checkvol
   
c       call plot(0.,0.,999)
      endif

c     **** Create restartfiles
      f2name = 'tmar.restart'
      call openf2(.true.)
      call writbs(0,1,numarr,'isol1',isol1,ihelp)
      call writbs(0,2,numarr,'isol2',isol2,ihelp)
      call writb1

      pname = 'markers'
      open(13,file=pname,form='unformatted')
      rewind(13)
      write(13) t,nochain,(imark(ichain),ichain=1,nochain)
      ntot = 0
      do 18 ichain=1,nochain
18       ntot = ntot + imark(ichain)
      write(13) (coormark(i),i=1,2*ntot)
      call flush(13)
      
      return
      end
