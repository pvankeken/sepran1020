c *************************************************************
c *   PELEM3
c *
c *   This version calculates element matrices for the
c *   convection-diffusion equation for the bilinear rectangle,
c *   with sides 1-2 and 3-4 in the x-direction (length a) and
c *   sides 2-3 and 4-1 in the y-direction (length b).
c *
c *************************************************************
c *   Parameters on the equation should be stored in arrays user
c *   and iuser:
c *  
c *    user(31)           alpha        diffusivity
c *
c *    user(32)           u1           x-velocity in point 1
c *      ....             ....
c *    user(31+npoint)    uN           x-velocity in point N
c *
c *    user(32+npoint)    v1           y-velocity in point 1
c *      .....            ....
c *    user(31+2*npoint)  vN           y-velocity in point N
c *
c *************************************************************
c *    Following elementtypes are implemented
c *    
c *        3          Galerkin weighting; exact integration
c *
c *        5          Upwind weighting according to scheme
c *                   of Huyakorn etc.; exact integration
c *       
c *        7          Pure convection; upwind weighting; 
c *                   exact integration
c *
c *        8          Mass matrix; exact integration
c * 
c *        9          Upwind weighted mass matrix; exact integration
c *
c *   PvK 150190-300190
c *************************************************************
      subroutine pelem3(coor,elemmt,elemvc,iuser,user,uold,matrix,
     v                vector,index1,index2)
      implicit double precision(a-h,o-z)
      dimension coor(*),elemmt(*),elemvc(*),iuser(*),user(*)
      dimension uold(*),index1(*),index2(*)
      dimension x(4),y(4),u(4),v(4)

      logical matrix,vector
      common /cactl/ ielem,itype,ielgrp,inpelm,icount,ifirst,
     v               notmat,notvec,irelem,nusol,nelem,npoint
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

      if (ifirst.eq.1.and.(itype.eq.2.or.itype.eq.4)) return

      if (vector) then
         elemvc(1) = 0.
         elemvc(2) = 0.
         elemvc(3) = 0.
         elemvc(4) = 0.
      endif
     
      if (nrule.ne.0) then
c        *** Numerical integration
         if (nrule.gt.0) then
c           *** Gauss quadrature: 
c           *** fill values of shapefunctions and derivatives of
c           *** shapefunction in the nodalpoints.
c           *** Use common pegauss and e3shape
	    if (nrule.eq.1) ngaus=4
	    if (nrule.eq.2) ngaus=9
	    if (nrule.eq.3) ngaus=16
            call fwgaus
	    nphi = 4
	    call fdergaus
         endif
      endif

c     *** Fill coordinates and velocity components 
      do 10 i=1,inpelm
         ih = 2*index1(i)
         x(i) = coor(ih-1)
         y(i) = coor(ih)
         iu = 31+index1(i)
         iv = iu+npoint
         u(i) = user(iu)
         v(i) = user(iv)
10    continue
      a = abs(x(1)-x(2))
      b = abs(y(1)-y(4))
      alpha = user(31)

      if (matrix) then

         if (itype.eq.3) then
c        *** Stiffness matrix; Galerkin weighting
            call blrdif(elemmt,a,b,alpha)
            call blrconv(elemmt,a,b,u,v)
	    ifirst=1
            return
         endif

c        *** Mass matrix
         if (itype.eq.8) then
            call blrmas(elemmt,a,b)
	    ifirst=1
            return
         endif

c        *** upwinding is used; initialize some parameters,
c        *** depending on integration rules
	 if (nrule.eq.0) then
            call upwab(u,v,alf,bet,a,b,alpha)            
         else if (nrule.gt.0) then 
            if (ifirst.eq.0) call upwinit
	    call upwgaus(u,v,a,b,alpha)
         endif

         if (itype.eq.5) then
	   if (nrule.eq.0) then
	      call blrdif(elemmt,a,b,alpha)
              call upwdif(elemmt,a,b,alf,bet,alpha)
           else if (nrule.gt.0) then
	      call upwgdif(elemmt,a,b,alpha)
	   endif
         endif
c        *** Convective terms 
	 if (nrule.eq.0) then
            call upwc2(elemmt,a,b,alf,bet,u,v)
            call upwc3(elemmt,a,b,alf,bet,u,v)
            call upwc4(elemmt,a,b,alf,bet,u,v)
         else if (nrule.gt.0) then
	    call upwgcon(elemmt,a,b,u,v)
	 endif
      endif
c        if (itype.eq.9) then
c         if (nrule.eq.0) call upwab(u,v,alf,bet,a,b,alpha)      
c            if (ifirst.eq.0) then
c               print *
c               print *,'upwind factors'
c               write(6,'(2f12.3)') alf,bet
c            endif
c            call upwmas(elemmt,alf,bet,a,b)
c            if (ifirst.eq.0) then
c              print *,'Mass'
c              call printmat(elemmt)
c            endif
c	    ifirst=1
c            return
c         endif
            
      ifirst=1
      return
      end

      subroutine printmat(elemmt)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)
    
      write(6,*)
      write(6,10) 24*elemmt(1),24*elemmt(2),24*elemmt(3),
     v                24*elemmt(4)
      write(6,10) 24*elemmt(5),24*elemmt(6),24*elemmt(7),
     v                24*elemmt(8)
      write(6,10) 24*elemmt(9),24*elemmt(10),24*elemmt(11),
     v                24*elemmt(12)
      write(6,10) 24*elemmt(13),24*elemmt(14),24*elemmt(15),
     v                24*elemmt(16)
10    format(4f12.3)

      return
      end
 
c *************************************************************
c *   FDERGAUS
c *
c *   Fill values of shapefunctions and derivatives of 
c *   shapefunctions in pegauss
c *
c *************************************************************
      subroutine fdergaus
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

      if (nphi.ne.4) then
	 write(6,*) 'PERROR(fdergaus) nphi<>4 not yet implemented'
	 stop
      endif

      do 10 i=1,ngaus
	 x = xg(i)
	 y = yg(i)
	 phi(1,i) = (1-x)*(1-y)
	 phi(2,i) = x*(1-y)
	 phi(3,i) = x*y
	 phi(4,i) = (1-x)*y
	 dphidx(1,i) = y-1
	 dphidx(2,i) = 1-y
	 dphidx(3,i) = y
	 dphidx(4,i) = -y
	 dphidy(1,i) = x-1
	 dphidy(2,i) = -x
	 dphidy(3,i) = x
	 dphidy(4,i) = (1-x)
10    continue

      return
      end



      subroutine blrdif(elemmt,a,b,alpha) 
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      parameter(drie=0.33333333333333333d0,zes=0.16666666666666667d0)
      save ifirst
      dimension elemmt(*)
      data ifirst/0/

      ab=a/b*alpha
      ba=b/a*alpha
      if (nrule.eq.0) then
c        *** exact integration
         d1 = drie*ab + drie*ba
         d2 = zes*ab  - drie*ba
         d3 = -zes*ab - zes*ba
         d4 = -drie*ab+ zes*ba
         elemmt(1) = d1
         elemmt(2) = d2
         elemmt(3) = d3  
         elemmt(4) = d4
         elemmt(5) = d2
         elemmt(6) = d1
         elemmt(7) = d4
         elemmt(8) = d3
         elemmt(9) = d3
         elemmt(10)= d4
         elemmt(11)= d1
         elemmt(12)= d2
         elemmt(13)= d4
         elemmt(14)= d3
         elemmt(15)= d2
         elemmt(16)= d1
      else
c        *** numerical integration (symmetry!)
         do 20 j=1,nphi
	    do 20 i=j,4
	       ioff = (j-1)*4+i
	       sumx  = 0.
	       sumy  = 0.
	       do 30 k=1,ngaus
		  sumx = sumx+w(k)*dphidx(i,k)*dphidx(j,k)*ba
30                sumy = sumy+w(k)*dphidy(i,k)*dphidy(j,k)*ab
	       if (ifirst.eq.0) write(6,'(2i3,2f12.5)') i,j,sumx,sumy
               elemmt(ioff) = sumx+sumy
20       continue
	 elemmt(5) = elemmt(2)
	 elemmt(9) = elemmt(3)
	 elemmt(10)= elemmt(7)
	 elemmt(13)= elemmt(4)
	 elemmt(14)= elemmt(8)
	 elemmt(15)= elemmt(12)
	 ifirst = 1
      endif
      return
      end

c *************************************************************
c *   BLRCONV
c *
c *   Compute element integrals for the convective terms.
c *   These are of the form 
c *
c *        s(i,j)= // N(i) u dN(j)/dx dxdy + // N(i) v dN(j)/dy dxdy
c *
c *   The integrals are calculated by expressing (u,v) as the sum of
c *   basisfunctions 
c *
c *        u = sum(k)   u(k)N(k)
c *
c *   so 
c *
c *        s(i,j)= sum(k)  u(k) // N(i)N(k) dN(j)/dx  dxdy +
c *
c *                sum(k)  v(k) // N(i)N(k) dN(j)/dy  dxdy
c *
c *   The integrals are evaluated exactly, yielding a weighted sum
c *   over the velocity components.
c *
c *
c *   PvK 110190
c *************************************************************
      subroutine blrconv(elemmt,a,b,u,v)
      implicit double precision(a-h,o-z)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      dimension elemmt(16),u(4),v(4)
      dimension tx(4,4,4),ty(4,4,4)
      save tx,ty
      save ifirst
      data ifirst/0/

      if (ifirst.eq.0) then
	 write(6,*) 'blrconv: u,v'
	 write(6,'(4f12.3)') (u(i),i=1,4)
	 write(6,'(4f12.3)') (v(i),i=1,4)
	 write(6,'(2f12.3)') a,b
      endif
      if (nrule.eq.0) then
        a72 = a/72
        b72 = b/72
        u1  = u(1)*b72
        u2  = u(2)*b72
        u3  = u(3)*b72
        u4  = u(4)*b72
        v1  = v(1)*a72
        v2  = v(2)*a72
        v3  = v(3)*a72
        v4  = v(4)*a72
        u12 = 2*u1
        u13 = 3*u1
        u16 = 6*u1
        u22 = 2*u2
        u23 = 3*u2
        u26 = 6*u2
        u32 = 2*u3
        u33 = 3*u3
        u36 = 6*u3
        u42 = 2*u4
        u43 = 3*u4
        u46 = 6*u4
        v12 = 2*v1
        v13 = 3*v1
        v16 = 6*v1
        v22 = 2*v2
        v23 = 3*v2
        v26 = 6*v2
        v32 = 2*v3
        v33 = 3*v3
        v36 = 6*v3
        v42 = 2*v4
        v43 = 3*v4
        v46 = 6*v4
        elemmt(1) = elemmt(1) -u16-u23-u3 -u42 -v16-v22-v3 -v43
        elemmt(2) = elemmt(2) +u16+u23+u3 +u42 -v12-v22-v3 -v4
        elemmt(3) = elemmt(3) +u12+u2 +u3 +u42 +v12+v22+v3 +v4
        elemmt(4) = elemmt(4) -u12-u2 -u3 -u42 +v16+v22+v3 +v43
        elemmt(5) = elemmt(5) -u13-u26-u32-u4  -v12-v22-v3 -v4 
        elemmt(6) = elemmt(6) +u13+u26+u32+u4  -v12-v26-v33-v4
        elemmt(7) = elemmt(7) +u1 +u22+u32+u4  +v12+v26+v33+v4
        elemmt(8) = elemmt(8) -u1 -u22-u32-u4  +v12+v22+v3 +v4 
        elemmt(9) = elemmt(9) -u1 -u22-u32-u4  -v1 -v2 -v32-v42
        elemmt(10)= elemmt(10)+u1 +u22+u32+u4  -v1 -v23-v36-v42
        elemmt(11)= elemmt(11)+u1 +u22+u36+u43 +v1 +v23+v36+v42
        elemmt(12)= elemmt(12)-u1 -u22-u36-u43 +v1 +v2 +v32+v42
        elemmt(13)= elemmt(13)-u12-u2 -u3 -u42 -v13-v2 -v32-v46
        elemmt(14)= elemmt(14)+u12+u2 +u3 +u42 -v1 -v2 -v32-v42
        elemmt(15)= elemmt(15)+u12+u2 +u33+u46 +v1 +v2 +v32+v42
        elemmt(16)= elemmt(16)-u12-u2 -u33-u46 +v13+v2 +v32+v46
      else if (nrule.gt.0) then
c        *** Numerical integration; Gauss quadrature
        if (ifirst.eq.0) then
           do 10 i=1,nphi
              do 10 j=1,nphi
                 do 10 k=1,nphi
                    sumx = 0.
                    sumy = 0.
                    do 20 m=1,ngaus
                       sumx = sumx+phi(i,m)*phi(k,m)*dphidx(j,m)*w(m)
20                     sumy = sumy+phi(i,m)*phi(k,m)*dphidy(j,m)*w(m)
                    tx(i,k,j) = sumx
                    ty(i,k,j) = sumy
c	            write(6,'(2i3,2f12.5)') i,j,sumx,sumy
10         continue
       endif

       do 30 j=1,nphi
          do 30 i=1,nphi
             ioff = j+(i-1)*4
             sumx = 0.
             sumy = 0.
             do 40 k=1,nphi
		sumx = sumx + u(k)*tx(i,k,j)*b
		sumy = sumy + v(k)*ty(i,k,j)*a
40           continue
c            if (ifirst.eq.0) write(6,'(2i3,2f12.5)') i,j,sumx,sumy
             elemmt(ioff) = elemmt(ioff)+sumx+sumy
30      continue
      endif
      ifirst=1
      return
      end


c *************************************************************
c *   BLRMAS
c *
c *   Built Galerking weighted mass matrix for the bilinear triangles
c *
c *              1
c *   M     =   //  N N  dxdy   =
c *    ij       0    i j 
c *
c *   PvK 300190
c *************************************************************
      subroutine blrmas(elemmt,a,b)
      implicit double precision (a-h,o-z)
      dimension elemmt(16)

      a1 = a*b/36
      a2 = a*b/18
      a4 = a*b/9

      elemmt(1) = a4 
      elemmt(2) = a2 
      elemmt(3) = a1 
      elemmt(4) = a2 
      elemmt(5) = a2 
      elemmt(6) = a4 
      elemmt(7) = a2 
      elemmt(8) = a1 
      elemmt(9) = a1 
      elemmt(10)= a2 
      elemmt(11)= a4 
      elemmt(12)= a2 
      elemmt(13)= a2 
      elemmt(14)= a1 
      elemmt(15)= a2 
      elemmt(16)= a4 

      return
      end

      
c *************************************************************
c *   UPWAB
c *
c *   Calculate the upwind factors alpha and beta defined by (i=1,4)
c *
c *   - alpha  = coth ( gamma  /2) - 2/gamma      
c *          i               i              i
c *
c *   - beta   = coth ( delta  /2) - 2/delta
c *         i                i              i
c *
c *   where gamma  = (u a)/kappa  and delta  = (v a)/kappa
c *              i     i                   i     i
c *
c *   The minus sign enters the equations because of the choice
c *   of F(x).
c *
c *   PvK 220190
c *************************************************************
      subroutine upwab(u,v,alf,bet,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension u(4),v(4)

      ug = 0.25*(u(1)+u(2)+u(3)+u(4))
      vg = 0.25*(v(1)+v(2)+v(3)+v(4))
      if (alpha.lt.1d-4) then
         alf=-1d0
         bet=-1d0
      else
         gamma = -abs(ug)*a/alpha
         delta = -abs(vg)*b/alpha
         if (abs(gamma).gt.1d-4) then
            alf = 1d0/tanh(gamma/2)-2d0/gamma
         else 
            alf = 0.
         endif
         if (abs(delta).gt.1d-4) then
            bet = 1d0/tanh(delta/2)-2d0/delta
         else
            bet = 0.
         endif
      endif
      return
      end


c *************************************************************
c *   UPWINIT
c *
c *   Calculate values of the linear basisfunctions N(x) and N(y),
c *   of the quadratic correction functions F(x) and F(y),
c *   their derivatives in the Gausspoints
c *
c *   phip(i,1,j)   Ni(x) in Gausspoint j
c *   phip(i,2,j)   Ni(y)
c *   dphip(i,1)    dNi(x)/dx
c *   dphip(i,2)    dNi(y)/dy
c *   fp(1,j)       F(x) 
c *   fp(2,j)       F(y)
c *   dfp(1,j)      dF(x)/dx
c *   dfp(2,j)      dF(y)/dy
c *   wp(i,j)       Wi(x,y)
c *   dwdx(i,1,j)   dWi(x,y)/dx
c *   dwdy(i,2,j)   dWi(x,y)/dy
c *   alpha1,alpha2 Upwind parameters in x-direction
c *   beta1,beta2   Upwind parameters in y-direction
c *
c *   PvK 190390
c *************************************************************
      subroutine upwinit
      implicit double precision(a-h,o-z)
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi

      do 10 i=1,ngaus
	 x = xg(i)
	 y = yg(i)
	 phip(1,1,i) = 1-x
	 phip(2,1,i) = x
	 phip(3,1,i) = x
	 phip(4,1,i) = 1-x
	 phip(1,2,i) = 1-y
	 phip(2,2,i) = 1-y
	 phip(3,2,i) = y
	 phip(4,2,i) = y
	 fp(1,i)   = 3*x*(1-x)
	 fp(2,i)   = 3*y*(1-y)
	 dfp(1,i)  = 3*(1-2*x)
	 dfp(2,i)  = 3*(1-2*y)
10    continue
      dphip(1,1) = -1.
      dphip(2,1) =  1.
      dphip(3,1) =  1.
      dphip(4,1) = -1.
      dphip(1,2) = -1.
      dphip(2,2) = -1.
      dphip(3,2) =  1.
      dphip(4,2) =  1.

      return
      end
      
c *************************************************************
c *   UPWGAUS
c *
c *   Fill values of the upwind corrected weightfunctions
c *   and its derivatives in the Gausspoints
c *
c *   PvK 190390
c *************************************************************
      subroutine upwgaus(u,v,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension u(4),v(4),asn(2,4)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2
      double precision u1,u2,v1,v2,gx1,gx2,gy1,gy2
      save ifirst
      data ifirst/0/

c     *** calculate upwind factors
      u1 = (u(1)+u(2))/2d0
      u2 = (u(3)+u(4))/2d0
      v1 = (v(1)+v(4))/2d0
      v2 = (v(2)+v(3))/2d0
      if (alpha.lt.1d-7) then
	 alpha1 = sign(1d0,u1)
	 alpha2 = sign(1d0,u2)
	 beta1  = sign(1d0,v1)
	 beta2  = sign(1d0,v2)
      else
	 gx1    = u1*a/alpha
	 gx2    = u2*a/alpha
	 gy1    = v1*b/alpha
	 gy2    = v2*b/alpha
	 alpha1 = 0.
	 if (abs(gx1).gt.1d-4) alpha1 = 1d0/tanh(gx1/2)-2d0/gx1
	 alpha2 = 0.
	 if (abs(gx2).gt.1d-4) alpha2 = 1d0/tanh(gx2/2)-2d0/gx2
	 beta1  = 0.
	 if (abs(gy1).gt.1d-4) beta1  = 1d0/tanh(gy1/2)-2d0/gy1
	 beta2  = 0.
	 if (abs(gy2).gt.1d-4) beta2  = 1d0/tanh(gy2/2)-2d0/gy2
      endif
      if (ifirst.eq.0) then
	 write(6,'(''u: '',4f12.4)') u1,u2,v1,v2
	 write(6,'(''g: '',4f12.4)') gx1,gx2,gy1,gy2
	 write(6,'(''a: '',4f12.4)') alpha1,alpha2,beta1,beta2
         ifirst = 1
      endif
      asn(1,1) = -alpha1
      asn(2,1) = -beta1
      asn(1,2) = alpha1
      asn(2,2) = -beta2
      asn(1,3) = alpha2
      asn(2,3) = beta2
      asn(1,4) = -alpha2
      asn(2,4) = beta1
      do 10 i=1,ngaus
	do 10 j=1,nphi
           rnx = phip(j,1,i)
           rny = phip(j,2,i)
	   fx  = fp(1,i)
	   fy  = fp(2,i)
	   alf = asn(1,j)
	   bet = asn(2,j)
           wp(j,i) = rnx*rny + alf*fx*rny + bet*rnx*fy + alf*bet*fx*fy
           dndx = dphip(j,1)
           dndy = dphip(j,2)
	   dfdx = dfp(1,i)
	   dfdy = dfp(2,i)
	   dwdx(j,1,i) = rny*dndx + alf*rny*dfdx + bet*fy*dndx 
     v                   + alf*bet*fy*dfdx
	   dwdx(j,2,i) = rnx*dndy + alf*fx*dndy + bet*rnx*dfdy
     v                   + alf*bet*fx*dfdy
10    continue
      return
      end

c *************************************************************
c *   UPWDIF
c *   
c *   Add the upwind weighted diffusive terms to the stiffness
c *   matrix for the bilinear triangle. It is assumed that the
c *   Galerkin terms are already added. The integrals are calculated
c *   analytically.
c *
c *   PvK 220190
c *************************************************************
      subroutine upwdif(elemmt,a,b,alf,bet,alpha)
      implicit double precision(a-h,o-z)
      dimension elemmt(16)
   
      a1 = a/(4*b)*alf*alpha
      b1 = b/(4*a)*bet*alpha

      elemmt(1)  = elemmt(1)  + a1 + b1 
      elemmt(2)  = elemmt(2)  + a1 - b1
      elemmt(3)  = elemmt(3)  - a1 - b1
      elemmt(4)  = elemmt(4)  - a1 + b1 
      elemmt(5)  = elemmt(5)  + a1 - b1 
      elemmt(6)  = elemmt(6)  + a1 + b1
      elemmt(7)  = elemmt(7)  - a1 + b1 
      elemmt(8)  = elemmt(8)  - a1 - b1
      elemmt(9)  = elemmt(9)  - a1 - b1 
      elemmt(10) = elemmt(10) - a1 + b1 
      elemmt(11) = elemmt(11) + a1 + b1 
      elemmt(12) = elemmt(12) + a1 - b1
      elemmt(13) = elemmt(13) - a1 + b1
      elemmt(14) = elemmt(14) - a1 - b1
      elemmt(15) = elemmt(15) + a1 - b1
      elemmt(16) = elemmt(16) + a1 + b1

      return
      end


c *************************************************************
c *   UPWGDIF
c *
c *   Calculate upwind corrected diffusion matrix
c *   Gauss integration
c *
c *   PvK 190390
c *************************************************************
      subroutine upwgdif(elemmt,a,b,alpha)
      implicit double precision(a-h,o-z)
      dimension elemmt(16)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2

      ab = a/b
      ba = b/a
      do 20 j=1,nphi
         do 20 i=1,nphi
            ioff = (i-1)*4+j
            sum  = 0
            do 30 k=1,ngaus
               sum = sum+w(k)*dwdx(i,1,k)*dphidx(j,k)*ba
30             sum = sum+w(k)*dwdx(i,2,k)*dphidy(j,k)*ab
            elemmt(ioff) = sum*alpha
20    continue

      return
      end

c *************************************************************
c *   UPWC2
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part II
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc2(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = alf*b*(u(1)+u(2))/48
      u2 = alf*b*(u(3)+u(4))/48
      v1 = alf*a*v(1)/120
      v2 = alf*a*v(2)/120
      v3 = alf*a*v(3)/120
      v4 = alf*a*v(4)/120

      elemmt(1)  = elemmt(1) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4 
      elemmt(2)  = elemmt(2) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(3)  = elemmt(3) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(4)  = elemmt(4) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4
 
      elemmt(5)  = elemmt(5) -3*u1-  u2 -6*v1-4*v2-2*v3-3*v4
      elemmt(6)  = elemmt(6) +3*u1+  u2 -4*v1-6*v2-3*v3-2*v4
      elemmt(7)  = elemmt(7) +  u1+  u2 +4*v1+6*v2+3*v3+2*v4
      elemmt(8)  = elemmt(8) -  u1-  u2 +6*v1+4*v2+2*v3+3*v4

      elemmt(9)  = elemmt(9) -  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(10) = elemmt(10)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(11) = elemmt(11)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4 
      elemmt(12) = elemmt(12)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4
 
      elemmt(13) = elemmt(13)-  u1-  u2 -3*v1-2*v2-4*v3-6*v4
      elemmt(14) = elemmt(14)+  u1+  u2 -2*v1-3*v2-6*v3-4*v4
      elemmt(15) = elemmt(15)+  u1+3*u2 +2*v1+3*v2+6*v3+4*v4
      elemmt(16) = elemmt(16)-  u1-3*u2 +3*v1+2*v2+4*v3+6*v4

      return
      end

c *************************************************************
c *   UPWC3
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part III
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc3(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = bet*b*u(1)/120
      u2 = bet*b*u(2)/120
      u3 = bet*b*u(3)/120
      u4 = bet*b*u(4)/120
      v1 = bet*a*(v(1)+v(4))/48
      v2 = bet*a*(v(2)+v(3))/48

      elemmt(1)  = elemmt(1) -6*u1-3*u2-2*u3-4*u4 -3*v1-  v2  
      elemmt(2)  = elemmt(2) +6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(3)  = elemmt(3) +4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(4)  = elemmt(4) -4*u1-2*u2-3*u3-6*u4 +3*v1+  v2

      elemmt(5)  = elemmt(5) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(6)  = elemmt(6) +3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(9)  = elemmt(9) -3*u1-6*u2-4*u3-2*u4 -  v1-  v2
      elemmt(10) = elemmt(10)+3*u1+6*u2+4*u3+2*u4 -  v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+4*u2+6*u3+3*u4 +  v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-4*u2-6*u3-3*u4 +  v1+  v2

      elemmt(13) = elemmt(13)-6*u1-3*u2-2*u3-4*u4 -3*v1-  v2
      elemmt(14) = elemmt(14)+6*u1+3*u2+2*u3+4*u4 -  v1-  v2
      elemmt(15) = elemmt(15)+4*u1+2*u2+3*u3+6*u4 +  v1+  v2
      elemmt(16) = elemmt(16)-4*u1-2*u2-3*u3-6*u4 +3*v1+  v2


      return
      end

c *************************************************************
c *   UPWC4
c *
c *   Add the upwind weighted convective terms to the stiffness
c *   matrix. It is assumed that the Galerkin terms are already
c *   added.
c *
c *   Part IV
c *
c *   PvK 220190
c *************************************************************
      subroutine upwc4(elemmt,a,b,alf,bet,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)

      u1 = -alf*bet*b*(u(1)+u(2))/80
      u2 = -alf*bet*b*(u(3)+u(4))/80
      v1 = -alf*bet*a*(v(1)+v(4))/80
      v2 = -alf*bet*a*(v(2)+v(3))/80


      elemmt(1)  = elemmt(1) -3*u1-2*u2-3*v1-2*v2 
      elemmt(2)  = elemmt(2) +3*u1+2*u2-2*v1-3*v2
      elemmt(3)  = elemmt(3) +2*u1+3*u2+2*v1+3*v2
      elemmt(4)  = elemmt(4) -2*u1-3*u2+3*v1+2*v2

      elemmt(5)  = elemmt(5) -3*u1-2*u2-3*v1-2*v2 
      elemmt(6)  = elemmt(6) +3*u1+2*u2-2*v1-3*v2
      elemmt(7)  = elemmt(7) +2*u1+3*u2+2*v1+3*v2
      elemmt(8)  = elemmt(8) -2*u1-3*u2+3*v1+2*v2

      elemmt(9)  = elemmt(9) -3*u1-2*u2-3*v1-2*v2 
      elemmt(10) = elemmt(10)+3*u1+2*u2-2*v1-3*v2
      elemmt(11) = elemmt(11)+2*u1+3*u2+2*v1+3*v2
      elemmt(12) = elemmt(12)-2*u1-3*u2+3*v1+2*v2

      elemmt(13) = elemmt(13)-3*u1-2*u2-3*v1-2*v2 
      elemmt(14) = elemmt(14)+3*u1+2*u2-2*v1-3*v2
      elemmt(15) = elemmt(15)+2*u1+3*u2+2*v1+3*v2
      elemmt(16) = elemmt(16)-2*u1-3*u2+3*v1+2*v2

      return
      end

c *************************************************************
c *   UPWGCON
c *   Calculate the upwind corrected convection matrix
c *   Gauss integration
c *
c *   PvK 190390
c *************************************************************
      subroutine upwgcon(elemmt,a,b,u,v)
      implicit double precision(a-h,o-z)
      dimension elemmt(16),u(4),v(4)
      common /peinterg/ nrule
      common /pegauss/ w(16),xg(16),yg(16),ngaus
      common /e3shape/ phi(16,16),dphidx(16,16),dphidy(16,16),
     v                 d2phidx(16,16),d2phidy(16,16),d2phixy(16,16),nphi
      common /peupw/ phip(4,2,16),dphip(4,2),fp(2,16),dfp(2,16),
     v               wp(4,16),dwdx(4,2,16),alpha1,alpha2,beta1,beta2

      save ifirst
      data ifirst/0/

      if (ifirst.eq.0) then
         write(6,*) 'upwgcon: u,v'
         write(6,'(4f12.3)') (u(i),i=1,4)
         write(6,'(4f12.3)') (v(i),i=1,4)
	 write(6,'(2f12.3)') a,b
      endif
      do 30 i=1,nphi
         do 30 j=1,nphi
            ioff = j+(i-1)*4
            sumx = 0.
	    sumy = 0.
            do 10 k=1,nphi
               tx = 0.
               ty = 0.
               do 20 m=1,ngaus
                  tx = tx + wp(i,m)*phi(k,m)*dphidx(j,m)*w(m)
20                ty = ty + wp(i,m)*phi(k,m)*dphidy(j,m)*w(m)
               sumx = sumx + u(k)*tx*b
	       sumy = sumy + v(k)*ty*a
10          continue
	    if (ifirst.eq.0) write(6,'(2i3,2f12.5)') i,j,sumx,sumy
	    elemmt(ioff) = elemmt(ioff)+sumx+sumy
30    continue
      ifirst=1
      return
      end
