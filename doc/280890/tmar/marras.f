c *************************************************************
c *   MARRAS
c *
c *   Create a rasterfile in which the value of the pixels 
c *   indicate the layer in which it is positioned.
c *   The layerboundaries are represented by the boundaries
c *   of the geometry ([0,rlam]x[0,1]) and the markerchains
c *   (common c1mark)
c *   The algorithm is described in PVK 040690
c *
c *   PvK 050690/300790
c *************************************************************
      subroutine marras(coormark,rname,iout)
      implicit double precision (a-h,o-z)
      dimension coormark(*),icy(500)
      common /cpix/ rlam,ipix(160000),nxpix,nypix
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /colmar/ icol(10),ivol(10)
      character*80 rname
      
      if (nxpix.eq.0) return
c     *** Initialize
      do 5 i=1,nxpix*nypix
         ipix(i) = 1
5     continue
      ip = 0
      rlam =  coormark(1)
      do 10 ichain=1,nochain
         nmark = imark(ichain)
c        *** Loop over columns
         do 20 ix=1,nxpix
c           *** Determine in which pixels the markerchain 
c           *** intersects the column. Start with the upper
c           *** boundary (y=1,iy=1) and finish with the 
c           *** lower one (y=0,iy=nypix) because of the
c           *** fantastic choice of the imagetools origin
            xm = (0.5+(ix-1))*rlam/nxpix
            icut=1
            icy(1)=1
            do 30 im=2,nmark
               x1 = coormark(ip+2*im-3) 
               x2 = coormark(ip+2*im-1) 
               if (x1.le.xm.and.x2.gt.xm.or.x1.ge.xm.and.x2.lt.xm) then
                  y1 = coormark(ip+2*im-2)
                  y2 = coormark(ip+2*im)
                  ym = y1+(xm-x1)/(x2-x1)*(y2-y1)
                  icut = icut+1
                  icy(icut) = 1+(1-ym)*(nypix-1)
               endif
30          continue
            icut=icut+1
            icy(icut)=nypix
c           *** sort this array
            do 40 i=2,icut-1
               do 40 j=3,icut-1
                  if (icy(j-1).gt.icy(j)) then
                     it = icy(j-1)
                     icy(j-1) = icy(j)
                     icy(j) = it
                  endif
40             continue
c           *** Add 1 to the layer which lies below the markerchain
            do 50 iregio=1,icut-1,2
               do 70 iy=icy(iregio+1),icy(iregio+2)
                  ic = (iy-1)*nxpix + ix
70                ipix(ic)=ipix(ic)+1
50          continue
20       continue
         call checkvol
         ip = ip+2*nmark
10    continue

      if (iout.eq.-1) then
         ntot = nxpix*nypix
c        *** Represents the layers 1,2,3 ... with colors stored in
c        *** icol
         do 100 ip=1,ntot
100         ipix(ip) = icol(ipix(ip))
         call rasout(ntot,ipix,rname)
      endif
       
      end
