c *************************************************************
c *   TMSTEP
c *
c *   Determines timestep from velocity field
c *
c *   PvK 010890
c *************************************************************
      subroutine tmstep(kmesh1,kmesh2,kprob,isol,coormark,velmark,
     v                  iuser,user,dtm,dtt)
      implicit double precision(a-h,o-z)
      dimension kmesh1(*),kmesh2(*),kprob(*),isol(*),coormark(*)
      dimension iuser(*),user(*),velmark(*)
      common /ctime/ t,tout,tstep,tend,rtime(6),iflag,itime(9)
      common /c1solve/ rat,ram,ism,ist,ibm,ibt
      common /c1mark/ wm(10),nochain,ichain,imark(10)
      common /peiter/ tmax,dtout,dtcfl,tfac,tstepmax,tvalid,difcor,
     v                difcormax,nitermax,nout,ncor

      if (ism.eq.1) then
         call pedetcfl(velmark)
         dtm  = dtcfl
         tstm = dtcfl*tfac
         tstep = tstm
      endif
      if (ist.eq.1) then
         call bmcof(2,kmesh1,kmesh2,kprob,isol,iuser,user)
         call pedtcf(kmesh2,user,dtcfl)
         dtt = dtcfl
         tstt = dtcfl*tfac
         tstep = tstt
      endif
      if (ist.eq.1.and.ism.eq.1) tstep = min(tstt,tstm)
      if (tstep.gt.tstepmax.and.t.lt.tvalid) tstep=tstepmax
c     *** round the timestep
      if (t+tstep.gt.tout) then
         tstep = tout-t
      else 
         if (tstep.gt.0) call rtstep(tstep)
      endif
      write(6,'(a,3f15.6)') 'tstm,tstt,tstep  ',tstm,tstt,tstep
  
      return
      end
