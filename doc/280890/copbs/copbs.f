      program copmesh
      implicit double precision(a-h,o-z)
      dimension isol1(5),isol2(5)
      common /bstore/ f2name
      character*80 f2name
      character*80 fname1,fname2
      logical flag1,flag2

      read(5,*) fname1,fname2
      read(5,*) flag1,flag2
      read(5,*) nrec1,nrec2

      call start(0,0,0,0)
      f2name = fname1
      call openf2(flag1)
      call readbs(nrec1,isol1,ihelp)
      f2name = fname2
      call openf2(flag2)
      call writbs(0,nrec2,numarr,'isol1',isol1,ihelp)
      call writb1

      end

       
        

