subroutine handle_err(errcode)
implicit none
include 'netcdf.inc'
integer errcode
write(6,*) 'netcdf error: ',nf_strerror(errcode)
call instop
end subroutine handle_err
