! *************************************************************
! *   SOLINT
! *
! *   Interpolation of a solutionvector from one mesh to another.
! *   
! *   Input:
! *        ICHOICE        Choice parameter:
! *                       1  Linear triangles, 1 dof
! *                       2  Quadratic triangles, 2 dof's
! *        FILE_SOL1      Name of the bsfile containing the
! *                       solution vector to be interpolated
! *        NREC1          Record number of solution vector on FILE_SOL1
! *        FILE_MESH1     File containing the mesh on which the
! *                       vector is defined
! *        FILE_MESH2     File containing the mesh on which the
! *                       solutionvector has to be interpolated
! *        FILE_SOL2      Name of bsfile on which the output is
! *                       written
! *        NREC2          Corresponding record number
! *
! *   PvK 260690
! *   PvK 040405  Modernized...
! *************************************************************
program solint
use sepmoduleplot
use sepmodulemain
implicit none
integer :: kmeshold=0,kmeshnew=0,kprobold=0,kprobnew=0
integer ::  kmeshdum(400)
integer ::  isol1(5),isol2(5)
integer ::  iu1(2)
character(len=80) :: file_mesh1,file_mesh2,file_sol1,file_sol2
character(len=80) :: myformat
integer ::  ichoice,ncntln,numarr,iinput,jchoice,iprint,lu
real(kind=8) :: format,yfac,contln,u1(2)
logical :: single_mesh_file,check1,netcdf

write(6,*) 'start solint'
read(5,*) ichoice
read(5,*) file_sol1
read(5,*) file_sol2
file_sol1=file_sol1(1:len_trim(file_sol1))
file_sol2=file_sol2(1:len_trim(file_sol2))
write(6,*) 'file_sol1: ',file_sol1
write(6,*) 'file_sol2: ',file_sol2
!stop
call start(0,1,0,0)
open(9,file='meshoutput.old',form='formatted')
call meshrd(-2,9,kmeshold)
close(9)
open(9,file='meshoutput',form='formatted')
call meshrd(-2,9,kmeshnew)
write(6,*) 'kmesh: ',kmeshold,kmeshnew

write(6,*) 'probdf1'
call probdf(0,kprobold,kmeshold,iinput)
     
iu1(1)=0
u1(1)=0
call creavc(0,1,0,isol1,kmeshold,kprobold,iu1,u1,iu1,u1)
write(6,*) 'readbs'
call readbs_netcdf(file_sol1,isol1)
write(6,*) 'probdf2'
call probdf(0,kprobnew,kmeshnew,iinput)
write(6,*) 'intmsh'
call intmsh(0,kmeshold,kmeshnew,kprobold,kprobnew,isol1,isol2)
 
format=10d0
! suppress plotting by using ichoice=0, or anything but 1 & 2
if (ichoice.eq.1) then
   call plotc1(1,kmeshold,kprobold,isol1,contln,0,format,yfac,0)
   call plotc1(1,kmeshnew,kprobnew,isol2,contln,0,10d0,1d0,0)
else if (ichoice==2) then
   call plotvc(1,2,isol1,isol1,kmeshold,kprobold,format,yfac,0d0)
   call plotvc(1,2,isol2,isol2,kmeshnew,kprobnew,format,yfac,0d0)
endif
write(6,*) 'writbs'
call writbs_netcdf(file_sol2,isol2)
end program solint

