.SUFFIXES:      .o .f .c .a .F .f90
.f.o:
	compile $<

.F.o:
	compile $<

.f90.o:
	compile $<

.f.a:
	$(SEPF77) $(SEP_FFLAGS) -I$(NETCDF_INCLUDE_DIR) -I$(SPHOME) $<

.c.o:
	$(SEPCC) $(SEP_CFLAGS) $<

.c.a:
	$(SEPCC) $(SEP_CFLAGS) $<
