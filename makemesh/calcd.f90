real(kind=8) function calcd(n,f,rl)
implicit none
real(kind=8) :: rl,f(*)
integer :: n(*)
integer :: n1,n2,n3,i
real(kind=8) :: f1,f2,f3,sf1,fn1,sft,sf2,sf3

n1 = n(1)
n2 = n(2)
n3 = n(3)
f1 = f(1)
f3 = f(2)
sf1 = 0.
do i=1,n1
   sf1 = sf1+f1**(i-1)
enddo
fn1 = f1**(n1-1)
sft = sf1
sf2 = n2
sft = sft+sf2*fn1
sf3 = 0.
do  i=1,n3
    sf3 = sf3+f3**(i-1)
enddo
sft = sft+sf3*fn1
calcd = rl/sft

end
