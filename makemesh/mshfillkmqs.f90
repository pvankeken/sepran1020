      subroutine mshfillkmqs ( iinput, rinput, iseqsurfs, ipointsurf )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.1  date 27-11-2012 Long integers
!        version  2.0  date 02-09-2010 Remove intarmsh
!        version  1.4  date 10-05-2004 Correction rinput(0)
!
!   copyright (c) 2002-2012  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill array kmesh parts q, r, s as well as array volume_surfs
!
! **********************************************************************
!
!                       KEYWORDS
!
!     topology
!     mesh
!     surface
!     volume
! **********************************************************************
!
!                       MODULES USED
!
      use sepmoduledebug
      use sepmodulekmesh
      use sepmodulemesh
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iinput(*), iseqsurfs(*), ipointsurf(*)
      double precision rinput(*)

!     iinput         i    integer input array, standard for subroutine MESH
!                         The starting address in MESH is 6
!     ipointsurf     i    Array containing the relative starting addresses
!                         of all surfaces in the part of iinput referring
!                         to surfaces
!     iseqsurfs      i    Contains new sequence of surface numbers
!     rinput         i    Real input array, standard for subroutine MESH
!                         The starting address in MESH is 6
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer lenvolsurfs, maxncursur, lencurveusp
      integer, allocatable :: curveusp(:)

!     curveusp       array containing the user points corresponding to
!                    the curves in the following way:
!                    curveusp(1)=ncurvs+1
!                    number of user points in curve i:
!                    curveusp(i+1)-curveusp(i)
!                    user points in curve i are stored in curveusp from
!                    position curveusp(i)+1 to curveusp(i+1)
!     lencurveusp    Length of array curveusp
!     lenvolsurfs    Length of array volume_surfs
!     maxncursur     Estimate of the length of the last part of iwork
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERALLOC        Produce error message in case allocate went wrong
!     ERCLOS         Resets old name of previous subroutine of higher level
!     ERDEALLOC      Produce error message in case deallocate went wrong
!     EROPEN         Produces concatenated name of local subroutine
!     MSH051         Fill user points corresponding to curves in work array
!     MSH060         Fill array KMESH parts Q, R and S
!     MSHFILLVOLSUR  Fill array volume_surfs with all single surfaces enclosing
!                    the volume
!     PR0012         Print accumulated array
!     PRININ1        print 2d integer array
!     PRINNMESHX     Print the contents of an integer array with structure of
!                    sub array of nmesh
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     Initialize parameters
!     Reserve temporary space for the various arrays
!     Compute kmeshq,  kmeshr and kmeshs
!     Make arrays KMESH parts Q, R and S permanent
!     Fill array volume_surfs, with all surfaces corresponding to the volumes
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'mshfillkmqs' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from mshfillkmqs'

      end if

!     --- Reserve temporary space for the various arrays

      if ( max(ncurvs,nsurfs)>1000 ) then
         maxncursur = 20*max(ncurvs,nsurfs)
      else
         maxncursur = ncurvs*nsurfs*3
      end if

!     --- Compute kmeshq,  kmeshr and kmeshs

      call msh060 ( iinput, rinput(max(1,irnp4)), iseqsurfs, &
                    ipointsurf, maxncursur )

      if ( debug ) then

!     --- Debug information

         if ( kelmr/=0 ) then
            write(irefwr,200)
200         format ( //' Subsurface numbers of surfaces'/ )
            call pr0012 ( nsurfs, kmeshr, kmeshr )
         end if
         if ( kelms/=0 ) then
            write(irefwr,210)
210         format ( //' Curves corresponding to surfaces'/ )
            call pr0012 ( nsurfs, kmeshs, kmeshs )
            call prinin1 ( kmeshs(nsurfs+2), nsurfs, 8, &
                           'Rest of information per surface' )

         end if

      end if

!     --- Fill array curveusp with the user point numbers corresponding to
!         the curves

      lencurveusp = 4000*ncurvs
      allocate ( curveusp(lencurveusp), stat = error )
      if ( error/=0 ) call eralloc ( error, lencurveusp, 'curveusp' )
      if ( ierror/=0 ) go to 1000

      call msh051 ( curveusp, ncurvs, iinput, ndim, jcoars, &
                    iseqcurvs, ipointcurv, lencurveusp )

!     --- Make array curveusp permanent

      allocate ( km(iseqkm)%curveusrp(lencurveusp) )
      curveusrp => km(iseqkm)%curveusrp
      kelmak = lencurveusp
      curveusrp = curveusp(1:lencurveusp)

      deallocate ( curveusp, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'curveusp' )
      if ( ierror/=0 ) go to 1000

      if ( nvolms>0 ) then

!     --- Fill array volume_surfs, with all surfaces corresponding to
!         the volumes
!         First estimate the length of this array

         lenvolsurfs = 2*(nvolms+nsurfs)

!        --- Reserve temporary space to store volume_surfs

         allocate ( volume_surfs(lenvolsurfs), stat = error )
         if ( error/=0 ) call eralloc ( error, lenvolsurfs, 'volume_surfs' )
         if ( ierror/=0 ) go to 1000

!        --- Fill array volume_surfs

         call mshfillvolsur ( iinput, lenvolsurfs )

!        --- Make array volume_surfs permanent

         allocate ( km(iseqkm)%volume_surfs(lenvolsurfs) )
         km(iseqkm)%volume_surfs = volume_surfs(1:lenvolsurfs)
         kelmag = lenvolsurfs

         if ( debug ) then

!        --- debug = true, print array volume_surfs

            write(irefwr,*) 'Surfaces corresponding to volumes'
            call prinnmeshx ( nvolms, volume_surfs )

         end if

         deallocate ( volume_surfs, stat = error )
         if ( error/=0 ) call erdealloc ( error, 'volume_surfs' )

         volume_surfs => km(iseqkm)%volume_surfs

      end if
1000  call erclos ( 'mshfillkmqs' )

      end subroutine mshfillkmqs
