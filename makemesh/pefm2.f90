! pefm2
! Fill arrays iinput and rinput to be used in the mesh
! creation. The mesh is built by meshgenerator RECTANGLE
! for a rectangular geometry ([0,xl]x[0,yl]). To include
! refinements in the 
subroutine pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,ichoice)
implicit none
      
integer :: iinput(*),nx(*),ny(*),ichoice
real(kind=8) :: rinput(*),fx(*),fy(*),xl,yl
real(kind=8) :: x(2000),y(2000)
integer :: ipsurf,nusx,nusy,nuspnt,ncurvs,nsurfs,nsurel,irenumber
integer :: iileng,irleng,i,ielgrp,isurf,ic,ip,ishape,n,j
integer :: ifirst,ilast,ipcurv,ipbase,icrv
real(kind=8) :: calcd,dx,dy,yy,xx
data ipsurf/0/
save ipsurf

if (xl.le.0d0.or.yl.le.0d0) then
   write(6,*) 'PERROR(pefm2): xl <= 0 or yl <= 0'
endif
if (ichoice.eq.2.and.ipsurf.eq.0) then
   write(6,*) 'PERROR(pefm2): ichoice=2 and ipsurf=0'
endif

nusx      = nx(1)+nx(2)+nx(3)
nusy      = ny(1)+ny(2)+ny(3)
nuspnt    = 4*nusx+4*nusy
ncurvs    = 4
nsurfs    = 1
nsurel    = 1
irenumber = 0

iileng = nuspnt + 7 + ncurvs*4 + 20 + 13
irleng = 7+2*nuspnt
if (iinput(1).lt.iileng) then
   write(6,*) 'PERROR(pefm2): array iinput too small'
   write(6,*) '                 computed length = ',iileng
endif
if (rinput(1).lt.irleng) then
   write(6,*) 'PERROR(pefm2): array rinput too small'
   write(6,*) '                 computed length = ',irleng
endif

if (ichoice.eq.1) then
   ! Fill iinput and rinput for the first time.
   ! Eight curves are defined by userpoints; RECTANGLE
   ! creates one surface with quadratic triangles. 
   do i=2,20
      iinput(i) = 0
   enddo
   iinput(6) = 2
   iinput(7) = nuspnt
   iinput(8) = ncurvs
   iinput(9) = nsurfs
   iinput(12)= nsurel
   iinput(15)= irenumber

   ipcurv = 21
   ipbase = 1
   do icrv = 1,ncurvs
      ! pointer to curve information in iinput
      if (icrv.eq.1.or.icrv.eq.3) then
         n = 2*nusx
      else
         n = 2*nusy
      endif
      iinput(ipcurv)   = 2
      iinput(ipcurv+1) = 1
      iinput(ipcurv+2) = n
      iinput(ipcurv+3) = 0
      do ip = 1,n+1
         iinput(ipcurv+3+ip) = ipbase+ip-1
      enddo
      ipcurv = ipcurv + 4 + n + 1
      ipbase = ipbase + n
   enddo
   ! correct last user point number
   iinput(ipcurv-1) = 1        

   ! surface information
   ipsurf = ipcurv
   isurf = 1
   ishape = 4
   iinput(ipsurf)   = isurf
   iinput(ipsurf+1) = ishape
   iinput(ipsurf+2) = ncurvs
   do ic = 1,ncurvs
      iinput(ipsurf+2+ic) = ic
   enddo
   iinput(ipsurf+7) = nusx
   iinput(ipsurf+8) = nusy

   ! Element group information
   ielgrp = 1
   ifirst = 1
   ilast  = 1
   iinput(ipsurf+9) = ishape
   iinput(ipsurf+10) = ielgrp
   iinput(ipsurf+11) = ifirst
   iinput(ipsurf+12) = ilast 

   ! Fill RINPUT with the coordinates of the user points
   dx = calcd(nx,fx,xl)
   call calcoor(nx,fx,dx,x)
   write(6,*) 'x-coor: '
   do i=3,2*(nx(1)+nx(2)+nx(3))+1,2
      write(6,*) i,x(i),x(i)-x(i-2)
   enddo
   dy = calcd(ny,fy,yl)
   call calcoor(ny,fy,dy,y)
   write(6,*) 'y-coor: '
   do i=3,2*(ny(1)+ny(2)+ny(3))+1,2
      write(6,*) i,y(i),y(i)-y(i-2)
   enddo

   open(10,file='mesh_plot.dat')
   do i=2*(ny(1)+ny(2)+ny(3)),1,-1
      do j=1,2*(nx(1)+nx(2)+nx(3))
         write(10,*) x(j),y(i)
      enddo
   enddo
   close(10)
 
           
   do ip=2,7
      rinput(ip)=0d0
   enddo
   ipbase = 8
   do ip=1,2*nusx+1
      xx = x(ip)
      yy = 0d0         
      rinput(ipbase)   = xx
      rinput(ipbase+1) = yy
      ipbase = ipbase+2
   enddo
   do ip=2,2*nusy+1
      xx = xl
      yy = y(ip)
      rinput(ipbase)   = xx
      rinput(ipbase+1) = yy
      ipbase = ipbase+2
   enddo
   do ip=2*nusx,1,-1
      xx = x(ip)
      yy = yl
      rinput(ipbase)   = xx
      rinput(ipbase+1) = yy
      ipbase = ipbase+2
   enddo
   do ip=2*nusy,2,-1
      yy = y(ip)
      xx = 0d0
      rinput(ipbase)   = xx
      rinput(ipbase+1) = yy
      ipbase = ipbase+2
   enddo


else if (ichoice.eq.2) then
   ! Use the same nodalpoint distribution and RECTANGLE to
   ! create mesh with linear triangles
   ishape = 3
   iinput(ipsurf+1)  = ishape
   iinput(ipsurf+7) = nusx*2
   iinput(ipsurf+8) = nusy*2
   iinput(ipsurf+9) = ishape
   iinput(ipsurf+10) = 1
   iinput(ipsurf+11) = 1
   iinput(ipsurf+12) = 1

else if (ichoice.eq.3) then
   ! Use the same nodal point distribution and RECTANGLE to
   ! create mesh with bilinear quadrilaterals
   ishape = 5
   iinput(ipsurf+1)  = ishape
   iinput(ipsurf+7) = nusx*2
   iinput(ipsurf+8) = nusy*2
   iinput(ipsurf+9) = ishape
   iinput(ipsurf+10) = 1
   iinput(ipsurf+11) = 1
   iinput(ipsurf+12) = 1
      
else
   write(6,*) 'PERROR(pefm2): unknown value for ICHOIS'
   write(6,*) '                 ICHOIS = ',ichoice
      
endif
         
end
