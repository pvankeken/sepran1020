      subroutine sepmeshsub ( ichois, iinputusr, rinputusr )
! ======================================================================
!
!        programmer    Guus Segal
!        version  6.0  date 06-01-2016 Use sepmodulecomio
!        version  5.3  date 11-11-2014 Extra debug
!        version  5.2  date 29-05-2014 Extension of timebefore
!
!   copyright (c) 1997-2016  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     create mesh, fill array kmesh
!
! **********************************************************************
!
!                       KEYWORDS
!
!     mesh
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulemain
      use sepmoduleplot
      use sepmodulekmesh
      use sepmodulemesh
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ichois, iinputusr(*)
      double precision rinputusr(*)

!     ichois     i    Choice parameter consisting of two parts jchois and itopol
!                     according to ichois = jchois + 100 * itopol
!                     with:
!                     jchois
!                     0  standard method,  input from standard input file
!                        the mesh is created for the first time
!                     1  input is read from the arrays iinput and rinput
!                        the mesh is created for the first time
!                     2  input is read from the standard input file and stored
!                        in the arrays iinput and rinput
!                        the mesh is created for the first time
!                     3  input is read from the arrays iinput and rinput
!                        the co-ordinates of the existing mesh are changed
!                        the topological description remains unchanged
!                     4  input is read from the arrays iinput and rinput.
!                        the coordinates and the topological description of the
!                        mesh are changed, however the input as stored in iinput
!                        and rinput is the same as in a preceding call of mesh.
!                        The number of points and curves remains the same, as
!                        well as the number of elements along the curves.
!                        The coordinates of the user points and the curves are
!                        changed by a call of subroutine chanbn and as a
!                        consequence the number elements in the surfaces may be
!                        changed.
!                        This possibility must be preceded by calls of
!                        subroutine mesh and subroutine chanbn
!                     5  See jchois = 4, however, the topological description
!                        remains unchanged
!                     6  Only the arrays iinput and rinput are filled
!                        The subroutine is returned without actually creating a
!                        mesh
!                     7  The input must be stored in the arrays IINPUT and
!                        RINPUT
!                        The subroutine creates the curves and stores the
!                        information of curves and user points via KMESH
!                        After this step the subroutine returns
!                        This possibility is meant for program SEPGEOM
!                    10  The boundary of the mesh has been changed by subroutine
!                        ADAPBN. Information with respect to this change
!                        has been stored in the arrays ICURVS, CURVES,IINPUT
!                        and RINPUT, which are all part of array KMESH
!                        The input/output parameters IINPUT and RINPUT are not
!                        used.
!                        The topological description of the mesh may be changed
!                    11  See ICHOIS=10, however, the topological description
!                        may not be changed
!                    12  See ICHOIS=10, however, also the description of the
!                        surfaces has been changed
!                    51-53  See ICHOIS = 0, however, the first record has been
!                        read by READSP, NDIM = ICHOIS - 50
!                     itopol
!                     0  subroutine topol is called (standard situation)
!                     1  subroutine topol is skipped (must only be used when the
!                        mesh will be written by meshwr)
!     iinputusr i/o   integer input array
!                     is only used when jchois # 0
!                     see programmer's guide
!     rinputusr i/o   standard sepran array
!                     is only used when jchois # 0
!                     see programmer's guide
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer itopol, jchois, lengin, lengrl, &
              jtimsv, jmarsv, krenum(10), ireadr, &
              lengthin, lengthrl, lniinp, &
              lnrinp, nvolinsurfs, sumextrasurfs
      double precision timebefore(2)
      logical jnew, jread, jlocal
      integer, allocatable :: iinput(:), iinputcp(:)
      double precision, allocatable :: rinput(:), rinputcp(:)

!     iinput         integer input array
!                    Is defined in the same way as iinputusr from position 6
!     iinputcp       Contains copy of iinput
!     ireadr         If 1 the first record has been read by READSP
!     itopol         See ICHOIS
!     jchois         See ICHOIS
!     jlocal         indication whether iinput and rinput are local arrays
!                    (jlocal=true), i.e. stored in IBUFFR or not (false)
!     jmarsv         Help variable to store the old value of jkader temporarily
!     jnew           indication whether the mesh is new (true) or old
!     jread          Indicates if the input from the standard input file (true)
!                    must be read or not (false)
!     jtimsv         Help variable to save jtimes
!     krenum         Array of length 10 with information about the
!                    renumbering.
!     lengin         Length of integer array iinput
!     lengrl         Length of real array rinput (in double precision variables)
!     lengthin       Length of integer array iinput
!     lengthrl       Length of real array rinput (in double precision variables)
!     lniinp         Available space for array IINPUT
!     lnrinp         Available space for array RINPUT
!     nvolinsurfs    Number of volumes with internal surfaces
!     rinput         Real input array
!                    Is defined in the same way as rinputusr from position 6
!     rinputcp       Contains copy of rinput
!     sumextrasurfs  sum of number of extra surfaces
!     timebefore     Time at entrance of subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERALLOC        Produce error message in case allocate went wrong
!     ERCLMN         Resets old name of previous subroutine of higher level
!     ERDEALLOC      Produce error message in case deallocate went wrong
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     ERSETTIME      Get the CPU time at start of subroutine
!     ERTRACE        Prints trace back of subroutines
!     MSHCRT         Create the mesh
!     MSHDELSEL      Delete a selected set of subarrays of kmesh
!     MSHNORM2D      Compute normals on boundary in R^2 and store in array
!                    normal
!     MSHNORM3D      Compute normals on boundary in R^3 and store in array
!                    normal
!     MSHOUTERBOUN2D Compute the outer boundary curves of a region in R^2 and
!                    sort this boundary such that it is always counterclockwise
!     MSHOUTERBOUN3D Compute the outer boundary surfaces of a region in R^3 and
!                    sort this boundary such that the normal is directed
!                    outwards
!     MSHOUTERCHECK  Compute the outer boundary of amesh from the single
!                    edges /faces in the  mesh and compare with the previously
!                    computed outer boundary
!     MSHRDMESHINPUT Read input from standard input file
!                    Store arrays IINPUT and RINPUT in IBUFFR
!     MSHSPECIAL     Creates special rectangular grid
!     MSHUPDMESHINP1 Compute number of volumes with internal surfaces
!     MSHUPDMESHINP2 Fill array iinputnew with information of extra surfaces
!     PLOTRENNODES   Plot renumbered nodes
!     PRINLG         Print and checks length of user arrays
!     SEPCRMESHINFO  Create permanent space for mesh
!     SEPPRINKMESH   Print array KMESH
!     SEPRELKMESH1   Make the relation between kmref and the pointers
!                    in sepmodulekmeshp
!     TOPOL          Fill integer information corresponding to the mesh in KMESH
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!       1   ICHOIS is incorrect
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'sepmeshsub' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from sepmeshsub'
         call ertrace
  1      format ( a, 1x, (10i10) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000
      if ( itime>0 ) call ersettime ( timebefore )
      if ( kmesh<=0 .or. kmesh>lastmesh ) kmesh = 0
      iseqkm = kmesh
      if ( iseqkm==0 ) call sepcrmeshinfo ( kmesh )
      call seprelkmesh1
      jmarsv = jmark
      jtimsv = jtimes
      itopol = ichois/100
      jchois = ichois-100*itopol
      if ( jchois>50 ) then
         ireadr = 1
         ndim = jchois - 50
         jchois = 0
      else
         ireadr = 0
         ndim = 0
      end if
      if ( debug ) &
         write(irefwr,1) 'ichois, jchois, itopol, ireadr, ndim', &
                          ichois, jchois, itopol, ireadr, ndim
      call errint ( ichois, 1 )
      if ( jchois<0 .or. jchois>7 .and. jchois<10 ) &
          call errsub ( 1, 1, 0, 0 )
      if ( itopol<0 .or. itopol>1 ) call errsub ( 1, 1, 0, 0 )
      if ( jchois>=3 .and. jchois<=5 .or. jchois>=10 ) then
         jnew = .false.
      else
         jnew = .true.
      end if
      if ( debug ) write(irefwr,*) 'jnew ', jnew

      if ( jchois==1 .or. jchois>=3 .and. jchois<=5 &
           .or. jchois==7 .or. jchois>=10 ) then
         jread = .false.
      else
         jread = .true.
      end if
      if ( debug ) write(irefwr,*) 'jread ', jread

      if ( jread ) then

!     --- iinput and rinput must be read

         lniinp = 500000
         lnrinp = 500000

      else if ( jchois<10 ) then

!     --- iinput and rinput must be copied from iinputusr and rinputusr

         lniinp = iinputusr(1)-5
         lnrinp = nint(rinputusr(1))-5

      else

!     --- jchois>=10  read input from arrays iinput and rinput as
!         stored in kmesh

         lniinp = size ( iinputkm )
         lnrinp = size ( rinputkm )

      end if  ! ( jread .or. jchois>=10 )
      lengin = lniinp  ! available space for iinput
      lengrl = lnrinp  ! available space for rinput

      if ( debug ) write(irefwr,1) 'lniinp, lnrinp', lniinp, lnrinp

      allocate ( iinput(lniinp), rinput(lnrinp), stat = error )
      if ( error/=0 ) call eralloc ( error, lniinp+lnrinp, 'iinput' )
      if ( ierror/=0 ) go to 1000

      if ( jchois>=0 .and. jchois<=2 .or. jchois==4 .or. &
           jchois==10 .or. jchois==12 ) then

!     --- The topological description may be changed
!         Remove restricted parts of kmesh if they exist

         call mshdelsel ( kmesh )

      end if
      if ( ierror/=0 ) go to 1000

      if ( debug ) write(irefwr,*) 'jread', jread

      if ( jread ) then

!     --- jread = true   read input from standard input file
!                        Store arrays IINPUT and RINPUT in IBUFFR

         call mshrdmeshinput ( iinput, rinput, ichois, jlocal, &
                               ireadr, krenum, lengin, lengrl )

         if ( debug ) write(irefwr,1) 'ispecial', ispecial
         if ( ispecial==1 ) then

!        --- Special case: the user defines the mesh by giving the co-ordinates
!            of the nodes himself

            call mshspecial ( iinput, rinput )
            deallocate ( iinput, rinput, stat = error )
            if ( error/=0 ) call erdealloc ( error, 'iinput' )
            go to 1000

         end if  ! ( ispecial==1 )

      else if ( jchois<10 ) then

!     --- jread = false  read input from arrays iinput and rinput

         iinput(1:lniinp-5) = iinputusr(6:lniinp)
         rinput(1:lnrinp-5) = rinputusr(6:lnrinp)
         jcoars = iinputusr(16)
         jlocal = .false.

      else

!     --- jchois>=10  read input from arrays iinput and rinput as
!         stored in kmesh

         iinput = iinputkm
         rinput = rinputkm
         jlocal = .true.
         jcoars = iinput(11)

      end if
      if ( ierror/=0 ) go to 1000

      if ( debug ) then
         write(irefwr,1) 'lengin, lengrl/1', lengin, lengrl
         write(irefwr,*) 'jlocal', jlocal
      end if  ! ( debug )

!     --- Copy old iinput/rinput into iinputcp/rinputcp and remove iinput/rinput

      allocate ( iinputcp(lengin), rinputcp(lengrl), stat = error )
      if ( error/=0 ) call eralloc ( error, lengin+lengrl, 'iinput' )
      if ( ierror/=0 ) go to 1000

      iinputcp = iinput(1:lengin)
      rinputcp = rinput(1:lengrl)

      deallocate ( iinput, rinput, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'iinput' )
      if ( ierror/=0 ) go to 1000

!     --- Check if internal surfaces are generated,
!         If so update the mesh to create one surface of outer surfaces
!         and internal surfaces
!         Compute number of volumes with internal surfaces

      if ( debug ) write(irefwr,*) 'before mshupdmeshinp1'
      call mshupdmeshinp1 ( iinputcp, nvolinsurfs, sumextrasurfs )

!     --- allocate new iinput with extended length

      lengthin = nvolinsurfs*4+sumextrasurfs
      lengthrl = nvolinsurfs*4
      if ( debug ) write(irefwr,1) 'lengthin, lengthrl/1', lengthin, lengthrl
      allocate ( iinput(lengin+lengthin), rinput(lengrl+lengthrl), &
                 stat = error )
      if ( error/=0 ) &
         call eralloc ( error, lengin+lengrl+lengthin+lengthrl, &
                        'iinput' )
      if ( ierror/=0 ) go to 1000

      if ( nvolinsurfs>0 ) then

!     --- there are volumes with internal surfaces

         if ( debug ) write(irefwr,*) 'before mshupdmeshinp2'
         call mshupdmeshinp2 ( iinputcp, iinput, nvolinsurfs, &
                               sumextrasurfs, lengin, &
                               rinputcp, rinput, lengrl )

      else

!     --- Copy contents of iinputcp/rinputcp back into iinput/rinput

         iinput(1:lengin) = iinputcp(1:lengin)
         rinput(1:lengrl) = rinputcp(1:lengrl)

      end if  ! ( nvolinsurfs>0 ) then

      deallocate ( iinputcp, rinputcp, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'iinputcp' )
      if ( ierror/=0 ) go to 1000
      if ( debug ) write(irefwr,1) 'lengin, lengrl/2', lengin, lengrl

      if ( debug ) write(irefwr,*) 'before mshcrt'
      if ( jchois/=6 ) then

!     --- jchois#6, create mesh

         call mshcrt ( jnew, jchois, iinput, rinput, krenum, jcoars )
         if ( ierror/=0 ) go to 1000

      end if
      if ( debug ) write(irefwr,*) 'after mshcrt'

      if ( jchois/=0 .and. (jchois<10 .or. jchois>12) ) then

!     --- jchois # 0, 10, 11,  fill iinputusr and rinputusr

         if ( .not. jlocal ) then
            lengin = iinp8
            lengrl = irnp3
         else
            lengin = size(iinputkm)
            lengrl = size(rinputkm)
         end if
         if ( debug ) write(irefwr,1) 'lengin, lengrl/3', lengin, lengrl

!        --- Check if arrays are long enough

         call prinlg ( 'iinput', lengin+5, rinputusr, iinputusr )
         call prinlg ( 'rinput', -lengrl-5, rinputusr, iinputusr )
         if ( ierror/=0 ) go to 1000

         iinputusr(6:lengin+5) = iinputkm(1:lengin)
         rinputusr(6:lengrl+5) = rinputkm(1:lengrl)

      end if

      if ( jchois==6 .or. ierror/=0 ) go to 1000
      if ( jchois/=3 .and. jchois/=5 .and. jchois/=7 .and. &
           jchois/=11 .and. itopol==0 .and. notopol==0 ) then

!     --- Compute topological description

         if ( debug ) write(irefwr,*) 'before topol'
         call topol ( krenum )

         if ( kelmu>0 ) nspec = kmeshu(2)
         if ( ierror/=0 ) go to 1000
         if ( debug ) write(irefwr,*) 'after topol'

!        --- Compute outer boundary and normals

         if ( ndim==2 .and. nsurfs>0 ) then

!        --- ndim = 2, and nsurfs>0, store normals along curves
!            and store outer boundary

            call mshouterboun2d
            call mshnorm2d

         else if ( ndim==3 .and. nvolms>0 ) then

!        --- ndim = 3, and nvolms>0
!            store normals along surfaces and store outer boundary
!            In case of spectral elements the normal is not computed yet

            if ( debug ) write(irefwr,*) 'before mshouterboun3d'
            call mshouterboun3d
            if ( kelmu==0 ) call mshnorm3d

         end if

      end if
      if ( ierror/=0 ) go to 1000

      if ( icheck>=2 .and. ndim==2 .and. nspec==0 ) then

!     --- check level>1, check outer boundary

         call mshoutercheck

      end if

      if ( icheck==3 ) then

!     --- Print selected mesh information

         call sepprinkmesh ( -1 )

      else if ( icheck>3 ) then

!     --- Print complete mesh information

         call sepprinkmesh ( 0 )

      end if

      if ( ndim<=2 .and. irenpl==1 ) then

!     --- Plot renumbered nodes

         call plotrennodes

      end if
      if ( ierror/=0 ) go to 1000
      jmark = jmarsv
      jtimes = jtimsv

1000  if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End sepmeshsub'

      end if  ! ( debug )
      call erclmn ( 'sepmeshsub', timebefore )
      end subroutine sepmeshsub
