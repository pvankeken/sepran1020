constants
  reals
    1: slab_velocity=2.18 # 5 cm/yr based on h/kappa=1000/0.7272e-6
    2: eps=0      # penalty function parameter
    3: rho=1         # reference density
    4: eta=1         # reference viscosity
    5: T_mantle=1300 # mantle temperature
    6: eps_loop=1d-4 # Convergence criterion for main loop
    7: shear_heating= 0  # 11.26 = non-dimensional value for 0.029 W/m^2
    8: shear_gradient=0   # in 2.5755e-3 W/m^2/km
    9: sub_epsloop = 0.05  # convergence criterion for velocity sub loop
   10: vgrad_length = 2 # smearing interval ; suggested: about 3x element size in corner
  integers
    1: veloc=1       # velocity vector
    2: temp=2        # temperature vector
    3: veloc_old=4   # old velocity vector
    4: temp_old=5    # old temperature vector
    5: dtdx=6        # dT/dx vector
    6: L1=3          # Use L1 norm
    7: upwinding=0   # Il'in upwinding
    8: ibench_type=11  # 1x=isoviscous 1a-1c, 2x=2a-2b
    9: maxiter=1     # maximum number of iterations
   10: log_viscosity=7   # log viscosity vector
   11: max_subiter=20 # maximum number of subloop iterations
   12: empty2=0
   13: empty3=0
   14: pressure=8 # pressure
   15: dpdx=9  # dP/dx
   16: dpdy=10 # dP/dy
   17: secsqrt=11 # secsqrt
   18: itype_stokes=903
   19: velocall=3
   20: velocint=12
  variables
    1: vel_dif       # difference of velocity/temperature between iterations
    2: temp_dif
    3: vel_max       # maximum of velocity/temperature
    4: temp_max
   10: surface_heatflow=0 # surface heatflow
   11: vrms = 0           # rms velocity
   12: itypv = 0           # type of viscosity
  vector_names
    1: nVELOCITY
    2: nTEMPERATURE
    3: nVELOC_ALL
    4: nV_old
    5: nT_old
    6: ndT_dx
    7: nLOG_VISCOSITY
    8: nPRESSURE
    9: nDPDX
   10: nDPDY
   11: nSECSQRT
   12: nVELOCINT
end

start
   name_back='sepranback'
   database=new
   renumber best profile
end

problem $veloc
   types
     elgrp1=0  # crust: V=0 (not computed)
     elgrp2=0  # slab: V=constant (not computed)
     elgrp3=903  # Solve Stokes equations with penalty function method
   essboundcond
*    *** Essential boundary conditions are set 1) where flow components
*    *** are zero; 2) where velocity is prescribed. Actual values of b.c.
*    *** are set below.
     degfd1,degfd2=curves0(c107)
     degfd1,degfd2=curves200(c103)
     degfd3=point(p5)
   renumber levels (1,2),3
problem $temp
  types
     elgrp1=800   # crust
     elgrp2=800   # slab
     elgrp3=800   # wedge
  essboundcond
     curves0(c10)   # back arc inflow
     curves0(c22,c25,c23) # overriding plate
     curves0(c100) # top of model
     curves0(c4,c5) # inflow trench side
problem $velocall
  types 
     elgrp1=903
     elgrp2=903
     elgrp3=903
  essboundcond
     degfd1,degfd2=curves0(c107)
     degfd3=point(p5)
   renumber plast
end

*******************************************************
*  STRUCTURE
*
*  Provides the main structure of the program.
*  Prescribe actual values of b.c.
*  Solve Stokes equation
*  Solve Heat equation (using linear sub elements of quadratic
*      grid)
*  Derive temperature gradient
*  Output solutions.
*  Each of these steps has further information in subsections below.
******************************************************
structure
*  * *** Create initial conditions for velocity and temperature
   create_vector, sequence_number=1, vector=$veloc
   print_text 'after creation of vector'
*  plot_vector v$velocall
   prescribe_boundary_conditions, sequence_number=1, vector=$veloc
   solve_linear_system, seq_coef=$veloc, seq_solve=$veloc, vector=$veloc
   solve_linear_system, seq_coef=$temp, seq_solve=$temp, vector=$temp

   derivatives, seq_coef=$temp, seq_deriv=$temp, problem=$temp, vector=$dtdx


   compute_vector v$velocint map v$veloc problem=3
   compute_vector v$velocall v$velocall v$velocint
*  * slab surface velocity has been added twice
   prescribe_boundary_conditions, sequence_number=$velocall, vector=$velocall
   plot_contour v$temp
   plot_vector v$veloc
   plot_vector v$velocall
*  compute_scalar 1 norm=3 vector $velocall
*  print_scalar 1 text='maximum velocity'
*  print_vector $velocall curves=c107
   user_output,sequence_number=1
   output
end

matrix
   storage_method=mumps,problem=$veloc
   storage_method=mumps,problem=$temp
   problem=$velocall,storage_scheme=compact,symmetric
end

************************************************************
*  ESSENTIAL BOUNDARY CONDITIONS
*  Specify only those that are not zero.
************************************************************
essential boundary conditions $veloc
*  *** Velocity in the slab.
   curves0(c107),degfd1,func=10
   curves0(c107),degfd2,func=11
   point(p5),degfd3,value=0
essential boundary conditions $temp
*  *** inflow boundary
   curves0(c4,c5),func=1
*  *** Crustal part at wedge inflow
   curves0(c22,c25,c23),func=2
*  *** Wedge inflow
   curves0(c10),value=$T_mantle
essential boundary conditions $velocall
*  *** Velocity in the slab.
   curves0(c107),degfd1,value=0
   curves0(c107),degfd2,value=0
   point(p5),degfd3,value=0
end

************************************************
*  CREATE
*  First sequence: For initial condition (u,T)
*  Second sequence: for log(viscosity) vector.
*      Compute both log(eta) and log(secsqrt)
************************************************
create vector $veloc, problem $veloc, sequence_number=1
  func=12, degfd1, surfaces (s4)
  func=13, degfd2, surfaces (s4)
create vector $temp, problem $temp
create vector $velocall, problem $velocall
* set velocity in slab
  func=12, degfd1, surfaces(s5,s6)
  func=13, degfd2, surfaces(s5,s6)
end

create vector, problem=$veloc, sequence_number=2
  type=vector of special structure V2
* *** define viscosity through funcvect; use temp and secsqrt
* *** as 'old' vectors; only for wedge (surface 3)
  old_vector=1,seq_vector=V$temp,V$secsqrt,surfaces(s4)
end

***************************************
*  COEFFICIENTS
*  Specify coefficients of the differential equations
*  See Standard Problems guide (chapter 7 and 3).
***************************************
coefficients, sequence_number=$veloc, problem=$veloc
   elgrp3(nparm=20)
     icoef2 = 0
     icoef5 = 0
     coef6  = $eps # Penalty function parameter
     coef7  = $rho  # density (constant 1)
     coef12 = $eta  # viscosity  (constant 1)
end

coefficients, sequence_number=$temp, problem=$temp
   elgrp1(nparm=20) # CRUST + STATIONARY MANTLE

     icoef2= 0
     coef6 = 1     # diffusivity
     coef9 = coef6

   elgrp2(nparm=20) # SLAB
     icoef2 = $upwinding  # Il'in type upwinding
     coef6 = 1   # diffusivity
     coef9 = coef6
     coef12 = func = 10     # velocity specified through funccf=func
     coef13 = func = 11     # velocity specified through funccf=func

   elgrp3(nparm=20)  # WEDGE
     icoef2 = $upwinding  # Il'in type upwinding
     coef6 = 1   # diffusivity
     coef9 = coef6
     coef12 = old solution $veloc, degree of freedom 1
     coef13 = old solution $veloc, degree of freedom 2

end

****************************************************
*  SOLVE
*  Specification of solution methods
****************************************************
solve, sequence_number=1
*  *** iterative solution of Stokes equations
* iteration_method=bicgstab preconditioning=ilu print_level=1  //
*      rel_accuracy=1d-8,abs_accuracy=1d-8 start=old_solution
end

solve, sequence_number=2
*  *** iterative solution of heat equation
* iteration_method=bicg preconditioning=ilu print_level=1  //
*      rel_accuracy=1d-8,abs_accuracy=1d-8 start=old_solution
end

derivatives, seq_deriv=$temp
    type_output=special,ivec=2   # special structure type 2
    icheld=2   # Calculate grad T
    seq_input_vector=2  # use T as input vector
end




output
 write 3 solution
end
