real(kind=8) function funcbc(ichoice,x,y,z)
use sepmodulecons
implicit none
integer,intent(in) :: ichoice
real(kind=8),intent(in) :: x,y,z
real(kind=8) :: vgrad_length
real(kind=8) :: Thalfspacemodel,crustal_geotherm,func

funcbc=0.0_8
vgrad_length=rlcons(10)

funcbc=0.0_8
if (ichoice==1) then
   ! T at trench
   funcbc = Thalfspacemodel(-y)
else if (ichoice==2) then
   ! T at arc side
   funcbc = crustal_geotherm(-y)
else if (ichoice >= 10 .and. ichoice <=13) then
   ! velocity at top of slab
   ! get full velocity first
   funcbc=func(ichoice,x,y,z)
   if (ichoice>=10 .and. ichoice<=11) then
     ! now apply taper if necessary for wedge side b.c.
     if (x>=50.0_8 .and. x<=(50+vgrad_length)) then
        funcbc=funcbc*(x-50)/vgrad_length
        write(6,*) 'funcbc corrected: ',ichoice,x-50,funcbc
     endif
   endif
endif
end function funcbc
