! Batchelor
! Obtain solution in wedge through Batchelor's cornerflow solution
! BK 111900
real(kind=8) function batchelor(ichoice,x,y)
implicit none
integer,intent(in) :: ichoice
real(kind=8),intent(in) :: x,y
real(kind=8), parameter :: pi=4.0_8*atan(1.0_8)
real(kind=8) :: theta0,vx,vy,speed,stream,edot12

integer :: ii,nx,ny,ix,iy,ip

theta0=pi/4.0_8 ! dip of lower boundary
batchelor=0.0_8
call batchelor_v1(x,y,vx,vy,speed,stream,edot12,theta0)
if (ichoice==1) batchelor=-vx
if (ichoice==2) batchelor=vy

end function batchelor

! batchelor_v1
! Find velocity, streamfunction, and strainrate from Batchelor cornerflow solution
! in wedge with lower boundary dipping under angle theta0
! assume slab speed is unity 
subroutine batchelor_v1(x,y,vx,vy,speed,stream,edot12,theta0)
implicit none
real(kind=8),intent(in) :: x,y,theta0
real(kind=8),intent(out) :: vx,vy,speed,stream,edot12
real(kind=8) :: x0,y0,xprime,yprime,r,theta
real(kind=8) :: eps=1e-6_8 ! tolerance to check whether we're outside of the domain
real(kind=8) :: vr,vtheta,dfdtheta,ftheta,factor

! Define the coordinate system
!
!                ----------------> +y
!                |\
!                | \                The angle (y,r) is theta
!                |  \
!                |   \ r
!                |
!                v +x
!
! Make sure the boundary conditions are set the right way on both
! sides of the wedge:
x0=0
y0=0
xprime=x-x0
yprime=y-y0
r=sqrt(xprime*xprime+yprime*yprime)
theta=acos(xprime/r)
if (theta<=0.0_8) then 
   write(6,*) 'batchelor solution above wedge'
   vx=0.0_8
   vy=0.0_8
   speed=0.0_8
   stream=0.0_8
   edot12=0.0_8
else if (theta>theta0+eps) then
   write(6,*) 'batchelor solution in slab'
   vx=sin(theta0)
   vy=cos(theta0)
   speed=1.0_8
   stream=0.0_8
   edot12=0.0_8
else 
   ! inside wedge
   theta=theta0-theta
   factor=1.0_8/(theta0**2.d0-sin(theta0)**2.d0)

   ftheta=-theta0**2.d0*sin(theta)+(theta0-sin(theta0)*cos(theta0))*theta*sin(theta)+ &
     &         sin(theta0)**2.d0*theta*cos(theta)
   ftheta=factor*ftheta

   dfdtheta=-theta0**2.d0*cos(theta)+(theta0-sin(theta0)*cos(theta0))* (sin(theta)+theta*cos(theta))+ &
     &  sin(theta0)**2.d0*(cos(theta)-theta*sin(theta))
   dfdtheta=factor*dfdtheta
   vr=dfdtheta
   vtheta=-ftheta
! Perform the transformation from polar coordinates back to
! Cartesian coordinates.
   vx= vr*cos(theta0-theta)+vtheta*sin(theta0-theta)
   vy= vr*sin(theta0-theta)-vtheta*cos(theta0-theta)
   speed=sqrt(vx*vx+vy*vy)
   stream=r*ftheta
   edot12=theta0*theta0*sin(theta)+(theta0-sin(theta0)*cos(theta0))*(2*cos(theta)-theta*sin(theta))- &
     & sin(theta)**2.0_8*(2.0_8*sin(theta)+theta*cos(theta))
   edot12=factor*edot12/r
endif

end subroutine batchelor_v1
