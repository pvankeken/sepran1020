real(kind=8) function func(ichoice,x,y,z)
use sepmodulecons
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8) :: slab_velocity,batchelor,xt,yt
real(kind=8), parameter :: eps=1e-3

func=0.0_8
slab_velocity=rlcons(1)

! Benchmark velocities (V,-V)/sqrt(2)
if (ichoice==10) then
   func = slab_velocity/sqrt(2.0_8)
else if (ichoice==11) then
   func = - slab_velocity/sqrt(2.0_8)
else if (ichoice==12.or.ichoice==13) then
   if (x<-y) then 
      ! inside slab
      if (ichoice==12) func= slab_velocity/sqrt(2.0_8)
      if (ichoice==13) func=-slab_velocity/sqrt(2.0_8)
   else if (y>=-50) then
      ! inside overriding plate
      func=0.0_8
   else
      ! in wedge: use Batchelor solution
      xt = x-50.0_8
      yt = -50.0_8-y
      func = slab_velocity*batchelor(ichoice-11,xt,yt)
   endif
else if (ichoice==14.or.ichoice==15) then
   if (x<-y-eps) then 
      ! inside slab
      if (ichoice==14) func= slab_velocity/sqrt(2.0_8)
      if (ichoice==15) func=-slab_velocity/sqrt(2.0_8)
   endif
endif

end function func

