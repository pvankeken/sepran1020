real(kind=8) function crustal_geotherm(z)
use sepmodulecons
implicit none
real(kind=8),intent(in) :: z
integer :: ibenchtype
real(kind=8) :: Tmantle,z1=0,z2,z3,z4,H1,H2,H3,q1,q2,q3,q4,T1,T2,T3,T4
integer :: crustal_layers
!real(kind=8),parameter :: kc=2.5_8/3.0_8,km=1 ! reverse decision from past
real(kind=8),parameter :: kc=1,km=3.0_8/2.5_8 ! reverse decision from past
logical :: first=.true.
save first

ibenchtype=incons(8)
Tmantle=rlcons(5)
crustal_layers=nint(rlcons(24))
z2 = rlcons(25)
z3 = rlcons(26)
z4 = rlcons(27)

H1 = rlcons(28)
H2 = rlcons(29)
H3 = rlcons(30)

q1 = rlcons(31)

crustal_geotherm=0.0_8
if (ibenchtype==0) then
  q2 = q1 - (z2-z1)*H1
  q3 = q2 - (z3-z2)*H2
  q4 = q3 - (z4-z3)*H3

  T1 = 0
  T2 = -H1*(z2-z1)*(z2-z1)/(2d0*kc)+q1*(z2-z1)/kc+T1
  T3 = -H2*(z3-z2)*(z3-z2)/(2d0*kc)+q2*(z3-z2)/kc+T2
  T4 = -H3*(z4-z3)*(z4-z3)/(2d0*kc)+q3*(z4-z3)/kc+T3
  if (first) then
     write(6,'(''************** Continental geotherm ********* '')')
     write(6,*) 'First  layer has thickness: ',z2-z1,' and H=',H1
     write(6,*) 'Second layer has thickness: ',z3-z2,' and H=',H2
     if (crustal_layers==3) then
        write(6,*) 'Third layer has thickness: ',z4-z3,' and H=',H3
     endif
     write(6,*)
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.3)') z1,T1,q1/400d0
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.3)') z2,T2,q2/400d0
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.3)') z3,T3,q3/400d0
     if (crustal_layers==3) then
        write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.3)') z4,T4,q4/400d0
     endif
     first=.false.
  endif

  if (z<z2) then
     ! upper crust
     crustal_geotherm = -H1*(z-z1)*(z-z1)/(2d0*kc)+q1*(z-z1)/kc+T1
  else if (z<z3) then
     crustal_geotherm= -H2*(z-z2)*(z-z2)/(2d0*kc)+q2*(z-z2)/kc+T2
  else if (crustal_layers==3) then
     if (z<z4) then
       crustal_geotherm= -H3*(z-z3)*(z-z3)/(2d0*kc)+q3*(z-z3)/kc+T3
     else
       crustal_geotherm= T4 + q4/km*(z-z4)
     endif
  else
     crustal_geotherm= T3 + q3/km*(z-z3)
  endif
  crustal_geotherm = min(crustal_geotherm,Tmantle)
  return
endif
if (ibenchtype>0) then
   ! van Keken et al., 2008 benchmark
   crustal_geotherm=Tmantle
   if (z<50) then 
      crustal_geotherm = z*Tmantle/50.0_8
   endif
else
    
endif

end function crustal_geotherm

real(kind=8) function Thalfspacemodel(z)
use sepmodulecons
implicit none
real(kind=8),intent(in) :: z
integer :: ibenchtype
real(kind=8) :: Tmantle,rerf,ageMa,ages,akappa,denom,zd

! set defaults for age and diffusivity
ageMa=50.0e6_8 
akappa=1e-6 
Tmantle=rlcons(5)
ibenchtype=incons(8)
!if (ibenchtype>0) then
   ageMa=50.0e6_8
   akappa=0.7272727272e-6
!endif

ages=ageMa*365*24*3600
denom=2*sqrt(akappa*ages)/1000.
zd=z/denom
Thalfspacemodel=Tmantle*rerf(zd)
!write(6,'(''Th: '',4f12.3)') z,zd,Thalfspacemodel,Tmantle
end function Thalfspacemodel
