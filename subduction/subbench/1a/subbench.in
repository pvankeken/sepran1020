constants
  reals
    1: slab_velocity=2.18 # 5 cm/yr based on h/kappa=1000/0.7272e-6
    2: eps=0      # penalty function parameter
    3: rho=1         # reference density
    4: eta=1         # reference viscosity
    5: T_mantle=1300 # mantle temperature
    6: eps_loop=1d-4 # Convergence criterion for main loop
    7: shear_heating= 0  # 11.26 = non-dimensional value for 0.029 W/m^2
    8: shear_gradient=0   # in 2.5755e-3 W/m^2/km
    9: sub_epsloop = 0.05  # convergence criterion for velocity sub loop
   10: vgrad_length = 2 # smearing interval ; suggested: about 3x element size in corner
   24: crustal_layers = 2 # use 2 for upper/middle and 3 for upper/middle/lower crust
   25: z2 = 15 # depth of upper crust
   26: z3 = 40 # depth of middle crust
   27: z4 = 40 # depth of lower crust (ignored if crustal_layers=2)
   28: H1 = 0.0 # heat production in upper crust (0.5253 is equivalent to 1.31e-6 W/m^3)
   29: H2 = 0.0  # heat production in middle crust
   30: H3 = 0.0    # heat production in lower crust (ignored if crustal_layers=2)
   31: surface_heatflow = 26 # based on 65 mW/m^2
  integers
    1: veloc=1       # velocity vector
    2: temp=2        # temperature vector
    3: veloc_old=3   # old velocity vector
    4: temp_old=4    # old temperature vector
    5: dtdx=5        # dT/dx vector
    6: L1=3          # Use L1 norm
    7: upwinding=0   # Il'in upwinding
    8: ibench_type=11  # 1x=isoviscous 1a-1c, 2x=2a-2b
    9: maxiter=1     # maximum number of iterations
   10: log_viscosity=6   # log viscosity vector
   11: max_subiter=20 # maximum number of subloop iterations
   12: empty2=0
   13: empty3=0
   14: pressure=7 # pressure
   15: dpdx=8  # dP/dx
   16: dpdy=9  # dP/dy
   17: secsqrt=10 # secsqrt
  variables
    1: vel_dif       # difference of velocity/temperature between iterations
    2: temp_dif
    3: vel_max       # maximum of velocity/temperature
    4: temp_max
   10: vsurface_heatflow=0 # surface heatflow
   11: vrms = 0           # rms velocity
   12: itypv = 0           # type of viscosity
  vector_names
    1: nVELOCITY
    2: nTEMPERATURE
    3: nV_old
    4: nT_old
    5: ndT_dx
    6: nLOG_VISCOSITY
    7: nPRESSURE
    8: nDPDX
    9: nDPDY
   10: nSECSQRT
end

start
   name_back='sepranback'
   database=new
   renumber best profile
end

problem $veloc
   types
     elgrp1=0  # crust: V=0 (not computed)
     elgrp2=0  # slab: V=constant (not computed)
     elgrp3=903  # Solve Stokes equations with penalty function method
   essboundcond
*    *** Essential boundary conditions are set 1) where flow components
*    *** are zero; 2) where velocity is prescribed. Actual values of b.c.
*    *** are set below.
     degfd1,degfd2=curves0(c107)
     degfd1,degfd2=curves200(c103)
     degfd3=point(p5)
problem $temp
  types
     elgrp1=800   # crust
     elgrp2=800   # slab
     elgrp3=800   # wedge
  essboundcond
     curves0(c10)   # back arc inflow
     curves0(c22,c25,c23) # overriding plate
     curves0(c100) # top of model
     curves0(c4,c5) # inflow trench side
end

*******************************************************
*  STRUCTURE
*
*  Provides the main structure of the program.
*  Prescribe actual values of b.c.
*  Solve Stokes equation
*  Solve Heat equation (using linear sub elements of quadratic
*      grid)
*  Derive temperature gradient
*  Output solutions.
*  Each of these steps has further information in subsections below.
******************************************************
structure
*  * *** Create initial conditions for velocity and temperature
   create_vector, sequence_number=1, vector=$veloc
   prescribe_boundary_conditions, sequence_number=1, vector=$veloc
   solve_linear_system, seq_coef=$temp, seq_solve=$temp, vector=$temp
   derivatives, seq_coef=$temp, seq_deriv=$temp, problem=$temp, vector=$dtdx
   plot_contour v$temp
   user_output,sequence_number=1
   output
end

matrix
   storage_method=mumps,problem=$veloc
   storage_method=mumps,problem=$temp
end

************************************************************
*  ESSENTIAL BOUNDARY CONDITIONS
*  Specify only those that are not zero.
************************************************************
essential boundary conditions $veloc
*  *** Velocity in the slab.
   curves0(c107),degfd1,func=10
   curves0(c107),degfd2,func=11
   point(p5),degfd3,value=0
essential boundary conditions $temp
*  *** inflow boundary
   curves0(c4,c5),func=1
*  *** Crustal part at wedge inflow
   curves0(c22,c25,c23),func=2
*  *** Wedge inflow
   curves0(c10),func=2 # value=$T_mantle
end

************************************************
*  CREATE
*  First sequence: For initial condition (u,T)
*  Second sequence: for log(viscosity) vector.
*      Compute both log(eta) and log(secsqrt)
************************************************
create vector $veloc, problem $veloc, sequence_number=1
  func=12, degfd1, surfaces (s4)
  func=13, degfd2, surfaces (s4)
create vector $temp, problem $temp
end

create vector, problem=$veloc, sequence_number=2
  type=vector of special structure V2
* *** define viscosity through funcvect; use temp and secsqrt
* *** as 'old' vectors; only for wedge (surface 3)
  old_vector=1,seq_vector=V$temp,V$secsqrt,surfaces(s4)
end

***************************************
*  COEFFICIENTS
*  Specify coefficients of the differential equations
*  See Standard Problems guide (chapter 7 and 3).
***************************************
coefficients, sequence_number=$veloc, problem=$veloc
   elgrp3(nparm=20)
     icoef2 = 0
     icoef5 = 0
     coef6  = $eps # Penalty function parameter
     coef7  = $rho  # density (constant 1)
     coef12 = $eta  # viscosity  (constant 1)
end

coefficients, sequence_number=$temp, problem=$temp
   elgrp1(nparm=20) # CRUST + STATIONARY MANTLE

     icoef2= 0
     coef6 = 1 
     coef9 = coef6
     coef16 = 0 # func = 1 # radiogenic heating
     coef17 = 1 # func = 2 # variable density
*    coef17 =0.8181 

   elgrp2(nparm=20) # SLAB
     icoef2 = $upwinding  # Il'in type upwinding
     coef6 = 1   # diffusivity
     coef9 = coef6
     coef12 = func = 10     # velocity specified through funccf=func
     coef13 = func = 11     # velocity specified through funccf=func
     coef17 = 1

   elgrp3(nparm=20)  # WEDGE
     icoef2 = $upwinding  # Il'in type upwinding
     coef6 = 1   # diffusivity
     coef9 = coef6
     coef12 = old solution $veloc, degree of freedom 1
     coef13 = old solution $veloc, degree of freedom 2
     coef17 = 1

end

****************************************************
*  SOLVE
*  Specification of solution methods
****************************************************
solve, sequence_number=1
*  *** iterative solution of Stokes equations
* iteration_method=bicgstab preconditioning=ilu print_level=1  //
*      rel_accuracy=1d-8,abs_accuracy=1d-8 start=old_solution
end

solve, sequence_number=2
*  *** iterative solution of heat equation
* iteration_method=bicg preconditioning=ilu print_level=1  //
*      rel_accuracy=1d-8,abs_accuracy=1d-8 start=old_solution
end

derivatives, seq_deriv=$temp, problem=$temp
    type_output=special,ivec=2   # special structure type 2
    icheld=2   # Calculate grad T
    seq_input_vector=2  # use T as input vector
end



output
 write 1 solution
end
