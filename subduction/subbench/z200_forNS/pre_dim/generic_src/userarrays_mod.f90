module userarrays
implicit none
real(kind=8),dimension(:),allocatable :: funcx,funcy,temp,dtdx
real(kind=8),dimension(:,:),allocatable :: coor,uvp,secinv,pressure
end module userarrays
