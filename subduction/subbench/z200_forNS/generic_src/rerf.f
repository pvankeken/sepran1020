c *************************************************************
c *   RERF
c *   Calculate the error function. From numerical recipes
c *************************************************************
      real*8 function rerf(x)
      implicit none
      real*8 x
      integer iret
      real*8 delx,x0,x1,xx

      if (x.gt.4.) then
            rerf=1.
            goto 20
      endif
         iret=0
         rerf=0.
         delx=0.001
         x0=0.
10       x1=x0+delx
         if(x1.gt.x) then
            x1=x
            iret=1
         endif
         xx=(x0+x1)/2.
         rerf=rerf+dexp(-xx*xx)*(x1-x0)
         if(iret.eq.1) then
            rerf=rerf*2./dsqrt(3.1415926535d0)
            goto 20
         endif
         x0=x1
         goto 10
c
20     return
         end
