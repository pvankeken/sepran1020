subroutine userout(isol,isequence)
use sepmodulemain
use sepmodulecons
use control
implicit none
integer,intent(in) :: isol(*),isequence
integer, parameter :: NUNK1=1,NUNK2=3,NUNK3=6,NDIM=2
integer :: iinder(10),iinmaph(10),maph(3),idegfd,ichoice,ip,ip1,ix,iy,ibenchtype=0
character(len=120) :: namedof(4),fname
save maph,iinder,iinmaph

! reference conductivity
slab_velocity=0.0
kr=rlcons(34)
if (kr>3.5.or.kr<2.4) then
   write(6,*) 'PERROR(userout): kr rlcons(34) seems off'
   call instop
endif
! reference diffusivity
kappa_r = kr / (3300*1250)
! velocity in m/yr
slab_velocity_dim_myr=rlcons(1)
! non-dim velocity
slab_velocity = slab_velocity_dim_myr/(365.24*24*3600) * 1e3 / kappa_r
write(6,*) 'slab_velocity = ',slab_velocity
!call instop
if (isequence==2) return
   
ibenchtype=incons(8)
itype_stokes=incons(18)

!if (ibenchtype>0) then
   call userout_bm(isol,isequence)
!endif

if (isol(2)>0) then
   ichoice=1
   namedof(1)='temperature'
   idegfd=0
   fname='temp'
   call sol2vtu(ichoice,isol(2),fname,idegfd,namedof)
endif
! isol3 has velocity on entire grid
if (isol(3)>0) then
   ichoice=2
   namedof(1)='uvp'
   idegfd=0
   fname='uvp'
   call sol2vtu(ichoice,isol(3),fname,idegfd,namedof)
endif

end subroutine userout

subroutine userout_bm(isol,isequence)
use sepmodulemain
use sepmodulecons
use control
implicit none
integer,intent(in) :: isol(*),isequence
integer, parameter :: NUNK1=1,NUNK2=3,NUNK3=6,NPX=111,NPY=101,NDIM=2,NCOOR=NPX*NPY
integer, parameter :: NPMAX=20000
real(kind=8) :: funcx(6+NDIM*NPMAX),funcy(6+NDIM*NPMAX),temp(NCOOR),dtdx(NPX)
real(kind=8) :: coor(NDIM,NCOOR),uvp(NUNK3,NCOOR),secinv(NCOOR),pressure(NCOOR)
real(kind=8),parameter :: DPX=2.0_8,DPY=2.0_8
integer :: iinder(10),iinmaph(10),maph(3),mapuvp(3,NPY),idegfd,ichoice,ip,ip1,ix,iy,ibenchtype=0
integer :: ix60,iy60,i60,allocate_status,npcurvs,icurvs(2),i
real(kind=8) :: x,y,func,kc
character(len=80) :: fname
save maph,iinder,iinmaph,mapuvp

funcx(1)=6+NDIM*NPMAX
funcy(1)=6+NDIM*NPMAX

slab_velocity_dim_myr=rlcons(1)
kc = rlcons(32)
if (isequence==2) then
   ! values have been set
   ! return
endif


if (isequence==1) then
   ! interpolate temperature etc. on the 6x6 benchmark output grid
    iinmaph=0
    maph=0
    ! set up coordinate array
    ip=0
    do iy=1,NPY
       y = -(iy-1)*DPY
       do ix=1,NPX
          ip=ip+1
          x=(ix-1)*DPX
          coor(1,ip)=x
          coor(2,ip)=y
       enddo
    enddo
    write(6,*) 'Interpolate temperature',coor(1,1121),coor(2,1121)
    call intcoor(kmesh,kprob,isol(2),temp,coor,1,NCOOR,NDIM,iinmaph,maph)
    write(6,*) 'done interpolating'
    open(9,file='coor.dat') 
    do ip=1,NPY*NPX
       write(9,'(i5,2f15.3)') ip,coor(1,ip),coor(2,ip)
    enddo
    close(9)
   
    open(9,file='T.dat') 
    ip=0
    do iy=1,NPY
       write(9,'(111e15.7,:)') (temp(ip+ix),ix=1,NPX)
       ip=ip+NPX
    enddo
    close(9)

    ix60=31
    iy60=31
    i60=(iy60-1)*NPX+ix60
    open(9,file='T60.dat') 
    write(9,*) coor(1,i60),coor(2,i60),temp(i60),i60
    close(9)
    write(6,'(''spot T: '',3f15.3,i5)') coor(1,i60),coor(2,i60),temp(i60),i60

    icurvs(1)=0 
    icurvs(2)=100
    call compcr(0,kmesh,kprob,isol(5),-1,icurvs,funcx,funcy)
    fname='heatflow.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i-1),-funcy(5+i)*kc
    enddo
    close(9)
   icurvs(1)=0
    icurvs(2)=111
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='T_z40.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs
       write(9,*) funcx(5+2*i-1),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=0
    icurvs(2)=112
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='T_z15.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs
       write(9,*) funcx(5+2*i-1),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=0 
    icurvs(2)=108
    call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
    fname='wedgeU.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=50,200
       y=-i*1.0_8
       write(9,'(3f12.3)') y,func(12,220.0_8,y,y)
    enddo
    close(9)
    icurvs(1)=0 
    icurvs(2)=108
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='wedgeT.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i),funcy(5+i)
    enddo
    close(9)
    icurvs(1)=0 
    icurvs(2)=102
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='Tinput.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=0 
    icurvs(2)=109
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='slabT.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i),funcy(5+i)
    enddo
    close(9)

endif

end subroutine userout_bm

