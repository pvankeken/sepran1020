real(kind=8) function funccf(ichoice,x,y,z)
use sepmodulecons
use control
implicit none
integer :: ichoice
real(kind=8) :: x,y,z,func
real(kind=8) :: z1=0,z2,z3,z4,H1,H2,H3,H1d,H2d,H3d,kcd,kmd,kc,km,fac
integer :: crustal_layers

crustal_layers=nint(rlcons(24))
z2 = rlcons(25)
z3 = rlcons(26)
z4 = rlcons(27)

H1d = rlcons(28)
H2d = rlcons(29)
H3d = rlcons(30)
H1 = H1d*1e6/kr
H2 = H2d*1e6/kr
H3 = H1d*1e6/kr
kcd = rlcons(32)
kmd = rlcons(33)
kc = kcd/kr
km = kmd/kr

if (ichoice==1) then
   funccf = 0
   if (y>=-z2) then
      funccf=H1
   else if (y>=-z3) then
      funccf=H2
   else if (y>=-z4.and.crustal_layers==3) then
      funccf=H3
   endif
   if (x>218) write(6,'(''H '',6f12.3)') -y,funccf ! H1,H2,-y,z2,z3,funccf
   return
else if (ichoice==2) then
   if (y>-40) then
      funccf = 2700.0_8/3300.0_8
   else 
      funccf = 1.0_8
   endif
   if (x>218.and.y>-60) write(6,'(''r '',6f12.3)') -y,funccf
   return
else if (ichoice==3) then
   ! conductivity in crust
   if (y>-40) then
      funccf = kc 
   else 
      funccf = km
   endif
   if (x>218.and.y>-60) write(6,'(''k '',6f12.3)') -y,funccf
   return
else if (ichoice==4) then
   ! conductivity in mantle
   funccf = km
else
   funccf=0.0_8
   if (ichoice>=10.and.ichoice<=13) funccf=func(ichoice,x,y,z)
endif
   
end function funccf

