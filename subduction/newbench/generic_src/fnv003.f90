! FNV003
! Function used to specify viscosity in Gaussian points
!
! Called when MODELV=103 is specified for Stokes elements.
!
! Parameters
!     X,Y,Z  i  coordinates of local point
!     U,V,W  i  velocity components in local point
!     SECINV i  square of the second invariant of strain rate
!               tensor (II in SP Chapter 7)
!     NUMOLD i  Number of old vectors stored in UOLD
!     MAXUNK i  Maximum number of unknowns in UOLD
!     UOLD   i  Array containing old solution values in this point
!
! Output depends on ITYPV (SCALAR 12)
!
! ITYPV=2
!     Viscosity is temperature dependent. temperature is stored
!     in UOLD(1).
!     This implementation: Ol diffusion creep 
! ITYPV=4
!     Viscosity is temperature and strain rate dependent.
!     Temperature is stored in UOLD(1), strainrate is sqrt(secinv)
!     Creep law follows Karato & Wu, 1993.
!
!     .          -n                      n-1
!     e   = Ak mu   exp[-(E+pV)/RT] sigma     sigma
!      ij                                          ij
!
!     or
!             -1/n                    . (1-n)/n
!     eta = Ak     mu exp[(E+pV)/nRT] e
!
!     where Ak=3.5e22 s-1, mu=80 GPa, E=540e3 J/mol, n=3.5
!
!     for V=0 this translates to
!                                     . -0.7143
!     eta = 28968.6 exp [ 18557.3/T ] e
!
!     strain rate scales as 1/s or kappa/h^2 = 7.272e-13
!          (using k=2.5, rho=3300, cp=1250, h=1000)
!     so dimensional edot = model edot * 7.272e-3
!
! PvK 111400
real(kind=8) function fnv003(x,y,z,u,v,w,secinv,numold,maxunk,uold)
use sepmodulecons
implicit none
real(kind=8) :: x,y,z,u,v,w,secinv
integer :: numold,maxunk
real(kind=8) :: uold(numold, maxunk),temp,fnv003_inv
integer ifirst
real(kind=8) :: temp_slab,prefac
integer itypv
real(kind=8) :: n,E,R,A0,T_a,strain_power,p,Vact,secsqrt
save ifirst
data ifirst/0/

temp_slab = rlcons(5)
itypv=nint(scalars(12))

if (temp_slab.le.0) then
   write(6,*) 'PERROR(fnv003): temp_slab <= 0'
   write(6,*) 'set temp_slab (rlcon(5))'
   write(6,*) 'before using this function'
   call instop
endif
if (itypv.eq.0) then
   fnv003 = 1d0
   return
endif
if (itypv.ne.2.and.itypv.ne.4) then
   write(6,*) 'PERROR(fnv003): itypv has wrong value'
   write(6,*) 'itypv = ',itypv
   write(6,*) 'This should be 0,2 or 4'
   write(6,*) 'Check scalars(12)'
   call instop
endif


fnv003=1d0
if (itypv==2) then
!  Temperature dependent viscosity
   if (ifirst.eq.0) then
      write(6,'(''ETA(T)'')')
      ifirst=1
   endif
   temp = uold(2,1)

!  New rheology E_activation= 335 kJ/mol; T_ref=1200 C
!  (Olivine, diffusion creep)
!  prefac = exp(-335d3/(8.3145d0*(1200d0+273d0)))
!  prefac=1.3204345d-12

!  eta = A_diff*exp(335d3/(8.3145d0*(temp+273d0))); A_diff=1.32043e9 Pa.s
!  use reference viscosity of 1e21 Pa.s; A_diff/1e21 = 1.32043e-12
   fnv003 = 1.32043e-12_8*exp(40.291058e3_8/(temp+273e0_8))
!  Cutoff for viscosity (upper bound is eta_max=1e25 Pa.s or 1e4 non-dim):
   fnv003_inv = 1d0/fnv003 + 1e-4
   fnv003 = 1d0/fnv003_inv
!  write(6,'(e15.7)') fnv003


else if (itypv.eq.4) then

!  non-Newtonian, dislocation creep
   if (ifirst.eq.0) then
      write(6,'(''ETA(T,edot)'')')
      ifirst=1
   endif

   temp = uold(2,1)

   n=3.5
   E=540e3
   R=8.3145
   A0 = 28968.6_8
   Vact=0
   secsqrt = sqrt(secinv)
!  note that Sepran's definition of strain rate is two times
!  that common in geological applications. This is important
!  in defining the stress-dependent rheology!
   secsqrt = 0.5*secsqrt
   strain_power = (secsqrt*7.272e-13_8)**((1.0_8-n)*1.0_8/n)
   T_a = temp+273_8
   p=0
   fnv003 = A0*exp((E+p*Vact)/(n*R*T_a))*strain_power
   fnv003 = fnv003/1e21_8
   fnv003_inv = 1.0_8/fnv003 + 1e-4_8
   fnv003 = 1.0_8/fnv003_inv

endif

if (fnv003.le.0) then
   write(6,*) 'PERROR(fnv003): viscosity <= 0'
   write(6,*) 'x,y,z: ',x,y,z
   write(6,*) 'temp : ',temp
   write(6,*) 'secinv: ',secinv
   write(6,*) 'itypv:  ',itypv
   write(6,*) 'fnv003: ',fnv003
endif

end function fnv003

