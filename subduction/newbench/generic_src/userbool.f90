!   USERBOOL
!   Function used to test whether main steady state iteration
!   has converged. Also provides output on # of iterations,
logical function userbool(k)
use sepmodulecons
implicit none
integer :: k
real(kind=8) :: difference,eps_loop,dif1,dif2,gnus,vrms,eps_subloop
integer :: iteration,maxiter,iter_sub,max_subiter,itypv,nnew_sub
save iteration,iter_sub
data iteration,iter_sub/0,0/

itypv=nint(scalars(12))
nnew_sub=incons(15)

if (k.eq.1.or.k.eq.3) then
!  *** Main loop
   iter_sub=0
   eps_loop=rlcons(6)
   maxiter=incons(9)
   dif1=1
   dif2=1
   if (scalars(3).gt.0) dif1 = scalars(1)/scalars(3)
   if (scalars(4).gt.0) dif2 = scalars(2)/scalars(4)

   iteration=iteration+1

   write(6,*)
   write(6,*) 'ITERATION: ',iteration,max(dif1,dif2),scalars(12)
   write(6,*)

   if (k.eq.1) then
      if (max(dif1,dif2).gt.eps_loop) then
         userbool=.true.
      else
         userbool=.false.
      endif
   else
      if (max(dif1,dif2).gt.1d-2) then
         userbool=.true.
      else
         userbool=.false.
      endif
   endif
   if (itypv.eq.0) then
      userbool = .false.
   endif


   if (iteration.gt.maxiter) then
      userbool=.false.
   endif
   write(6,'(''dif1, dif2, eps, maxiter, userbool: '', 3e15.7,i5,L5)') &
     &               dif1,dif2,eps_loop,maxiter,userbool

else if (k.eq.2.and.nnew_sub.eq.1) then

!        *** sub loop for velocity
   eps_subloop=rlcons(9)
   max_subiter = incons(12)
   dif1=1
   if (scalars(3).gt.0) dif1 = scalars(1)/scalars(3)
      iter_sub=iter_sub+1
      write(6,*)  'SUB_ITERATION : ',iter_sub,dif1

      if (dif1.gt.eps_subloop) then
         userbool=.true.
      else
         userbool=.false.
      endif
      if (iter_sub.gt.max_subiter) then
         userbool=.false.
      endif
   else if (k.eq.2.and.nnew_sub.eq.0) then
      userbool = .false.
   endif

end function userbool
