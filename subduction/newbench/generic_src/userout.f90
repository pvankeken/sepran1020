subroutine userout(isol,isequence)
use sepmodulemain
use sepmodulecons
use sepmodulekmesh
use control
implicit none
integer,intent(in) :: isol(*),isequence
integer, parameter :: NUNK1=1,NUNK2=3,NUNK3=6,NDIMH=2
integer :: iinder(10),iinmaph(10),maph(3),idegfd,ichoice,ip,ip1,ix,iy,ibenchtype=0
character(len=120) :: namedof(4),fname
save maph,iinder,iinmaph
real(kind=8) :: pi,funcbc,rintbn(10),T100,volume

write(6,*) 'userout: ',isequence

! reference conductivity
slab_velocity=0.0
kr=rlcons(34)
if (kr>3.5.or.kr<2.4) then
   write(6,*) 'PERROR(userout): kr rlcons(34) seems off'
   call instop
endif
! reference diffusivity
kappa_r = kr / (3300*1250)
! velocity in m/yr
slab_velocity_dim_myr=rlcons(1)
! non-dim velocity
slab_velocity = slab_velocity_dim_myr/(365.24*24*3600) * 1e3 / kappa_r
write(6,*) 'slab_velocity = ',slab_velocity
!theta0=1.0_8/sqrt(2.0_8) ! default 45
xd=50 ! default coorhdinates coupling point
yd=-50
if (rlcons(37)>1e-7) then
   theta0=rlcons(37)
else
   write(6,*) 'PERROR(userout2): set theta0 as rlcons(37)'
   call instop
endif
!! this needs some work
!! change for batcher() definition
!!pi=4.0_8*atan(1.0_8)
!!theta0=pi-theta0
sintheta0=sin(theta0)
costheta0=cos(theta0)
z_d=rlcons(38)
if (z_d < 1) z_d=50e3 ! default if not filled
if (xd>1e-7) then
   xd=rlcons(35)
   yd=rlcons(36)
endif
!write(6,*) theta0*180.0_8/3.141592653_8
!call instop
if (isequence==2) return
   
ibenchtype=incons(8)
itype_stokes=incons(18)

if (isol(2)>0) then
   ichoice=1
   namedof(1)='temperature'
   idegfd=0
   fname='temp'
   call sol2vtu(ichoice,isol(2),fname,idegfd,namedof)
endif
! isol3 has velocity on entire grid
if (isol(3)>0) then
   ichoice=2
   namedof(1)='uvp'
   idegfd=0
   fname='uvp'
   call sol2vtu(ichoice,isol(3),fname,idegfd,namedof)
endif

!if (ibenchtype>0) then
   call userout_bm(isol,isequence)
!endif


end subroutine userout

subroutine userout_bm(isol,isequence)
use sepmodulemain
use sepmodulecons
use sepmodulekmesh
use control
implicit none
integer,intent(in) :: isol(*),isequence
integer, parameter :: NUNK1=1,NUNK2=3,NUNK3=6,NPX=111,NPY=101,NDIMH=2,NCOOR=NPX*NPY
integer, parameter :: NPMAX=20000
real(kind=8) :: funcx(6+NDIMH*NPMAX),funcy(6+NDIMH*NPMAX),temp(NCOOR),dtdx(NPX)
real(kind=8) :: coorh(NDIMH,NCOOR),uvp(NUNK3,NCOOR),secinv(NCOOR),pressure(NCOOR)
real(kind=8),parameter :: DPX=2.0_8,DPY=2.0_8
integer :: iinder(10),iinmaph(10),maph(3),mapuvp(3,NPY),idegfd,ichoice,ip,ip1,ix,iy,ibenchtype=0
integer :: ix60,iy60,i60,allocate_status,npcurvs,icurvs(2),i
real(kind=8) :: x,y,func,kc,funcbc,Tint110,rintbn(10),x1,xN,y1,yN,dS
real(kind=8) :: avT6,avvrms6,volume6,volume,T100
integer :: iintbn(10)
character(len=80) :: fname
save maph,iinder,iinmaph,mapuvp

funcx(1)=6+NDIMH*NPMAX
funcy(1)=6+NDIMH*NPMAX

slab_velocity_dim_myr=rlcons(1)
kc = rlcons(32)
if (isequence==2) then
   ! values have been set
   ! return
endif


if (isequence==1) then
   ! interpolate temperature etc. on the 6x6 benchmark output grid
    iinmaph=0
    maph=0
    ! set up coorhdinate array
    ip=0
    do iy=1,NPY
       y = -(iy-1)*DPY
       do ix=1,NPX
          ip=ip+1
          x=(ix-1)*DPX
          coorh(1,ip)=x
          coorh(2,ip)=y
       enddo
    enddo
    write(6,*) 'Interpolate temperature',coorh(1,1121),coorh(2,1121)
    call intcoor(kmesh,kprob,isol(2),temp,coorh,1,NCOOR,NDIMH,iinmaph,maph)
    write(6,*) 'done interpolating'
    open(9,file='coorh.dat') 
    do ip=1,NPY*NPX
       write(9,'(i5,2f15.3)') ip,coorh(1,ip),coorh(2,ip)
    enddo
    close(9)
   
    open(9,file='T.dat') 
    ip=0
    do iy=1,NPY
       write(9,'(111e15.7,:)') (temp(ip+ix),ix=1,NPX)
       ip=ip+NPX
    enddo
    close(9)
    write(6,*) 'Interpolate temperature in spot'
    coorh(1,1)=200
    coorh(2,1)=-100
    call intcoor(kmesh,kprob,isol(2),temp,coorh,1,1,NDIMH,iinmaph,maph)

    open(9,file='T100.dat') 
    write(9,*) coorh(1,1),coorh(2,1),temp(1)
    close(9)
    write(6,'(''spot T: '',3f15.3,i5)') coorh(1,1),coorh(2,1),temp(1)
    T100=temp(1)


    ! Find average temperature along part of the slab top (curve 110)
    iintbn=0
    iintbn(1)=8 ! number of entries
    iintbn(2)=1 ! int f u ds
    iintbn(4)=3 ! simpson's rule
    iintbn(6)=1 ! number
    iintbn(7)=110 ! icurve1
    iintbn(8)=110 ! icurve2
    call intbou(iintbn,rintbn,kmesh,kprob,isol(2),Tint110)
    open(9,file='Tint100.dat')
    write(9,*) Tint110
    close(9)
    theta0=rlcons(37)
    icurvs(1)=0
    icurvs(2)=110
    ! find start and end coorhdinates
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    npcurvs=nint(funcx(5)/2)
    x1=funcx(6)
    y1=funcx(7)
    xN=funcx(5+2*npcurvs-1)
    yN=funcx(5+2*npcurvs)
    write(6,'(''110 runs from: '',2f12.3,3x,2f12.3)') x1,y1,xN,yN
    dS=sqrt((x1-xN)*(x1-xN)+(y1-yN)*(y1-yN))
    write(6,'(''Tint110: '',3f15.3)') Tint110/dS,dS,ds*cos(theta0)

    ! Find average temperature in surface from 40 km down to the slab surface above curve 110 (surface 6)
    ! volint etc. do not allow selection geometrical surfaces (just element groups) so let's mask the solution vector
    call findav6(avT6,avvrms6,volume6,kmesh,kprob,isol(1),isol(2))
!   call findav6(1,avT6,kmesh,kprob,isol(2))
!   call findav6(2,avvrms6,kmesh,kprob,isol(1))
    volume=100*30+0.5*100*50
    write(6,'(''avt6   : '',i10,'' & '',3(f10.2,'' & ''),f10.4)') npoint,T100,Tint110/dS,avT6/volume,avvrms6*23.715
    open(9,file='integrals.dat')
    write(9,'('' & '',i10,'' & '',3(f10.2,'' & ''),f10.4,'' \\ '')') npoint,T100,Tint110/dS,avT6/volume,avvrms6*23.715
    close(9)

    icurvs(1)=1
    icurvs(2)=100
    call compcr(0,kmesh,kprob,isol(6),-1,icurvs,funcx,funcy)
    fname='heatflow.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i-1),-funcy(5+i)*kc
    enddo
    close(9)
    write(6,*) 'heatflow.dat entries: ',funcx(5)/2
   icurvs(1)=1
    icurvs(2)=103
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='T_z40.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs
       write(9,*) funcx(5+2*i-1),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=1
    icurvs(2)=107
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='T_z15.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs
       write(9,*) funcx(5+2*i-1),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=0 
    icurvs(2)=104
    call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
    fname='wedgeU.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs
       write(9,'(2f12.3)') funcx(5+2*i),funcy(5+i)
    enddo
    close(9)
    icurvs(1)=0 
    icurvs(2)=104
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='wedgeT.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i),funcy(5+i)
    enddo
    close(9)
    icurvs(1)=0 
    icurvs(2)=102
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='Tinput.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       write(9,*) funcx(5+2*i),funcy(5+i)
    enddo
    close(9)

    icurvs(1)=0 
    icurvs(2)=101
    call compcr(0,kmesh,kprob,isol(2),1,icurvs,funcx,funcy)
    fname='slabT.dat'
    open(9,file=fname)
    npcurvs=funcx(5)/2
    do i=1,npcurvs 
       x=funcx(5+2*i-1)
       y=funcx(5+2*i)
       write(9,'(5f15.7)') funcx(5+2*i-1),funcx(5+2*i),funcy(5+i),funcbc(10,x,y,y),funcbc(11,x,y,y)
    enddo
    close(9)

endif

end subroutine userout_bm

subroutine findav6(avT,avvrms,volume,kmesh,kprob,isol1,isol2)
use sepmodulecomio
!use sepmodulemain
use sepmodulekmesh
use sepmodulemeshinf
use sepmodulemesh
use sepmodulekprob
use sepmoduleprobinf
implicit none
real(kind=8),intent(out) :: avT,avvrms,volume
integer,intent(in) :: kmesh,kprob,isol1,isol2

call sepactsolbf1(isol2)
! find <T>
call findav6_00(npoint,npelm,nelem,nphysi,ndim,ks(isol2)%sol,nusoli,coor,kmeshc,avT,volume)
! 
call sepactsolbf1(isol1)
call findav6_01(npoint,npelm,nelem,nphysi,ndim,ks(isol1)%sol,nusoli,coor,kmeshc,kprobpi,avvrms)
!avT=0.0
!avvrms=0.0
!volume=0.0
end subroutine findav6

subroutine findav6_00(npoint,npelm,nelem,nphys,ndim,usol,nusol,coor,kmeshc,avT,volume)
implicit none
integer,intent(in) :: npoint,npelm,nelem,nphys,ndim,nusol,kmeshc(*)
real(kind=8),intent(in) :: coor(2,*),usol(*)
real(kind=8),intent(out) :: avT,volume
integer :: ip,i,j,k,ip1,ip2,ip3,ip12,ip13,ip23,nelem_in_area
real(kind=8) :: xc,yc,xl(3),yl(3),al(3),bl(3),cl(3),area_element,averageT=0
real(kind=8) :: x12=0,x13=0,x23=0,y12=0,y13=0,y23=0,u12=0,u13=0,u23=0,u123=0

if (npelm/=6) then
   write(6,*) 'PERROR(findav6_00): npelm should be 6 for P2 but is: ',npelm
   call instop
endif
avT=0.0
volume=0.0

ip=0
avT=0
!open(9,file='centroids.dat')
averageT=0
nelem_in_area=0
do i=1,nelem
   ! indices of vertices of current triangle
   ip1=kmeshc(ip+1)
   ip2=kmeshc(ip+3)
   ip3=kmeshc(ip+5)
   ip12=kmeshc(ip+2)
   ip23=kmeshc(ip+4)
   ip13=kmeshc(ip+5)
   ! centroid 
   xc=(coor(1,ip1)+coor(1,ip2)+coor(1,ip3))/3
   yc=(coor(2,ip1)+coor(2,ip2)+coor(2,ip3))/3
   ! Find those that lie within integration area
   if (xc<140.or.xc>240) then
      ! to left or right of integration regions
   else if (yc<-120.or.yc>-40) then
      ! below or above integration regions
   else if (yc>=-70 .or. yc>-70-0.5*(xc-140)) then
      ! either in square region well above slab
      ! or just above slab in triangular region
      !write(9,*) xc,yc
      xl(1)=coor(1,ip1)
      yl(1)=coor(2,ip1)
      xl(2)=coor(1,ip2)
      yl(2)=coor(2,ip2)
      xl(3)=coor(1,ip3)
      yl(3)=coor(2,ip3)
      ! find barycentric coordinatese and area of the element
      call trilin(xl,yl,al,bl,cl,area_element)
      volume=volume+area_element
      ! find coordinates of points halfway along the sides of the triangle
      x12=coor(1,ip12)
      x13=coor(1,ip13)
      x23=coor(1,ip23)
      y12=coor(2,ip12)
      y13=coor(2,ip13)
      y23=coor(2,ip23)
      ! get values of the solution
      u12=usol(ip12)
      u13=usol(ip13)
      u23=usol(ip23)
!     write(6,'(3(2f12.1,3x),5x,f12.1)') x12,y12,x13,y13,x23,y23,(u12+u13+u23)/3
      averageT=averageT+(u12+u13+u23)/3
      u123=-yc
!     Trapezoid rule exact for u in P2
      avT=avT+area_element/3*(u12+u13+u23)
      nelem_in_area=nelem_in_area+1
!     ! centroid formula (exact for u in P1)
!     avT=avT+area_element*u123
   endif
   ip=ip+6
enddo
averageT=averageT/nelem_in_area
!close(9)
write(6,*) 'volume   : ',volume,100*30 +0.5*100*50
write(6,*) 'avt/V    : ',avT/volume
write(6,*) 'averageT : ',averageT
!call instop

end subroutine findav6_00
 


subroutine findav6_01(npoint,npelm,nelem,nphys,ndim,usol,nusol,coor,kmeshc,kprobp,avvrms)
implicit none
integer,intent(in) :: npoint,npelm,nelem,nphys,ndim,nusol,kmeshc(*),kprobp(npoint,3)
real(kind=8),intent(in) :: coor(2,*),usol(*)
real(kind=8),intent(out) :: avvrms
integer :: ip,i,j,k,ip1,ip2,ip3,ip12,ip13,ip23,nelem_in_area
real(kind=8) :: xc,yc,xl(3),yl(3),al(3),bl(3),cl(3),area_element,averagevrms=0
real(kind=8) :: x12=0,x13=0,x23=0,y12=0,y13=0,y23=0,u12=0,u13=0,u23=0,u123=0,v12=0,v13=0,v23=0,v123=0
real(kind=8) :: volume=0,u1=0,u2=0,u3=0,v1=0,v2=0,v3=0,vint=0,uint=0,zint=0,x2int=0,y2int=0,x2y2int=0

if (npelm/=6) then
   write(6,*) 'PERROR(findav6_00): npelm should be 6 for P2 but is: ',npelm
   call instop
endif
avvrms=0.0
volume=0

ip=0
!open(9,file='centroids.dat')
averagevrms=0
nelem_in_area=0
do i=1,nelem
   ! indices of vertices of current triangle
   ip1=kmeshc(ip+1)
   ip2=kmeshc(ip+3)
   ip3=kmeshc(ip+5)
   ip12=kmeshc(ip+2)
   ip23=kmeshc(ip+4)
   ip13=kmeshc(ip+5)
   ! centroid 
   xc=(coor(1,ip1)+coor(1,ip2)+coor(1,ip3))/3
   yc=(coor(2,ip1)+coor(2,ip2)+coor(2,ip3))/3
   ! Find those that lie within integration area
   if (xc<140.or.xc>240) then
      ! to left or right of integration regions
   else if (yc<-120.or.yc>-40) then
      ! below or above integration regions
   else if (yc>=-70 .or. yc>-70-0.5*(xc-140)) then
      ! either in square region well above slab
      ! or just above slab in triangular region
      !write(9,*) xc,yc
      xl(1)=coor(1,ip1)
      yl(1)=coor(2,ip1)
      xl(2)=coor(1,ip2)
      yl(2)=coor(2,ip2)
      xl(3)=coor(1,ip3)
      yl(3)=coor(2,ip3)
      ! find barycentric coordinatese and area of the element
      call trilin(xl,yl,al,bl,cl,area_element)
      volume=volume+area_element
      ! find coordinates of points halfway along the sides of the triangle
      x12=coor(1,ip12)
      x13=coor(1,ip13)
      x23=coor(1,ip23)
      y12=coor(2,ip12)
      y13=coor(2,ip13)
      y23=coor(2,ip23)
      ! get values of the solution
      u12=usol(kprobp(ip12,1))
      u13=usol(kprobp(ip13,1))
      u23=usol(kprobp(ip23,1))
      v12=usol(kprobp(ip12,2))
      v13=usol(kprobp(ip13,2))
      v23=usol(kprobp(ip23,2))
      u1=usol(kprobp(ip1,1))
      u2=usol(kprobp(ip2,1))
      u3=usol(kprobp(ip3,1))
      v1=usol(kprobp(ip1,2))
      v2=usol(kprobp(ip2,2))
      v3=usol(kprobp(ip3,2))
      u123=(u1+u2+u3)/3
      v123=(v1+v2+v3)/3
!     write(6,'(3(2f12.1,3x),5x,2f12.1)') x12,y12,x13,y13,x23,y23,u123,v123
      uint = uint + area_element/3*(u12+u13+u23)
      vint = vint + area_element/3*(v12+v13+v23)
      zint = zint - area_element/3*(y12+y13+y23)
      averagevrms=averagevrms+u123*u123+v123*v123
      x2int = x2int + area_element/3*(x12*x12+x13*x13+x23*x23)
      y2int = y2int + area_element/3*(y12*y12+y13*y13+y23*y23)
      x2y2int = x2int+y2int
!     Trapezoid rule exact for u in P2
      avvrms=avvrms+area_element/3*(u12*u12+u13*u13+u23*u23+v12*v12+v13*v13+v23*v23)
      nelem_in_area=nelem_in_area+1
!     ! centroid formula (exact for u in P1)
!     avT=avT+area_element*u123
   endif
   ip=ip+6
enddo
averagevrms=sqrt(averagevrms)/nelem_in_area
!close(9)
write(6,*) 'volume          : ',volume,100*30 +0.5*100*50
write(6,*) 'avvrms/V        : ',sqrt(avvrms/volume)
write(6,*) 'averagevrms     : ',averagevrms
write(6,*) 'uint/V          : ',uint,uint/volume
write(6,*) 'vint/V          : ',vint,vint/volume
write(6,*) 'zint/V          : ',zint,zint/volume
write(6,*) 'sqrt(x2int)/V   : ',sqrt(x2int),sqrt(x2int/volume)
write(6,*) 'sqrt(y2int)/V   : ',sqrt(y2int),sqrt(y2int/volume)
write(6,*) 'sqrt(x2y2int)/V : ',sqrt(x2y2int),sqrt(x2y2int/volume)

avvrms=sqrt(avvrms/volume)
!call instop

end subroutine findav6_01
 
subroutine gauss6()
implicit none
real(kind=8) :: xg(6),yg(6),xl(3),yl(3),phil(6,3),phiq(6,6)
integer :: i

xg(1)=0.659027622374092
yg(1)=0.231933368553031
xg(2)=0.659027622374092
yg(2)=0.109039009072877
xg(3)=0.231933368553031  
yg(3)=0.659027622374092
xg(4)=0.231933368553031
yg(4)=0.109039009072877
xg(5)=0.109039009072877
yg(5)=0.659027622374092
xg(6)=0.109039009072877
yg(6)=0.231933368553031
xl(1)=0
yl(1)=0
xl(2)=1
yl(2)=0
xl(3)=0
yl(3)=0
call trilin(xl,yl,a,b,c,area
! linear shape functions in Gauss points
do i=1,6
   do k=1,3
      phil(i,k)=a(i)+b(i)*xg(i)+c(i)*yg(i)
   enddo
enddo
! quadratic shape function ins same
do i=1,6
   phiq(i,1)=phil(i,1)*(2*phil(i,1)-1)
   phiq(i,3)=phil(i,2)*(2*phil(i,2)-1)
   phiq(i,5)=phil(i,3)*(2*phil(i,3)-1)
   phiq(i,2) = 4*phil(i,1)*phil(i,2)
   phiq(i,4) = 4*phil(i,2)*phil(i,3)
   phiq(i,6) = 4*phil(i,3)*phil(i,1)
enddo
do i=1,6
   write(6,*) sum(phiq(i,1:6))   
enddo
end subroutine gauss6
