subroutine findav6_integr(ichoice,av6,kmesh,kprob,isol)
!use sepran_arrays
use sepmodulekmesh
use sepmodulecomio
implicit none
real(kind=8),intent(out) :: av6
integer,intent(in) :: isol,ichoice,kmesh,kprob
real(kind=8),allocatable :: userh(:)
integer, parameter :: NIUSERMAX=1000
integer :: iuserh(NIUSERMAX),alloc_status

iuserh(1)=NIUSERMAX
if (.not.allocated(userh)) then
   allocate(userh(5+npoint),stat=alloc_status)
   if (alloc_status/=0) then
      write(irefwr,*) 'PERROR(findav6: trouble allocating user : ',alloc_status,npoint
      call instop
   endif
endif
userh(1)=5+npoint
call sepactsolbf1(isol)
av6=0.0_8
! prepare userh with info for volint
call findav6_integr00(ichoice,kmesh,kprob,isol,iuserh,userh,av6)


if (allocated(userh)) deallocate(userh)

end subroutine findav6_integr

subroutine findav6_integr00(ichoice,kmesh,kprob,isol,iuserh,userh,av6)
use sepmoduleoldrouts
use sepmodulecomio
use sepmodulekmesh
use sepmodulekprob
use sepmodulevecs
implicit none
integer,intent(in) :: ichoice,isol,kmesh,kprob
integer,intent(inout) :: iuserh(*)
real(kind=8),intent(inout) :: userh(*)
real(kind=8),intent(out) :: av6
integer  :: i,iinvol(10)
real(kind=8) :: outval(10)

if (ichoice==1.or.ichoice==3) then
   call findav6T_integr(npoint,coor,ks(isol)%sol,userh(6))
else if (ichoice==2) then
   call findav6vrms_integr(npoint,coor,ks(isol)%sol,nunkpi,nphysi,indprfi,kprobfi,indprpi,kprobpi,userh(6))
else
   write(irefwr,*) 'PERROR(findav6_00):: ichoice <> 1,2 : ',ichoice
   call instop
endif
if (ichoice==3) then
   ! we're just looking for volume
   do i=1,npoint
      if (userh(5+i)>1e-5) userh(5+i)=1.0
   enddo
endif

! fill iuser for info for volint
iuserh(2)=1
iuserh(3)=0
iuserh(4)=0
iuserh(5)=0
iuserh(6)=7
iuserh(7)=0
iuserh(8)=0
iuserh(9)=0
iuserh(10)=2001
iuserh(11)=6
iinvol=0
call integr(iinvol,outval,kmesh,kprob,isol,iuserh,userh)
av6=outval(1)
end subroutine findav6_integr00

subroutine findav6T_integr(npoint,coor,usol,user)
implicit none
integer,intent(in) :: npoint
real(kind=8),intent(in) :: usol(*),coor(2,*)
real(kind=8),intent(out) :: user(*)
real(kind=8) :: x,y,xmax,xmin,ymax,ymin
integer :: i

xmax=-1e9
ymax=-1e9
xmin=1e9
ymin=1e9
! mask the wedge region above where it is at x=140 to x=240, or z=70 to z=140
do i=1,npoint
   x=coor(1,i)
   y=coor(2,i)
   if (x<140.or.x>240) then
      ! to left or right of integration regions
      user(i)=0.0_8
   else if (y<-120.or.y>-40) then
      ! below or above integration regions
      user(i)=0.0_8
   else if (y>-70 .or. y>-70-0.5*(x-140)) then
      ! either in square region well above slab
      ! or just above slab in triangular region
      user(i)=1
      write(6,'(''(x,y,T) : '',3f12.3)') x,y,user(i)
       xmax=max(xmax,x)
       ymax=max(ymax,y)
       xmin=min(xmin,x)
       ymin=min(ymin,y)
   endif
enddo
write(6,'(''volume integral bounds: '',2f12.3,3x,2f12.3)') xmin,xmax,ymin,ymax
!call instop

end subroutine findav6T_integr

subroutine findav6vrms_integr(npoint,coor,usol,nunkp,nphys,indprf,kprobf,indprp,kprobp,user)
implicit none
integer,intent(in) :: npoint,nunkp,nphys,indprf,kprobf,indprp,kprobp
real(kind=8),intent(in) :: coor(2,*),usol(*)
real(kind=8),intent(out) :: user(*)

user(1)=1.0_8

end subroutine findav6vrms_integr

