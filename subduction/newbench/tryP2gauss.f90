program tryP2gauss
implicit none
real(kind=8) :: coor2d(2,6),Te(6),phil(3),phiq(6),xc,yc
real(kind=8) :: coor2g(2,4),wg(4),element_int=0.0
integer :: i,k,ip,ncoord,ngauss

! 4 point Gauss rule
coor2g(1,1)=1.0_8/3
coor2g(2,1)=1.0_8/3
coor2g(1,2)=1.0_8/5
coor2g(2,2)=3.0_8/5
coor2g(1,3)=1.0_8/5
coor2g(2,3)=1.0_8/5
coor2g(1,4)=3.0_8/5
coor2g(2,4)=1.0_8/5
wg(1) = -27.0_8/48
wg(2:4) = 25.0_8/48
ngauss=4

! 3 point Gauss rule
!coor2g(:,:)=0.0
!wg=0
!coor2g(1,1)=1.0_8/6
!coor2g(2,1)=1.0_8/6
!coor2g(1,2)=2.0_8/6
!coor2g(2,2)=1.0_8/6
!coor2g(1,3)=1.0_8/6
!coor2g(2,3)=2.0_8/6
!wg(1:3)=1.0_8/3
!ngauss=3

xc=1.0_8/3
yc=xc
ncoord=6
coor2d(1,1) = 0
coor2d(2,1) = 0
coor2d(1,2) = 0.5
coor2d(2,2) = 0
coor2d(1,3) = 1.0
coor2d(2,3) = 0
coor2d(1,4) = 0.5
coor2d(2,4) = 0.5
coor2d(1,5) = 0
coor2d(2,5) = 1
coor2d(1,6) = 0
coor2d(2,6) = 0.5
do i=1,ncoord
   Te(i)=coor2d(1,i)+coor2d(2,i)  ! x+y
   !Te(i)=1 ! 1
   ! Te(i)=coor2d(2,i)*coor2d(2,i)+coor2d(1,i)*coor2d(1,i)
   Te(i)=coor2d(2,i)
enddo
element_int=0.0
do k=1,ngauss
   call getshape_quad_xi_eta(coor2g(1,k),coor2g(2,k),phil,phiq)
   do i=1,6
      element_int=element_int+(Te(i)*phiq(i))**3*wg(k)
   enddo
enddo
write(6,*) 'element_int : ',element_int/2,0.5_8*(Te(2)+Te(4)+Te(6))/3 ! multiply by area

write(6,*) sum(phil(1:3)),sum(phiq(1:6))

end program tryP2gauss

subroutine getshape_quad_xi_eta(xc,yc,phil,phiq)
implicit none
real(kind=8) :: xc,yc
real(kind=8) :: phil(3),phiq(6)
integer :: i
real(kind=8) :: a(3),b(3),c(3)
! coefficients for linear shape functions of reference element
data (a(i),i=1,3)/ 1d0, 0d0, 0d0/
data (b(i),i=1,3)/-1d0, 1d0, 0d0/
data (c(i),i=1,3)/-1d0, 0d0, 1d0/

do i=1,3
   phil(i) = a(i)+b(i)*xc+c(i)*yc
enddo
! quadratic shapefunctions
phiq(1) = phil(1)*(2*phil(1)-1)
phiq(3) = phil(2)*(2*phil(2)-1)
phiq(5) = phil(3)*(2*phil(3)-1)
phiq(2) = 4*phil(1)*phil(2)
phiq(4) = 4*phil(2)*phil(3)
phiq(6) = 4*phil(3)*phil(1)

end subroutine getshape_quad_xi_eta

