real(kind=8) function func(ichoice,x,y,z)
use sepmodulecons
use control
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8) :: batchelor,xt,yt
real(kind=8), parameter :: eps=1e-3,pi=3.141592653589793_8
!real(kind=8), parameter :: theta0=0.463647609_8,sintheta0=sin(theta0),costheta0=cos(theta0)

func=0.0_8

xt=0.0_8
yt=0.0_8
! Benchmark velocities (V,-V)/sqrt(2) modified for theta0/=0.5*pi
if (ichoice==10) then
   func = slab_velocity*costheta0
else if (ichoice==11) then
   func = -slab_velocity*sintheta0
else if (ichoice==12.or.ichoice==13) then
   if (x<-0.5*y) then 
      ! inside slab
      if (ichoice==12) func= slab_velocity*costheta0
      if (ichoice==13) func=-slab_velocity*sintheta0
   else if (y>=-50) then
      ! inside overriding plate
      func=0.0_8
   else
      ! in wedge: use Batchelor solution
      xt = x-xd
      yt = -yd-y
      func = slab_velocity*batchelor(ichoice-11,xt,yt)
   endif
else if (ichoice==14.or.ichoice==15) then
   if (x<-0.5*y-eps) then 
      ! inside slab
      if (ichoice==14) func= slab_velocity*costheta0
      if (ichoice==15) func=-slab_velocity*sintheta0
   endif
endif
!if (x>60.and.x<100.and.(ichoice==10.or.ichoice==12.or.ichoice==14)) then
!   write(6,'(''U: '',i10,5f15.7)') ichoice,x,y,xt,yt,func
!endif

end function func

