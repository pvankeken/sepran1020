real(kind=8) function funcbc(ichoice,x,y,z)
use sepmodulecons
use control
implicit none
integer,intent(in) :: ichoice
real(kind=8),intent(in) :: x,y,z
real(kind=8) :: vgrad_length
real(kind=8) :: Thalfspacemodel,func,Tc,ramp

funcbc=0.0_8
vgrad_length=rlcons(10)

funcbc=0.0_8
if (ichoice==1) then
   ! T at trench
   funcbc = Thalfspacemodel(-y)
else if (ichoice==2) then
   ! T at arc side
   call crustal_geotherm(-y,Tc)
   funcbc = Tc
!  call instop
else if (ichoice >= 10 .and. ichoice <=13) then
   ! velocity at top of slab
   ! get full velocity first
   if (-y<=z_d) then 
      funcbc=0.0_8
   else
      funcbc=func(ichoice,x,y,z)
      if (ichoice>=10 .and. ichoice<=11) then
        ! now apply taper if necessary for wedge side b.c.
        if (-y>=z_d .and. -y<=(z_d+vgrad_length)) then
           ramp = (-y-z_d)/vgrad_length
           funcbc=funcbc*ramp
           write(6,'(''funcbc corrected: '',i5,4f15.7)') ichoice,-y,-y-z_d,funcbc,ramp
        endif
      endif
   endif
endif
end function funcbc
