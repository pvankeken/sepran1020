subroutine crustal_geotherm(z,Tc)
use sepmodulecons
use control
implicit none
real(kind=8),intent(in) :: z
real(kind=8),intent(out) :: Tc
integer :: ibenchtype
real(kind=8) :: Tmantle,z1=0,z2,z3,z4,H1,H2,H3,q1,q2,q3,q4,T1,T2,T3,T4
integer :: crustal_layers
!real(kind=8),parameter :: kc=2.5_8/3.0_8,km=1 ! reverse decision from past
!real(kind=8),parameter :: kc=1,km=3.0_8/2.5_8 ! reverse decision from past
real(kind=8) :: kc,km,kcd,kmd,H1d,H2d,H3d
real(kind=8) :: smoothstep,xs
logical :: first=.true.
save first

ibenchtype=incons(8)
Tmantle=rlcons(5)
crustal_layers=nint(rlcons(24))
z2 = rlcons(25)
z3 = rlcons(26)
z4 = rlcons(27)

H1d = rlcons(28)
H2d = rlcons(29)
H3d = rlcons(30)
H1 = H1d*1e6/kr
H2 = H2d*1e6/kr
H3 = H3d*1e6/kr

q1 = rlcons(31)
kcd = rlcons(32)
kmd = rlcons(33)
kc=kcd/kr
km=kmd/kr

Tc=0.0_8
if (ibenchtype==0) then
  q2 = q1 - (z2-z1)*1e3*H1d
  q3 = q2 - (z3-z2)*1e3*H2d
  q4 = q3 - (z4-z3)*1e3*H3d

  T1 = 0
  T2 = -H1d*(z2-z1)*(z2-z1)*1e6/(2d0*kcd)+q1*(z2-z1)*1e3/kcd+T1
  T3 = -H2d*(z3-z2)*(z3-z2)*1e6/(2d0*kcd)+q2*(z3-z2)*1e3/kcd+T2
  T4 = -H3d*(z4-z3)*(z4-z3)*1e6/(2d0*kcd)+q3*(z4-z3)*1e3/kcd+T3
  if (first) then
     write(6,'(''************** Continental geotherm ********* '')')
     write(6,*) 'First  layer has thickness: ',z2-z1,' and H=',H1
     write(6,*) 'Second layer has thickness: ',z3-z2,' and H=',H2
     if (crustal_layers==3) then
        write(6,*) 'Third layer has thickness: ',z4-z3,' and H=',H3
     endif
     write(6,'(''H1, H2, H3 dim    = '',3e12.3)') H1d,H2d,H3d
     write(6,'(''H1, H2, H3 nondim = '',3e12.3)') H1,H2,H3
     write(6,'(''kc, km     dim    = '',3f12.3)') kcd,kmd
     write(6,'(''kc, km     nondim = '',3f12.3)') kc,km
     write(6,*)
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.5)') z1,T1,q1
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.5)') z2,T2,q2
     write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.5)') z3,T3,q3
     if (crustal_layers==3) then
        write(6,'(''At depth '',f8.2,'' T and heatflow are '',f8.1,f8.3)') z4,T4,q4/400d0
     endif
     first=.false.
  endif

  if (z<z2) then
     ! upper crust
     Tc = -H1d*(z-z1)*(z-z1)*1e6/(2d0*kcd)+q1*(z-z1)*1e3/kcd+T1
  else if (z<z3) then
     Tc= -H2d*(z-z2)*(z-z2)*1e6/(2d0*kcd)+q2*(z-z2)*1e3/kcd+T2
  else if (crustal_layers==3) then
     if (z<z4) then
       Tc= -H3d*(z-z3)*(z-z3)*1e3/(2d0*kcd)+q3*(z-z3)*1e3/kcd+T3
     else
       Tc= T4 + q4/kmd*(z-z4)*1e3
     endif
  else
     Tc= T3 + q3/kmd*(z-z3)*1e3
  endif
  Tc = min(Tc,Tmantle)
! ! not quite it - modifies gradient too much
! if (Tc > Tmantle-100) then
!    xs = (Tmantle-Tc)/100.0_8
!    if (xs<0) then
!       smoothstep=0.0_8
!    else if (xs>1) then
!       smoothstep=1.0_8
!    else
!       smoothstep = xs*xs*(3.0_8-2.0_8*xs)
!    endif
!    write(6,'(4f15.4,$)') Tc,Tmantle,xs,smoothstep
!    Tc = Tc*smoothstep+Tmantle*(1.0_8-smoothstep)
!    write(6,'(f15.4)') Tc
! endif
  return
endif
if (ibenchtype>0) then
   ! van Keken et al., 2008 benchmark
   Tc=Tmantle
   if (z<50) then 
      Tc = z*Tmantle/50.0_8
   endif
else
    
endif

end subroutine crustal_geotherm

real(kind=8) function Thalfspacemodel(z)
use sepmodulecons
implicit none
real(kind=8),intent(in) :: z
integer :: ibenchtype
real(kind=8) :: Tmantle,rerf,ageMa,ages,akappa,denom,zd,k_m

! set defaults for age and diffusivity
ageMa=50.0e6_8 
akappa=1e-6 
Tmantle=rlcons(5)
ibenchtype=incons(8)
!if (ibenchtype>0) then
!  ageMa=50.0e6_8
!  akappa=0.7272727272e-6
!endif
k_m=3
if (rlcons(33)>1e-7) then
   k_m=rlcons(33)
endif
ageMa=50
if (rlcons(39)>1e-7) then
   ageMa=rlcons(39) ! age in Myr
endif
akappa=k_m/(1250*3300)
!write(6,*) 'akappa, ageMa: ',akappa,ageMa
!call instop

ages=ageMa*365.25*24*3600*1e6 ! convert to seconds
denom=2*sqrt(akappa*ages)/1000.
zd=z/denom
Thalfspacemodel=Tmantle*rerf(zd)
!write(6,'(''Th: '',4f12.3)') z,zd,Thalfspacemodel,Tmantle
end function Thalfspacemodel
