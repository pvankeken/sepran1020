program sepcompexe
! ********************************************************************
!
!                         DESCRIPTION
!
!   Main program for the computational part of SEPRAN
!   A large number of standard problems may have been solved
!   The mesh must have been created by program SEPMESH and written to
!   file meshoutput ( on PC to file meshout.put )
!
!   The output is written to the files sepcomp.inf and sepcomp.out
!
! ********************************************************************
implicit none
call startsepran
call sepcompmain
end program sepcompexe

real(kind=8) function funccf(ichoice,x,y,z)
implicit none
real(kind=8) :: x,y,z
integer :: ichoice
real(kind=8) :: bigGamma,d,bigG,prespi
real(kind=8) :: rho=5500,v0=1e-9,L=215e3

prespi=(250e3-y)-125e3
d=1e3 ! transition thickness
bigG=bigGamma(prespi,d)

funccf=rho*v0*L*2/d*(bigG-bigG*bigG)
if (y>120e3.and.y<130e3) write(6,'(4e15.7)') y,funccf,bigG,prespi

end function funccf

real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

subroutine userout ( isol, isequence )
use sepmodulemnsub
use sepmodulemain
implicit none
integer :: isol(*),isequence
real(kind=8) :: funcx(10005),funcy(10005)
integer :: icurvs(3),ichoice,i

funcx(1)=10005
funcy(1)=10005
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
open(9,file='T_channel.dat')
do i=1,funcy(5)
   write(9,*) funcx(5+2*i-1),funcy(5+i)
enddo
close(9)

write(6,*) 'isol(1): ',isol(1)

end subroutine userout
