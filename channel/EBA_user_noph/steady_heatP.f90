subroutine steady_heatP(kmesh,kprob,intmat,isol2,iuserh,userh)
use sepmoduleoldrouts
implicit none
integer :: kmesh,kprob,intmat,isol2
integer :: matr,irhsd,matrm,build_in(20),iuserh(*),solve_in(20),iread
real(kind=8) :: userh(*),solve_rin(20)
save matr,irhsd,matrm,solve_in,solve_rin

!write(6,*) iuserh(1),nint(userh(1))
build_in=0
build_in(1)=2
build_in(2)=1
call build(build_in,matr,intmat,kmesh,kprob,irhsd,matrm,isol2,isol2,iuserh,userh)

!write(6,*) iuserh(1),nint(userh(1))
solve_in=0
solve_rin=0.0_8
solve_in(1)=3
solve_in(3)=0
iread=-1
call solvel(solve_in,solve_rin,matr,isol2,irhsd,intmat,kmesh,kprob,iread)

end subroutine steady_heatP

