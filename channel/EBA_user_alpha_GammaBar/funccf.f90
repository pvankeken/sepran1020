real(kind=8) function funccf(ichoice,x,y,z)
implicit none
real(kind=8) :: x,y,z
integer :: ichoice
real(kind=8) :: bigGamma,d,bigG,prespi
real(kind=8) :: rho=1,v0=137.5,L=1,scale_dim
real(kind=8), parameter :: cp_dim=1200,L_r=215e3,DeltaT_dim=1

prespi=y-0.5
d=0.002 ! transition thickness
bigG=bigGamma(prespi,d)

L=1
scale_dim=L_r/(cp_dim*deltaT_dim)
funccf=rho*v0*L*scale_dim*2/d*(bigG-bigG*bigG)! *0.0613 ! Di
!if (y>0.45.and.y<0.55) 
!if (x<0.01) write(6,'(5e15.7)') y,funccf,bigG,prespi,bigG-bigG*bigG

end function funccf

