program sepcompexe
! ********************************************************************
!
!                         DESCRIPTION
!
!   Main program for the computational part of SEPRAN
!   A large number of standard problems may have been solved
!   The mesh must have been created by program SEPMESH and written to
!   file meshoutput ( on PC to file meshout.put )
!
!   The output is written to the files sepcomp.inf and sepcomp.out
!
! ********************************************************************
implicit none
call startsepran
call sepcompmain
end program sepcompexe

real(kind=8) function funccf(ichoice,x,y,z)
implicit none
real(kind=8) :: x,y,z
integer :: ichoice
real(kind=8) :: bigGamma,d,bigG,prespi
real(kind=8) :: rho=1,v0=137.5,L=1,scale_dim
real(kind=8), parameter :: cp_dim=1200,L_r=215e3,DeltaT_dim=1

prespi=y-0.5
d=0.002 ! transition thickness
bigG=bigGamma(prespi,d)

L=1
scale_dim=L_r/(cp_dim*deltaT_dim)
funccf=rho*v0*L*scale_dim*2/d*(bigG-bigG*bigG)! *0.0613 ! Di
!if (y>0.45.and.y<0.55) 
if (x<0.01) write(6,'(5e15.7)') y,funccf,bigG,prespi,bigG-bigG*bigG

end function funccf

real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

subroutine userout ( isol, isequence )
use sepmodulemnsub
use sepmodulemain
implicit none
integer :: isol(*),isequence
real(kind=8) :: funcx(100005),funcy(100005),x,y,funccf,prespi,d,bigGamma
integer :: icurvs(3),ichoice,i

funcx(1)=100005
funcy(1)=100005
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
open(9,file='T_channel.dat')
do i=1,funcy(5)
   x=funcx(5+2*i-1)
   y=funcx(5+2*i)
   prespi=0.5-y
   d=0.002
   write(9,'(4e15.7)') funcx(5+2*i),funcy(5+i),funccf(1,x,y,y),bigGamma(prespi,d)
enddo
close(9)

write(6,*) 'isol(1): ',isol(1)

end subroutine userout
