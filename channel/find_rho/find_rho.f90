program find_rho
implicit none
integer :: i
integer, parameter :: N=1001
real(kind=8) :: rho(N),rho1,rho2,drho,z(N),dz,rhor,drhodz,Di
real(kind=8) :: phd,bigG(N)=0.0_8,dGdpi(N)=0.0_8,deltarho,prespi,bigGamma

!Di=0.0613
deltarho=580
dz=1.0_8/(N-1)
read(5,*) Di,rhor,phd
z(1)=0
rho(1)=rhor
do i=2,N
   z(i)=(i-1)*dz
   drhodz=Di*rho(i-1)*dz
   prespi=z(i)-0.5
   bigG(i)=bigGamma(prespi,phd)
   dGdpi(i)=2.0_8/phd*bigG(i)*(1.0_8-bigG(i))
   rho(i)=rho(i-1)+drhodz !+ deltarho*dGdpi(i)*dz
   write(6,'(4e15.7)') z(i),rho(i),bigG(i),dGdpi(i)
enddo
   


end program find_rho

real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

