subroutine fillcoef800(iuser,user,isol2)
use sepmodulekmesh
use sepmodulekprob
use control
implicit none
integer :: iuser(*),isol2
real(kind=8) :: user(*)
integer :: ihorvel,ivervel,ibeta,iright,irhocp,itemp,iuser_cond,ip,idGdpi,ibigG
logical :: first=.true.
save first


iuser_cond=10
ihorvel=iuser_cond+npoint
ivervel=iuser_cond+2*npoint
ibeta=iuser_cond+3*npoint
iright=iuser_cond+4*npoint
irhocp=iuser_cond+5*npoint
itemp=iuser_cond+6*npoint
idGdpi=iuser_cond+7*npoint
ibigG=iuser_cond+8*npoint
if (ibigG+npoint>nint(user(1))) then
   write(6,*) 'PERROR(fillcoef800): array user is too small'
   write(6,*) itemp+npoint,nint(user(1))
   call instop
endif

!Di=0.0613

iuser(2:iuser(1))=0
user(2:nint(user(1)))=0.0_8
iuser(2)=1
iuser(6)=15
! first define integer information for coefficients
ip=14
! (1) not used
iuser(ip+1) = 0
! (2) type of upwinding ; avoid with P2 for now
iuser(ip+2) = 0
! (3) intrule
iuser(ip+3) = 0
! (4) icoor: type of coordinate system
iuser(ip+4) = 0    ! Cartesian
! (5) not yet used
iuser(ip+5) = 0

! (6) k11
iuser(ip+6) = 2001
iuser(ip+7) = iuser_cond
ip=ip+1 ! account for increment since type 2001 takes two integer spaces
! (7) k12
iuser(ip+7) = 0
! (8) k13
iuser(ip+8) = 0
! (9) k22
iuser(ip+9) = 2001
iuser(ip+10) = iuser_cond
ip=ip+1
! (10) k23
iuser(ip+10) = 0
! (11) k33
iuser(ip+11) = 0

! (12) u
iuser(ip+12) = 2001
iuser(ip+13) = ihorvel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces
! (13) v
iuser(ip+13) = 2001
iuser(ip+14) = ivervel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (14) w==0
iuser(ip+14) = 0

! (15) beta
iuser(ip+15) = 2001
iuser(ip+16) = ibeta
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (16) q    Note we use iright because we may add viscous dissipation to form
!           the right-hand side
iuser(ip+16)=2001
iuser(ip+17)=iright
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (17) rho*cp
iuser(ip+17) = 2001
iuser(ip+18) = irhocp
ip=ip+1

user(iuser_cond:iuser_cond+npoint-1)=1.0_8
user(ivervel:ivervel+npoint-1)=v0
user(irhocp:irhocp+npoint-1)=1.0_8

call pecopy(0,user(itemp:itemp+npoint-1),isol2)

call fill800(npoint,user(ibeta),user(iright),user(itemp),user(ihorvel),user(ivervel),user(irhocp),coor,ks(isol2)%sol,user(idGdpi),user(ibigG))

if (first) then
   call coef800_print(npoint,coor,user(ihorvel),user(ivervel),user(ibeta),user(iright),user(irhocp),user(itemp),user(iuser_cond))
   first=.false.
endif


end subroutine fillcoef800
subroutine fill800(N,beta,right,temp,velx,vely,rhocp,coor,usol2,dGdpi,bigG)
use control
implicit none
integer :: N
real(kind=8),dimension(N) :: beta,temp,usol2,velx,vely,rhocp,dGdpi,bigG,right
real(kind=8) :: coor(2,N)
integer :: i
real(kind=8) :: x,y,w,z,prespi,bigGamma,alpha,cp

!write(6,*) 'N: ',N
alpha=1.0_8
cp=1.0_8
if (effacp) then
   do i=1,N
     x=coor(1,i)
     y=coor(2,i)
     z=1-y
     if (nph>0) then
        prespi=phy0-y ! -gamma*(temp(i)-phT0)
        bigG(i)=bigGamma(prespi,phd)
        dGdpi(i)=2.0_8/phd*bigG(i)*(1-bigG(i))
        alpha=1+gamma*Rb/Ra*dGdpi(i)
        cp=1+gamma*gamma*Rb/Ra*dGdpi(i)*Di*temp(i)
     endif
     w=vely(i)
     beta(i)=Di*w*alpha
     rhocp(i)=cp
     !if (x<0.01) write(6,'(5e15.7)') y,bigG(i),dGdpi(i),alpha,cp
   enddo
else 
   ! explicit phase change latent heat exchange term
   do i=1,N
     x=coor(1,i)
     y=coor(2,i)
     z=1-y
     w=vely(i)
     beta(i)=Di*w*alpha
     rhocp(i)=cp
     prespi=phy0-y-gamma*(temp(i)-phT0)
     bigG(i)=bigGamma(prespi,phd)
     dGdpi(i)=2.0_8/phd*bigG(i)*(1-bigG(i))
     right(i)=gamma*drho/(rho1*rho2)*max(temp(i),3200.0_8)*Lr/cpr*dGdpi(i)*w
   enddo
endif

end subroutine fill800

