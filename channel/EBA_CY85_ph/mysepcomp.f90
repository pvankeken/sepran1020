program sepcompexe
! ********************************************************************
!
!                         DESCRIPTION
!
!   Main program for the computational part of SEPRAN
!   A large number of standard problems may have been solved
!   The mesh must have been created by program SEPMESH and written to
!   file meshoutput ( on PC to file meshout.put )
!
!   The output is written to the files sepcomp.inf and sepcomp.out
!
! ********************************************************************
implicit none
call startsepran
call sepcompmain
end program sepcompexe

real(kind=8) function funccf(ichoice,x,y,temp)
implicit none
real(kind=8) :: x,y,temp
integer :: ichoice
real(kind=8) :: bigGamma,d,bigG,prespi,dGdpi
real(kind=8) :: rho=1,v0=137.5,L=1,scale_dim
real(kind=8), parameter :: cp_dim=1200,L_r=215e3,DeltaT_dim=1
! based on assumed 3350 center T:
!real(kind=8), parameter :: Ra=12,Rb=4.63e4,Di=0.0613,gamma=-2.23e-4,gRbRa=gamma*Rb/Ra,T0=3361
real(kind=8), parameter :: Ra=13.896,Rb=3.7309e4,Di=0.0613,gamma=-2.97e-4,gRbRa=gamma*Rb/Ra,T0=3361

!write(6,*) gRbRa
funccf=0
prespi=0.5-y ! -gamma*(temp-T0)
d=0.02 ! transition thickness
bigG=bigGamma(prespi,d)
dGdpi=2.0_8/d*(bigG-bigG*bigG)
if (ichoice==1) then
   ! latent heating
   L=1
   scale_dim=L_r/(cp_dim*deltaT_dim)
   funccf=rho*v0*L*scale_dim*dGdpi
   !if (y>0.45.and.y<0.55) 
else if (ichoice==2) then
   ! effective alpha * Di * v0
   funccf = Di*(1 + gRbRa*dGdpi)*v0
else if (ichoice==3) then
   ! effective cp
   funccf=1+dGdpi*gamma*gRbRa*Di*T0 ! estimated T  
   if (x<0.01) write(6,'(6e15.7)') y,funccf,bigG,prespi,bigG-bigG*bigG,funccf
else if (ichoice==4) then
   funccf=bigG
else if (ichoice==5) then
   funccf=dGdpi
else if (ichoice==6) then
   ! effective alpha 
   funccf = 1 + gRbRa*dGdpi
endif


end function funccf

real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

subroutine userout ( isol, isequence )
use sepmodulemnsub
use sepmodulemain
implicit none
integer :: isol(*),isequence
real(kind=8) :: funcx(100005),funcy(100005),x,y,funccf,temp
integer :: icurvs(3),ichoice,i

funcx(1)=100005
funcy(1)=100005
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
open(9,file='T_channel.dat')
do i=1,funcy(5),20
   x=funcx(5+2*i-1)
   y=funcx(5+2*i)
   temp=funcy(5+i)
   ! y,T,Q_L,alpha,cp,Gamma,dGdpi
   write(9,'(8e15.7,:)') y,temp,funccf(1,x,y,temp),funccf(6,x,y,temp),funccf(3,x,y,temp),funccf(4,x,y,temp),funccf(5,x,y,temp)
enddo
close(9)

write(6,*) 'isol(1): ',isol(1)

end subroutine userout
