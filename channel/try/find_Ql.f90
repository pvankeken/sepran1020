program find_Ql
implicit none
integer, parameter :: N=10001
real(kind=8), dimension(6,N) :: Tz
real(kind=8), dimension(N) :: dGdpi
integer :: i,j
real(kind=8), parameter :: gamma=-2.97e-4,Rb=4.068e4,Ra=13.9,Di=0.06125,v0=137.5,hr=250e3
real(kind=8), parameter :: prefac=gamma*Rb/Ra*Di*v0
real(kind=8) :: Ql,Q2,v0int,dz


write(6,*) 'prefac = ',prefac
do i=1,N
  read(5,*) (Tz(j,i),j=1,6)
enddo

! dimensionalize
Tz(1,:)=hr*Tz(1,:)
dz=hr/(N-1)
! for d=0.02 nondim
dGdpi(:)=Tz(6,:)*(1.0_8-Tz(6,:))*2.0_8/(0.002*hr)
write(6,*) maxval(dGdpi)*hr


Ql=0.5*dGdpi(1)*Tz(2,1)*dz
Ql=Ql+0.5*dGdpi(N)*Tz(2,N)*dz
do i=2,N-1
   Ql=Ql+dGdpi(i)*Tz(2,i)*dz
enddo
Ql=prefac*Ql*12
write(6,*) Ql/hr




end program find_Ql
