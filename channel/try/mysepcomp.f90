program sepcompexe
! ********************************************************************
!
!                         DESCRIPTION
!
!   Main program for the computational part of SEPRAN
!   A large number of standard problems may have been solved
!   The mesh must have been created by program SEPMESH and written to
!   file meshoutput ( on PC to file meshout.put )
!
!   The output is written to the files sepcomp.inf and sepcomp.out
!
! ********************************************************************
implicit none
call startsepran
call sepcompmain
end program sepcompexe

real(kind=8) function funccf(ichoice,x,y,z)
implicit none
real(kind=8) :: x,y,z
integer :: ichoice
real(kind=8) :: bigGamma,d,bigG,prespi,dGdpi
real(kind=8) :: rho=1,v0=137.5,L=1,scale_dim
real(kind=8), parameter :: cp_dim=1200,L_r=215e3,DeltaT_dim=1
real(kind=8), parameter :: Di=0.06125, gRbRa=-0.86895 ! gRbRa=-0.79699*1.088 ! gRbRa=-0.79699
!! real(kind=8), parameter :: Ra=12,Rb=4.63e4,Di=0.0613,gamma=-2.23e-4,gRbRa=gamma*Rb/Ra,T0=3361

funccf=0
!prespi=0.5-(1-y)
prespi=0.5-y
d=0.002 ! transition thickness
bigG=bigGamma(prespi,d)
dGdpi=2.0_8/d*(bigG-bigG*bigG)
if (ichoice==1) then
   ! latent heating
   L=1
   scale_dim=L_r/(cp_dim*deltaT_dim)
   funccf=rho*v0*L*scale_dim*dGdpi
   !if (y>0.45.and.y<0.55) 
   if (x<0.01) write(6,'(5e15.7)') y,funccf,bigG,prespi,bigG-bigG*bigG
else if (ichoice==2) then
   ! effective alpha
   funccf = Di*(1 + gRbRa*dGdpi)*v0
   !funccf = Di*v0
else if (ichoice==3) then
   ! effective cp
else if (ichoice==4) then
   ! effective alpha - 1
   ! effective alpha
   funccf = -Di*(gRbRa*dGdpi)*v0
else if (ichoice==5) then
   funccf = dGdpi
else if (ichoice==6) then
   funccf = bigG
endif


end function funccf

real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

subroutine userout ( isol, isequence )
use sepmodulemnsub
use sepmodulemain
implicit none
integer :: isol(*),isequence
real(kind=8) :: funcx(100005),funcy(100005),x,y,funccf
integer :: icurvs(3),ichoice,i

funcx(1)=100005
funcy(1)=100005
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol(1),1,icurvs,funcx,funcy)
open(9,file='T_channel.dat')
do i=1,funcy(5)
   x=funcx(5+2*i-1)
   y=funcx(5+2*i)
   write(9,'(6e15.7)') funcx(5+2*i),funcy(5+i),funccf(1,x,y,y),funccf(4,x,y,y)*funcy(5+i),funccf(5,x,y,y),funccf(6,x,y,y)
enddo
close(9)

write(6,*) 'isol(1): ',isol(1)

end subroutine userout
