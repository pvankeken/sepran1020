program channel
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulekprob
use control
implicit none
integer, parameter :: NUSER=1000000,NFUNC=100005
real(kind=8),dimension(NUSER) :: userh(NUSER)
integer :: iuserh(1000),kprob,kmesh,intmat,iu1lc(10),isol2,icurvs(2),isolold
real(kind=8) :: u1lc(10),x,y,temp,d,bigGamma,prespi,funccf,contln(20),tempmax,pee,quu,relax1,relax2
real(kind=8),dimension(NFUNC) :: funcx,funcy
!real(kind=8),parameter :: Lr=215e3_8,cpr=1200.0_8,rhor=5500_0.8,deltaTr=1.0_8
integer :: i,ncntln,iter,ihelp=0,idGdpi,ibigG
namelist /channel_nml/ Di,T_bot,v0,rho1,rho2,drho,gamma,effacp,Ra,Rb,nph,phd

rho1=-1.0_8
rho2=-1.0_8
iuserh(1)=1000
 userh(1)=1.0_8*NUSER
call sepstr(kmesh,kprob,intmat)
idGdpi=10+7*npoint
ibigG=10+8*npoint

open(lu_nml,file='channel.nml')
read(lu_nml,NML=channel_nml)
close(lu_nml)
if (rho1>1e-7.and.rho2>1e-7) then
   drho=rho2-rho1
endif
relax=0.01

call creavc(0,1,1,isol2,kmesh,kprob,iu1lc,u1lc)
call bvalue(0,2,kmesh,kprob,isol2,T_bot,1,1,1,0)

iter=0
nitermax=10
do
  call copyvc(isol2,isolold)
  iter=iter+1
  call fillcoef800(iuserh,userh,isol2)
  call steady_heatP(kmesh,kprob,intmat,isol2,iuserh,userh)
  dif2=anorm(0,3,0,kmesh,kprob,isol2,isolold,ihelp)
  tempmax=anorm(1,3,0,kmesh,kprob,isol2,isol2,ihelp)
  relax1=relax
  relax2=1.0-relax
  call algebr(3,0,isolold,isol2,isol2,kmesh,kprob,relax1,relax2,pee,quu,ihelp)
  dif2=dif2/tempmax
  write(6,'(i5,2f12.3)') iter,dif2,tempmax
  if (dif2<1e-4) exit
enddo

funcx(1)=NFUNC
funcy(1)=NFUNC
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol2,1,icurvs,funcx,funcy)

open(9,file='T_channel.dat')
do i=1,nint(funcy(5))
   x=funcx(5+2*i-1)
   y=funcx(5+2*i)
   temp=funcy(5+i)
   !prespi=0.5-y-gamma*(temp-pht0)
   !d=0.002
   write(9,'(4e15.7)') y,temp,userh(idGdpi+i-1),userh(ibigG+i-1)
enddo
close(9)
write(6,*) 'top T = ',temp

ncntln=0
call plotc1(1,kmesh,kprob,isol2,contln,ncntln,15.0_8,1.0_8,1)


end program channel

