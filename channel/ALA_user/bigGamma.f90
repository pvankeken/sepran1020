real(kind=8) function bigGamma(prespi,d)
implicit none
real(kind=8) :: prespi,d

bigGamma=0.5*(1+tanh(prespi/d))

end function bigGamma

