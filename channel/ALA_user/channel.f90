program channel
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulekprob
use control
implicit none
integer, parameter :: NUSER=1000000,NFUNC=100005
real(kind=8),dimension(NUSER) :: userh(NUSER)
integer :: iuserh(1000),kprob,kmesh,intmat,iu1lc(10),isol2,icurvs(2),isolold,irho,i1
real(kind=8) :: u1lc(10),x,y,temp,d,bigGamma,prespi,funccf,contln(20),tempmax,pee,quu,rhomin,rhomax
real(kind=8),dimension(NFUNC) :: funcx,funcy
!real(kind=8),parameter :: Lr=215e3_8,cpr=1200.0_8,rhor=5500_0.8,deltaTr=1.0_8
integer :: i,ncntln,iter,ihelp=0,idGdpi,ibigG
namelist /channel_nml/ Di,T_bot,v0,rho1dim,rho2dim,gamma,effacp,Ra,Rb,nph,phd

rho1=-1.0_8
rho2=-1.0_8
iuserh(1)=1000
 userh(1)=1.0_8*NUSER
call sepstr(kmesh,kprob,intmat)
idGdpi=10+7*npoint
ibigG=10+8*npoint

open(lu_nml,file='channel.nml')
read(lu_nml,NML=channel_nml)
close(lu_nml)
rho1=rho1dim/rhor
rho2=rho2dim/rhor
Rb1=Rb*rhor*rhor/(rho1dim*rho2dim)
rhoz0dim=rho1dim*exp(-Di/2.0_8)
rhoz0=rhoz0dim/rhor
drho=drhodim/rhor
write(6,*) 'rho: ',rho1,rho2,Rb1,drho
write(6,*) 'rhod:',rho1dim,rho2dim,rhoz0dim,drhodim

call findrhoeos()
call creavc(0,1,1,irho,kmesh,kprob,iu1lc,u1lc)
call fillrho(ks(irho)%sol,coor,npoint)
call algebr(6,1,irho,i1,i1,kmesh,kprob,rhomin,rhomax,pee,quu,ihelp)
ncntln=0
!call plotc1(1,kmesh,kprob,irho,contln,ncntln,15.0_8,1.0_8,1)
write(6,*) 'rho ranges from ',rhoeos(1),' to ', rhoeos(Neos)
write(6,*) rhomin,rhomax
write(6,*) rhomin*rhor,rhomax*rhor
!call instop
!if (rho1>1e-7.and.rho2>1e-7) then
!   drho=rho2-rho1
!   Rb=Rb*rho2*rho2/(rho1*rho2)
!   write(6,*) 'adjusting Rb by a factor: ',rho2*rho2/(rho1*rho2)
!endif
relax=0.5

call creavc(0,1,1,isol2,kmesh,kprob,iu1lc,u1lc)
call bvalue(0,2,kmesh,kprob,isol2,T_bot,1,1,1,0)

iter=0
nitermax=10
do
  call copyvc(isol2,isolold)
  iter=iter+1
  call fillcoef800(iuserh,userh,isol2,irho)
  call steady_heatP(kmesh,kprob,intmat,isol2,iuserh,userh)
  dif2=anorm(0,3,0,kmesh,kprob,isol2,isolold,ihelp)
  tempmax=anorm(1,3,0,kmesh,kprob,isol2,isol2,ihelp)
  call algebr(3,0,isolold,isol2,isol2,kmesh,kprob,relax,relax,pee,quu,ihelp)
  dif2=dif2/tempmax
  write(6,'(i5,2f12.3)') iter,dif2,tempmax
  if (dif2<1e-4) exit
enddo

funcx(1)=NFUNC
funcy(1)=NFUNC
icurvs(1)=0
icurvs(2)=2
call compcr(0,kmesh,kprob,isol2,1,icurvs,funcx,funcy)

open(9,file='T_channel.dat')
do i=1,nint(funcy(5))
   x=funcx(5+2*i-1)
   y=funcx(5+2*i)
   temp=funcy(5+i)
   !prespi=0.5-y-gamma*(temp-pht0)
   !d=0.002
   write(9,'(4e15.7)') y,temp,userh(idGdpi+i-1),userh(ibigG+i-1)
enddo
close(9)
write(6,*) 'top T = ',temp

ncntln=0
call plotc1(1,kmesh,kprob,isol2,contln,ncntln,15.0_8,1.0_8,1)


end program channel

subroutine findrhoeos
use control
implicit none
real(kind=8) :: dz,drhodz
real(kind=8) :: bigG=0.0_8,dGdpi=0.0_8,prespi,bigGamma
integer :: i

open(9,file='eosrho.dat')
dz=1.0_8/(Neos-1)
zeos(1)=0
rhoeos(1)=rhoz0dim
do i=2,Neos
   zeos(i)=(i-1)*dz
   drhodz=Di*rhoeos(i-1)*dz
   prespi=zeos(i)-0.5
   bigG=bigGamma(prespi,phd)
   dGdpi=2.0_8/phd*bigG*(1.0_8-bigG)
   rhoeos(i)=rhoeos(i-1)+drhodz + drhodim*dGdpi*dz
   write(9,'(2e12.5)') zeos(i),rhoeos(i)
enddo
close(9)


end subroutine findrhoeos

subroutine fillrho(rho,coor,npoint)
use control
implicit none
real(kind=8) :: rho(*),coor(2,*),dz,rl,z,y
integer :: npoint,find_eos_iz,iz,i

do i=1,npoint
   y=coor(2,i)
   z=1-y
   iz=find_eos_iz(z,zeos,Neos)
   dz=zeos(iz+1)-zeos(iz)
   rl=(z-zeos(iz))/dz
   rho(i)=(rhoeos(iz)*(1.0_8-rl)+rhoeos(iz+1)*rl)/rhor
   !if (coor(1,i)<1e-5) write(6,*) z,iz
enddo

end subroutine fillrho

integer function find_eos_iz(z,zeos,Neos)
implicit none
integer :: Neos,i
real(kind=8) :: zeos(Neos),z

if (z<=zeos(1)) then
   find_eos_iz=1
   return
else if (z>=zeos(Neos)) then
   find_eos_iz=Neos
endif
i=1
do 
    i=i+1
    if (z>=zeos(i-1).and.z<=zeos(i)) then
       find_eos_iz=i-1
       return
    else if (i>=Neos) then
       write(6,*) 'PERROR(find_eos_iz): out of range'
       write(6,*) 'i, Neos: ',i,Neos
       write(6,*) 'zeos(1),z,zeos(Neos): ',zeos(1),z,zeos(Neos)
       call instop
    endif
enddo
end function find_eos_iz

