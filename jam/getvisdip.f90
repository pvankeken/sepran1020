! compute viscous dissipation from velocity gradients (see comprP:phifromgrad)
subroutine getvisdip(kmesh,kprob,ivelx,ively,iphi,iuser_here,user_here)
use sepmodulekmesh
use sepmodulevecs
implicit none
integer :: kmesh,kprob,ivelx,ively,iphi,iuser_here(*)
real(kind=8) :: user_here(*)
integer :: gradv11=0,gradv12=0,gradv22=0,div11=0,ielhlp
integer :: iuserh(100)
real(kind=8) :: userh(100)
integer :: iinder(15)
save gradv11,gradv12,gradv22,div11

! gradv11 = du/dx
call deriva(2,1,1,1,1,gradv11,kmesh,kprob,ivelx,ivelx,iuser_here,user_here,ielhlp)
! gradv22 = dv/dy
call deriva(2,1,2,1,1,gradv22,kmesh,kprob,ively,ively,iuser_here,user_here,ielhlp)
! gradv12 = du/dy = dv/dx
call deriva(2,1,1,1,1,gradv12,kmesh,kprob,ively,ively,iuser_here,user_here,ielhlp)

!write(6,*) 'phifromgrad: ',gradv11,gradv12,gradv22,iphi
if (iphi==0) call copyvc(gradv11,iphi)
call phifromgrad(ks(gradv11)%sol,ks(gradv12)%sol,ks(gradv22)%sol,ks(iphi)%sol,npoint)

end subroutine getvisdip

