subroutine movetracers4()
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulevecs
use tracers
use mtime
use geometry
use sepran_arrays
implicit none
real(kind=8) :: dt_little
real(kind=8) :: rk1x,rk1y,rk2x,rk2y,rk3x,rk3y,rk4x,rk4y,oneminfrac1,oneminfrac2
integer :: itrac,ipsiav,ntot,ipoint,ip1,ip2,ielh,ip,ichoice
real(kind=8) :: pee,quu,xm,ym,u,v,xi,eta
real(kind=8) :: onehalf=0.5_8
save ipsiav

dt_little=tstepp
ntot=imark(1)
! find velocity at time now in coormark
ichoice=1
call tracvel(ichoice,ipsiold)

! find velocity at time t=tnow+0.5*dt
call algebr(3,0,ipsi,ipsiold,ipsiav,kmesh,kprob,onehalf,onehalf,pee,quu,ipoint)
!write(6,*) 'dt_little = ',dt_little,tstepp
!write(6,'(''u,v = '',4e15.7)') coormark(1),coormark(2),velmark(1),velmark(2)

do itrac=1,ntot
   ip1=2*itrac-1
   ip2=ip1+1
   rk1x = dt_little*velmark(ip1)
   rk1y = dt_little*velmark(ip2)
   xm = coormark(ip1)+onehalf*rk1x
   ym = coormark(ip2)+onehalf*rk1y

   ! find average velocity at midpoint
   call velintmark_cart(xm,ym,u,v,ielh,xi,eta,ks(ipsiav)%sol,'movetracers4_cart')
   !write(6,'(''u,v = '',4e15.7)') xm,ym,u,v
   rk2x = dt_little*u
   rk2y = dt_little*v
   xm=coormark(ip1)+0.5*rk2x 
   ym=coormark(ip2)+0.5*rk2y

   ! find average velocity at corrected midpoint
   call velintmark_cart(xm,ym,u,v,ielh,xi,eta,ks(ipsiav)%sol,'movetracers4_cart')
   !write(6,'(''u,v = '',4e15.7)') xm,ym,u,v
   rk3x = dt_little*u
   rk3y = dt_little*v
   xm=coormark(ip1)+rk3x 
   ym=coormark(ip2)+rk3y

   ! find new velocity at t+dt
   call velintmark_cart(xm,ym,u,v,ielh,xi,eta,ks(ipsi)%sol,'movetracers4_cart')
   !write(6,'(''u,v = '',4e15.7)') xm,ym,u,v
   rk4x = dt_little*u
   rk4y = dt_little*v

   coornewm(ip1) = coormark(ip1)+rk1x/6.0_8+rk2x/3.0_8+rk3x/3.0_8+rk4x/6.0_8
   coornewm(ip2) = coormark(ip2)+rk1y/6.0_8+rk2y/3.0_8+rk3y/3.0_8+rk4y/6.0_8
   !write(6,'(''u,v = '',2e15.7)') coornewm(ip1),coornewm(ip2)

enddo

! make sure beginning and end tracers stay on left and right hand boundary
ip=0
do ichain=1,nochain
   nmark=imark(ichain)
   ip1=ip+1
   coornewm(2*ip1-1)=xcmax
   ip2=ip+nmark
   coornewm(2*ip2-1)=xcmin
   ip=ip+nmark
enddo
end subroutine movetracers4

