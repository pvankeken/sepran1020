!   ICHECKINELEM
!
!   Check of point (xm,ym) is in the quadratic triangular
!   element (xn,yn).  For this, the barycentric coordinates
!   should all be positive
!
!   PvK 950508
integer function icheckinelem(xn,yn,xm,ym,rl)
implicit none
real(kind=8) :: xn(6),yn(6),xm,ym,rl(3)
real(kind=8) :: a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,rl1,rl2,rl3
logical :: out

a1 = xn(3)*yn(5) - yn(3)*xn(5)
a2 = -xn(1)*yn(5) + yn(1)*xn(5)
a3 = xn(1)*yn(3) - yn(1)*xn(3)
b1 = yn(3) - yn(5)
b2 = yn(5) - yn(1)
b3 = yn(1) - yn(3)
c1 = xn(5) - xn(3)
c2 = xn(1) - xn(5)
c3 = xn(3) - xn(1)
delta = -c3*b1 + b3*c1
a1 = a1/delta
a2 = a2/delta
a3 = a3/delta
b1 = b1/delta
b2 = b2/delta
b3 = b3/delta
c1 = c1/delta
c2 = c2/delta
c3 = c3/delta

out = .true.
rl(1) = a1 + b1*xm + c1*ym
rl(2) = a2 + b2*xm + c2*ym
rl(3) = a3 + b3*xm + c3*ym
out = (rl(1).ge.-1d-5).and.(rl(2).ge.-1d-5).and.(rl(3).ge.-1d-5)
icheckinelem=-999
if (out) icheckinelem=1

!phiq(1)=rl(1)*(2*rl(1)-1)
!phiq(3)=rl(2)*(2*rl(2)-1)
!phiq(5)=rl(3)*(2*rl(3)-1)
!phiq(2)=4*rl(1)*rl(2)
!phiq(4)=4*rl(2)*rl(3)
!phiq(6)=4*rl(3)*rl(1)
!dldx1 = b(1)
!!dldx2 = b(2)
!dldx3 = b(3)
!dldy1 = c(1)
!dldy2 = c(2)
!dldy3 = c(3)

!! derivatives of shape functions with respect to xi
!dphidx(1) = (4*phil(1)-1d0)*dldx1
!dphidx(3) = (4*phil(2)-1d0)*dldx2
!dphidx(5) = (4*phil(3)-1d0)*dldx3
!dphidx(2) = 4*(dldx1*phil(2) + dldx2*phil(1))
!dphidx(4) = 4*(dldx2*phil(3) + dldx3*phil(2))
!dphidx(6) = 4*(dldx3*phil(1) + dldx1*phil(3))
!! and with respect to eta
!dphidy(1) = (4*phil(1)-1d0)*dldy1
!dphidy(3) = (4*phil(2)-1d0)*dldy2
!dphidy(5) = (4*phil(3)-1d0)*dldy3
!dphidy(2) = 4*(dldy1*phil(2) + dldy2*phil(1))
!dphidy(4) = 4*(dldy2*phil(3) + dldy3*phil(2))
!dphidy(6) = 4*(dldy3*phil(1) + dldy1*phil(3))


end function icheckinelem
