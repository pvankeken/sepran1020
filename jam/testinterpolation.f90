subroutine testinterpolation
use tracers
use sepmodulekmesh
use msper01
use sepran_arrays
use sepmodulevecs
implicit none
integer :: i,ichoice,nodno(6),ielh,k,m,iu1lc(2)
real(kind=8) :: xm,ym,xn(6),yn(6),phil(3),phiq(6),dphidx(6),dphidy(6)
real(kind=8) :: psi,u,v,psin(6),func,du,dv,dvel,u1lc(2),xi,eta,dvsum,dusum,dvelsum
real(kind=8) :: umax,vmax
logical :: in_element


dvsum=0.0_8
dusum=0.0_8
dvelsum=0.0_8
umax=0.0_8
vmax=0.0_8
iu1lc(1)=11
call creavc(0,1001,1,ipsi,kmesh,kprob,iu1lc,u1lc)
nmark=imark(1)
do i=1,nmark
   xm=coormark(2*i-1)
   ym=coormark(2*i)
!  call pedetel(ichoice,xm,ym,ielh)
!  call sper01(kmeshc,coor,nodno,xn,yn,ielh)
   !correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,rl,xi,eta,phiq,isub,ielh)
   !correct=icheckinelem(xn,yn,xm,ym,phil)
!  call getshape6(xn,yn,xm,ym,phil,phiq,dphidx,dphidy,in_element)
!  write(6,'(2f12.3,i5,L5,3f12.3)') xm,ym,ielh,in_element,(phil(k),k=1,3)
!  psi_local=0.0
!  u_local=0.0_8
!  v_local=0.0_8
!  do k=1,6
!     psin(k)=func(11,xn(k),yn(k),yn(k))
!     !un(k)=func(12,xn(k),yn(k),yn(k))
      !vn(k)=func(13,xn(k),yn(k),yn(k))
!     write(6,'(''xn,yn,psi: '',9f12.3)') xn(k),yn(k),psin(k),(phiq(m),m=1,6)
!     psi_local=psi_local+phiq(k)*psin(k)
!     u_local=u_local-dphidy(k)*psin(k)
!     v_local=v_local+dphidx(k)*psin(k)
!  enddo
   call velintmark_cart(xm,ym,u,v,ielh,xi,eta,ks(ipsi)%sol,'testinterpolation')
   du=u-func(12,xm,ym,ym)
   dv=v-func(13,xm,ym,ym)
   umax=max(umax,abs(func(12,xm,ym,ym)))
   vmax=max(vmax,abs(func(12,xm,ym,ym)))
   dvel=sqrt(du*du+dv*dv)
   write(6,'(7e15.7)') xm,ym,u,func(12,xm,ym,ym),du,dv,dvel
   dvelsum=dvelsum+dvel
   dusum=abs(du)+dusum
   dvsum=abs(dv)+dvsum
enddo
write(6,*) 
write(6,*) dusum/nmark/umax,dvsum/nmark/vmax,dvelsum/nmark
call instop
end subroutine testinterpolation
