subroutine findomega(ichoice_coor,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
use sepmoduleoldrouts
use sepmodulekmesh
use coeff
use control
implicit none
integer :: kmesh,kprob,iomega,idTdx,intmat,iuser_here(*),ichoice_coor
real(kind=8) :: user_here(*)
integer :: matr=0,matrm=0,irhsd=0,iomrhsd=0
integer :: build_in(20),solve_in(20),iread,ip,i,iright,iinvec(11)
real(kind=8) :: solve_rin(20),factor
real(kind=8) :: omegamin,omegamax,pee=0.0_8,quu=0.0_8,rinvec(2)
integer :: i1,ip0,ichoice
logical :: first=.true.
save matr,irhsd,first,matrm,iomrhsd

use_coormark=.false.
use_coornewm=.false.
if (ichoice_coor==1) then
   use_coormark=.true.
else if (ichoice_coor==2) then
   use_coornewm=.true.
endif
! fill information about coefficients in iuser/user
iuser_here(3:iuser_here(1))=0
user_here(2:nint(user_here(1)))=0.0_8
iuser_here(2)=1
iuser_here(6)=7
! start information at iuser_here(7)
ip=6
! (1) not used
iuser_here(ip+1) = 0
! (2) type of upwinding 
iuser_here(ip+2) = 0
! (3) intrule
iuser_here(ip+3) = 2 ! let element decide on integration rule
! (4) icoor: type of coordinate system
iuser_here(ip+4) = 0    ! Cartesian
! (5) not yet used
iuser_here(ip+5) = 0

! (6) k11
iuser_here(ip+6)=-7
user_here(7)=1.0_8
! (7) k12
iuser_here(ip+7) = 0
! (8) k13
iuser_here(ip+8) = 0
! (9) k22
iuser_here(ip+9) = -7
! (10) k23
iuser_here(ip+10) = 0
! (11) k33
iuser_here(ip+11) = 0
! (12) u
iuser_here(ip+12) = 0
! (13) v
iuser_here(ip+13) = 0
! (14) w=0
iuser_here(ip+14) = 0
! (15) beta=0
iuser_here(ip+15) = 0
! (16) rhds
iuser_here(ip+16)=2001
iright=10
iuser_here(ip+17)=iright
ip=ip+1
! (17) rho*cp
iuser_here(ip+17)=-7

call copyvc(idTdx,iomrhsd)
   iinvec=0
   ! form iomrhsd = idTdx * exp((1-y)*DiG)
   iinvec(1)=11
   iinvec(2)=32  ! uout:=f(u1)
   iinvec(3)=1 ! idegfd
   iinvec(5)=1 ! number of vectors
   iinvec(11)=1 ! iprob
   rinvec(1)=3.0_8 ! signal to use funccf(3) for straight up density
   !write(6,*) 'iomrhsd: ',iomrhsd
   call manvec(iinvec,rinvec,iomrhsd,iomrhsd,iomrhsd,kmesh,kprob)

! make sure element information in ielemmark is up to date
if (abs(Rb)>1e-7) then
   tracer_buoyancy=.true. 
   call detelemtrac
endif


factor=-Ra ! note negative sign to offset negative sign assumed in front of Laplacian
!write(6,*) 'idTdx: ',idTdx
ichoice=0
call pecopy(ichoice,user_here(iright),iomrhsd,factor)
!write(6,*) 'algebr:'
call algebr(6,1,iomrhsd,i1,i1,kmesh,kprob,omegamin,omegamax,pee,quu,ip0)
!write(6,*) 'algebr done:'
! trial function T=1-y+0.1*sin(pi y)*cos(pi x)
! should yield dTdx=0.1*pi*sin(pi y)*sin(pi x)
! with f=Ra*dTdx the solution should yield
! omega = -0.1*Ra/(2*pi)*sin(pi x)*sin(pi y)
!write(6,'(''rho*dTdx   min/max = '',3e15.7)') omegamin,omegamax,-0.1*pi

!do i=1,npoint,100
!   write(6,'(''omega rhsd: '',e15.7)') user_here(iright+i-1)
!enddo
!call instop


! after setting coefficients build the linear system
build_in=0
build_in(1)=9
if (first) then
   build_in(2)=1 ! build S and f
   first=.false.
else
   build_in(2)=2 ! build just f
endif
build_in(9)=1
call build(build_in,matr,intmat,kmesh,kprob,irhsd,matrm,iomega,iomega,iuser_here,user_here)

! now solve it
solve_in=0
solve_rin=0.0_8
solve_in(1)=3
solve_in(3)=0
iread=-1
call solvel(solve_in,solve_rin,matr,iomega,irhsd,intmat,kmesh,kprob,iread)

call algebr(6,1,iomega,i1,i1,kmesh,kprob,omegamin,omegamax,pee,quu,ip0)
!write(6,'(''omega  min/max = '',3e15.7)') omegamin,omegamax,-0.05*factor/pi

! make sure to unset tracer_buoyancy so that it isn't added to the rhsd in the streamfunction equation
tracer_buoyancy=.false.

end subroutine findomega

