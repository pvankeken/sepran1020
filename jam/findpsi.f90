subroutine findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)
use sepmoduleoldrouts
use sepmodulekmesh
use control
use coeff
implicit none
integer :: kmesh,kprob,ipsi,ipsiold,iomega,intmat,iuser_here(*)
real(kind=8) :: user_here(*)
integer :: matr,matrm,irhsd,iright,ipsirhsd
integer :: build_in(20),solve_in(20),i,iread,ip,iinvec(11)
real(kind=8) :: solve_rin(20),factor,rinvec(2)
real(kind=8) :: psimin,psimax,pee=0.0_8,quu=0.0_8
integer :: i1,ip0
logical :: first=.true.
save matr,irhsd,first,matrm,ipsirhsd

!write(6,*) 'ipsi,ipsiold: ',ipsi,ipsiold
call copyvc(ipsi,ipsiold)
! fill information about coefficients in iuser/user
iuser_here(3:iuser_here(1))=0
user_here(2:nint(user_here(1)))=0.0_8
iuser_here(2)=1
iuser_here(6)=7
! start information at iuser_here(7)
ip=6
! (1) not used
iuser_here(ip+1) = 0
! (2) type of upwinding ; avoid with P2 for now
iuser_here(ip+2) = 0
! (3) intrule
iuser_here(ip+3) = 2 ! 0=let element decide on integration rule
! (4) icoor: type of coordinate system
iuser_here(ip+4) = 0    ! Cartesian
! (5) not yet used
iuser_here(ip+5) = 0

! (6) k11
iuser_here(ip+6)=-7
user_here(7)=1.0_8
! (7) k12
iuser_here(ip+7) = 0
! (8) k13
iuser_here(ip+8) = 0
! (9) k22
iuser_here(ip+9) = -7
! (10) k23
iuser_here(ip+10) = 0
! (11) k33
iuser_here(ip+11) = 0
! (12) u
iuser_here(ip+12) = 0
! (13) v
! implement Di*Gamma*dpsi/dy by setting v to -DiG
! (negative sign because coefficients of u.grad(c) have opposite sign to the diffusional operator)
if (ignore_advection) then
   iuser_here(ip+13) = 0
   user_here(8)=0
else
   iuser_here(ip+13) = -8
   user_here(8)=-DiG
endif
! (14) w=0
iuser_here(ip+14) = 0
! (15) beta=0
iuser_here(ip+15) = 0
! (16) rhds
iuser_here(ip+16)=2001
iright=10
iuser_here(ip+17)=iright
ip=ip+1
! (17) rho*cp
iuser_here(ip+17)=-7

call copyvc(iomega,ipsirhsd)
iinvec=0
! form ipsirhsd = iomega * exp(-y*DiG)
iinvec(1)=11
iinvec(2)=32  ! uout:=f(u1)
iinvec(3)=1 ! idegfd
iinvec(5)=1 ! number of vectors
iinvec(11)=2 ! iprob (streamfunction equation=2)
rinvec(1)=4.0_8 ! signal to use funccf(4)
!write(6,*) 'ipsirhsd: ',ipsirhsd
call manvec(iinvec,rinvec,ipsirhsd,ipsirhsd,ipsirhsd,kmesh,kprob)

factor=1 ! negative * negative = positive (I hope that still works)
!write(6,*) 'ipsirhsd: ',ipsirhsd
call pecopy(0,user_here(iright),ipsirhsd,factor)
call algebr(6,1,ipsirhsd,i1,i1,kmesh,kprob,psimin,psimax,pee,quu,ip0)
!write(6,'(''omega*rho1   min/max = '',3e15.7)') psimin,psimax,-0.1*Ra/(2*pi)
! trial function T=1-y+0.1*sin(pi y)*cos(pi x)
! should yield dTdx=0.1*pi*sin(pi y)*sin(pi x) for Di=0
! with f=Ra*dTdx the solution should yield
! omega = -0.1*Ra/(2*pi)*sin(pi x)*sin(pi y)
! and then psi=0.1*Ra/(4*pi^3)*sin(pi x)*sin(pi y)

!do i=1,npoint,100
!   write(6,'(''omega rhsd: '',e15.7)') user_here(iright+i-1)
!enddo
!call instop


! after setting coefficients build the linear system
build_in=0
build_in(1)=9
if (first) then
   build_in(2)=1 ! build S and f
   first=.false.
else
   build_in(2)=2 ! build just f
endif
build_in(9)=2
call build(build_in,matr,intmat,kmesh,kprob,irhsd,matrm,ipsi,ipsiold,iuser_here,user_here)

! now solve it
solve_in=0
solve_rin=0.0_8
solve_in(1)=3
solve_in(3)=0
iread=-1
!write(6,*) 'call solve'
call solvel(solve_in,solve_rin,matr,ipsi,irhsd,intmat,kmesh,kprob,iread)

!call algebr(6,1,ipsi,i1,i1,kmesh,kprob,psimin,psimax,pee,quu,ip0)
!write(6,'(''psi   min/max = '',3e15.7)') psimin,psimax,0.1*Ra/(4*pi*pi*pi)


end subroutine findpsi

