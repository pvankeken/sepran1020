subroutine setup_markerchain
use tracers
use control
implicit none
integer :: i
real(kind=8) :: x,piw


if (JGR97_appC) then
  nochain=1
  imark(1)=1
  nmark=1
  allocate(coormark(2),coornewm(2),velmark(2),velnewm(2))
  coormark(1)=0.5
  coormark(2)=0.0159221
  return
endif

nochain=1
ainit(1)=0.02
y0(1)=0.2
!dm(1)=0.01
if (dm(1)<1e-7) then
   write(6,*) 'dm is probably 0: ',dm(1)
   call instop
endif
wavel(1)=rlampix
imark(1)=rlampix/dm(1)+1
nmark=imark(1)
write(6,*) 'nmark: ',nmark
nmarkmax=100*nmark ! allow for 100x expansion of length of markerchain
allocate(coormark(2*nmarkmax),coornewm(2*nmarkmax),velmark(2*nmarkmax))
allocate(ielemmark(nmarkmax),xi_eta(2,nmarkmax))
allocate(rk1(2*nmarkmax),rk2(2*nmarkmax),rk3(2*nmarkmax))
dm(1)=rlampix/(nmark-1)
piw=pi/wavel(1)
do i=1,nmark
   x=dm(1)*(nmark-i)
   coormark(2*i-1)=x
   coormark(2*i)=y0(1)+ainit(1)*cos(piw*x)
enddo


end subroutine setup_markerchain

