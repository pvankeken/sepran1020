module geometry
! pexcyc.inc
integer, parameter :: NXCYCMAX=2500
real(kind=8),dimension(NXCYCMAX) :: xc,yc
real(kind=8) ::xcmin,xcmax,ycmin,ycmax,dx_two_elements
integer :: nx,ny

! cfilxy.inc
integer :: ifilchoice=0,ncurvn(4),icurv(4,10)
end module geometry
