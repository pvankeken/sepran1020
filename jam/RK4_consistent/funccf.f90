real(kind=8) function funccf(ichoice,x,y,z)
use coeff
implicit none
integer :: ichoice
real(kind=8) :: x,y,z

if (ichoice==3) then
   ! density
   funccf=exp((1-y)*DiG)
else if (ichoice==4) then
   ! simplified density profile from derivative exp(-y*DiG)
   funccf=exp(-y*DiG)
else if (ichoice==5) then
   ! simplified density profile from derivative exp(y*DiG)
   funccf=exp(y*DiG)
else if (ichoice==6) then
   funccf=1
endif

end function funccf


