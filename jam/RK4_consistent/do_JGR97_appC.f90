! trace particle along stream line as in Appendix C of JGR97
subroutine do_JGR97_appC()
use control
use coeff
use geometry
use tracers
use sepran_arrays
use mtime
implicit none
logical :: output
real(kind=8) :: tnew,dx,dy,x0t,y0t

dtoutp=0.2203712_8
tmaxp=dtoutp
toutp=dtoutp
call pedtcf
write(6,*) 'dtcfl = ',dtcfl
tfac=0.5_8
time_now=0.0_8
x0t=coormark(1)
y0t=coormark(2)

call copyvc(ipsi,ipsiold)
open(9,file='JGR97_path.dat')
niter=0
do
   niter=niter+1
   tstepp=dtcfl*tfac
   tnew=time_now+tstepp
   if (tnew>=tmaxp) then
      tstepp=tmaxp-time_now
      time_now=tmaxp
      output=.true.
   else if (tnew.ge.toutp) then
      tstepp = toutp-time_now
      time_now = toutp
      output = .true.
   else
      time_now = tnew
      output = .false.
   endif
   !write(6,*) 'tnew, tstepp: ',tnew,tstepp,toutp,tmaxp
   call movetracers4()
   call copcoor()
   write(9,*) coormark(1),coormark(2)
   if (time_now>=tmaxp) exit
enddo

write(6,'(''    Final time: '',f12.7,i5)') time_now,niter
write(6,'(''    Final x,y : '',2f12.7)') coormark(1),coormark(2)
dx = coormark(1)-x0t
dy = coormark(2)-y0t
write(6,'(''    Error     : '',3e15.7)') dx,dy,sqrt(dx*dx+dy*dy)

close(9)

end subroutine do_JGR97_appC

