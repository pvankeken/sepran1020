      subroutine sepelload1 ( source, jdiag, phi )
      use coeff
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 04-08-2015
!
!   copyright (c) 2015-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill the element vector with a load
!     Old subroutine el3003
! **********************************************************************
!
!                       KEYWORDS
!
!     element_vector
!     elasticity_equation
! **********************************************************************
!
!                       MODULES USED
!
      use sepmoduleelm
      use sepmodulebasefn
      use sepmodulecomio
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer, intent(in) :: jdiag
      double precision, intent(in) :: source(B%m), phi(B%n,B%m)

!     jdiag          i    if 1 the basis functions are 0 or 1 in the quadrature
!                         points
!                         if  0 they are general
!     phi            i    Contains the values of the basis functions in the
!                         quadrature points
!     source         i    right-hand side
!     vec            o    vector to be filled
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer :: i, n, kstep, k
      double precision :: work(B%m), h
      double precision, pointer :: vec(:)

!     h              Help variable to store a constant temporarily
!     i              General loop variable
!     k              Counting variable
!     kstep          Step in loop
!     n              Number of basis functions
!     work           work array to store help arrays
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRINRL         Print 1d real vector
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     The part of the element matrix is given by
!
!     / source phi_i  d Omega
!
!     The numerical values are given by
!
!     sum_k w(k) {source phi }(x_k)
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'sepelload1' )
      debug = .false. .and. ioutp>=0
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from sepelload1'
         write(irefwr,1) 'jdiag', jdiag
         call prinrl ( B%w, B%m, 'w' )
         call prinrl ( source, B%m, 'source' )
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      n = B%n
      kstep = B%kstep
      vec => E%subvec
      if ( debug ) write(irefwr,1) 'n, kstep', B%n, B%kstep

!     --- Store w source in work

      work = source*B%w

      if ( debug ) call prinrl ( work, B%m, 'work' )

      if ( jdiag==1 ) then

!     --- Special case: phi_i(x_j) = delta_ij

         if ( kstep==1 ) then
            vec = vec+work
         else
            do k = kstep, n, kstep
               vec(k) = vec(k)+work(k)
            end do  ! k = kstep, n, kstep
         end if  ! ( kstep==1 )

      else

!     --- General case

         do i = 1, n
            h = sum(work(:)*phi(i,:))
            vec(i) = vec(i)+h
         end do  ! i = 1, n
         if (tracer_buoyancy) call pesepelload1(vec,n,E%ielem)

         

      end if  ! ( jdiag==1 )

1000  if ( debug ) then

!     --- Debug information

         call prinrl ( vec, n, 'element vector' )
         write(irefwr,*) 'End sepelload1'

      end if  ! ( debug )
      call erclos ( 'sepelload1' )

      end subroutine sepelload1

