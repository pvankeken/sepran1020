! Add markerchain buoyancy to rhsd following CY84 
subroutine pesepelload1(vec,n,ielem)
use coeff
use tracers
implicit none
integer :: n,ielem
real(kind=8) :: vec(*)
integer :: itrac,k,ichoice=3
real(kind=8) :: xm,ym,phil(3),phiq(6),dphidksi(6),dphideta(6),slope
real(kind=8) :: xi,eta,funccf

nmark=imark(1)
! funccf(3) provides density
if (use_coormark) then
  do itrac=2,nmark-1 ! follow CY84
     if (ielemmark(itrac)==ielem) then 
        xi=xi_eta(1,itrac)
        eta=xi_eta(2,itrac)
        call getshape_quad_xi_eta(xi,eta,phil,phiq,dphidksi,dphideta)
        slope=(coormark(2*itrac+2)-coormark(2*itrac-2))*0.5_8
        do k=1,6
           vec(k)=vec(k)+slope*phiq(k)*funccf(ichoice,coormark(2*itrac-1),coormark(2*itrac),coormark(2*itrac))
        enddo
        !write(6,*) 'ielem: ',ielem,itrac
     endif
  enddo
else if (use_coornewm) then
  do itrac=2,nmark-1 ! follow CY84
     if (ielemmark(itrac)==ielem) then 
        xi=xi_eta(1,itrac)
        eta=xi_eta(2,itrac)
        call getshape_quad_xi_eta(xi,eta,phil,phiq,dphidksi,dphideta)
        slope=(coornewm(2*itrac+2)-coornewm(2*itrac-2))*0.5_8
        do k=1,6
           vec(k)=vec(k)+slope*phiq(k)*funccf(ichoice,coornewm(2*itrac-1),coornewm(2*itrac),coornewm(2*itrac))
        enddo
        !write(6,*) 'ielem: ',ielem,itrac
     endif
  enddo
endif
  

end subroutine pesepelload1

