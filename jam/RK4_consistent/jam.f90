!jam: new implementation of Jarvis&McKenzie 1980 stream-function vorticity formulation.
!Just Stokes equation under TALA with rho_bar=exp[(1-y)Di/gamma]
!
!\nabla^2 omega = rho_bar dT/dx
!
!\nabla^2 psi + Di/gamma dpsi/dz = omega exp(-zDi/gamma)

program jam
use sepmoduleoldrouts
use sepmodulekmesh
use geometry
use tracers
use control
use coeff
use msper01
use sepran_arrays
use mtime
implicit none

integer :: iu1lc(2),ncntln,ielhlp,icurvs(2),i,ielh,ichoice
real(kind=8) :: u1lc(2),contln(100),funcx(NFMAX),funcy(NFMAX),vrms,funccf
real(kind=8) :: curlv_max,y,xm,ym,xn(6),yn(6),xi,eta,psin(6),psi_local,u_local,v_local
real(kind=8) :: phil(3),phiq(6),dphidx(6),dphidy(6),func,tnew
integer :: nodno(6),nodlin(6),isub,k,m,irk4step
logical :: in_element,checkinelem,icheckinelem,output,exit_time_loop
real(kind=4) :: time0,timeA,timeB,cpu

namelist /yummie/ Di,Grueneisen,Ra,Rb,volume,JGR97_appC,dm,toutp,tmaxp,tfac,dtoutp,ncor

print_node=.true.
iuser_here(1)=1000
user_here(1)=NUSER*1.0
call sepstr(kmesh,kprob,intmat)
write(6,*) 'done with sepstr',npoint

open(9,file='jam.nml')
read(9,NML=yummie)
close(9)
if (Grueneisen>1e-7) DiG=Di/Grueneisen
if (Di>1e-7) compress=.true.
if (abs(Rb)>1e-7) tracer_buoyancy=.true.

if (ncor==0) stop 'ncor=0'

iu1lc=0
call creavc(0,1,1,iomega,kmesh,kprob,iu1lc,u1lc)
if (JGR97_appC) then
  iu1lc(1)=11
else
  iu1lc(1)=0
endif
call creavc(0,1001,1,ipsi,kmesh,kprob,iu1lc,u1lc)
iu1lc(1)=1
call creavc(0,2001,1,itemperature,kmesh,kprob,iu1lc,u1lc)


do i=1,11
   y=0.1*(i-1)
   write(6,'(''rhos: '',4f12.3)') y,funccf(3,0.0_8,y,0.0_8),funccf(4,0.0_8,y,0.0_8),funccf(5,0.0_8,y,0.0_8)
enddo

call pefilxy(2,kmesh,kprob,ipsi)
rlampix=xcmax-xcmin
write(6,*) 'rlam: ',rlampix
call setup_markerchain
! call testinterpolation

if (JGR97_appC) call do_JGR97_appC()

! we'll set up a fixed temperature that we won't evolve (for now)
! just need to make sure idTdx exists.
call copyvc(itemperature,itemperature_old)
! compute dTdx; ichoice=2,icheld=1,ix=1,jdegfd=1,ivec=1)
call deriva(2,1,1,1,1,idTdx,kmesh,kprob,itemperature,itemperature,iuser_here,user_here,ielhlp)
call copyvc(ipsi,ipsiold)

call cpu_time(time0)
! find initial solution to Stokes equations; 1=use coormark
call findomega(1,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
call findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)

inout=0
call markerout(1,inout)

!curlv_max=anorm(1,3,1,kmesh,kprob,iomega,iomega,ielhlp)
time_now=0.0_8
niter=0
exit_time_loop=.false.
timeA=time0
open(9,file='vrms.dat')
do 
   
   ! derive vrms etc. from latest Stokes update
   call findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)
   call pevrms(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here,vrms)
   write(9,*) time_now,vrms
   call tracvel(1,ipsi)
   call cpu_time(timeB)
   cpu=timeB-time0
   dcpu=timeB-timeA
   write(6,'(''T: '',4e15.7,i10,2f8.2)') time_now,vrms,coormark(2),velmark(2),imark(1),cpu,dcpu
   timeA=timeB
   niter=niter+1
   call pedtcf
   tstepp=dtcfl*tfac
   tnew=time_now+tstepp
   ! limit tstep if it brings time past the next output or maximum time set
   if (tnew>=tmaxp) then
      tstepp=tmaxp-time_now
      time_now=tmaxp
      output=.true.
      exit_time_loop=.true.
   else if (tnew.ge.toutp) then
      tstepp = toutp-time_now
      time_now = toutp
      toutp=toutp+dtoutp
      output = .true.
   else
      time_now = tnew
      output = .false.
   endif
   call copyvc(ipsi,ipsiold)

   do irk4step=1,4
      ! provide an update to the tracer positions  and compute rk(irk4step)
      call movetracers(irk4step)
      ! now find the predicted velocity at time t+dt after marker correction; 2=use coornewm
      call findomega(2,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
      call findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)
      call tracvel(2,ipsi)
   enddo
   ! final update to Stokes this time step
   call remarker
   call findomega(1,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
   call findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)
   if (output) then
      inout=inout+1
      ncntln=0
      call plotc1(1,kmesh,kprob,ipsi,contln,ncntln,15.0_8,1.0_8,1)
      call markerout(1,inout)
   endif
   if (exit_time_loop) exit
enddo
! find velocity and vrms at last stage
call findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)
call pevrms(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here,vrms)
write(6,'(''T: '',2e15.7,i10)') time_now,vrms,imark(1)
close(9)

!icurvs(1)=0
!icurvs(2)=4
!funcx(1)=1.0_8*NFMAX
!funcy(1)=1.0_8*NFMAX
!call compcr(0,kmesh,kprob,ively,1,icurvs,funcx,funcy)
!write(6,*) 'velocity y component at left side: '
!do i=1,nint(funcy(5))
!!   write(6,'(2f12.3)') funcx(5+2*i),funcy(5+i)
!enddo
!
!write(6,*) 'velocity x component at top: '
!icurvs(2)=3
!call compcr(0,kmesh,kprob,ivelx,1,icurvs,funcx,funcy)
!do i=1,nint(funcy(5))
!    write(6,'(2f12.3)') funcx(4+2*i),funcy(5+i)
!enddo
!
!write(6,*) 'vrms = ',vrms
!write(6,*) 'rho  = ',funccf(3,0.0_8,1.0_8,0.0_8),funccf(3,0.0_8,0.0_8,0.0_8)

!call plotc1(1,kmesh,kprob,itemperature,contln,ncntln,15.0_8,1.0_8,1)
!call plotc1(1,kmesh,kprob,idTdx,contln,ncntln,15.0_8,1.0_8,1)

ichoice=1 ! find velmark in coormark
call tracvel(ichoice,ipsi)
write(6,'('' tracer velocity: '',4e15.7)') coormark(1),coormark(2),velmark(1),velmark(2)

call finish(0)


end program jam

subroutine markerout(ichoice,inout)
use tracers
use sepran_arrays
implicit none
integer :: ichoice,inout
integer :: i
character(len=80) :: fname

nmark=imark(1)
call tracvel(ichoice,ipsi)
write(fname,'(''markers.'',i3.3)') inout
open(99,file=fname)
if (ichoice==1) then
   do i=1,nmark
      write(99,'(4e15.7)') coormark(2*i-1),coormark(2*i),velmark(2*i-1),velmark(2*i)
   enddo
   !if (inout==2) then
   !  do i=2,nmark
   !      write(6,*) coormark(2*i-1)-coormark(2*i-3)
   !   enddo
   !endif
else if (ichoice==2) then
   do i=1,nmark
      write(99,'(4e15.7)') coornewm(2*i-1),coornewm(2*i),velnewm(2*i-1),velnewm(2*i)
   enddo
endif
close(99)

end subroutine markerout
