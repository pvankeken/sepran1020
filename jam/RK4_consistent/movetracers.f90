! Move tracers with RK4 and store intermediate rk values 
subroutine movetracers(istep)
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulevecs
use tracers
use mtime
use geometry
use sepran_arrays
implicit none
integer :: istep
real(kind=8) :: dt_little
real(kind=8) :: rk1x,rk1y,rk2x,rk2y,rk3x,rk3y,rk4x,rk4y,oneminfrac1,oneminfrac2
integer :: itrac,ipsiav,ntot,ipoint,ip1,ip2,ielh,ip,ichoice
real(kind=8) :: pee,quu,xm,ym,u,v,xi,eta
real(kind=8) :: onehalf=0.5_8
save ipsiav

dt_little=tstepp
ntot=imark(1)
! find velocity at time now in coormark
ichoice=1
call tracvel(ichoice,ipsi)

if (istep==1) then
  do itrac=1,ntot
     ip1=2*itrac-1
     ip2=ip1+1
     xm=coormark(ip1)
     ym=coormark(ip2)
     rk1(ip1) = dt_little*velmark(ip1)
     rk1(ip2) = dt_little*velmark(ip2)
     coornewm(ip1) = coormark(ip1)+onehalf*rk1(ip1)
     coornewm(ip2) = coormark(ip2)+onehalf*rk1(ip2)
  enddo
  return
else if (istep==2) then
  do itrac=1,ntot
     ip1=2*itrac-1
     ip2=ip1+1
     xm=coormark(ip1)
     ym=coormark(ip2)
     rk2(ip1) = dt_little*velmark(ip1)
     rk2(ip2) = dt_little*velmark(ip2)
     coornewm(ip1) = coormark(ip1)+onehalf*rk2(ip1)
     coornewm(ip2) = coormark(ip2)+onehalf*rk2(ip2)
  enddo
  return
else if (istep==3) then
  do itrac=1,ntot
     ip1=2*itrac-1
     ip2=ip1+1
     xm=coormark(ip1)
     ym=coormark(ip2)
     rk3(ip1) = dt_little*velmark(ip1)
     rk3(ip2) = dt_little*velmark(ip2)
     coornewm(ip1) = coormark(ip1)+rk3(ip1)
     coornewm(ip2) = coormark(ip2)+rk3(ip2)
  enddo
  return
else if (istep==4) then
  do itrac=1,ntot
     ip1=2*itrac-1
     ip2=ip1+1
     xm=coormark(ip1)
     ym=coormark(ip2)
     rk4x = dt_little*velmark(ip1)
     rk4y = dt_little*velmark(ip2)
     coornewm(ip1) = coormark(ip1)+rk1(ip1)/6.0_8+rk2(ip1)/3.0_8+rk3(ip1)/3.0_8+rk4x/6.0_8
     coornewm(ip2) = coormark(ip2)+rk1(ip2)/6.0_8+rk2(ip2)/3.0_8+rk3(ip2)/3.0_8+rk4y/6.0_8
  enddo
  ! make sure beginning and end tracers stay on left and right hand boundary
  ip=0
  do ichain=1,nochain
     nmark=imark(ichain)
     ip1=ip+1
     coornewm(2*ip1-1)=xcmax
     ip2=ip+nmark
     coornewm(2*ip2-1)=xcmin
     ip=ip+nmark
  enddo
  return
else 
  write(6,*) 'PERROR(movetracers): incorrect irk4step: ',istep
  call instop
endif


end subroutine movetracers

