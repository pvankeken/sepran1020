subroutine tracvel(ichoice,ipsi)
use tracers
use sepmodulekmesh
use msper01
use sepmodulevecs
implicit none
integer :: ichoice,ipsi

call tracvel01(ichoice,kmeshc,coor,ks(ipsi)%sol,npoint)

end subroutine tracvel

subroutine tracvel01(ichoice,kmeshc,coor,psi,npoint)
use tracers
use msper01
implicit none
integer :: ichoice,kmeshc(*),npoint
real(kind=8) :: coor(*),psi(*)
integer :: itrac,ntot,jchoice,ielh,nodno(6),k
real(kind=8) :: xm,ym,xn(6),yn(6),phil(3),phiq(6),dphidx(6),dphidy(6),un(6),vn(6)
real(kind=8) :: u,v,psin(6),funccf
logical :: in_element

ntot=imark(1)
jchoice=1
do itrac=1,ntot
   if (ichoice==1) then
      xm=coormark(2*itrac-1)
      ym=coormark(2*itrac)
   else if (ichoice==2) then
      xm=coornewm(2*itrac-1)
      ym=coornewm(2*itrac)
   endif
   call pedetel(jchoice,xm,ym,ielh)
   call sper01(kmeshc,coor,nodno,xn,yn,ielh)
   !correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,rl,xi,eta,phiq,isub,ielh)
   !correct=icheckinelem(xn,yn,xm,ym,phil)
   call getshape6(xn,yn,xm,ym,phil,phiq,dphidx,dphidy,in_element)
   u=0
   v=0
   call sper06(psi(1:npoint),psin,nodno)
   ! find velocity from derivatives of streamfunction
   ! multiply by density profile (rhobar=1 if Di=0)
   do k=1,6
      u=u-dphidy(k)*psin(k)*funccf(5,xn(k),yn(k),yn(k))
      v=v+dphidx(k)*psin(k)*funccf(5,xn(k),yn(k),yn(k))
   enddo
   velmark(2*itrac-1)=u
   velmark(2*itrac)=v
enddo

end subroutine tracvel01
