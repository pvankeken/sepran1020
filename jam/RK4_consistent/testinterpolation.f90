subroutine testinterpolation
use tracers
use sepmodulekmesh
use msper01
implicit none
integer :: i,ichoice,nodno(6),ielh,k,m
real(kind=8) :: xm,ym,xn(6),yn(6),phil(3),phiq(6),dphidx(6),dphidy(6)
real(kind=8) :: psi_local,u_local,v_local,psin(6),func
logical :: in_element

nmark=imark(1)
do i=1,nmark
   xm=coormark(2*i-1)
   ym=coormark(2*i)
   ichoice=1
   call pedetel(ichoice,xm,ym,ielh)
   call sper01(kmeshc,coor,nodno,xn,yn,ielh)
   !correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,rl,xi,eta,phiq,isub,ielh)
   !correct=icheckinelem(xn,yn,xm,ym,phil)
   call getshape6(xn,yn,xm,ym,phil,phiq,dphidx,dphidy,in_element)
   write(6,'(2f12.3,i5,L5,3f12.3)') xm,ym,ielh,in_element,(phil(k),k=1,3)
   psi_local=0.0
   u_local=0.0_8
   v_local=0.0_8
   do k=1,6
      psin(k)=func(11,xn(k),yn(k),yn(k))
      !un(k)=func(12,xn(k),yn(k),yn(k))
      !vn(k)=func(13,xn(k),yn(k),yn(k))
      write(6,'(''xn,yn,psi: '',9f12.3)') xn(k),yn(k),psin(k),(phiq(m),m=1,6)
      psi_local=psi_local+phiq(k)*psin(k)
      u_local=u_local-dphidy(k)*psin(k)
      v_local=v_local+dphidx(k)*psin(k)
   enddo
   write(6,'(''psi_local '',2f12.5)') psi_local,func(11,xm,ym,ym)
   write(6,'(''u_local : '',2f12.5)') u_local,func(12,xm,ym,ym)
   write(6,'(''v_local : '',2f12.5)') v_local,func(13,xm,ym,ym)
enddo
end subroutine testinterpolation
