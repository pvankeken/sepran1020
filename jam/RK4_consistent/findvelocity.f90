subroutine findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)
use sepmoduleoldrouts
use coeff
use sepmodulekmesh
use sepmodulevecs
implicit none
integer :: kmesh,kprob,ipsi,ivelx,ively,iuser_here(*)
real(kind=8) :: user_here(*)
integer :: iphi=0,gradv11=0,gradv12=0,gradv22=0,isol1=0
integer :: ielhlp,iinvec(11),ip0,i1,iu1h(2),iinder(10)
real(kind=8) :: rinvec(2),velmin,velmax,pee=0.0,quu=0.0,u1h(2)
save iphi,gradv11,gradv12,gradv22,isol1

! u = -dpsi/dy
call deriva(2,1,2,1,1,ivelx,kmesh,kprob,ipsi,ipsi,iuser_here,user_here,ielhlp)
! v = dpsi/dy
call deriva(2,1,1,1,1,ively,kmesh,kprob,ipsi,ipsi,iuser_here,user_here,ielhlp)

iinvec=0
! multiply ivelx by -1
iinvec(1)=11
iinvec(2)=27  ! uout:=c1*u1+c2*u2
iinvec(3)=1 ! idegfd
iinvec(11)=2 ! iprob
rinvec(1)=-1.0_8 ! signal to use funccf(4)
rinvec(2)=0.0_8
call manvec(iinvec,rinvec,ivelx,ivelx,ivelx,kmesh,kprob)
call algebr(6,1,ivelx,i1,i1,kmesh,kprob,velmin,velmax,pee,quu,ip0)
!write(6,*) 'min/max velx: ',velmin,velmax
call algebr(6,1,ively,i1,i1,kmesh,kprob,velmin,velmax,pee,quu,ip0)
!write(6,*) 'min/max vely: ',velmin,velmax

if (compress) then
   iinvec=0
   ! multiply ivelx/y by exp(y*DiG)
   iinvec(1)=11
   iinvec(2)=32  ! uout:=f(u1)
   iinvec(3)=1 ! idegfd
   iinvec(5)=1 ! number of vectors
   iinvec(11)=2 ! iprob
   rinvec(1)=5.0_8 ! signal to use funccf(5)
   call manvec(iinvec,rinvec,ivelx,ivelx,ivelx,kmesh,kprob)
   call manvec(iinvec,rinvec,ively,ively,ively,kmesh,kprob)
   call algebr(6,1,ivelx,i1,i1,kmesh,kprob,velmin,velmax,pee,quu,ip0)
   !write(6,*) 'min/max velx: ',velmin,velmax
   call algebr(6,1,ively,i1,i1,kmesh,kprob,velmin,velmax,pee,quu,ip0)
   !write(6,*) 'min/max vely: ',velmin,velmax
endif

iu1h=0
!!!call creavc(0,1001,0,isol1,kmesh,kprob,iu1h,u1h,iu1h,u1h)

!!!call pecopyvc(ks(ivelx)%sol,ks(isol1),npoint)
! gradv11 = du/dx
!!!write(6,*) 'gradv11'
!!!iinder=0
!!!iinder(1)=10
!!!iinder(2)=1
!!!iinder(4)=1 ! icheld
!!!iinder(5)=2 ! ix
!!!iinder(6)=1 ! jdegfd
!!!iinder(7)=1 ! ivec
!!!call deriv(iinder,gradv11,kmesh,kprob,ivelx,ivelx,iuser_here,user_here)
!!!!call deriva(2,1,1,1,1,gradv11,kmesh,kprob,isol1,isol1,iuser_here,user_here,ielhlp)
!!!! gradv22 = dv/dy
!!!write(6,*) 'gradv22'
!!!call deriva(2,1,2,1,1,gradv22,kmesh,kprob,ively,ively,iuser_here,user_here,ielhlp)
!!!! gradv12 = du/dy = dv/dx
!!!write(6,*) 'gradv12'
!!!call deriva(2,1,1,1,1,gradv12,kmesh,kprob,ively,ively,iuser_here,user_here,ielhlp)

! while iuser/user still has streamfunction info in it...
!!!write(6,*) 'call getvisdip: '
!!!call getvisdip(kmesh,kprob,ivelx,ively,iphi,iuser_here,user_here)
!!!write(6,*) 'done with getvisdip: '

end subroutine findvelocity

subroutine pecopyvc(sol1,sol2,npoint)
implicit none
integer :: npoint
real(kind=8) :: sol1(*),sol2(*)
integer :: i

do i=1,npoint
   sol2(i)=sol1(i)
enddo

end subroutine pecopyvc
