! translate coornewm to coormark while regridding the markerchain to preserve similar distances
subroutine remarker
use tracers
use coeff
use geometry
implicit none
integer :: ipn,ipm,inew,ioldip,inewtot,ip,iold,nmnew(10)
real(kind=8) :: dx,dy,rl,x1,x2,y1,y2,distance,xnew,ynew

ipm=0
ipn=0
inewtot=1
do ichain=1,nochain
   nmark=imark(ichain)
   ip = 2*(ipn+1)-1
   coormark(ip) = coornewm(ip)
   coormark(ip+1) = coornewm(ip+1)
   inew=1
   do iold=1,nmark-1
     inew = inew+1
     inewtot = inewtot+1
     ip = 2*(ipm+iold)-1
     x1 = coornewm(ip)
     y1 = coornewm(ip+1)
     x2 = coornewm(ip+2)
     y2 = coornewm(ip+3)
     distance  = sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
     if (distance.gt.dm(ichain)) then
        xnew = 0.5*(x1+x2)
        ynew = 0.5*(y1+y2)
        ip = 2*(ipn+inew)-1
        coormark(ip) = xnew
        coormark(ip+1) = ynew
        inew=inew+1
        inewtot = inewtot+1
     endif
     if (inewtot.gt.nmarkmax) then
!       markerchain has become to big: routine failed
        write(6,*) 'PERROR(remarker_cart): markerchain too big'
        write(6,*) 'inewtot, NTRACMAX: ',inewtot,nmarkmax
        call instop
     endif
     ip = 2*(ipn+inew)-1
     coormark(ip) = x2
     coormark(ip+1) = y2
   enddo
   nmnew(ichain) = inew
   ipm = ipm+nmark
   ipn = ipn+inew
enddo

do ichain=1,nochain
   imark(ichain) = nmnew(ichain)
enddo

end subroutine remarker

