subroutine velintmark_cart(xm,ym,u,v,ielh,xi,eta,psi,callingroutine)
use sepmodulekmesh
use msper01
implicit none
real(kind=8) :: xm,ym,u,v,xi,eta,psi(*)
character(len=*) :: callingroutine
integer :: ielh
integer :: ichoice,nodno(6),k
real(kind=8) :: xn(6),yn(6),phil(3),phiq(6),dphidx(6),dphidy(6),psin(6),funccf
logical :: in_element

ichoice=1
call pedetel(ichoice,xm,ym,ielh)
call sper01(kmeshc,coor,nodno,xn,yn,ielh)
call getshape6(xn,yn,xm,ym,phil,phiq,dphidx,dphidy,in_element)
call sper06(psi(1:npoint),psin,nodno)
! find velocity from derivatives of streamfunction
u=0
v=0
do k=1,6
   u=u-dphidy(k)*psin(k)*funccf(5,xn(k),yn(k),yn(k))
   v=v+dphidx(k)*psin(k)*funccf(5,xn(k),yn(k),yn(k))
enddo

end subroutine velintmark_cart

