real(kind=8) function func(ichoice,x,y,z)
use control
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8),parameter :: psimax=250.0_8/(pi*pi*pi),velmax=250.0_8/(pi*pi)

if (ichoice==1) then
   func=1.0_8-y+0.1*sin(pi*y)*cos(pi*x)
else if (ichoice==11) then
   ! JGR97 App C stream function
   func=psimax*sin(pi*x)*sin(pi*y)
else if (ichoice==12) then
   ! JGR97 App C horizontal velocity
   func=-velmax*sin(pi*x)*cos(pi*y)
else if (ichoice==13) then
   ! JGR97 App C vertical velocity
   func=velmax*cos(pi*x)*sin(pi*y)
endif

end function func

