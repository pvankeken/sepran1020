subroutine pecopy(ichoice,user,isol,factor)
use sepmodulecomio
use sepmodulemain
use sepmodulekmesh
use sepmodulekprob
use sepmodulesol
use control
implicit none
interface
  subroutine pecopy01(ichoice,ndim,npoint,user,nusol,usol,indprpi,kprobpi,factor)
     integer,intent(in) :: ichoice,ndim,npoint,nusol,indprpi,kprobpi(:,:)
     real(kind=8),intent(in) :: usol(nusol)
     real(kind=8),intent(inout) :: user(*),factor
  end subroutine pecopy01
end interface     
integer,intent(in) :: ichoice,isol
real(kind=8),intent(inout) :: user(*),factor

! Make sure everything points to isol
!call sepactsolbf1(isol)

if (ichoice < 0 .or. ichoice==1 .or. ichoice > 3) then
   if (print_node) write(irefwr,*) 'PERROR(pecopy) not suited for ichoice other than 0,2,3 but: ',ichoice
   call instop
endif
 
if (npelm/=6) then
   if (print_node) write(irefwr,*) 'PERROR(pecopy) not suited for npelm other than 6'
   call instop
endif
if (ndim/=2) then
   if (print_node) write(irefwr,*) 'PERROR(pecopy) not suited for ndim other than 2'
   call instop
endif
!if (debug) write(irefwr,*) 'pecopy: indprpi=',indprpi
if (indprpi==0 .and. ichoice>0) then
   if (print_node) write(irefwr,*) 'PERROR(pecopy) expect kprob P here but indprpi = ',indprpi
   if (print_node) write(irefwr,*) 'ichoice = ',ichoice
   call instop
endif
!write(6,*) 'indprpi, indprfi: ',indprpi,indprfi
if (indprpi>0) then
   call pecopy01(ichoice,ndim,npoint,user,nusol,ks(isol)%sol,indprpi,kprobpi,factor)
else
   call pecopy02(ichoice,ndim,npoint,user,nusol,ks(isol)%sol,factor)
endif
end subroutine pecopy

subroutine pecopy02(ichoice,ndim,npoint,user,nusol,usol,factor)
integer,intent(in) :: ichoice,ndim,npoint,nusol
real(kind=8),intent(in) :: usol(nusol)
real(kind=8),intent(inout) :: user(*),factor

user(1:npoint)=factor*usol(1:npoint)

end subroutine pecopy02

subroutine pecopy01(ichoice,ndim,npoint,user,nusol,usol,indprp,kprobp,factor)
use control
use sepmodulecomio
implicit none
integer,intent(in) :: ichoice,ndim,npoint,nusol,indprp,kprobp(:,:)
real(kind=8),intent(in) :: usol(nusol)
real(kind=8),intent(inout) :: user(*),factor
integer :: i,ip
write(6,*) 'ichoice=',ichoice
if (ichoice==0) then
   !do i=1,npoint
   !   user(i)=usol(i)
   !enddo
   user(1:npoint) = usol(1:npoint)*factor
   !if (pedebug) write(irefwr,*) 'min/max user: ',minval(user(1:npoint)),maxval(user(1:npoint))
else
  do i=1,npoint
     ip=kprobp(i,ichoice-1)
     user(i)=usol(ip)*factor
     !if ((i/100)*100==i) write(6,'(2i7,f12.1)') i,ip,user(i)
  enddo
endif

end subroutine pecopy01

