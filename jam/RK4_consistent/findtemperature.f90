subroutine findtemperature(kmesh,kprob,ivelx,ively,itemperature,iuser_here,user_here)
use sepmoduleoldrouts
use sepmodulekmesh
implicit none
integer :: kmesh,kprob,ivelx,ively,itemperature,iuser_here(*)
real(kind=8) :: user_here(*)
integer :: iuser_cond,ihorvel,ivervel,ibeta,iright,itemp,ip,iphi=0,ielhlp
real(kind=8) :: factor,phimax
save iphi

! while iuser/user still has streamfunction info in it...
write(6,*) 'getvisdip'
call getvisdip(kmesh,kprob,ivelx,ively,iphi,iuser_here,user_here)

iuser_here(2:iuser_here(1))=0
user_here(2:nint(user_here(1)))=0.0_8
! position of real information in user_here(*)
iuser_cond=10
ihorvel=11
ivervel=11+npoint
ibeta=11+2*npoint
iright=11+3*npoint
itemp=11+4*npoint

phimax=anorm(1,3,1,kmesh,kprob,iphi,iphi,ielhlp)
write(6,*) 'phimax=',phimax
call instop

! first define integer information for coefficients
ip=14
! (1) not used
iuser_here(ip+1) = 0
! (2) type of upwinding ; avoid with P2 for now
iuser_here(ip+2) = 3
! (3) intrule
iuser_here(ip+3) = 0
! (4) icoor: type of coordinate system
iuser_here(ip+4) = 0
! (5) not yet used
iuser_here(ip+5) = 0

! (6) k11
iuser_here(ip+6) = -7
 user_here(7) = 1.0_8
iuser_here(ip+7) = 0
! (8) k13
iuser_here(ip+8) = 0
! (9) k22
iuser_here(ip+9) = -7
! (10) k23
iuser_here(ip+10) = 0
! (11) k33
iuser_here(ip+11) = 0

! (12) u
iuser_here(ip+12) = 2001
iuser_here(ip+13) = ihorvel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces
! (13) v
iuser_here(ip+13) = 2001
iuser_here(ip+14) = ivervel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (15) beta
iuser_here(ip+15) = 2001
iuser_here(ip+16) = ibeta
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (16) q    Note we use iright because we may add viscous dissipation to form
!           the right-hand side
iuser_here(ip+16)=2001
iuser_here(ip+17)=iright
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (17) rho*cp
iuser_here(ip+17)=3 ! funccf(3)

factor=1.0_8
call pecopy(0,user_here(itemp),itemperature,factor)


end subroutine findtemperature

