module sepran_arrays
implicit none
! sepran vector numbers
integer :: kmesh=0,kprob=0,intmat=0,intmat2=0
integer :: iomega=0,ipsi=0,idTdx=0,itemperature=0,ivelx=0,ively=0,ipsiold=0
integer :: itemperature_old=0,ivelx_old=0,ively_old=0,igradT=0,iheat=0
integer :: iadia=0,irho=0,icp=0,ialpha=0,icond=0,ipress=0,iphi=0,icompwork=0
! user arrays
integer :: iuser_here(1000)
integer, parameter :: NUSER=4200000,NFMAX=2000
real(kind=8) :: user_here(NUSER)

end module sepran_arrays
