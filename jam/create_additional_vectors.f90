! CREATE_ADDITIONAL_VECTORS
! 
! Set up vectors for density, expansivity etc.
! PvK 070511
subroutine create_additional_vectors()
use sepmodulecomio
use sepmoduleoldrouts
use coeff
use sepran_arrays
use control
implicit none
integer :: iu1(10),ipoint,i1,lu,i,ival
real(kind=8) ::  u1(10),tmin2,tmax2,p,q,avrho(6),avalpha(6)
real(kind=8) ::  avcond(6),avcp(6),rcond1,rcond2,rho1,rho2,funccf
real(kind=8) ::  Ks1,Ks2,Ta1,Ta2,cp1,cp2,alpha1,alpha2
real(kind=8) ::  pefvis,eta_top,eta_bot,rhos1,rhos2

! Create density vector (even if rho=1) and fill with analytical values
! With two problems on a single mesh choose these to become a solution 
iu1=0
! vector for temperature (otherwise errors appear in deriv and volint)
if (compress) then
!  use func(3)=funccf(3)
   iu1(1) = 3
else
!  density is constant
   iu1(1) = 0 
    u1(1) = 1d0
endif
call creavc(0,1001,1,irho,kmesh,kprob,iu1,u1,iu1,u1)


iu1(1) = 7
 u1(1) = 0d0
!write(6,*) 'PINFO(create_additional_vectors: ',iadiabat,Ts_eos0
call creavc(0,1001,1,iadia,kmesh,kprob,iu1,u1,iu1,u1)
iu1(1) = 0
call creavc(0,1,1,ipress,kmesh,kprob,iu1,u1,iu1,u1)
iu1(1)=0 
 u1(1)=0d0
! ichcrv = ichcr + (iprob-1)*1000
call creavc(0,1001,1,icompwork,kmesh,kprob,iu1,u1,iu1,u1)

!write(irefwr,*) 'stopping after <rho>'
!call instop

! iphi is used to find viscous dissipation in getvisdip()
call creavc(0,1001,1,iphi,kmesh,kprob,iu1,u1,iu1,u1)
call creavc(0,1001,1,iheat,kmesh,kprob,iu1,u1,iu1,u1)


!  expansivity is constant
iu1(1) = 0 
 u1(1) = 1d0
call creavc(0,1001,1,ialpha,kmesh,kprob,iu1,u1,iu1,u1)

!  conductivity is constant
iu1(1) = 0 
 u1(1) = 1d0
call creavc(0,1001,1,icond,kmesh,kprob,iu1,u1,iu1,u1)

!  cp is constant
iu1(1) = 0
 u1(1) = 1d0
call creavc(0,1001,1,icp,kmesh,kprob,iu1,u1,iu1,u1)

end subroutine create_additional_vectors
