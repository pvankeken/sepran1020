subroutine findnusselt(nusselt,qz)
use sepmoduleoldrouts
use control
use sepran_arrays
implicit none
real(kind=8) :: nusselt(*),qz(*)
integer :: ihelp=0,ip

call deriva(0,2,0,1,0,igradT,kmesh,kprob,itemperature,itemperature,iuser_here,user_here,ihelp)
nusselt(1)=-bounin(2,3,1,1,kmesh,kprob,3,3,igradT,iuser_here,user_here)
nusselt(2)=-bounin(2,3,1,1,kmesh,kprob,1,1,igradT,iuser_here,user_here)

end subroutine findnusselt
