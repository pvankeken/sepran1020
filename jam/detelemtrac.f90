subroutine detelemtrac
use tracers
use sepmodulekmesh
use msper01
use coeff
implicit none
integer :: i,ichoice,nodno(6),ielh,k,m,nodlin(6),isub
real(kind=8) :: xm,ym,xn(6),yn(6),phil(3),phiq(6),dphidx(6),dphidy(6)
real(kind=8) :: psi_local,u_local,v_local,psin(6),func,xi,eta
logical :: in_element,checkinelem,correct

nmark=imark(1)
if (use_coornewm) then
  do i=1,nmark
     xm=coornewm(2*i-1)
     ym=coornewm(2*i)
     ichoice=1
     call pedetel(ichoice,xm,ym,ielh)
     call sper01(kmeshc,coor,nodno,xn,yn,ielh)
     correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,phil,xi,eta,phiq,isub,ielh)
     ielemmark(i)=ielh
     xi_eta(1,i)=xi
     xi_eta(2,i)=eta
  enddo
else if (use_coormark) then
  do i=1,nmark
     xm=coormark(2*i-1)
     ym=coormark(2*i)
     ichoice=1
     call pedetel(ichoice,xm,ym,ielh)
     call sper01(kmeshc,coor,nodno,xn,yn,ielh)
     correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,phil,xi,eta,phiq,isub,ielh)
     ielemmark(i)=ielh
     xi_eta(1,i)=xi
     xi_eta(2,i)=eta
  enddo
endif

end subroutine detelemtrac
