real(kind=8) function func(ichoice,x,y,z)
use coeff
use control
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8),parameter :: psimax=250.0_8/(pi*pi*pi),velmax=250.0_8/(pi*pi)
real(kind=8) :: funccf

if (ichoice==1) then
   ! Cartesian conductive solution with harmonic perturbation
   func=1.0_8-y+0.1*sin(pi*y)*cos(pi*x)

else if (ichoice==3) then
   ! density
   func = exp(Di*(1.0_8-y))

else if (ichoice==7) then
   ! adiabatic temperature difference
   func=0.0 ! we're solving by default for full T

else if (ichoice==111) then
   ! JGR97 App C stream function
   func=psimax*sin(pi*x)*sin(pi*y)
!  func=x*x+y*y+x*y
endif

end function func

