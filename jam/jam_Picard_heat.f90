subroutine jam_Picard_heat
use sepmoduleoldrouts
use sepmodulekmesh
use geometry
use tracers
use control
use coeff
use msper01
use sepran_arrays
use mtime
implicit none
integer :: iu1lc(2),ncntln=0,ielhlp,icurvs(2),i,ielh,ichoice
real(kind=8) :: u1lc(2),contln(100),funcx(NFMAX),funcy(NFMAX),vrms,funccf
real(kind=8) :: curlv_max,y,xm,ym,xn(6),yn(6),xi,eta,psin(6),psi_local,u_local,v_local
real(kind=8) :: phil(3),phiq(6),dphidx(6),dphidy(6),func,tnew,nusselt(2),rinvec(2),tempmax
integer :: nodno(6),nodlin(6),isub,k,m,iinvec(12)=0,ihelp
logical :: in_element,checkinelem,icheckinelem,output,exit_time_loop,rechain=.true.,testinterpol=.false.
real(kind=4) :: time0,timeA,timeB,cpu
real(kind=8) :: qz(10000)

! just need to make sure idTdx exists.
call copyvc(itemperature,itemperature_old)
call plotc1(1,kmesh,kprob,itemperature,contln,ncntln,15.0_8,1.0_8,1)
! compute dTdx; ichoice=2,icheld=1,ix=1,jdegfd=1,ivec=1)
call deriva(2,1,1,1,1,idTdx,kmesh,kprob,itemperature,itemperature,iuser_here,user_here,ielhlp)
call copyvc(ipsi,ipsiold)
call cpu_time(time0)
! find initial solution to Stokes equations; 0=no tracers; 1=use coormark; 2=use coornewm
call findomega(0,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
call findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)
call findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)
call pevrms(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here,vrms)
! for test
call findtemperature()
call findnusselt(nusselt,qz)
write(6,'(i5,3f12.4)') 0,1.0_8,nusselt(1),vrms

do
   exit
   niter=niter+1
   call copyvc(ipsi,ipsiold)
   call copyvc(itemperature,itemperature_old)
   !write(6,*) 'temperature'
   call findtemperature()
   if (relax>1e-6_8) then 
      iinvec(1)=2
      iinvec(2)=27 ! linear combination
      rinvec(1)=1.0_8-relax
      rinvec(2)=relax
      call manvec(iinvec,rinvec,itemperature_old,itemperature,itemperature,kmesh,kprob)
   endif
   ! update dTdx
   call deriva(2,1,1,1,1,idTdx,kmesh,kprob,itemperature,itemperature,iuser_here,user_here,ielhlp)
   !write(6,*) 'omega'
   call findomega(0,kmesh,kprob,intmat,iomega,idTdx,iuser_here,user_here)
   !write(6,*) 'psi'
   call findpsi(kmesh,kprob,intmat,ipsi,ipsiold,iomega,iuser_here,user_here)
   !write(6,*) 'vrms'
   !write(6,*) 'velocity'
   call findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)
   !write(6,*) 'vrms'
   call pevrms(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here,vrms)
   !write(6,*) 'nusselt'
   call findnusselt(nusselt,qz)
   dif2=anorm(0,3,0,kmesh,kprob,itemperature,itemperature_old,ihelp)
   tempmax=anorm(1,3,0,kmesh,kprob,itemperature,itemperature,ihelp)
   dif2=dif2/tempmax
   write(6,'(i5,3f12.4)') niter,dif2,nusselt(1),vrms
   if (dif2<eps .or. niter>=nitermax) exit
enddo
call plotc1(1,kmesh,kprob,iomega,contln,ncntln,15.0_8,1.0_8,1)
call plotc1(1,kmesh,kprob,ipsi,contln,ncntln,15.0_8,1.0_8,1)
call plotc1(1,kmesh,kprob,itemperature,contln,ncntln,15.0_8,1.0_8,1)
call plotvc(-1,-1,ivelx,ively,kmesh,kprob,15.0_8,1.0_8,0.0_8)


end subroutine jam_Picard_heat

