subroutine phifromgrad(gradv11,gradv12,gradv22,phi,npoint)
implicit none
integer :: npoint
real(kind=8),dimension(*) :: gradv11,gradv12,gradv22,phi
integer :: i
real(kind=8) :: stress11,stress22,stress12

do i=1,npoint
   stress11 = 4.0_8/3*gradv11(i)-2.0_8/3*gradv22(i)
   stress22 = 4.0_8/3*gradv22(i)-2.0_8/3*gradv11(i)
   stress12 = 2*gradv12(i)
   phi(i)=stress11*gradv11(i)+stress22*gradv22(i)+stress12*stress12
enddo

end subroutine phifromgrad
