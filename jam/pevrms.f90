subroutine pevrms(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here,vrms)
use sepmodulekmesh
use sepmodulevecs
use coeff
implicit none
integer :: ivelx,ively,kmesh,kprob,ipsi
real(kind=8) :: user_here(*),vrms
integer :: iuser_here(*)
integer :: i,iinvol(10)
real(kind=8) :: outval(1)

iuser_here(2) = 1
iuser_here(3) = 0
iuser_here(4) = 0
iuser_here(5) = 0
iuser_here(6) = 7
iuser_here(7) = 0
iuser_here(8) = 0
iuser_here(9) = 0
iuser_here(10) = 2001
iuser_here(11) = 6

!call sepactsolbf1(ivelx)
!write(6,*) 'pevrms01: '
call pevrms01(npoint,ks(ivelx)%sol,ks(ively)%sol,user_here(6))
iinvol=0
!write(6,*) 'integr: '
call integr(iinvol,outval,kmesh,kprob,ipsi,iuser_here,user_here)
vrms=sqrt(abs(outval(1))/volume)



end subroutine pevrms

subroutine pevrms01(npoint,velx,vely,user_here)
implicit none
integer :: npoint
real(kind=8) :: velx(*),vely(*),user_here(*)
integer :: i

do i=1,npoint
   user_here(i)=velx(i)*velx(i)+vely(i)*vely(i)
enddo

end subroutine pevrms01
