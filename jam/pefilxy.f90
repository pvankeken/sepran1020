! PEFILXY
! 
! Fill the coordinates of the primary nodes of a rectangular
! mesh in common /pexcyc/
!    
! 
! PvK 2/1/99 Updated to allow for more than one curve along
!            either the bottom or side boundary. Information
!            on the curve definition should be stored in 
!            cfilxy.inc
subroutine pefilxy(ishape,kmesh,kprob,isol)
use geometry
implicit none
integer ishape,kmesh(*),kprob(*),isol(*)

real(kind=8) :: funcy,dx,dy,dxmin,dymin
integer :: number,i,ic,nc
integer, parameter :: NMAX=10000 
real(kind=8) ::funcx(NMAX)
integer :: icurvs(12)

if (ifilchoice==0.or.ifilchoice==1) then
   call oldpefilxy(ishape,kmesh,kprob,isol)
   return
endif

! Bottom curve first
nc = ncurvn(1)

if (nc==1) then
   funcx(1) = NMAX
   icurvs(1)=0
   icurvs(2)=icurv(1,1)
else
   funcx(1) = NMAX
   icurvs(1)=nc
   do i=1,nc
      icurvs(i+1) = icurv(1,i)
   enddo
endif
call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
if (funcx(5)/2.gt.NXCYCMAX) then
   write(6,*) 'PERROR(pefilxy) Mesh is too large in x-direction.'
   write(6,*) 'Limit number of x-nodal points to NXCYCMAX'
   write(6,*) 'nx       = ',funcx(5)/2
   write(6,*) 'NXCYCMAX = ',NXCYCMAX
endif
if (ishape==1) then
! linear elements
  nx = funcx(5)/2
  do i=1,nx
     xc(i) = funcx(4+2*i)
  enddo
else if (ishape==2) then
! Quadratic elements
  nx = funcx(5)/4+1
  do i=1,nx
     xc(i) = funcx(2+4*i)
  enddo
endif

! *** Now the side curve 
nc = ncurvn(2)
if (nc==1) then
   funcx(1) = NMAX
   icurvs(1)=0
   icurvs(2)=icurv(2,1)
else
   funcx(1) = NMAX
   icurvs(1)=nc
   do i=1,nc
      icurvs(i+1) = icurv(2,i)
   enddo
endif

call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
if (funcx(5)/2.gt.NXCYCMAX) then
   write(6,*) 'PERROR(pefilxy) Mesh is too large in y-direction.'
   write(6,*) 'Limit number of y-nodal points to NXCYCMAX'
   write(6,*) 'nx       = ',funcx(5)/2
   write(6,*) 'NXCYCMAX = ',NXCYCMAX
endif
if (ishape==1) then
!  *** linear elements
   ny = funcx(5)/2
   do i=1,ny
      yc(i) = funcx(5+2*i)
   enddo
else if (ishape==2) then
!  *** Quadratic elements
   ny = funcx(5)/4+1
   do i=1,ny
      yc(i) = funcx(3+4*i)
   enddo
endif

! Finalize the data in /pexcyc/
xcmin = xc(1)
ycmin = yc(1)
xcmax = xc(nx)
ycmax = yc(ny)

dxmin = xc(2)-xc(1)
do ic=2,nx-1
   dx = xc(ic+1)-xc(ic)
   dxmin = min(dxmin,dx)
enddo
 
dymin = yc(2)-yc(1)
do ic=2,ny-1
   dy = yc(ic+1)-yc(ic)
   dymin = min(dymin,dy)
enddo
      
if (ishape==2) then 
   dxmin = dxmin/2 
   dymin = dymin/2
endif

! assume no grid refinement in horizontal direction
dx_two_elements=4*(xcmax-xcmin)/nx
!write(6,*) 'dx_two_elements = ',dx_two_elements,xcmax,xcmin,nx

end subroutine pefilxy

! OLDPEFILXY
! 
! Fill the coordinates of the primary nodes of a rectangular
! mesh in common /pexcyc/
! 
! 
! PvK 10-8-89
subroutine oldpefilxy(ishape,kmesh,kprob,isol)
use geometry
implicit none
integer :: ishape,kmesh(*),kprob(*),isol(*)

real(kind=8) :: funcy,dx,dy,dxmin,dymin
integer :: icurvs(2),number,i,ic
integer,parameter :: NMAX=10000
real(kind=8) :: funcx(NMAX)

funcx(1) = NMAX
icurvs(1)=0
icurvs(2)=1
call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
if (funcx(5)/2.gt.NXCYCMAX) then
   write(6,*) 'PERROR(pefilxy) Mesh is too large in x-direction.'
   write(6,*) 'Limit number of x-nodal points to NXCYCMAX'
   write(6,*) 'nx       = ',funcx(5)/2
   write(6,*) 'NXCYCMAX = ',NXCYCMAX
endif
if (ishape==1) then
!  linear elements
   nx = funcx(5)/2
   do i=1,nx
      xc(i) = funcx(4+2*i)
   enddo
else if (ishape==2) then
!  quadratic elements
   nx = funcx(5)/4+1
   do i=1,nx
      xc(i) = funcx(2+4*i)
   enddo
endif

icurvs(1)=0
icurvs(2)=2
call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
if (funcx(5)/2.gt.NXCYCMAX) then
   write(6,*) 'PERROR(pefilxy) Mesh is too large in y-direction.'
   write(6,*) 'Limit number of y-nodal points to NXCYCMAX'
   write(6,*) 'ny       = ',funcx(5)/2
   write(6,*) 'NXCYCMAX = ',NXCYCMAX
   call instop
endif
if (ishape==1) then
!  linear elements
   ny = funcx(5)/2
   do i=1,ny
      yc(i) = funcx(5+2*i)
   enddo
else if (ishape==2) then
!  quadratic elements
   ny = funcx(5)/4+1
   do  i=1,ny
      yc(i) = funcx(3+4*i)
   enddo
endif
      
xcmin = xc(1)
ycmin = yc(1)
xcmax = xc(nx)
ycmax = yc(ny)

dxmin = xc(2)-xc(1)
do ic=2,nx-1
   dx = xc(ic+1)-xc(ic)
   dxmin = min(dxmin,dx)
enddo
 
dymin = yc(2)-yc(1)
do ic=2,ny-1
   dy = yc(ic+1)-yc(ic)
   dymin = min(dymin,dy)
enddo
      
if (ishape==2) then 
   dxmin = dxmin/2 
   dymin = dymin/2
endif
! assume no grid refinement in horizontal direction
dx_two_elements=4*(xcmax-xcmin)/nx
!write(6,*) 'dx_two_elements = ',dx_two_elements,xcmax,xcmin,nx

end subroutine oldpefilxy

