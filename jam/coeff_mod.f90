module coeff

real(kind=8) :: Di=0.0_8,Grueneisen=1.0_8,DiG=0.0_8,Ra=0.0_8,volume=1.0_8,Rb=0,DiRa=0.0_8
logical :: compress=.false.,tracer_buoyancy=.false.,use_coormark=.false.,use_coornewm=.false.
real(kind=8) :: Ts_dimK=273_8,deltaT_dim=3000.0_8
real(kind=8) :: Ts_nondimK
integer :: ialphatype=0


end module coeff
