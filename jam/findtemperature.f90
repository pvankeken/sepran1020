subroutine findtemperature()
use sepmodulecomio
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulekprob
use sepran_arrays
use coeff
implicit none
integer :: iuser_cond,ihorvel,ivervel,ibeta,iright,itemp,ip,ielhlp,irhocp,i
integer :: build_in(20),solve_in(20),iread
real(kind=8) :: solve_rin(20)
real(kind=8) :: factor,phimax
integer :: matr=0,matrm=0,irhsd=0
save matr,matrm,irhsd

! while iuser/user still has streamfunction info in it...
!write(irefwr,*) 'getvisdip'
call getvisdip(kmesh,kprob,ivelx,ively,iphi,iuser_here,user_here)
!write(irefwr,*) 'done with getvisdip'

call fillcoef800()

build_in=0
build_in(1)=10 ! maximum number of entries
build_in(2)=1 ! build matrix and rhsd vector
build_in(9)=0
!write(6,*) 'matr=',matr
call build(build_in,matr,intmat2,kmesh,kprob,irhsd,matrm,itemperature,itemperature,iuser_here,user_here)

solve_in=0
solve_rin=0.0_8
solve_in(1)=3
solve_in(3)=0
iread=-1
call solvel(solve_in,solve_rin,matr,itemperature,irhsd,intmat,kmesh,kprob,iread)



end subroutine findtemperature

subroutine fillcoef800()
use sepmodulecomio
use sepmoduleoldrouts
use sepmodulekmesh
use sepmodulekprob
use sepran_arrays
use coeff
implicit none
integer :: iuser_cond,ihorvel,ivervel,ibeta,iright,itemp,ip,ielhlp,irhocp,i
real(kind=8) :: factor,phimax
iuser_here(2:iuser_here(1))=0
user_here(2:nint(user_here(1)))=0.0_8
! position of real information in user_here(*)
iuser_cond=10
ihorvel=10+npoint
ivervel=10+2*npoint
ibeta=10+3*npoint
iright=10+4*npoint
irhocp=10+5*npoint
itemp=10+6*npoint

!phimax=anorm(1,3,1,kmesh,kprob,iphi,iphi,ielhlp)
!write(irefwr,*) 'phimax=',phimax
!call instop

iuser_here(2:iuser_here(1))=0
 user_here(2:nint(user_here(1)))=0.0_8
iuser_here(2)=1
iuser_here(6)=15 ! start of actual information

! first define integer information for coefficients
ip=14
! (1) not used
iuser_here(ip+1) = 0
! (2) type of upwinding ; avoid with P2 for now
iuser_here(ip+2) = 0
! (3) intrule
iuser_here(ip+3) = 0
! (4) icoor: type of coordinate system
iuser_here(ip+4) = 0
! (5) not yet used
iuser_here(ip+5) = 0

! (6) k11
iuser_here(ip+6) = 2001
iuser_here(ip+7) = iuser_cond
ip=ip+1 ! account for increment for type 2001
! (7) k12
iuser_here(ip+7) = 0
! (8) k13
iuser_here(ip+8) = 0
! (9) k22
iuser_here(ip+9) = 2001
iuser_here(ip+10) = iuser_cond
ip=ip+1 ! account for increment for type 2001
! (10) k23
iuser_here(ip+10) = 0
! (11) k33
iuser_here(ip+11) = 0

! (12) u
iuser_here(ip+12) = 2001
iuser_here(ip+13) = ihorvel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces
! (13) v
iuser_here(ip+13) = 2001
iuser_here(ip+14) = ivervel
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (14) w==0
iuser_here(ip+14) = 0 

! (15) beta
iuser_here(ip+15) = 2001
iuser_here(ip+16) = ibeta
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (16) q    Note we use iright because we may add viscous dissipation to form
!           the right-hand side
iuser_here(ip+16)=2001
iuser_here(ip+17)=iright
ip=ip+1 ! account for increment since type 2001 takes two integer spaces

! (17) rho*cp
iuser_here(ip+17)=2001 
iuser_here(ip+18)=irhocp
ip=ip+1

factor=1.0_8
call pecopy(0,user_here(itemp),itemperature,factor)

! actual values for conductivity
user_here(iuser_cond:iuser_cond+npoint-1)=1.0_8
! actual values for velocity
call pecopy(0,user_here(ihorvel),ivelx,factor)
call pecopy(0,user_here(ivervel),ively,factor)
! actual values for temperature
call pecopy(0,user_here(itemp),itemperature,factor)

! work & phi needs to be added for Di>0
write(irefwr,*) 'pecophi'
call pecophi(user_here(iright:iright+npoint-1),iphi,iheat)

write(irefwr,*) 'fill800_beta'
call fill800_beta_phase(npoint,user_here(ihorvel),user_here(ivervel),user_here(ibeta),user_here(iright), & 
     & user_here(irhocp),user_here(itemp), &
     & user_here(iuser_cond),coor,ks(iadia)%sol,ks(ialpha)%sol,ks(icp)%sol,ks(irho)%sol,ks(itemperature)%sol)

do i=1,100
   write(irefwr,'(i20,i20,e15.7)') i,iuser_here(i),user_here(i)
enddo
call coef800_print(npoint,coor,user_here(ihorvel),user_here(ivervel),user_here(ibeta),user_here(iright), &
      & user_here(irhocp),user_here(itemp),user_here(iuser_cond))

end subroutine fillcoef800

! Start of rebuild fill800_beta_phase as in comprP but for Cartesian only
! No phase changes yet
! PvK 100121
subroutine fill800_beta_phase(N,horvel,vervel,beta,right,rhocp,temp,cond,coor,adia,alpha,cp,rhoa,temp2)
use sepmodulecomio
use coeff
use control
implicit none
integer :: N
real(kind=8),dimension(*) :: horvel,vervel,beta,right,rhocp,temp,cond,adia,alpha,cp,rhoa,temp2
real(kind=8) :: coor(2,N)
integer :: i
real(kind=8) :: radial_velocity,x,y,cps,rho,alphas,rhoalphas,rhocps

do i=1,N
   x=coor(1,i)
   y=coor(2,i)
   radial_velocity=vervel(i)
   cps=1.0_8
   if (compress) then
      rho=rhoa(i)
   else
      rho=1.0_8
   endif
   alphas=alpha(i)
   rhoalphas=rho*alphas
   rhocps=rho*cps
   beta(i)=rhoalphas*Di*radial_velocity
   right(i)=right(i)+rhoalphas*Di*radial_velocity*(-Ts_nondimK)
   rhocp(i)=rhocps
enddo

end subroutine fill800_beta_phase

