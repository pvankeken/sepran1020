subroutine pedtcf
use sepmodulevecs
use sepmodulekmesh
use mtime
use sepran_arrays
implicit none

! find velocity from given streamfunction vector
call findvelocity(kmesh,kprob,ipsi,ivelx,ively,iuser_here,user_here)

call pedtcf2(nelem,kmeshc,coor,ks(ivelx)%sol,ks(ively)%sol,dtcfl,npelm)

end subroutine pedtcf

subroutine pedtcf2(nelem,kmeshc,coor,xvel,yvel,dtcfl,npelm)
implicit none
integer :: kmeshc(*),nelem,npelm
real(kind=8) :: coor(2,*),xvel(*),yvel(*),dtcfl
integer :: index(7),i,ipc,j
real(kind=8) :: u(7),v(7),dx1,dy1,dx2,dy2,dx,dy,x,y,udx,vdy
real(kind=8) :: ubiggest,vbiggest,xsmallest,ysmallest,dx3,dy3
integer :: ismallest

dtcfl=1e30_8
do i=1,nelem
   ! find the nodal point numbers in this element from kmesh part c
   ipc = npelm*(i-1)+1
   do j=1,npelm
      index(j) = kmeshc(ipc+j-1)
   enddo
   ! find the length of the sides of the quadratic element
   dx1 = abs(coor(1,index(1)) - coor(1,index(3)))
   dy1 = abs(coor(2,index(3)) - coor(2,index(5)))
   dx2 = abs(coor(1,index(3)) - coor(1,index(5)))

   dy2 = abs(coor(2,index(1)) - coor(2,index(3)))
   dx3 = abs(coor(1,index(5)) - coor(1,index(1)))
   dy3 = abs(coor(2,index(5)) - coor(2,index(1)))
   ! find maximum size of element in x and y
   ! then divide by two for quadratic elements
   dx = max(dx1,dx2,dx3)/2
   dy = max(dy1,dy2,dy3)/2
   do j=1,npelm
      ! get the velocity in the nodal points
      u(j) = xvel(index(j))
      v(j) = yvel(index(j))
      x = coor(1,index(j))
      y = coor(2,index(j))
      ! exclude regions with very low velocity
      if (abs(u(j)).gt.1e-7.and.abs(v(j)).gt.1e-7) then
         udx = dx/abs(u(j))
         vdy = dy/abs(v(j))
         dtcfl = min(dtcfl,udx,vdy)
         ismallest = i
         xsmallest = dx
         ysmallest = dy
         ubiggest = u(j)
         vbiggest = v(j)
      endif
   enddo
   !write(6,'(''element, max(u),max(v): '',i5,4f12.3,e15.7)') i,maxval(abs(u(1:6))),maxval(abs(v(1:6))),dx,dy,dtcfl
enddo

end subroutine pedtcf2

