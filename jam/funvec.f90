subroutine funvec(rinvec,reavec,nvec,coor,outvec)
implicit none
integer :: nvec
real(kind=8) :: rinvec(*),reavec(*),coor(2),outvec
real(kind=8) :: funccf,rho
integer :: ichoice

ichoice=nint(rinvec(1))
rho=funccf(ichoice,coor(1),coor(2),coor(2))
outvec=reavec(1)*rho

end subroutine funvec

