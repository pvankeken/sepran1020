      program mysepcomp
      call sepcom(0)
      end

      real*8 function funccf(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z
      real*8 pi
      parameter(pi=3.1415926)
      integer N
      parameter(N=3)
      real*8 xa(2,N),ya(2,N)
      logical xdomain,ydomain
      integer i,j
    
      xa(1,1) = 1d0/9
      xa(2,1) = 2d0/9
      xa(1,2) = 1d0/3+1d0/9
      xa(2,2) = 1d0/3+2d0/9
      xa(1,3) = 2d0/3+1d0/9
      xa(2,3) = 2d0/3+2d0/9
      ya=xa
      
      ydomain = .false.
      xdomain = .false.
      do j=1,N
         if (x.ge.xa(1,j).and.x.le.xa(2,j)) xdomain=.true.
      enddo
      do j=1,N
         if (y.ge.ya(1,j).and.y.le.ya(2,j)) ydomain=.true.
      enddo
      
      funccf=0
      if (ichoice == 1) then  
         if (xdomain.and.ydomain) funccf = 9d0
      endif

      end

      real*8 function func(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z,funccf

      func = funccf(ichoice,x,y,z)

      end
      

