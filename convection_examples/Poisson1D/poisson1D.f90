program poisson1D

call sepcom(0)

end program poisson1D

subroutine userout(kmesh,kprob,isol,isequence,numvec)
implicit none
integer :: kmesh(*),kprob(*),isol(5,*)
integer :: isequence,numvec,nunks,ncoor,ndim

integer :: map(5),iinmap(10)
real(kind=8) :: coor(100),T(100),q(100)

map=0
iinmap(1)=1
coor=0

nunks=1
ncoor=1
ndim=1
coor(1)=2*sqrt(2.0e0_8)
call intcoor(kmesh,kprob,isol(1,1),T,coor,nunks,ncoor,ndim,iinmap,map)
write(6,*) 'T2 = ',T(1)
!coor(1)=0.0_8
!call intcoor(kmesh,kprob,isol(1,2),q,coor,nunks,ncoor,ndim,iinmap,map)
!write(6,*) 'q1 = ',q(1)

end subroutine userout
