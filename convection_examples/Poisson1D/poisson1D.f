      program poisson1D
      implicit none
      integer NBUFDEF
      parameter(NBUFDEF=500 000 000)
      integer ibuffr
      common ibuffr(NBUFDEF)
      
      call sepcom(NBUFDEF)
      
      end

      subroutine userout(kmesh,kprob,isol,isequence,numvec)
      implicit none
      integer kmesh(*),kprob(*),isol(*)
      integer isequence,numvec
      real second

      write(6,'(''cpu time is : '',f12.2)') second()
      
      end
