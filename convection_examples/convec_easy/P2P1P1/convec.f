       program convec
       implicit none

       call sepcom(0)
  
       end

       real*8 function func(ichoice,x,y,z)
       implicit none
       real*8 x,y,z,pi
       parameter(pi=3.1415926)
       integer ichoice

       if (ichoice.eq.1) then
          func = 1-y + 0.1*cos(pi*x)*sin(pi*y)
       else if (ichoice.eq.2) then
          func = cos(pi*x)*sin(pi*y)
       else if (ichoice == 3) then
          func = -sin(pi*x)*cos(pi*y)
       endif
  
       return 
       end

       subroutine funalg(val1,val2,val3)
       implicit none
       real*8 val1,val2,val3
       val3 = val1*val1 + val2*val2
 
       return
       end
       
       
