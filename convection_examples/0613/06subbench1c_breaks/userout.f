c *************************************************************
c *   USEROUT
c *
c *   Code to output surface heat flow and geotherm along
c *   slab interface
c *   Dimensional heat flow by q=q'/r, with r=1d0/(k*dT/dz)=
c *    1d0/(2.5*1/1e3)=400; or q=q'/0.4 to output in mW/m^2.
c *************************************************************
      subroutine userout(kmesh,kprob,isol,isequence,numvec)
      implicit none
      integer kmesh(*),kprob(*),isol(5,*),isequence,numvec
      integer ipkprb,iniprb,npoint,icurvs(10),i,npcurv,isigmaprime(5)
      integer NPMAX,NUNK1,NUNK2,NCOOR,NDIM,NUNK3
      parameter(NPMAX=10 000,NUNK1=1,NUNK2=3,NUNK3=6)
      parameter(NCOOR=101*61,NDIM=2)
      real*8 funcx(NPMAX),funcy(NPMAX),temp(NUNK1,NCOOR)
      real*8 x,y,coor(NDIM,NCOOR),uvp(NUNK2,NCOOR)
      real*8 sigma_prime(NUNK3,NCOOR),sigma_n(101),sigma_t(101),p_l(101)
      integer iinder(10),iinmap(10),map,ix,iy,ip
      real*8 slab_velocity
      include 'SPcommon/comcons1'
      include 'SPcommon/cuscons'

      slab_velocity=rlcons(1)

      if (isequence.eq.1) then

c        *** extract heat flow along surface from dT/dx 
         write(6,*) 'NUMVEC = ',NUMVEC
         funcx(1)=NPMAX
         funcy(1)=NPMAX
c        *** compute (grad T, n) where n is the normal to curve 100
         icurvs(1)=0
         icurvs(2)=100
         call compcr(0,kmesh,kprob,isol(1,5),-1,icurvs,funcx,funcy)
         npcurv = funcx(5)/2

c        *** Output non-dimensional data
         open(9,file='dtdx_100.p') 
         rewind(9)
         write(9,*) 2,npcurv
         do i=1,npcurv
            write(9,*) funcx(5+2*i-1),funcy(5+i)
         enddo
         close(9)

c        *** Output in dimensional form (mW/m^2)
         open(9,file='dtdx_dim_100.p') 
         rewind(9)
         write(9,*) 2,npcurv
         do i=1,npcurv
            write(9,*) funcx(5+2*i-1),funcy(5+i)/0.4
         enddo
         close(9)

         call sigma_out(104,kmesh,kprob,isol,funcx,funcy)
         call sigma_out(105,kmesh,kprob,isol,funcx,funcy)

c        *** Extract temperature along top of slab; output in (z,T) and (p,T)
         icurvs(1)=0
         icurvs(2)=102
         call compcr(0,kmesh,kprob,isol(1,2),0,icurvs,funcx,funcy)
         npcurv = funcx(5)/2
          
c        *** output in (z,T) where T is in C and z is in km.
         open(9,file='slabT_z.p') 
         rewind(9)
         write(9,*) 2,npcurv
         do i=1,npcurv
            write(9,*) funcy(5+i),-funcx(5+2*i)
         enddo
         close(9)

c        *** output in (p,T) where p is in GPa (based on rho=3300, g=9.8)
         open(9,file='slabT_p.p') 
         rewind(9)
         write(9,*) 2,npcurv
         do i=1,npcurv
            write(9,*) funcy(5+i),-funcx(5+2*i)*0.03234
         enddo
         close(9)


c        *** interpolate temperature to regular set of grid points
         iinmap(1)=0
         ip=0
         do iy=1,61
            y = -(iy-1)*10d0
            do ix=1,101
              ip=ip+1
              x = (ix-1)*10d0
              coor(1,ip) = x
              coor(2,ip) = y
            enddo
         enddo
        
         call intcoor ( kmesh, kprob, isol(1,2), temp, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         open(9,file='temp.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (temp(1,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)

c        *** Interpolate velocities and pressure to regular grid
         call intcoor(kmesh,kprob,isol(1,1),uvp,coor,
     v           NUNK2,NCOOR,NDIM,iinmap,map)     
         ip=0
         do iy=1,61
            do ix=1,101
               x = (ix-1)*10
               y = -(iy-1)*10
               if (1000-x.le.-y) then 
                  uvp(1,ip+ix)=-1.541492783
                  uvp(2,ip+ix)=-1.541492783
               endif
            enddo
            ip=ip+101
         enddo

c        *** output velocity in cm/yr
         open(9,file='u.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (uvp(1,ip+ix)/slab_velocity*5,ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='v.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (uvp(2,ip+ix)/slab_velocity*5,ix=1,101)
            ip=ip+101
         enddo
         close(9)
c        *** output in pressure MPa: kappa=0.7272e-6, eta=1e21, h=1e3.
         open(9,file='p.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (uvp(3,ip+ix)*727.2,ix=1,101)
            ip=ip+101
         enddo
         close(9)

c        *** Interpolate components of deviatoric stress tensor (in MPa)
c        *** 6 components are stored as txx, tyy, tzz=0, txy, tyz=0, tzx=0.
         call intcoor ( kmesh, kprob, isol(1,8), sigma_prime, coor, 
     v           NUNK3, NCOOR, NDIM, iinmap, map)
         open(9,file='sigma_prime_xx.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(1,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='sigma_prime_yy.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(2,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='sigma_prime_zz.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(3,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='sigma_prime_xy.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(4,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='sigma_prime_yz.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(5,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)
         open(9,file='sigma_prime_xz.p')
         write(9,10) 3,101,61,1.,0.,10.,1000.,0.,-10.,-600.
         ip=0
         do iy=1,61
            write(9,*) (727.2*sigma_prime(6,ip+ix),ix=1,101)
            ip=ip+101
         enddo
         close(9)

c        *** figure out how the boundary conditions behave
c        *** first left boundary: 
c        ***     sigma_n = sigma_prime_xx - P
c        ***     sigma_t = sigma_prime_xy
         do iy=1,101
            sigma_n(iy)=-999
            sigma_t(iy)=-999
         enddo
         write(6,*) 'Left boundary: '
         ip=5*101
         do iy=6,61
            sigma_n(iy) = 727.2*(sigma_prime(1,ip+1) - uvp(3,ip+1))
            sigma_t(iy) = 727.2*sigma_prime(4,ip+1)
            p_l(iy) = 727.2*uvp(3,ip+1)
            write(6,'(i4,4f15.7)') iy,sigma_n(iy),
     v               sigma_t(iy),
     v               p_l(iy),sigma_prime(1,ip+1)
            ip=ip+101
         enddo
c        *** then bottom boundary: 
c        ***     sigma_n = sigma_prime_yy - P
c        ***     sigma_t = sigma_prime_xy
         ip=60*101
         write(6,*) 'Bottom boundary: '
         do ix=1,41
            sigma_n(ix) = sigma_prime(2,ip+ix) - uvp(3,ip+ix) 
            sigma_t(ix) = sigma_prime(4,ip+ix)
            p_l(ix)=uvp(3,ip+ix)
            write(6,'(i4,4f15.7)') ix,727.2*sigma_n(ix),
     v           727.7*sigma_t(ix),
     v           -727.2*p_l(ix),727.2*sigma_prime(2,ip+ix)
         enddo

      endif

10    format(3i4,7f6.0)
      return
      end

