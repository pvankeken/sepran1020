c ****************************************************
c *     BENCH1B
c *
c *     Sepran application to calculate thermal structure of
c *     a subduction zone. For benchmark calculation 1b.
c *   
c *     PvK 031903
c *************************************************************
        program cornerflow
        implicit none
        integer(kind=8) :: NBUFDEF
        parameter(NBUFDEF=45000000_8)
        integer,allocatable,dimension(:) :: ibuffr
        integer :: error
    
        allocate(ibuffr(NBUFDEF),stat=error)
        if (error /= 0) then
           write(6,*) 'error in allocating ibuffr'
           write(6,*) 'error = ',error
           stop
        endif
        call sepcombf(ibuffr,ibuffr,NBUFDEF)
  
        end

c *************************************************************
c *   FUNC
c *
c *   ICHOIS=10,11: Specify horizontal (or vertical) velocity in slab,
c *   parallel to top of slab.
c *
c *   PvK 110100
c *************************************************************
      real*8 function func(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'
      real*8 slab_velocity,batchelor,xt,yt


      slab_velocity=rlcons(1)
        
c     *** Calculate velocity in the slab. Assume velocity is parallel
c     *** to that portion of the top of the slab that is closest to the
c     *** current point.
      if (ichois.eq.10) then
         func = -slab_velocity/sqrt(2d0)
      else if (ichois.eq.11) then
         func = -slab_velocity/sqrt(2d0)
      else if (ichois.eq.12.or.ichois.eq.13) then

         if (x.gt.y+660) then
            if (ichois.eq.12) func = -slab_velocity/sqrt(2d0)
            if (ichois.eq.13) func = -slab_velocity/sqrt(2d0)
            return
         else if (y.ge.-50) then
            func=0
         else 
           xt = 610-x
           yt = -50-y
           func = slab_velocity*batchelor(ichois-11,xt,yt)
         endif
      endif
c     write(6,'(''velocity: '',i3,3f8.3)') ichois,x,y,func

      return
      end

      real*8 function funccf(ichois,x,y,z)
      integer ichois
      real*8 x,y,z,func

      funccf=0
      if (ichois.ge.10.and.ichois.le.13) then
         funccf=func(ichois,x,y,z)
      endif
      return
      end
        
      
c *************************************************************
c *     FUNCBC
c *
c *     Specify boundary values through a function. Used in
c *     ESSENTIAL BOUNDARY CONDITIONS, with FUNC=ICHOIS
c *
c *
c *     Parameters:
c *     ICHOIS i  toggle between different options 
c *     X,Y    i  coordinates in km. Y is negative for depth below
c *            i     sea level
c *     Z      d  unused (2D model)
c *
c *     Output:
c *     ichois=1:  temperature (in C) of ingoing oceanic lithosphere
c *           =2:  temperature (in C) of lithosphere of overriding plate. 
c *     ichois=10,11: velocity components in slab
c *
c *     PvK 102000
c *************************************************************
        real*8 function funcbc(ichois,x,y,z)
        implicit none
        real*8 x,y,z,crustal_geotherm,func,T_in_from_Simon
        real*8 vgrad_length
        real*8 T_for_young_lithosphere
        integer ichois,young_or_old
        include 'SPcommon/comcons1'
        include 'SPcommon/comcons2'
        include 'SPcommon/cuscons'

        vgrad_length=rlcons(10)
        young_or_old=incons(13)

   
        if (ichois.eq.1) then
c          *** inflow. y is depth in km. 

             funcbc = T_for_young_lithosphere(y)

        else if (ichois.eq.2) then
c          *** arc side. Analytical solution for crustal geotherm.
           if (y.ge.-50) then
              funcbc = crustal_geotherm(-y)
           else
c             *** FUNCBC should not be called with this option, for this depth.
              write(6,*) 'error: ',ichois,x,y,z
           endif
        else if (ichois.ge.10) then
c          *** velocity in top of slab
           funcbc = func(ichois,x,y,z)
           if (ichois.ge.10.and.ichois.le.11) then
c          *** Smear out velocity boundary condition in tip of wedge
           if (x.le.610.and.x.ge.(610-vgrad_length)) then
               funcbc = funcbc*(610-x)/vgrad_length
               write(6,*) 'funcbc corrected: ',ichois,610-x,funcbc
           endif
           endif

        endif
   
        return
        end

c *************************************************************
c *   CRUSTAL_GEOTHERM
c * 
c *   PvK 102200
c *************************************************************
      real*8 function crustal_geotherm(z)
      implicit none
      real*8 z
      real*8 H1,H2,kc,km,T2,T1,z1,z2,q1,q2,q3
      real*8 z3
      parameter(H1=0.5253,H2=0.110,kc=1,km=1.24)
      parameter(T1=331.5,T2=592.35,z1=15,z2=30)
      parameter(q3=16.6,q2=18.2,q1=26)

      if (z.lt.50) then
         crustal_geotherm = z*1300d0/50d0
      endif

      return
      end

      real*8 function T_for_young_lithosphere(y)
      implicit none
c
      real*8 y,tmantle,factor,rerf,y_red
      real*8 age,akappa,denom
c
c Don't forget to do the scaling here to provide the correct
c and shifted exponent for the errorfunction profile!
c
c Boundary condition:
c
      tmantle=1300.
      age=50.d6
      age=365.*24.*3600.*age
      akappa=0.72727272e-6
c
      denom=2.*dsqrt(akappa*age)/1000.
c
c       Depth is counted negative => minus sign. 
c
      y_red = -y/denom
c
      T_for_young_lithosphere=tmantle*rerf(y_red)
c     write(6,'(6hBORIS ,4f12.5)')y,y_red,
c    ,    T_for_young_lithosphere,rerf(y_red)
c
      return
      end

      
c *************************************************************
c *   USERBOOL
c *   Function used to test whether main steady state iteration
c *   has converged. Also provides output on # of iterations,
c *************************************************************
      logical function userbool(k)
      implicit none
      integer k

      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'

      real*8 difference,eps_loop,dif1,dif2,gnus,vrms,eps_subloop
      integer iteration,maxiter,iter_sub,max_subiter,itypv
      save iteration,iter_sub
      data iteration,iter_sub/0,0/

      itypv=nint(scalars(12))

      if (k.eq.1) then
c        *** Main loop
         iter_sub=0
         eps_loop=rlcons(6)
         maxiter=incons(9)
         dif1=1
         dif2=1
         if (scalars(3).gt.0) dif1 = scalars(1)/scalars(3)
         if (scalars(4).gt.0) dif2 = scalars(2)/scalars(4)

         write(6,*)
         write(6,*) 'ITERATION: ',iteration,max(dif1,dif2),scalars(12) 
         write(6,*)
         iteration=iteration+1

         if (max(dif1,dif2).gt.eps_loop) then
            userbool=.true.
         else
            userbool=.false.
         endif

         if (itypv.eq.0) then
            userbool = .false.
         endif

         if (iteration.gt.maxiter) then
            userbool=.false.
         endif

      else if (k.eq.2) then

c        *** sub loop for velocity
         eps_subloop=rlcons(9)
         max_subiter = incons(12)
         dif1=1
         if (scalars(3).gt.0) dif1 = scalars(1)/scalars(3)
         iter_sub=iter_sub+1
         write(6,*)  'SUB_ITERATION : ',iter_sub,dif1

         if (dif1.gt.eps_subloop) then
            userbool=.true.
         else
            userbool=.false.
         endif
         if (iter_sub.gt.max_subiter) then
            userbool=.false.
         endif
      endif

      return
      end

c *************************************************************
c *   USEROUT
c *
c *   Code to output surface heat flow and geotherm along
c *   slab interface
c *   Dimensional heat flow by q=q'/r, with r=1d0/(k*dT/dz)=
c *    1d0/(3*1/1e3)=333.333; or q=q'*3 to output in mW/m^2.
c *************************************************************
      subroutine userout(kmesh,kprob,isol,isequence,numvec)
      implicit none
      integer kmesh(*),kprob(*),isol(5,*),isequence,numvec
      integer ipkprb,iniprb,npoint,icurvs(10),i,npcurv,isigmaprime(5)
      integer NPMAX,NUNK1,NUNK2,NCOOR,NDIM,NUNK3,NPX,NPY,DPX
      parameter(NPMAX=10 000,NUNK1=1,NUNK2=3,NUNK3=6)
      parameter(NPX=111,NPY=101,NCOOR=NPX*NPY,NDIM=2,DPX=6)
      real*8 funcx(NPMAX),funcy(NPMAX),temp(NUNK1,NCOOR)
      real*8 x,y,coor(NDIM,NCOOR),uvp(NUNK2,NCOOR),dtdx(NDIM,NCOOR)
      real*8 sigma_prime(NUNK3,NCOOR),sigma_n(NPX),sigma_t(NPX),p_l(NPX)
      real*8 secinv(NUNK1,NCOOR),dpdx(NUNK1,NCOOR),dpdy(NUNK1,NCOOR)
      real*8 pressure(NUNK2,NCOOR),coors3(NDIM,NCOOR)
      integer iinder(10),iinmap(10),map(3),ix,iy,ip,mapuvp(3,NPY)
      integer ip1,length,ncoors3 
      real*8 uvps3(NUNK2,NCOOR),secinvs3(NUNK1,NCOOR)
      real*8 pressures3(NUNK2,NCOOR)
      real*8 dpdxs3(NUNK1,NCOOR),dpdys3(NUNK1,NCOOR)
      real*8 slab_velocity
      logical first_on_this_row
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'

      slab_velocity=rlcons(1)

      if (isequence.eq.1) then

c        *** interpolate temperature to regular set of grid points
c        *** Use two sets:  one for the whole grid; one for just the wedge
         iinmap(1)=0
         iinmap(2)=0
         ip=0
         do iy=1,NPY
            y = -(iy-1)*DPX
            do ix=1,NPX
              ip=ip+1
              x = (ix-1)*DPX
              coor(1,ip) = x
              coor(2,ip) = y
            enddo
         enddo
         open(9,file='coor.dat')
         ip=0
         do ip=1,NPY*NPX
            write(9,'(i5,2f15.7)') ip,coor(1,ip),coor(2,ip)
         enddo
         close(9)

c        ***
         ip=0
         do iy=1,NPY
            y = -(iy-1)*DPX
            first_on_this_row=.true.
            do ix=1,NPX
               x = (ix-1)*DPX
               if ((660-x.ge.-y).and.(y.lt.-50)) then 
c                 *** we're in the wedge
                  ip=ip+1
                  if (first_on_this_row) then
c                     *** record start of this array in the full grid
                      mapuvp(1,iy)=ix
c                     *** and that in the wedge only grid
                      mapuvp(2,iy)=ip
                      first_on_this_row = .false.
                  endif
                  coors3(1,ip)=x
                  coors3(2,ip)=y
c                 *** make sure last point gets recorded too
                  mapuvp(3,iy)=ip
               endif
            enddo
         enddo
         ncoors3 = ip

c        do i=1,ncoors3
c           write(6,*) 'wedge: ',coors3(1,i),coors3(2,i)
c        enddo

         do iy=1,NPY
            write(6,'(''cooruvp: '',4i5)') mapuvp(1,iy),mapuvp(2,iy),
     v         mapuvp(3,iy),mapuvp(3,iy)-mapuvp(2,iy)+1
         enddo
               
         
        
         write(6,*) 'Interpolate temperature'
         call intcoor ( kmesh, kprob, isol(1,2), temp, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         open(9,file='T.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (temp(1,ip+ix),ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)


c        *** reuse information in map
         iinmap(2)=0
c        *** output dimensional heatflow along the top of the model
         write(6,*) 'Interpolate heatflow'
         call intcoor ( kmesh, kprob, isol(1,5), dtdx, coor,
     v           NDIM , NCOOR, NDIM, iinmap, map)
         open(9,file='dtdx.dat')
c        *** The factor three comes from the dimensionalization
         do ix=NPX,1,-1
            write(9,*) coor(1,NPX-ix+1),-dtdx(2,ix)*3.
         enddo
         close(9)

c        *** Interpolate velocities and pressure to regular grid
c        *** recompute information in map
         iinmap(2)=0
         write(6,*) 'Interpolate velocities',NUNK2
         call intcoor(kmesh,kprob,isol(1,1),uvps3,coors3,
     v           NUNK2,ncoors3,NDIM,iinmap,map)     

         do i=1,ncoors3
            write(6,'(''wedge-uvp: '',5f15.7)') coors3(1,i),
     v       coors3(2,i),uvps3(1,i),uvps3(2,i),uvps3(3,i)
         enddo
 
         ip=0
         do iy=1,NPY
            do ix=1,NPX
               x = (ix-1)*DPX
               y = -(iy-1)*DPX
               if (660-x.le.-y) then 
                  uvp(1,ip+ix)=-1.541492783
                  uvp(2,ip+ix)=-1.541492783
               endif
            enddo
            if (mapuvp(1,iy).gt.0) then
c              *** copy info from uvps3 to uvp
               length = mapuvp(3,iy)-mapuvp(2,iy)+1
               ip1=0
               do ix=mapuvp(1,iy),length
                  uvp(1,ip+ix) = uvps3(1,mapuvp(2,iy)+ip1)
                  uvp(2,ip+ix) = uvps3(2,mapuvp(2,iy)+ip1)
                  uvp(3,ip+ix) = uvps3(3,mapuvp(2,iy)+ip1)
                  ip1=ip1+1
               enddo
            endif
            ip=ip+NPX
         enddo

c        *** output velocity in cm/yr
         open(9,file='vx.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (uvp(1,ip+ix)/slab_velocity*5,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)
         open(9,file='vy.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (uvp(2,ip+ix)/slab_velocity*5,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)
c        *** output in pressure MPa: kappa=0.7272e-6, eta=1e21, h=1e3.
         open(9,file='p.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (uvp(3,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)
c        *** reuse information in map
         iinmap(2)=0

         write(6,*) 'Interpolate pressure',NUNK1
         call intcoor ( kmesh, kprob, isol(1,7), pressure, coor, 
     v           NUNK2, NCOOR, NDIM, iinmap, map)
         open(9,file='p1.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (pressure(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)

c        *** compute and output dP/dx and dP/dy in MPa/km: 
         write(6,*) 'Interpolate pressure gradients',NUNK1
         call intcoor ( kmesh, kprob, isol(1,8), dpdx, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         call intcoor ( kmesh, kprob, isol(1,9), dpdy, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         open(9,file='dpdx.dat') 
         ip = 0
         do iy=1,NPY
            write(9,100) (dpdx(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)
         open(9,file='dpdy.dat') 
         ip = 0
         do iy=1,NPY
            write(9,100) (dpdy(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)

      endif

10    format(3i4,7f6.0)
100   format(111e15.7)
      return
      end



c *************************************************************
c *   FNV003
c *   Function used to specify viscosity in Gaussian points
c *
c *   Called when MODELV=103 is specified for Stokes elements.
c * 
c *   Parameters
c *       X,Y,Z  i  coordinates of local point
c *       U,V,W  i  velocity components in local point
c *       SECINV i  square of the second invariant of strain rate
c *                 tensor (II in SP Chapter 7)
c *       NUMOLD i  Number of old vectors stored in UOLD
c *       MAXUNK i  Maximum number of unknowns in UOLD
c *       UOLD   i  Array containing old solution values in this point
c *
c *   Output depends on ITYPV (SCALAR 12)
c *
c *   ITYPV=2
c *       Viscosity is temperature dependent. temperature is stored
c *       in UOLD(1).
c *       This implementation: Ol diffusion creep 
c *   ITYPV=4
c *       Viscosity is temperature and strain rate dependent.
c *       Temperature is stored in UOLD(1), strainrate is sqrt(secinv)
c *       Creep law follows Karato & Wu, 1993.
c *
c *       .          -n                      n-1
c *       e   = Ak mu   exp[-(E+pV)/RT] sigma     sigma
c *        ij                                          ij
c * 
c *       or
c *               -1/n                    . (1-n)/n
c *       eta = Ak     mu exp[(E+pV)/nRT] e
c *
c *       where Ak=3.5e22 s-1, mu=80 GPa, E=540e3 J/mol, n=3.5
c *
c *       for V=0 this translates to
c *                                       . -0.7143
c *       eta = 28968.6 exp [ 18557.3/T ] e
c *
c *       strain rate scales as 1/s or kappa/h^2 = 6e-13 
c *            (using k=2.5, rho=3300, cp=1250, h=1000)
c *       so dimensional edot = model edot * 6e-13
c *
c *   PvK 111400
c *************************************************************
      real*8 function fnv003(x,y,z,u,v,w,secinv,numold,maxunk,uold)
      implicit none
      real*8 x,y,z,u,v,w,secinv
      integer numold,maxunk
      real*8 uold(numold, maxunk),temp
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'
      integer ifirst
      real*8 temp_slab,prefac
      integer itypv
      real*8 n,E,R,A0,T_a,strain_power,p,Vact,secsqrt
      save ifirst
      data ifirst/0/

      temp_slab = rlcons(5)
      itypv=nint(scalars(12))

      if (temp_slab.le.0) then
         write(6,*) 'PERROR(fnv003): temp_slab <= 0'
         write(6,*) 'set temp_slab (rlcon(5))'
         write(6,*) 'before using this function'
         call instop
      endif
      if (itypv.eq.0) then
         fnv003 = 1d0
         return
      endif
      if (itypv.ne.2.and.itypv.ne.4) then
         write(6,*) 'PERROR(fnv003): itypv has wrong value'
         write(6,*) 'itypv = ',itypv
         write(6,*) 'This should be 0,2 or 4'
         write(6,*) 'Check scalars(12)'
         call instop
      endif


      fnv003=1d0
 
      if (itypv.eq.2) then
c       *** Temperature dependent viscosity
        if (ifirst.eq.0) then
           write(6,'(''ETA(T)'')') 
           ifirst=1
        endif
        temp = uold(2,1)
c
c       *** Mind: R = 8.3154 kJ/(mol K)
c
c       **** Old rheology E_activation = 81 kJ/mol; T_ref=1450 C
c
c        fnv003 = exp(-6.907755279*temp/temp_slab)
c
c       *** New rheology E_activation= 335 kJ/mol; T_ref=1200 C
c       *** (Olivine, diffusion creep)
c       **  prefac = exp(-335d3/(8.3154d0*(1200d0+273d0)))
c
          prefac=1.3204345d-12
c
c       **  eta = prefac*exp(335d3/(8.3154d0*(temp+273d0)))
c
         fnv003 = prefac*exp(40.286697d3/(temp+273d0))
c
c       ** Cutoff for viscosity (upper bound):
c
         fnv003 = min(fnv003,1d4)
c

      else if (itypv.eq.4) then

c       *** non-Newtonian, dislocation creep
        if (ifirst.eq.0) then
           write(6,'(''ETA(T,edot)'')') 
           ifirst=1
        endif

        temp = uold(2,1)

        n=3.5
        E=540e3
        R=8.3145
        A0 = 28968.6
        Vact=0
        secsqrt = sqrt(secinv)
        strain_power = (secsqrt*6e-13)**((1d0-n)*1d0/n)
        T_a = temp+273
        p=0
        fnv003 = A0*exp((E+p*Vact)/(n*R*T_a))*strain_power
        fnv003 = fnv003/1e21
        fnv003 = min(fnv003,1d4)
 
c       if (x.gt.490) then
c          write(6,'(''eta: '',6e12.3)') 
c    v         x,y,secsqrt,strain_power,T_a,fnv003
c       endif

      endif

      if (fnv003.le.0) then
         write(6,*) 'PERROR(fnv003): viscosity <= 0'
         write(6,*) 'x,y,z: ',x,y,z
         write(6,*) 'temp : ',temp
         write(6,*) 'secinv: ',secinv
         write(6,*) 'itypv:  ',itypv
         write(6,*) 'fnv003: ',fnv003
      endif

      end

c *************************************************************
c *   FUNCVECT
c *   User defined function used in creation of vectors
c *   Called when OLD_VECT=ichoice is specified in CREATE block
c *
c *   Parameters
c *      ICHOICE   i    choice parameter (see below)
c *      NDIM      i    dimension of space
c *      COOR      i    Array containing coordinates of nodal points
c *      NUMNODES  i    Total number of nodal points
c *      UOLD      i    Array containing 'old' vectors
c *      NUOLD     i    Number of old vectors
c *      RESULT    o    Array into which the resulting function is to be stored
c *      NPHYS     i    Maximum number of degrees of freedom in old vectors
c *  
c *   ICHOICE=1:
c *      Compute viscosity from strain rate and temperature
c *      through FNV003; store log(viscosity) and log(secsqrt)
c *      Specify in the CREATE block:
c *        old_vector=1,seq_vector=V$temp,V$secsqrt,surfaces(s6)
c *
c *   PvK 111400
c *************************************************************
      subroutine funcvect(ichoice,ndim,coor,numnodes,uold,
     v                    nuold,result,nphys)
      implicit none 
      integer ichoice,ndim,numnodes,nuold,nphys
      real*8 coor(ndim,numnodes),uold(numnodes,nphys,nuold)
      real*8 result(numnodes,*)
      integer i
      real*8 temp(2),secsqrt,secinv,x,y,z,u,v,w,fnv003

      if (ichoice.eq.1) then

c         *** compute viscosity
          do i=1,numnodes
            temp(2) = uold(i,1,1)
            secsqrt = uold(i,1,2)
            secinv = secsqrt*secsqrt
            x = coor(1,i)
            y = coor(2,i)
            z = 0.
            u = 0.
            v = 0.
            w = 0.
            result(i,1) = log10(fnv003(x,y,z,u,v,w,secinv,1,1,temp)) 
            result(i,2) = log10(secsqrt)
          enddo

       else if (ichoice.eq.2) then
c         *** extract pressure
          do i=1,numnodes
             result(i,1) = uold(i,3,1)
          enddo
          write(6,*) 'numnodes: ',numnodes,nuold,nphys
          do i=1,numnodes,100
             write(6,'(''uvp: '',5e15.7)') coor(1,i),coor(2,i),
     v          uold(i,1,1),uold(i,2,1),uold(i,3,1)
          enddo

       endif

       return
       end

c *************************************************************
c *   RERF
c *   Calculate the error function. From numerical recipes
c *************************************************************
      real*8 function rerf(x)
      implicit none
      real*8 x
      integer iret
      real*8 delx,x0,x1,xx

      if (x.gt.4.) then
            rerf=1.
            goto 20
      endif
         iret=0
         rerf=0.
         delx=0.001
         x0=0.
10       x1=x0+delx
         if(x1.gt.x) then
            x1=x
            iret=1
         endif
         xx=(x0+x1)/2.
         rerf=rerf+dexp(-xx*xx)*(x1-x0)
         if(iret.eq.1) then
            rerf=rerf*2./dsqrt(3.1415926535d0)
            goto 20
         endif
         x0=x1
         goto 10
c
20     return
         end

c *************************************************************
c *   BATCHELOR
c *
c *   Obtain velocity in wedge through Batchelor's cornerflow
c *   solution.
c *
c *   BK 111900
c *************************************************************
      real*8 function batchelor(ichois,x,y)
      implicit none
      integer ichois
      real*8 x,y

      real*8 vx,vy,stream,edot12,theta0,pi
      real*8 speed,v0,x0,y0
      real*8 xbase,ybase,deltax,deltay,triangle(4,7)
      real*8 xmax,ymax
      real*8 vmin,vmax,stmax,stmin,e12max,e12min

      integer ii,nx,ny,ix,iy,ip
      integer nc(3)
      common /angle/ theta0,v0
c     common /corner/ x0,y0

      pi=4.d0*atan(1.d0)
c     *** Angle is 45 degrees
      theta0 = pi/4d0
      v0=1

      call batchlor(x,y,vx,vy,speed,stream,edot12)
      if (ichois.eq.1) batchelor = vx
      if (ichois.eq.2) batchelor = vy
      end

      subroutine batchlor(x,y,vx,vy,speed,stream,edot12)
      implicit none
      real*8 x,y,vx,vy,speed,stream,edot12
      real*8 rr,theta0,v0,thetaprime,theta
      real*8 pi,vr,vtheta,xprime,yprime
      real*8 ftheta,dfdtheta,factor,x0,y0
      real*8 eps
c
      data eps /1d-5/
c
      common /angle/ theta0,v0
c     common /corner/ x0,y0
c
c Define the coordinate system
c
c                ----------------> +y
c                |\
c                | \                The angle (y,r) is thetaprime
c                |  \
c                |   \ r
c                |
c                v +x
c
c Make sure the boundary conditions are set the right way on both
c sides of the wedge:
c
      x0=0
      y0=0
      xprime=x-x0
      yprime=y-y0
      rr=dsqrt(xprime**2.d0+yprime**2.d0)
c
c Calculate theta: There may be a problem for small radial diistances.
c
      thetaprime=acos(xprime/rr)
      if (thetaprime.lt.0.d0) then
         write(*,*)'Above wedge!'
         vx=0.d0
         vy=0.d0
         speed=0.d0
         stream=0.d0
         edot12=0.d0
      else if (thetaprime.gt.theta0+eps) then
         write(*,*)'In slab!'
         vx=v0*sin(theta0)
         vy=v0*cos(theta0)
         speed=dsqrt(vx**2.d0+vy**2.d0)
         stream=0.d0
         edot12=0.d0
      else
c
c Here is Batchelor's solution for the problem:
c
       theta=theta0-thetaprime

       factor=v0/(theta0**2.d0-sin(theta0)**2.d0)

       ftheta=-theta0**2.d0*sin(theta)+
     +         (theta0-sin(theta0)*cos(theta0))*theta*sin(theta)+
     +         sin(theta0)**2.d0*theta*cos(theta)
       ftheta=factor*ftheta

       dfdtheta=-theta0**2.d0*cos(theta)+
     +  (theta0-sin(theta0)*cos(theta0))*
     *    (sin(theta)+theta*cos(theta))+
     +  sin(theta0)**2.d0*(cos(theta)-theta*sin(theta))
       dfdtheta=factor*dfdtheta
      vr=dfdtheta
       vtheta=-ftheta
c       write(*,'(''theta,vr,vtheta: '',3f12.5)')
c    v    theta,vr,vtheta
c
c Perform the transformation from polar coordinates back to
c cartesian coordinates.
c
       vx= vr*cos(theta0-theta)+vtheta*sin(theta0-theta)
       vy= vr*sin(theta0-theta)-vtheta*cos(theta0-theta)
c
c Speed at nodal point:
c
       speed=dsqrt(vx**2.d0+vy**2.d0)
c
c Stream function:
c
       stream=rr*ftheta
c
c Divergent component of strain rate: e_dot(1,2):
c
       edot12=theta0**2.d0*sin(theta)+
     +  (theta0-sin(theta0)*cos(theta0))*
     *    (2.d0*cos(theta)-theta*sin(theta))-
     -  sin(theta0)**2.d0*(2d0*sin(theta)+theta*cos(theta))
       edot12=factor*edot12/rr
c
      endif
c
      return
      end

c *************************************************************
c *   SIGMA_OUT
c *
c *   Output velocity, pressure (isol1), temperature (isol2)
c *   and stress components (isol8) for plotting with GMT
c *************************************************************
      subroutine sigma_out(ipcurve,kmesh,kprob,isol,funcx,funcy)
      implicit none
      integer kmesh(*),kprob(*),isol(5,*),ipcurve
      real*8 funcx(*),funcy(*)
      integer NPMAX
      parameter(NPMAX=1000)
      real*8 x(NPMAX),y(NPMAX),u(NPMAX),v(NPMAX),p(NPMAX)
      real*8 sxx(NPMAX),syy(NPMAX),sxy(NPMAX)
      real*8 dudx(NPMAX),dudy(NPMAX),dvdx(NPMAX),dvdy(NPMAX)
      integer icurvs(3),npcurv,i,j,ichoice,number
      real*8 pi,rl,angle,z,slab_velocity
      parameter(pi=3.1415926)
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'

      slab_velocity=rlcons(1)

      icurvs(1)=0
      icurvs(2)=ipcurve
c     *** first get coordinates
      ichoice=-1
      number=1
      call compcr(ichoice,kmesh,kprob,isol(1,1),number,
     v           icurvs,funcx,funcy)
      npcurv = funcx(5)/2
      write(6,*) 'sigma_out #coordinates: ',npcurv
      if (npcurv.gt.NPMAX) stop 'too much for me (sigma_out)'
      do i=1,npcurv
         x(i) = funcx(5+2*i-1)
         y(i) = funcx(5+2*i)
      enddo

c     *** Get horizontal velocity
      ichoice=0
      number=1
      call compcr(ichoice,kmesh,kprob,isol(1,1),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #u: ',npcurv
      do i=1,npcurv
         u(i) = funcy(5+i)
      enddo

c     *** Get vertical velocity
      ichoice=0
      number=2
      call compcr(ichoice,kmesh,kprob,isol(1,1),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #v: ',npcurv
      do i=1,npcurv
         v(i) = funcy(5+i)
      enddo

c     *** Get pressure
      ichoice=0
      number=3
      call compcr(ichoice,kmesh,kprob,isol(1,1),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #p: ',npcurv
      do i=1,npcurv
         p(i) = funcy(5+i)
      enddo

c     *** Get sigma_xx
      ichoice=0
      number=1
      call compcr(ichoice,kmesh,kprob,isol(1,8),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #sxx: ',npcurv
      do i=1,npcurv
         sxx(i) = funcy(5+i)
      enddo

c     *** Get sigma_yy
      ichoice=0
      number=2
      call compcr(ichoice,kmesh,kprob,isol(1,8),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #syy: ',npcurv
      do i=1,npcurv
         syy(i) = funcy(5+i)
      enddo

c     *** Get sigma_xy
      ichoice=0
      number=4
      call compcr(ichoice,kmesh,kprob,isol(1,8),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #sxy: ',npcurv
      do i=1,npcurv
         sxy(i) = funcy(5+i)
      enddo

c     *** Get dudx
      ichoice=0
      number=1
      call compcr(ichoice,kmesh,kprob,isol(1,9),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #dudx: ',npcurv
      do i=1,npcurv
         dudx(i) = funcy(5+i)
      enddo
c     *** Get dvdx
      ichoice=0
      number=2
      call compcr(ichoice,kmesh,kprob,isol(1,9),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #dvdx: ',npcurv
      do i=1,npcurv
         dvdx(i) = funcy(5+i)
      enddo
c     *** Get dudy
      ichoice=0
      number=3
      call compcr(ichoice,kmesh,kprob,isol(1,9),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #dudy: ',npcurv
      do i=1,npcurv
         dudy(i) = funcy(5+i)
      enddo
c     *** Get dvdy
      ichoice=0
      number=4
      call compcr(ichoice,kmesh,kprob,isol(1,9),number,
     v           icurvs,funcx,funcy)
      npcurv=funcy(5)
      write(6,*) 'sigma_out #dvdy: ',npcurv
      do i=1,npcurv
         dvdy(i) = funcy(5+i)
      enddo
    
      if (ipcurve.eq.104) then
         open(9,file='bottom_vel.dat')
         open(10,file='bottom_p.dat')
         open(11,file='bottom_sxx.dat')
         open(12,file='bottom_syy.dat')
         open(13,file='bottom_sxy.dat')
         open(14,file='bottom_dudx.dat')
         open(15,file='bottom_dvdx.dat')
         open(16,file='bottom_dudy.dat')
         open(17,file='bottom_dvdy.dat')
      else if (ipcurve.eq.105) then
         open(9,file='side_vel.dat')
         open(10,file='side_p.dat')
         open(11,file='side_sxx.dat')
         open(12,file='side_syy.dat')
         open(13,file='side_sxy.dat')
         open(14,file='side_dudx.dat')
         open(15,file='side_dvdx.dat')
         open(16,file='side_dudy.dat')
         open(17,file='side_dvdy.dat')
      endif
 
      do i=1,npcurv
         z = y(i)+600
c        *** convert to cm/yr
         u(i) = u(i)*5d0/slab_velocity
         v(i) = v(i)*5d0/slab_velocity
c        *** convert to MPa
         p(i) = p(i)*727.2
         sxx(i) = sxx(i)*727.2
         syy(i) = syy(i)*727.2
         sxy(i) = sxy(i)*727.2
c        *** convert to 1e-16 s^-1 
         dudx(i) = dudx(i)*1e4  
         dvdx(i) = dvdx(i)*1e4  
         dudy(i) = dudy(i)*1e4  
         dvdy(i) = dvdy(i)*1e4  
         if (u(i).eq.0.and.v(i).ge.0) then
            angle = 90
         else if (u(i).eq.0.and.v(i).lt.0) then
            angle = -90
         else if (u(i).gt.0) then
            angle=atan(v(i)/u(i))*180./pi
         else if (u(i).lt.0) then
            angle=180+atan(v(i)/u(i))*180./pi
         endif
         rl = sqrt(u(i)*u(i)+v(i)*v(i))
         write(9,*) x(i),z,angle,rl
         if (ipcurve.eq.104) then
c           *** bottom boundary
            write(10,*) x(i),p(i)
            write(11,*) x(i),sxx(i)
            write(12,*) x(i),syy(i)
            write(13,*) x(i),sxy(i)
            write(14,*) x(i),dudx(i)
            write(15,*) x(i),dvdx(i)
            write(16,*) x(i),dudy(i)
            write(17,*) x(i),dvdy(i)
         else
            write(10,*) p(i),z
            write(11,*) sxx(i),z
            write(12,*) syy(i),z
            write(13,*) sxy(i),z
            write(14,*) dudx(i),z
            write(15,*) dvdx(i),z
            write(16,*) dudy(i),z
            write(17,*) dvdy(i),z
         endif
      enddo
      close(9)
      close(10)
      close(11)
      close(12)
      close(13)

      return
      end
