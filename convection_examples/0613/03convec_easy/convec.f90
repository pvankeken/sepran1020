program convec
implicit none

integer(kind=8),parameter :: NBUFDEF=500000000_8
integer(kind=8),allocatable :: ibuffr(:)
integer :: error

allocate(ibuffr(NBUFDEF),stat=error)
if (error /= 0) then
  write(6,*) 'PERROR(poisson1D): unable to allocate space'
  write(6,*) 'for ibuffr: NBUFDEF = ',NBUFDEF
  stop
endif

call sepcombf(ibuffr,ibuffr,NBUFDEF)

if (allocated(ibuffr)) deallocate(ibuffr)
  
end program convec

real(kind=8) function func(ichoice,x,y,z)
implicit none
real(kind=8) :: x,y,z
real(kind=8),parameter :: pi=3.1415926_8
integer :: ichoice

if (ichoice==1) then
   func = 1-y + 0.1*cos(pi*x)*sin(pi*y)
else if (ichoice==2) then
   func = cos(pi*x)*sin(pi*y)
else
   func = -sin(pi*x)*cos(pi*y)
endif
  
end function func

subroutine funalg(val1,val2,val3)
implicit none
real(kind=8) val1,val2,val3
val3 = val1*val1 + val2*val2
 
end subroutine funalg
       
       
