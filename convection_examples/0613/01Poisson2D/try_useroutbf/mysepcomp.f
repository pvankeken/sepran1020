      program mysepcomp
      implicit none
      integer(kind=8) :: NBUFDEF
      parameter(NBUFDEF=45000000_8)
      integer,allocatable,dimension(:) :: ibuffr
      integer :: error

      allocate(ibuffr(NBUFDEF),stat=error)
      if (error /= 0) then
           write(6,*) 'error in allocating ibuffr'
           write(6,*) 'error = ',error
           stop
      endif
      call sepcombf(ibuffr,ibuffr,NBUFDEF)

      end

      subroutine useroutbf(ibuffr,buffer,kmesh,kprob,isol,isequence,
     v        numvec,intmat)
      implicit none
      integer :: ibuffr(*)
      real(kind=8) :: buffer(*)
      integer :: kmesh(*),kprob(*),isol(*),isequence,numvec,intmat(5)
      integer(kind=8) :: ikelmm,iptemp,inigettext,inidgttext
      integer :: ncurvs,icurve
      
      call ini060text(ibuffr,kmesh(27),'userout:kmeshm')
      ikelmm = inigettext(kmesh(27),'userout:kmeshm')
      write(6,*) 'ikelmm: ',ikelmm
      write(6,*) 'ninner = ',ibuffr(ikelmm)

      end subroutine useroutbf

      real*8 function funccf(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z
      real*8 pi
      parameter(pi=3.1415926)

      if (ichoice == 1) then  
         funccf = 1d0
      endif

      end

      real*8 function func(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z,funccf

      func = funccf(ichoice,x,y,z)

      end
      

