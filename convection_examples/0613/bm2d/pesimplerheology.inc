c *   *** pesimplerrheology.inc
c *   *** coefficients for viscosity law (for Blankenbach et al., 1989
c *   *** benchmark).
      common /pesimplerheology/ avisc,bvisc,wvisc
      real*8 avisc,bvisc,wvisc
