c ***************************************************
c *   VRMSNEW
c *
c *   Calculates u*u + v*v per nodal point from the
c *   solution vector and places the result in array user.
c *
c *   - Takes into account that solution vectors are not renumbered
c *     anymore
c *   - Suitable for the integrated method 
c *
c *   PvK 991020
c *****************************************************************
      subroutine vrmsnew(isol,kmesh,kprob,iuser,user,ipemap)
      implicit none
      integer isol(*),kmesh(*),kprob(*),iuser(*),ipemap(*)
      real*8 user(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      integer iniget,inidgt,itype,kprobc

      include 'pecof900.inc'
      integer npoint,ipusol,indprf,ipkprf,nunkp,nelem
      integer ikmeshc,ikmeshi
      integer iipemap,irpemap
      logical mapneeded

      kprobc = kprob(16)
      itype = kprob(kprobc)
c     write(6,*) 'itype: ',itype
      if (itype.eq.901) then
         mapneeded=.true.
      else 
         mapneeded=.false.
      endif
      npoint = kmesh(8)
      nelem = kmesh(9)
      indprf = kprob(19)
      nunkp  = kprob(4)
      call ini050(isol(1),'vrmsnew: isol')
      ipusol = inidgt(isol(1))

      if (indprf.ne.0) then
c        *** variable number of dofs: probably integrated 
c        *** method. Activate KPROBF
         call ini050(indprf,'vrmsnew: indprf')
         ipkprf = iniget(indprf)
      endif
      if (indprf.eq.0.and.nunkp.ne.2) then
         write(6,*) 'PERROR(vrmsnew): nunkp should be 2'
         call instop
      endif

      if (mapneeded) then
c        *** first need to find the value of u*u+v*v in the
c        *** barycenter of each element
         call ini050(ipemap(3),'vrmsnew: ipemap(3)')
         call ini050(ipemap(5),'vrmsnew: ipemap(5)')
         iipemap = iniget(ipemap(3))
         irpemap = inidgt(ipemap(5))
         call ini050(kmesh(17),'vrmsnew: kmesh(17)')
         call ini050(kmesh(23),'vrmsnew: kmesh(23)')
         ikmeshc = iniget(kmesh(17))
         ikmeshi = inidgt(kmesh(23))
         call pemapinterpolate(2,ibuffr(ikmeshc),buffr(ikmeshi),
     v          buffr(ipusol),ibuffr(iipemap),buffr(irpemap),
     v          indprf,ibuffr(ipkprf),nunkp,nelem)
      endif
      
   
      call vrmsn01(buffr(ipusol),indprf,ibuffr(ipkprf),
     v             npoint,nunkp,user(6),itype,mapneeded,
     v             ibuffr(iipemap),buffr(irpemap))
    
c     if (isolmethod.le.0) then
c        *** kmesh is based on 6 point triangles
c        mapneeded = .false.
c     else
c        *** kmesh is based on 7 point triangles
c        mapneeded = .true.
c        if (ipemap(2).ne.-99) then
c           write(6,*) 'PERROR(vrmsnew): ipemap is not of type PvK'
c           call instop
c        endif
c        if (ipemap(3).eq.0) then
c           write(6,*) 'PERROR(vrmsnew): ipemap has not been filled' 
c           call instop
c        endif
c        call ini070(ipemap(3))
c        call ini070(ipemap(5))
c     endif
   
c     call ini070(isol(1))
c     ipusol = infor(1,isol(1))
c     call pevrm1(npoint,ibuffr(ipusol),ibuffr(iindprh),
c    v            indprh,user(6))

      iuser(6) = 7
      iuser(7) = intrule900
      iuser(8) = icoor900
      iuser(10) = 2001
      iuser(11) = 6

      return
      end

      subroutine vrmsn01(sol,indprf,kprobf,npoint,nunkp,
     v                   temp,itype,mapneeded,map,rmap)
      implicit none
      real*8 sol(*),temp(*),u,v,rmap(*)
      integer indprf,kprobf(*),npoint,j1,j2,nunkp,i,itype
      integer ndof,map(*),ip
      logical mapneeded

c     if (itype.eq.901.and.(.not.mapneeded)) then
c        write(6,*) 'PERROR(vrmsnew): itype = 901,' 
c    v       yet mapneeded = ',mapneeded
c        write(6,*) 'Map is needed for type 901'
c        call instop
c     endif

c     write(6,*) 'INDPRF: ',indprf
      if (indprf.eq.0) then
         do i=1,npoint
            j1 = (i-1)*nunkp+1
            j2 = j1+1
            u = sol(j1)
            v = sol(j2)
            temp(i) = u*u+v*v
c           write(6,'(''vrms: '',i5,3f9.3)') 
c    v            i,u,v,temp(i)
         enddo
      else 
c        write(6,'(10i5)') (kprobf(i),i=1,npoint)
c        write(6,'(10i5)') 0,(kprobf(i)-kprobf(i-1),i=2,npoint)
         if (itype.eq.902) then
c           *** 7 point triangle with (u,v) in barycenter
            do i=1,npoint
               j1 = kprobf(i)+1
               j2 = j1+1
               u = sol(j1)
               v = sol(j2)
               temp(i) = u*u+v*v
c              write(6,'(''vrms: '',i5,3f9.3)') 
c    v              i,u,v,temp(i)
            enddo
         else if (itype.eq.901) then
c           *** 7 point triangle with only p in barycenter
            do i=1,npoint
               ip = map(i)
               if (ip.lt.0) then
                  u=0
                  v=0
                  temp(i) = rmap(-ip)
               else
c                 *** point on the edge of the element, contains (u,v)
                  j1 = kprobf(i)+1
                  j2 = j1+1
                  u = sol(j1)
                  v = sol(j2)
                  temp(i) = u*u+v*v
               endif
c              write(6,'(''vrms: '',i5,3f9.3)') 
c    v              i,u,v,temp(i)
            enddo
         else 
            write(6,*) 'PERROR(vrmsnew): unsuited for itype <> 901,902'
            write(6,*) 'itype = ',itype
            call instop
         endif
      endif

      return
      end
