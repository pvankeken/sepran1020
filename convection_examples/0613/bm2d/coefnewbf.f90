! *************************************************************
! *   COEFNEW
! *
! *   Specify coefficients for Stokes and heat equation.
! *   Stokes:
! *      Use fil103 to specify coefficients directly 
! *             (not through input)
! *      Use modelv=103 and use fnv003 
! *
! *   PvK 990302
! *
! *   Modification to allow for transport between extended quadratic
! *   mesh for the Stokes equation (integrated method, ISHAPE=7)
! *   and the linear mesh (ISHAPE=3) for the heat equation.
! *   IPEMAP contains the mapping information from quadratic to
! *   linear grid.
! *   1) For specification of Ra*T in Stokes equation, we need
! *      the mapping for the gridpoints at the boundaries of the
! *      elements. For the internal nodal point we need to 
! *      interpolate the temperature in the inner linear triangle
! *   2) For specification of velocity, we can use the map to
! *      restrict the values that need to be copied from the first
! *      to second problem.  No interpolation is necessary.
! *   PvK 080100
! *
! *   PvK 071713   Update for 0613
! *                Fortran90
! *************************************************************
include 'PvKmodules.inc'
subroutine coefnewbf(ibuffr,buffer,ichoice,kmesh1,kmesh2,kprob1,kprob2, &
   isol1,isol2,iuser,user,islol1,ipemap)
use solutionmethod
use pecof800
use pecof900
use peparam
use c1visc
implicit none
integer :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: kmesh1(:),kmesh2(:),kprob1(:),kprob2(:),isol1(:)
integer :: islol1(:,:),ipemap(:)
integer :: isol2(:),iuser(:),ichoice
real(kind=8) :: user(:)


integer :: itime,nparm,iwork(4,100),iincop(6)
real(kind=8) :: work(100),eps,f2

integer :: mconv,modelv,npoint,i,k
logical :: mapneeded

iwork=0
work=0d0
iuser(6:iuser(1))=0
user(6:)=0d0

! **** Need to specify the mapping from 7 to 3 point based grid?
if (isolmethod <= 0) then
!  *** kmesh1 is based on 6 point triangles
  mapneeded = .false.
else
! *** kmesh1 is based on 7 point triangles
  mapneeded = .true.
  if (ipemap(2) /= -99) then
     write(6,*) 'PERROR(coefnew): ipemap is not of type PvK' 
    call instop
  endif
  if (ipemap(3) == 0) then
     write(6,*) 'PERROR(coefnew): ipemap has not been filled' 
     call instop
  endif
endif

if (ichoice==1) then
!  *** Momentum equation type 900 only
   modelv=  103
   mconv =  0
   itime = 0
   iwork(1,1) = itime
   iwork(1,2) = modelv
   iwork(1,3) = intrule900 + 100*interpol900
   iwork(1,4) = icoor900
   iwork(1,5) = mconv

   if (isolmethod.eq.0) then 
!     *** penaltyfunction parameter
      eps    = 1d-6
   else 
      eps = 0
   endif
   f2     = 1d0
   
!  *** 6: epsilon
   iwork(1,6) = 0
   work(6) = eps
!  *** 7: rho
   iwork(1,7) = 0
   work(7)   = 1d0
!  *** 8: omega
!  *** 9: f1
!  *** 10: f2. First dof of second vector in islold.
   iwork(1,10) = 2003
   iwork(2,10) = 2
   iwork(3,10) = 1
!  *** 11: f3
!  *** 12: eta
   iwork(1,12) = 0
   work(12)   = 1
   nparm=12

!  *** copy temperature, multiplied by Ra into islold

   call cofcopybf(ibuffr,buffer,0,islol1(1,2),isol2,kmesh1,kmesh2,kprob1,kprob2,rayleigh,ipemap,mapneeded)
   call fil103bf(ibuffr,buffer,1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1) 
else

   do i=1,25
      user(5+i)=0d0
      iuser(5+i)=0
   enddo

   npoint = kmesh2(8)
!  *** Coefficients for element 800
!  *** First five, integer info
!  *** 1 = not yet used
   iuser(6)  = 7
   iuser(7)  = 0
!  *** 2 = Type of upwinding
   iuser(8)  = metupw
!  *** 3 = Integration rule
   iuser(9)  = intrule
!  *** 4 = Type of coordinate system
   iuser(10)  = icoorsystem
!  *** 5 = not yet used
   iuser(11) = 0

!  *** a11,a12,a13,a22,a23,a33,u1,u2,u3,beta,f,rhocp
!  *** 6-11 = diffusivity tensor
   iuser(12) = -6
   iuser(15) = -6
   user(6) = 1d0
!  *** 12-14: velocity components
   iuser(18) = 2001
   iuser(19) = 10
   iuser(20) = 2001
   iuser(21) = 10+npoint
!  *** 15: beta; 16: q
   if (iqtype == 1) then
      iuser(24) = -7
       user(7) = q
   endif
!  *** 17: rho*cp
   iuser(25) = -6
         
   if (isolmethod <= 0) then
      call pecopybf(ibuffr,buffer,2,isol1,user,kprob1,10,1d0)
      call pecopybf(ibuffr,buffer,3,isol1,user,kprob1,10+npoint,1d0)
   else
      call penewcopybf(ibuffr,buffer,2,isol1,kmesh1,kprob1,kmesh2,kprob2,user,10,ipemap,mapneeded) 
      call penewcopybf(ibuffr,buffer,3,isol1,kmesh1,kprob1,kmesh2,kprob2,user,10+npoint,ipemap,mapneeded) 
   endif
       
endif

end subroutine coefnewbf

! *************************************************************
! *   PENEWCOPYBF
! *
! *   Copy velocity components from extended quadratic mesh 
! *   (ISHAPE=7) to linear mesh (ISHAPE=3) for heat equation.
! *   ipemap contains the mapping of 
! *
! *   PvK 071713 Fortran90
! *************************************************************
subroutine penewcopybf(ibuffr,buffer,ichoice,isol,kmesh,kprob,kmesh2,kprob2,user,istart,ipemap,mapneeded)
implicit none
integer(kind=8) :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: ichoice,isol(:),kmesh(:),kprob(:),istart,ipemap(:)
integer :: kprob2(:),kmesh2(:)
real(kind=8) :: user(*)
logical :: mapneeded
integer :: iipemap,ipusol,npoint,indprf,nunkp,npoint2
integer :: ipkprf
integer(kind=8) :: inidgt,iniget
 
npoint = kmesh(8)
indprf = kprob(19)
nunkp = kprob(4)
npoint2 = kmesh2(8)
      
if (ichoice /= 2 .and. ichoice /= 3) then
   write(6,*) 'PERROR(penewcopy): ichoice should be 2 or 3'
   write(6,*) 'ichoice = ',ichoice
   call instop
endif

if (mapneeded) then
   if (ipemap(2) /= -99) then
      write(6,*) 'PERROR(penewcopy): ipemap is not of type PvK'
      call instop
   endif
   if (ipemap(3) == 0) then
      write(6,*) 'PERROR(penewcopy): ipemap has not been filled'
      call instop
   endif
   call ini060text(ibuffr,ipemap(3),'coefnew: ipemap(3)')
   iipemap = iniget(ipemap(3))
endif

call ini060text(ibuffr,isol(1),'coefnew: isol')
ipusol = inidgt(isol(1))

if (indprf /= 0) then
!  *** variable number of dofs: probably integrated
!  *** method. Activate KPROBF
   call ini060text(ibuffr,indprf,'coefnew: indprf')
   ipkprf = iniget(indprf)
endif
if (indprf == 0 .and. nunkp /= 2) then
   write(6,*) 'PERROR(penewcopy): nunkp should be 2'
   call instop
endif

call penwcp01(ichoice,buffer(ipusol),indprf,ibuffr(ipkprf),npoint,npoint2,nunkp,user(istart),ibuffr(iipemap),mapneeded)

end subroutine penewcopybf

subroutine penwcp01(ichoice,sol,indprf,kprobf,npoint,npoint2,nunkp,temp,map,mapneeded)
implicit none
real(kind=8) :: sol(:),temp(:)
integer :: ichoice,indprf,kprobf(:),npoint,nunkp,map(:),npoint2
logical :: mapneeded
integer :: i,j1,ip

if (.not.mapneeded) then
   if (indprf /= 0) then
      write(6,*) 'PERROR(penwcp01): not suited for this case'
      write(6,*) 'yet: mapneeded=.false. yet indprf<>0'
      call instop
   endif
   if (npoint /= npoint2) then
      write(6,*) 'PERROR(penwcp01): npoint <> npoint2'
      write(6,*) 'Number of points in two meshes should be equal' 
      call instop
   endif
   if (ichoice == 2) then
      ! *** horizontal velocity component
      do i=1,npoint2
         j1 = (i-1)*nunkp + 1
         temp(i) = sol(j1)
      enddo
  else if (ichoice == 3) then
      ! *** vertical velocity component
      do i=1,npoint2
         j1 = (i-1)*nunkp + 2
         temp(i) = sol(j1)
      enddo
  endif
else
  if (indprf > 0) then
     ! **** mapneeded AND variable dofs
     if (ichoice == 2) then
       do i=1,npoint
          ! *** first find corresponding number in second mesh
          ip = map(i)
          if (ip>0) then
!            *** nodal points overlap
             j1 = kprobf(i)+1
             temp(ip) =  sol(j1)
          endif
       enddo
     else
       do i=1,npoint
          ! *** first find corresponding number in second mesh
          ip = map(i)
          if (ip>0) then
             ! *** nodal points overlap; second dof
             j1 = kprobf(i)+2
             temp(ip) =  sol(j1)
          endif
      enddo
     endif
  else
    ! *** mapneeded, constant dofs
    if (ichoice==2) then
       do i=1,npoint
          ip = map(i)
          if (ip>0) then
             ! *** nodal points overlap
             j1 = nunkp*(i-1)+1
             temp(ip) =  sol(j1)
          endif
       enddo
    else if (ichoice == 3) then
       do i=1,npoint
          ip = map(i)
          if (ip>0) then
             ! *** nodal points overlap
             j1 = nunkp*(i-1)+2
             temp(ip) =  sol(j1)
          endif
       enddo
    endif
  endif
endif

end subroutine penwcp01
  
! *************************************************************
! *   cofcopy
! *
! *   Copy first dof of isol2 into first dof of isol1
! *
! *   Note: as of 1995 or so, solution vectors are not renumbered
! *   anymore...
! *************************************************************
subroutine cofcopybf(ibuffr,buffer,ichoice,isol1,isol2,kmesh1,kmesh2,kprob1,kprob2,factor,ipemap,mapneeded)
implicit none
integer(kind=8) :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: ichoice,isol1(:),isol2(:),kmesh1(:), kmesh2(:)
integer :: kprob1(:),kprob2(:),ipemap(:)
real(kind=8) :: factor
logical :: mapneeded
 
integer :: indprf1,indprf2,nunkp,iipemap,irpemap,ikmeshc
integer :: ikmeshi,nelem1
integer :: ipusol1,ipusol2,npoint1,npoint2
integer :: nunkp1,nunkp2 ,ipkprf1,nphys1
integer(kind=8) :: iniget,inidgt

if (kprob1(21) /= 0 .and. kprob2(21) /= 0) then
   write(6,*) 'PERROR(cofcopy): kprobh <> 0!' 
   write(6,*) ' Should not happen anymore!'
   call instop
endif

indprf1 = kprob1(19)
indprf2 = kprob2(19) 
nunkp1 = kprob1(4)
nphys1 = kprob1(33)
nunkp2 = kprob2(4)
npoint1 = kmesh1(8)      
nelem1 = kmesh1(9)      
npoint2 = kmesh2(8)      
 
if (indprf1 /= 0) then
  ! *** Variable number of dofs: probably ishape=7 for
  ! *** integrated method. Use only first 2 dofs for copy
  call ini060text(ibuffr,indprf1,'coefnew: indprf1')
  ipkprf1 = iniget(indprf1)
endif

if (indprf2 /= 0) then
   write(6,*) 'PERROR(cofcopy): not suited for variable numbers '
   write(6,*) '  of dofs in heat equation'
   write(6,*) 'indprf2: ',indprf2
   call instop 
endif

if (indprf1==0) then
   if (nunkp1 /= 2 .or. nunkp2 /= 1) then
      write(6,*) 'PERROR(cofcopy): number of dofs incorrect' 
      write(6,*) 'nunkp1 should be 2, but is: ',nunkp1
      write(6,*) 'nunkp2 should be 1, but is: ',nunkp2
      call instop
   endif
endif

call ini060text(ibuffr,isol1(1),'coefnew: isol1')
call ini060text(ibuffr,isol2(1),'coefnew: isol2')

ipusol1 = inidgt(isol1(1))
ipusol2 = inidgt(isol2(1))

if (mapneeded) then
   ! *** first find the value of temperature in the
   ! *** barycenter by interpolation
   call ini060text(ibuffr,ipemap(3),'coefnew: ipemap(3)')
   call ini060text(ibuffr,ipemap(5),'coefnew: ipemap(5)')
   iipemap = iniget(ipemap(3))
   irpemap = inidgt(ipemap(5))
   ! *** nodal point numbers per element (mesh1)
   call ini060text(ibuffr,kmesh1(17),'coefnew: kmesh1(17)')
   ! *** coordinates of nodal points (mesh1)
   call ini060text(ibuffr,kmesh1(23),'coefnew: kmesh1(23)')
   ikmeshc = iniget(kmesh1(17))
   ikmeshi = inidgt(kmesh1(23))
   call pemapinterpolate(1,ibuffr(ikmeshc),buffer(ikmeshi),buffer(ipusol2),ibuffr(iipemap),buffer(irpemap), &
         indprf1,ibuffr(ipkprf1),nunkp1,nelem1)      
   call printmap(ibuffr(iipemap),buffer(irpemap),buffer(ipusol2),buffer(ikmeshi),npoint1)
endif

if (.not.mapneeded) then
   call cofcop01(buffer(ipusol1),buffer(ipusol2),indprf1,ibuffr(ipkprf1),npoint1,nunkp1,nunkp2,factor) 
else 
   call ini060text(ibuffr,ipemap(3),'coefnew: ipemap(3)')
   iipemap = iniget(ipemap(3))
   call cofcop02(buffer(ipusol1),buffer(ipusol2),indprf1,ibuffr(ipkprf1),npoint1,nunkp1,nunkp2,factor, &
        ibuffr(iipemap),buffer(irpemap))  
endif

end subroutine cofcopybf

! *************************************************************
! *   COFCOP01
! *   Copy temperature from sol2 to sol1. Sol1 is used in 
! *   the specification of the coefficients.
! *   PvK 9906
! *
! *   PvK 071713 Fortran90
! *************************************************************
subroutine cofcop01(sol1,sol2,indprf1,kprobf1,npoint,nunkp1,nunkp2,factor)
implicit none
real(kind=8) :: sol1(:),sol2(:)
integer :: npoint,nunkp1,nunkp2,indprf1,kprobf1(:)
real(kind=8) :: factor
integer :: i,j,j1,j2

if (indprf1 == 0) then
   do i=1,npoint
      j1 = (i-1)*nunkp1 + 1
      sol1(j1) = factor*sol2(i)
   enddo
else 
   do i=1,npoint
      j1 = kprobf1(i) + 1
      sol1(j1) = factor*sol2(i)
   enddo
endif

end subroutine cofcop01
            
! *************************************************************
! *   COFCOP02
! *
! *   Copy temperature from sol2 (linear triangles) to sol1.
! *   Use MAP to find mapping of overlapping points and RMAP
! *   for the interpolated values in the barycenters. 
! *
! *   PvK 991019
! *
! *   PvK 071713 Fortran90
! *************************************************************
subroutine cofcop02(sol1,sol2,indprf1,kprobf1,npoint,nunkp1,nunkp2,factor,map,rmap)
implicit none
real(kind=8) :: sol1(:),sol2(:),rmap(:)
integer :: npoint,nunkp1,nunkp2,indprf1,kprobf1(*),map(*)
real(kind=8) :: factor,temp
integer :: i,j,j1,j2,ip

if (indprf1 == 0) then
   do i=1,npoint
      ip = map(i)
      if (ip<0) then
         temp = rmap(-ip)
      else
         temp = sol2(ip)
      endif
      j1 = (i-1)*nunkp1 + 1
      sol1(j1) = factor*temp
   enddo
else 
   do i=1,npoint
      ip = map(i)
      if (ip<0) then
         temp = rmap(-ip)
      else
         temp = sol2(ip)
      endif
      j1 = kprobf1(i) + 1
      sol1(j1) = factor*temp
   enddo
endif

end subroutine cofcop02
            
