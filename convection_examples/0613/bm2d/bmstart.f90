include 'PvKmodules.inc'
! *************************************************************
! *   BMSTART
! *
! *   Pseudocode:
! *
! *     Read user input
! *
! *     Create mesh for Stokes equation (mesh)
! *     Derive mesh for heat equation by subdivision of Stokes grid (nstmsh)
! *        If necessary: provide mapping information (pemap1to1/2)
! *
! *     Create problem definition (problem)
! *     Create information on the large matrices (commat/matstruc)
! * 
! *     Create initial condition (creavc) or read from file (readbs)
! *     Prescribe boundary conditions (presdf)
! *
! *************************************************************
! *     Arrays:
! *        kmesh1, kmesh2 :   mesh definition (for Stokes/heat)
! *        kprob1, kprob2 :   problem definition ( ,, / ,,)
! *        intmt1, intmt2 :   info on structure of large matrix
! *        matr1 , matr2  :   coefficients of large matrix
! *        iuser , user   :   user arrays 
! *        isol1 , isol2  :   solution vectors                    
! *        islol1, islol2 :   old (previous) solution vectors
! *        ipemap11, ipemap12: vectors containing info on maps between
! *                            different meshes
! *        nbufdef, num   :   declared size of IBUFFR and NUM
! *
! *   PvK 080100
! *
! *   PvK 071713: update to 0613 version with explicit ibuffr/buffr
! *               Fortran90
! *************************************************************
subroutine bmstart(ibuffr,buffer, &
     kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2, &
     matr1,matr2,iuser,user,isol1,isol2, &
     islol1,ipemap11,ipemap12,nbufdef,num)
use bmiter
use peparam
use vislo
use c1visc
use inival
use pexcyc
use vislo
use powerlaw
use pesimplerheology
use pecof900
use pecof800
use solutionmethod
implicit none
integer :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: kmesh1(:),kmesh2(:),kprob1(:),kprob2(:),intmt1(:)
integer :: intmt2(:),matr1(:),matr2(:),iuser(:),isol1(:),isol2(:)
integer :: islol1(:,:)
integer :: nbufdef,num,kmeshdum(400),ipemap11(:),ipemap12(:)
real(kind=8) :: user(*)

!PvK do something for this     include 'SPcommon/cplot'
integer ishape,iinput,jtypv,nsup1,nsup2,npoint,iincommt(5)
integer iu1(3)
integer irestart,iu2,i,idummy,idum
real(kind=8) rinput,u2,rl,u1
real(kind=4) second

kmesh1(1)  = 400
kmesh2(1)  = 400
kmeshdum(1)  = 400
kprob1(1)  = 400
kprob2(1)  = 400
iuser(1)   = 100
 user(1)   = num+5
ipemap11(1)  = 5
ipemap12(1)  = 5

! *************************************************************
! ***        USER INPUT
! *************************************************************

! *   *** Nitermax: maximum number of iterations 
! *   *** eps     : relative accuracy required for convergence
! *   *** relax   : relaxation parameter 
! *   *** irestart: indication for restart (1) or not (0)
read(5,*) nitermax,eps,relax,irestart

! *   *** Rayleigh: Thermal rayleigh number
! *   *** wavel   : Wavelength of initial perturbation
! *   *** ampini  : amplitude of initial perturbation
! *   *** itypv   : Type of viscosity law:
! *   ***           0 = isoviscous
! *   ***           1 = layer dependent
! *   ***           2 = temperature, pressure dependent
! *   ***           4 = non-Newtonian 
! *   ***           (see pefvis.f for actual specification of viscosity)
read(5,*) rayleigh,wavel,ampini,itypv

! *   *** Coefficients used in viscosity law:
! *   *** avisc   :  nondimensional activation energy
! *   *** bvisc   :  activation volume
! *   *** wvisc   :  ??
read(5,*) avisc,bvisc,wvisc

! *   *** Specification of layering (for viscosity)
! *   *** nlay    : number of layers
! *   *** prefac  : prefactors in viscosity law
! *   *** zint    : depth of the interface (z=1-y)
read(5,*) nlay,(prefac(i),i=1,nlay),(zint(i),i=1,nlay-1)
 
! *   *** q       : internal heating rate
read(5,*) q
if (q.gt.0) iqtype=1

! *   *** Specification of some coefficients for differential equations
! *   *** See SP guide, chapters 3 and 7.
! *   *** metupw  : Type of upwinding (0=none, 2=Il'in)
! *   *** intrule : integration rule for heat equation 
! *   ***           (0=default, 1=NC, 3=4 point Gauss)
! *   *** icoorsystem: coordinate system (0=Cartesian)
! *   *** intrule900: integration rule for Stokes equations
! *   *** interpol900: for interpolation of quadratic elements
read(5,*) metupw,intrule,icoorsystem,intrule900,interpol900
!
! *   *** Information on solution method (see PG Chapter 8)
! *   *** isolmethod : type of solution method
! *   ***              -1: old fashioned direct method
! *   ***               0: direct method (new approach)
! *   ***               1: CG/BiCGSTAB
! *   ***               2: GMRES
! *   *** iluprec    :  set to 1 if you want to keep the preconditioning matrix
! *   *** ipreco     : type of preconditioner (for isolmethod>0)
! *   ***               0: none
! *   ***               1: diagonal
! *   ***               2: ILU (eisenstat)
! *   ***               3: ILU
! *   *** iprint     : controls amount of info printed during iteration process
! *   *** keep       : set to 1 to keep precond matrix
! *   *** cgeps      : accuracy of iterative solution
read(5,*) isolmethod,iluprec,ipreco,maxiter,iprint,keep,cgeps


! *   *** Define mesh for Stokes equations. Option 2: read from mesh file
! *   *** that is generated, for example, by makemesh
open(99,file='mesh1')
call meshrd(-2,99,kmesh1)
close(99)
open(99,file='mesh2')
call meshrd(-2,99,kmesh2)
close(99)

!     *** Determine coordinates of nodal points along
!     *** x=0 and y=0.
ishape=2
call pefilxy(ishape,kmesh1,kprob1,idummy)
!     *** aspect ratio
rlam = xcmax-xcmin

!     *** Problem definition: new form of probdf as of June 2013
call probdfbf(ibuffr,buffer,0,kprob1,kmesh1,iinput)
call probdfbf(ibuffr,buffer,0,kprob2,kmesh2,iinput)

! *** Compute structure of large matrices
if (isolmethod.eq.-1) then
!   *** penalty function method
  call commatbf(ibuffr,1,kmesh1,kprob1,intmt1)
else
  if (isolmethod.eq.0) then
!    *** penalty function method, symmetric
     iincommt(1) = 2
     iincommt(2) = 1
  else
!    *** 6=compact non symmetric. 12=row/column compressed
     iincommt(1) = 3
     iincommt(2) = 6
     iincommt(4) = iluprec
  endif
  call matstrbf(ibuffr,iincommt,kmesh1,kprob1,intmt1)
  if (iluprec.eq.1) then
     intmt1(1) = 6 + 100*iluprec
  endif
endif
call commatbf(ibuffr,2,kmesh2,kprob2,intmt2)
write(6,*) 'kamat: ',intmt1(5),intmt2(5)
     
!     *** Set logicals for viscosity law
jtypv=abs(itypv)
if (jtypv.gt.2) then
   write(6,*) 'PERROR(bmstart): itypv>2'
   call instop
endif
if (jtypv.eq.1.or.jtypv.eq.3) ivl=.true.
if (jtypv.eq.2) ivt=.true.

!     *** See if matrix is too large for current size of IBUFFR
nsup1  = kprob1(29)
nsup2  = kprob2(29)
npoint = kmesh1(8)
if (nsup1 /= 0 .or. nsup2 /= 0) then
   write(6,*) 'NSUPER: ',nsup1,nsup2
   call instop
endif
if (npoint>num) then
   write(6,*) 'PERROR(bmstart): Number of points too large '
   write(6,*) 'for user arrays. Npoint = ',npoint
   call instop
endif

if (irestart.gt.0) then
!  *** Read from file
   namef2 = 'restart.back'
   call openf2(.false.)
!  call readbsbf_netcdf(ibuffr,buffer,isol1,kmesh1,kprob1,'velo_restart.nc')
   call readbsbf_netcdf(ibuffr,buffer,isol2,kmesh2,kprob2,'temp_restart.nc')
else
!  *** Define temperature field with func(ichois=1).
   iu1(1) = 1
   call creavcbf(ibuffr,buffer,0,1,idum,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
!  *** Make velocity 0
   iu1(1) = 0
   iu1(2) = 0
   call creavcbf(ibuffr,buffer,0,1,idum,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
endif
iu1(1) = 0
iu1(2) = 0
call creavcbf(ibuffr,buffer,0,1,idum,islol1(1,1),kmesh1,kprob1,iu1,u1,iu2,u2)
call creavcbf(ibuffr,buffer,0,1,idum,islol1(1,2),kmesh1,kprob1,iu1,u1,iu2,u2) 

! *** Prescribe boundary conditions
call presdfbf(ibuffr,buffer,kmesh1,kprob1,isol1)
call presdfbf(ibuffr,buffer,kmesh2,kprob2,isol2)


end subroutine bmstart

