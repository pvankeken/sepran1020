c *************************************************************
c *   intermediate_output
c * 
c *   Intermediate output for program BM
c *
c *   Output of iteration number, accuracy, Nusselt number
c *   rms velocity, local temperature gradients and cpu time
c *
c *   PvK 120490
c *************************************************************
      subroutine intermediate_output(ibuffr,buffr,
     v         kmesh1,kmesh2,kprob1,kprob2,
     v         isol1,isol2,islol1,islol2,iuser,user,ipemap)
      implicit none
      integer :: ibuffr(:)
      real(kind=8) :: buffr(:)
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),islol1(*),islol2(*),iuser(*),ipemap(*)
      real*8 user(*)
      include 'bmiter.inc'
      include 'peparam.inc'
      integer NFUNC
      parameter(NFUNC=505)
      integer NUM
      parameter(NUM=100 000)
      integer igradt(5),icurvs(3),npoint
      real*8 funcx(NFUNC),funcy(NFUNC)
      integer ivser(100)
      real*8 vser(NUM),smax,tmax,volint,vrms1,vrms2
      real*8 bounin,gnus,temp1,q1,q2,vrms,anorm
      integer nunkp,irule,ihelp,ip,ielhlp
      real*4 second



      data ivser(1),vser(1),funcx(1),funcy(1)/100,NUM,2*NFUNC/

      t2 = second()
      dcpu = t2-t1
      cput = t2-t0
      t1   = t2

c     **** Relaxation
      if (relax.ne.0) then
	 call algebr(3,0,islol2,isol2,isol2,kmesh2,kprob2,
     v               1-relax,relax,0,0,ip)
      endif
      dif1 = anorm(0,3,0,kmesh1,kprob1,isol1,islol1,ielhlp)
      dif2 = anorm(0,3,0,kmesh2,kprob2,isol2,islol2,ielhlp)
      smax = anorm(1,3,0,kmesh1,kprob1,isol1,isol1,ielhlp)
      tmax = anorm(1,3,0,kmesh2,kprob2,isol2,isol2,ielhlp)
      dif1 = dif1/smax
      dif2 = dif2/tmax
      dif  = dif2

      call vrmsnew(isol1,kmesh1,kprob1,ivser,vser,ipemap)
      ivser(2)=1
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v                  vser,ihelp)/rlam
      vrms1 = sqrt(vrms2)
      vrms = vrms1

      npoint=kmesh2(8)
      if (npoint.gt.NUM) then
         write(6,*) 'PERROR(intermediate_output)'
         write(6,*) 'npoint > NUM'
         write(6,*) npoint,NUM
         call instop
      endif


      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,isol2,
     v            iuser,user,ihelp)
      irule = 3
      temp1 = bounin(1,irule,1,1,kmesh2,kprob2,1,1,isol2,iuser,user)
      gnus  = -bounin(1,irule,1,2,kmesh2,kprob2,3,3,igradt,iuser,
     v                user)/temp1
      icurvs(1) = -1
      icurvs(2) = 3
      icurvs(3) = 3
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      nunkp = funcy(5)
      q2 = -funcy(6)
      q1 = -funcy(5+nunkp)

      write(6,11) niter,dif,gnus,vrms,q1,q2,dcpu,cput
11    format(i4,f12.8,f12.7,f12.4,2f12.7,2f8.2)
c     call flush(6)

      return
      end
