c *************************************************************
c *   FNV003
c *
c *   Viscosity description used with modelv=103.
c *   Temperature is stored as first degree of freedom of
c *   second vector in uold (multiplied by Ra)
c *************************************************************
      real*8 function fnv003(x1,x2,x3,v1,v2,v3,secinv,
     v                  numold,maxunk,uold)
      implicit none
      integer numold,maxunk
      real*8 x1,x2,x3,v1,v2,v3,secinv,uold(numold,maxunk)

      include 'pesimplerheology.inc'
      include 'vislo.inc'
      include 'c1visc.inc'
      include 'powerlaw.inc'
      include 'peparam.inc'
      real*8 temp
      integer jtypv,ival,ivalfind

      if (numold.lt.2) then
         write(6,*) 'PERROR(fnv003): numold < 2'
         write(6,*) 'Second vector should contain temperature here'  
         call instop
      endif

      jtypv = abs(itypv)
      if (jtypv.eq.0) then 
         fnv003 = 1d0
         return
      else if (jtypv.eq.1) then
         ival = ivalfind(x2)
         fnv003 = prefac(ival)
         return
      else 
         temp = uold(2,1)/rayleigh
         fnv003 = exp(-avisc*temp + bvisc*(1-x2))
c        write(6,*) 'fnv003: ',fnv003,avisc,temp,x2
         return
      endif

      return
      end

