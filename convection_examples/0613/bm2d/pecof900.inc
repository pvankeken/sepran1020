c *   *** pecof900.inc
c *   *** specification of coefficients for Stokes equation (type 900)
c *   *** intrule900 : type of numerical integration
c *   *** icoor900   : type of coordinate system
c *   *** interpol900: interpolate coefficients by quadratic (0) or
c *   ***              or by subdivided linear interpolation (1)
      integer intrule900,icoor900,interpol900
      common /pecof900/ intrule900,icoor900,interpol900
