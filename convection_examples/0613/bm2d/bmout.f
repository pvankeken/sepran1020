c *************************************************************
c *   BMOUT
c *
c *   output of program BM
c *
c *   Compute heatflow (Nu) along top, bottom and side boundaries.
c *   Find local values of heat flow in cornerpoints
c *   find minimum and maximum velocity
c *   Compute rms velocity
c *   Plot velocity and temperature (in one plot)
c *   Write solutions to file
c *
c *   PvK 080100
c *************************************************************
      subroutine bmout(ibuffr,buffr,
     v           kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                 iuser,user,ipemap)
      implicit none
      integer :: ibuffr(:)
      real(kind=8) :: buffr(:)
      integer NFUNC,NPUS,DONE,DZERO
      parameter(NFUNC=505,NPUS=100000,DONE=1d0,DZERO=0d0)
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),iuser(*),ivelx(5),ively(5),ipemap(*)
      real*8 user(*),funcx(NFUNC),funcy(NFUNC)
      integer igradt(5),icurvs(3)
      integer kmesh3(100),kprob3(100),iinput(100)
      integer ivser(100)
      real*8 vser(NPUS)
      include 'bmiter.inc'
      include 'peparam.inc'
      include 'solutionmethod.inc'
      include 'pecof800.inc'
      include 'SPcommon/cmacht'
      character*80 fname
      common /seppl2/ fname
      include 'SPcommon/cplot'
      integer npoint,ir,nunkp,nx,ny,ihelp,ncntln,numarr,iinvec(3)
      real*8 temp1,gnus1,gnus2,gnus3,gnus4,q1,q2,q3,q4
      real*8 vrms2,vrms1,volint
      real*8 format,x,y,psiphi,bounin,contln,rinvec(2)
     
      
      kmesh3(1)= 100
      kprob3(1)= 100
      funcx(1) = NFUNC
      funcy(1) = NFUNC
      ivser(1) = 100
       vser(1) = NPUS

      npoint = kmesh2(8)
      if (5+npoint.gt.NPUS) then
         write(6,*) 'PERROR(bmout): npoint > NPUS'
         write(6,*) npoint,NPUS
         call instop
      endif
      metupw=0
      call coefnew(2,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                    iuser,user,isol1,ipemap)
      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,isol2,
     v            iuser,user,ihelp)
      ir = 3
      temp1 = bounin(1,ir,1,1,kmesh2,kprob2,1,1,isol2,iuser,user)
      gnus1=-bounin(2,ir,1,1,kmesh2,kprob2,3,3,igradt,iuser,user)/temp1
      gnus2= bounin(2,ir,1,1,kmesh2,kprob2,1,1,igradt,iuser,user)/rlam
      gnus3= bounin(2,ir,1,1,kmesh2,kprob2,2,2,igradt,iuser,user)
      gnus4= bounin(2,ir,1,1,kmesh2,kprob2,4,4,igradt,iuser,user)

      icurvs(1) = -1
      icurvs(2) = 3
      icurvs(3) = 3
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      nunkp = funcy(5)
      q2 = -funcy(6)
      q1 = -funcy(5+nunkp)

      icurvs(2) = 1
      icurvs(3) = 1
      call compcr(0,kmesh2,kprob2,igradt,2,icurvs,funcx,funcy)
      q4 = -funcy(6)
      q3 = -funcy(5+nunkp)
      nx = nunkp
      ny = npoint/nunkp

      iinvec(1) = 3
      iinvec(2) = 3
      iinvec(3) = 1
      call manvec(iinvec,rinvec,isol1,isol1,isol1,kmesh1,kprob1)
      write(6,*) 'umin/max: ',rinvec(1),rinvec(2)
      iinvec(3) = 2
      call manvec(iinvec,rinvec,isol1,isol1,isol1,kmesh1,kprob1)
      write(6,*) 'vmin/max: ',rinvec(1),rinvec(2)

c     **** RMS mean square velocity ********
      call vrmsnew(isol1,kmesh1,kprob1,ivser,vser,ipemap)
      ivser(2)=1
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v                  vser,ihelp)/rlam
      vrms1 = sqrt(vrms2)

      write(6,*)
      write(6,'(''Niter  '',i12,f12.2)') niter,cput
      write(6,'(''Nu     '',2f12.7)') gnus1,gnus2
      write(6,'(''Vrms   '',f12.7)') vrms1
      write(6,'(''q1     '',2f12.7)') q1,q3
      write(6,'(''q2     '',2f12.7)') q2,q4

c     if (isolmethod.le.0) then
c       call pevrms9(isol1,kmesh1,kprob1,ivser,vser)
c       vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
c    v                  vser,ihelp)/rlam
c       vrms1 = sqrt(vrms2)
c       write(6,*) 'Old VRMS: ',vrms1
c     endif

      format = 10d0
      fname = 'PLOT.001'
      x=0d0
      y=0d0
      jtimes=1
      call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,DONE,DZERO)

      jtimes=3
      ncntln=0
      call plotc1(1,kmesh2,kprob2,isol2,contln,ncntln,format,DONE,1)
 
      namef2='restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'velo',isol1,kprob1)
      call writbs(0,2,numarr,'temp',isol2,kprob2)
      call writb1

      return
      end


