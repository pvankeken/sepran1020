module bmiter
  ! *   *** Information on iteration process  
  ! *   *** eps  : required relative accuracy
  ! *   *** relax: relaxation parameter
  ! *   *** dif1, dif2, dif: difference in solution with previous
  ! *   ***       iteration (Stokes, heat, maximum)
  ! *   *** nitermax: maximum number of iterations
  ! *   *** niter   : current iteration number
  real(kind=8):: eps,relax,dif1,dif2,dif
  integer :: nitermax,niter
end module bmiter
