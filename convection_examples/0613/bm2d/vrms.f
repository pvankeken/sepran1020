c ***************************************************
c *   PEVRMS
c *
c *   Calculates u*u + v*v per nodal point from the
c *   solution vector and places the result in array user.
c *
c *   PvK, 17-4-89/930107
c *****************************************************************
      subroutine pevrms(isol,kmesh,kprob,iuser,user)
      implicit none

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      integer isol(*),kmesh(*),kprob(*),iuser(*)
      real*8 user(*)
      integer npoint,inidgt,ipusol

      call ini050(isol(1),'pevrms: isol')
      npoint = kmesh(8)
      ipusol = inidgt(isol(1))
      call pevrm1(npoint,buffr(ipusol),user(6))

      iuser(6) = 7
      iuser(7) = 2001
      iuser(8) = 6

      return
      end

c ************************************************************
c *   PEVRM1
c *   
c *   Calculates u*u+v*v from the solutionvector usol and stores
c *   the result in array user from position one.
c *
c *   Pvk 17-4-89
c ************************************************************
      subroutine pevrm1(npoint,usol,user)
      implicit none
      real*8 usol(*),user(*)
      integer npoint
      integer ip,ips1,ips2
      real*8 uv2
     
      do ip=1,npoint
         ips1 = 2*ip-1
         ips2 = 2*ip
         uv2   = usol(ips1)*usol(ips1) + usol(ips2)*usol(ips2)
         user(ip) = uv2
      enddo

      return
      end 
