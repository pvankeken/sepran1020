module bmiter
  ! *   *** Information on iteration process  
  ! *   *** eps  : required relative accuracy
  ! *   *** relax: relaxation parameter
  ! *   *** dif1, dif2, dif: difference in solution with previous
  ! *   ***       iteration (Stokes, heat, maximum)
  ! *   *** nitermax: maximum number of iterations
  ! *   *** niter   : current iteration number
  real(kind=8) :: eps,relax,dif1,dif2,dif
  integer :: nitermax,niter
end module bmiter

module peparam
  ! *   *** peparam.inc
  ! *   *** specification of some parameters for convection equations
  ! *   *** rayleigh: thermal rayleigh number
  ! *   *** rlam    : aspect ratio
  ! *   *** q       : internal heating rate
  ! *   *** visc0,b,c: obsolete
  real(kind=8) :: rayleigh,rlam,q,visc0,b,c
end module peparam
 
module vislo
  ! *   *** logical used in viscosity description
  ! *   *** ivl : layer dependence
  ! *   *** ivt : temperature dependence
  ! *   *** ivn : non-Newtonian
  logical :: ivl,ivt,ivn
end module vislo

module c1visc
  ! *   *** Information on viscosity description
  ! *   *** viscl, ctd, cpd, nl: unused
  ! *   *** itypv: type of viscosity (0=iso; 2=T,p)
  real(kind=8) :: viscl,ctd,cpd
  integer :: itypv,nl
end module c1visc

module inival
  real(kind=8) :: wavel,ampini
end module inival

module pexcyc
  integer :: nx,ny
  integer, parameter :: NXCYCMAX=1000
  real(kind=8) :: xc(NXCYCMAX),yc(NXCYCMAX),xcmin,xcmax,ycmin,ycmax
end module pexcyc

module powerlaw
  ! *   *** Specification of layer dependent creep laws
  ! *   *** zint   : depth of interfaces
  ! *   *** spw    : local powerlaw index (well, (n-1)/n if all is well)
  ! *   *** prefac : local value of pre-exponential factor
  ! *   *** isoplind: depth dependence or not?
  real(kind=8) :: zint(10),spw(11),prefac(10)
  integer :: nlay
  logical :: isoplind
end module powerlaw

module pesimplerheology
  ! *   *** coefficients for viscosity law (for Blankenbach et al., 1989
  ! *   *** benchmark).
  real(kind=8) :: avisc,bvisc,wvisc
end module pesimplerheology

module pecof900
  ! *   *** specification of coefficients for Stokes equation (type 900)
  ! *   *** intrule900 : type of numerical integration
  ! *   *** icoor900   : type of coordinate system
  ! *   *** interpol900: interpolate coefficients by quadratic (0) or
  ! *   ***              or by subdivided linear interpolation (1)
  integer :: intrule900,icoor900,interpol900
end module pecof900

module pecof800
  ! *   *** Specification of coefficients for heat equation (type 800)
  ! *   *** heatproduction :  constant heat production rate
  ! *   *** metupw         :  type of upwinding
  ! *   *** intrule        :  type of numerical integration
  ! *   *** icoorsystem    :  type of coordinate system
  ! *   *** iqtype         :  type of internal heating
  real(kind=8) :: heatproduction
  integer :: metupw,intrule,icoorsystem,iqtype
end module pecof800

module solutionmethod
  ! *   *** Information of solution method for Stokes equations
  real(kind=8) :: cgeps
  integer :: isolmethod,iluprec,ipreco,maxiter,iprint,keep
end module solutionmethod
