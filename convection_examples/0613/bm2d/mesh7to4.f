c *************************************************************
c *   MESH7TO4
c *
c *   Convert a mesh from ISHAPE=7 to ISHAPE=4
c *
c *   AvdB 910717
c *************************************************************
      subroutine MESH7TO4(kmesh1,kmesh2,iprint)
      implicit none
      integer kmesh1(*), kmesh2(*),iprint
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      character*6 modnam
      parameter ( MODNAM = 'nstmsh')
      integer ndim,npelm,nelgrp,npoint,nelem,kelma,kelmb
      integer ikelmc,ikelmi,ik,kelm1
      integer npelm2,nelem2,i,lengpc2,ikelmc2,inidgt,iniget 
 
      ndim  =  kmesh1(6)
      npelm =  kmesh1(4)
      nelgrp=  kmesh1(5)
      npoint=  kmesh1(8)
      nelem =  kmesh1(9)
      kelma =  kmesh1(15)
      kelmb =  kmesh1(16)

      do i=1,50
        write(6,'(i5,i10)') i,kmesh1(i)
      enddo
 
      write(6,1000) ndim,npelm,nelgrp,npoint,nelem,kelm1,kelmb
 
 
c     *** locate element/nod.point data (kmesh,c)
c     *** and coordinates (kmesh,i) in buffer 
      call ini050(kmesh1(17),'nstmsh: nod.point numb.')
      call ini050(kmesh1(23),'nstmsh: coordinates')
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
 
c     *** copy kmesh1 to kmesh2 - the element nod.point numbers in
c     *** part kelem,c will be redefined
      do ik = 1,kmesh1(1)
         kmesh2(ik) = kmesh1(ik)
      enddo
c     *** update relevant parameters
      npelm2    = 6
      nelem2    = nelem
      kmesh2(4) = npelm2
      kmesh2(9) = nelem2
 
c     *** create space in buffer for array that will be modified in the
c     *** split-up procedure - kelem2,c kelm2,f kelm2,g
c     *** (kelm,f,g are modified only incase nelgrp>1)
      kmesh2(17) = 0
      lengpc2    = nelem2 * npelm2
      call ini051(kmesh2(17), lengpc2, MODNAM)
      ikelmc2 = iniget(kmesh2(17))
 
      call nest7to4(kmesh1,kmesh2,ibuffr(ikelmc),buffr(ikelmi),
     1          ibuffr(ikelmc2),npelm)
 
c     **** print out some array's ?
      if (iprint.gt.0) then
c        *** array kmesh
         write(6,1100) '1',(i,kmesh1(i),i=1,60)
         write(6,1100) '2',(i,kmesh2(i),i=1,60)
         if (iprint.gt.10) then
c           **** check - print element nodal points and coordinates
c           **** call prelnod(nelem,npelm,kelmc,coor)
c           **** mesh1
            call prelnod(nelem,npelm,ibuffr(ikelmc),ibuffr(ikelmi))
c           **** mesh2
            call prelnod(nelem2,npelm2,ibuffr(ikelmc2),ibuffr(ikelmi))
         end if
      end if
 
      return
c
1000  format(/,'NSTMSH - parameters input mesh:',/,
     .         'ndim ...... ',i5,/,
     1         'npelm ..... ',i5,/,
     2         'nelgrp .... ',i5,/,
     3         'npoint .... ',i5,/,
     4         'nelem ..... ',i5,/,
     5         'kelma ..... ',i5,/,
     6         'kelmb ..... ',i5)
1100  format(/,'printout header kmesh',a,':',/,
     1       8( 5(i3,':',i5,1x), /))
      end

c *************************************************************
c *   NEST
c *   AvdB version 910715
c *************************************************************
      subroutine nest7to4(kmesh1,kmesh2,kelmc,coor,kelmc2,npelm) 
      implicit none
      integer kmesh1(*),kmesh2(*),kelmc(*),kelmc2(*),npelm
      real*8 coor(2,*)
 
c     * npelm - # points per element before split
c     * NSUB  - # sub-elements
c     * NPSUB - # points per sub-element
c
c     * nodal points in sub-elements
c     - odd numbers correspond to the vertices of the orig. elements
c     - even numbers are the midside points
c     - the nodalpoint numbering sequence is counterclock wise in all
c       elements.
      integer NSUB,NPSUB,ISHAPE
      parameter (NSUB = 1, NPSUB = 6, ISHAPE=4)
      integer nodpsub(NPSUB,NSUB),i,j
      integer nelgrp,nelem,ikelmf,ikelmg,np,ielem,iptelm,lsub
      integer iknodpt,ipsub,ielgrp,ipntg,inelem1

      nodpsub(1,1)=1
      nodpsub(2,1)=2
      nodpsub(3,1)=3
      nodpsub(4,1)=4
      nodpsub(5,1)=5
      nodpsub(6,1)=6
      nelgrp = kmesh1(5)
      nelem  = kmesh1(9)
      ikelmf = kmesh1(20)
      ikelmg = kmesh1(21)
 
      np = 0
      do ielem = 1,nelem
c        * split element into NSUB triangles
c        - define corresponding nodalpoints in new kelmc 
         iptelm = (ielem-1)*npelm
         do lsub = 1,NSUB
            do ipsub = 1, NPSUB
               iknodpt = kelmc( iptelm + nodpsub(ipsub,lsub) )
               np = np + 1
               kelmc2(np) = iknodpt
            enddo
         enddo
      enddo
c
c     * update some parts of kmesh part 2
c     - modify kelm,f - shape numbers
      do ielgrp = 1, nelgrp
         kmesh2(ikelmf+ielgrp-1) = ISHAPE
      enddo
c     *** elements per element group (only for nelgrp > 1)
      if (nelgrp.gt.1) then
c        *** kelm,g
         do ielgrp = 1, nelgrp
            ipntg = ikelmg + ielgrp - 1
            inelem1 = kmesh1(ipntg)
            kmesh2(ipntg) = inelem1 * NSUB 
         enddo
      end if
 
      return
      end

