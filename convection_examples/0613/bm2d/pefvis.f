c *************************************************************
c *   PEFVIS
c *
c *   Version for sigma,T dependent rheology in layered case
c *   Find value of ival through /powerlaw/zint(10).
c ************************************************************* 
      real*8 function pefvis(x,y,temp,ival,secinv)
      implicit none
      real*8 x,y,temp,secinv
      integer ival
      include 'powerlaw.inc'
      include 'vislo.inc'
      include 'pesimplerheology.inc'
      include 'c1visc.inc'
      
      if (abs(itypv).eq.0) then
        pefvis = 1d0 
        return
      else if (abs(itypv).eq.1) then
        write(6,*) 'PERROR(pefvis) itypv=1 (or ivl=.true.) obsolete'
        call instop
      else 
        pefvis = exp(-avisc*temp + bvisc*(1-y))
      endif
    
       return
      end
