c *************************************************************
c *   COEFNEW
c *
c *   Specify coefficients for Stokes and heat equation.
c *   Stokes:
c *      Use fil103 to specify coefficients directly 
c *             (not through input)
c *      Use modelv=103 and use fnv003 
c *
c *   PvK 990302
c *
c *   Modification to allow for transport between extended quadratic
c *   mesh for the Stokes equation (integrated method, ISHAPE=7)
c *   and the linear mesh (ISHAPE=3) for the heat equation.
c *   IPEMAP contains the mapping information from quadratic to
c *   linear grid.
c *   1) For specification of Ra*T in Stokes equation, we need
c *      the mapping for the gridpoints at the boundaries of the
c *      elements. For the internal nodal point we need to 
c *      interpolate the temperature in the inner linear triangle
c *   2) For specification of velocity, we can use the map to
c *      restrict the values that need to be copied from the first
c *      to second problem.  No interpolation is necessary.
c *   PvK 080100
c *************************************************************
      subroutine coefnew(ibuffr,buffr, 
     v                    ichoice,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,iuser,user,islol1,ipemap)
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer islol1(5,*),ipemap(5)
      integer isol2(*),iuser(*),ichoice
      real*8 user(*)
      integer :: ibuffr(:)
      real(kind=8) :: buffr(:)

      include 'solutionmethod.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'peparam.inc'
      include 'c1visc.inc'
      integer itime,nparm
      integer iwork(4,100),iincop(6)
      real*8 work(100)

      real*8 eps,f2
      integer mconv,modelv,npoint,i,k
      logical mapneeded

      do i=1,100
         do k=1,4
           iwork(k,i) = 0
         enddo
         work(i) = 0d0
      enddo
      do i=6,iuser(1)
         iuser(i)=0
      enddo
      do i=6,user(1)
         user(i)=0d0
      enddo

c     **** Need to specify the mapping from 7 to 3 point based grid?
      if (isolmethod.le.0) then
c        *** kmesh1 is based on 6 point triangles
         mapneeded = .false.
      else
c        *** kmesh1 is based on 7 point triangles
         mapneeded = .true.
         if (ipemap(2).ne.-99) then
            write(6,*) 'PERROR(coefnew): ipemap is not of type PvK' 
            call instop
         endif
         if (ipemap(3).eq.0) then
            write(6,*) 'PERROR(coefnew): ipemap has not been filled' 
            call instop
         endif
    
      endif

      if (ichoice.eq.1) then

c        *** Momentum equation type 900 only
         modelv=  103
         mconv =  0
         itime = 0
         iwork(1,1) = itime
         iwork(1,2) = modelv
         iwork(1,3) = intrule900 + 100*interpol900
         iwork(1,4) = icoor900
         iwork(1,5) = mconv

         if (isolmethod.eq.0) then 
c           *** penaltyfunction parameter
            eps    = 1d-6
         else 
             eps = 0
         endif
         f2     = 1d0
   
c        *** 6: epsilon
         iwork(1,6) = 0
          work(6) = eps
c        *** 7: rho
         iwork(1,7) = 0
          work(7)   = 1d0
c        *** 8: omega
c        *** 9: f1
c        *** 10: f2. First dof of second vector in islold.
         iwork(1,10) = 2003
         iwork(2,10) = 2
         iwork(3,10) = 1
c        *** 11: f3
c        *** 12: eta
         iwork(1,12) = 0
          work(12)   = 1
         nparm=12


c        *** copy temperature, multiplied by Ra into islold

         call cofcopy(0,islol1(1,2),isol2,kmesh1,kmesh2,
     v        kprob1,kprob2,rayleigh,ipemap,mapneeded)
         call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1) 
c        call prinrv(islol1(1,2),kmesh1,kprob1,4,1,'islol1-2')
       else

         do i=1,25
            user(5+i)=0d0
           iuser(5+i)=0
         enddo

         npoint = kmesh2(8)
c        *** Coefficients for element 800
c        *** First five, integer info
c        *** 1 = not yet used
         iuser(6)  = 7
         iuser(7)  = 0
c        *** 2 = Type of upwinding
         iuser(8)  = metupw
c        *** 3 = Integration rule
         iuser(9)  = intrule
c        *** 4 = Type of coordinate system
         iuser(10)  = icoorsystem
c        *** 5 = not yet used
         iuser(11) = 0

c        *** a11,a12,a13,a22,a23,a33,u1,u2,u3,beta,f,rhocp
c        *** 6-11 = diffusivity tensor
         iuser(12) = -6
         iuser(15) = -6
          user(6) = 1d0
c        *** 12-14: velocity components
         iuser(18) = 2001
         iuser(19) = 10
         iuser(20) = 2001
         iuser(21) = 10+npoint
c        *** 15: beta; 16: q
         if (iqtype.eq.1) then
           iuser(24) = -7
            user(7) = q
         endif
c        *** 17: rho*cp
         iuser(25) = -6
         
         if (isolmethod.le.0) then
           call pecopy(2,isol1,user,kprob1,10,1d0)
           call pecopy(3,isol1,user,kprob1,10+npoint,1d0)
c          do i=1,npoint
c             write(6,'(''Vel: '',i5,2f8.3)') i,user(9+i),
c    v      user(9+i+npoint)
c          enddo
         else
           call penewcopy(2,isol1,kmesh1,kprob1,kmesh2,kprob2, 
     v              user,10,ipemap,mapneeded) 
           call penewcopy(3,isol1,kmesh1,kprob1,kmesh2,kprob2,
     v              user,10+npoint,ipemap,mapneeded) 
c          do i=1,npoint
c             write(6,'(i5,2f8.3)') i,user(9+i),user(9+npoint+i)
c          enddo
         endif
       
       endif

      return
      end

c *************************************************************
c *   PENEWCOPY
c *
c *   Copy velocity components from extended quadratic mesh 
c *   (ISHAPE=7) to linear mesh (ISHAPE=3) for heat equation.
c *   ipemap contains the mapping of 
c *************************************************************
      subroutine penewcopy(ichoice,isol,kmesh,kprob,kmesh2,kprob2,
     v        user,istart,ipemap,mapneeded)
      implicit none
      integer ichoice,isol(*),kmesh(*),kprob(*),istart,ipemap(*)
      integer kprob2(*),kmesh2(*)
      real*8 user(*)
      logical mapneeded
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      integer iipemap,ipusol,npoint,indprf,nunkp,npoint2
      integer ipkprf,inidgt,iniget
 
      npoint = kmesh(8)
      indprf = kprob(19)
      nunkp = kprob(4)
      npoint2 = kmesh2(8)
      
      if (ichoice.ne.2.and.ichoice.ne.3) then
         write(6,*) 'PERROR(penewcopy): ichoice should be 2 or 3'
         write(6,*) 'ichoice = ',ichoice
         call instop
      endif

      if (mapneeded) then
        if (ipemap(2).ne.-99) then
           write(6,*) 'PERROR(penewcopy): ipemap is not of type PvK'
           call instop
        endif
        if (ipemap(3).eq.0) then
           write(6,*) 'PERROR(penewcopy): ipemap has not been filled'
           call instop
        endif
        call ini050(ipemap(3),'coefnew: ipemap(3)')
        iipemap = iniget(ipemap(3))
      endif

      call ini050(isol(1),'coefnew: isol')
      ipusol = inidgt(isol(1))

      if (indprf.ne.0) then
c        *** variable number of dofs: probably integrated
c        *** method. Activate KPROBF
         call ini050(indprf,'coefnew: indprf')
         ipkprf = iniget(indprf)
      endif
      if (indprf.eq.0.and.nunkp.ne.2) then
         write(6,*) 'PERROR(penewcopy): nunkp should be 2'
         call instop
      endif

      call penwcp01(ichoice,buffr(ipusol),indprf,ibuffr(ipkprf),npoint, 
     v          npoint2,nunkp,user(istart),ibuffr(iipemap),mapneeded)

      return
      end

      subroutine penwcp01(ichoice,sol,indprf,kprobf,npoint,npoint2,
     v             nunkp,temp,map,mapneeded)
      implicit none
      real*8 sol(*),temp(*)
      integer ichoice,indprf,kprobf(*),npoint,nunkp,map(*),npoint2
      logical mapneeded
      integer i,j1,ip

      if (.not.mapneeded) then
         if (indprf.ne.0) then
            write(6,*) 'PERROR(penwcp01): not suited for this case'
            write(6,*) 'yet: mapneeded=.false. yet indprf<>0'
            call instop
         endif
         if (npoint.ne.npoint2) then
           write(6,*) 'PERROR(penwcp01): npoint <> npoint2'
           write(6,*) 'Number of points in two meshes should be equal' 
           call instop
         endif
         if (ichoice.eq.2) then
c           *** horizontal velocity component
            do i=1,npoint2
               j1 = (i-1)*nunkp + 1
               temp(i) = sol(j1)
            enddo
         else if (ichoice.eq.3) then
c           *** vertical velocity component
            do i=1,npoint2
               j1 = (i-1)*nunkp + 2
               temp(i) = sol(j1)
            enddo
         endif
      else
         if (indprf.gt.0) then
c           **** mapneeded AND variable dofs
            if (ichoice.eq.2) then
              do i=1,npoint
c               *** first find corresponding number in second mesh
                ip = map(i)
                if (ip.gt.0) then
c                  *** nodal points overlap
                   j1 = kprobf(i)+1
                   temp(ip) =  sol(j1)
                endif
              enddo
            else
              do i=1,npoint
c               *** first find corresponding number in second mesh
                ip = map(i)
                if (ip.gt.0) then
c                  *** nodal points overlap; second dof
                   j1 = kprobf(i)+2
                   temp(ip) =  sol(j1)
                endif
              enddo
            endif
         else
c           *** mapneeded, constant dofs
            if (ichoice.eq.2) then
              do i=1,npoint
                ip = map(i)
                if (ip.gt.0) then
c                  *** nodal points overlap
                   j1 = nunkp*(i-1)+1
                   temp(ip) =  sol(j1)
                endif
              enddo
            else if (ichoice.eq.3) then
              do i=1,npoint
                ip = map(i)
                if (ip.gt.0) then
c                  *** nodal points overlap
                   j1 = nunkp*(i-1)+2
                   temp(ip) =  sol(j1)
                endif
              enddo
            endif
         endif
      endif

      return 
      end
  
c *************************************************************
c *   cofcopy
c *
c *   Copy first dof of isol2 into first dof of isol1
c *
c *   Note: as of 1995 or so, solution vectors are not renumbered
c *   anymore...
c *************************************************************
      subroutine cofcopy(ichoice,isol1,isol2,kmesh1,kmesh2,
     v               kprob1,kprob2,factor,ipemap,mapneeded)
      implicit none
      integer ichoice,isol1(5),isol2(5),kmesh1(*), kmesh2(*)
      integer kprob1(*),kprob2(*),ipemap(*)
      real*8 factor
      logical mapneeded
 
      integer ibuffr
      common ibuffr(1) 
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      real*8 buffer(1)
      equivalence( ibuffr(1),buffer(1))

      integer indprf1,indprf2,nunkp,iipemap,irpemap,ikmeshc
      integer ikmeshi,nelem1
      integer ipusol1,ipusol2,iniget,inidgt,npoint1,npoint2
      integer nunkp1,nunkp2 ,ipkprf1,nphys1

      if (kprob1(21).ne.0.and.kprob2(21).ne.0) then
         write(6,*) 'PERROR(cofcopy): kprobh <> 0!' 
         write(6,*) ' Should not happen anymore!'
         call instop
      endif


      indprf1 = kprob1(19)
      indprf2 = kprob2(19) 
      nunkp1 = kprob1(4)
      nphys1 = kprob1(33)
      nunkp2 = kprob2(4)
      npoint1 = kmesh1(8)      
      nelem1 = kmesh1(9)      
      npoint2 = kmesh2(8)      
c     write(6,*) 'nphys1: ',nphys1,nunkp1,nunkp2
 
      if (indprf1.ne.0) then
c        *** Variable number of dofs: probably ishape=7 for
c        *** integrated method. Use only first 2 dofs for copy
         call ini050(indprf1,'coefnew: indprf1')
         ipkprf1 = iniget(indprf1)
      endif

      if (indprf2.ne.0) then
         write(6,*) 'PERROR(cofcopy): not suited for variable numbers '
         write(6,*) '  of dofs in heat equation'
         write(6,*) 'indprf2: ',indprf2
         call instop 
      endif

      if (indprf1.eq.0) then
        if (nunkp1.ne.2.or.nunkp2.ne.1) then
           write(6,*) 'PERROR(cofcopy): number of dofs incorrect' 
           write(6,*) 'nunkp1 should be 2, but is: ',nunkp1
           write(6,*) 'nunkp2 should be 1, but is: ',nunkp2
           call instop
        endif
      endif

      call ini050(isol1(1),'coefnew: isol1')
      call ini050(isol2(1),'coefnew: isol2')

      ipusol1 = inidgt(isol1(1))
      ipusol2 = inidgt(isol2(1))

      if (mapneeded) then
c        *** first find the value of temperature in the
c        *** barycenter by interpolation
         call ini050(ipemap(3),'coefnew: ipemap(3)')
         call ini050(ipemap(5),'coefnew: ipemap(5)')
         iipemap = iniget(ipemap(3))
         irpemap = inidgt(ipemap(5))
c        *** nodal point numbers per element (mesh1)
         call ini050(kmesh1(17),'coefnew: kmesh1(17)')
c        *** coordinates of nodal points (mesh1)
         call ini050(kmesh1(23),'coefnew: kmesh1(23)')
         ikmeshc = iniget(kmesh1(17))
         ikmeshi = inidgt(kmesh1(23))
         call pemapinterpolate(1,ibuffr(ikmeshc),buffr(ikmeshi),
     v            buffr(ipusol2),ibuffr(iipemap),buffr(irpemap), 
     v            indprf1,ibuffr(ipkprf1),nunkp1,
     v            nelem1)      
         call printmap(ibuffr(iipemap),buffr(irpemap),
     v            buffr(ipusol2),buffr(ikmeshi),npoint1)
      endif

      if (.not.mapneeded) then
        call cofcop01(buffer(ipusol1),buffer(ipusol2),
     v              indprf1,ibuffr(ipkprf1),
     v              npoint1,nunkp1,nunkp2,factor) 
      else 
        call ini050(ipemap(3),'coefnew: ipemap(3)')
        iipemap = iniget(ipemap(3))
        call cofcop02(buffer(ipusol1),buffer(ipusol2),
     v              indprf1,ibuffr(ipkprf1),
     v              npoint1,nunkp1,nunkp2,factor,ibuffr(iipemap),
     v              buffr(irpemap))  
      endif

      return
      end

c *************************************************************
c *   COFCOP01
c *   Copy temperature from sol2 to sol1. Sol1 is used in 
c *   the specification of the coefficients.
c *   PvK 9906
c *************************************************************
      subroutine cofcop01(sol1,sol2,indprf1,kprobf1,
     v              npoint,nunkp1,nunkp2,factor)
      implicit none
      real*8 sol1(*),sol2(*)
      integer npoint,nunkp1,nunkp2,indprf1,kprobf1(*)
      real*8 factor
      integer i,j,j1,j2

      if (indprf1.eq.0) then
        do i=1,npoint
           j1 = (i-1)*nunkp1 + 1
           sol1(j1) = factor*sol2(i)
        enddo
      else 
        do i=1,npoint
           j1 = kprobf1(i) + 1
           sol1(j1) = factor*sol2(i)
        enddo
      endif

      return
      end
            
c *************************************************************
c *   COFCOP02
c *
c *   Copy temperature from sol2 (linear triangles) to sol1.
c *   Use MAP to find mapping of overlapping points and RMAP
c *   for the interpolated values in the barycenters. 
c *
c *   PvK 991019
c *************************************************************
      subroutine cofcop02(sol1,sol2,indprf1,kprobf1,
     v              npoint,nunkp1,nunkp2,factor,map,rmap)
      implicit none
      real*8 sol1(*),sol2(*),rmap(*)
      integer npoint,nunkp1,nunkp2,indprf1,kprobf1(*),map(*)
      real*8 factor,temp
      integer i,j,j1,j2,ip

      if (indprf1.eq.0) then
        do i=1,npoint
           ip = map(i)
           if (ip.lt.0) then
              temp = rmap(-ip)
           else
              temp = sol2(ip)
           endif
           j1 = (i-1)*nunkp1 + 1
           sol1(j1) = factor*temp
        enddo
      else 
        do i=1,npoint
           ip = map(i)
           if (ip.lt.0) then
              temp = rmap(-ip)
           else
              temp = sol2(ip)
           endif
           j1 = kprobf1(i) + 1
           sol1(j1) = factor*temp
        enddo
      endif

      return
      end
            
