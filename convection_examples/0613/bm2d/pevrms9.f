c ***************************************************
c *   PEVRMS
c *
c *   Calculates u*u + v*v per nodal point from the
c *   solution vector and places the result in array user.
c *   Used in calculation of vrms
c *
c *   PvK, 17-4-89/930107/970105
c *****************************************************************
      subroutine pevrms9(isol,kmesh,kprob,iuser,user)
      implicit none
      include 'pecof900.inc'
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (buffr(1),ibuffr(1))
      integer isol(*),kmesh(*),kprob(*),iuser(*)
      real*8 user(*)
      integer ipusol,inidgt,npoint

      npoint = kmesh(8)
      call ini050(isol(1),'pevrms9: isol')
      ipusol = inidgt(isol(1))
      call pevrm1(npoint,buffr(ipusol),user(6))

      iuser(6) = 7
      iuser(7) = intrule900
      iuser(8) = icoor900
      iuser(10) = 2001
      iuser(11) = 6

      return
      end
