include 'PvKmodules.inc'
program bm2d
use bmiter
implicit none
integer(kind=8),parameter :: NBUFDEF=100000000_8,NUM=100000_8
integer,allocatable,dimension(:) :: ibuffr
integer :: error
! *************************************************************
! *   BM
! *
! *   Stationary convection benchmark models
! *
! *   NBUFDEF = size of large buffer. Increase if Sepran complains
! *             about the need to increase size of IBUFFR.
! *   NUM     = defines size of user arrays. Increase for meshes
! *             with lots of nodal points.
! *
! *   Pseudocode:
! *         Start: read user input, create mesh and problem definition
! *         Until convergence
! *               Solve Stokes equations
! *               Solve Stationary Heat equation
! *               Provide some output (convergence, Nu, Vrms, cpu)
! *         Main output (plots, restart files)
! *
! *   PvK 080100
! *************************************************************
real(kind=8) :: user(NUM+5)
integer :: kmesh1(400),kmesh2(400),kprob1(400),kprob2(400)
integer :: intmt1(5),intmt2(5),matr1(5),matr2(5)
integer :: iuser(105),ipemap11(5),ipemap12(5)
integer :: isol1(5),isol2(5),islol1(5,2),islol2(5)
real :: second
real(kind=8) :: tstart,tnow

allocate(ibuffr(NBUFDEF),stat=error)
if (error /= 0) then
  write(6,*) 'PERROR(poisson1D): unable to allocate space'
  write(6,*) 'for ibuffr: NBUFDEF = ',NBUFDEF
  stop
endif

call sepcombf(ibuffr,ibuffr,NBUFDEF)

call bmstart(ibuffr,ibuffr,kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2, &
     matr1,matr2,iuser,user,isol1,isol2, &
     islol1,ipemap11,ipemap12,NBUFDEF,NUM)
niter=0
tstart = second()
do
   niter = niter +1
!  *****************************************************
!  **** STOKES EQUATIONS
!  *****************************************************
   call coefnewbf(ibuffr,ibuffr,1,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2, &
        iuser,user,islol1,ipemap12)
   call bmstokesbf(ibuffr,ibuffr,kmesh1,kprob1,intmt1,iuser,user, &
        isol1,islol1,matr1)

!  *****************************************************
!  **** HEAT EQUATION
!  *****************************************************
   call coefnewbf(ibuffr,ibuffr,2,kmesh1,kmesh2,kprob1,kprob2,isol1, &
        isol2,iuser,user,islol1,ipemap12)
   call bmheatbf(ibuffr,ibuffr,kmesh2,kprob2,intmt2,iuser,user, &
        isol2,islol2,matr2)
      
   call intermediate_output(ibuffr,ibuffr,kmesh1,kmesh2,kprob1,kprob2, &
        isol1,isol2,islol1,islol2,iuser,user,ipemap11)

   tnow = second()
   if (niter>=nitermax .or. dif < eps) exit
end do

call bmout(ibuffr,ibuffr,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2, &
     iuser,user,ipemap11)

if (allocated(ibuffr)) deallocate(ibuffr)

end program bm2d

