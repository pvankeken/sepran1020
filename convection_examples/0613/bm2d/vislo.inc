c *   *** vislo.inc
c *   *** logical used in viscosity description
c *   *** ivl : layer dependence
c *   *** ivt : temperature dependence
c *   *** ivn : non-Newtonian
      common /vislo/ ivl,ivt,ivn
      logical ivl,ivt,ivn
