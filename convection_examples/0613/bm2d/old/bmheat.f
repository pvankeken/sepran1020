c *************************************************************
c *   BMHEAT
c *
c *   Solve stationary heat equation
c *   iuser/user should contain information on the coefficients
c *   (see SP). Old fashioned build/solve routines. Might want
c *   to change this to build/solvel at some point...
c *   
c *   PvK 120490
c *************************************************************
      subroutine bmheat(ibuffr,buffr,kmesh,kprob,intmat,iuser,user,
     v        isol,islold,matr)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),iuser(*),islold(*),isol(*)
      integer matr(*)
      real*8 user(*)
      integer irhsd(5),ielhlp
      integer(kind=8) :: ibuffr(:)
      real(kind=8) :: buffr(:)

c     *** Make copy of old vector
      call copyvc(isol,islold)
c     *** Build system of equations
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
c     *** Solve with direct method (non-symmetric matrix)
      call solve(1,matr,isol,irhsd,intmat,kprob)

      return
      end

