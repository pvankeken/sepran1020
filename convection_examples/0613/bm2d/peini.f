c *************************************************************
c *   PEINI
c *************************************************************  
      subroutine peini(noreal,index,ifill,value)
      implicit none
      integer noreal,index,ifill
      real*8 value
      include 'SPcommon/cbuffr'
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1) 
      equivalence(buffr(1),ibuffr(1))
      integer iindex,inidgt
      

c     write(6,*) 'INI051: length array: ',noreal*intlen
      call ini051(index,noreal*intlen,'peini')
c     write(6,*) 'INI051: index: ',index
      
      if (ifill.eq.1) then
c        *** initialize the points with value VALUE
         iindex = inidgt(index)
         call peini01(buffr(iindex),noreal,value)
      endif

      return 
      end
 
      subroutine peini01(vector,noreal,value)
      implicit none
      real*8 vector(*),value
      integer noreal,i

      do i=1,noreal
         vector(i) = value
      enddo

      return
      end

