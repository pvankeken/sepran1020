! WRITBSBF_NETCDF
! Alternative for sepran writbs with output to netcdf file
! PvK 071713
subroutine writbsbf_netcdf(ibuffr,buffer,ivec,kmesh,kprob,fname)
implicit none
character(len=*) :: fname
integer :: ivec(:),kmesh(:),kprob(:)
integer :: ibuffr(:)
real(kind=8) :: buffer(:)

include 'netcdf.inc'

integer :: npoint,ndegfd,nunkp
integer :: iniget,inidgt,ipvec,i,dimids(2)
integer :: ncid,netcdf_err,ivec_id,nrdims,varid,extra_varid
integer :: iextra_id,nextra_length
integer,parameter :: NEXTRA_MAX=10
real(kind=8) :: extra_array(NEXTRA_MAX)

if (ivec(2) /= 110) then
  write(6,*) 'PERROR(writbs_netcdf): ivec is not a solution' 
  write(6,*) 'vector but of type: ',ivec(1)
  call instop 
endif

npoint=kmesh(8)
nunkp=kprob(4)
ndegfd=ivec(5)

call ini050(ivec(1),'writbs_netcdf:ivec')
ipvec=inidgt(ivec(1))

! create netcdf file
netcdf_err = nf_create(fname,NF_CLOBBER,ncid)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)

! Metadata
! length of data in vector
netcdf_err = nf_def_dim(ncid,'ndegfd',ndegfd,ivec_id)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
! length of extra parameters (time etc.
nextra_length = 1
if (nextra_length > NEXTRA_MAX) then
  write(6,*) 'PERROR(netcdf_writbs): nextra_length is too'
  write(6,*) 'large : ',nextra_length,NEXTRA_MAX
  call instop 
endif
netcdf_err = nf_def_dim(ncid,'nextra',nextra_length,iextra_id)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
nrdims=1
netcdf_err = nf_def_var(ncid,'vec',NF_DOUBLE,nrdims,ivec_id,varid)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
nrdims=1
netcdf_err = nf_def_var(ncid,'extra',NF_DOUBLE,nrdims,iextra_id,extra_varid)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
!     End of definition mode

netcdf_err = nf_enddef(ncid)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
    
! write data to file
! *** first write extra info (time etc.)
!extra_array(1)=t
netcdf_err = nf_put_var_double(ncid,extra_varid,extra_array)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
! *** then write vector
call ini060(ibuffr,ivec(1),'writbs_netcdf:ivec')
ipvec=inidgt(ivec(1))
netcdf_err = nf_put_var_double(ncid,varid,buffer(ipvec))
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)


! close file
netcdf_err = nf_close(ncid)
if (netcdf_err /= nf_noerr) call handle_err(netcdf_err)
      
end subroutine writbsbf_netcdf


