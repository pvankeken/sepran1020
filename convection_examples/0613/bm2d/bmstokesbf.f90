! *************************************************************
! *   BMSTOKES
! *
! *   Build equations for Stokes equations and solve
! *   Depending on value of isolmethod a direct or iterative 
! *   solver is used.
! *
! *   PvK 080100
! *
! *   PvK 071713 Fortran90
! *************************************************************
include 'PvKmodules.inc'
subroutine bmstokesbf(ibuffr,buffer,kmesh,kprob,intmat,iuser,user,isol,islol,matr)
use peparam
use c1visc
use solutionmethod
implicit none
integer :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: kmesh(:),kprob(:),intmat(:),iuser(:)
real(kind=8) :: user(*)
integer :: isol(:),islol(:,:),matr(:)
integer :: irhsd(5),matrback(5),iinbld(20),matrm(5)
integer :: inpsol(30),iread
real(kind=8) :: rinsol(10)
logical :: rebuild
integer :: ifirst,i
save irhsd,matrback,iinbld,matrm
save ifirst
data ifirst/0/

iinbld = 0

call copyvcbf(ibuffr,buffer,isol,islol(1,1))
if (itypv > 0 .or. ifirst==0) then
   ! *** Build system: A and f
   rebuild = .true.
   iinbld(1) = 10
   iinbld(2) = 1
   iinbld(10) = 2
   call buildbf(ibuffr,buffer,iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,islol,iuser,user)
else
!  *** Build only f (useful for isoviscous calculations)
   rebuild = .false.
   iinbld(1) = 10
   iinbld(2) = 2
   iinbld(10) = 2
   call buildbf(ibuffr,buffer,iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,islol,iuser,user)
endif

if (isolmethod == -1) then
  ! *** penalty function method, direct solution method
  call solvebf(ibuffr,buffer,1,matr,isol,irhsd,intmat,kprob)
else
  if (isolmethod.eq.0) then
     ! *** Penalty function method, direct, iposst=1
     inpsol(1) = 3 ! *** profile method
     inpsol(2) = 1 ! *** solution method
     inpsol(3) = 0
  else
     inpsol(1) = 14 
     inpsol(2) = 0           ! *** ipos
     inpsol(3) = isolmethod  ! *** solution_method 1=CG 2=CGS 3=GMRES 4=GMRESR
     inpsol(4) = ipreco      ! *** ipreco 1=diag 2=eisenstat 3=ILU
     inpsol(5) = maxiter
     inpsol(6) = iprint
     inpsol(7) = 0           ! *** matrix is symmetric(1) or not (0)?
     inpsol(8) = 20          ! *** dimension of Krylov space for GMRES
     inpsol(9) = 1           ! *** ISTART: 1=starts with given vector
     if (rebuild.and.keep==1) then
              inpsol(10)= 1  ! *** KEEP: 1=keep preconditioning matrix; 
     else if (keep == 1 .and. ifirst == 1) then
        ! *** use old matrix
        inpsol(10) = 2
     else
        inpsol(10) = 1
     endif
     inpsol(11)= 2
     inpsol(12)= 0
     inpsol(13)= 5     !          *** NTRUNC: for GMRESR: maximum number of search directions
     inpsol(14)= 5     !          *** NINNER: for GMRESR: maximum number of inner loop iters
     rinsol(1) = cgeps !          *** EPS: required accuracy for iteration process
  endif
  iread = -1
  call solvelbf(inpsol,rinsol,matr,isol,irhsd,intmat,kmesh,kprob,iread)
endif
ifirst=1

end subroutine bmstokesbf


