! *************************************************************
! *   BMHEAT
! *
! *   Solve stationary heat equation
! *   iuser/user should contain information on the coefficients
! *   (see SP). Old fashioned build/solve routines. Might want
! *   to change this to build/solvel at some point...
! *   
! *   PvK 120490
! *
! *   PvK 071713 Fortran90
! *************************************************************
subroutine bmheatbf(ibuffr,buffr,kmesh,kprob,intmat,iuser,user,isol,islold,matr)
implicit none
integer :: ibuffr(:)
real(kind=8) :: buffer(:)
integer :: kmesh(:),kprob(:),intmat(:),iuser(:),islold(:),isol(:)
integer :: matr(:)
real(kind=8) :: user(:)
integer :: irhsd(5),ielhlp

! *** Make copy of old vector
call copyvcbf(ibuffr,buffr,isol,islold)
! *** Build system of equations
call systm0bf(ibuffr,buffer,1,matr,intmat,kmesh,kprob,irhsd,isol,iuser,user,islold,ielhlp)
! *** Solve with direct method (non-symmetric matrix)
call solvebf(ibuffr,buffer,1,matr,isol,irhsd,intmat,kprob)

end subroutine bmheatbf
