      subroutine to0089 ( ibuffr, buffer, ictime, kmesh, kprob, intmat,
     +                    isolut, iinsol )
! ======================================================================
!
!        programmer    Guus Segal
!        version  4.4  date 21-10-2007 New call to solvelbf
!        version  4.3  date 23-03-2007 Extension with test_force
!        version  4.2  date 21-02-2007 Extension with keep matrix
!        version  4.1  date 30-08-2006 New definition of cpost5
!        version  4.0  date 19-08-2005 Split subroutine in parts
!        version  3.19 date 15-04-2005 Optional: print memory
!        version  3.18 date 28-02-2005 Fortran 90
!        version  3.17 date 14-01-2005 Layout
!        version  3.16 date 08-09-2004 Adaptation of reaction forces
!        version  3.15 date 30-12-2003 Change w.r.t. spectral element method
!
!   copyright (c) 1989-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!   TO0089 constructs the matrix and right-hand side for a linear
!   problem and solves it.
!   Depending on the value of ictime coefficients are read and stored.
!   TO0089 replaces the calls of the SEPRAN subroutines:
!      FILCOF: Fill coefficients for differential equation and
!              natural boundary conditions
!      BUILD : Build matrix and right-hand side
!      SOLVE : Solve system of linear equations
! **********************************************************************
!
!                       KEYWORDS
!
!     coefficient
!     matrix
!     right_hand_side
!     solve
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/comcons2'
      include 'SPcommon/cbuffr'
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cpost5'
      include 'SPcommon/cdebug'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ictime, kmesh(*), kprob(*), intmat(5,*), isolut(5,*),
     +        iinsol(*), ibuffr(*)
      double precision buffer(*)

!    buffer   i/o  Real buffer array in which all large data is stored
!    ibuffr   i/o  Integer buffer array in which all large data is stored
!    ictime    i   Indication if information must be read from standard
!                  input file ( <=0 ) or that information read before is
!                  used ( 1 )
!                  Other possibilities
!                      2 : The same as 0, however, the matrix and right-hand-
!                          side vector are not removed
!                      3 : The same as 1, however, the matrix and right-hand-
!                          side vector are not removed
!                      4 : The same as 3, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      5 : The same as 1, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      6 : The same as 2, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      7 : The same as 0, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!    iinsol    i   Input array defined by the user
!                  With this array the user may indicate which actions
!                  he wants to be performed by LINPRB
!                  See subroutine linprbbf
!                  If the is length is enlarged adapt:
!                  linprbinp, linprbbf, linsol, linstmbf, simvelsol,
!                  simfilllinsol
!    intmat    i   Standard SEPRAN array containing information of the
!                  structure of the large matrix. See the Programmer's Guide
!                  for a complete description
!    isolut   i/o  Standard SEPRAN array containing information of
!                  solution vectors and vectors of special structure.
!                  In the vector corresponding to isol(.,ivectr), the essential
!                  boundary conditions must have been filled. The other vectors
!                  are only used to create element matrices and vectors.
!                  At output the solution is filled in the vector corresponding
!                  to isolut(.,ivectr)
!    kmesh     i   Standard SEPRAN array containing information of the mesh
!                  See the Programmer's Guide for a complete description
!    kprob     i   Standard SEPRAN array containing information of the problem
!                  definition. See the Programmer's Guide for a complete
!                  description
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ninpsol, nrinsol
      parameter ( ninpsol=140, nrinsol=60 )
      double precision rinsol(nrinsol), timebefore
      integer nbufol, ipiusr, ipuser, irhsd(5,2), iseqintmat,
     +        matr(5,2), inpsol(ninpsol), i, iinbld(17), massmt(1),
     +        isubr, iseqcf, iseqsl, iprob, ivectr, maxvct, ictold,
     +        ivecreac, ivecfem, iprecfem, lenkmesh, icofiusr,
     +        icofuser, nch, iread, narbufsav, matr1(5), jmethod
      logical debug, direct_method
      save inpsol, rinsol, ictold, irhsd, matr, icofiusr, icofuser

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     direct_method  If true a direct method is applied in the solver
!     i              Counting variable
!     icofiusr       Value of iuser1 that has been used last
!     icofuser       Value of iuser2 that has been used last
!     ictold         Value of ictime at previous call
!     iinbld         Choice parameter for subroutine BUILD
!                    IINBLD must be filled as follows
!                     1 Number of entries of IINBLD that are filled by the user.
!                       If IINBLD(1) = 0, only defaults are used
!                     2 JCHOIS    general choice parameter, possibilities:
!                              1  Both the large matrix and large vector are
!                                 built.
!                              2  Only the large vector is built.
!                              3  The large matrix is built and the large vector
!                                 is made equal to zero.
!                                 If EFFBOUNCOND = 0 the large vector may be
!                                 unequal to zero, due to
!                                 non-homogeneous essential boundary conditions.
!                              5  The large matrix is not built;
!                                 the large vector is made equal to zero.
!                                 If EFFBOUNCOND = 0 the large vector may be
!                                 unequal to zero, due to
!                                 non-homogeneous essential boundary conditions.
!                             14  The large matrix is built;
!                                 no right-hand-side vector is made.
!                     3 EFFBOUNCOND   Indication whether the effect of essential
!                                 boundary  conditions must be taken into
!                                 account (0) or not (1).
!                     4 IMAS      Indication if the mass matrix must be computed
!                                 (>1) or not (0)
!                                 If IMAS=1, the mass matrix is stored as a
!                                 diagonal matrix, if IMAS=2 the mass matrix is
!                                 stored as a large matrix. The structure of
!                                 this matrix must be stored in INTMAT(.,2)
!                     5 IPREC     Indication if the old solution is stored in
!                                 ISLOLD (1) or not (0)
!                                 IPREC is not used if IINBLD(10)>0
!                     6 ALLELEMENTS  Indication if all standard element
!                                 sequence numbers are used (0) or not (1)
!                                 In that case the positions 16 to 15+NELGRP+
!                                 NUMNATBND   (NELGRP is number of element
!                                 groups and NUMNATBND the number of boundary
!                                 element groups) are considered as an array
!                                 IELHLP to be filled by the user in the
!                                 following way:
!                                 In IELHLP(IELGRP)
!                                 ( 1<=IELGRP<=NELGRP+NUMNATBND )
!                                 the user must store, whether elements with
!                                 standard element sequence number IELGRP (or
!                                 IELGRP = NELGRP + IBNGRP for boundary
!                                 elements) are added to the large matrix and
!                                 large vector or not.
!                                 When IELHLP(IELGRP) = 0 the elements with
!                                 standard element sequence number IELGRP are
!                                 skipped, when IELHLP(IELGRP) = 1 these
!                                 elements are added.
!                     7 ADDMATVEC    When ADDMATVEC = 0 the large matrix and
!                                 vector are cleared before the assembling
!                                 process.
!                                 When ADDMATVEC = 1 the right-hand-side vector
!                                 and the large matrix are not cleared,
!                                 but added to the already existing right-hand
!                                 side vector and large matrix.
!                                 Hence        RHSD := RHSD + DELTA  RHSD
!                                              MATRIX := MATRIX +  DELTA  MATRIX
!                     8  CLEARLASTRHSD  When CLEARLASTRHSD = 0 the last part of
!                                 the right-hand-side vector corresponding to
!                                 the boundary conditions is cleared
!                                 (standard situation)
!                                 when CLEARLASTRHSD = 1 the standard assembling
!                                 process is also applied to this last part of
!                                 the right-hand-side vector. For most
!                                 applications CLEARLASTRHSD = 0 suffices.
!                     9  IPROB    Problem definition number
!                    10  NUMOLD   Number of arrays that have been filled in
!                                 array ISLOLD
!                    11  ISPECTRAL Indication if a spectral element method is
!                                 used needing extra information (1) or not (0)
!                    12  NRIGHTHS Number of right-hand sides that must be
!                                 computed by subroutine BUILD in this call.
!                                 This parameter is only used if JCHOIS = 1 or 2
!                                 For all these vectors the other parameters in
!                                 IINBLD are used in exactly the same way.
!                                 The information about the right-hand sides is
!                                 stored in the following way:
!                                 f  refers to rhsd(.,i)  . = 1 (1) 5
!                                  i
!                                 Default value: 1
!                    13  ISEQIN   Sequence number of old solution in array
!                                 ISLOLD
!                    14  IDEF_COR indicates if defect correction should be used
!                                 0: no defect correction
!                                 1: defect correction
!                    15  ISUBDIVIDE indicates if quadratic elements must be
!                                 treated as a conglomerate of linear elements
!                                 (1) or as quadratice elements itself (0)
!     inpsol         Contains integer information concerning the solution
!                    process for subroutine SOLVEL.
!                    If iread = -2, inpsol is filled by SOLVEL, if iread = -1
!                    it is used
!     ipiusr         Starting address of IUSER in IBUFFR
!     iprecfem       Indicates if a FEM preconditioning must be applied
!                    in case of a spectral mesh
!                    Possible values:
!                    0: no fem preconditioning
!                    1: fem preconditioning
!     iprob          Problem sequence number
!     ipuser         Starting address of USER in BUFFER
!     iread          With this parameter it is indicated if array INPSOL is used
!                    in SOLVEL or that input is read from the standard INPUT
!                    file
!                    Possible values:
!                    0   The input is read from the standard input file
!                    >0  The input as read from the standard input file by
!                        subroutine SEPSTN is used. The value of iread
!                        defines the sequence number
!                    -1  No input is read, all information from array INPSOL
!                        is used
!                    -2  The input for SOLVEL is read from the standard input
!                        file. This input is stored in array INPSOL
!     irhsd          Standard SEPRAN array, containing information of the large
!                    vector
!                    In case of defect correction the right-hand side consists
!                    of two parts:
!                    irhsd(.,i) refers to the right-hand side corresponding to
!                               matr(.,i)
!     iseqcf         Sequence number with respect to the storage in array ISEPIN
!                    for coefficients
!     iseqintmat     Sequence number with respect to array intmat
!     iseqsl         Sequence number with respect to the storage in array ISEPIN
!                    for linear solver
!     isubr          Indicates the type of calling subroutine
!                    Possible values
!                    1:    LINSOL
!                    2:    LINSTM
!                    3:    LINPRB
!     ivecfem        Sequence number of FEM solution vector
!     ivecreac       Sequence number of the reaction force vector with respect
!                    to array isolut (if >0)
!                    If (<0) the right-hand side contribution is added
!     ivectr         Sequence number of the solution vector with respect to
!                    array isolut
!     jmethod        Indicates type of storage scheme for the matrix
!                    See Programmers Guide for a description
!     lenkmesh       Length of array KMESH
!     massmt         Parameter referring to mass matrix (not used here)
!     matr           Standard SEPRAN array, containing information of the large
!                    matrix
!                    In case of defect correction the matrix consists of two
!                    parts:
!                    matr(.,1) refers to the matrix to be inverted for the
!                              defect correction, for example the upwind matrix
!                    matr(.,2) refers to the matrix for the correction in the
!                              defect correction, for example the central
!                              difference matrix
!     matr1          Standard SEPRAN array to store information of the
!                    copy of the matrix
!     maxvct         Maximum number of vectors stored in isolut
!     narbufsav      Help variable to store the value of narbuf
!     nbufol         help variable to store the old value of nbuffr
!     nch            Number of characters in a string
!     ninpsol        Length of array inpsol
!     nrinsol        Length of array rinsol
!     rinsol         Array containing the real user input if iread=-1
!                    The storage of rinsol depends on iinsol.
!                    The following positions are used
!                    1. eps      Required accuracy for the iteration process
!                    2. droptol  Drop tolerance with respect to Y12M
!     timebefore     Time at entrance of subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     BUILDBF        assemble a system of equations
!     COPYMTBF       Copy matrix
!     COPYVCBF       Copy one SEPRAN vector into another one
!     ERCLMN         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERSETTIME      Get the CPU time at start of subroutine
!     INI066TEXT     delete temporary array
!     INIDAXPY       Overwrite double precision dy with double precision
!                    da*dx + dy
!     INIDELMATR     Delete all parts of a matrix from array IBUFFR
!     PRININ         print 1d integer array
!     SOLVELBF       Body of subroutine SOLVEL (linear solver)
!     TO0089CHECK    Perform some check with respect to subroutine to0089
!     TO0089CONSTS   Extract some constants from array iinsol
!     TO0089REAC     Compute the reaction forces in the linear system of
!                    equations
!     TO0089SEM      Compute SEM solution by FEM preconditioning
!     TO0089TEXT     Write some text information for subroutine to0089 (build)
!                    depending on output parameters
!     TO0089TEXT1    Write some text information for subroutine to0089 (solve)
!                    depending on output parameters
!     TOCOMPTSTFORCE Compute test force in outer boundary
!     TOFILLCOEFS    Fill coefficients at the end of the buffer array
!                    with starting addresses ipiusr and ipuser
! **********************************************************************
!
!                       I/O
!
!     All I/O is performed by subroutines ERLINS and READ03
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data ictold /-1/, icofiusr /0/, icofuser /0/
! ======================================================================
!
      call eropen ( 'to0089' )
      if ( itime>0 ) call ersettime ( timebefore )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0089'
         call prinin ( iinsol, 19, 'iinsol' )
         write(irefwr,1) 'ictime, ictold', ictime, ictold
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

!     --- Check input

      lenkmesh = 1
      iseqintmat = max(1,iinsol(19))  ! sequence number intmat

!     --- Perform some checks

      call to0089check ( kmesh, kprob, ictime, ictold, lenkmesh,
     +                   intmat(1,iseqintmat), iinsol )
      if ( ierror/=0 ) go to 1000

!     --- Clear arrays

      if ( iinsol(15)==1 .or. iinsol(15)==0 .and.
     +     ( ictime==2 .or. ictime==3 .or. ictime<=0) ) then
         if ( debug ) write(irefwr,*) 'Set matr to 0'
         do i = 1, 5
            matr(i,1) = 0
            matr(i,2) = 0
         end do
      else if ( debug ) then
         write(irefwr,*) 'Old matrix'
         call prinin ( matr(1,1), 5, 'matr' )
      end if

      if ( iinsol(16)==1 .or. iinsol(16)==0 .and.
     +     ( ictime==2 .or. ictime==3 .or. ictime<=0) ) then
         if ( debug ) write(irefwr,*) 'Set rhsd to 0'
         do i = 1, 5
            irhsd(i,1) = 0
            irhsd(i,2) = 0
         end do
      else if ( debug ) then
         write(irefwr,*) 'Old rhsd'
         call prinin ( irhsd(1,1), 5, 'irhsd' )
      end if

!     --- extract constants from iinsol and fill iinbld partly

      call to0089consts ( iinsol, isubr, iread, iseqcf, iseqsl,
     +                    iprob, ivectr, maxvct, ivecreac,
     +                    ivecfem, iprecfem, iinbld, ictime )

      inpsol(1) = ninpsol
      rinsol(1) = dble(nrinsol)
      nbufol = nbuffr
      narbufsav = narbuf

!     --- Fill coefficients in IUSER and USER

      call tofillcoefs ( ibuffr, buffer, kprob, kmesh, isubr,
     +                   iprob, iseqcf, ipiusr, ipuser, nbufol,
     +                   ictime, .true., icofiusr, icofuser )
      if ( ierror/=0 ) go to 1000

!     --- Write some text information for subroutine to0089
!         depending on output parameters

      call to0089text ( ivectr, iprecfem, ictime, nch )

!     --- Actual building of system of equations
!         In case of local transformations both the matrix and
!         right-hand side correspond to the local coordinate system

      call buildbf ( ibuffr, buffer, iinbld, matr, intmat(1,iseqintmat),
     +               kmesh(lenkmesh), kprob, irhsd, massmt,
     +               isolut(1,ivectr), isolut,
     +               ibuffr(ipiusr), buffer(ipuser) )
      if ( ierror/=0 ) go to 1000
      if ( debug ) call prinin ( matr(1,1), 5, 'matr/1' )

      if ( iinsol(18)>0 ) then

!     --- iinsol(18)>0, hence right-hand side is stored in isolut
!         we copy the vector into irhsd

         call copyvcbf ( ibuffr, buffer, isolut(1,iinsol(18)), irhsd )

      end if  ! ( iinsol(18)>0 )

!     --- Print some information and fill iread for solver

      call to0089text1 ( isubr, iseqsl, ictime, iprecfem, ivectr,
     +                   ivecreac, nch, iread )

      if ( ivecfem<0 ) then

!     --- Special case: ivecfem<0, add extra rhsd to rhsd
!         Both correspond to the local coordinate system

         call inidaxpy ( ibuffr, buffer, isolut(1,-ivecfem), irhsd,
     +                   1d0 )

      end if  ! ( ivecfem<0 )

      if ( iinsol(17)>0 ) then

!     --- iinsol(17)>0, hence test force must be computed
!         In case of a direct method we need the original matrix
!         so we copy this to matr1

         jmethod = mod(matr(1,1),100)
         direct_method = jmethod>0 .and. jmethod<5 .or. jmethod==19
         if ( direct_method ) then

!        --- Direct method, hence copy matrix

            do i = 1, 5
               matr1(i) = 0
            end do
            call copymtbf ( ibuffr, buffer, matr, matr1, kprob )
            if ( debug ) call prinin ( matr(1,1), 5, 'matr/2' )

         end if  ! ( jmethod>0 .and. jmethod<5 .or. jmethod==19 )

      end if  ! ( iinsol(17)>0 )

!     --- Solve system of linear equations
!         In case of a spectral method, the solution of the finite element
!         system is computed which is used later on as start vector for the
!         spectral preconditioning
!         In case of local transformations, the output solution and reaction
!         force are in the Cartesian coordinate system, not in the local one

      call solvelbf ( ibuffr, buffer, inpsol, rinsol, matr, isolut,
     +                irhsd, intmat, kmesh(lenkmesh), kprob, iread,
     +                ivectr, ivecreac, iseqintmat )
      if ( ierror/=0 ) go to 1000

!     --- Fill reaction forces, depending on iinsol

      call to0089reac ( ibuffr, buffer, matr, isolut,
     +                  intmat(1,iseqintmat), kprob, iinsol, ivectr,
     +                  iinbld, kmesh, irhsd,
     +                  ipiusr, ipuser, ivecreac, ivecfem )

      if ( ivecfem>0 ) then

!     --- Fill FEM vector

         write(irefwr,200) ivecfem
200      format( ' Compute vector ', i3, ' as FEM solution vector' )
         call copyvcbf ( ibuffr, buffer, isolut(1,ivectr),
     +                   isolut(1,ivecfem) )

      end if
      if ( ierror/=0 ) go to 1000

      if ( iprecfem>0 ) then

!     --- iprecfem>0, Compute SEM solution by FEM preconditioning

         call to0089sem ( ibuffr, buffer, matr, intmat(1,iseqintmat),
     +                    kmesh, kprob, irhsd, massmt, isolut,
     +                    ibuffr(ipiusr), buffer(ipuser), ivectr,
     +                    iseqsl )

      end if
      if ( ierror/=0 ) go to 1000

      if ( iinsol(17)>0 ) then

!     --- Special option to compute the test force
!         First copy solution array to temporary array and set all
!         boundary values to zero
!         Then replace the solution at the outer boundary by 0
!         Finally compute the test force

         if ( direct_method ) then

!        --- direct method is applied, use copy of matrix

            call tocomptstforce ( ibuffr, buffer, kmesh, kprob,
     +                            matr1, intmat(1,iseqintmat), isolut,
     +                            iinsol, ivectr, iinbld, irhsd, ipiusr,
     +                            ipuser, ivecfem )

!           --- Delete matrix

            call inidelmatr ( ibuffr, matr1 )

         else

!        --- Use original matrix

            call tocomptstforce ( ibuffr, buffer, kmesh, kprob,
     +                            matr, intmat(1,iseqintmat), isolut,
     +                            iinsol, ivectr, iinbld, irhsd, ipiusr,
     +                            ipuser, ivecfem )

         end if  ! ( direct_method )

      end if  ! ( iinsol(17)>0 )

!     --- Restore nbuffr

      nbuffr = nbufol
      narbuf = narbufsav

!     --- Delete arrays matr and irhsd

      ictold = ictime
      if ( iinsol(15)==2 .or. iinsol(15)==0 .and.
     +     ( ictime<2 .or. ictime>4 .and. ictime/=6 ) ) then
         if ( matr(2,1)/=0 ) call inidelmatr ( ibuffr, matr(1,1) )
         if ( debug ) write(irefwr,*) 'Destroy matrix'
      else
         ictold = 6
      end if  ! ( iinsol(15)==2 .or. iinsol(15)==0 .. )

      if ( iinsol(16)==2 .or. iinsol(16)==0 .and.
     +     ( ictime<2 .or. ictime>4 .and. ictime/=6 ) ) then
         if ( irhsd(1,1)>0 )
     +      call ini066text ( ibuffr, irhsd(1,1), 'rhsd' )
         if ( debug ) write(irefwr,*) 'Destroy right-hand side'
      end if  ! ( iinsol(16)==2 .or. iinsol(16)==0 .. )

1000  call erclmn ( 'to0089', timebefore )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0089'

      end if  ! ( debug )

      end
