      program mysepcomp
      call sepcom(0)
      end

      subroutine userout(kmesh2,kprob2,isol2,isequence,numvec)
      implicit none
      integer kmesh2(*),kprob2(*),isol2(5,numvec),isequence,numvec

      include 'SPcommon/cplot'
      include 'SPcommon/cmacht'

      integer i
      integer kmesh1(500),kprob1(500),iuser,iinput(100)
      integer iincommt(10),intmt1(5),iu1(2),iu2(2),idum
      integer isol1(5),islol1(5,2),irestart
      real*8 u1(2),u2(2)
      data kmesh1(1),kprob1(1)/2*500/
      
      irestart=0
 
      if (isequence.eq.1) then
         write(6,*) 'isequence = 1'
         write(6,*) 'userout: numvec',numvec
         write(6,*) 'userout: isol2',(isol2(i,1),i=1,5)


c        **** Set up Stokes problem outside of main sepran structure
         open(99,file='mesh1')
         call meshrd(-2,99,kmesh1)
         close(99)
       
         jmark=5
         call plotm2(0,kmesh1,iuser,10d0,1d0)
         call plotm2(0,kmesh2,iuser,10d0,1d0)

         call probdf(0,kprob1,kmesh1,iinput)

         iincommt(1) = 2
         iincommt(2) = 1
         call matstruc(iincommt,kmesh1,kprob1,intmt1)

         write(6,*) 'intmt1: ',(intmt1(i),i=1,5)

c        *** define initial conditions, boundary conditions  
         if (irestart.gt.0) then
c           *** Read from file
            namef2 = 'restart.back'
            call openf2(.false.)
            call readbs(1,isol1,kprob1)
            call readbs(2,isol2,kprob2)
         else
c           *** Make velocity 0
            iu1(1) = 2
            iu1(2) = 3
            call creavc(0,1,idum,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
         endif
         iu1(1) = 0
         iu1(2) = 0
         call creavc(0,1,idum,islol1(1,1),kmesh1,kprob1,iu1,u1,iu2,u2)
         call creavc(0,1,idum,islol1(1,2),kmesh1,kprob1,iu1,u1,iu2,u2)

c        *** Prescribe boundary conditions
         call presdf(kmesh1,kprob1,isol1)



      else
         write(6,*) 'isequence = 2'
      endif
 
      end


c *************************************************************
c *   FUNC
c *  
c *   Define local value of functions used to initiate vectors
c *************************************************************
      real*8 function func(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z
      real*8 pi
      parameter(pi=3.1415926)
      real*8 wavel,ampini
      common /inival/ wavel,ampini
      real*8 wpi
    
      if (ichois.eq.1) then
         wpi = pi/wavel
         func = ampini*sin(pi*y)*cos(wpi*x) + (1-y)
c        func = x + y
      else if (ichois.eq.2) then
	 func = -y
      else if (ichois.eq.3) then
	 func = x
      endif
  
      return
      end
      subroutine to0089sem ( ibuffr, buffer, matr, intmat, kmesh,
     +                       kprob, irhsd, massmt, isolut, iuser, user,
     +                       ivectr, iread )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 29-12-2003
!
!   copyright (c) 2003-2003  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Compute SEM solution by FEM preconditioning
!     reset intmat(2)
! **********************************************************************
!
!                       KEYWORDS
!
!     spectral
!     solve
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ibuffr(*), matr(*), intmat(*), kmesh(*), kprob(*),
     +        irhsd(*), massmt(*), isolut(5,*), iuser(*), ivectr, iread
      double precision buffer(*), user(*)

!     buffer        i/o   Buffer array
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     intmat         i    Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     iread          i    Defines how the input must be read
!                         Possible values:
!                         -1:  No input is required
!                          0:  All SEPRAN input has been read by
!                              subroutine SEPSTN until
!                              END_OF_SEPRAN_INPUT or
!                              end of file has been found
!                              This input is used
!                          1:  The input is read as described for
!                              EIGENVAL
!     irhsd          i    Standard SEPRAN array, containing information of the
!                         large vector
!     isolut        i/o   Standard SEPRAN array containing information of
!                         the solution vector (same as isol)
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     ivectr        i/o   standard sepran array denoting USOL or vector of
!                         special structure defined in nodal points
!     kmesh          i    Standard SEPRAN array containing information of the
!                         mesh
!     kprob          i    Standard SEPRAN array, containing information of
!                         the problem
!     massmt         i    Parameter referring to mass matrix (not used here)
!     matr           i    Standard SEPRAN array, containing information of the
!                         large matrix
!     user           i    Real user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ninpcp, nrincp,i
      parameter ( ninpcp=140, nrincp=60 )
      integer iinbld(4), inpcop(ninpcp), inpsol(2), nprob
      double precision rincop(nrincp), rinsol(1)
      logical debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     iinbld         User input array for subroutine BUILD
!     inpcop         Copy of integer input array with respect to linear
!                    solver extended with defaults
!     inpsol         Integer input array for subroutine PRPCGRADBF
!     ninpcp         Length of array inpcop
!     nprob          Number of various problems
!     nrincp         Length of array rincop
!     rincop         Copy of real input array with respect to linear
!                    solver extended with defaults
!     rinsol         Real input array for subroutine PRPCGRADBF
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     BUILDBF        assemble a system of equations
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     PRPCGRAD       finite-element preconditioned conjugate-gradient solver
!     PRSOLVEINPUT   Fill arrays iincop and rincop for subroutine solvelbf
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2482   iread has wrong value
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0089sem' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0089sem'

      end if
      if ( ierror.ne.0 ) go to 1000
      write(6,*) 'matr before build: ',(matr(i),i=1,5)
      iinbld(1) = 4
      iinbld(2) = 2
      iinbld(3) = 1
      iinbld(4) = 0
      call buildbf ( ibuffr, buffer, iinbld, matr, intmat, kmesh,
     +               kprob, irhsd, massmt, isolut(1,ivectr), isolut,
     +               iuser, user )
      write(6,*) 'matr: ',(matr(i),i=1,5)
      write(6,*) 'intmat: ',(intmat(i),i=1,5)
      write(6,*) 'npoint: ',kmesh(8)
      if ( ierror.ne.0 ) go to 1000

!     --- Fill input for linear solver into inpcop and rincop

      if ( iread.le.0 ) then

!     --- Only iread > 0, is permitted

         call errint ( iread, 1 )
         call errsub ( 2482, 1, 0, 0 )
         go to 1000

      end if

      nprob = kprob(40)
      call  prsolveinput ( ibuffr, buffer, iread, inpsol, ninpcp,
     +                     nrincp, rincop, rinsol, inpcop, nprob,
     +                     1 )

!     --- Copy specific input for pcgrad into inpsol and rinsol

      inpsol(1) = inpcop(38)
      inpsol(2) = inpcop(37)
      rinsol(1) = rincop(8)
      write(6,*) 'inpsol: ',inpsol(1),inpsol(2)
      write(6,*) 'rinsol: ',rinsol(1)
      write(6,*) 'inpcop: ',(inpcop(i),i=1,38)
      write(6,*) 'rincop: ',(rincop(i),i=1,8)
      call prpcgrad ( ibuffr, buffer, isolut(1,ivectr),
     +                isolut(1,ivectr), matr, kmesh, kprob, intmat,
     +                irhsd, iuser, user, inpsol, rinsol, inpcop,
     +                rincop )
      intmat(2) = 1102

1000  call erclos ( 'to0089sem' )

      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0089sem'

      end if

      end

      subroutine to0089 ( ibuffr, buffer, ictime, kmesh, kprob, intmat,
     +                    isolut, iinsol )
! ======================================================================
!
!        programmer    Guus Segal
!        version  4.4  date 21-10-2007 New call to solvelbf
!        version  4.3  date 23-03-2007 Extension with test_force
!        version  4.2  date 21-02-2007 Extension with keep matrix
!        version  4.1  date 30-08-2006 New definition of cpost5
!        version  4.0  date 19-08-2005 Split subroutine in parts
!        version  3.19 date 15-04-2005 Optional: print memory
!        version  3.18 date 28-02-2005 Fortran 90
!        version  3.17 date 14-01-2005 Layout
!        version  3.16 date 08-09-2004 Adaptation of reaction forces
!        version  3.15 date 30-12-2003 Change w.r.t. spectral element method
!
!   copyright (c) 1989-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!   TO0089 constructs the matrix and right-hand side for a linear
!   problem and solves it.
!   Depending on the value of ictime coefficients are read and stored.
!   TO0089 replaces the calls of the SEPRAN subroutines:
!      FILCOF: Fill coefficients for differential equation and
!              natural boundary conditions
!      BUILD : Build matrix and right-hand side
!      SOLVE : Solve system of linear equations
! **********************************************************************
!
!                       KEYWORDS
!
!     coefficient
!     matrix
!     right_hand_side
!     solve
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/comcons2'
      include 'SPcommon/cbuffr'
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cpost5'
      include 'SPcommon/cdebug'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ictime, kmesh(*), kprob(*), intmat(5,*), isolut(5,*),
     +        iinsol(*), ibuffr(*)
      double precision buffer(*)

!    buffer   i/o  Real buffer array in which all large data is stored
!    ibuffr   i/o  Integer buffer array in which all large data is stored
!    ictime    i   Indication if information must be read from standard
!                  input file ( <=0 ) or that information read before is
!                  used ( 1 )
!                  Other possibilities
!                      2 : The same as 0, however, the matrix and right-hand-
!                          side vector are not removed
!                      3 : The same as 1, however, the matrix and right-hand-
!                          side vector are not removed
!                      4 : The same as 3, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      5 : The same as 1, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      6 : The same as 2, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!                      7 : The same as 0, however, only the right-hand-side
!                          vector is filled, the matrix of a previous call
!                          is used.
!    iinsol    i   Input array defined by the user
!                  With this array the user may indicate which actions
!                  he wants to be performed by LINPRB
!                  See subroutine linprbbf
!                  If the is length is enlarged adapt:
!                  linprbinp, linprbbf, linsol, linstmbf, simvelsol,
!                  simfilllinsol
!    intmat    i   Standard SEPRAN array containing information of the
!                  structure of the large matrix. See the Programmer's Guide
!                  for a complete description
!    isolut   i/o  Standard SEPRAN array containing information of
!                  solution vectors and vectors of special structure.
!                  In the vector corresponding to isol(.,ivectr), the essential
!                  boundary conditions must have been filled. The other vectors
!                  are only used to create element matrices and vectors.
!                  At output the solution is filled in the vector corresponding
!                  to isolut(.,ivectr)
!    kmesh     i   Standard SEPRAN array containing information of the mesh
!                  See the Programmer's Guide for a complete description
!    kprob     i   Standard SEPRAN array containing information of the problem
!                  definition. See the Programmer's Guide for a complete
!                  description
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ninpsol, nrinsol
      parameter ( ninpsol=140, nrinsol=60 )
      double precision rinsol(nrinsol), timebefore
      integer nbufol, ipiusr, ipuser, irhsd(5,2), iseqintmat,
     +        matr(5,2), inpsol(ninpsol), i, iinbld(17), massmt(1),
     +        isubr, iseqcf, iseqsl, iprob, ivectr, maxvct, ictold,
     +        ivecreac, ivecfem, iprecfem, lenkmesh, icofiusr,
     +        icofuser, nch, iread, narbufsav, matr1(5), jmethod
      logical debug, direct_method
      save inpsol, rinsol, ictold, irhsd, matr, icofiusr, icofuser

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     direct_method  If true a direct method is applied in the solver
!     i              Counting variable
!     icofiusr       Value of iuser1 that has been used last
!     icofuser       Value of iuser2 that has been used last
!     ictold         Value of ictime at previous call
!     iinbld         Choice parameter for subroutine BUILD
!                    IINBLD must be filled as follows
!                     1 Number of entries of IINBLD that are filled by the user.
!                       If IINBLD(1) = 0, only defaults are used
!                     2 JCHOIS    general choice parameter, possibilities:
!                              1  Both the large matrix and large vector are
!                                 built.
!                              2  Only the large vector is built.
!                              3  The large matrix is built and the large vector
!                                 is made equal to zero.
!                                 If EFFBOUNCOND = 0 the large vector may be
!                                 unequal to zero, due to
!                                 non-homogeneous essential boundary conditions.
!                              5  The large matrix is not built;
!                                 the large vector is made equal to zero.
!                                 If EFFBOUNCOND = 0 the large vector may be
!                                 unequal to zero, due to
!                                 non-homogeneous essential boundary conditions.
!                             14  The large matrix is built;
!                                 no right-hand-side vector is made.
!                     3 EFFBOUNCOND   Indication whether the effect of essential
!                                 boundary  conditions must be taken into
!                                 account (0) or not (1).
!                     4 IMAS      Indication if the mass matrix must be computed
!                                 (>1) or not (0)
!                                 If IMAS=1, the mass matrix is stored as a
!                                 diagonal matrix, if IMAS=2 the mass matrix is
!                                 stored as a large matrix. The structure of
!                                 this matrix must be stored in INTMAT(.,2)
!                     5 IPREC     Indication if the old solution is stored in
!                                 ISLOLD (1) or not (0)
!                                 IPREC is not used if IINBLD(10)>0
!                     6 ALLELEMENTS  Indication if all standard element
!                                 sequence numbers are used (0) or not (1)
!                                 In that case the positions 16 to 15+NELGRP+
!                                 NUMNATBND   (NELGRP is number of element
!                                 groups and NUMNATBND the number of boundary
!                                 element groups) are considered as an array
!                                 IELHLP to be filled by the user in the
!                                 following way:
!                                 In IELHLP(IELGRP)
!                                 ( 1<=IELGRP<=NELGRP+NUMNATBND )
!                                 the user must store, whether elements with
!                                 standard element sequence number IELGRP (or
!                                 IELGRP = NELGRP + IBNGRP for boundary
!                                 elements) are added to the large matrix and
!                                 large vector or not.
!                                 When IELHLP(IELGRP) = 0 the elements with
!                                 standard element sequence number IELGRP are
!                                 skipped, when IELHLP(IELGRP) = 1 these
!                                 elements are added.
!                     7 ADDMATVEC    When ADDMATVEC = 0 the large matrix and
!                                 vector are cleared before the assembling
!                                 process.
!                                 When ADDMATVEC = 1 the right-hand-side vector
!                                 and the large matrix are not cleared,
!                                 but added to the already existing right-hand
!                                 side vector and large matrix.
!                                 Hence        RHSD := RHSD + DELTA  RHSD
!                                              MATRIX := MATRIX +  DELTA  MATRIX
!                     8  CLEARLASTRHSD  When CLEARLASTRHSD = 0 the last part of
!                                 the right-hand-side vector corresponding to
!                                 the boundary conditions is cleared
!                                 (standard situation)
!                                 when CLEARLASTRHSD = 1 the standard assembling
!                                 process is also applied to this last part of
!                                 the right-hand-side vector. For most
!                                 applications CLEARLASTRHSD = 0 suffices.
!                     9  IPROB    Problem definition number
!                    10  NUMOLD   Number of arrays that have been filled in
!                                 array ISLOLD
!                    11  ISPECTRAL Indication if a spectral element method is
!                                 used needing extra information (1) or not (0)
!                    12  NRIGHTHS Number of right-hand sides that must be
!                                 computed by subroutine BUILD in this call.
!                                 This parameter is only used if JCHOIS = 1 or 2
!                                 For all these vectors the other parameters in
!                                 IINBLD are used in exactly the same way.
!                                 The information about the right-hand sides is
!                                 stored in the following way:
!                                 f  refers to rhsd(.,i)  . = 1 (1) 5
!                                  i
!                                 Default value: 1
!                    13  ISEQIN   Sequence number of old solution in array
!                                 ISLOLD
!                    14  IDEF_COR indicates if defect correction should be used
!                                 0: no defect correction
!                                 1: defect correction
!                    15  ISUBDIVIDE indicates if quadratic elements must be
!                                 treated as a conglomerate of linear elements
!                                 (1) or as quadratice elements itself (0)
!     inpsol         Contains integer information concerning the solution
!                    process for subroutine SOLVEL.
!                    If iread = -2, inpsol is filled by SOLVEL, if iread = -1
!                    it is used
!     ipiusr         Starting address of IUSER in IBUFFR
!     iprecfem       Indicates if a FEM preconditioning must be applied
!                    in case of a spectral mesh
!                    Possible values:
!                    0: no fem preconditioning
!                    1: fem preconditioning
!     iprob          Problem sequence number
!     ipuser         Starting address of USER in BUFFER
!     iread          With this parameter it is indicated if array INPSOL is used
!                    in SOLVEL or that input is read from the standard INPUT
!                    file
!                    Possible values:
!                    0   The input is read from the standard input file
!                    >0  The input as read from the standard input file by
!                        subroutine SEPSTN is used. The value of iread
!                        defines the sequence number
!                    -1  No input is read, all information from array INPSOL
!                        is used
!                    -2  The input for SOLVEL is read from the standard input
!                        file. This input is stored in array INPSOL
!     irhsd          Standard SEPRAN array, containing information of the large
!                    vector
!                    In case of defect correction the right-hand side consists
!                    of two parts:
!                    irhsd(.,i) refers to the right-hand side corresponding to
!                               matr(.,i)
!     iseqcf         Sequence number with respect to the storage in array ISEPIN
!                    for coefficients
!     iseqintmat     Sequence number with respect to array intmat
!     iseqsl         Sequence number with respect to the storage in array ISEPIN
!                    for linear solver
!     isubr          Indicates the type of calling subroutine
!                    Possible values
!                    1:    LINSOL
!                    2:    LINSTM
!                    3:    LINPRB
!     ivecfem        Sequence number of FEM solution vector
!     ivecreac       Sequence number of the reaction force vector with respect
!                    to array isolut (if >0)
!                    If (<0) the right-hand side contribution is added
!     ivectr         Sequence number of the solution vector with respect to
!                    array isolut
!     jmethod        Indicates type of storage scheme for the matrix
!                    See Programmers Guide for a description
!     lenkmesh       Length of array KMESH
!     massmt         Parameter referring to mass matrix (not used here)
!     matr           Standard SEPRAN array, containing information of the large
!                    matrix
!                    In case of defect correction the matrix consists of two
!                    parts:
!                    matr(.,1) refers to the matrix to be inverted for the
!                              defect correction, for example the upwind matrix
!                    matr(.,2) refers to the matrix for the correction in the
!                              defect correction, for example the central
!                              difference matrix
!     matr1          Standard SEPRAN array to store information of the
!                    copy of the matrix
!     maxvct         Maximum number of vectors stored in isolut
!     narbufsav      Help variable to store the value of narbuf
!     nbufol         help variable to store the old value of nbuffr
!     nch            Number of characters in a string
!     ninpsol        Length of array inpsol
!     nrinsol        Length of array rinsol
!     rinsol         Array containing the real user input if iread=-1
!                    The storage of rinsol depends on iinsol.
!                    The following positions are used
!                    1. eps      Required accuracy for the iteration process
!                    2. droptol  Drop tolerance with respect to Y12M
!     timebefore     Time at entrance of subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     BUILDBF        assemble a system of equations
!     COPYMTBF       Copy matrix
!     COPYVCBF       Copy one SEPRAN vector into another one
!     ERCLMN         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERSETTIME      Get the CPU time at start of subroutine
!     INI066TEXT     delete temporary array
!     INIDAXPY       Overwrite double precision dy with double precision
!                    da*dx + dy
!     INIDELMATR     Delete all parts of a matrix from array IBUFFR
!     PRININ         print 1d integer array
!     SOLVELBF       Body of subroutine SOLVEL (linear solver)
!     TO0089CHECK    Perform some check with respect to subroutine to0089
!     TO0089CONSTS   Extract some constants from array iinsol
!     TO0089REAC     Compute the reaction forces in the linear system of
!                    equations
!     TO0089SEM      Compute SEM solution by FEM preconditioning
!     TO0089TEXT     Write some text information for subroutine to0089 (build)
!                    depending on output parameters
!     TO0089TEXT1    Write some text information for subroutine to0089 (solve)
!                    depending on output parameters
!     TOCOMPTSTFORCE Compute test force in outer boundary
!     TOFILLCOEFS    Fill coefficients at the end of the buffer array
!                    with starting addresses ipiusr and ipuser
! **********************************************************************
!
!                       I/O
!
!     All I/O is performed by subroutines ERLINS and READ03
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data ictold /-1/, icofiusr /0/, icofuser /0/
! ======================================================================
!
      call eropen ( 'to0089' )
      if ( itime>0 ) call ersettime ( timebefore )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0089'
         call prinin ( iinsol, 19, 'iinsol' )
         write(irefwr,1) 'ictime, ictold', ictime, ictold
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

!     --- Check input

      lenkmesh = 1
      iseqintmat = max(1,iinsol(19))  ! sequence number intmat

!     --- Perform some checks

      call to0089check ( kmesh, kprob, ictime, ictold, lenkmesh,
     +                   intmat(1,iseqintmat), iinsol )
      if ( ierror/=0 ) go to 1000

!     --- Clear arrays

      if ( iinsol(15)==1 .or. iinsol(15)==0 .and.
     +     ( ictime==2 .or. ictime==3 .or. ictime<=0) ) then
         if ( debug ) write(irefwr,*) 'Set matr to 0'
         do i = 1, 5
            matr(i,1) = 0
            matr(i,2) = 0
         end do
      else if ( debug ) then
         write(irefwr,*) 'Old matrix'
         call prinin ( matr(1,1), 5, 'matr' )
      end if

      if ( iinsol(16)==1 .or. iinsol(16)==0 .and.
     +     ( ictime==2 .or. ictime==3 .or. ictime<=0) ) then
         if ( debug ) write(irefwr,*) 'Set rhsd to 0'
         do i = 1, 5
            irhsd(i,1) = 0
            irhsd(i,2) = 0
         end do
      else if ( debug ) then
         write(irefwr,*) 'Old rhsd'
         call prinin ( irhsd(1,1), 5, 'irhsd' )
      end if

!     --- extract constants from iinsol and fill iinbld partly

      call to0089consts ( iinsol, isubr, iread, iseqcf, iseqsl,
     +                    iprob, ivectr, maxvct, ivecreac,
     +                    ivecfem, iprecfem, iinbld, ictime )

      inpsol(1) = ninpsol
      rinsol(1) = dble(nrinsol)
      nbufol = nbuffr
      narbufsav = narbuf

!     --- Fill coefficients in IUSER and USER

      call tofillcoefs ( ibuffr, buffer, kprob, kmesh, isubr,
     +                   iprob, iseqcf, ipiusr, ipuser, nbufol,
     +                   ictime, .true., icofiusr, icofuser )
      if ( ierror/=0 ) go to 1000

!     --- Write some text information for subroutine to0089
!         depending on output parameters

      call to0089text ( ivectr, iprecfem, ictime, nch )

!     --- Actual building of system of equations
!         In case of local transformations both the matrix and
!         right-hand side correspond to the local coordinate system

      write(6,*) 'to0089/iinbuild: ',(iinbld(i),i=1,iinbld(1))
      write(6,*) 'lenkmesh: ',lenkmesh,iseqintmat
      call buildbf ( ibuffr, buffer, iinbld, matr, intmat(1,iseqintmat),
     +               kmesh(lenkmesh), kprob, irhsd, massmt,
     +               isolut(1,ivectr), isolut,
     +               ibuffr(ipiusr), buffer(ipuser) )
      if ( ierror/=0 ) go to 1000
      if ( debug ) call prinin ( matr(1,1), 5, 'matr/1' )

      if ( iinsol(18)>0 ) then

!     --- iinsol(18)>0, hence right-hand side is stored in isolut
!         we copy the vector into irhsd

         call copyvcbf ( ibuffr, buffer, isolut(1,iinsol(18)), irhsd )

      end if  ! ( iinsol(18)>0 )

!     --- Print some information and fill iread for solver

      call to0089text1 ( isubr, iseqsl, ictime, iprecfem, ivectr,
     +                   ivecreac, nch, iread )

      if ( ivecfem<0 ) then

!     --- Special case: ivecfem<0, add extra rhsd to rhsd
!         Both correspond to the local coordinate system

         call inidaxpy ( ibuffr, buffer, isolut(1,-ivecfem), irhsd,
     +                   1d0 )

      end if  ! ( ivecfem<0 )

      if ( iinsol(17)>0 ) then

!     --- iinsol(17)>0, hence test force must be computed
!         In case of a direct method we need the original matrix
!         so we copy this to matr1

         jmethod = mod(matr(1,1),100)
         direct_method = jmethod>0 .and. jmethod<5 .or. jmethod==19
         if ( direct_method ) then

!        --- Direct method, hence copy matrix

            do i = 1, 5
               matr1(i) = 0
            end do
            call copymtbf ( ibuffr, buffer, matr, matr1, kprob )
            if ( debug ) call prinin ( matr(1,1), 5, 'matr/2' )

         end if  ! ( jmethod>0 .and. jmethod<5 .or. jmethod==19 )

      end if  ! ( iinsol(17)>0 )

!     --- Solve system of linear equations
!         In case of a spectral method, the solution of the finite element
!         system is computed which is used later on as start vector for the
!         spectral preconditioning
!         In case of local transformations, the output solution and reaction
!         force are in the Cartesian coordinate system, not in the local one

      call solvelbf ( ibuffr, buffer, inpsol, rinsol, matr, isolut,
     +                irhsd, intmat, kmesh(lenkmesh), kprob, iread,
     +                ivectr, ivecreac, iseqintmat )
      if ( ierror/=0 ) go to 1000

!     --- Fill reaction forces, depending on iinsol

      call to0089reac ( ibuffr, buffer, matr, isolut,
     +                  intmat(1,iseqintmat), kprob, iinsol, ivectr,
     +                  iinbld, kmesh, irhsd,
     +                  ipiusr, ipuser, ivecreac, ivecfem )

      if ( ivecfem>0 ) then

!     --- Fill FEM vector

         write(irefwr,200) ivecfem
200      format( ' Compute vector ', i3, ' as FEM solution vector' )
         call copyvcbf ( ibuffr, buffer, isolut(1,ivectr),
     +                   isolut(1,ivecfem) )

      end if
      if ( ierror/=0 ) go to 1000

      if ( iprecfem>0 ) then

!     --- iprecfem>0, Compute SEM solution by FEM preconditioning

         call to0089sem ( ibuffr, buffer, matr, intmat(1,iseqintmat),
     +                    kmesh, kprob, irhsd, massmt, isolut,
     +                    ibuffr(ipiusr), buffer(ipuser), ivectr,
     +                    iseqsl )

      end if
      if ( ierror/=0 ) go to 1000

      if ( iinsol(17)>0 ) then

!     --- Special option to compute the test force
!         First copy solution array to temporary array and set all
!         boundary values to zero
!         Then replace the solution at the outer boundary by 0
!         Finally compute the test force

         if ( direct_method ) then

!        --- direct method is applied, use copy of matrix

            call tocomptstforce ( ibuffr, buffer, kmesh, kprob,
     +                            matr1, intmat(1,iseqintmat), isolut,
     +                            iinsol, ivectr, iinbld, irhsd, ipiusr,
     +                            ipuser, ivecfem )

!           --- Delete matrix

            call inidelmatr ( ibuffr, matr1 )

         else

!        --- Use original matrix

            call tocomptstforce ( ibuffr, buffer, kmesh, kprob,
     +                            matr, intmat(1,iseqintmat), isolut,
     +                            iinsol, ivectr, iinbld, irhsd, ipiusr,
     +                            ipuser, ivecfem )

         end if  ! ( direct_method )

      end if  ! ( iinsol(17)>0 )

!     --- Restore nbuffr

      nbuffr = nbufol
      narbuf = narbufsav

!     --- Delete arrays matr and irhsd

      ictold = ictime
      if ( iinsol(15)==2 .or. iinsol(15)==0 .and.
     +     ( ictime<2 .or. ictime>4 .and. ictime/=6 ) ) then
         if ( matr(2,1)/=0 ) call inidelmatr ( ibuffr, matr(1,1) )
         if ( debug ) write(irefwr,*) 'Destroy matrix'
      else
         ictold = 6
      end if  ! ( iinsol(15)==2 .or. iinsol(15)==0 .. )

      if ( iinsol(16)==2 .or. iinsol(16)==0 .and.
     +     ( ictime<2 .or. ictime>4 .and. ictime/=6 ) ) then
         if ( irhsd(1,1)>0 )
     +      call ini066text ( ibuffr, irhsd(1,1), 'rhsd' )
         if ( debug ) write(irefwr,*) 'Destroy right-hand side'
      end if  ! ( iinsol(16)==2 .or. iinsol(16)==0 .. )

1000  call erclmn ( 'to0089', timebefore )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0089'

      end if  ! ( debug )

      end
      subroutine incommat ( ibuffr, iincommt, kmesh, kprob, intmat )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.2  date 08-10-2007 Extension with decoupled unknowns
!        version  3.1  date 31-08-2007 Extension with jmethod 24-27
!        version  3.0  date 30-04-2003 New body
!        version  2.17 date 11-04-2003 Debug statements
!        version  2.16 date 02-01-2003 Create kmesh part e if it does not exist
!        version  2.15 date 11-10-2002 Correction to prevent integer overflow
!        version  2.14 date 07-06-2002 Initialize isumnp
!        version  2.13 date 18-03-2002 Call to inirst
!        version  2.12 date 07-11-2001 Correction if no curves are available
!        version  2.11 date 01-10-2000 Initialize ipkprp
!        version  2.10 date 13-07-2000 New call to ini010/incm0x
!        version  2.9  date 31-03-2000 Extension for fictitious unknowns
!        version  2.8  date 17-03-2000 Correction for complex matrices
!        version  2.7  date 01-04-1999 New call to iniext
!        version  2.6  date 10-01-1999 New calls to ini011/14
!        version  2.5  date 21-12-1998 New calls to incm01/02
!        version  2.4  date 08-10-1998 Extension for Block ILU
!
!   copyright (c) 1982-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     compute structure of the large matrix
!     Actual body of commat and matstruc
! **********************************************************************
!
!                       KEYWORDS
!
!     matrix_structure
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iincommt(*), kmesh(*), kprob(*), intmat(*), ibuffr(*)

!     ibuffr    i/o  Integer buffer array in which all large data is stored
!     iincommt   i    Integer input array containing information concerning
!                     the structure of the large matrix
!                     See subroutine read00 for a description
!     intmat     o    Standard SEPRAN array containing information of
!                     the structure of the matrices
!     kmesh      i    Standard SEPRAN array containing information of the mesh
!     kprob      i    Standard SEPRAN array containing information of the
!                     problem
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer i, invmat, ipintm, ipkprb, iprob, isum, jmetod,
     +        kamata, jmethod, ipint1, ipin2b,
     +        maxmat, nbound, npboun, npoint, nrusol, isumnp,
     +        nusol, iprint, ibcmat, invmtt, ipin1a, ipin1b, ipin2a,
     +        isumbc, nblocks, lenim2, iblkmeth
      logical split, debug, fillbouncond

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     fillbouncond   If true the part realted to the boundary conditions
!                    must be filled
!     i              Counting variable
!     ibcmat         Indicates whether the matrices necessary for computation
!                    of the reaction forces (S_pu and S_pp (see maver))
!                    are calculated
!     iblkmeth       Type of block storage method
!                    0: none
!                    1: blocks per node
!     invmat         Indicates if the approximate inverse of the matrix has been
!                    built by Kaasschieters method (1) or not (0)
!     invmtt         Temporary for invmat
!     ipin1a         Starting address of array imat part 1a in IBUFFR
!     ipin1b         Starting address of array imat part 1b in IBUFFR
!     ipin2a         Starting address of array imat part 2a in IBUFFR
!     ipin2b         Starting address of array imat part 2b in IBUFFR
!     ipint1         Starting position of first part of array intmat in array
!                    IBUFFR
!     ipintm         Starting address of array imat part 1 in IBUFFR
!     ipkprb         Starting address of array KPROB for actual problem number
!     iprint         See iincommt(3)
!     iprob          Actual problem number
!     isum           length of array imat part 2a
!     isumbc         length of array imat part 2b
!     isumnp         length of array imat part 2a minus the periodical boundary
!                    conditions part
!     jmethod        See input, parameter ichois
!     jmetod         Indicates type of storage scheme for the matrix
!                    Possible values:
!                     1:  Symmetric profile matrix
!                     2:  Non-symmetric profile matrix
!                     3:  Symmetric complex profile matrix
!                     4:  Non-symmetric complex profile matrix
!                     5:  Symmetric compact storage (Real matrix)
!                     6:  Non-symmetric compact storage (Real matrix)
!                     7:  Symmetric compact storage (Complex matrix)
!                     8:  Non-symmetric compact storage (Complex matrix)
!                     9:  Non-symmetric structure compact storage (Real matrix)
!                    10:  Non-symmetric structure compact storage
!                         (Complex matrix)
!                    11:  Compact storage for vectorprocessors (Real matrix)
!                    12:  Compact storage for vectorprocessors (Complex matrix)
!     kamata         Help parameter to store the number of entries in a part of
!                    the large matrix
!     lenim2         length of the array imat part 2
!     maxmat         Help parameter to store the number of entries in the large
!                    matrix
!     nblocks        Number of blocks in the matrix
!     nbound         Number of essential boundary conditions
!     npboun         Number of periodical boundary conditions
!     npoint         Number of nodal points
!     nrusol         Number of free unknowns
!     nusol          Total number of unknowns
!     split          If true splitting of the matrix is allowed
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer inigettext, iniprb

!     CHRIK          Special renumbering algorithm to be used in connection
!                    with the preconditioning of Rik Kaasschieter
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     INCOMMATBOUN   Fill parts of array intmat with respect to
!                    boundary conditions
!     INCOMMATCOMP   Fill structure of matrix in case of a compact matrix
!     INCOMMATPROF   Fill structure of matrix in case of a profile matrix
!     INI060TEXT     Reactivate existing array in IBUFFR
!     INIGETTEXT     Compute pointer of array in IBUFFR
!     INIPRB         Get value of ipkprb as function of iprob
!     PR0004         Print INTMAT corresponding to internal matrix
!     PR0005         Print INTMAT corresponding to boundary conditions
!     PRINTMATSIMPLE Print the structure of the various matrices in case of a
!                    compact storage for the SIMPLE method
!     PRNI21         write differences of integer array with special format
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
!      26   Array KPROB has not been filled
!      75   Array KMESH has not been filled
!    2366   The computed length of the matrix is larger than the largest
!           integer
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
!     --- Initialize some parameters

      write(6,*) 'COMMAT begin: jmetod=',jmetod
      call eropen ( 'incommat' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from incommat'

      end if

!     --- Standard tests

      if ( kmesh(2)/=100 ) then
         call errint ( kmesh(2), 1 )
         call errsub ( 75, 1, 0, 0 )
      end if

      jmethod = iincommt(2)
      iprint  = iincommt(3)
      invmtt  = iincommt(4)
      iprob   = iincommt(5)

!     --- Split of invmat (0 or 1) and ibcmat (0 or 1)

      iblkmeth = invmtt/4
      invmat   = mod(invmtt,2)
      ibcmat   = (invmtt-4*iblkmeth)/2
      invmtt   = invmtt-4*iblkmeth
      nblocks = 0
      isumnp = 0
      if ( jmethod>=31 ) then

!     --- Splitting of the matrix is not allowed

         split = .false.
         jmethod = jmethod - 30

      else

!     --- Splitting of the matrix is allowed

         split = .true.

      end if
      if ( intmat(2)/=102 ) then
         do i = 1, 5
            intmat(i) = 0
         end do
      end if

      ipkprb = iniprb(iprob,kprob)

      if ( kprob(ipkprb+2)/=101 ) then
         call errint ( kprob(ipkprb+2), 1 )
         call errsub ( 26, 1, 0, 0 )
      end if

      if ( invmat==1 ) then

!     --- invmat = 1,  Special method of Rik Kaasschieter
!                      Renumber nodes

         call chrik ( ibuffr, kmesh, kprob(ipkprb+1) )

      end if
      nusol = kprob(ipkprb+5)

      if ( jmethod>=20 .and. jmethod<24 ) then

!     --- 20<=jmethod>=23  special situation  full matrices

         intmat(1) = jmethod
         intmat(2) = 102
         intmat(3) = 0
         intmat(4) = 0
         if ( jmethod<22 ) then
            intmat(5) = nusol**2
            if ( intmat(5)/nusol/=nusol ) then

!           --- This is not possible, hence we have integer overflow

               call errsub ( 2366, 0, 0, 0 )
               go to 1000

            end if

         else

            intmat(5) = 2*nusol**2
            if ( intmat(5)/nusol/=2*nusol ) then

!           --- This is not possible, hence we have integer overflow

               call errsub ( 2366, 0, 0, 0 )
               go to 1000

            end if

         end if

      else

!     --- jmethod<20   Standard situation

         isum = 0
         isumbc = 0
         isumnp = 0
         nbound = kprob(ipkprb+10)
         npboun = kprob(ipkprb+34)
         nrusol = nusol-nbound-npboun
         npoint = kmesh(8)
         jmetod = abs(jmethod)
         intmat(1) = jmetod+100*invmtt+(iprob-1)*1000
         fillbouncond = iincommt(12)==0 .and. nbound>0 .or. npboun>0

         if ( debug ) then
            write(irefwr,*) 'nbound, npboun, nusol, nrusol, npoint',
     +                       nbound, npboun, nusol, nrusol, npoint
            write(irefwr,*) 'jmetod, invmtt, iprob ',
     +                       jmetod, invmtt, iprob

         end if
         if ( fillbouncond ) then

!        --- Fill information with respect to boundary conditions

            call incommatboun ( ibuffr, kmesh, kprob(ipkprb+1), intmat,
     +                          ibcmat, isum, isumbc, isumnp )

         end if


         if ( jmetod<=4 .or. jmetod==19 ) then

!        --- jmetod  =  1, 2, 3, 4, 19  profile method

            call incommatprof ( ibuffr, jmetod, kmesh, kprob(ipkprb+1),
     +                          intmat, kamata, iprint, split, maxmat )

         else if ( jmetod<=16 .or. jmetod>=24 .and. jmetod<=27 ) then

!        --- jmetod  =  5 ... 16, 24 .. 27, compact storage

            call incommatcomp ( ibuffr, jmetod, kmesh, kprob(ipkprb+1),
     +                          intmat, kamata, maxmat, iincommt,
     +                          iblkmeth, lenim2, nblocks )

         end if  ! ( jmetod<=16 .or. jmetod>=24 .and. jmetod<=27 )
         write(6,*) 'COMMAT: jmetod=',jmetod,ipkprb,kamata

      end if
      if ( ierror/=0 ) go to 1000
      intmat(5) = kamata+isum+ibcmat*(isumnp+isumbc)
      intmat(2) = 102
      if ( ioutp>=1 .or. mod(iprint,10)>=1 ) then

!     --- ioutp>1   print size of large matrix

         maxmat = maxmat+isum
         if ( jmetod==3 .or. jmetod==4 .or. jmetod==8 .or.
     +        jmetod==10 .or. jmetod==12 ) then

!        --- complex matrix

            write(irefwr, 610) maxmat
610         format(' the large matrix contains', i10,' complex entries')

         else

!        --- real matrix

            write(irefwr, 620) maxmat
620         format(' the large matrix contains', i10, ' real entries')

         end if
      end if
      if ( nbound==0 .and. npboun==0 ) intmat(4) = 0
      if ( debug .or. mod(iprint,10)>=2 ) then

!     --- print first part of array intmat

         ipint1 = inigettext ( intmat(3), 'intmat-1' )
         ipintm = ipint1+nrusol
         if ( jmetod>4 .and. jmetod<=12 ) ipintm = ipintm+1
         if ( jmetod>=13 .and. jmetod<=16 .or.
     +        jmetod>=24 .and. jmetod<=27 ) then

!        --- jmethod = 13-16, special storage for simple
!                      24-27, storage for AL

            call printmatsimple ( jmetod, ibuffr(ipint1) )

         else

!        --- standard storage

            call pr0004 ( jmetod, nrusol, ibuffr(ipint1),
     +                    ibuffr(ipintm) )

         end if

         if ( nblocks>0 ) then

!        --- nblocks>0, print block information

            ipint1 = ipintm+1+lenim2
            write(irefwr,*) ' Number of unknowns per block '
            call prni21 ( nblocks, ibuffr(ipint1) )
            ipint1 = ipint1+nblocks+1
            ipintm = ipint1+nblocks+1
            write(irefwr,*) ' information of block structure'
            call pr0004 ( jmetod, nblocks, ibuffr(ipint1),
     +                    ibuffr(ipintm) )

         end if

      end if
      if ( nbound>0 .and. (debug .or. mod(iprint,10)>=3) ) then

!     --- print second part of array intmat

         call ini060text ( ibuffr, intmat(4), 'intmat-2' )
         ipin1a = inigettext ( intmat(4), 'intmat-2' )
         ipin1b = ipin1a+ibcmat*(nbound+1+npboun)
         ipin2a = ipin1a+nbound+1+npboun+ibcmat*(nbound+1)
         ipin2b = ipin1b+nbound+1+ibuffr(ipin1a+nbound+npboun)
         write(irefwr,*) ' boundary conditions i, coupled with',
     +                   ' internal unknowns'
         call pr0005 ( nbound, ibuffr(ipin1a),
     +                 ibuffr(ipin2a), nrusol+npboun )
         if ( ibcmat==1 ) then
            write(irefwr,*) ' boundary conditions i,',
     +           ' coupled with boundary conditions'
            call pr0005 ( nbound, ibuffr(ipin1b),
     +                    ibuffr(ipin2b), nrusol+npboun )
         end if

      end if

      if ( npboun>0 .and. (debug .or. mod(iprint,10)>=3) ) then

!     --- print second part of array intmat corresponding to periodical bc's

         call ini060text ( ibuffr, intmat(4), 'intmat-2' )
         ipin1a = inigettext ( intmat(4), 'intmat-2' )
         ipin2a = ipin1a+(1+ibcmat)*(nbound+1)+npboun
         write(irefwr,*) ' periodical boundary conditions i,',
     +                   ' coupled with internal unknowns'
         call pr0005 ( npboun, ibuffr(ipin1a+nbound),
     +                 ibuffr(ipin2a), nrusol )

      end if
1000  call erclos ( 'incommat' )
      if ( debug ) write(irefwr,*) 'End incommat'

      end
      subroutine tosepcomsub ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         inpsep, rinsep, lenmsh, lenprob, istart,
     +                         isubr, meshnr, ioutpsav, isolut,
     +                         meshseqs, iseqlp, niterlp, ireadlp,
     +                         timefinished, numveclc, icalltimstr,
     +                         iinmap, irhsd, massmt, mattot, stiffm,
     +                         maxprb  )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.9  date 17-10-2007 Extension of inpsep for prcomm
!        version  2.8  date 23-03-2007 Extension with test_force
!        version  2.7  date 07-03-2007 Plot function
!        version  2.6  date 21-02-2007 extra options linear solver
!        version  2.5  date 28-11-2006 extra option read_solutions
!        version  2.4  date 20-11-2006 Allow writing solution to file
!        version  2.3  date 04-09-2006 Extension with bearing
!        version  2.2  date 04-01-2006 Extension of input for plotmesh
!        version  2.1  date 28-12-2005 New length inpsep for scalar expressions
!        version  2.0  date 27-09-2005 Extra parameters irhsd, ...
!        version  1.12 date 15-06-2005 Extension with pressure correction
!        version  1.11 date 03-05-2005 Extension with enthapy + refinement
!        version  1.10 date 20-04-2005 New call to intpolmbf
!
!   copyright (c) 2003-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Body of subroutines to0094 and tofreesb
!
! **********************************************************************
!
!                       KEYWORDS
!
!     compute_all
!     driving_subroutine
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'
      include 'SPcommon/clinsl'
      include 'SPcommon/csepcm'
      include 'SPcommon/cplot'
      include 'SPcommon/cmacht'
      include 'SPcommon/cmachn'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/ctimen'
      include 'SPcommon/cpost1'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer maxmesh
      parameter (maxmesh=5)
      integer lenmsh, lenprob, kmesh(lenmsh, maxmesh), ibuffr(*),
     +        kprob(lenprob,maxmesh), intmat(5,10,maxmesh), inpsep(*),
     +        istart, isubr, meshnr, ioutpsav, isolut(5,maxvc1,*),
     +        meshseqs(*), iseqlp, niterlp(10), ireadlp(10), numveclc,
     +        icalltimstr, iinmap(*), irhsd(5,*), maxprb,
     +        massmt(5,*), mattot(5,*), stiffm(5,*)
      double precision rinsep(*), buffer(*)
      logical timefinished

!     buffer        i/o   Real buffer array in which all large data is stored
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     icalltimstr   i/o   Number of calls of loop start
!     iinmap        i/o   Help array for subroutine INTPOLMS
!                         For a descriptoin see INTPOLMS
!     inpsep        i/o   The integer input read is stored in array INPSEP
!                  Array INPSEP is filled as follows:
!                  Pos. 1  NUMVETORS, i.e. number of vectors to be created
!                          Next positions information of the subroutines to be
!                          called with the corresponding information
!                          The subroutines are stored in the sequence as they
!                          are called, which means that the input of this
!                          subroutine is essential for the course of the
!                          program
!                          For each subroutine at least the following
!                          information is stored:
!                  Pos. 1  Subroutine sequence number
!                          Possible values:
!                          -1:  End of input
!                          1:   PRESBC  Read essential boundary conditions
!                          2:   LINPRB  Build system of equations and solves
!                                       linear system of equations
!                          3:   NONLNS  Build system of equations and solves
!                                       non-linear system of equations
!                          4:   CREATN  Create vector
!                          5:   OUTSOL  Write solution to file "sepcomp.out"
!                          6:   MANVEC  Manipulate vectors
!                          7:   PRINT_SCALAR: Print scalar
!                          8:   DERIV   Compute derivatives
!                          9:   PRINTV  Print vector
!                         11:   INTEGR  Compute integral
!                         12:   SCALAR i =  value/func
!                         13:   COMMAT  Change structure of matrix
!                         14:   COPYVC  Copy vectors
!                         15:   PRINT_TEXT: Print text
!                         16:   INTBND  Compute boundary integral
!                         17:   WRITBS  Write array to backing storage
!                         18:   READBS  Read array from backing storage
!                         19:   TIMPRB  Solve time-dependent problem
!                         20:   USEROUT User output subroutine
!                         21:   WHILE   Start of while loop
!                         22:   END_WHILE  End of while loop
!                         23:   START_STAT_FREE_BOUN  Start of stationary free
!                                       boundary loop
!                         24:   END_STAT_FREE_BOUN  End of stationary free
!                                       boundary loop
!                         25:   CHANGE_COEFFICIENTS
!                         26:   START_INSTAT_FREE_BOUN  Start of instationary
!                                      free boundary loop
!                         27:   END_INSTAT_FREE_BOUN  End of instationary free
!                                      boundary loop
!                         28:   INSTFREE, time integration in free boundary loop
!                         29:   INPUT_VECTOR
!                         30:   OPEN_PLOT
!                         31:   CLOSE_PLOT
!                         32:   PLOT_BOUNDARY
!                         33:   REFINE_MESH (mesh refinement)
!                         34:   PRESENT_MESH (set mesh sequence number)
!                         35:   WRITE_MESH (write mesh to file)
!                         36:   INTERPOLATE (interpolate between two meshes)
!                         37:   FOR Start for loop
!                         38:   END_FOR_LOOP
!                         39:   PLOT_VECTOR
!                         40:   PLOT_CONTOUR
!                         41:   CONTACT
!                         42:   INTERCHANGE_MESH
!                         43:   CREATE_FORCE_VECTOR
!                         44:   START_LOOP  Start of loop
!                         45:   END_LOOP    End of loop
!                         46:   START_TIME_LOOP  Start of time loop
!                         47:   END_TIME_LOOP    End of time loop
!                         48:   CHANGE_COORDINATES
!                         49:   COMPUTE_EIGENVALUES
!                         50:   CHANGE_PROBLEM
!                         51:   COMPUTE_CAPACITY
!                         52:   SOLVE_INVERSE_PROBLEM
!                         53:   DEFORM_MESH
!                         54:   PRINT_TIME
!                         55:   COMPUTE_PRINCIPAL_STRESSES
!                         56:   PLOT_COLOURED_LEVELS
!                         57:   PLOT_TENSOR
!                         58:   MOVE_OBSTACLE
!                         59:   MAKE_OBSTACLE_MESH
!                         60:   REMOVE_OBSTACLE_MESH
!                         61:   NO_OUTPUT (no action)
!                         62:   PLOT_MESH
!                         64:   STOP
!                         65:   READ_MESH
!                         66:   SET_TIME
!                         67:   MAKE_LEVELSET_MESH
!                         68:   REMOVE_LEVELSET_MESH
!                         71:   ENTHALPY_INTEGRATION
!                         72:   COMPUTE_ENTHALPY
!                         73:   INTERSECTION
!                         74:   TIME_HISTORY
!                         75:   PROBDFEXT
!                         76:   REF_ENTHALPY_INTEGRATION
!                         77:   PRESSURE_CORRECTION
!                         78:   NAVIER-STOKES
!                  Pos. 2  ISEQNR     Sequence number corresponding to input
!                                     If iseqnr = -1, the next one will be used
!                  Pos. 3  IPROB      Problem sequence number
!                                     If iprob = -1, the next one will be used
!                  Pos. 4  IVECTOR    Vector sequence number (first vector)
!                                     If ivector = -1, the same value as iprob
!                                     will be used
!                          The next positions depend on the subroutine itself:
!                   --- In case of MANVEC the positions 5 - 20+nelgrp are
!                          used:, see subroutine PRMANV
!                   --- In case of PRINT_SCALAR  position 2
!                        refers to the number of scalars  (numscalars)
!                        position 3 refers to the position in the text array
!                        In this case 0, means no text
!                        Position 4 indicates the number of characters in the
!                        text
!                        Position 5 to 4+3*numscalars contain 3 positions per
!                        scalar:
!                         1: type (1=integer, 2=real, 3=scalar)
!                         2: position in incons rlcons or scalars
!                         3: if>0, index of scalar used as array index
!                   --- In case of PRINT_VECTOR position 2
!                        refers to the vector sequence number and
!                        position 3 refers to the position in the text array
!                        In this case 0, means no text
!                        Position 4 indicates the number of characters in the
!                        text
!                       Also the positions 5-13 and possibly more
!                       are used as well as positions in rinsep in the following
!                       way:
!                        5:  ISTRIN (Starting address in rinsep)
!                        6:  ICHOICE (inppri(2), see prinvc)
!                        7:  IDIM    (inppri(3), see prinvc)
!                        8:  IQUANT  (inppri(4), see prinvc)
!                        9:  IDEGFD  (inppri(5), see prinvc)
!                        10: ISEQ    (inppri(6), see prinvc)
!                        11: IREGION (inppri(7), see prinvc)
!                        12: IHEADER (inppri(8), see prinvc)
!                        13: ICOOR   (inppri(9), see prinvc)
!                        14: IFORMAT (inppri(10), see prinvc)
!                        15: NREAD   (number of curves, surfaces etc)
!                            If NREAD>0 followed by NREAD curves etc.
!                        If IREGION#0, 6 positions in rinsep are used
!                   --- In case of NONLINEAR_SOLVER 5 positions are used:
!                         2  ISEQNR  Sequence number corresponding to input
!                                    If iseqnr = -1, the next one will be used
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: IVEC_REACT indicates if reaction forces must be
!                            computed (>0) or not (0)
!                            If >0, IVEC_REACT defines the sequence number
!                            of the vector in which the reaction forces
!                            must be stored
!                   --- In case of LINEAR_SOLVER 8 positions are used:
!                         2: contains the sequence number for the coefficients
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: contains the sequence number for the solver
!                         6: indicates if defect correction should be used:
!                            0: no defect correction
!                            1: defect correction
!                         7: indicates if quadratic elements should be
!                            treated as a cluster of linear subelements
!                            0: no subelements
!                            1: subelements
!                         8: IVEC_REACT indicates if reaction forces must be
!                            computed (>0) or not (0)
!                            If >0, IVEC_REACT defines the sequence number
!                            of the vector in which the reaction forces
!                            must be stored
!                         9: IVEC_FEM is only used in case of a spectral mesh
!                            It indicates if also the fem solution vector
!                            must be stored besides the sem solution vector
!                            if>0, IVEC_FEM defines the sequence number
!                            of the fem solution vector
!                        10: Indicates if a FEM preconditioning must be applied
!                            in case of a spectral mesh
!                            Possible values:
!                            0: no fem preconditioning
!                            1: fem preconditioning
!                   --- In case of DERIV the positions 2 - 5 are used:
!                        2:  Sequence number coefficients
!                        3:  IPROB
!                        4:  IVECTOR Sequence number output vector
!                        5:  ISEQNR Sequence number derivatives
!                   --- In case of INTEGR the positions 2 - 7 are used:
!                        2:  Sequence number coefficients
!                        3:  ISEQNR Sequence number integrals
!                        4:  IVECTOR  Sequence number input vector
!                        5:  ISCALAR1 Sequence number scalar number of integral
!                        6:  Scalar number of min integral per element
!                        7:  Scalar number of max integral per element
!                   --- In case of INTBND the positions 2 - 6 are used:
!                        2:  ISEQNR Sequence number boundary integrals
!                        3:  IVECTOR  Sequence number input vector
!                        4:  ISCALAR1 Sequence number first scalar of integral
!                        5:  ISCALAR2 Sequence number second scalar of integral
!                        6:  ISCALAR3 Sequence number third scalar of integral
!                   --- In case of SCALAR i = value the positions 2-3 are used:
!                        2:  Sequence number scalar
!                        3:  Type of construction
!                            1:  value
!                            2:  func
!                            3:  min
!                            4:  max
!                        4:  Position of value in rinsep (>0) or
!                            parameter IFUNC in FUNCSCAL ( IFUNC, SCALARS)
!                            In case of min/max: number of parameters
!                        5...:  In case of min/max: information of parameters
!                            >0: value in rinsep; <0 -Scalar sequence number
!                   --- In case of COMMAT position 2 is used:
!                        2:  Sequence number COMMAT input
!                   --- In case of COPY VECTOR the positions 2-3 are used:
!                        2:  Sequence number vector1
!                        3:  Sequence number vector2
!                        4:  Degree of freedom vector 1
!                        5:  Degree of freedom vector 2
!                   --- In case of PRINT_TEXT the positions 2-3 are used:
!                        2:  Position in text array
!                        3:  number of characters
!                   --- In case of WRITE_VECTOR or READ_VECTOR the positions
!                        2-4 are used:
!                        2:  array sequence number
!                        3:  sequence number in file 2
!                        4:  save administration (1) or not yet (0)
!                   --- In case of USEROUT at least positions 2 and 3 are used:
!                        2:  Sequence number USEROUT
!                        3:  nextra is number of extra integer positions
!                            If this number is larger than 0, nextra
!                            positions are used after position 2
!                   --- In case of WHILE the positions 2-3 are used:
!                        2:  sequence number k of boolean_expr(k)
!                        3:  address in inpsep just after the while loop
!                   --- In case of END_WHILE only position 2 is used:
!                        2:  starting address of while loop
!                   --- In case of START_(IN)STAT_FREE_BOUN or START_(TIME)_LOOP
!                       the positions 2-3 are used:
!                        2:  sequence number k
!                        3:  address in inpsep just after the loop
!                   --- In case of END_(IN)STAT_FREE_BOUN or END_(TIME)_LOOP
!                       only position 2 is used:
!                        2:  starting address of loop + 3
!                   --- In case of CHANGE_COEFFICIENTS the 4 positions have
!                       the meaning
!                        2:  coefficient sequence number
!                        3:  change coefficient sequence number
!                        4:  iteration sequence number
!                        5:  problem sequence number (if 0, the last one)
!                   --- In case of TIME_INTEGRATION 3 positions are used:
!                        2:  time_method sequence number
!                        3:  vector sequence number
!                   --- In case of INPUT_VECTOR the positions 2-5 are used:
!                        2:  Sequence number of output vector
!                        3:  Number of unknowns per point
!                        4:  Position of file name in text array
!                        5:  number of characters of file name
!                   --- In case of CLOSE_PLOT or OPEN_PLOT
!                       only position 1 is used
!                   --- In case of PLOT_BOUNDARY positions 2-3 are used:
!                        2:  IREGION
!                        3:  Position of yfact
!                        If IREGION#0, 4 positions in rinsep are used
!                   --- In case of PLOT_MESH position 2 is used:
!                        2:  IREGION
!                        3:  Position of yfact
!                        If IREGION#0, 4 positions in rinsep are used
!                   --- In case of MAKE_OBSTACLE_MESH position 2-3 are used:
!                        2:  MESH_ORIG Sequence number of mesh to be adapted
!                        3:  MESH_OBST Sequence number of adapted mesh
!                   --- In case of REMOVE_OBSTACLE_MESH position 2-3 are used:
!                        2:  MESH_ORIG Sequence number of mesh to be adapted
!                        3:  MESH_OBST Sequence number of adapted mesh
!                   --- In case of REFINE_MESH position 2-5 are used:
!                        2:  N (Number of refinements)
!                        3:  MESH_IN Sequence number of mesh to be refined
!                        4:  MESH_OUT Sequence number of refined mesh
!                        5:  ISEQ Sequence number of refine input
!                   --- In case of PRESENT_MESH position 2 is used:
!                        2:  Sequence number of mesh
!                   --- In case of INTERCHANGE_MESH position 2-3 ARE used:
!                        2:  New sequence number of mesh 1
!                        3:  New sequence number of mesh 2
!                   --- In case of INTERPOLATE positions 2-4 are used:
!                        2:  Sequence number V1 (Defines also MESH_IN)
!                        3:  Sequence number V2
!                        4:  MESH_OUT
!                   --- In case of OUTPUT positions 2-4 are used:
!                        2:  ISEQNR
!                        3:  INAME Sequence number of name in text array
!                                  0 = default name
!                        4:  IVECTOR
!                   --- In case of FOR_LOOP positions 2-6 are used:
!                        2:  Lower bound loop
!                        3:  Upper bound loop
!                        4:  Step in loop
!                        5:  Sequence number of variable
!                        6:  address in inpsep just after the for loop
!                   --- In case of END_FOR_LOOP position 2 is used:
!                        2:  starting address in for loop
!                   --- In case of PLOT_VECTOR or PLOT_TENSOR positions 2-7
!                       are used:
!                        2:  IVECTOR
!                        3:  IREGION
!                        If IREGION#0, 4 positions in rinsep are used
!                        4:  IDEGFD1
!                        5:  IDEGFD2
!                        6:  IYFACT
!                        If IYFACT#0, 1 extra position in rinsep is used
!                        7:  IFACTOR
!                        If IFACTOR#0, 1 extra position in rinsep is used
!                         In case of plot tensor position 8 is also used:
!                        8:  IDEGFD3
!                   --- In case of PLOT_CONTOUR or PLOT_COLOURED_LEVELS
!                       positions 2-8 are used:
!                        2:  IVECTOR
!                        3:  IREGION
!                        If IREGION#0, 4 positions in rinsep are used
!                        4:  IDEGFD
!                        5:  NLEVEL
!                        6:  MINLEVEL
!                        If MINLEVEL#0, 1 extra position in rinsep is used
!                        7:  MAXLEVEL
!                        If MAXLEVEL#0, 1 extra position in rinsep is used
!                        8:  IYFACT
!                        If IYFACT#0, 1 extra position in rinsep is used
!                   --- In case of CREATE_FORCE_VECTOR 5 positions are used:
!                         2: contains the sequence number for the coefficients
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: indicates if quadratic elements should be
!                            treated as a cluster of linear subelements
!                            0: no subelements
!                            1: subelements
!                   --- In case of CHANGE_COORDINATES 2 positions are used:
!                         2: contains the sequence number for the input
!                   --- In case of EIGENVALUES 5 positions are used:
!                         2: contains the sequence number for the input
!                         3: IVECTOR
!                         4: ISCALAR
!                         5: IPROB
!                         6: NEIGV
!                   --- In case of DEFORM_MESH
!                         2: Sequence number of the vector to add
!                         3: SCALE
!                   --- In case of COMPUTE_PRINCIPAL_STRESSES 5 positions are
!                       used:
!                         2: iseqstress
!                         3: iseqeigval
!                         4: iseqeigvec
!                         5: ichoice
!                   --- In case of MOVE_OBSTACLE 12 positions are used:
!                         2: iobst
!                         3: Sequence number first position in RINSEP
!                         4: if>0 reference to scalar w.r.t. u_veloc
!                         5: if>0 reference to scalar w.r.t. v_veloc
!                         6: if>0 reference to scalar w.r.t. w_veloc
!                         7: if>0 reference to scalar w.r.t. phi_veloc
!                         8: Rotation plane 1: (xy) 2: (yz) 3: (xz)
!                            RINSEP(1-3) contain the velocity vector
!                            RINSEP(4) contains the rotation velocity
!                         9: Indicates if positions 4 to 6 refer to a
!                            velocity (0) or displacement (1)
!                        10: if>0 reference to scalar w.r.t. x_orig
!                        11: if>0 reference to scalar w.r.t. y_orig
!                        12: if>0 reference to scalar w.r.t. z_orig
!                   --- In case of READ_MESH 3 positions are used:
!                         2: meshnr
!                         3: sequence number for name in text array
!                   --- In case of SET_TIME positions 2 is used:
!                        2:  Position of value in rinsep (>0)
!                   Warning: the contents of inpsep and rinsol are yet not
!                           defined in such a way that they are upwards
!                           compatible. In the future the description may be
!                           changed. Please do not take the present description
!                           for granted
!     intmat        i/o   Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     ioutpsav       i    Help parameter to store the initial value of ioutp
!     ireadlp       i/o   Sequence number for reading of loop information
!     irhsd         i/o   Standard SEPRAN array, containing information of the
!                         large vector(s)
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     iseqlp        i/o   Present level of loop
!     isolut        i/o   Standard SEPRAN array containing information of
!                         the solution vector (same as isol)
!     istart        i/o   Starting address in array INPVEC
!     isubr          i    Indicates the type of calling subroutine
!                         See subroutine TO0094 for a description
!     kmesh         i/o   Standard SEPRAN array containing information of the
!                         mesh
!     kprob         i/o   Standard SEPRAN array, containing information of
!                         the problem
!     lenmsh         i    Declared length of array KMESH
!     lenprob        i    Declared length of array KPROB
!     massmt        i/o   SEPRAN array, containing information of the mass
!                         matrix
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     mattot        i/o   SEPRAN array, containing information of the matrix
!                         M/dt + S
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     maxprb         i    Maximum number of coupled problems allowed
!     meshnr        i/o   Mesh sequence number
!     meshseqs      i/o   Contains actual sequence numbers of meshes
!     niterlp       i/o   Array of length 10.
!                         Pos. i contains iteration number for loop i
!     numveclc      i/o   Number of vectors that have been filled
!     rinsep        i/o   Array containing the real input for this subroutine
!                         See subroutine TO0094 for a description
!     stiffm        i/o   SEPRAN array, with information of the stiffness matrix
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     timefinished  i/o   If true the time loop has finished
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      logical debug, complrun, loopready
      double precision rplots(3)
      integer iplots(10), i, iname, iref, map(5), idummy(1), leninpsep,
     +        nelgrp, ivecseq, jmaxsave, k, lenname, meshin, meshout
     +
      character (len=100) namesav
      save complrun, loopready

!     complrun       If true a complete time loop must be carried out, if
!                    false one time step only
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     i              Counting variable
!     idummy         Dummy array
!     iname          Sequence number of name in text array
!     iplots         Integer input array for plot subroutine
!     iref           File reference number
!     ivecseq        Sequence number of vector to be considered with respect
!                    to the solution array (isol or isolut)
!     jmaxsave       Help variable to save the value of jmax
!     k              Counting variable
!     leninpsep      Length of part of array inpsep
!     lenname        Number of significant characters in a text
!     loopready      If true the loop has finished
!     map            Mapping array that is filled if ichoice = 2 or must
!                    be filled if ichoice = 3
!                    map is an integer array of length 5 with the following
!                    contents:
!                    1: mmimap  memory management reference to array imap
!                    2: 130 to indicate the type of array
!                    3: mmrmap  memory management reference to array rmap
!                    4: mmcoor1  memory management reference to array coor1
!                    5: mmcoor2  memory management reference to array coor2
!     maxmesh        Maximum number of meshes allowed
!     meshin         Sequence number of first mesh to be used
!     meshout        Sequence number of last mesh to be used
!     namesav        Help variable to save a name
!     nelgrp         Number of element groups
!     rplots         Real input array for plot subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      logical userbool

!     DEFMSHBF       Deform mesh for solid computation
!     ERCLOS         Resets old name of previous subroutine of higher level
!     ERCRET         Create vector
!     ERLOOPFOR      Perform all actions necessary for the start of a for loop
!     EROPEN         Produces concatenated name of local subroutine
!     EROUTS         Write solution to file "sepcomp.out"
!     ERPRES         Read essential boundary conditions
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     ERRWAR         Print warnings
!     ERSCAL         Print scalar
!     INIFIL         Open files
!     INTPOLMBF      Interpolate solution from one mesh to another
!     MESHWRBF       fill the file meshoutput with the mesh
!     MSHCHCOR       Change coordinates in array coor by calls to FUNCCOOR
!     PLAFEP         General SEPRAN low level plotting subroutine
!     PLOTCU         Plot curves and user points
!     PRBEARING      Perform the complete iteration for one time step
!                    of the mass conservation scheme of Kumar
!     PRBNIN         Compute boundary integrals
!     PRCHANPROB     Change problem number of solution vector
!     PRCOMM         New call to COMMAT
!     PRCOMPCAP      calls the actual capacity subroutine prcompcapacity
!     PRCOMPENTHALPY Compute the enthalpy from the given temperature
!     PRCOMPINVER    calls the actual inverse subroutine prcompinv
!     PRCONTCT       Compute new contact
!     PRCOPY         Copy one SEPRAN vector into another one
!     PRDERV         Compute derivatives
!     PREIGVAL       calls the actual eigenvalue subroutine EIGENVBF
!     PRENTHALPYMAIN Solve the time dependent temperature equation
!                    with  freezing front by a enthalpy method
!     PRENTHREFINE   Solve the time dependent temperature equation
!                    with  freezing front by a enthalpy method and local
!                    refinement
!     PRFIL2         Read solutions from and write to backing storage
!     PRFORCEV       Fill the force vector with a right-hand side vector
!     PRINCIPSTRBF   Computes the principle stresses from a stress tensor
!     PRININ         print 1d integer array
!     PRINTERSECT    Prepare input for intersection subroutine
!     PRINTG         Compute integrals
!     PRLINS         Build system of equations and solves linear system of
!                    equations
!     PRMANV         Vector manipulation subroutine
!     PRMOVEOBSTAC   Move obstacle over a distance for one time step
!     PRMSHLEVMS     Create a new mesh from a fixed mesh with a zero levelset
!                    crossing the mesh
!     PRMSHOBSTMS    Create a new mesh from a fixed mesh with an obstacle
!                    crossing the mesh
!     PRNAVSTOKES    Solve the time dependent Navier-Stokes equations
!     PRNEWPROB      Call subroutine PROBDFEXT to define a new problem
!                    definition
!     PRNLNS         Solves non-linear system of equations
!     PRPLOTCN       Plot contours
!     PRPLOTFN       Plot function along curve
!     PRPLOTMESH     plot mesh
!     PRPLOTVC       Plot vectors
!     PRPRESSCORR    Perform one or more time steps with the pressure correction
!                    method to solve the Navier-Stokes equations
!     PRREADMESH     Read mesh input from file
!     PRREFINE       Refine an existing mesh, compute the corresponding problem
!                    definition and create the corresponding matrix structure
!     PRRMLEVMESH    Delete the levelset mesh created for a zero levelset
!                    crossing the mesh
!     PRRMOBSTMESH   Remove obstacle mesh created from obstacle mesh
!     PRSCLR         Fill scalar corresponding to the input part SCALAR i = ...
!     PRSETTIME      Set time in main program sepcomp or sepfree
!     PRTIMD         Solves time-dependent system of equations
!     PRUSER         Prepare output for userout or userouts subroutine
!     READSOLUTS     Read all solution arrays from sepcomp.in for one time step
!     READVECBF      Read vector from ASCII file
!     TIMEFRBF       Actual body of subroutine TIMEFREE
!     TOLOOP         Perform test in loop
!     USERBOOL       User subroutine corresponding to boolean_expr
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     433   Open plot found, whereas the plot has already been opened
!    1472   Plot file has been opened and closed without making any plot.
!    1473   The plot file has not been opened  before, whereas jtimes = 10.
!    1928   free boundaries not allowed in SEPCOMP
!    2040   Value of isubr has not yet been implemented
!    2107   Mesh has not been filled
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data map /5*0/
! ======================================================================
!
      call eropen ( 'tosepcomsub' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from tosepcomsub'
         write(irefwr,1) 'isubr, istart', isubr, istart
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

      select case ( isubr)

         case(:0, 69, 70, 82:)

!        --- Option has not been programmed

            call errint ( isubr, 1 )
            call errsub ( 2040, 1, 0, 0 )
            go to 1000

         case(1)

!        --- isubr = 1, call to presbc
!                       Fill essential boundary conditions

            if ( debug ) write(irefwr,*) 'essential boundary conditions'
            call erpres ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1, numveclc )
            istart = istart + 4

         case(2)

!        --- isubr = 2, call to linprb
!                       Build and solve system of linear equations
!                       istart is raised by the subroutine itself

            if ( debug ) write(irefwr,*) 'system of linear equations'
            call prlins ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1),
     +                    numveclc, istart )

         case(3)

!        --- isubr = 3, call to nonlns
!                       Build and solve system of non-linear equations

            if ( debug ) write(irefwr,*)'system of non-linear equations'
            call prnlns ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1),
     +                    numveclc )
            istart = istart + 5

         case(4)

!        --- isubr = 4, call to creatn
!                       Create vector

            if ( debug ) write(irefwr,*) 'Create vector'
            call ercret ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1, numveclc )
            istart = istart + 4

         case(5)

!        --- isubr = 5, call to outsol
!                       Write result to file "sepcomp.out"

            if ( debug ) write(irefwr,*) 'call to outsol'
            if ( itimloop==0 .or. ioutact==1 ) then
               numvec = max(numvec,numveclc)
               call erouts ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), isolut(1,1,meshnr),
     +                       inpsep(istart+1), maxvc1, numveclc )
            end if
            istart = istart + 4

         case(6)

!        --- isubr = 6, call to manvec
!                       Manipulate vectors

            if ( debug ) write(irefwr,*) 'Manipulate vectors'
            nelgrp = kmesh(5,meshnr)
            if ( debug ) then

!           --- debug information

               write(irefwr,*) 'Before prmanv'
               call prinin ( inpsep(istart), 21+nelgrp,
     +                       'inpsep from istart' )

            end if
            call prmanv ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), rinsep,
     +                    ivecseq, scalars, numveclc, numscals, nelgrp )
            istart = istart + 21+nelgrp

         case(7,9,15,54)

!        --- isubr = 7, print scalar or isubr = 9, print vector
!            or isubr = 15, print text or isubr = 54, print time

            if ( debug ) then
               write(irefwr,*) 'print scalar/vector'
               write(irefwr,*)  'itimloop, ioutact, isubr ',
     +                           itimloop, ioutact, isubr
            end if  ! ( debug ) then

            if ( itimloop==0 .or. ioutact==1 .or. isubr==7 )
     +         call erscal ( ibuffr, buffer, inpsep(istart), scalars,
     +                       isolut(1,1,meshnr), kmesh(1,meshnr),
     +                       kprob(1,meshnr), rinsep )
            if ( isubr==15 ) then
               istart = istart + 3
            else if ( isubr==7 ) then
               istart = istart + 4 + inpsep(istart+1)*3
            else if ( isubr==54 ) then
               istart = istart + 1
            else
               istart = istart + 21
               istart = istart + inpsep(istart-1)
            end if

         case(8)

!        --- isubr = 8, call to derivs
!                       Compute derivatives

            if ( debug ) write(irefwr,*) 'Compute derivatives'
            call prderv ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1,
     +                    numveclc, scalars, numscals )
            istart = istart + 6

         case(10,25,61,63,64)

!        --- Jump

         case(11)

!        --- isubr = 11, call to integr
!                        Compute integral

            if ( debug ) write(irefwr,*) 'Compute integral'
            call printg ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), scalars, numscals,
     +                    numveclc )
            istart = istart + 7

         case(12)

!        --- isubr = 12, scalar i = value/func

            if ( debug ) write(irefwr,*) 'scalar i = value'
            leninpsep = inpsep(istart+1)
            call prsclr ( inpsep(istart+1), rinsep, numscals )
            istart = istart + leninpsep+2

         case(13)

!        --- isubr = 13, call to COMMAT, i.e. change structure of matrix

            if ( debug ) write(irefwr,*) 'change structure of matrix'
            write(6,*) 'tosepcom: call to prcomm: ',meshnr
            call prcomm ( ibuffr, kmesh(1,meshnr), kprob(1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1) )
            istart = istart + 3

         case(14)

!        --- isubr = 14, call to COPYVC, i.e. copy vectors

            if ( debug ) write(irefwr,*) 'copy vectors'
            call prcopy ( ibuffr, buffer, isolut(1,1,meshnr),
     +                    inpsep(istart+1), numveclc, kmesh(1,meshnr),
     +                    kprob(1,meshnr) )
            istart = istart + 5

         case(16)

!        --- isubr = 16, call to intbnd
!                        Compute boundary integral

            if ( debug ) write(irefwr,*) 'boundary integral'
            call prbnin ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), scalars, numscals )
            istart = istart + 6

         case(17,18)

!        --- isubr = 17 or 18, call to PRFIL2
!                              Read or write array to file 2

            if ( debug ) write(irefwr,*) 'array to file 2'
            call prfil2 ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart), maxvc1, numveclc )
            istart = istart + 4

         case(19)

!        --- isubr = 19, call to PRTIMD
!                        Solve time-dependent problem

            if ( debug ) write(irefwr,*) 'time-dependent problem'
            call prtimd ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1,
     +                    numveclc, intmat(1,1,meshnr), irhsd, massmt,
     +                    mattot, stiffm, maxprb )
            istart = istart + 4

         case(20)

!        --- isubr = 20, call to USEROUT
!                        User output subroutine

            if ( debug ) write(irefwr,*) 'User output subroutine'
            call pruser ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), numveclc,
     +                    intmat(1,1,meshnr) )
            istart = istart + 4 + inpsep(istart+2) + inpsep(istart+3)

         case(21)

!        --- isubr = 21, Start while loop

            if ( debug ) write(irefwr,*) 'Start while loop'
            if ( userbool ( inpsep(istart+1) ) ) then

!           --- While must be executed, go to next step

               istart = istart + 3
               ioutp = -1

            else

!           --- While must not be executed, skip loop

               istart = inpsep(istart+2)
               ioutp = ioutpsav

            end if

         case(22)

!        --- isubr = 22, End while loop

            if ( debug ) write(irefwr,*) 'End while loop'
            istart = inpsep(istart+1)
            ioutp = ioutpsav

         case(23,24,26,27)

!        --- Not implemented

            call errsub ( 1928, 0, 0, 0 )

         case(28)

!        --- isubr = 28, call subroutine TIMEFRBF to perform a time step

            if ( debug ) write(irefwr,*) 'perform a time step'
            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            call timefrbf ( ibuffr, buffer, timefinished,
     +                      kmesh(1,meshnr),
     +                      kprob(1,meshnr), intmat(1,1,meshnr),
     +                      isolut(1,1,meshnr), inpsep(istart+1),
     +                      numveclc, maxvc1, complrun, irhsd, massmt,
     +                      mattot, stiffm, maxprb )
            istart = istart + 3

         case(29)

!        --- isubr = 29, call subroutine READVEC

            if ( debug ) write(irefwr,*) 'subroutine READVEC'
            call readvecbf ( ibuffr, buffer, kmesh(1,meshnr),
     +                     kprob(1,meshnr),
     +                     isolut(1,inpsep(istart+1),meshnr),
     +                     inpsep(istart+2),
     +                     texts(inpsep(istart+3))(1:inpsep(istart+4)) )
            istart = istart + 5

         case(30)

!        --- OPEN PLOT found
!            Check value of jtimes, and set this value to 1

            if ( debug ) write(irefwr,*) 'OPEN PLOT'
            if ( jtimes/=10 ) then

!           --- Error 433:  Plot file has already been opened before

               call errwar ( 433, 0, 0, 0 )
            else
               jtimes = 11
            end if
            istart = istart+1

         case(31)

!        --- CLOSE PLOT found
!            Check value of jtimes, and set this value to 3

            if ( debug ) write(irefwr,*) 'CLOSE PLOT'
            if ( jtimes==11 ) then

!           --- Error 1472:  Plot file is opened and closed, without
!                           making any plot

               call errwar ( 1472, 0, 0, 0 )
               jtimes = 10

            else if ( jtimes==10 ) then

!           --- Error 1473:  Plot file has not been opened

               call errwar ( 1473, 0, 0, 0 )

            else

!           --- Close Plot dataset

               jtimes = 13
               call plafep ( 2, 0d0, 0d0, 0d0, 0d0, 0 )
               jtimes = 10

            end if
            istart = istart+1

         case(32)

!        --- PLOT BOUNDARY found

            if ( debug ) write(irefwr,*) 'PLOT BOUNDARY'
            iplots(1) = 2
            do i = 2, 10
               iplots(i) = 0
            end do
            iplots(3) = 1
            rplots(1) = 15d0                          ! plotfm
            if ( inpsep(istart+2)==0 ) then
               rplots(2) = 1d0                        ! yfact
            else
               rplots(2) = rinsep(inpsep(istart+2))
            end if
            if ( inpsep(istart+1)>0 ) then
               jmaxsave = jmax
               jmax = 1
               xmin = rinsep(inpsep(istart+1))
               xmax = rinsep(inpsep(istart+1)+1)
               ymin = rinsep(inpsep(istart+1)+2)
               ymax = rinsep(inpsep(istart+1)+3)
            end if
            call plotcu ( ibuffr, buffer, kmesh(1,meshnr), iplots,
     +                    rplots, 'x', 'y' )
            if ( inpsep(istart+1)==1 ) jmax = jmaxsave
            istart = istart+3

         case(33)

!        --- REFINE_MESH found

            if ( debug ) write(irefwr,*) 'REFINE_MESH'
            call prrefine ( ibuffr, buffer, kmesh, kprob, intmat,
     +                      isolut, inpsep(istart+1), maxvc1, numveclc,
     +                      meshnr, lenmsh, lenprob, meshseqs, iinmap,
     +                      map )
            istart = istart+5

         case(34)

!        --- PRESENT_MESH found

            if ( debug ) write(irefwr,*) 'PRESENT_MESH'
            meshnr = meshseqs(inpsep(istart+1))
            if ( ioutp>=0 ) write(irefwr,695) meshnr
            if ( kmesh(2,meshnr)/=100 ) then

!           --- Mesh has not been filled

               call errint ( meshnr, 1 )
               call errint ( kmesh(2,meshnr), 2 )
               call errint ( 100, 3 )
               call errsub ( 2107, 3, 0, 0 )
               go to 1000

            end if
695         format ( ' Present mesh number is ', i1 )
            istart = istart+2

         case(35)

!        --- WRITE_MESH found

            if ( debug ) write(irefwr,*) 'WRITE_MESH'
            iname = inpsep(istart+1)
            namesav = name10
            if ( iname>0 ) then

!           --- iname>0, set name of meshoutput file

               name10 = texts(iname)(1:50)

            end if
            call inifil(10)
            lenname = index(name10,' ')-1
            if ( lenname==-1 ) lenname = len(name10)
            if ( ioutp>=0 ) write(irefwr,705) meshnr,
     +           name10(1:lenname)
705         format ( ' Write mesh ', i1, ' to file ', a )
            call meshwrbf ( ibuffr, buffer, iref10, kmesh(1,meshnr) )
            name10 = namesav
            iref = abs(iref10)
            close ( iref )
            istart = istart+2

         case(36)

!        --- INTERPOLATE found

            if ( debug ) write(irefwr,*) 'INTERPOLATE'
            ivecseq  = inpsep(istart+1)
            meshin  = meshseqs(inpsep(istart+2))
            meshout = meshseqs(inpsep(istart+3))
            if ( ioutp>=0 ) write(irefwr,715) ivecseq, meshin, meshout
715         format ( ' Interpolate vector ', i3, ' from mesh ', i1,
     +               ' to mesh ', i1 )
            if ( iinmap(2)==2 ) then

!           --- Check if meshin and meshout correspond to previous map

               if ( iinmap(4)/=meshin .or. iinmap(5)/=meshout )
     +            iinmap(2) = 1

            end if
            call intpolmbf ( ibuffr, buffer, iinmap, kmesh(1,meshin),
     +                       kmesh(1,meshout), kprob(1,meshin),
     +                       kprob(1,meshout), isolut(1,ivecseq,meshin),
     +                       isolut(1,ivecseq,meshout), map, idummy )
            iinmap(2) = 2
            iinmap(4) = meshin
            iinmap(5) = meshout
            istart = istart+4

         case(37)

!        --- isubr = 37, Start for loop

            if ( debug ) write(irefwr,*) 'Start for loop'
            call erloopfor ( istart, inpsep )
            ioutp = -1

         case(38)

!        --- isubr = 38, End for loop

            if ( debug ) write(irefwr,*) 'End for loop'
            istart = inpsep(istart+1)
            ioutp = ioutpsav

         case(39,57)

!        --- PLOT VECTOR or TENSOR found

            if ( debug ) write(irefwr,*) 'PLOT VECTOR or TENSOR'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotvc ( ibuffr, buffer, kmesh,
     +                         kprob(1,meshnr), isolut(1,1,meshnr),
     +                         inpsep(istart), rinsep, meshnr, lenmsh )

            end if
            istart = istart+7
            if ( isubr==57 ) istart = istart+2

         case(40,56)

!        --- PLOT CONTOUR or COLOURED LEVELS found

            if ( debug ) write(irefwr,*) 'PLOT CONTOUR'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotcn ( ibuffr, buffer, isubr, inpsep(istart+1),
     +                         rinsep, lenmsh, kmesh(1,meshnr),
     +                         kprob(1,meshnr), isolut(1,1,meshnr) )

            end if
            istart = istart+10

         case(41)

!        --- CONTACT found

            if ( debug ) write(irefwr,*) 'CONTACT'
            call prcontct ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut(1,1,meshnr),
     +                      inpsep(istart+1), numveclc,
     +                      intmat(1,1,meshnr) )
            istart = istart+4

         case(42)

!        --- INTERCHANGE_MESH found

            if ( debug ) write(irefwr,*) 'INTERCHANGE_MESH'
            k = meshseqs(inpsep(istart+1))
            meshseqs(inpsep(istart+1)) = meshseqs(inpsep(istart+2))
            meshseqs(inpsep(istart+2)) = k
            istart = istart+3

         case(43)

!        --- isubr = 43, call to build
!                       Build right-hand side vector

            if ( debug ) write(irefwr,*) 'Build right-hand side vector'
            call prforcev ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut(1,1,meshnr),
     +                      intmat, inpsep(istart+1), numveclc )
            istart = istart + 5

         case(44)

!        --- isubr = 44, start loop

            if ( debug ) write(irefwr,*) 'start loop'
            iseqlp = iseqlp+1
            ireadlp(iseqlp) = inpsep(istart+1)
            niterlp(iseqlp) = 0
            call toloop ( ibuffr, buffer, niterlp(iseqlp), loopready,
     +                    kmesh(1,meshnr), kprob(1,meshnr),
     +                    isolut(1,1,meshnr), ireadlp(iseqlp), iseqlp )
            ioutp = -1
            istart = istart + 3

         case(45)

!        --- isubr = 45, call subroutine TOLOOP and check on convergence

            if ( debug ) write(irefwr,*) 'TOLOOP'
            call toloop ( ibuffr, buffer, niterlp(iseqlp), loopready,
     +                    kmesh(1,meshnr), kprob(1,meshnr),
     +                    isolut(1,1,meshnr), ireadlp(iseqlp), iseqlp )
            if ( loopready ) then
               istart = istart+2
               niterlp(iseqlp) = 1
               iseqlp = iseqlp-1
               loopready = .false.
               ioutp = ioutpsav
            else
               istart = inpsep(istart+1)
            end if

         case(46)

!        --- isubr = 46, start time loop

            if ( debug ) write(irefwr,*) 'start time loop'
            if ( .not. timefinished ) then

!           --- Not yet ready, go to next step

               istart = istart + 3
               if ( ioutp>=0 .and. nitercom<=1 .and. itimloop<=0 )
     +            write(irefwr,811)
811            format ( /'Start time loop'/)
               icalltimstr = icalltimstr+1
               icalltim = 0

            else

!           --- End of loop

               istart = inpsep(istart+2)
               itimloop = 0
               icalltimstr = 0
               timefinished = .false.
               maxcalltim = 0
               icalltim = 0
               if ( ioutp>=0 ) write(irefwr,812)
812            format ( /'End time loop'/)

            end if

         case(47)

!        --- isubr = 47, End time loop

            if ( debug ) write(irefwr,*) 'End time loop'
            istart = inpsep(istart+1)-3

         case(48)

!        --- isubr = 48, call to mshchcor
!                        Change coordinates

            if ( debug ) write(irefwr,*) 'Change coordinates'
            call mshchcor ( ibuffr, buffer, kmesh(1,meshnr),
     +                      inpsep(istart+1) )
            iinmap(2) = 1
            istart = istart + 2

         case(49)

!        --- isubr = 49, Compute eigenvalues

            if ( debug ) write(irefwr,*) 'Compute eigenvalues'
            call preigval ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), intmat(1,1,meshnr),
     +                      isolut(1,1,meshnr), inpsep(istart+1),
     +                      scalars, maxvc1, numveclc, maxcls )
            istart = istart + 6

         case(50)

!        --- isubr = 50, Change problem

            if ( debug ) write(irefwr,*) 'Change problem'
            call prchanprob ( ibuffr, kprob(1,meshnr), kmesh(1,meshnr),
     +                        isolut(1,1,meshnr), inpsep(istart+1) )
            istart = istart + 4

         case(51)

!        --- isubr = 51, Compute capacity

            if ( debug ) write(irefwr,*) 'Compute capacity'
            call prcompcap ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), intmat(1,1,meshnr),
     +                       isolut(1,1,meshnr), inpsep(istart+1),
     +                       maxvc1, numveclc )
            istart = istart + 4

         case(52)

!        --- isubr = 52, Solve inverse problem

            if ( debug ) write(irefwr,*) 'Solve inverse problem'
            call prcompinver ( ibuffr, buffer, kmesh(1,meshnr),
     +                         kprob(1,meshnr), intmat(1,1,meshnr),
     +                         isolut(1,1,meshnr), inpsep(istart+1),
     +                         maxvc1, numveclc )
            istart = istart + 4

         case(53)

!        --- isubr = 53, Deform mesh

            if ( debug ) write(irefwr,*) 'Deform mesh'
            call defmshbf ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr),
     +                      isolut(1,inpsep(istart+1),meshnr),
     +                      rinsep(inpsep(istart+2)) )
            istart = istart + 3

         case(55)

!        --- isubr = 55, COMPUTE_PRINCIPLE_STRESSES

            if ( debug ) write(irefwr,*) 'COMPUTE_PRINCIPLE_STRESSES'
            call principstrbf ( ibuffr, buffer, isolut(1,1,meshnr),
     +                          inpsep(istart+1), kmesh(1,meshnr) )
            numveclc = max(numveclc, inpsep(istart+3), inpsep(istart+4))
            istart = istart + 5

         case(58)

!        --- isubr = 58, MOVE_OBSTACLE

            if ( debug ) write(irefwr,*) 'MOVE_OBSTACLE'
            call prmoveobstac ( ibuffr, buffer, inpsep(istart+1),
     +                          rinsep, kmesh(1,meshnr),
     +                          kprob(1,meshnr), intmat(1,1,meshnr) )
            istart = istart + 12

         case(59)

!        --- isubr = 59, MAKE_OBSTACLE_MESH

            if ( debug ) write(irefwr,*) 'MAKE_OBSTACLE_MESH'
            call prmshobstms ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, inpsep(istart+1), maxvc1,
     +                         numveclc, meshnr, lenmsh, lenprob,
     +                         meshseqs, iinmap, map )
            istart = istart + 4

         case(60)

!        --- isubr = 60, REMOVE_OBSTACLE_MESH

            if ( debug ) write(irefwr,*) 'REMOVE_OBSTACLE_MESH'
            call prrmobstmesh ( ibuffr, buffer, kmesh, kprob, intmat,
     +                          isolut, inpsep(istart+1), maxvc1,
     +                          numveclc, meshnr, lenmsh, lenprob,
     +                          meshseqs, iinmap, map )
            istart = istart + 3

         case(62)

!        --- PLOT MESH found

            if ( debug ) write(irefwr,*) 'PLOT MESH'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotmesh ( ibuffr, buffer, inpsep(istart+1),
     +                           kmesh(1,meshnr), rinsep )

            end if  ! ( itimloop==0 .or. ioutact==1 )
            istart = istart+5

         case(65)

!        --- isubr = 65, READ_MESH

            if ( debug ) write(irefwr,*) 'READ_MESH'
            call prreadmesh ( ibuffr, buffer, inpsep(istart+1), lenmsh,
     +                        kmesh )
            istart = istart + 3

         case(66)

!        --- isubr = 66, SET_TIME

            if ( debug ) write(irefwr,*) 'SET_TIME'
            call prsettime ( inpsep(istart+1), rinsep )
            istart = istart + 2

         case(67)

!        --- isubr = 67, MAKE_LEVELSET_MESH

            if ( debug ) write(irefwr,*) 'MAKE_LEVELSET_MESH'
            call prmshlevms ( ibuffr, buffer, kmesh, kprob, intmat,
     +                        isolut, inpsep(istart+1), maxvc1,
     +                        numveclc, meshnr, lenmsh, lenprob,
     +                        meshseqs, iinmap, map )
            istart = istart + 5

         case(68)

!        --- isubr = 68, REMOVE_LEVELSET_MESH

            if ( debug ) write(irefwr,*) 'REMOVE_LEVELSET_MESH'
            call prrmlevmesh ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, inpsep(istart+1), maxvc1,
     +                         numveclc, meshnr, lenmsh, lenprob,
     +                         meshseqs, iinmap, map )
            istart = istart + 3

         case(71)

!        --- isubr = 71, ENTHALPY_INTEGRATION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'ENTHALPY_INTEGRATION'
            call prenthalpymain ( ibuffr, buffer, timefinished,
     +                            kmesh(1,meshnr),
     +                            kprob(1,meshnr), intmat(1,1,meshnr),
     +                            isolut(1,1,meshnr), inpsep(istart+1),
     +                            numveclc, maxvc1, complrun, irhsd,
     +                            massmt, mattot, stiffm, maxprb )
            istart = istart + 2

         case(72)

!        --- isubr = 72, COMPUTE_ENTHALPY

            if ( debug ) write(irefwr,*) 'COMPUTE_ENTHALPY'
            call prcompenthalpy ( ibuffr, buffer, inpsep(istart+1),
     +                            isolut(1,1,meshnr), numveclc,
     +                            kmesh(1,meshnr), kprob(1,meshnr) )
            istart = istart + 2

         case(73)

!        --- isubr = 73, INTERSECTION

            if ( debug ) write(irefwr,*) 'INTERSECTION'
            call printersect ( ibuffr, buffer, kmesh(1,meshnr),
     +                         inpsep(istart+1), rinsep,
     +                         scalars, numscals )
            istart = istart + 5

         case(75)

!        --- isubr = 75, New problem description

            if ( debug ) write(irefwr,*) 'NEW_PROBLEM_DESCRIPTION'
            call prnewprob ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), inpsep(istart+1),
     +                       intmat(1,1,meshnr) )
            istart = istart + 2

         case(76)

!        --- isubr = 76, REF_ENTHALPY_INTEGRATION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'REF_ENTHALPY_INTEGRATION'
            call prenthrefine ( ibuffr, buffer, kmesh, kprob, intmat,
     +                          isolut, lenmsh, lenprob, timefinished,
     +                          numvec, maxvc1, complrun,
     +                          inpsep(istart+1), meshnr, maxmesh,
     +                          irhsd, massmt, mattot, stiffm )
            istart = istart + 3

         case(77)

!        --- isubr = 77, PRESSURE_CORRECTION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'PRESSURE_CORRECTION'
            call prpresscorr ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, lenmsh, lenprob, timefinished,
     +                         numvec, maxvc1, complrun,
     +                         inpsep(istart+1) )
            istart = istart + 2

         case(78)

!        --- isubr = 78, NAVIER_STOKES

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'NAVIER_STOKES'
            call prnavstokes ( ibuffr, buffer, timefinished,
     +                         kmesh(1,meshnr), kprob(1,meshnr),
     +                         intmat(1,1,meshnr), isolut,
     +                         inpsep(istart+1), numvec, maxvc1,
     +                         irhsd, massmt, mattot, stiffm,
     +                         maxprb, lenmsh, lenprob )
            istart = istart + 2

         case(79)

!        --- isubr = 79, BEARING

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'BEARING'
            call prbearing ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), intmat(1,1,meshnr),
     +                       isolut, inpsep(istart+1),
     +                       numvec )
            istart = istart + 2

         case(80)

!        --- isubr = 80, Read solutions

            call readsoluts ( ibuffr, buffer, isolut, kmesh(1,meshnr),
     +                        kprob(1,meshnr) )
            istart = istart + 2

         case(81)

!        --- isubr = 81, Plot function
!                        istart is raised in subroutine itself

            call prplotfn ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut, inpsep(istart+1),
     +                      istart )

      end select  ! case ( isubr)

1000  call erclos ( 'tosepcomsub' )

      if ( debug ) then
         write(irefwr,1) 'istart', istart
         write(irefwr,*) 'End tosepcomsub'
      end if  ! ( debug )

      end
      subroutine to0092 ( ibuffr, buffer, isubr, kmesh, kprob, intmat,
     +                    nbuf, iread, lenmesh, lenprob, maxmesh )
! ======================================================================
!
!        programmer    Guus Segal
!        version  5.18 date 07-11-2007 Jump in case of error
!        version  5.17 date 08-10-2007 Extension of lniincommat
!        version  5.16 date 26-09-2007 New call to mshrdparmesh
!        version  5.15 date 13-12-2006 option: read sepcomp.out
!        version  5.14 date 28-11-2006 extension of manag
!        version  5.13 date 25-10-2006 Minor adaptation to suppress output
!        version  5.12 date 20-03-2006 Do not write first part of sepcomp.out
!                                      in case of simalex
!        version  5.11 date 16-01-2006 princons in case of simalex replaced
!        version  5.10 date 12-12-2005 Jump in case of error
!        version  5.9  date 15-04-2005 Decrease priorities
!        version  5.8  date 02-03-2005 New call to readal
!        version  5.7  date 31-01-2005 Remove file 17
!        version  5.6  date 05-01-2005 New call to simsetconst
!        version  5.5  date 20-12-2004 Extra call to simsetconst
!        version  5.4  date 15-11-2004 Debug statements
!        version  5.3  date 29-03-2004 Extension with mshextra
!        version  5.2  date 12-03-2004 Adaptation for simalex
!        version  5.1  date 26-01-2004 New call to readsp
!        version  5.0  date 24-12-2003 Extra parameters
!
!   copyright (c) 1993-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!    Undersubroutine of SEPSTR and SEPSTL
!    The actual tasks of SEPSTR/SEPSTL are carried out
!    The mesh is supposed to be made by program SEPMESH and stored in the
!    file meshoutput
!    The input for subroutine PROBDF is stored in array iinput and written
!    to the file sepcomp.inf
!    The files meshoutput, sepcomp.inf and sepcomp.out are opened,
!    the file meshoutput is closed.
!    The machine dependent reference numbers iref10, iref73 and iref74 get
!    a value
! **********************************************************************
!
!                       KEYWORDS
!
!     read
!     start
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cmachn'
      include 'SPcommon/cmacht'
      include 'SPcommon/cbuffr'
      include 'SPcommon/csepmain'
      include 'SPcommon/cparallel'
      include 'SPcommon/cpackage'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/comcons3'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer lenmesh, lenprob, maxmesh, kmesh(lenmesh,maxmesh),
     +        kprob(lenprob), intmat(5,*), nbuf, isubr, iread, ibuffr(*)
      double precision buffer(*)

!     buffer        i/o   Buffer array
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     intmat         o    Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     iread          i    Indicates if all SEPRAN input until end
!                         of file or END_OF_SEPRAN_INPUT is read (1) or that
!                         only the input as described for SEPSTR is read (0)
!     isubr          i    Indicates the type of calling subroutine
!                         Possible values
!                         1:  SEPSTR, nbuf does not have a value
!                         2:  SEPSTL, nbuf has a value
!                         3:  SEPSTN, nbuf may have a value
!                             All SEPRAN input is read in this subroutine
!                             provided iread = 1
!                         4:  See 3, now called by sepcomp or sepfree
!                         11: simalex
!     kmesh          o    Standard SEPRAN array containing information of the
!                         mesh
!     kprob          o    Standard SEPRAN array, containing information of
!                         the problem
!     lenmesh        i    Declared length of array KMESH (first entry)
!     lenprob        i    Length of array kprob
!     maxmesh        i    Maximum number of meshes allowed
!     nbuf           i    Length of nbuffr as declared by the user.
!                         The user must declare ibuffr himself in the main
!                         program
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer lniinp, lnrinp, lniincommat
      parameter ( lniinp=5000, lnrinp=10, lniincommat=25 )
      integer i, iprob, nprob, iinput(lniinp), iincommt(lniincommat,10),
     +        iref, jstart, iseqnr, ipmetd, idummy, j, ihelp(10),
     +        ishift, lastps, lastps1, manag(lenmanag), nextrecord,
     +        iseqprob, irefsav
      double precision rinput(lnrinp)
      logical check, debug, callcommat
      character (len=100) filename, filename1, name

!     callcommat     If true subroutine commat must be called
!     check          Indication if a file has been opened (true) or not (false)
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     filename       Name of file
!     filename1      Name of file
!     i              General counting variable
!     idummy         Dummy parameter
!     ihelp          Work array to store input from the input file
!     iincommt       Input array for subroutine incommat.
!                    Defines the type of matrix to be used. (See User's Manual)
!                    iincommt is an array of length 10, which means that at most
!                    10 problems may be solved at one time
!     iinput         Output array of subroutine PROBDF. A fixed length of 500
!                    has been declared. The input for subroutine PROBDF is
!                    written to array iinput and this array is written to the
!                    file sepcomp.inf
!                    In this way the output program can call PROBDF again with
!                    the same input
!     ipmetd         Starting address of array JMETOD in IBUFFR
!     iprob          Actual problem number
!     iref           Local reference number
!     irefsav        Help parameter to save the value of iref
!     iseqnr         Dummy parameter
!     iseqprob       Sequence number of problem input
!     ishift         Shift in mesh sequence number
!     j              General counting variable
!     jstart         Absolute value of first parameter in the call of START
!     lastps         Last non-blank position in filename
!     lastps1        Last non-blank position in filename1
!     lniincommat    Length of first index of iincommat
!     lniinp         Length of array iinput
!     lnrinp         Available space for array RINPUT
!     manag          Manager array that sets the parameters in subroutine
!                    SEPSTL
!                    See subroutine readsp
!     name           Name of file
!     nextrecord     Defines last record read by readsp
!                    Possible values:
!                    0: none
!                    1: PROBLEM
!                    2: POSTPROCESSING
!                    3: MESH
!                    4: READ MESH
!                    5: EXTRA_MESH_INPUT
!     nprob          Number of problems to be solved
!     rinput         Real input array
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer inigettext

!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     FEMESHBF       creates a linear element mesh based on the nodes
!                    of a spectral element mesh
!     INCOMMAT       Actual body of COMMAT and MATSTRUC
!     INI015         Extract infor entries of integer and real input arrays from
!                    ISEPIN
!     INICHINPSOL    Adapt input array with respect to variables
!     INIDBG         read file sepran.dbg and set debug parameters
!     INIFIL         Open files
!     INIGETTEXT     Compute pointer of array in IBUFFR
!     INIMPI         Start MPI
!     INIPRI         Decrease priorities of arrays
!     INIRMF         Remove file if it exists
!     INITCB         Initializes SEPRAN machine-independent common blocks
!     INITMDBF       Set machine-dependent quantities
!     MSH071         Read information of the mesh from the file indicated by
!                    iref
!     MSHEXTRA       Read extra input for mesh and apply the actions required
!                    by this extra input
!     MSHRDPARMESH   Read information parallel information about global mesh
!                    from file meshoutput_par.000
!     PRINCONS       Print all constants, variables and vector names
!     PROBDFEXT      Reads and stores problem definition
!     READ00         Reads input for subroutine COMMAT from standard input
!                    file.
!     READ74SUB      Read names of vectors from the sepcomp.out file
!     READAL         Read all SEPRAN input for the computational program except
!                    problem definition and preceding information
!     READPROBIINP   Read information concerning the input for probdfbf from
!                    file sepcomp.out and call subroutine probdfbf
!     READSP         Reads information for subroutine SEPSTL
!     SIMSETCONST    Set constants defined in the input file, which depend on
!                    the mesh
!     STARTBF        Standard SEPRAN starting subroutine
!     WRITPROBIINP   Write arrays iinput and rinput corresponding to kprob to
!                    file sepcomp.out
! **********************************************************************
!
!                       I/O
!
!    Input is read from the standard input file
!    See the various subroutines that perform this action
!    Furthermore the file meshoutput is read
!    File sepcomp.inf is created
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     719   Too much problems are to be created at one time
!    1669   file does not exist
!    2860   The sepcomp.in and sepcomp.out file have the same name
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      debug = .false.

!     --- Start the program

      iinput(1) = lniinp
      do i = 2, 5
         iinput(i) = 0
      end do
      rinput(1) = dble(lnrinp)
      do i = 2, 5
         rinput(i) = 0d0
      end do
      call initcb
      if ( debug ) write(6,*) 'after initcb'
      if ( isubr==11 ) mainsubr = 102   ! simalex only

!     --- Check if parallel computations must be carried out

      inquire ( file = 'sepran_par.input', exist = parallel )

      if ( parallel ) then

!     --- File sepran_par.input has been found, i.e. parallel computations
!         initialize mpi

         call inimpi
         cputiming = .false.

      else

!     --- Serial computions

         cputiming = .true.

      end if  ! ( parallel )

      call initmdbf ( nbuf )
      if ( debug ) write(irefwr,*) 'after initmdbf'

!     --- Open read and write files

      call inifil(5)
      if ( parallel ) then

!     --- Parallel, we rename irefwr to 26 and couple a name to
!         the output file

         irefwr = 26
         name(1:11) = 'sepran_out.'
         write ( name(12:14),100 ) inodenr
100      format(i3.3)
         open ( unit = irefwr, file = name(1:14) )
         write(irefwr,*) 'node number is ',inodenr

      else

!     --- Serial computation, standard

         call inifil(6)

      end if

!     --- Open standard elements file and error message file

      call inifil(1)
      call inifil(4)
      if ( nbuf>0 ) nbuffr = max(nbuffr,nbuf)
      call inidbg ( ibuffr )
      if ( debug ) write(irefwr,*) 'after inifil'

      if ( parallel ) then

!     --- File sepran_par.input has been found, i.e. parallel computations
!         First set input file

         open ( unit=irefre, file = 'sepran_par.input' )

      end if  ! ( parallel )

!     --- Read array manag (defaults)

      manag(5) = -2
      if ( isubr>=4 .and. isubr/=11 ) then
         manag(5) = manag(5)-1000
         mainsubr = 2
      end if
      call readsp ( ibuffr, manag, lenmanag, nextrecord )
      if ( debug ) write(irefwr,*) 'after readsp'

      if ( parallel ) then

!     --- create name of general parallel mesh input file

         lastps = index ( name10, ' ' )-1
         filename = name10
         filename(lastps+1:lastps+8) = '_par.000'
         inquire ( file = filename(1:lastps+8), exist = check )
         if ( .not. check ) then

!        --- The file meshoutput_par.000 does not exist

            call errchr ( filename(1:lastps+8), 1 )
            call errsub ( 1669, 0, 0, 1 )
            go to 1000

         end if

!        --- Set name of mesh input file

         write(filename(lastps+1:lastps+8),110) inodenr
110      format('_par.',i3.3)
         name10 = filename(1:lastps+8)

         lastps1 = index ( name74, ' ' )-5
         filename1 = name74
         write(filename1(lastps1+1:lastps1+8),110) inodenr
         name74 = filename1(1:lastps1+8)

      end if

!     --- Print a list of all constants, variables, reals and vector names

      if ( mainsubr/=102 ) call princons
      if ( debug ) write(irefwr,*) 'after princons'

!     --- Standard SEPRAN start subroutine

      jstart = abs(manag(1))
      if ( mod(jstart/100,10)==0 ) jstart = jstart + 100
      if ( manag(1)<0 ) then
         manag(1) = -jstart
      else
         manag(1) = jstart
      end if
      if ( mainsubr==102 ) manag(3) = -1   ! simalex only
      call startbf ( ibuffr, manag(1), manag(2), manag(3), manag(4),
     +               nbuf )
      if ( debug ) write(irefwr,*) 'after startbf'
      if ( isubr==1 ) then
         call eropen( 'sepstr' )
      else if ( isubr==2 ) then
         call eropen( 'sepstl' )
      else
         call eropen( 'sepstn' )
      end if
      call eropen ( 'to0092' )
      if ( nbuf>0 ) nbuffr = max(nbuffr,nbuf)

!     --- Open files meshoutput, sepcomp.out and read mesh

      call inifil ( -10 )
      if ( debug ) write(irefwr,*) 'after inifil ( -10 )'
      if  ( manag(11)==1 ) then

!     --- manag(11)>0, read from sepcomp.xxx

         if ( iref74<0 ) iref73 = -abs(iref73)  ! sepcomp.in has same
                                                ! structure as sepcomp.out
         call inifil ( -73 )

      end if  ! ( manag(11)==1 )

      if ( manag(7)==1 ) then

!     --- manag(7) = 1, fill files 74 (sepcomp.out)

         if ( manag(11)==0 ) then

!        --- manag(11)=0, standard

            call inirmf ( 73 )  ! remove sepcomp.inf
            call inirmf ( 74 )  ! remove sepcomp.out

         end if  ! ( manag(11)==0 )

!        --- sepcomp.out is not opened in case of simalex

         if ( mainsubr/=102 ) then

!        --- Not simalex, open file sepcomp.out

            if ( manag(7)==1 ) then

!           --- manag(7) = 1, write to sepcomp.out

               call inifil ( 74 )
               if ( manag(11)>0 ) then

!              --- manag(11)>0, in and output files must have
!                  different names

                  if ( name74==name73 ) then

!                 --- sepcomp.in and sepcomp.out have the same name

                     call errchr ( name73, 1 )
                     call errsub ( 2860, 0, 0, 1 )
                     go to 1000

                  end if  ! ( namef74==namef73 )

               end if  ! ( manag(11)>0 )

            end if  ! ( manag(7)==1 )

         end if  ! ( mainsubr/=102 )

         iref = 22
         inquire ( file='sepcomp.freq', exist=check )
         if ( check ) then

            open ( unit = iref, file = 'sepcomp.freq',
     +             status = 'unknown' )
            close ( unit = iref, status = 'delete' )

         end if

      end if  ! ( manag(7)==1 )

      if ( debug ) write(irefwr,*) 'before msh071'
      call msh071 ( ibuffr, buffer, manag(5), iref10, kmesh, ihelp )
      if ( debug ) write(irefwr,*) 'after msh071'
      iref = abs(iref10)
      close ( unit = iref )

      if ( mainsubr==102 ) then

!     --- mainsubr = 102: i.e. simalex
!         Special call to set constants in the constants block depending
!         on the mesh

         call simsetconst ( ibuffr, buffer, kmesh )
         call princons
         if ( debug ) write(irefwr,*) 'na simsetconst'
         if ( ierror/=0 ) go to 1000

      end if  ! ( mainsubr==102 )

      if ( parallel ) then

!     --- parallel is true, hence parallel computing
!         Read file meshoutput_par.000 and some information from meshoutput

         call mshrdparmesh ( ibuffr, buffer, kmesh )

      end if

!     --- Check if spectral mesh has been found
!         If so call femesh

      if ( isubr==4 .and. kmesh(38,1)/=0 ) then

!     --- Spectral mesh: fill fem mesh in kmesh(.,2)

         iref = 22
         write(6,*) 'call femeshbf: '
         call femeshbf ( ibuffr, buffer, kmesh(1,1), kmesh(1,2),
     +                   iref, 1 )
         close ( unit = iref )
         if ( debug ) write(irefwr,*) 'after femeshbf'

      end if

      if ( nextrecord==5 ) then

!     --- Read extra mesh input and perform corresponding actions

         call mshextra ( ibuffr, buffer, kmesh )
         if ( debug ) write(irefwr,*) 'after mshextra'

      end if  ! ( nextrecord==5 )

!     --- Read problem definition

      if ( manag(11)==0 ) then

!     --- manag(11)=0, standard case

         call probdfext ( ibuffr, buffer, manag(6), kprob, kmesh,
     +                    iinput, rinput, iseqprob )

      else

!     --- manag(11)=1, read problem definition

         irefsav = iref74    ! save iref74
         iref74 = iref73     ! replace by iref73
         call readprobiinp ( ibuffr, buffer, kprob, kmesh, iinput,
     +                       .false. )

!        --- Read names of vectors

         call read74sub
         iref74 = irefsav    ! reset iref74
         iseqprob = 1

      end if  ! ( manag(11)==0 )
      if ( ierror/=0 ) go to 1000

      if ( debug ) then
         write(irefwr,*) 'after probdfext'
         write(irefwr,200) 'iseqprob', iseqprob
200      format ( a, 1x, 100(i6,1x) )
      end if  ! ( debug )

      if ( manag(7)==1 .and. mainsubr/=102 ) then

!     --- Write array iinput to file sepcomp.out
!         This part is skipped in case of simalex

         call writprobiinp ( ibuffr, buffer, kprob )
         if ( debug ) write(irefwr,*) 'after writprobiinp'

      end if

      nprob = max ( 1, kprob(40) )
      if ( nprob>10 ) then

!     --- nprob>10 not yet implemented

         call errint ( nprob, 1 )
         call errint ( 10, 2 )
         call errsub ( 719, 2, 0, 0 )
         go to 1000

      end if

      callcommat = .true.  ! default commat must be called

      if ( iread==1 ) then

!     --- isubr = 1, Read rest of the SEPRAN input

         call readal ( ibuffr, buffer, kmesh, kprob, iseqprob, manag )
         if ( debug ) write(irefwr,*) 'after readal'
         if ( ierror/=0 ) go to 1000

!        --- Find first input of incommat and store in iincommt

         call ini015 ( ibuffr, buffer, 3, 1, iinput(1), idummy )
         if ( ierror/=0 ) go to 1000
         if ( iinput(1)>0 ) then

!        --- iinput(1)>0, hence input for commat is defined

            ipmetd = inigettext ( iinput(1), 'iinput' )
            do i = 1, nprob
               do j= 1, lniincommat
                  iincommt(j,i) = ibuffr(ipmetd-1+j+(i-1)*lniincommat)
               end do
            end do

         else

!        --- iinput(1)=0, hence input for commat is not defined

            callcommat = .false.  ! commat must not be called

         end if  ! ( iinput(1)>0 )

      else

!     --- Read iincommt and call commat

         iseqnr = 1
         call read00 ( iincommt, nprob, iseqnr, 0, kmesh, lniincommat )
         if ( debug ) write(irefwr,*) 'after read00'

      end if
      if ( ierror/=0 ) go to 1000

!     --- Adapt variables

      call inichinpsol ( iincommt, lniincommat*nprob )
      if ( debug ) write(irefwr,*) 'after inichinpsol'
      if ( ierror/=0 ) go to 1000

      if ( callcommat ) then

!     --- callcommat true, call subroutine commat

         do iprob = 1, nprob
            ishift = iincommt(9,iprob)
            call incommat ( ibuffr, iincommt(1,iprob),
     +                      kmesh(1,1+ishift), kprob, intmat(1,iprob) )
            intmat(2,iprob) = intmat(2,iprob)+1000*ishift
         end do
         if ( debug ) write(irefwr,*) 'after incommat'

      end if  ! ( callcommat )

!     --- Decrease priorities

      call inipri

1000  call erclos ( 'to0092' )
      if ( isubr==1 ) then
         call erclos( 'sepstr' )
      else if ( isubr==2 ) then
         call erclos( 'sepstl' )
      else
         call erclos( 'sepstn' )
      end if

      end
