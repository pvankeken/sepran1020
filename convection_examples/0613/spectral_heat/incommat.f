      subroutine incommat ( ibuffr, iincommt, kmesh, kprob, intmat )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.2  date 08-10-2007 Extension with decoupled unknowns
!        version  3.1  date 31-08-2007 Extension with jmethod 24-27
!        version  3.0  date 30-04-2003 New body
!        version  2.17 date 11-04-2003 Debug statements
!        version  2.16 date 02-01-2003 Create kmesh part e if it does not exist
!        version  2.15 date 11-10-2002 Correction to prevent integer overflow
!        version  2.14 date 07-06-2002 Initialize isumnp
!        version  2.13 date 18-03-2002 Call to inirst
!        version  2.12 date 07-11-2001 Correction if no curves are available
!        version  2.11 date 01-10-2000 Initialize ipkprp
!        version  2.10 date 13-07-2000 New call to ini010/incm0x
!        version  2.9  date 31-03-2000 Extension for fictitious unknowns
!        version  2.8  date 17-03-2000 Correction for complex matrices
!        version  2.7  date 01-04-1999 New call to iniext
!        version  2.6  date 10-01-1999 New calls to ini011/14
!        version  2.5  date 21-12-1998 New calls to incm01/02
!        version  2.4  date 08-10-1998 Extension for Block ILU
!
!   copyright (c) 1982-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     compute structure of the large matrix
!     Actual body of commat and matstruc
! **********************************************************************
!
!                       KEYWORDS
!
!     matrix_structure
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iincommt(*), kmesh(*), kprob(*), intmat(*), ibuffr(*)

!     ibuffr    i/o  Integer buffer array in which all large data is stored
!     iincommt   i    Integer input array containing information concerning
!                     the structure of the large matrix
!                     See subroutine read00 for a description
!     intmat     o    Standard SEPRAN array containing information of
!                     the structure of the matrices
!     kmesh      i    Standard SEPRAN array containing information of the mesh
!     kprob      i    Standard SEPRAN array containing information of the
!                     problem
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer i, invmat, ipintm, ipkprb, iprob, isum, jmetod,
     +        kamata, jmethod, ipint1, ipin2b,
     +        maxmat, nbound, npboun, npoint, nrusol, isumnp,
     +        nusol, iprint, ibcmat, invmtt, ipin1a, ipin1b, ipin2a,
     +        isumbc, nblocks, lenim2, iblkmeth
      logical split, debug, fillbouncond

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     fillbouncond   If true the part realted to the boundary conditions
!                    must be filled
!     i              Counting variable
!     ibcmat         Indicates whether the matrices necessary for computation
!                    of the reaction forces (S_pu and S_pp (see maver))
!                    are calculated
!     iblkmeth       Type of block storage method
!                    0: none
!                    1: blocks per node
!     invmat         Indicates if the approximate inverse of the matrix has been
!                    built by Kaasschieters method (1) or not (0)
!     invmtt         Temporary for invmat
!     ipin1a         Starting address of array imat part 1a in IBUFFR
!     ipin1b         Starting address of array imat part 1b in IBUFFR
!     ipin2a         Starting address of array imat part 2a in IBUFFR
!     ipin2b         Starting address of array imat part 2b in IBUFFR
!     ipint1         Starting position of first part of array intmat in array
!                    IBUFFR
!     ipintm         Starting address of array imat part 1 in IBUFFR
!     ipkprb         Starting address of array KPROB for actual problem number
!     iprint         See iincommt(3)
!     iprob          Actual problem number
!     isum           length of array imat part 2a
!     isumbc         length of array imat part 2b
!     isumnp         length of array imat part 2a minus the periodical boundary
!                    conditions part
!     jmethod        See input, parameter ichois
!     jmetod         Indicates type of storage scheme for the matrix
!                    Possible values:
!                     1:  Symmetric profile matrix
!                     2:  Non-symmetric profile matrix
!                     3:  Symmetric complex profile matrix
!                     4:  Non-symmetric complex profile matrix
!                     5:  Symmetric compact storage (Real matrix)
!                     6:  Non-symmetric compact storage (Real matrix)
!                     7:  Symmetric compact storage (Complex matrix)
!                     8:  Non-symmetric compact storage (Complex matrix)
!                     9:  Non-symmetric structure compact storage (Real matrix)
!                    10:  Non-symmetric structure compact storage
!                         (Complex matrix)
!                    11:  Compact storage for vectorprocessors (Real matrix)
!                    12:  Compact storage for vectorprocessors (Complex matrix)
!     kamata         Help parameter to store the number of entries in a part of
!                    the large matrix
!     lenim2         length of the array imat part 2
!     maxmat         Help parameter to store the number of entries in the large
!                    matrix
!     nblocks        Number of blocks in the matrix
!     nbound         Number of essential boundary conditions
!     npboun         Number of periodical boundary conditions
!     npoint         Number of nodal points
!     nrusol         Number of free unknowns
!     nusol          Total number of unknowns
!     split          If true splitting of the matrix is allowed
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer inigettext, iniprb

!     CHRIK          Special renumbering algorithm to be used in connection
!                    with the preconditioning of Rik Kaasschieter
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     INCOMMATBOUN   Fill parts of array intmat with respect to
!                    boundary conditions
!     INCOMMATCOMP   Fill structure of matrix in case of a compact matrix
!     INCOMMATPROF   Fill structure of matrix in case of a profile matrix
!     INI060TEXT     Reactivate existing array in IBUFFR
!     INIGETTEXT     Compute pointer of array in IBUFFR
!     INIPRB         Get value of ipkprb as function of iprob
!     PR0004         Print INTMAT corresponding to internal matrix
!     PR0005         Print INTMAT corresponding to boundary conditions
!     PRINTMATSIMPLE Print the structure of the various matrices in case of a
!                    compact storage for the SIMPLE method
!     PRNI21         write differences of integer array with special format
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
!      26   Array KPROB has not been filled
!      75   Array KMESH has not been filled
!    2366   The computed length of the matrix is larger than the largest
!           integer
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
!     --- Initialize some parameters

      call eropen ( 'incommat' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from incommat'

      end if

!     --- Standard tests

      if ( kmesh(2)/=100 ) then
         call errint ( kmesh(2), 1 )
         call errsub ( 75, 1, 0, 0 )
      end if

      jmethod = iincommt(2)
      iprint  = iincommt(3)
      invmtt  = iincommt(4)
      iprob   = iincommt(5)

!     --- Split of invmat (0 or 1) and ibcmat (0 or 1)

      iblkmeth = invmtt/4
      invmat   = mod(invmtt,2)
      ibcmat   = (invmtt-4*iblkmeth)/2
      invmtt   = invmtt-4*iblkmeth
      nblocks = 0
      isumnp = 0
      if ( jmethod>=31 ) then

!     --- Splitting of the matrix is not allowed

         split = .false.
         jmethod = jmethod - 30

      else

!     --- Splitting of the matrix is allowed

         split = .true.

      end if
      if ( intmat(2)/=102 ) then
         do i = 1, 5
            intmat(i) = 0
         end do
      end if

      ipkprb = iniprb(iprob,kprob)

      if ( kprob(ipkprb+2)/=101 ) then
         call errint ( kprob(ipkprb+2), 1 )
         call errsub ( 26, 1, 0, 0 )
      end if

      if ( invmat==1 ) then

!     --- invmat = 1,  Special method of Rik Kaasschieter
!                      Renumber nodes

         call chrik ( ibuffr, kmesh, kprob(ipkprb+1) )

      end if
      nusol = kprob(ipkprb+5)

      if ( jmethod>=20 .and. jmethod<24 ) then

!     --- 20<=jmethod>=23  special situation  full matrices

         intmat(1) = jmethod
         intmat(2) = 102
         intmat(3) = 0
         intmat(4) = 0
         if ( jmethod<22 ) then
            intmat(5) = nusol**2
            if ( intmat(5)/nusol/=nusol ) then

!           --- This is not possible, hence we have integer overflow

               call errsub ( 2366, 0, 0, 0 )
               go to 1000

            end if

         else

            intmat(5) = 2*nusol**2
            if ( intmat(5)/nusol/=2*nusol ) then

!           --- This is not possible, hence we have integer overflow

               call errsub ( 2366, 0, 0, 0 )
               go to 1000

            end if

         end if

      else

!     --- jmethod<20   Standard situation

         isum = 0
         isumbc = 0
         isumnp = 0
         nbound = kprob(ipkprb+10)
         npboun = kprob(ipkprb+34)
         nrusol = nusol-nbound-npboun
         npoint = kmesh(8)
         jmetod = abs(jmethod)
         intmat(1) = jmetod+100*invmtt+(iprob-1)*1000
         fillbouncond = iincommt(12)==0 .and. nbound>0 .or. npboun>0

         if ( debug ) then
            write(irefwr,*) 'nbound, npboun, nusol, nrusol, npoint',
     +                       nbound, npboun, nusol, nrusol, npoint
            write(irefwr,*) 'jmetod, invmtt, iprob ',
     +                       jmetod, invmtt, iprob

         end if
         if ( fillbouncond ) then

!        --- Fill information with respect to boundary conditions

            call incommatboun ( ibuffr, kmesh, kprob(ipkprb+1), intmat,
     +                          ibcmat, isum, isumbc, isumnp )

         end if

         if ( jmetod<=4 .or. jmetod==19 ) then

!        --- jmetod  =  1, 2, 3, 4, 19  profile method

            call incommatprof ( ibuffr, jmetod, kmesh, kprob(ipkprb+1),
     +                          intmat, kamata, iprint, split, maxmat )

         else if ( jmetod<=16 .or. jmetod>=24 .and. jmetod<=27 ) then

!        --- jmetod  =  5 ... 16, 24 .. 27, compact storage

            call incommatcomp ( ibuffr, jmetod, kmesh, kprob(ipkprb+1),
     +                          intmat, kamata, maxmat, iincommt,
     +                          iblkmeth, lenim2, nblocks )

         end if  ! ( jmetod<=16 .or. jmetod>=24 .and. jmetod<=27 )

      end if
      if ( ierror/=0 ) go to 1000
      intmat(5) = kamata+isum+ibcmat*(isumnp+isumbc)
      intmat(2) = 102
      if ( ioutp>=1 .or. mod(iprint,10)>=1 ) then

!     --- ioutp>1   print size of large matrix

         maxmat = maxmat+isum
         if ( jmetod==3 .or. jmetod==4 .or. jmetod==8 .or.
     +        jmetod==10 .or. jmetod==12 ) then

!        --- complex matrix

            write(irefwr, 610) maxmat
610         format(' the large matrix contains', i10,' complex entries')

         else

!        --- real matrix

            write(irefwr, 620) maxmat
620         format(' the large matrix contains', i10, ' real entries')

         end if
      end if
      if ( nbound==0 .and. npboun==0 ) intmat(4) = 0
      if ( debug .or. mod(iprint,10)>=2 ) then

!     --- print first part of array intmat

         ipint1 = inigettext ( intmat(3), 'intmat-1' )
         ipintm = ipint1+nrusol
         if ( jmetod>4 .and. jmetod<=12 ) ipintm = ipintm+1
         if ( jmetod>=13 .and. jmetod<=16 .or.
     +        jmetod>=24 .and. jmetod<=27 ) then

!        --- jmethod = 13-16, special storage for simple
!                      24-27, storage for AL

            call printmatsimple ( jmetod, ibuffr(ipint1) )

         else

!        --- standard storage

            call pr0004 ( jmetod, nrusol, ibuffr(ipint1),
     +                    ibuffr(ipintm) )

         end if

         if ( nblocks>0 ) then

!        --- nblocks>0, print block information

            ipint1 = ipintm+1+lenim2
            write(irefwr,*) ' Number of unknowns per block '
            call prni21 ( nblocks, ibuffr(ipint1) )
            ipint1 = ipint1+nblocks+1
            ipintm = ipint1+nblocks+1
            write(irefwr,*) ' information of block structure'
            call pr0004 ( jmetod, nblocks, ibuffr(ipint1),
     +                    ibuffr(ipintm) )

         end if

      end if
      if ( nbound>0 .and. (debug .or. mod(iprint,10)>=3) ) then

!     --- print second part of array intmat

         call ini060text ( ibuffr, intmat(4), 'intmat-2' )
         ipin1a = inigettext ( intmat(4), 'intmat-2' )
         ipin1b = ipin1a+ibcmat*(nbound+1+npboun)
         ipin2a = ipin1a+nbound+1+npboun+ibcmat*(nbound+1)
         ipin2b = ipin1b+nbound+1+ibuffr(ipin1a+nbound+npboun)
         write(irefwr,*) ' boundary conditions i, coupled with',
     +                   ' internal unknowns'
         call pr0005 ( nbound, ibuffr(ipin1a),
     +                 ibuffr(ipin2a), nrusol+npboun )
         if ( ibcmat==1 ) then
            write(irefwr,*) ' boundary conditions i,',
     +           ' coupled with boundary conditions'
            call pr0005 ( nbound, ibuffr(ipin1b),
     +                    ibuffr(ipin2b), nrusol+npboun )
         end if

      end if

      if ( npboun>0 .and. (debug .or. mod(iprint,10)>=3) ) then

!     --- print second part of array intmat corresponding to periodical bc's

         call ini060text ( ibuffr, intmat(4), 'intmat-2' )
         ipin1a = inigettext ( intmat(4), 'intmat-2' )
         ipin2a = ipin1a+(1+ibcmat)*(nbound+1)+npboun
         write(irefwr,*) ' periodical boundary conditions i,',
     +                   ' coupled with internal unknowns'
         call pr0005 ( npboun, ibuffr(ipin1a+nbound),
     +                 ibuffr(ipin2a), nrusol )

      end if
1000  call erclos ( 'incommat' )
      if ( debug ) write(irefwr,*) 'End incommat'

      end
