      program boussdec

!     --- Main program for decoupled Boussinesq equations
!         To link this program use:
!
!         seplink boussdec

      implicit none
      call sepcom(0)
      end

!     --- Function subroutine func is used to create an initial temperature

      function func ( ichois, x, y, z )
      implicit none
      integer ichois
      double precision func, x, y, z
      if ( ichois==1 ) then

!     --- ichois = 1, the temperature is set equal to 1-x

         func = 1 - x

      end if

      end
