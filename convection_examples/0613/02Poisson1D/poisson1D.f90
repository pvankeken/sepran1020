program poisson1D
implicit none
integer(kind=8),parameter :: NBUFDEF=500000000_8
integer,allocatable :: ibuffr(:)
integer :: error

allocate(ibuffr(NBUFDEF),stat=error)
if (error /= 0) then
   write(6,*) 'PERROR(poisson1D): unable to allocate space'
   write(6,*) 'for ibuffr: NBUFDEF = ',NBUFDEF
   stop
endif

call sepcombf(ibuffr,ibuffr,NBUFDEF)

if (allocated(ibuffr)) deallocate(ibuffr)
      
end

subroutine useroutbf(ibuffr,buffer,kmesh,kprob,isol,isequence,numvec)
implicit none
integer :: ibuffr(:)
real(kind=8) :: buffer(:)
integer kmesh(:),kprob(:),isol(:)
integer isequence,numvec
real second

write(6,'(''cpu time is : '',f12.2)') second()
      
end
