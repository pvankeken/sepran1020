c *************************************************************
c *   GMTout
c *
c *   Interpolate temperature to regular set of grid points for
c *   plotting with GMT
c *
c *   PvK 050304
c *************************************************************
      subroutine GMTout(kmesh,kprob,isol,nout)
      implicit none
      integer kmesh(*),kprob(*),isol(*),nout
      integer map1(5)
      integer NUNK1,NDIM,ix,iy,ip,NPX,NPY,np,i
      integer iinmap(5)
      integer NMAX,NTOT
      parameter(NMAX=401*401,NDIM=2,NUNK1=1)
      real*8 temp(1,NMAX),coor(2,NMAX),DPX,DPY,x,y,r
      character*80 fname
      include 'ccc.inc'
      include 'cpix.inc'


      logical first
      save first,iinmap
      data first/.true./

      iinmap(1)=2
      iinmap(2)=2
      if (first) then
         iinmap(2)=1
      endif

c     *** Define regular grid for interpolation
      if (r2.ne.1d0) then
         write(6,*) 'PERROR(GMTout): r2 /= 1'
         write(6,*) ' r2 = ',r2    
         call instop
      endif
      NPY=41
      NPX=rlampix*(NPY-1)+1
      DPX=rlampix/(NPX-1)
      DPY=1d0/(NPY-1)
      NTOT = NPX*NPY
      if (NTOT.gt.NMAX) then
         write(6,*) 'PERROR(GMTout): NTOT>NMAX' 
         call instop
      endif
      ip=0
      do iy=1,NPY
         y = (NPY-iy)*DPY
         do ix=1,NPX
            ip=ip+1
            x = (ix-1)*DPX
            coor(1,ip) = x
            coor(2,ip) = y
         enddo
      enddo
      open(9,file='GMT.log')
      write(9,*) 'Regular grid output with resolution ',NPX,NPY
      write(9,*) '   and grid spacing ',DPX,DPY
      write(9,*) '   aspect ratio ',rlampix
      write(9,*) DPX
      close(9)

c     *** compute interpolation. At first pass fill map1 for more
c     *** efficient interpolation on subsequent calls 
      call intcoor( kmesh, kprob, isol, temp, coor,
     v              NUNK1, NTOT, NDIM, iinmap, map1)     
      if (first) then
c        *** output coordinates
         write(6,*) 'output coordinates'
         open(9,file='GMT/coor.dat') 
         ip=0
         do ip=1,NTOT
            write(9,'(i5,2f15.7)') ip,coor(1,ip),coor(2,ip)
         enddo
         close(9)
         first = .false.
         iinmap(2)=2
      endif
      
c     *** Output temperature
      write(6,*) 'output interpolated T'
      write(fname,'(''GMT/T'',i3.3,''.dat'')') nout
      open(9,file=fname)
      do ip=1,NTOT
         x = coor(1,ip)
         y = coor(2,ip)
         r = sqrt(x*x+y*y)
!        if (r.lt.r1.or.r.gt.r2) temp(1,ip)=-1d0
         write(9,*) temp(1,ip)
      enddo
      close(9)

      return
      end
