c *************************************************************
c *   BMOUT
c *
c *   output of program BM
c *
c *   PvK 120490
c *************************************************************
      subroutine bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,isol3,
     v                 iuser,user)
      implicit none
      integer NFUNC,NPUS,DONE,DZERO
      parameter(NFUNC=505,NPUS=200000,DONE=1d0,DZERO=0d0)
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),iuser(*),ivelx(5),ively(5),isol3(*)
      real*8 user(*),funcx(NFUNC),funcy(NFUNC)
      integer igradt(5),icurvs(3)
      integer ivser(100)
      real*8 vser(NPUS)
      include 'pesteady.inc'
      include 'peparam.inc'
      include 'pecpu.inc'
      include 'solutionmethod.inc'
      include 'depth_thermodyn.inc'
      include 'ccc.inc'
      include 'SPcommon/cmacht'
      include 'bound.inc'
      include 'plotpvk.inc'
      include 'SPcommon/cplot'
      include 'bstore.inc'
      include 'dimensional.inc'
      include 'cpephase.inc'
      include 'pecof900.inc'
      include 'pecof800.inc'
      include 'compression.inc'
      include 'cjarvis.inc'
      integer npoint,ir,nunkp,nx,ny,ihelp,ncntln,numarr,iinvec(20)
      real*8 temp1,gnus1,gnus2,gnus3,gnus4,q1,q2,q3,q4
      real*8 vrms2,vrms,volint,funccf,z,rkap1,rkap2,phiav
      real*8 format,x,y,psiphi,bounin,contln(10)
      real*8 rinvec(2),q_a(20),q_a_d(20)
      integer nq_a,ivisdip(5),iphi(5),i,icompwork(5,3)
      integer ncoorc(100),input,iplots(20),iuser_dum(100),iinder(10)
      real*8 rplots(10),coorcn(10),veloc(2),veloc_d(2),vrms_d,NuwT
      real*8 avT(2),avphi(2),avwork(2),gnusj,avphi2(2),gn1,gn2,dgn
      integer ichois,icheld,ix,jdegfd,ivec
      logical didnotconverge
      save iphi,ivisdip,icompwork,iinvec,rinvec,ivser,vser
      data (iinvec(i),i=1,20)/20*0/
      data contln(1)/10/
     
      
      funcx(1) = NFUNC
      funcy(1) = NFUNC
      ivser(1) = 100
       vser(1) = NPUS
      do i=2,ivser(1)
         ivser(i)=0
      enddo
      do i=2,int(vser(1))
         vser(i)=0d0
      enddo
      npoint = kmesh2(8)
      if (5+npoint.gt.NPUS) then
         write(6,*) 'PERROR(bmout): npoint > NPUS'
         call instop
      endif
      if (Nu_with_Tbar) then
c        *** compute total temperature (potential + adiabatic contribution)
         iinvec(1)=5
         iinvec(2)=32
         iinvec(3)=0
         iinvec(4)=0
         iinvec(5)=1
         call manvec(iinvec,rinvec,isol2,isol2,isol3,kmesh2,kprob2)
         call nusselt(kmesh2,kprob2,isol3,iuser,user,q_a,nq_a,q_a_d)
      else
         call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      endif
       
      if (numnat800==1 .or. insulbot) then
c       *** <Tbot> is not necessarily 1
        temp1=q_a(3)
        gnus1=q_a(1)/temp1
        gnus2=q_a(2)/temp1
      else if (iadiabat.gt.1) then
        temp1=q_a(3)
        gnus1=q_a(1)/temp1
        gnus2=q_a(2)/temp1
      else
        gnus1=q_a(1)
        gnus2=q_a(2)
        temp1=q_a(3)
      endif

      q2 = q_a(5)
      q1 = q_a(6)

      q4 = q_a(7)
      q3 = q_a(8)
      call nusseltwT(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v               iuser,user,NuwT)
      NuwT=1+volint(0,1,1,kmesh1,kprob1,isol1,iuser,user,
     v            user,ihelp)/volume


!     write(6,*) 'before pevrms9: ',ivser(1),vser(1),iuser(1),user(1)
      call pevrms9(isol1,kmesh1,kprob1,ivser,vser)
!     write(6,*) 'before volint : ',ivser(1),vser(1),iuser(1),user(1)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,ivser,
     v               vser,ihelp)/volume
      vrms = sqrt(abs(vrms2))
      vrms_d = vrms*rkappa_dim/height_dim*100*year_dim
      write(6,*) 'vrms: ',vrms
      write(6,*) 'before averageT-1: ',ivser(1),vser(1),iuser(1),user(1)
      call averageT(1,isol2,kmesh2,kprob2,iuser,user,avT)
      if (Di.gt.0) then
          call compressionwork(kmesh1,kmesh2,kprob1,kprob2,
     v                     isol1,isol2,icompwork)
c     write(6,*) 'before averageT-2: ',ivser(1),vser(1),iuser(1),user(1)
        call averageT(3,icompwork,kmesh2,kprob2,iuser,user,avwork)

        call phifromgrad(kmesh1,kprob1,isol1,ivisdip)
      write(6,*) 'before averageT-3: ',ivser(1),vser(1),iuser(1),user(1)
        call averageT(2,ivisdip,kmesh2,kprob2,iuser,user,avphi)

        avphi(1) = avphi(1)*DiRa
        avphi2(1) = avphi2(1)*DiRa
        avwork(1) = avwork(1)*Di
        write(6,*) '<phi> : ',avphi(1),avphi2(1)
        write(6,*) '<work>: ',avwork(1)

        call check_stress_tensor(kmesh1,kprob1,isol1,iuser,user)
      endif

      call surfacevel(kmesh1,kprob1,isol1,itop,veloc,veloc_d)

      didnotconverge=.false.
      if (niter.eq.nsteadymax) then
          write(6,*) 'PWARN(bmout): '
          write(6,*) 'Steady state calculation did not converge'
          didnotconverge = .true.
          write(6,*) 
      endif
      write(6,*)
      write(6,'(''Niter  '',i12,f12.2)') niter,cput
      write(6,'(''cpu    '',3f12.2)') cpu1,cpu2,cpu3
      write(6,'(''Nu     '',4f12.7,2f8.3,f10.1)') gnus1,gnus2,NuwT,
     v         temp1,q_a_d(1),q_a_d(2),q_a_d(3)
      write(6,'(''Vrms   '',f12.3,f12.7)') vrms,vrms_d
      write(6,'(''q1     '',4f12.7)') q1,q3,q_a_d(6),q_a_d(5)
      write(6,'(''q2     '',2f12.7)') q2,q4,q_a_d(7),q_a_d(8)
      write(6,'(''avT    '',f12.7,f12.3)') avT(1),avT(2)
      if (Di.gt.0) then
        write(6,'(''<phi>    '',2e15.7,f12.1)') avphi(1),avwork(1),
     v           (avphi(1)-avwork(1))/avwork(1)*100d0
      endif
      write(6,'(''Maximum surface velocity '',2f12.7)') 
     v            veloc(1),veloc_d(1)
      write(6,'(''Average surface velocity '',2f12.7)') 
     v            veloc(2),veloc_d(2)

      write(6,*) 'Difference in heat flow through top and bottom: '
      gn1 = gnus2*surf(1)
      gn2 = gnus1*surf(2)
      dgn = (abs(gn1+gn2)/max(gn1,gn2))*100
      write(6,'(3f12.5)') gn1,gn2,dgn
      write(6,*)
      gnusj = avT_cond(1)/avT(1)
      if (Di.gt.0d0) then
         write(6,101) Ra,Di,tala,gnus1,vrms,
     v          veloc(1),veloc(2),avT(1),avphi(1),avwork(1),
     v          (avphi(1)-avwork(1))/avwork(1)*100,avphi(1)/gnus1,
     v          dgn,didnotconverge
      else 
         write(6,102) Ra,Di,tala,gnus1,vrms,
     v          veloc(1),veloc(2),avT(1),dgn
      endif
101   format(es11.2,f8.2,L3,10f8.3,L3)
102   format(es11.3,f8.3,L3,f10.2,3f10.1,e11.3)



      jkader=-1
      format = 10d0
      fname = 'TEMP.000'
      x=0d0
      y=0d0
      ncntln=0
!     jtimes=1
      call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,DONE,DZERO)

!     jtimes=3
      call plotc1(1,kmesh2,kprob2,isol2,contln,ncntln,format,DONE,1)
!     jtimes=0
      
c     write(6,*) 'Di: ',Di
      if (Di.gt.0) then
        fname = 'PHI.000'
        iplots(1)=4
        iplots(2)=1
        iplots(3)=1
        iplots(4)=0
        rplots(1)=format
        rplots(2)=1d0
        ncoorc(1)=100
        call plotcn(kmesh1,kprob1,ivisdip,iplots,rplots,contln,input,
     v            'phi',ncoorc,coorcn)
        fname = 'WORK.000'
        iplots(1)=4
        iplots(2)=1
        iplots(3)=1
        iplots(4)=0
        rplots(1)=format
        rplots(2)=1d0
        ncoorc(1)=100
        call plotcn(kmesh2,kprob2,icompwork,iplots,rplots,contln,input,
     v            'work',ncoorc,coorcn)
c       call chtype(2,kprob1)
c       fname = 'PHI1.000'
c       ichois = 2
c       icheld = 2
c       ix = 0
c       jdegfd = 0
c       ivec = 1
c       iinder(1) = 7
c       iinder(2) = 1
c       iinder(3) = 0
c       iinder(4) = 2
c       iinder(5) = 0
c       iinder(6) = 0
c       iinder(7) = 1
c       call deriv(iinder,iphi,kmesh1,kprob1,isol1,iuser_dum,user)
c       call chtype(1,kprob1)
c       call plotcn(kmesh1,kprob1,iphi,iplots,rplots,contln,input,
c    v              'log10(phi)',ncoorc,coorcn)
      endif

      call averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,user,
     v              0,0,0)

 
      f2name='restart.back'
      call openf2(.true.)
      call writbs(0,1,numarr,'temp',isol2,kprob2)
      call writbs(0,2,numarr,'velo',isol1,kprob1)
      call writb1
      close(2)

      return
      end

      subroutine averageT(ichoice,isol,kmesh,kprob,iuser,user,avT)
      implicit none
      real*8 user(*),avT(*)
      integer isol(*),kmesh(*),kprob(*),iuser(*)
      real*8 volint
      integer ihelp,i,ichoice
      include 'pecof800.inc'
      include 'ccc.inc'
      include 'dimensional.inc'
      include 'compression.inc'
 
      iuser(2)=1
      iuser(6)=7
      iuser(7)=intrule
      iuser(8)=icoorsystem
      iuser(9)=0
c     *** specify f
      iuser(10)=-6
       user(6)=1d0
      do i=11,min(100,iuser(1))
         iuser(i) = 0
      enddo

      avT(1) = volint(0,2,1,kmesh,kprob,isol,iuser,user,ihelp)
      avT(1) = avT(1)/volume
      if (ichoice.eq.1) then
c        *** temperature
         avT(2) = avT(1)*deltaT_dim
      else if (ichoice.eq.2) then
c        *** viscous dissipation (unit: work = J/s)
c        *** This needs work
c        avT(2) = rho_dim*avT(1)/height_dim
c        avT(2) = avT(2)*rkappa_dim*rkappa_dim*rkappa_dim
      else if (ichoice.eq.3) then
c        *** vertical advective heat transport
c        *** This needs work
c        avT(2) = rho_dim*cp_dim*DeltaT_dim*avT(1)
c        avT(2) = avT(2)*height_dim*height_dim
      else if (ichoice.eq.4) then
         avt(2) = avT(1)*rho_dim
      else if (ichoice.eq.5) then
         avT(2) = avT(1)*rkappa_dim
      else if (ichoice.eq.6) then
         avT(2) = avT(1)*alpha_dim
      endif

      return 
      end
c *************************************************************
c *   COMPRESSIONWORK
c *   Compute the amount of work done against hydrostatic pressure
c *   Make sure to define the variability of alpha,rho,c_p in exactly
c *   the same way as specified in coef800().
c *   kmesh1,kmesh2    i   Mesh description for velocity and temperature
c *   kprob1,kprob2    i   Problem description for same
c *   isol1,isol2      i   Arrays containing velocity and temperature
c *   icompwork        o   Array containing the work in each nodal point
c *
c *   PvK 091805
c *************************************************************
      subroutine compressionwork(kmesh1,kmesh2,kprob1,kprob2,
     v             isol1,isol2,icompwork)
      implicit none
      integer kmesh1(*),kprob1(*),kmesh2(*),kprob2(*),isol1(*),isol2(*)
      integer icompwork(5,*)
      integer inpcre(20)
      integer indprf1,ipkprf1,iniget,inidgt,ipusol1,ipusol2,iwork
      integer npoint,nunkp,ipkelmi,ipwork,ipwork2,ipwork3
      real*8 rincre(20)
      integer ibuffr 
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (buffr(1),ibuffr(1))

         inpcre(1)=6
         inpcre(2)=2
         inpcre(3)=1
         inpcre(4)=1
         inpcre(5)=0
         inpcre(6)=0

      if (icompwork(1,1).eq.0) then
         call creatv(kmesh2,kprob2,icompwork,inpcre,rincre)
      endif
      if (icompwork(1,2).eq.0) then
         call creatv(kmesh2,kprob2,icompwork(1,2),inpcre,rincre)
      endif
      if (icompwork(1,3).eq.0) then
         call creatv(kmesh2,kprob2,icompwork(1,3),inpcre,rincre)
      endif

c     *** Velocity may have variable numbers of dof (element 903)
      indprf1 = kprob1(19)
      if (indprf1.ne.0) then
         call ini070(indprf1)
         ipkprf1 = iniget(indprf1)
      endif

c     *** make sure solution vectors and work vector are active
      call ini070(isol1(1))
      call ini070(isol2(1))
      call ini070(icompwork(1,1))
      call ini070(icompwork(1,2))
      call ini070(icompwork(1,3))
      ipusol1 = inidgt(isol1(1))
      ipusol2 = inidgt(isol2(1))
      ipwork  = inidgt(icompwork(1,1))
      ipwork2 = inidgt(icompwork(1,2))
      ipwork3 = inidgt(icompwork(1,3))
      npoint = kmesh1(8)
      nunkp = kprob1(4)
      call ini070(kmesh2(23))
      ipkelmi = inidgt(kmesh2(23))

      call compressw01(npoint,buffr(ipusol1),buffr(ipusol2),
     v                 buffr(ipwork),buffr(ipwork2),buffr(ipwork3),
     v                 buffr(ipkelmi),indprf1,ibuffr(ipkprf1),nunkp)

      end

      subroutine compressw01(npoint,sol1,sol2,work,w2,T2,coor,
     v              indprf,kprobf,nunkp)
      implicit none
      integer npoint,nunkp,indprf,kprobf(*)
      real*8 sol1(*),sol2(*),work(*),coor(2,*),w2(*),T2(*)
      integer i,j1
      real*8 x,y,z,funccf,w,temp,rho,c_p,alpha,adiabat
      include 'dimensional.inc'
      include 'depth_thermodyn.inc'
      include 'pecof900.inc'
  
c     write(6,*) 'Ts_dim: ',Ts_dim,Ts_dim/DeltaT_dim,DeltaT_dim
      c_p = 1d0
      if (indprf.eq.0) then
         do i=1,npoint
c           *** get vertical velocity
            j1 = (i-1)*nunkp +2
            w = sol1(j1) 
c           *** get local density
            x = coor(1,i)
            y = coor(2,i)
            z = 0d0
            if (compress) then
               rho = funccf(3,x,y,z)
            else 
               rho = 1d0
            endif
            if (ialphatype.eq.1) then
               alpha = funccf(5,x,y,z)
            else
               alpha = 1d0
            endif
c           *** make sure to add the contribution of the surface T
c           temp = sol2(i)+(Ts_dim-adiabat(1d0))/DeltaT_dim
            temp = sol2(i)
c           *** vertical advective transport 
            work(i) = alpha*rho*c_p*temp*w
            w2(i) = w*w
            T2(i) = temp*temp
c           write(6,'(''work: '',6f8.3)') x,y,rho,temp,w,work(i)
          enddo
       else
          do i=1,npoint
            j1 = kprobf(i) + 2
            w = sol1(j1)
            x = coor(1,i)
            y = coor(2,i)
            z = 0d0 
            if (compress) then
               rho = funccf(3,x,y,z)
            else
               rho = 1d0
            endif
            if (ialphatype.eq.1) then
               alpha = funccf(5,x,y,z)
            else
               alpha = 1d0
            endif
c           temp = sol2(i)+(Ts_dim-adiabat(1d0))/DeltaT_dim
            temp = sol2(i)
            work(i) = alpha*rho*c_p*temp*w
            w2(i) = w*w
            T2(i) = temp*temp
c           write(6,'(''work: '',6f8.3)') x,y,rho,temp,w,work(i)
          enddo
       endif
       return
       end
  

       subroutine check_stress_tensor(kmesh1,kprob1,isol1,iuser,user)
       implicit none
       integer kmesh1(*),kprob1(*),isol1(*),iuser(*)
       real*8 user(*)
       integer igradv(5),idivv(5),istress(5)
       integer iinder(200)
       real*8 anorm,volint,eps,pressure
       real*8 gradv11,gradv12,gradv22,div11,stress11,stress12,stress22
       integer ihelp,nparm,iwork(4,100),i
       real*8 work(100)
       include 'pecof900.inc'   
       include 'solutionmethod.inc'
    
       iuser(1)=100
        user(1)=100
       iinder(1)=8
       iinder(2)=1
       iinder(3)=0
       iinder(5)=0
       iinder(6)=0
       iinder(7)=0
       iinder(8)=2
        iuser(6)=7
c     ** itime, modelv, intrule, icoor, mcontv
        iuser(2)=1
        iuser(7)=0
        iuser(8)=1
        iuser(9)=intrule900
       iuser(10)=icoor900
       iuser(11)=mcontv
c      *** eps, rho
       iuser(12)=-6
       if (compress) then
          iuser(13)=3
       else
          iuser(13)=-7
       endif
c      *** omega,f1,f2,f3,eta
       iuser(14)=0
       iuser(15)=0
       iuser(16)=0
       iuser(17)=0 
       iuser(18)=-7
       if (isolmethod.eq.0) then
          user(6)=1d-6
       else
          user(6)=0d0
       endif
       user(7)=1d0


       write(6,*) 'compute grad v'
c      *** icheld = 2 for grad v
       iinder(4)=2
       call deriv(iinder,igradv,kmesh1,kprob1,isol1,iuser,user)

c      *** icheld = 4 for div v
       iinder(4)=4
       write(6,*) 'compute div v'
       call deriv(iinder,idivv,kmesh1,kprob1,isol1,iuser,user)


       do i=1,100
          iwork(1,i)=0
          iwork(2,i)=0
          iwork(3,i)=0
          iwork(4,i)=0
           work(i) = 0d0
       enddo
       iwork(1,1) = 0
       iwork(1,2) = 1
       iwork(1,3) = intrule900 
       iwork(1,4) = icoor900
       iwork(1,5) = mcontv
       if (isolmethod.eq.0) then
c         *** penaltyfunction parameter
          eps = 1d-6
       else
          eps = 0
       endif
c      *** 6: epsilon
       iwork(1,6) = 0
        work(6) = eps
c      *** 7: rho
       if (compress) then
          iwork(1,7) = 3
       else 
          iwork(1,7) = 0
           work(7)   = 1d0
       endif
c      *** 12: eta assumed constant 1 here
       iwork(1,12) = 0
        work(12)   = 1d0

       do i=6,iuser(1)
          iuser(i)=0
       enddo
       do i=6,user(1)
          user(i)=0d0
       enddo
c      write(6,*) 'before fil103: iuser/user:'
c      do i=1,20 
c         write(6,*) i,iuser(i),user(i)
c      enddo
       nparm=12
       call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1)
c      write(6,*) 'after fil103: iuser/user:'
c      do i=1,20 
c         write(6,*) i,iuser(i),user(i)
c      enddo
c      *** icheld = 6 for the stress tensor
       write(6,*) 'compute stress tensor'
       iinder(4)=6
       call deriv(iinder,istress,kmesh1,kprob1,isol1,iuser,user)


       gradv11 = anorm(1,1,1,kmesh1,kprob1,igradv,igradv,ihelp)
       gradv12 = anorm(1,1,2,kmesh1,kprob1,igradv,igradv,ihelp)
       gradv22 = anorm(1,1,4,kmesh1,kprob1,igradv,igradv,ihelp)
       div11   = anorm(1,1,1,kmesh1,kprob1,idivv,idivv,ihelp)
       stress11= anorm(1,1,1,kmesh1,kprob1,istress,istress,ihelp)
       stress22= anorm(1,1,2,kmesh1,kprob1,istress,istress,ihelp)
       stress12= anorm(1,1,4,kmesh1,kprob1,istress,istress,ihelp)
       pressure= anorm(1,1,3,kmesh1,kprob1,isol1,isol1,ihelp)
       write(6,*) 'NORMS: '
       write(6,*) 'grad v = ',gradv11,gradv22,gradv12
       write(6,*) 'div  v = ',div11
       write(6,*) 'stress = ',stress11,stress22,stress12
       write(6,*) 'first:   ',2*gradv11-2d0/3*div11,stress11
       write(6,*) 'second:  ',2*gradv22-2d0/3*div11,stress22
       write(6,*) 'pressure:',pressure

c      gradv11 = volint(0,1,1,kmesh1,kprob1,igradv,iuser,user,ihelp)
c      gradv12 = volint(0,1,2,kmesh1,kprob1,igradv,iuser,user,ihelp)
c      write(6,*) 'gradv22: '
c      gradv22 = volint(0,1,4,kmesh1,kprob1,igradv,iuser,user,ihelp)
c      div11   = volint(0,1,1,kmesh1,kprob1,idivv,iuser,user,ihelp)
c      stress11= volint(0,1,1,kmesh1,kprob1,istress,iuser,user,ihelp)
c      stress22= volint(0,1,2,kmesh1,kprob1,istress,iuser,user,ihelp)
c      write(6,*) 'stress12: '
c      stress12= volint(0,1,4,kmesh1,kprob1,istress,iuser,user,ihelp)

c      write(6,*) 'VOLUME INTEGRALS: '
c      write(6,*) 'grad v = ',gradv11,gradv22,gradv12
c      write(6,*) 'div  v = ',div11
c      write(6,*) 'stress = ',stress11,stress22,stress12
c      write(6,*) 'first:   ',2*gradv11-2d0/3*div11,stress11
c      write(6,*) 'second:  ',2*gradv22-2d0/3*div11,stress22

       call checkstress001(kmesh1,kprob1,isol1,idivv,igradv,istress)

       return
       end

       subroutine checkstress001(kmesh,kprob,isol,idivv,igradv,istress)
       implicit none
       integer kmesh(*),kprob(*),isol(*),idivv(*),igradv(*),istress(*)
       integer ibuffr(1)
       common ibuffr
       real*8 buffr(1)
       equivalence(ibuffr(1),buffr(1))
       integer iniget,inidgt,ipcoor,ipdiv,ipgrad,ipstress,i
       integer ndef_div,ndef_grad,ndef_stress,npoint,ippres
       integer indprf,nunkp,ipkprf

       
       if (idivv(2).ne.115) then
          write(6,*) 'PERROR(checkstress001): idivv(2) <> 115'
          write(6,*) 'idivv: ',(idivv(i),i=1,5)
          call instop
       endif
       if (igradv(2).ne.115) then
          write(6,*) 'PERROR(checkstress001): igradv(2) <> 115'
          write(6,*) 'igradv: ',(igradv(i),i=1,5)
          call instop
       endif
       if (istress(2).ne.115) then
          write(6,*) 'PERROR(checkstress001): istress(2) <> 115'
          write(6,*) 'istress: ',(istress(i),i=1,5)
          call instop
       endif

       call ini070(idivv(1))
       call ini070(igradv(1))
       call ini070(istress(1))
       call ini070(kmesh(23))
       call ini070(isol(1))
       indprf = kprob(19)
       if (indprf.ne.0) then
          call ini070(indprf)
       endif
       npoint = kmesh(8)
       ipcoor = inidgt(kmesh(23))
       ipdiv  = inidgt(idivv(1))
       ipgrad = inidgt(igradv(1))
       ipstress = inidgt(istress(1))
       ippres = inidgt(isol(1))
       ndef_div = idivv(5)/npoint
       ndef_grad = igradv(5)/npoint
       ndef_stress = istress(5)/npoint
       nunkp = kprob(4)
       write(6,*) 'ndef: ',npoint,ndef_div,ndef_grad,ndef_stress

       call checkstress002(buffr(ipcoor),npoint,buffr(ipdiv),ndef_div,
     v          buffr(ipgrad),ndef_grad,buffr(ipstress),ndef_stress,
     v          buffr(ippres),nunkp,ibuffr(ipkprf),indprf)
       return
       end

       subroutine checkstress002(coor,npoint,div,ndiv,grad,ngrad,
     v              stress,nstress,uvp,nunkp,kprobf,indprf)
       implicit none
       integer npoint,ndiv,ngrad,nstress,nunkp,indprf,kprobf(*)
       real*8 coor(2,npoint),div(ndiv,npoint),grad(ngrad,npoint)
       real*8 stress(nstress,npoint),sep_stress11,dstress11,stress11
       real*8 uvp(*)
       integer i,j1
       real*8 stress11tot,sep_stress11tot,divtot,dstress11tot
       real*8 presstot,press
       include 'dimensional.inc'

       stress11tot=0
       divtot=0
       dstress11tot=0
       sep_stress11tot=0
       presstot=0
       do i=1,npoint
          stress11 = 2*grad(1,i) - 2d0/3d0*div(1,i)
          sep_stress11 = stress(1,i)
          dstress11 = (sep_stress11-stress11)
          stress11tot = stress11tot+abs(stress11)
          sep_stress11tot = sep_stress11tot+abs(sep_stress11)
          dstress11tot = dstress11tot+abs(dstress11)
          divtot = divtot+abs(div(1,i))
          if (indprf.eq.0) then
             j1 = (i-1)*nunkp+3
          else 
             j1 = kprobf(i)+3
          endif
          press = uvp(j1)
          presstot = presstot + abs(press)
c         write(6,'(''stress11: '',4e15.7)') stress11,
c    v            sep_stress11,dstress11,div(1,i)
      enddo
 
      stress11tot = stress11tot/npoint
      sep_stress11tot = sep_stress11tot/npoint
      dstress11tot = dstress11tot/npoint
      divtot = divtot/npoint
      presstot = presstot/npoint
      write(6,'(''stress11: '',5f12.3)') stress11tot,sep_stress11tot,
     v          dstress11tot,divtot,presstot
      write(6,*) 'dimensional pressure average: ',
     v   presstot*pressure_scale
 
      return
      end 

