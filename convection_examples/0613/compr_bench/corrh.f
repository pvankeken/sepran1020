c *************************************************************
c *   CORRH
c *
c *   Solves    .
c *           M T  + S T  = f
c *   using a CN scheme. The system is non-linear, S depends on time.
c *************************************************************
c *   Parameters:
c *      kmesh     i   Standard Sepran array
c *      kprob     i   Standard Sepran array
c *      intmat    i   Information of the large matrix for this problem
c *      isol      o   Solution vector T(n+1)
c *      islold    i   Old solution vector T(n): output of predictorstep
c *      (i)user   i   Contains information on coefficients
c *      numold    i   number of solutionvectors in ivcold
c *      ivcold    i   'old solution array' used to transport 
c *                    information between problems
c *      matrs     i   Stiffness matrix S(n)
c *      matrm     i   Mass matrix (assumed to be constant in time)
c *      irhsd     i   Righthandside vector f(n)
c * 
c *   290889 PvK
c *************************************************************
      subroutine corrh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,matrm,irhsd)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),isol(*),islold(*)
      integer iuser(*),matrs(*),matrm(*),irhsd(*)
      real*8 user(*)
      integer ipser,mrhsd,matrs1,irhs1,ivec1,ivec2,ivec3,ifirst
      real*8 pser
      dimension ipser(100),pser(100),mrhsd(5)
      dimension matrs1(5),irhs1(5),ivec1(5),ivec2(5),ivec3(5)
      character*10 tname

      integer ibuffr
      common ibuffr(1)
      include 'SPcommon/cbuffr'
      include 'SPcommon/ctimen'
      include 'petime.inc'
      integer iinbld(20),ichois,i
      real*8 t2step,p,q,alpha2,beta2,bee
      save ipser,pser,mrhsd,ifirst,iinbld
      save matrs1,irhs1,ivec1,ivec2,ivec3

      data pser(1),ipser(1)/100,100/

      t2step = tstep*0.5d0

c     *** Built matrices and rhs vector at time t
c     *** Stiffness matrix and
      if (idia.eq.2) then
        iinbld(1) = 4
        iinbld(2) = 1
        iinbld(3) = 1
        iinbld(4) = idia
      else
        iinbld(1) = 3
        iinbld(2) = 1
        iinbld(3) = 0
      endif
      call build(iinbld,matrs1,intmat,kmesh,kprob,irhsd,matrm,isol,
     v           islold,iuser,user)

c     *** If irhsd is time-dependent  do something with f(n)
c     *** irhsd = dt/2*(irhsd+irhs1)
c     call algebr(3,0,irhsd,irhs1,irhsd,kmesh,kprob,t2step,t2step,p,q,i)
     
c     *** ivec1 = M*uold  add it to irhsd*dt
c     write(6,*) 'matrm: ',(matrm(i),i=1,5),idia
      ichois=idia+3
      call maver(matrm,islold,ivec1,intmat,kprob,ichois)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,tstep,1d0,p,q,i)

c     *** S1 = M + dt/2*S1
      call addmat(kprob,matrs1,matrm,intmat,t2step,alpha2,1d0,beta2)

c     *** ivec1 = S1*usol(p) ; add it to irshd
c     write(6,*) 'matrs1: ',(matrs1(i),i=1,5),idia
      call maver(matrs1,isol,ivec1,intmat,kprob,6)
      bee = -1d0
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,bee,p,q,i)

c     *** ivec1 = S*uold ; add it to irhsd
c     write(6,*) 'matrs: ',(matrs(i),i=1,5),idia
      call maver(matrs,islold,ivec1,intmat,kprob,5)
      call algebr(3,0,irhsd,ivec1,irhsd,kmesh,kprob,1d0,-t2step,p,q,i)

c     *** solve the system
      call solve(0,matrs1,isol,irhsd,intmat,kprob)

      return
      end
