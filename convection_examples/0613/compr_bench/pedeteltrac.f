c *************************************************************
c *   PEDETELTRAC
c *   Find element information for velocity interpolation
c *     - find element in which this tracer sits
c *       first by using the ielfromgrid lookup mechanism
c *       if this fails, check neighbors of the indicated elements
c *       if that fails too, check all elements (which shouldn't be 
c *             necessary; a warning will be printed if too many
c *             tracers are not found by the first two look up 
c *             mechanisms)
c *     - get coordinates
c *     - check for and fix curvature if necessary
c *     - if necessary, interpolate the solution in (xm,ym)
c *
c *   The mesh is defined using quadratic triangles that may
c *   have curved edges. In order to compute the correct shape functions
c *   we straighten the curved elements by recomputing the coordinates
c *   of the midpoints and reinterpolate the velocity field using quadratic
c *   interpolation. The test whether a tracer is within an element
c *   should therefore be for straight elements.
c *
c *   ic        i     choice parameter to determine 
c *                   0 = determine element # and coordinates
c *                   1 = correct velocity components, element # and coords
c *                   2 = correct first velocity component, element # and coords
c *                 >10 = return with iel=-1 if element couldn't be located
c *                       rather than stop
c *   xm,ym     i     coordinates of the tracer
c *   ikelmc    i     pointer to nodal points information in kmesh
c *   ikelmi    i     pointer to coordinate information in kmesh
c *   ikelmo    i     pointer to kmesh part o (not used yet)
c *   nodno     o     nodal point numbers of this element
c *   nodlin    o     nodal point numbers of linear subelement
c *   rl        o     barycentric coordinates
c *   xn,yn     o     coordinates of nodal point numbers
c *   un,vn     o     velocity components in nodal points (filled if ic>0)
c *   user      i     user array to pass velocity components through
c *   iel       o     element number
c *   npoint    i     number of points in the grid
c *   nelem     i     number of elements in the grid
c *   imissed   o     indicator of number of tracers for which the
c *                   tracer lookup mechanism fails
c * 
c *   PvK 220104
c *************************************************************
      subroutine pedeteltrac(ic,xm,ym,ikelmc,ikelmi,ikelmo,nodno,
     v            nodlin,rl,xn,yn,un,vn,user,iel,npoint,nelem,imissed,
     v            ifound,nelgrp,phiq,xi,eta,guess_first,calling_routine)
      implicit none
      real*8 xm,ym,rl(*),xn(*),yn(*),un(*),vn(*),user(*),phiq(*)
      real*8 xi,eta
      logical guess_first
      integer ikelmc,ikelmi,ikelmo,nodno(*),nodlin(*),iel,isub,imissed
      integer nelem,nelgrp,ifound
      character*(*) calling_routine
      integer npoint,ic,i,iel_now(4),j
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      logical midflag,dotflag,elementcurved
      logical correct,checkinelem,fail
      include 'elem_topo.inc'
      real*8 u,func,xi1,eta1,phiq1(6)
   

      if (guess_first.and.iel.gt.0) then
c          *** use the guess provided in iel
           call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,iel)
           if (curved_elem(iel)) then
              correct=checkinelem(3,xn,yn,xm,ym,nodno,nodlin,rl,
     v              xi,eta,phiq,isub)
           else 
              correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,rl,
     v              xi,eta,phiq,isub)
           endif
           if (correct) ifound = ifound+1
      endif

      if (.not.guess_first.or.iel.le.0.or..not.correct) then
c        *** Guess is not correct
c        *** Look up element number from table prepared by ieltogrid()
         call ielfromgrid(1,xm,ym,ibuffr(ikelmc),buffr(ikelmi),
     v                    nodno,xn,yn,iel_now,iel,phiq,xi,eta)
         if (iel.gt.0) ifound = ifound + 1

      endif

      if (iel.le.0) then
c        *** if this fails, check neighboring elements
c        *** Use kmesh part o to check the neighboring elements
         call check_neighbors(xm,ym,ibuffr(ikelmc),buffr(ikelmi),
     v            ibuffr(ikelmo),ibuffr(ikelmo+nelgrp),nelem,
     v            nodno,nodlin,xn,yn,iel_now,iel,rl,phiq,xi,eta)
      endif

      if (iel.le.0) then
c        *** Desperate measures: loop over all elements. 
         imissed=imissed+1
         iel=1
100      continue
           call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,iel)
           if (curved_elem(iel)) then
              correct=checkinelem(3,xn,yn,xm,ym,nodno,nodlin,rl,
     v              xi,eta,phiq,isub)
           else 
              correct=checkinelem(1,xn,yn,xm,ym,nodno,nodlin,rl,
     v              xi,eta,phiq,isub)
           endif
           if (.not.correct) then
              iel=iel+1
              if (iel.le.nelem) then
                 goto 100
              else
c                *** bail out
                 write(6,*) 'PWARN(pedeteltrac): did not find correct' 
                 write(6,*) 'element for tracer : ',iel,xm,ym,nelem
                 write(6,*) 'called from: ',calling_routine
                 write(6,*) 'r: ',sqrt(xm*xm+ym*ym)
                 call instop()
               endif
           endif
      endif

      if (ic.eq.1) then
         call sper06(user(10),un,nodno)
         call sper06(user(10+npoint),vn,nodno)
      else if (ic.eq.2) then
         call sper06(user(10),un,nodno)
         do i=1,6
            vn(i)=0
         enddo
      else 
         do i=1,6
            un(i)=0
            vn(i)=0
         enddo
      endif
         
      return
      end

