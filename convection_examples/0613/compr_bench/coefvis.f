c *************************************************************
c *   COEFVIS
c *
c *   Make array that contains the viscosity in the nodal points
c *   Used in the definition of viscous dissipation in the
c *   extended Boussinesq approximation.
c *   Cartesian version
c *
c *   PvK 971211/200400
c *************************************************************
      subroutine coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v        ivisc,isecinv,iuser,user)
      implicit none
      integer kmesh1(*),kprob1(*),isol1(*),kmesh2(*),kprob2(*)
      integer isol2(*),ivisc(*),isecinv(*),iuser(*),icall
      real*8 user(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      include 'c1visc.inc'
      include 'vislo.inc'

      integer npoint,i,j,k,iivisc,jdegfd,ix,ielhlp
      integer ind2rf,ind2rh,ind2rp,nphys2,ikelmi,iusol2,iind2rh
      integer inidgt,iisecinv

      npoint = kmesh1(8)

c     *** get information on coordinates and temperature from ibuffr
      call ini050(kmesh1(23),'coefvis: coor')
      call ini050(isol2(1),'coefvis: isol2')
      ikelmi = inidgt(kmesh1(23))
      iusol2 = inidgt(isol2(1))

c     *** make sure viscosity array is available
      call ini050(ivisc(1),'coefvis: ivisc')
      iivisc = inidgt(ivisc(1))
      if (ivn.or.tackley) then
c        *** get second invariant of strain rate tensor
         call deriva(2,10,ix,jdegfd,1,isecinv,kmesh1,kprob1, 
     v               isol1,isol1,iuser,user,ielhlp)
         call ini050(isecinv(1),'coefvis: isecinv')
         iisecinv = inidgt(isecinv(1))
      endif
       
c     *** compute actual viscosity values, store in ivisc()
      call coefv2(buffr(ikelmi),buffr(iusol2),
     v         buffr(iivisc),buffr(iisecinv),npoint)

      return
      end

      subroutine coefv2(coor,temp,visco,secinv,npoint)
      implicit none
      real*8 coor(2,*),temp(*),visco(*),secinv(*)
      integer npoint
      include 'vislo.inc'
      include 'c1visc.inc'

      real*8 x,y,pefvis,temp_l
      integer ival,ivalfind1,i


      ival = 1
      do i=1,npoint
         x = coor(1,i)
         y = coor(2,i)
         if (ivl) then
            ival = ivalfind1(x,y)
         endif
         temp_l = temp(i)
         visco(i) = pefvis(x,y,temp_l,ival,secinv(i)*secinv(i))
      enddo
 
      return
      end
