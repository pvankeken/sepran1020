c******************************************
c 
c     implement "Lenardic filter"
c
c

      subroutine skyfilter (kmesh2,isol2)
      implicit none
      integer kmesh2(*),isol2(5)
      integer ibuffr,ipusol2,inidgt,npoint
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      npoint  = kmesh2(8)

      call ini050(isol2(1),'coefnew: siol2')
      ipusol2 = inidgt(isol2(1))  

      call filter(npoint,buffr(ipusol2))
  


      return
      end



      subroutine filter(npoint,sol2)

      implicit none
      real*8 sol2(*),C_sum0,C_sum1,C_min,C_max,DIST
      integer npoint,NUM,i

      C_sum0  = 0d0
      C_sum1  = 0d0
      C_min   = 1d32
      C_max   = 1d0
      DIST    = 0d0
      NUM     = 0d0


      do i = 1,npoint
         C_sum0 = C_sum0 + sol2(i)

           if(sol2(i).le.0d0) C_min = min(C_min,sol2(i))
           if(sol2(i).gt.1d0) C_max = max(C_max,sol2(i))
      end do

c * ***

      do i = 1,npoint
         if(sol2(i).le.0d0) sol2(i) = 0d0
         if(sol2(i).ge.1d0) sol2(i) = 1d0

         if(sol2(i).le.dabs(C_min)) sol2(i) = 0d0
         if(sol2(i).ge.2d0-C_max) sol2(i) = 1d0
      end do

c * ***       

      do i = 1,npoint
         if(sol2(i).ne.0.and.sol2(i).ne.1d0)then
         NUM = NUM + 1
         end if

        C_sum1 = C_sum1 + sol2(i)
      end do

c * ***

      if(NUM.ne.0d0) DIST = (C_sum0 - C_sum1)/NUM
c * ***
      do i = 1,npoint
         if(sol2(i).ne.0d0.and.sol2(i).ne.1d0) then
         sol2(i) = sol2(i) + DIST
         end if
      end do


      return
      end
