c *************************************************************
c *   NSTMSH
c *************************************************************
      subroutine nstmsh_periodic(kmesh1,kmesh2,iprint)
      implicit none
      integer kmesh1(*), kmesh2(*), iprint
 
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      character*6 modnam
      parameter ( MODNAM = 'nstmsh')

      integer ndim,npelm,nelgrp,npoint,nelem,kelma,kelmb,i,kelm1
      integer ikelmc,ikelmi,ik,npelm2,nelem2,lengpc2,ikelmc2
      integer kelmg,nelemgroup(10),iniget,inidgt
 
      ndim  =  kmesh1(6)
      npelm =  kmesh1(4)
      nelgrp=  kmesh1(5)
      npoint=  kmesh1(8)
      nelem =  kmesh1(9)
      kelma =  kmesh1(15)
      kelmb =  kmesh1(16)
      kelmg =  kmesh1(21)
c     *** In case of periodic boundary conditions, there
c     *** are two element groups. Only the first group should
c     *** be subdivided into linear triangles
      do i=1,nelgrp
        nelemgroup(i) = kmesh1(kelmg+i-1)
      enddo
 
c     write(6,1000) ndim,npelm,nelgrp,npoint,nelem,kelm1,kelmb
c     write(6,1001) nelgrp,(i,nelemgroup(i),i=1,nelgrp)


      call ini050(kmesh1(17),'nstmsh: nod.point numb.')
      call ini050(kmesh1(23),'nstmsh: coordinates')
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
 
c     *** copy kmesh1 to kmesh2 - the element nod.point numbers in
c     ***  part kelem,c will be redefined
      do ik = 1,kmesh1(1)
         kmesh2(ik) = kmesh1(ik)
      enddo

c     *** update relevant parameters
      npelm2    = 3
      nelem2    = 4*nelemgroup(1)+nelemgroup(2)
      kmesh2(4) = npelm2
      kmesh2(9) = nelem2
 
c     *** create space in buffer for array that will be modified in the
c     *** split-up procedure - kelem2,c kelm2,f kelm2,g
c     *** (kelm,f,g are modified only incase nelgrp>1)
c     ***  kelm,c
      lengpc2    = 4 * nelemgroup(1) * npelm2 + nelemgroup(2)*2
      kmesh2(17) = 0
      call ini051(kmesh2(17), lengpc2, MODNAM)
      ikelmc2 = iniget(kmesh2(17))
 
      call nest_per(kmesh1,kmesh2,ibuffr(ikelmc),buffr(ikelmi),
     v          ibuffr(ikelmc2))
 
      return
 
1000  format(/,'NSTMSH - parameters input mesh:',/,
     .         'ndim ...... ',i5,/,
     1         'npelm ..... ',i5,/,
     2         'nelgrp .... ',i5,/,
     3         'npoint .... ',i5,/,
     4         'nelem ..... ',i5,/,
     5         'kelma ..... ',i5,/,
     6         'kelmb ..... ',i5)
1001  format(/,'NSTMSH - Elements per element group:',/, 
     v            'There are ',i3,' element groups',/,
     v         10('In group ',i3,' are ',i10,' elements'/) )
1100  format(/,'printout header kmesh',a,':',/,
     1       8( 5(i3,':',i5,1x), /))
      end

c *************************************************************
c *   NEST
c *************************************************************
      subroutine nest_per(kmesh1,kmesh2,kelmc,coor,kelmc2)
      implicit none
      integer kmesh1(*), kmesh2(*)
      integer kelmc(*), kelmc2(*)
      real*8 coor(2,*)
 
c     * NPELM - # points per element before split
c     * NSUB  - # sub-elements
c     * NPSUB - # points per sub-element
c
c     * nodal points in sub-elements
c     - odd numbers correspond to the vertices of the orig. elements
c     - even numbers are the midside points
c     - the nodalpoint numbering sequence is counterclock wise in all
c       elements.
      integer NPELM,NSUB,NPSUB,ISHAPE
      parameter (NPELM = 6, NSUB = 4, NPSUB = 3, ISHAPE=3)
      integer nodpsub,i,j,nelgrp,nelem,ikelmf,ikelmg,np,ielem
      integer iptelm,lsub,ipsub,iknodpt,ielgrp,ipntg,inelem1
      integer nelemgroup(10),ibase1,ibase2,isecond,ikelmt,ikelma
      dimension nodpsub(NPSUB,NSUB)
      data ((nodpsub(i,j),i=1,NPSUB),j=1,NSUB) 
     1      /1,2,6, 2,3,4, 4,5,6, 2,4,6 /
 
      nelgrp = kmesh1(5)
      nelem  = kmesh1(9)
      ikelma = kmesh1(15)
      ikelmf = kmesh1(20)
      ikelmg = kmesh1(21)
      ikelmt = kmesh1(36)
      do i=1,nelgrp
        nelemgroup(i) = kmesh1(ikelmg+i-1)
      enddo
c     write(6,1001) nelgrp,(i,nelemgroup(i),i=1,nelgrp)
1001  format(/,'NSTMSH - Elements per element group:',/, 
     v            'There are ',i3,' element groups',/,
     v         10('In group ',i3,' are ',i5,' elements'/) )
 
      np = 0
c     write(6,'(''First element group. Nodal point numbers'')')
      do ielem = 1,nelemgroup(1)
c        *** split element into NSUB=4 linear triangles
c        *** define corresponding nodalpoints in new kelmc 
         iptelm = (ielem-1)*NPELM
c        write(6,1002) ielem,(kelmc(iptelm+i),i=1,NPELM)
1002     format(i5,': ',6i5)
1003     format(i5,': ',2i5)
         do lsub = 1,NSUB
            do ipsub = 1, NPSUB
               iknodpt = kelmc( iptelm + nodpsub(ipsub,lsub) )
               np = np + 1
               kelmc2(np) = iknodpt
            enddo
         enddo
      enddo

c     *** copy the elements for the periodic boundary conditions
      ibase1 = nelemgroup(1)*NPELM
      ibase2 = np
c     write(6,*) 'IBASE1: ',ibase1
c     write(6,*) 'IBASE2: ',ibase2
c     write(6,'(/''Second element group. Nodal point numbers.'',2i5)') 
c    v    nelemgroup(1),nelemgroup(2)
      isecond = -1
      do ielem=nelemgroup(1)+1,nelemgroup(1)+nelemgroup(2)
         isecond = isecond+1
c        write(6,1003) ielem,(kelmc(ibase1+2*isecond+i),i=1,2)
         do i=1,2
            kelmc2(ibase2+2*isecond+i) = 
     v        kelmc(ibase1+2*isecond+i)
         enddo
      enddo

c     write(6,*)
c     write(6,*) 'SECOND MESH'
c     *** print kmesh2,c
c     write(6,'(''First element group. Nodal point numbers'')')
      do ielem=1,4*nelemgroup(1)
         iptelm = (ielem-1)*3
c        write(6,1004) ielem,(kelmc2(iptelm+i),i=1,3)
1004     format(i5,': ',3i5)
      enddo
      ibase2 = 4*nelemgroup(1)*3
      isecond=0
      do ielem=4*nelemgroup(1)+1,4*nelemgroup(1)+nelemgroup(2)
         iptelm = ibase2+2*isecond
c        write(6,1003) ielem,(kelmc2(iptelm+i),i=1,2)
         isecond=isecond+1
      enddo
     
 
c     *** update some parts of kmesh part 2
c     *** modify kelm,f - shape numbers
      kmesh2(ikelmf) = ISHAPE
      kmesh2(ikelmg) = NSUB*kmesh1(ikelmg)
c     *** Sum of number of points in each element group
      kmesh2(14) = 3+2
c     *** KMESH part T
      kmesh2(ikelmt) = kmesh1(ikelmt)
      kmesh2(ikelmt+1) = NSUB*kmesh1(ikelmt+1)
c     *** KMESH part A
      kmesh2(ikelma) = 0
      kmesh2(ikelma+1) = 3
      kmesh2(ikelma+2) = 5
 
c     write(6,*) 'OLD SHAPE NUMBERS: ',kmesh1(ikelmf),kmesh1(ikelmf+1)
c     write(6,*) 'NEW SHAPE NUMBERS: ',kmesh2(ikelmf),kmesh2(ikelmf+1)

      return
      end
