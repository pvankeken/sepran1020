      integer function ivalfind(y)
      implicit none
      real*8 y
      include 'powerlaw.inc'
      integer ilay

      ilay=1
100   continue
        if (y.gt.zint(ilay)) then
           ivalfind = ilay
           return
        else
           ilay=ilay+1
           if (ilay.lt.nlay) then
             goto 100
           endif
           ivalfind=ilay
      endif

      return
      end
