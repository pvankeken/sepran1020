c *************************************************************
c *   MARRAS
c *
c *   Create a rasterfile in which the value of the pixels
c *   indicate the layer in which it is positioned.
c *   The layerboundaries are represented by the boundaries
c *   of the geometry and the markerchains
c *   (common c1mark)
c *   The algorithm is described in PVK 040690
c *
c *   PvK 050690/300790
c *   PvK 921013 : correction for y-direction: midpoint
c *   PvK 072704: change pixel grid coordinates to 'normal' x,y orientation
c *               with iy=1 corresponding to y=0.
c *   PvK 073004: adapted for cylindrical/axisymmetric spherical geometry
c *************************************************************
      subroutine marras(coormark,rname,iout)
      implicit none
      character*80 rname
      integer iout
      real*8 coormark(*)
      include 'cpix.inc'
      include 'ccc.inc'
      include 'pieltogrid.inc'
      include 'c1mark.inc'

      integer ip,ntot,nmark,iadd,ic
      real*8 x1,x2,y1,y2,ymid,xmid
      real*8 xend,yend,xm,ym
      integer NEXTMAX,NYMAX
      parameter(NEXTMAX=10 000,NYMAX=5 000)
      real*8 extension(2*NEXTMAX),cy(NYMAX)
      integer icy(NYMAX)
      integer ncmb,next,icut,ix,iy,im,i,j,ncut
      real*8 xstart,ystart,dth,theta,dr,temp
      real*8 pi
      parameter(pi=3.1415926 535897932)
      integer icol(256)
      data icol(1),icol(2),icol(3),icol(4),icol(5)/1,128,180,200,255/

c     write(6,*) 'marras:'
      if (nochain.ne.1) then
         write(6,*) 'PERROR(marras): this subroutine is not'
         write(6,*) ' suited yet for anything but nochain=1'
         call instop
      endif
c     if (.not.quart) then
c        write(6,*) 'PERROR(marras): only suited for quarter '
c        write(6,*) 'geometry right now'
c        call instop
c     endif 
c     if (.not.axi) then
c        write(6,*) 'PERROR(marras): only suited for axisymmetric'
c        write(6,*) 'spherical geometry right now'
c        call instop
c     endif

c     *** First, initialize ipix=0 for pixels on the mesh; -1 for those
c     *** those outside
      ip=0
      do iy=1,nypix
        do ix=1,nxpix
           ip=ip+1
           ipix(ip) = -1
           if (ielempix(ip).ge.1) ipix(ip)=0
        enddo
      enddo

c     *** Determine which pixels are inside each markerchain
      ip = 0
      do ichain = 1,nochain
         nmark = imark(ichain)
c        *** We need to make a closed loop out of the markerchain
c        *** In the current quarter geometry the first tracer
c        *** is at x=0 and the last tracer is at y=0
         xstart=coormark(ip+1)
         ystart=coormark(ip+2)
         xend  =coormark(ip+2*nmark-1)
         yend  =coormark(ip+2*nmark)
c        write(6,*) ' start: ',xstart,ystart
c        write(6,*) ' end  : ',xend,yend
c        *** bring markerchain to left bottom of mesh (r=r1,theta=0)
         extension(1) = xend
         extension(2) = xstart
         extension(3) = r1
         extension(4) = 0
c        *** follow along the CMB with marker
         dth = 0.5d0*min(dxpix,dypix)
c        write(6,*) 'dth: ',dth
         ncmb = nint(0.5*pi/dth)
c        write(6,*) 'ncmb: ',ncmb
         dth = 0.5*pi/ncmb
c        write(6,*) 'dth: ',dth
         if (ncmb+3.gt.NEXTMAX) then
            write(6,*) 'PERROR(marras): ncmb+3>NEXTMAX'
            call instop
         endif
         do i=1,ncmb-1
            theta = (ncmb-i)*dth
            extension(2*i+3) = r1*sin(theta)
            extension(2*i+4)   = r1*cos(theta)
         enddo
         extension(2*ncmb+3) = 0
         extension(2*ncmb+4) = r1
         extension(2*ncmb+5) = xstart
         extension(2*ncmb+6) = ystart
         next = ncmb+3
         open(9,file='extended')
         do im=1,nmark
            write(9,*) coormark(2*im-1),coormark(2*im)
         enddo
         do im=1,next
            write(9,*) extension(2*im-1),extension(2*im)
         enddo
         close(9)
         do ix=1,nxpix
            xm = (ix-0.5)*dxpix
c           *** first follow the active part of the markerchain
            icut = 0
            do im=2,nmark
               x1 = coormark(ip+2*im-3)
               x2 = coormark(ip+2*im-1)
               if (x1.le.xm.and.x2.ge.xm.or.x1.ge.xm.and.x2.le.xm) then
c                 *** the markerchain is intersecting the current column
                  y1 = coormark(ip+2*im-2)
                  y2 = coormark(ip+2*im)
                  ym = y1+(xm-x1)/(x2-x1)*(y2-y1)
                  icut = icut+1
                  cy(icut) = ym
               endif
            enddo
c           *** now follow through with the extension      
            do im=2,next
               x1 = extension(2*im-3)
               x2 = extension(2*im-1)
               if (x1.le.xm.and.x2.gt.xm.or.x1.ge.xm.and.x2.lt.xm) then
c                 *** the markerchain is intersecting the current column
                  y1 = extension(2*im-2)
                  y2 = extension(2*im)
                  ym = y1+(xm-x1)/(x2-x1)*(y2-y1)
                  icut = icut+1
                  cy(icut) = ym
               endif
            enddo
c           *** sort this array
            ncut=icut
            do i=1,ncut
               do j=2,ncut
                  if (cy(j-1).gt.cy(j)) then
                     temp = cy(j-1)
                     cy(j-1) = cy(j)
                     cy(j) = temp
                  endif
               enddo
            enddo
c           write(6,*) 'ix: ',ncut,(cy(j),j=1,ncut)
c           *** in this case the number of intersections should be even
            if (ncut.ne.(ncut/2)*2) then
               write(6,*) 'PERROR(marras): ncut is not even'
               write(6,*) ' icut = ',ncut
               call instop
            endif
c           *** add 1 to those pixels that are within the markerchain
            iadd=1
            if (ncut.gt.0) then
               icut=ncut
               do iy=nypix,1,-1
                  ym = (iy-0.5)*dypix
c                 *** first test to see if we're below the markerchain     
c                 write(6,*) 'ym: ',cy(icut),ym,cy(icut-1)
                  if (ym.gt.cy(ncut)) then
                      icy(iy)=0
c                     write(6,*) 'ym>cy(ncut)',iy,icy(iy)
                  else if (ym.lt.cy(1)) then
                      icy(iy)=0
c                     write(6,*) 'ym<cy(1)',iy,icy(iy)
                  else
c                     *** if not, count down through the layers
111                   continue
                      if (ym.le.cy(icut).and.ym.ge.cy(icut-1)) then
c                      *** pixel is within the volume
                       icy(iy) = iadd
c                      write(6,*) 'ym>cy(icut)^ym>cy(icut-1)',iy,icy(iy)
                      else 
c                      *** try next interval
                       icut = icut - 1
                       iadd = mod(iadd+1,2)
                       if (icut.ge.2) then
                          goto 111
                       endif
                      endif
                    endif
c                   write(6,*) 'iy =',iy,icy(iy)
               enddo
c              *** add the column to IPIX
               do iy=1,nypix 
                  ic = (iy-1)*nxpix+ix
                  if (ipix(ic).ge.0) then
                     ipix(ic)=ipix(ic)+icy(iy)
                  endif
               enddo
            endif
        enddo
      enddo


      if (iout.eq.-1) then
c        *** ignore bottom layer in pixel grid in output
         ntot = nxpix*nypix
         do ip=1,ntot
            ipix(ip) = icol(ipix(ip)+2)
         enddo
         call rasout(ntot,ipix,rname)
      endif

      return
      end
