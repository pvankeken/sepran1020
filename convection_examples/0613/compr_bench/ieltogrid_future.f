c *************************************************************
c *   IELTOGRID
c *
c *   Find the mapping of element numbers to a regular Cartesian grid.
c *   This greatly facilitates tracer interpolation  (see ielfromgrid)
c *
c *   PvK 970406
c *
c *   Rewritten for curved elements. This should at some point be made
c *   more efficient by an iterative scheme: 1) fill regular grid using
c *   assumption of straight elements; 2) update regular grid using
c *   curved elements.
c * 
c *   PvK 020304
c *************************************************************
      subroutine ieltogrid(kmesh)
      implicit none
      integer kmesh(*)
      integer nelem,ikelmc,ikelmi,ielem
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      
      real*8 pi
      parameter(pi=3.1415926 535898)
c     *** /pieltogrid/ contains information on regular grid
      include 'pieltogrid.inc'
      include 'ccc.inc'
      include 'bound.inc'
      include 'dimensional.inc'
      include 'elem_topo.inc'

      real*8 xn(6),yn(6)
      real*8 a(3),b(3),c(3),x,y,rl(3)
      real*8 xcmin,xcmax,ycmin,ycmax
      real*8 dx1,dy1,dx2,dy2,dx3,dy3,dr1,dr2,dr3,rmin,rmax
      integer ipx,ipy,ipxmin,ipxmax,ipymin,ipymax,i
      integer NELEMMAX
      parameter(NELEMMAX=40 000)
      integer nodno(6),isub,ifillelem(NELEMMAX)
      integer iniget,inidgt,nodlin(3)
      logical checkinelem,correct
      real*8 xi,eta,phiq(6),dr_elem
      logical out,check,flip
      

c     *** prescribe the dimensions of the regular grid
      nxel = NXELM
      nyel = NYELM
      dxel = 2*r2/(NXELM-1)
      dyel = 2*r2/(NYELM-1)
c     *** Get the coordinates and nodal points of the quadratic mesh
      call ini050(kmesh(23),'iel: coordinates')
      call ini050(kmesh(17),'iel: nodalpoints')
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      nelem = kmesh(9)
      if (periodic) nelem=kmesh(kmesh(21))

c     *** initialize element numbers in regular grid to aid error check
      do ipy = 1,NYEL
         do ipx = 1,NXEL
            iel(ipx,ipy) = -1
         enddo
      enddo


c *************************************************************
c *   *** STEP 1
c *   *** First make an estimate of the element numbers by assuming
c *   *** the elements have straight boundaries.
c *************************************************************
      rmin = r2
      rmax = 0

c     *** loop over elements
      do ielem=1,nelem

c        *** determine coordinates for nodal points of this element
         call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,ielem)
         check = .false.

         xcmin = min(xn(1),xn(2),xn(3),xn(4),xn(5),xn(6))
         xcmax = max(xn(1),xn(2),xn(3),xn(4),xn(5),xn(6))
         ycmin = min(yn(1),yn(2),yn(3),yn(4),yn(5),yn(6))
         ycmax = max(yn(1),yn(2),yn(3),yn(4),yn(5),yn(6))
         dx1 = xn(1)-xn(3)
         dy1 = yn(1)-yn(3)
         dx2 = xn(3)-xn(5)
         dy2 = yn(3)-yn(5)
         dx3 = xn(5)-xn(1)
         dy3 = yn(5)-yn(1)
         dr1 = sqrt(dx1*dx1+dy1*dy1)
         dr2 = sqrt(dx2*dx2+dy2*dy2)
         dr3 = sqrt(dx3*dx3+dy3*dy3)
         rmin = min(rmin,dr1,dr2,dr3)
         rmax = max(rmax,dr1,dr2,dr3)
c        *** translate this to grid element values
         ipxmin = max(1.0,(xcmin+r2)/dxel+1.5)
         ipxmax = min(NXEL*1.0,(xcmax+r2)/dxel+1.5)
         ipymin = max(1.0,(ycmin+r2)/dyel+1.5)
         ipymax = min(NYEL*1.0,(ycmax+r2)/dyel+1.5)
         if (ipxmax.gt.NXEL.or.ipymax.gt.NYEL) then 
             write(6,*) 'PERROR(ieltogrid): ipxmax > NXEL'
             write(6,*) '   or ipymax > NYEL'
             write(6,*) '   ipxmax: ',ipxmax
             write(6,*) '   ipymax: ',ipymax
             write(6,*) '   nelem:  ',nelem
             call instop
         endif
c        *** loop over corresponding (x,y) domain in which the element lies
           do ipy = ipymin,ipymax
             do ipx = ipxmin,ipxmax
               x = (ipx-1)*dxel - r2
               y = (ipy-1)*dyel - r2
               correct = checkinelem(1,xn,yn,x,y,nodno,nodlin,rl,
     v                 xi,eta,phiq,isub)
               if (correct) then
c                 *** this grid point is in the element
                  iel(ipx,ipy) = ielem
               endif
             enddo
           enddo        
        enddo   

c *************************************************************
c *     *** STEP 2
c *     *** Refine first estimate by taking curvature of elements 
c *     *** into account
c *************************************************************
        do ipy = 1,NYEL
           y = (ipy-1)*dyel - r2
           do ipx = 1,NXEL
               x = (ipx-1)*dxel - r2
               r = sqrt(x*x+y*y)
               iel_estimate = iel(ipx,ipy)
               if (r.le.r2.and.iel_estimate.eq.-1) then
c                 *** Step 1 incorrectly identified this
c                 *** point to be outside the domain;
c                 *** Repeat with the assumption of straight 
c                 *** boundaries
              

        write(6,*) 
        write(6,*) 'PINFO(ieltogrid): '
        write(6,*) 'Minimum size of quadratic elements  : ',
     v           rmin,'(',rmin*height_dim*1e-3,' km)'
        write(6,*) 'maximum size of quadratic elements  : ',
     v           rmax,'(',rmax*height_dim*1e-3,' km)'
        write(6,*) 'resolution of regular grid          : ',
     v           dxel,'(',dxel*height_dim*1e-3,' km)'
        write(6,*) 

c       do ipy=NYEL,NYEL/2,-1
c          write(6,'(i5,'': '',51i4,:)') ipy,
c    v           (iel(ipx,ipy),ipx=NXEL/2,NXEL) 
c       enddo

        return
        end
