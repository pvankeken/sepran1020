      integer NELEM_TOPO
      parameter(NELEM_TOPO=50 000)
      real*8 rtop_max,rtop_min,rtop_threshold,eps_elem
      real*8 rbot_max,rbot_min,rbot_threshold
      integer ipxmin,ipxmax,ipymin,ipymax
      integer ipix_rmin,ipix_rmax,ipix_thmin,ipix_thmax
      logical boundary_elem,curved_elem
      common /elem_topo/ rtop_max,rtop_min,rtop_threshold,
     v    rbot_max,rbot_min,rbot_threshold,eps_elem,
     v    ipxmin(NELEM_TOPO),ipxmax(NELEM_TOPO),
     v    ipymin(NELEM_TOPO),ipymax(NELEM_TOPO),
     v    ipix_rmin(NELEM_TOPO),ipix_rmax(NELEM_TOPO),
     v    ipix_thmin(NELEM_TOPO),ipix_thmax(NELEM_TOPO),
     v    boundary_elem(NELEM_TOPO),curved_elem(NELEM_TOPO)
