c *************************************************************
c *   SPER01
c *
c *************************************************************
      subroutine sper01(kmeshc,coor,nodno,xn,yn,iel)
      implicit none
      integer kmeshc(*),nodno(*),iel
      real*8 coor(2,*),xn(*),yn(*)
      integer inpelm,ip,i

      inpelm = 6
      ip = (iel-1)*inpelm
      do i=1,inpelm
         nodno(i) = kmeshc(ip+i)
      enddo
      do i=1,inpelm
         xn(i) = coor(1,nodno(i))
         yn(i) = coor(2,nodno(i))
      enddo

      return
      end

c *************************************************************
c *   SPER02
c *   Determine velocity in nodalpoints of one element
c *************************************************************
      subroutine sper02(usol,kprobh,indprh,un,vn,nodno)
      implicit none
      real*8 usol(*),un(*),vn(*)
      integer kprobh(*),nodno(*),indprh
      integer inpelm,i,ip1,ip2

      inpelm = 6
      do i=1,inpelm
         ip1 = 2*nodno(i)-1
         ip2 = 2*nodno(i)
         if (indprh.gt.0) then
            ip1 = kprobh(ip1)
            ip2 = kprobh(ip2)
         endif
         un(i) = usol(ip1)
         vn(i) = usol(ip2)
      enddo
 
      return
      end

c *************************************************************
c *   SPER03
c *   Determine temperature in nodalpoints of one element
c *************************************************************
      subroutine sper03(usol,kprobh,indprh,tn,nodno)
      implicit none
      integer kprobh(*),nodno(*),indprh
      real*8 usol(*),tn(*)
      integer inpelm,i,ip1

      inpelm = 6
      do i=1,inpelm
         ip1 = nodno(i)
         if (indprh.gt.0) then
            ip1 = kprobh(ip1)
         endif
         tn(i) = usol(ip1)
      enddo
 
      return
      end

c *************************************************************
c *   SPER04
c *   Store velocity in nodalpoint
c *************************************************************
      subroutine sper04(usol,kprobh,indprh,u,v,nodnum)
      implicit none
      real*8 usol(*),u,v
      integer kprobh(*),nodnum,indprh
      integer ip1,ip2

      ip1 = 2*nodnum - 1
      ip2 = 2*nodnum
      if (indprh.gt.0) then
         ip1 = kprobh(ip1)
         ip2 = kprobh(ip2)
      endif
      usol(ip1) = u
      usol(ip2) = v
 
      return
      end

c *************************************************************
c *   SPER05
c *
c *************************************************************
      subroutine sper05(coor,nodnum,x,y)
      implicit none
      integer nodnum
      real*8 coor(2,*),x,y

      x = coor(1,nodnum)
      y = coor(2,nodnum)

      return
      end

c *************************************************************
c *   SPER06
c *************************************************************
      subroutine sper06(user,un,nodno)
      implicit none
      real*8 user(*),un(*)
      integer nodno(*),i

      do i=1,6
         un(i) = user(nodno(i))
      enddo
 
      return
      end

