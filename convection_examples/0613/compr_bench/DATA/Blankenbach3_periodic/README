Convection modeling: isoviscous, Cartesian

Peter van Keken, 2009-2010


Below are some notes for the use of a sepran application 'compr_bench' for use specifically
for the modeling work with Jared Whitehead in the math department.


Compilation of the application
------------------------------

Make sure you use sepran version 0108:
   % module load sepran/0108-pgi
The source code with some examples is available in ~keken/sepran/jaredwh/compr_bench. 
   % cd ~/sepran
   % mkdir compr_bench
   % cd compr_bench
   % cp -r ~keken/sepran/jaredwh/compr_bench/* .
To compile remove the old object files and run make:
   % rm *.o; make
This puts compr_bench in i$BINDIR.


Mesh generation:
----------------

When using reflective side boundaries, the code expects two files 'mesh1' and 'mesh2' for the 
Stokes and heat equation resp.  Mesh1 is generally composed of quadratic element; 
mesh2 has linear elements defined with the same nodal points as mesh1. For periodic boundary
conditions it expects a single file 'meshoutput' which is for the quadratic elements.

To make mesh1/mesh2 with REFLECTIVE side boundary conditions use the makemesh application which can
be found in ~keken/sepran/Util/makemesh
(if you don't have this yet do the following:
   % cd ~/sepran; mkdir makemesh; cd makemesh
   % cp -r ~keken/sepran/Util/makemesh/* .
   % rm *.o; make
and this will put makemesh in your BINDIR).
The input file for makemesh (say makemesh.in) looks like:
    2 1   # rlam plotinfo 
    8 4 8 1 1  # nx1 nx2 nx3  fx1 fx2
    4 2 4 1.2 0.833 # ny1 ny2 ny3 fy1 fy2
where rlam is the aspect ratio. The total number of quadratic elements in the x-direction is
nx+nx2+nx3. There are three regions: the left region has nx1 elements that increase in size 
by a factor fx1; the center region has nx2 equidistant elements; and the right region has nx3
elements that increase in size by fx2. Same for the y direction. This allows you to set up refinement
in the boundary layers (particularly important near top and bottom). The makemesh application computes
the right locations of the subdivision so that the mesh is gradually refined without major steps in
element size (which can make the solution inaccurate).

For PERIODIC boundary conditions additional information is necessary. The side boundaries are linked together
by line elements, that effectively couple the degrees of freedom in these point to each other. 
You can use the same makemesh.in file but need to make the mesh file with the application 'makemesh_periodic' which
you can find in ~/sepran/Util/makemesh/makemesh_periodic. If you've done the steps above you only need to do:
    % cd ~/sepran/makemesh/makemesh_periodic
    % rm *.o; make
To make the mesh file suited for periodic boundary conditions
    % makemesh_periodic < makemesh.in
This generates the file meshoutput (not mesh1/mesh2). 

You can inspect the quality of the meshes with 

% sepview sepplot.*

(right after generating the mesh files; if you want to keep a permanent plot file around you need to copy it, e.g.,
cp sepplot.001 MESH.001; cp sepplot.002 MESH.002). The first mesh plot shows the quadratic elements for the Stokes equations;
the second is the embedded mesh of linear triangles for the heat equation.
 
   
Some important input in compr_bench
-----------------------------------

compr_bench reads a number of user flags (that include Rayleigh number, information on rheology, geometry,
boundary conditions), followed by standard sepran information for the problem definition and boundary conditions
(see the Sepran User Manual or Introduction for more information). The problem information for 900 is for the Stokes 
equation. 800 is for the heat equation.
Examples of compr_bench.in input files are in the various subdirectories below DATA. 

A typical input file looks like:

0 F F        # solution_method (-1=steady; 0=time dependent)  axi cyl
0 0 0 0 0 0  0   # isolmethod,iluprec,ipreco,maxiter,iprint,keep,cgeps
200 1e-5 0.5    # STEADY: maxiter eps relax
1 -0.5 0.010 0.400  10 # TIME-DEPENDENT: ncor tfac dtout_d (Byr) tmax_d (Byr) nbetween
3 F                # metupw skyfilter
1.e4 0 F       # Ra Rb_local compress_with_Ra
1 0 0   # nlay zint(1:nlay-1)
.......
**************************************
problem
types
  elgrp1=(type=900)
* elgrp2=(type=-1)
essboundcond
  degfd2=curves0(c1)
  degfd2=curves0(c3)
  degfd1=curves0(c2)
  degfd1=curves0(c4)
end

problem
  types
    elgrp1=(type=800)
*   elgrp2=(type=-1)
  essboundcond
    curves0(c1)
    curves0(c3)
end
....


Here are some general guidelines for setting up boundary conditions:

1) Periodic boundary conditions:
   Use 'elgrp2=(type=-1)' in the types block of problem (for both 800 and 900).
   Set periodic to T (line 8 of compr.in). Make sure to generate the correct meshoutput file (see above).

2) Free slip side boundary conditions
   Comment out the 'elgrp2=(type=-1)' line in both instances by putting a * in the first column
   Set periodic to F (line 8 of compr.in). Make sure to generate mesh1/mesh2 (see above).

3) Free slip top boundaries
   In the 'essboundblock' Use 
      degfd2=curves0(c1)
      degfd2=curves0(c3)
   instead of
      curves0(c1)
      curves0(c3)
    in the essboundcond block of 900. This forces the y-component of velocity to be 0. The other b.c.
    is the natural b.c. (zero tangential stress). 

4) Internal heating with insulated bottom boundary (such as for Blankenbach benchmark case 3).
    Set insul_bot (line 8 of compr.in) to T
    Specify nondimensional heating by setting iqtype=11 and the internal heating rate as q_layer on line 16:
       11 1.0  # iqtype q_layer_d(1-nlay) (e.g., 1 0.7)  q_layer_d is the fraction of BSE heating!
    Comment out the bottom boundary condition in the 800 block:
       problem
         types
           elgrp1=(type=800)
           elgrp2=(type=-1)
         essboundcond
       *   curves0(c1)
           curves0(c3)
       end
             
User defined input
------------------

You can change the input to do use steady state convection or time-dependent convection, use variable viscosity,
etc. 

1) Steady state convection
        Set solution_method to -1. In the third line check the default settings for the Picard iteration:
        maxiter=maximum number of iterations; eps=relative difference for convergence; relax=relaxation parameter. 
        It's a good idea to use relaxation for most intermediate Ra problems (values of 0.2-0.5 are good)
2) Time-dependent convection
        Set solution_method to 0. In the 4th line check the settings for the time integration. Set ncor=1 for the
        more accurate Crank-Nicolson predictor-corrector scheme, ncor=0 for the less accurate but faster 
        implicit Euler predictor. Tfac is the ratio of the CFL criterion (negative means the values after this 
        are given in non-dimensional quantities which is appropriate here). dtout_d sets the interval between output 
        of plots, solution files etc. tmax_d gives the maximum integration time. nbetween is a flag that provides
        major output every 'nbetween'th step and image files every step. Setting nbetween to 10 or so allows you to 
        make movies at high resolution without getting full output every step. 

By default (krestart=-1) the code starts from a conductive stage. You can read an existing solution from restart file (stored as
restart.back or sol.001, sol.002 etc.). To restart copy the preferred restart file to 'start.back' and set the flag
krestart=0. 


Interpolation to different meshes
---------------------------------

You'll find that for high Ra case it is necessary to start from existing files but to use higher grid resolution. You can
interpolate the solution from one mesh to the other with the solint application:
    % cd ~/sepran; mkdir solint; cd solint
    % cp ~keken/sepran/Util/solint/* .
    % rm *.o; make
This compiles the application solint and puts it in your BINDIR. 
You only need to interpolate the solution for the temperature. Make sure you have the old mesh solution file copied
    % cp mesh2 mesh2.old   # for reflective boundary conditions
    % cp meshoutput meshoutput.old   # for periodic boundary conditions
before you make the new mesh files with makemesh or makemesh_periodic.  You also need the solution to be
interpolated that is consistent with the mesh2.old or meshoutput.old mesh file. 
A typical solint.in file looks like:

  'meshhoutput.old' 'meshoutput'   # old mesh file ; new mesh file
  'old.back' 1            # old solution file
  'start.back' 1          # new solution file
  problem                 # problem definition for the old problem
    types
      elgrp1=(type=800)
      elgrp2=(type=-1)
    essboundcond
      curves0(c1)
      curves0(c3)
  end
  problem                  # problem definition for the new problem
    types
      elgrp1=(type=800)
      elgrp2=(type=-1)
    essboundcond
      curves0(c1)
      curves0(c3)
  end

Note that this example is for a solution with periodic boundary conditions. For one with reflective boundary conditions use
mesh files 'mesh2.old' and 'mesh2' and comment out the 'elgrp2=type=-1' lines.
