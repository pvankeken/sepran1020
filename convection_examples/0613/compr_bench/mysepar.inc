      integer NDEF
      parameter(NDEF=400)
      integer kmesh1(NDEF),kmesh2(NDEF),kprob1(NDEF),kprob2(NDEF)
      integer isol1(5),isol2(5),intmt1(5),intmt2(5,2)
      integer matr1(5),matr2(5),islol1(5,3),islol2(5)
      integer iheat(5),idens(5),ivisc(5),isecinv(5),isol3(5)
      integer icompwork(5,3)
      common /mysepar/ kmesh1,kmesh2,kprob1,kprob2,isol1,isol2, 
     v                 intmt1,intmt2,matr1,matr2,islol1,islol2,
     v                 iheat,idens,ivisc,isecinv,isol3,icompwork

