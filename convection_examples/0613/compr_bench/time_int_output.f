c *************************************************************
c *   Time_int_output
c * 
c *   Intermediate output for time integration.
c *   Called at each time step.
c *   Output of time, Nusselt number, rms velocity.
c *   Store some information on chemistry (if required) in arrays.
c *
c *   PvK 071900
c *************************************************************
      subroutine time_int_output(kmesh1,kmesh2,kprob1,kprob2,
     v         isol1,isol2,islol1,islol2,iheat,idens,icompwork,
     v         ivisc,isecinv,iuser,user) 
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),islol1(*),islol2(*),iuser(*)
      integer ivisc(*),iheat(*),idens(*),isecinv(*)
      integer icompwork(5,*)
      real*8 user(*)

      integer ibuffr(1)
      common ibuffr
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      include 'pecpu.inc'
      include 'peparam.inc'
      include 'dimensional.inc'
      include 'petrac.inc'
      include 'cpephase.inc'
      include 'vislo.inc'
      include 'pexcyc.inc'
      include 'c1visc.inc'
      include 'degas.inc'
      include 'SPcommon/ctimen'
      include 'SPcommon/cmachn'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'depth_thermodyn.inc'
      include 'perealdata.inc'
      include 'tracer.inc'
      include 'coolcore.inc'
      include 'averages.inc'
      include 'ccc.inc'
      include 'bound.inc'
      include 'csky.inc'
      include 'c1mark.inc'
      common /peuav/ uav
      real*8 uav

      integer NFUNC
      parameter(NFUNC=505)
      integer NUM
      parameter(NUM=20 000)
      integer igradt(5),icurvs(3)
      real*8 funcx(NFUNC),funcy(NFUNC)
      integer ivser(100)
      real*8 vser(NUM),smax,tmax,volint,vrms1,vrms2,vrms_d
      real*8 bounin,temp1,vrms,anorm,funccf,x,z,gnus
      integer nunkp,irule,ihelp,ip,ielhlp,iphi(5),ivisdip(5) 
      integer ifirst,ikelmi,npoint,ichois,icheld,ix
      integer ipoint,inputcr(10),jdegfd,ivec,nq_a,inout,i
      integer iqtype_dum
      real*8 q_a(20),q_a_d(20),t_d
      real*8 phiav,heatdummy,rinputcr(10),rkap1,rkap2,y,heatav
      real*4 second
      real*8 DONE,pi
      parameter(DONE=1d0,pi=3.1415926 535898)
      real*8 heatprod1,heatprod2,veloc(2),veloc_d(2),NuwT
      integer ih,im,is,inidgt,ipheat,kmax,k,ntot
      real*8 avwork(2),avphi(2),avwork2(2),avwork3(2)
      logical first

      save iphi,ifirst,ivisdip,first

      data ivser(1),vser(1),funcx(1),funcy(1)/100,NUM,2*NFUNC/
      data ifirst,first/0,.true./

      kmax=4

      t2 = second()
      dcpu = t2-t1
      cput = t2-t00
      t1   = t2

      if (first) then
         if (restart) then
           open(20,file='vrms.dat',status='old',access='append')
           open(21,file='nusselt.dat',status='old',access='append')
           open(22,file='NuwT.dat',status='old',access='append')
           open(23,file='uav.dat',status='old',access='append')
           open(24,file='Ra.dat',status='old',access='append')
           open(26,file='phi.dat',status='old',access='append')
           open(27,file='work.dat',status='old',access='append')
           open(28,file='upward_vel2.dat',status='old',access='append')
           open(29,file='T2.dat',status='old',access='append')
           open(30,file='ratio.dat',status='old',access='append')
         else
           open(20,file='vrms.dat')
           open(21,file='nusselt.dat')
           open(22,file='NuwT.dat')
           open(23,file='uav.dat')
           open(24,file='Ra.dat')
           open(26,file='phi.dat')
           open(27,file='work.dat')
           open(28,file='upward_vel2.dat')
           open(29,file='T2.dat')
           open(30,file='ratio.dat')
         endif
      endif

c     *** Compute RMS velocity
      call pevrms9(isol1,kmesh1,kprob1,iuser,user)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
      vrms = sqrt(abs(vrms2))
      vrms_d = vrms*rkappa_dim/height_dim*100*year_dim

c     *** Compute heat flow through top and bottom
      call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      call nusseltwT(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v               iuser,user,NuwT)

  
c     *** Interpolate temperature to regular grid
      call averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                 user,nth_output,nr_output,-1)

c     *** Compute work and viscous dissipation even if Di=0
      call compressionwork(kmesh1,kmesh2,kprob1,kprob2,
     v                     isol1,isol2,icompwork)
      call averageT(3,icompwork,kmesh2,kprob2,iuser,user,avwork)
      call averageT(3,icompwork(1,2),kmesh2,kprob2,iuser,user,avwork2)
      call averageT(3,icompwork(1,3),kmesh2,kprob2,iuser,user,avwork3)

      call phifromgrad(kmesh1,kprob1,isol1,ivisdip)
      call averageT(2,ivisdip,kmesh2,kprob2,iuser,user,avphi)
      avphi(1)=avphi(1)/Ra

      if (Di.gt.0d0.or.itypv.ne.0) then
c       *** compute viscous dissipation
c       call phifromsepran(kmesh1,kprob1,isol1,ivisdip)
c       call phifromgrad(kmesh1,kprob1,isol1,ivisdip)

        if (itypv.ne.0) then
c          *** compute viscosity
           call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                  ivisc,isecinv,iuser,user)
        endif

c       *** calculate average viscous dissipation
        npoint=kmesh1(8)
        call ini050(kmesh1(23),'intermediate output: coor')
        ikelmi = inidgt(kmesh1(23))
        heatdummy = 0d0
        iqtype_dum=0
        call pecophi(ivisdip,user,6,npoint,DONE,
     v         iqtype_dum,buffr(ikelmi),ivisc,iheat)
        iuser(2) = 1
        iuser(6) = 7
        iuser(7) = intrule900
        iuser(8) = icoor900
        iuser(9) = 0
        iuser(10) = 2001
        iuser(11) = 6
        phiav = volint(0,1,1,kmesh1,kprob1,isol1,iuser,user,ielhlp)
        phiav = phiav * DiRa / volume

c       *** calculate average radiogenic heatproduction
        if (iqtype.eq.0) then
           heatav=0
        else 
           call ini050(iheat(1),'cylstart: iheat')
           ipheat=inidgt(iheat(1))
           do i=1,npoint
              user(6+i-1) = buffr(ipheat+i-1)
           enddo
           heatav = volint(0,1,1,kmesh2,kprob2,iheat,iuser,user,ielhlp)
           heatav = heatav/volume
        endif
      endif

c     *** Compute average and maximum surface velocity
      call surfacevel(kmesh1,kprob1,isol1,itop,veloc,veloc_d)

c     *** Finally, output intermediate information
      ih = cput/3600
      im = (cput - ih*3600)/60
      is = cput - ih*3600 -im*60
      if (insulbot) then
         t_bot = q_a(3)
      endif
      if (numnat800 == 1 .or. insulbot) then
         gnus = q_a(1)/t_bot
      else
         gnus = q_a(1)
      endif
      
      write(6,11) t,gnus,-q_a(2),vrms,t_bot,avphi(1),avwork(1),
     v        ih,im,is
      t_d = t/tscale_dim
11    format(f15.7,2f12.5,f15.5,f12.3,2e15.4,
     v       i5,':',i2.2,':',i2.2) 
21    format('D: ',e13.5,2e13.5,2e13.5,2e13.5,2e13.5)
      if (itracoption.ne.0) then
         ntot=0
         if (itracoption.eq.2) then
            do ichain=1,nochain
               ntot = ntot+imark(ichain)
            enddo
         else 
            do idist=1,ndist
               ntot=ntot+ntrac(idist)
            enddo
         endif
         write(6,22) coormark(1),coormark(2),ntot
      endif
22    format('T: ',2f12.7,i10)
      if (ifollowchem.ne.0) then
         write(6,13) nnow(1),ndegas(1),t*tscale,U238,U235,Th232, 
     v             K40,He4,Ar40 
c        do k=1,kmax
c           write(6,15) nnowc(k),ncont(k),U238loss(k),U235loss(k),
c    v         Th232loss(k),K40loss(k),
c    v         He4loss(k),He3loss(k),Ar40loss(k)
         do k=1,kmax
            write(6,16) nnow(k),ndegas(k),
     v         He4loss(k),He3loss(k),Ar40loss(k)
         enddo
      endif

      write(20,*) t,vrms
      write(21,*) t,gnus
      write(22,*) t,NuwT
      write(23,*) t,uav
      write(24,*) t,Ra
      write(26,*) t,avphi(1)
      write(27,*) t,avwork(1)
      write(28,*) t,avwork2(1)
      write(29,*) t,avwork3(1)
      write(30,*) t,avwork(1)*avwork(1)/(avwork2(1)*avwork3(1))
      call flush(20)
      call flush(21)
      call flush(22)
      call flush(23)
      call flush(24)
      call flush(26)
      call flush(27)
      call flush(28)
      call flush(29)
      call flush(30)

13    format(8x,i5,i7,8f9.3)
15    format(8x,i5,i7,7e9.2)
16    format(8x,i5,i7,3e9.2)

      return
      end
