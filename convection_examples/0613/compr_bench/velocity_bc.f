c *************************************************************
c *   VELOCITY_BC
c *
c *   Enforce essential boundary conditions for velocity
c *
c *   PvK 072902
c *************************************************************
      subroutine velocity_bc(ichoice)
      implicit none
      include 'mysepar.inc'
      include 'bound.inc'
      integer ibp,iall,ichoice

      if (ichoice.eq.0) then
        do ibp=1,iclc
           iall=icloc(ibp)
           call bvalue(0,2,kmesh1,kprob1,isol1,0d0,iall,iall,1,0)
        enddo
      endif

      do ibp=1,nbp
         iall=iboundpoints(ibp)
         call bvalue(0,1,kmesh1,kprob1,isol1,0d0,iall,iall,1,0)
         call bvalue(0,1,kmesh1,kprob1,isol1,0d0,iall,iall,2,0)
      enddo

      return
      end
