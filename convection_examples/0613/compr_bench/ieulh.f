c *****************************************************************
c *   IEULH
c *
c *   The temperature equation is solved using implicit euler
c *   Information concerning the coefficients of the equation
c *   are stored in (i)user and ivcold.
c * 
c *
c *   Pvk  160589/290889
c ****************************************************************
      subroutine ieulh(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,matrm,irhsd)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),isol(*),iuser(*)
      integer matrs(*),matrm(*),irhsd(*),islold(*)
      real*8 user(*)
      
      include 'SPcommon/ctimen'
      integer ibuffr
      common ibuffr(1)
      include 'SPcommon/cbuffr'
      include 'petime.inc'
      integer ifirst,ivec1,ivec2,ivec3,matr1,ipser,mrhsd
      dimension ivec1(5),ivec2(5),ivec3(5),matr1(5)
      integer iinbld,ichois,ip
      real*8 pser,p,q,alpha2,beta2,bee
      dimension ipser(100),mrhsd(5),iinbld(20),pser(100)
      character*10 tname
      save ifirst,ivec1,ivec2,ivec3,matr1,ipser,pser,mrhsd
      save iinbld
      data ifirst/0/
      data ipser(1),pser(1)/100,100/


      call copyvc(isol,islold)
c     *** Built matrices and rhs vector at time t
c     *** Stiffness matrix and 
      if (ifirst.eq.0.or.idia.eq.2) then
        iinbld(1) = 4
        iinbld(2) = 1
        iinbld(3) = 1
        iinbld(4) = idia
        ifirst = 1
      else
        iinbld(1) = 3
        iinbld(2) = 1
        iinbld(3) = 0
      endif
      call build(iinbld,matrs,intmat,kmesh,kprob,irhsd,matrm,isol,  
     v           islold,iuser,user)

c     *** Built the system of equations
     
c     *** M*uold
      ichois=idia+3
      call maver(matrm,islold,ivec1,intmat,kprob,ichois)

c     write(6,*) 'ieulh: ',tstep
c     *** tstep*f + M*uold
      call algebr(3,0,ivec1,irhsd,ivec3,kmesh,kprob,1d0,tstep,p,q,ip)

c     *** M + tstep*S
      call copymt(matrs,matr1,kprob)
      call addmat(kprob,matr1,matrm,intmat,tstep,alpha2,1d0,beta2)

c     *** ( M + tstep*S ) * usol(p)
      call maver(matr1,isol,ivec2,intmat,kprob,6)

c     *** tstep*f + M*uold - (M+tstep*S) * usol(p)
      bee = -1d0
      call algebr(3,0,ivec3,ivec2,ivec1,kmesh,kprob,1d0,bee,p,q,ip)

c     *** Solve the system

      call solve(0,matr1,isol,ivec1,intmat,kprob)

      return
      end
