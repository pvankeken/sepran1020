c     **** degas.inc
      integer ndegaszone
      real*8 thmindegas,thmaxdegas,rdegas,zdegas
      integer ndegas,nnow,ncont,nnowc,ifollowdegas
      integer numdegas_depth
      integer NSPECIES,NCHEMCASE,NDEGAS_DEPTHS,MAXDEGASZONE
      parameter(NSPECIES=5,NCHEMCASE=4,NDEGAS_DEPTHS=4,MAXDEGASZONE=4) 
      logical vardegaszones
      common /degaszone/ thmindegas(NSPECIES,MAXDEGASZONE),
     v     thmaxdegas(NSPECIES,MAXDEGASZONE),
     v     rdegas(NDEGAS_DEPTHS),zdegas(NDEGAS_DEPTHS),
     v     ndegaszone(NCHEMCASE),ndegas(NCHEMCASE),
     v     nnow(NCHEMCASE),ncont(NCHEMCASE),nnowc(NCHEMCASE),
     v     ifollowdegas,numdegas_depth,vardegaszones
      real*8 U238,U235,Th232,K40,He4,Ar40
      real*8 dU238,dU235,dTh232,dK40,dHe4,dAr40
      real*8 He4loss,He3loss,Ar40loss
      real*8 rl238,rl235,rl232,rl40
      real*8 U238loss,U235loss,Th232loss,K40loss
      common /ingrowth/ U238,U235,Th232,K40,He4,Ar40,
     v                dU238,dU235,dTh232,dK40,dHe4,dAr40,
     v                He4loss(4),He3loss(4),Ar40loss(4),
     v            U238loss(4),U235loss(4),Th232loss(4),K40loss(4)
c     *** decay times in per By
      parameter(rl238=1.55e-1,rl235=9.85e-1,rl232=4.95e-2,rl40=5.54e-1)
c     **** time scale in By
      real*8 tscale
      parameter(tscale=2.637e2)
c     **** initial concentrations (in 1e14 atoms/g)
      real*8 U238_0,U235_0,Th232_0,K40_0,K40_eff
      parameter(U238_0=1.0674,U235_0= 0.32813,
     v         Th232_0=2.4322,K40_0 =56.9129,K40_eff=0.1024)
