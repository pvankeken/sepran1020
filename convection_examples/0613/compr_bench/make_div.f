c *************************************************************
c *   MAKE_DIV
c *
c *   Create regular cell structure in cylindrical geometry
c *   Cells are created that are DRGRID high and approximately
c *   DRGRID wide. Cells at the same depth level have the same width.
c *   This structure has been used to compute averages in tracers
c *   (e.g. 3He, 3He/4He etc.) and may be useful for computing
c *   compositional buoyancy with the markerchain method as well.
c *
c *   PvK  970719
c *************************************************************
      subroutine make_div()
      implicit none
      include 'ccc.inc'
      include 'bound.inc'
      integer ngrid
      real*8 pi
      parameter(pi=3.1415926535897932)
      real*8 dx,r,th,x,y,rm,x1,y1,x2,y2
      integer i,j,k,nx
      include 'hegrid.inc'
      real*8 WHOLE
  
      WHOLE=frac
      write(6,*) 'PINFO(make_div): WHOLE = ',WHOLE,half,quart,eighth

      drgrid = (r2-r1)/nrdiv
      if (nrdiv.gt.NRDIVMAX) then
         write(6,*) 'PERROR(make_div): nrdiv > NRDIVMAX' 
         write(6,*) 'nrdiv = ',nrdiv
         write(6,*) 'NRDIVMAX = ',NRDIVMAX
         stop
      endif

      do i=1,nrdiv
         r = r1 + (i-1)*drgrid
         rgrid(i)=r
         rm = r1 + (i-0.5)*drgrid
         r = r1 + (i-1)*drgrid
         nx = 2*pi*rm/drgrid * WHOLE
         dthgrid(i) = 2*pi/nx * WHOLE
         nthgrid(i) = nx
         write(6,*) 'PINFO(make_div): ir ',i,r,nx,dthgrid(i),nthgrid(i)
      enddo

      ithgrid(1)=0
      write(6,*) ithgrid(1)
      do i=2,nrdiv
         ithgrid(i) = ithgrid(i-1)+nthgrid(i-1)
          write(6,*) ithgrid(i)
      enddo
 
      ngridtot = ithgrid(nrdiv) + nthgrid(nrdiv)
      write(6,*) 
      write(6,*) ngridtot

      if (ngridtot.gt.NDIVMAX) then
         write(6,*) 'PERROR(make_div): ngridtot > NDIVMAX'
         write(6,*) 'ngridtot = ',ngridtot
         write(6,*) 'NDIVMAX  = ',NDIVMAX
         call instop
      endif

      open(9,file='div.dat')
      do i=1,nrdiv
         r = r1 + (i-1)*drgrid
         rm = r1 + i*drgrid
         th=0
         do j=1,nthgrid(i)
            y1 = r*cos(th)
            x1 = r*sin(th)
            th = th+dthgrid(i)
            y2 = r*cos(th)
            x2 = r*sin(th)
            y1 = rm*cos(th)
            x1 = rm*sin(th)
            write(9,*) (x1+x2)*0.5,(y1+y2)*0.5
         enddo
      enddo
      r = nrdiv*drgrid
      th=0
      do j=1,nthgrid(nrdiv)
         y1 = r*cos(th)
         x1 = r*sin(th)
         th = th+dthgrid(i)
         y2 = r*cos(th)
         x2 = r*sin(th)
      enddo

      close(9)  
      return
      end

