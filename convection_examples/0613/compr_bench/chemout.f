c *************************************************************
c *   CHEMOUT 
c *
c *   PvK 970413
c *************************************************************
      subroutine chemout(chemmark,chemreal,nchem,no)
      implicit none
      integer nchem
      real*8 chemmark(nchem,*)
      real*4 chemreal(*)
      integer no
      include 'tracer.inc'
      include 'SPcommon/ctimen'
      character*80 fname
      integer ip,i,ntot,ichem
      real*8 x,y

      write(fname,'(''chem.'',i3.3)') no
      open(88,file=fname,form='unformatted')
      write(88) ndist,(ntrac(idist),idist=1,ndist),nchem

      ntot=0
      do idist=1,ndist
         ntot=ntot+ntrac(idist)
      enddo

      do ichem=1,nchem
         do i=1,ntot
            chemreal(i) = chemmark(ichem,i)
         enddo
   
         write(88) (chemreal(i),i=1,ntot)
      enddo

      write(6,*) 'Chemistry stored at t = ',t,no

      return
      end
