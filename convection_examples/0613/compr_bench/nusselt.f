      subroutine nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d) 
      implicit none
      integer kmesh2(*),kprob2(*),isol2(*),iuser(*),nq_a
      real*8 user(*),q_a(*),q_a_d(*)
      include 'ccc.inc'
      
      if (cyl) then
         if (axi) then
            call axinus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
         else
            call cylnus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
         endif
      else
         call cartnus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      endif
      return
      end
c *************************************************************
c *   AXINUS
c *
c *   Calculate local and integrated heatflow in axisymmetric
c *   geometry.
c *   Tested for conductive solution  + heat balance for simple
c *   steady state convective patterns.
c *
c *   output: q_a(1)  = average surface heatflow
c *               2   = average bottom heat flow
c *               3   = average bottom temperature
c *               4   = average surface temperature
c *
c *   Two approaches: 1) calculate by spherical trapezoid rule
c *                   2) use bounin
c *   Yield identical results.
c * 
c *   PvK 071000
c *************************************************************
      subroutine axinus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d) 
      implicit none
      real*8 pi
      parameter(pi=3.1415926 535898)
      integer kmesh2(*),kprob2(*),isol2(*),iuser(*),nq_a
      real*8 user(*),q_a(*),q_a_d(*)
      integer ielhlp,igradt(5),icurvs(10),i
      real*8 funcx(8000),funcy(8000),rkap1,rkap2
      real*8 funccf,fint,bounin
      save icurvs,funcx,funcy,igradt
      include 'bound.inc'
      include 'ccc.inc'
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'

      funcx(1)=8000
      funcy(1)=8000
      iuser(2)=1
      call deriva(2,2,0,1,2,igradt,kmesh2,kprob2,isol2,
     v                isol2,iuser,user,ielhlp)
 
      if (idiftype.eq.1) then
         rkap1=funccf(4,r1,0d0,0d0)
         rkap2=funccf(4,r2,0d0,0d0)
      else
         rkap1=1d0
         rkap2=1d0
      endif

c     *** Approach 1
c     icurvs(1)=0
c     icurvs(2)=ibot
c     call compcr(0,kmesh2,kprob2,igradt,0,icurvs,funcx,funcy)
c     call trap_spher(funcx,funcy,fint)
c     q_a(2) = -2*pi*rkap1*r1*r1*fint/surf(1)
c
c     icurvs(2)=itop
c     call compcr(0,kmesh2,kprob2,igradt,0,icurvs,funcx,funcy)
c     write(6,*) 'funcx: ',funcx(5),funcy(5)
c     call trap_spher(funcx,funcy,fint)
c     write(6,*) 'top: ',fint,rkap2,r2
c     q_a(1) = -2*pi*rkap2*r2*r2*fint/surf(2)
c     **** End Approach 1

c     *** Approach 2
c     *** Integrated heat flow along top and bottom curve
      q_a(1)= -bounin(2,4,1,1,kmesh2,kprob2,itop,itop,igradt,iuser,user) 
      q_a(2)= -bounin(2,4,1,1,kmesh2,kprob2,ibot,ibot,igradt,iuser,user) 
c     *** Note: rkap2 and surf(2) correspond to r=r2 (top)
      q_a(1) = rkap2*q_a(1)/surf(2)
      q_a(2) = rkap1*q_a(2)/surf(1)

c     *** Integrated temperature along top and bottom
      q_a(3) = bounin(1,4,1,1,kmesh2,kprob2,ibot,ibot,isol2,iuser,
     v                user)
      q_a(4) = bounin(1,4,1,1,kmesh2,kprob2,itop,itop,isol2,iuser,
     v                user)
      q_a(3) = q_a(3)/surf(1)
      q_a(4) = q_a(4)/surf(2)

c     *** Local heat flow at corners
      icurvs(1)=0
      icurvs(2)=itop
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(5) = -rkap2*funcy(6)
      q_a(6) = -rkap2*funcy(5+funcy(5))

      icurvs(1)=0
      icurvs(2)=ibot
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(7) = -rkap1*funcy(6)
      q_a(8) = -rkap1*funcy(5+funcy(5))

      do i=1,2
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo
      do i=3,4
         q_a_d(i) = q_a(i)*DeltaT_dim
      enddo
      do i=5,8
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo


      return
      end

      subroutine trap_spher(funcx,funcy,fint)
      implicit none
      real*8 funcx(*),funcy(*),fint
      real*8 x0,x1,y0,y1,th0,th1,f0,f1,dfint
      real*8 cost0,cost1,sint0,sint1,dtdx,dtdy,rr0,rr1
      integer i,npcurv

      npcurv = funcx(5)/2
      fint = 0
      do i=1,npcurv-1
c        *** cartesian coordinates
         x0=funcx(5+2*i-1)
         y0=funcx(5+2*i)
         x1=funcx(5+2*i+1)
         y1=funcx(5+2*i+2)
c        *** spherical coordinates
         rr0=sqrt(x0*x0+y0*y0)
         rr1=sqrt(x1*x1+y1*y1)
         cost0 = y0/rr0
         cost1 = y1/rr1
         sint0 = x0/rr0
         sint1 = x1/rr1
         th0 = acos(cost0)
         th1 = acos(cost1)
c        *** value of heat flow perpendicular to surface
         dtdx = funcy(5+2*i-1)
         dtdy = funcy(5+2*i)
         f0 = (dtdx*x0+dtdy*y0)/rr0
         dtdx = funcy(5+2*i+1)
         dtdy = funcy(5+2*i+2)
         f1 = (dtdx*x1+dtdy*y1)/rr1
c        f0=1
c        f1=1
c        *** value of integral over this segment
         dfint = f1*(th1*cost1-sint1+sint0-th0*cost1)  +
     v           f0*(th0*cost0-sint0+sint1-th1*cost0)
         fint = fint + dfint/(th0-th1)
      enddo   

      return
      end
c *************************************************************
c *   CYLNUS
c *
c *   Calculate local and integrated heatflow in cylindrical
c *   Tested for conductive solution + heat balance for simple
c *   steady state convective patterns.
c *
c *   output: q_a(1)  = average surface heatflow
c *               2   = average bottom heat flow
c *               3   = average bottom temperature
c *               4   = average surface temperature
c *           q_a_d : as q_a but in dimensional form
c *
c *   PvK 071000
c *************************************************************
      subroutine cylnus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      implicit none
      real*8 pi
      parameter(pi=3.1415926 535898)
      integer kmesh2(*),kprob2(*),isol2(*),iuser(*),nq_a
      real*8 user(*),q_a(*),q_a_d(*)
      integer ielhlp,igradt(5),icurvs(10),i
      real*8 funcx(8000),funcy(8000),rkap1,rkap2
      real*8 funccf,fint,bounin
      save icurvs,funcx,funcy,igradt
      include 'bound.inc'
      include 'ccc.inc'
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'

      funcx(1)=8000
      funcy(1)=8000
      call deriva(2,2,0,1,2,igradt,kmesh2,kprob2,isol2,
     v                isol2,iuser,user,ielhlp)
 
c     *** make sure to get correct values for thermal diffusivity
      if (idiftype.eq.1) then
         rkap1=funccf(4,r1,0d0,0d0)
         rkap2=funccf(4,r2,0d0,0d0)
      else
         rkap1=1d0
         rkap2=1d0
      endif

c     *** Approach 2
c     *** Integrated heat flow along top and bottom curve
c     *** irule=3 for Cartesian
      q_a(1) = bounin(2,3,1,1,kmesh2,kprob2,itop,itop,igradt,iuser,user) 
      q_a(2) = bounin(2,3,1,1,kmesh2,kprob2,ibot,ibot,igradt,iuser,user) 
c     *** Note: rkap2 and surf(2) correspond to r=r2 (top).
c     write(6,*) 'q_a(1): ',q_a(1),surf(1),surf(2)
      q_a(1) = -rkap2*q_a(1)/surf(2)
      q_a(2) = -rkap1*q_a(2)/surf(1)

c     *** Integrated temperature along top and bottom
      q_a(3) = bounin(1,3,1,1,kmesh2,kprob2,ibot,ibot,isol2,iuser,
     v                user)
      q_a(4) = bounin(1,3,1,1,kmesh2,kprob2,itop,itop,isol2,iuser,
     v                user)
c     write(6,*) 'temperature from bounin: ',q_a(3),q_a(4)
      q_a(3) = q_a(3)/surf(1)
      q_a(4) = q_a(4)/surf(2)

c     *** Local heat flow at corners
      icurvs(1)=0
      icurvs(2)=itop
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(5) = -rkap2*funcy(6)
      q_a(6) = -rkap2*funcy(5+funcy(5))

      icurvs(1)=0
      icurvs(2)=ibot
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(7) = -rkap1*funcy(6)
      q_a(8) = -rkap1*funcy(5+funcy(5))

      do i=1,2
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo
      do i=3,4
         q_a_d(i) = q_a(i)*DeltaT_dim
      enddo
      do i=5,8
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo

      return
      end

c *************************************************************
c *   CARTNUS
c *
c *   Calculate local and integrated heatflow in Cartesian geometry
c *
c *   output: q_a(1)  = average surface heatflow
c *               2   = average bottom heat flow
c *               3   = average bottom temperature
c *               4   = average surface temperature
c *           q_a_d : as q_a but in dimensional form
c *
c *   PvK 071000
c *************************************************************
      subroutine cartnus(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      implicit none
      real*8 pi
      parameter(pi=3.1415926 535898)
      integer kmesh2(*),kprob2(*),isol2(*),iuser(*),nq_a
      real*8 user(*),q_a(*),q_a_d(*)
      integer ihelp,igradt(5),icurvs(10),i
      real*8 funcx(8000),funcy(8000),rkap1,rkap2
      real*8 funccf,fint,bounin
      save icurvs,funcx,funcy,igradt
      include 'bound.inc'
      include 'ccc.inc'
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'

      funcx(1)=8000
      funcy(1)=8000
      call deriva(0,2,0,1,0,igradt,kmesh2,kprob2,isol2,
     v                isol2,iuser,user,ihelp)
 
c     *** make sure to get correct values for thermal diffusivity
      if (idiftype.eq.1) then
         rkap1=funccf(4,r1,0d0,0d0)
         rkap2=funccf(4,r2,0d0,0d0)
      else
         rkap1=1d0
         rkap2=1d0
      endif

c     *** Approach 2
c     *** Integrated heat flow along top and bottom curve
c     *** irule=3 for Cartesian
      q_a(1) = bounin(2,1,1,1,kmesh2,kprob2,itop,itop,igradt,iuser,user)
      q_a(2) = bounin(2,1,1,1,kmesh2,kprob2,ibot,ibot,igradt,iuser,user)
c     *** Note: rkap2 and surf(2) correspond to r=r2 (top).
c     write(6,*) 'q_a(1): ',q_a(1),surf(1),surf(2)
      q_a(1) = -rkap2*q_a(1)/surf(2)
      q_a(2) = -rkap1*q_a(2)/surf(1)

c     *** Integrated temperature along top and bottom
      q_a(3) = bounin(1,1,1,1,kmesh2,kprob2,ibot,ibot,isol2,iuser,
     v                user)
      q_a(4) = bounin(1,1,1,1,kmesh2,kprob2,itop,itop,isol2,iuser,
     v                user)
c     write(6,*) 'temperature from bounin: ',q_a(3),q_a(4)
      q_a(3) = q_a(3)/surf(1)
      q_a(4) = q_a(4)/surf(2)

c     *** Local heat flow at corners
      icurvs(1)=0
      icurvs(2)=itop
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(5) = -rkap2*funcy(6)
      q_a(6) = -rkap2*funcy(5+funcy(5))

      icurvs(1)=0
      icurvs(2)=ibot
      call compcr(0,kmesh2,kprob2,igradt,-1,icurvs,funcx,funcy)
      q_a(7) = -rkap1*funcy(6)
      q_a(8) = -rkap1*funcy(5+funcy(5))

      do i=1,2
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo
      do i=3,4
         q_a_d(i) = q_a(i)*DeltaT_dim
      enddo
      do i=5,8
         q_a_d(i) = q_a(i)*cp_dim*rkappa_dim*DeltaT_dim*rho_dim/
     v              height_dim
      enddo

      return
      end

