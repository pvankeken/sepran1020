c *************************************************************
c *   VELINTMARKc
c * 
c *   Find the velocity in marker xm,ym. output in u,v.
c *************************************************************
      subroutine velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,npoint,nelem, 
     v              nelgrp,u,v,iel,user) 
      implicit none
      real*8 xm,ym,u,v,user(*)
      integer ikelmc,ikelmi,ikelmo,iel,k,nelem,nelgrp
      real*8 un(6),vn(6),xn(6),yn(6),shapef(6)
      integer nodno(6),npoint,icorrect,imissed,ifound
      integer nodlin(3),isub
      real*8 rl(3)
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      common /pedebug/ debug
      logical debug
      logical midflag,dotflag
      logical guess_first
      real*8 xi,eta


c     *** find velocity and coordinates for the element in which
c     *** this tracer is located
      call pedeteltrac(1,xm,ym,ikelmc,ikelmi,ikelmo,nodno,nodlin,rl,
     v     xn,yn,un,vn,user,iel,npoint,nelem,imissed,ifound,
     v     nelgrp,shapef,xi,eta,guess_first,'velintmarkc')

c     write(6,'(''iel: '',i5,3(2f9.4,2x))') iel,xn(1),yn(1),
c    v             xn(3),yn(3),xn(5),yn(5)
c     write(6,'(''iel: '',5x,3(2f9.4,2x))')     un(1),vn(1),
c    v             un(3),vn(3),un(5),vn(5)
c     write(6,'(''sum of shapef: '',f9.4)') shapef(1)+shapef(2)+
c    v        shapef(3)+shapef(4)+shapef(5)+shapef(6)
c     *** compute quadratic shapefunctions
c     call detshape(xn,yn,xm,ym,shapef)
c     *** Find velocity in (xm,ym)
      u=0
      v=0
      do k=1,6
         u=u+shapef(k)*un(k)
         v=v+shapef(k)*vn(k)
      enddo

      return
      end

