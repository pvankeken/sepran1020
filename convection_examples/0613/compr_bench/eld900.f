      subroutine eld900 ( coor, elemvc, iuser, user, index1, index3,
     +                    index4, vecold, islold, vecloc, wrk1 )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.19 date 31-08-2007 New call to el0900
!        version  3.18 date 19-04-2007 New call to el0900
!        version  3.17 date 08-02-2006 Replace part of body by subroutines
!        version  3.16 date 19-02-2004 Correction for mesh velocity
!        version  3.15 date 04-07-2003 Remove common celwrk
!        version  3.14 date 19-05-2003 New call to elm800basefn
!        version  3.13 date 16-04-2003 Use elm800basefn instead of elp routines
!        version  3.12 date 03-03-2003 New call to el2005
!        version  3.11 date 08-12-2002 New call to el0900
!        version  3.10 date 08-11-2002 New call to el0900
!        version  3.9  date 17-08-2001 New call to el1000
!        version  3.8  date 15-04-2001 New: special form of equations
!        version  3.7  date 26-06-2000 New call to elp624
!        version  3.6  date 29-02-2000 Set iseqin
!        version  3.5  date 12-05-1999 New call to el1005, el4917
!        version  3.4  date 26-03-1999 Correction call to el1005
!        version  3.3  date 14-03-1999 Extension with icheld = 42..53
!        version  3.2  date 30-01-1999 New calls of elp subroutines
!        version  3.1  date 06-12-1998 Do not call el1000 for type 902
!        version  3.0  date 02-11-1998 New parameter list
!
!   copyright (c) 1992-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element derivative vector and weights for Navier-Stokes
!     equations
!     Two and three dimensional elements
!     ELD900 is a help subroutine for subroutine DERIV (TO0047)
!     it is called through the intermediate subroutine eld000
!     So:
!     DERIV
!     TO0047
!       -  Loop over elements
!          -  ELD000
!             - ELD900
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  4 node bilinear quadilateral (Type 900)
!         5 node bilinear quadilateral (Type 901)
!         6 node extended triangle (Type 900)
!         7 node extended triangle (Type 901)
!         9 node biquadratic quadilateral (Types 900 and 901)
!    3D: 27 node brick (Types 900 and 902)
!         15 node tetrahedron (Type 902)
! **********************************************************************
!
!                       KEYWORDS
!
!     derivative
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/cactl'
      include 'SPcommon/celiar'
      include 'SPcommon/celint'
      include 'SPcommon/cinforel'
      include 'SPcommon/celwrk'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemvc(*), coor(*), user(*),
     +                 vecold(*), vecloc(*), wrk1(*)
      integer iuser(*), index1(*), index3(numold,*), index4(*),
     +        islold(5,*)

!     coor      i    array of length ndim x npoint containing the co-ordinates
!                    of the nodal points with respect to the global numbering
!                    x  = coor (1,i);  y  = coor (2,i);  z  = coor (3,i);
!                     i                 i                 i
!     elemvc    o    Element vector to be computed,
!                    Length depends on application
!     index1    i    Array of length inpelm containing the point numbers
!                    of the nodal points in the element
!     index3    i    Two-dimensional integer array of length NUMOLD x NINDEX
!                    containing the positions of the "old" solutions in array
!                    VECOLD with respect to the present element
!                    For example VECOLD(INDEX3(i,j)) contains the j th
!                    unknown with respect to the i th old solution vector.
!                    The number i refers to the i th vector corresponding to
!                    IVCOLD in the call of SYSTM2 or DERIVA
!     index4    i    Two-dimensional integer array of length NUMOLD x INPELM
!                    containing the number of unknowns per point accumulated
!                    in array VECOLD with respect to the present element.
!                    For example INDEX4(i,1) contains the number of unknowns
!                    in the first point with respect to the i th vector stored
!                    in VECOLD.
!                    The number of unknowns in the j th point with respect to
!                    i th vector in VECOLD is equal to
!                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold    i    User input array in which the user puts information
!                    of all preceding solutions
!                    Integer array of length 5 x numold
!     iuser     i    Integer user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     user      i    Real user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     vecloc    i    Work array in which all old solution vectors for the
!                    integration points are stored
!     vecold    i    In this array all preceding solutions are stored, i.e.
!                    all solutions that have been computed before and
!                    have been carried to system or deriva by the parameter
!                    islold in the parameter list of these main subroutines
!     wrk1           work array of length 163 + 238m**3 + 6m + 15n**2 + 10n
!                    contents:
!                    Starting address    length   Name:
!                    1                   9*27     ARRAY
!                    ipugs               3*27     UGAUSS
!                    ipdudx              9*27     DUDX
!                    ipphix              27*27*3  PHIKSI for elp633
!                    ipetef              27       ETHA_EFFECTIVE
!                    ipseci              27       SECOND_INVARIANT
!                    ipdetd              27       DERIVATIVE of
!                                                 SECOND_INVARIANT
!                    ipsp                81*4     SP_matrix (Gradient P)
!                    ipdiv               81*4     DIV_matrix (Divergence matrix)
!                    ipwork              27*31    Work space
!                    ipgrad              9*7      Gradient (2D)
!                    ipsigm              6*7      Stress tensor (2D)
!                    iptran              12*2     Transformation matrix based
!                                                 on div
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    iptrnp              12*2     Transformation matrix based
!                                                 on sp
!                                                 (2D only, does not contribute
!                                                 to total length, since this
!                                                 length is maximized for 3d)
!                    Total length: ipend
!                    The subarrays contain the following contents:
!                    ARRAY:
!                    array in which the values of the coefficients
!                    in the integration points are stored in the sequence:
!                         2D                  3D
!                    1:   rho              rho
!                    2:   omega            omega
!                    3:   f1               f1
!                    4:   f2               f2
!                    5:                    f3
!                    6:   eta              eta
!                    7:   rho in nodal points (mcont=1 only)
!                    7:   G_L              G_L (mcont=2 only)
!                    8:   kappa            kappa
!                    9:   fdiv             fdiv
!                    Each coefficient requires exact m (is number of
!                    integration points) positions
!                    DPHIDX:
!                    Array of length inpelm x m x ndim containing the
!                    derivatives of the basis functions in the sequence:
!                    d phi / dx = dphidx (i,k,1);  d phi / dy = dphidx (i,k,2);
!                         i                             i
!                    d phi / dz = dphidx (i,k,3)
!                         i
!                      in node k
!                    WORK
!                    General work space
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer isub, isup, jelp, jtime, ipugs, ioldm,
     +        ipwork, ipphix, mconv, modelv, ipdudx, ipsp, ipetef,
     +        ipseci, ipdetd, jcheld, mult, ipgrad, ipsigm, iptran,
     +        mcont, ipdiv, iptrnp, iprho, ipphi, ipenalty,
     +        nveloc, npres, nsave, imesh, mdiv, mcoefconv, modelrhsd,
     +        ipunew, ipunewgs, ipfdiv, ipcoeffgrad, ipcoeffdiv
      double precision qmat(3,27), penalp, cn, clamb, p(4), tang(1),
     +                 rhsdiv(4), rhsdivtr(3), dphidx(3*27*27),
     +                 x(81), w(54), xgauss(81), uold(81)
      logical debug
      save isub, isup, jelp, jtime, ipwork, ipphix, ipugs,
     +     mconv, modelv, penalp, cn, clamb, ipdudx, ipsp, ipetef,
     +     ipseci, ipdetd, mult, ipgrad, ipsigm, iptran, mcont,
     +     ipdiv, iptrnp, iprho, nveloc, jcheld, imesh, ipphi,
     +     ipunew, ipunewgs, ipfdiv, ipcoeffgrad, ipcoeffdiv

!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     dphidx         Array of length n x m x ndim  containing the derivatives
!                    of the basis functions in the sequence:
!                    d phi / dx (xg ) = dphidx (i,k,1);
!                         i        k
!                    d phi / dy (xg ) = dphidx (i,k,2);
!                         i        k
!                    d phi / dz (xg ) = dphidx (i,k,3);
!                         i        k
!                    If the element is a linear simplex, the
!                    derivatives are constant and only k = 1 is filled.
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     ioldm          Dummy parameter
!     ipcoeffdiv     Starting address of array coeffdiv
!     ipcoeffgrad    Starting address of array coeffgrad
!     ipdetd         Starting address in array work of detdsc
!     ipdiv          Starting address in array work of div (2D only)
!     ipdudx         Starting address in array work of dudx
!     ipenalty       Indicates whether a penalty term must be added to
!                    the velocity part (1) or not (0)
!     ipetef         Starting address in array work of etheff
!     ipfdiv         Starting address of array fdiv
!     ipgrad         Starting address in array work of gradv (2D only)
!     ipphi          Starting address in array wrk1 of phi
!     ipphix         Starting address in array work of phiksi
!     iprho          Starting address in array work of rho
!     ipseci         Starting address in array work of secinv
!     ipsigm         Starting address in array work of the stress tensor sigma
!                    (2D only)
!     ipsp           Starting address in array work of sp
!     iptran         Starting address in array work of trans with respect to
!                    div
!     iptrnp         Starting address in array work of trans with respect to
!                    grad
!     ipugs          Starting address in array work of ugauss
!     ipunew         Starting address of mesh velocity
!     ipunewgs       Starting address of mesh velocity in gauss points
!     ipwork         Starting address in array work of work array
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     jcheld         if ( icheld<= 10 ) then icheld else icheld-20
!     jelp           Indication of the type of basis function subroutine must
!                    be called by ELM100. Possible vaues:
!                     1:  Subroutine ELP610  (Linear line element)
!                     2:  Subroutine ELP611  (Quadratic line element)
!                     3:  Subroutine ELP620  (Linear triangle)
!                     4:  Subroutine ELP622  (Quadratic triangle)
!                     5:  Subroutine ELP621  (Bilinear quadrilateral)
!                     6:  Subroutine ELP623  (Biquadratic quadrilateral)
!                     7:  Subroutine ELP624  (Extended quadratic triangle)
!                    11:  Subroutine ELP630  (Linear tetrahedron)
!                    12:  Subroutine ELP631  (Quadratic tetrahedron)
!                    13:  Subroutine ELP632  (Trilinear brick)
!                    14:  Subroutine ELP633  (Triquadratic  brick)
!                    16:  Subroutine ELP637  (Extended quadratic tetrahedron)
!     jtime          Integer parameter indicating if the mass matrix for the
!                    heat equation must be computed (>0) or not ( = 0)
!                    If jtime = 1, then the coefficient for the mass matrix is
!                    constant, otherwise (jtime = 2) it is variable
!     mcoefconv      Defines how the convetive term is treated:
!                    0: standard case
!                    1: special case: the convective term is defined as
!                          c_conv_1 u du/dx + c_conv_2 v du/dy
!                          c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!     mconv          see USER INPUT in subroutine ELM600
!     mdiv           Defines if divergence right-hand side is used (1) or
!                    not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     mult           Multiplication factor for the number of unknowns per point
!     npres          Number of pressure parameters
!     nsave          Help parameter to save the value of n
!     nveloc         Number of velocity parameters
!     p              Array to store the values of p in the element
!     penalp         Parameter eps for the penalty function formulation
!     qmat           transformation matrix global -> local coordinates
!                    in case of a 3D element
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     rhsdivtr       Transformed divergence right-hand side (if mdiv=1)
!     tang           Dummy
!     uold           Array to store the old solution in the nodal points
!     w              array of length m containing the weights for the
!                    numerical integration
!     x              array of length inpelm x 2 containing the x and
!                    y-coordinates of the nodal points with respect to the
!                    local numbering in the sequence:
!                    x  = x (i,1);  y  = x (i,2);
!                     i              i
!     xgauss         Array of length m x 2 containing the co-ordinates of
!                    the gauss points.  Array xgauss is only filled when Gauss
!                    integration is applied.
!                    xg  = xgauss (i,1);  yg  = xgauss (i,2);
!                      i                    i
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0900GEN      Performs the initializations for subroutine ELM900, ELI900
!                    or ELD900 in the case that IFIRST = 0
!     EL1005         fill of derivatives and weights in elemvc and elemwg
!                    with respect to subroutine ELD250
!     EL1007         Fill element vector for vertices only
!     EL1014         Restrict values of u, x and dudx to centroid
!     EL2005         Compute a local vector and its derivatives from a known
!                    vector
!     EL2006         compute the pressure for the incompressible Navier-Stokes
!                    eq.
!     EL4057         Compute the pressure for the incompressible Navier_Stokes
!                    equation in case of the penalty method
!     EL4917         Compute special derivatives for Navier-Stokes
!     ELD900ADDRESS  Compute the parameter jcheld
!                    Compute parameter mult for length of elemvc/wg
!                    Compute starting addresses of subarrays in array
!     ELD900COEFFS   Fill variable coefficients in coefficients array
!                    Fill velocity from prior iteration if necessary
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM900DIVGRAD  Compute the divergence and gradient matrix for
!                    Navier-Stokes
!                    Incompressible equations
!     ELM900TRANS    Compute the transformation matrix
!                    Compute the velocity in the centroid and the derivative
!                    of u
!     ELSTRUPD       Update stress term due to initial stress
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  ISEQ:  Sequence number of vector in VECOLD/ISLOLD from which UOLD and
!         derivatives must be computed.
!         If 0 or 1 the first vector is used
!         If more vectors are needed, these vectors are supposed to be stored
!         sequentially, where the first one is indicated by ISEQ
!     2:  type of viscosity model used (modelv)
!         Possible values:
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!     5:  Information about the convective terms (mconv)
!         Possible values:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!     6-20: Information about the equations and solution method
!     6:  Parameter penalp for the penalty function method
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-20: not yet used
!     Remark:  the coefficients 2-20 are only used in the case of icheld = 6
!              or 11, i.e. computation of the stress tensor
!              In the case icheld = 7 and an extended quadratic triangle
!              also all parameters are used
!     icheld                    Cartesian            Cylindrical/Polar
!       1                        du        / d
!                                  jdegfd     ix
!       2                        grad u               2D/Cyl: (r,z)
!                                i.e. du1/dx1,        dur/dr, duz/dr,
!                                du2/dx1, ...         dur/dz, duz/dz
!                                                     3D/Cyl: (r,z,phi)
!                                                     dur/dr, duz/dr, duphi/dr
!                                                     dur/dr, duz/dz, duphi/dz
!                                                     dur/rdphi-uphi/r,
!                                                     duz/rdwrk1(ipphi),
!                                                     duphi/rdphi+ur/r
!                                                     2D/Pol: (r,phi)
!                                                     dur/dr, duz/dr,
!                                                     dur/rdphi-uphi/r,
!                                                     duphi/rdphi+ur/r
!       3                        - grad u
!       4                        div u
!       5                        curl u                  Cylinder co-or
!                     2D:       du2/dx1-du1/dx2      2D:   duz/dr-dur/dz
!                     3D:       du3/dx2-du2/dx3      3D:   duz/rdphi-duphi/dz
!                               du1/dx3-du3/dx1       duphi/dr+(uphi-dur/dphi)/r
!                               du2/dx1-du1/dx2            dur/dz-duz/dr
!                                                         Polar co-or
!                                                     duphi/dr+(uphi-dur/dphi)/r
!                             | sigma x |                    | sigma r     |
!       6                     | sigma y |                    | sigma z     |
!                     sigma = | sigma z |            sigma = | sigma theta |
!                             | tau xy  |                    | tau rz      |
!                             | tau yz  |                    | tau zphi    |
!                             | tau zx  |                    | tau phir    |
!       7             pressure p
!       8             elongation
!                        ndim = 2, jcart # 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(1)*u(2)*(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(3)**2*gradu(3,3) + u(1)*u(2)*(gradu(1,2) +
!                             gradu(2,1)) + u(1)*u(3)*(gradu(1,3)+gradu(3,1)) +
!                             u(2)*u(3)*(gradu(2,3)+gradu(3,2)))/||u||*2
!       9             deformation
!                        ndim = 2, jcart # 4:
!                        g = (2u(1)u(2)(-gradu(1,1)+gradu(2,2)) +
!                             u(1)**2-u(2)**2(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        g1 = gradu(1,2) + gradu(2,1)
!                        g2 = gradu(2,3) + gradu(3,2)
!                        g3 = gradu(3,1) + gradu(1,3)
!      10             sqrt(secinv)
!      11             viscous dissipation: 1/2 * t:A1
!      21-31:  See 1-11, however, now defined in the vertices instead of the
!              nodal points
!      32-42:  See 1-11, but now per node per element
!      43-53:  See 1-11, but now per integration point per element
!     Structure of output arrays:
!     Solution array:  ndim unknowns per point (IVEC=0)
!     Arrays of special structure:
!     IVEC = 1:   1 unknowns per point
!     IVEC = 2:   2 unknowns per point
!     IVEC = 3:   3 unknowns per point
!     IVEC = 4:   6 unknowns per point
!     IVEC = 5:   ndim unknowns per point
!     IVEC = 6:   1 unknowns per vertex
!     IVEC = 7:   2 unknowns per vertex
!     IVEC = 8:   3 unknowns per vertex
!     IVEC = 9:   6 unknowns per vertex
!     IVEC =10:   ndim unknowns per vertex
!     IVEC =11:   1/3/6 unknowns per point
!     IVEC =12:   0/1/3 unknowns per point
!     IVEC =13:   1/4/9 unknowns per point
!     IVEC =14:   1/3/6 unknowns per vertex
!     IVEC =15:   0/1/3 unknowns per vertex
!     IVEC =16:   1/4/9 unknowns per vertex
!     ICHELD                 IVEC
!       1                      1
!       2                     13
!       3                     13
!       4                      1
!       5                     12
!       6                      4
!       7                      1
!       8                      1
!       9                     12
!      10                      1
!      11                      1
!      21                      6
!      22                     16
!      23                     16
!      24                      6
!      25                     15
!      26                      9
!      27                      6
!      28                      6
!      29                     15
!      30                      6
!      31                      6
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     if ( ifirst=0 ) then
!        Define element independent quantities
!        Compute pointers in array WORK
!     Compute basis functions and related quantities
!     Fill old solution into array UOLD and if necessary the derivatives
!     Compute variable coefficients if necessary
!     Compute derived quantities
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'eld900' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from eld900'
  1      format ( a, 1x, (10i6) )
  2      format ( a, 1x, (5d12.4) )

      end if
      if ( ierror/=0 ) go to 1000

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         call el0900gen ( iuser, user, jelp, jtime, penalp, cn,
     +                    clamb, mconv, mcont, modelv, isub, isup,
     +                    wrk1, work(1), nveloc, npres, imesh,
     +                    index4, mdiv, mcoefconv, modelrhsd, ioldm,
     +                    ipenalty )

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

!        --- Compute jcheld and starting addresses

         call  eld900address ( jcheld, mult, ipphi, ipugs,
     +                         mcont, iprho, ipdudx, ipphix, ipetef,
     +                         ipseci, ipdetd, ipsp, ipdiv, ipwork,
     +                         ipgrad, ipsigm, imesh, ipunew,
     +                         ipunewgs, iptran, iptrnp,
     +                         ipfdiv, ipcoeffgrad, ipcoeffdiv )

      end if
      if ( debug )
     +   write(irefwr,1) 'icheld, jcheld, jtrans, ipsigm, ipdudx',
     +                    icheld, jcheld, jtrans, ipsigm, ipdudx

!     --- compute basis functions and weights for numerical integration

      call elm800basefn ( coor, x, w, dphidx, xgauss, wrk1(ipphi),
     +                    index1, qmat, tang, jelp, wrk1(ipphix), work,
     +                    wrk1, wrk1 )
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      call eld900coeffs ( vecold, wrk1, dphidx, index3, index1,
     +                    index4, ipphi, ipugs, ipdudx, uold,
     +                    mcont, ipwork, isub, isup, iprho, iuser,
     +                    user, vecloc, x, xgauss, p, nveloc )
      if ( debug ) then
         call prinrl ( wrk1(ipdudx), n*ndim*ndim, 'dudx/1' )
         write(irefwr,1) 'mcont', mcont
      end if  ! ( debug )

!     --- Compute the divergence and gradient matrix

      if ( jcheld==7 .and. itype/=902 ) then

!     --- Compute the divergence and gradient matrix
!         Store in sp and div
!         pp is not filled and kappa not used

         call elm900divgrad ( wrk1(ipsp), wrk1(ipdiv), dphidx,
     +                        wrk1, wrk1(ipphi), w, x, xgauss,
     +                        mcont, wrk1(iprho), wrk1,
     +                        wrk1(ipwork), rhsdiv, wrk1(ipfdiv), mdiv,
     +                        ishape, wrk1(ipcoeffgrad),
     +                        wrk1(ipcoeffdiv) )
         if ( debug ) then
            write(irefwr,1) 'npres', npres
            call prinrl ( wrk1(ipsp), ndim*n*3, 'sp' )
            call prinrl ( wrk1(ipdiv), ndim*n*3, 'div' )
         end if  ! ( debug )

      end if

      if ( jtrans==1 .and. jcheld==7 ) then

!     --- Compute the transformation matrix
!         Compute the velocity in the centroid and the derivative of u

         call elm900trans ( wrk1(iptran), wrk1(ipdiv), mdiv, rhsdivtr,
     +                      rhsdiv, wrk1(iptrnp), wrk1(ipsp), uold,
     +                      wrk1(ipdudx), wrk1(ipphi), dphidx,
     +                      wrk1(ipugs), mcont, mconv, modelv, .true. )
         if ( debug )
     +      call prinrl1 ( wrk1(iptran), ndimlc, ndimlc*(n-1), 'trans' )

!        --- Store extra velocity for ALE mesh in wrk1

         if ( imesh/=0 )
     +      call el2005 ( vecold, wrk1(ipunew), wrk1(ipwork),
     +                    wrk1(ipphi), dphidx, index3, index1,
     +                    wrk1(ipunewgs), imesh, index4 )

      end if
      if ( debug ) call prinrl ( wrk1(ipdudx), n*ndim*ndim, 'dudx/2' )

!     --- Fill elemvc, content depends on icheld

      if ( jcheld==7 ) then

!     --- icheld = 7, 27, 20, 38 or 49
!         Compute the pressure and store in elemvc

         if ( icheld==7 .or. icheld==20 .or. icheld==38 .or.
     +        icheld==49 ) then

!        --- icheld = 7, fill in all nodes

            if ( jtrans==1 ) then

!           --- Special case, the centroid has been eliminated

               call el4917 ( 6, modelv, wrk1(1+5*m), wrk1(ipetef), cn,
     +                       clamb, xgauss, wrk1(ipugs), wrk1(ipdudx),
     +                       wrk1(ipseci), wrk1(ipgrad), 4,
     +                       wrk1(ipsigm), m, vecloc, maxunk, numold,
     +                       m )
               if ( itype==901 ) elemvc(1) = p(1)
               if ( debug ) write(irefwr,2) 'p(1)', p(1)
               if ( ind(10)/=0 ) then

!              --- An initial stress has been defined
!                  This is an ad-hoc approach and should be adapted once
!                  elm900 is adapted

                  call elstrupd ( wrk1(ipsigm), n, wrk1(1+9*m) )

               end if
               call el2006 ( elemvc, wrk1(ipsp), wrk1(ipdiv), w, x,
     +                       penalp, uold, wrk1(ipugs), wrk1(1+2*m),
     +                       wrk1(ipphi), wrk1, dphidx, wrk1(ipsigm),
     +                       wrk1(ipgrad), mconv, xgauss, wrk1(m+1),
     +                       icheld, wrk1(ipunew), wrk1(ipunewgs),
     +                       imesh )
               if ( debug ) call prinrl ( elemvc, n, 'elemvc' )

            else

!           --- Standard case

               call el4057 ( elemvc, wrk1(ipdiv), w, x, xgauss, penalp,
     +                       uold, p, icheld )

            end if

         else

!        --- icheld = 27, fill in vertices
!            First fill in all nodes

            if ( jtrans==1 ) then

!           --- Special case, the centroid has been eliminated

               call el4917 ( 6, modelv, wrk1(1+5*m), wrk1(ipetef), cn,
     +                       clamb, xgauss, wrk1(ipugs), wrk1(ipdudx),
     +                       wrk1(ipseci), wrk1(ipgrad), 4,
     +                       wrk1(ipsigm), m, vecloc, maxunk, numold,
     +                       m )
               if ( itype==901 ) elemvc(1) = p(1)
               call el2006 ( wrk1(ipwork), wrk1(ipsp), wrk1(ipdiv), w,
     +                       x, penalp, uold, wrk1(ipugs), wrk1(1+2*m),
     +                       wrk1(ipphi), wrk1, dphidx, wrk1(ipsigm),
     +                       wrk1(ipgrad), mconv, xgauss, wrk1(m+1),
     +                       icheld, wrk1(ipunew), wrk1(ipunewgs),
     +                       imesh )
            else

!           --- Standard case

               call el4057 ( wrk1(ipwork), wrk1(ipdiv), w, x, xgauss,
     +                       penalp, uold, p, icheld )

            end if

!           --- Copy in vertices

            call el1007 ( wrk1(ipwork), elemvc, mult, ishape )

         end if

      else if ( icheld<=5 .or. icheld>=32 .and. icheld<=36 ) then

!     --- 1<=icheld<=5 or 32<=icheld<=36,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, uold, wrk1(ipdudx),
     +                 x, mult, elemvc, m )

      else if ( icheld>=43 .and. icheld<=47 ) then

!     --- 43<=icheld<=47,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, wrk1(ipugs), wrk1(ipdudx),
     +                 xgauss, mult, elemvc, m )

      else if ( icheld<=11 .or. icheld>36 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation

         call el4917 ( jcheld, modelv, wrk1(1+5*m), wrk1(ipetef), cn,
     +                 clamb, xgauss, uold, wrk1(ipdudx), wrk1(ipseci),
     +                 wrk1(ipwork), mult, elemvc, m, vecloc, maxunk,
     +                 numold, m )

      else if ( icheld>20 .and. icheld<=25 ) then

!     --- 20<icheld<=25 fill elemvc by derivatives,
!         First fill in all nodes

         call el1005 ( icheld-20, ix, jdegfd, uold, wrk1(ipdudx),
     +                 x, mult, wrk1(ipwork), m )

!        --- Copy in vertices

         call el1007 ( wrk1(ipwork), elemvc, mult, ishape )

      else if ( icheld>11 .and. icheld<=15 ) then

!     --- 12<=icheld<=15
!         fill elemvc by derivatives
!         First set m equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, wrk1(ipdudx), x, ishape )
         m = 1
         call el1005 ( jcheld, ix, jdegfd, uold, wrk1(ipdudx),
     +                 x, mult, elemvc, nsave )

!        --- reset m

         m = nsave

      else if ( icheld<=20 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation
!         First set n equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, wrk1(ipdudx), x, ishape )
         m = 1
         call el4917 ( jcheld, modelv, wrk1(1+5*nsave), wrk1(ipetef),
     +                 cn,  clamb, xgauss, uold, wrk1(ipdudx),
     +                 wrk1(ipseci), wrk1(ipwork), mult, elemvc, m,
     +                 vecloc, maxunk, numold, nsave )

!        --- reset m

         m = nsave

      else

!     --- Compute stress tensor, elongation, shear rate or second invariant
!         First fill in all nodes

         call el4917 ( jcheld, modelv, wrk1(1+5*m), wrk1(ipetef), cn,
     +                 clamb, xgauss, uold, wrk1(ipdudx), wrk1(ipseci),
     +                 wrk1(ipwork+mult*n), mult, wrk1(ipwork), n,
     +                 vecloc, maxunk, numold, m )

!        --- Copy in vertices

         call el1007 ( wrk1(ipwork), elemvc, mult, ishape )

      end if

1000  call erclos ( 'eld900' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End eld900'

      end if

      end
