c *************************************************************
c *   CHECK_CURVED_ELEMENTS
c *   Check how many elements are curved. Set flags in 
c *   /elem_topo/ curved_elem(NELEM)
c *
c *   PvK 020304
c *************************************************************
      subroutine check_curved_elements(nelem,npoint,nodno,coor)
      implicit none
      integer nelem,npoint,nodno(*)
      real*8 coor(2,*),x(6),y(6)
      integer ielem,inpelm,i,icurved,ip
      real*8 dx,dy,fx,fy,eps
      logical curved
      include 'elem_topo.inc'

      eps = 1d-8
      inpelm = 6
      icurved = 0
      ip=0
      do ielem=1,nelem
         curved=.false.
         do i=1,inpelm
            x(i) = coor(1,nodno(ip+i))
            y(i) = coor(2,nodno(ip+i))
         enddo
         ip = ip+inpelm

         fx = abs( x(1)-2*x(2)+x(3) )
         fy = abs( y(1)-2*y(2)+y(3) )
         dx = abs(x(1)-x(3))
         dy = abs(y(1)-y(3))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.
         fx = abs( x(3)-2*x(4)+x(5) )
         fy = abs( y(3)-2*y(4)+y(5) )
         dx = abs(x(3)-x(5))
         dy = abs(y(3)-y(5))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.
         fx = abs( x(5)-2*x(6)+x(1) )
         fy = abs( y(5)-2*y(6)+y(1) )
         dx = abs(x(5)-x(1))
         dy = abs(y(5)-y(1))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.

         if (curved) then
            curved_elem(ielem) = .true.
            icurved = icurved + 1
c           write(6,*) 'curved element: ',ielem
c           do i=1,6
c              write(6,'(5x,2f12.8)') x(i),y(i) 
c           enddo
         else
            curved_elem(ielem) = .false.
         endif
      enddo

      write(6,*) 'Percentage of curved elements: ',icurved/nelem*100, 
     v   ' out of  ',nelem

  
      return
      end
            
