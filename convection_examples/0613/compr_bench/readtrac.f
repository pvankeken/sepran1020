c *************************************************************
c *   READTRAC
c *
c *   PvK 970419
c *************************************************************
      subroutine readtrac(fname)
      implicit none
      character*80 fname
      integer no
      include 'tracer.inc'
      include 'petrac.inc'
      include 'c1mark.inc'
      include 'SPcommon/ctimen'
      integer ip,i,ntot,nmark
      real*8 x,y

      if (itracoption.eq.1) then
c     write(fname,'(''tracers.'',i3.3)') no
         open(88,file=fname,form='unformatted')
         read(88) ndist,(ntrac(idist),idist=1,ndist)

         ntot=0
         do idist=1,ndist
            ntot=ntot+ntrac(idist)
         enddo

         do idist=1,ndist
           read(88) (coorreal(2*(ip+i)-1),coorreal(2*(ip+i)),
     v          i=1,ntrac(idist))
           ip = ip+ntrac(idist)
         enddo
         close(88)

         do i=1,2*ntot
           coormark(i) = coorreal(i)
         enddo
      endif
 
      if (itracoption.eq.2) then
         open(88,file=fname)
         i=1
100      continue
           read(88,*,end=200) coormark(2*i-1),coormark(2*i)
           i=i+1
           if (i.lt.NTRACMAX) goto 100
200      nmark=i
         imark(1)=nmark
      endif
         

      return
      end
