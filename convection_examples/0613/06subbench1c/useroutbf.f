c *************************************************************
c *   USEROUT
c *
c *   Code to output surface heat flow and geotherm along
c *   slab interface
c *   Dimensional heat flow by q=q'/r, with r=1d0/(k*dT/dz)=
c *    1d0/(3*1/1e3)=333.333; or q=q'*3 to output in mW/m^2.
c *************************************************************
      subroutine useroutbf(ibuffr,buffer,kmesh,kprob,isol,isequence,
     v        numvec,intmat)
      implicit none
      integer :: ibuffr(*)
      real(kind=8) :: buffer(*)
      integer(kind=8) :: ikelmn,iptemp,iniget,inidgt
      integer(kind=8) :: ikelmm,ikelmm3,ikelmm4,ikelmm2
      integer :: ncurvs,icurve,ninner
      integer :: elempersurf,nodperelem,isurf,nsurfs
      integer kmesh(*),kprob(*),isol(5,*),isequence,numvec
      integer intmat(*)
      integer ipkprb,iniprb,npoint,icurvs(10),i,npcurv,isigmaprime(5)
      integer NPMAX,NUNK1,NUNK2,NCOOR,NDIM,NUNK3,NPX,NPY,DPX
      parameter(NPMAX=10 000,NUNK1=1,NUNK2=3,NUNK3=6)
      parameter(NPX=111,NPY=101,NCOOR=NPX*NPY,NDIM=2,DPX=6)
      real*8 funcx(NPMAX),funcy(NPMAX),temp(NUNK1,NCOOR)
      real*8 x,y,coor(NDIM,NCOOR),uvp(NUNK2,NCOOR),dtdx(NDIM,NCOOR)
      real*8 sigma_prime(NUNK3,NCOOR),sigma_n(NPX),sigma_t(NPX),p_l(NPX)
      real*8 secinv(NUNK1,NCOOR),dpdx(NUNK1,NCOOR),dpdy(NUNK1,NCOOR)
      real*8 pressure(NUNK2,NCOOR),coors3(NDIM,NCOOR)
      integer iinder(10),iinmap(10),map(3),ix,iy,ip,mapuvp(3,NPY)
      integer ip1,length,ncoors3 
      real*8 uvps3(NUNK2,NCOOR),secinvs3(NUNK1,NCOOR)
      real*8 pressures3(NUNK2,NCOOR)
      real*8 dpdxs3(NUNK1,NCOOR),dpdys3(NUNK1,NCOOR)
      real*8 slab_velocity
      logical first_on_this_row
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'

      slab_velocity=rlcons(1)

      if (isequence.eq.1) then

c        *** interpolate temperature to regular set of grid points
c        *** Use two sets:  one for the whole grid; one for just the wedge
         iinmap(1)=0
         iinmap(2)=0
         ip=0
         do iy=1,NPY
            y = -(iy-1)*DPX
            do ix=1,NPX
              ip=ip+1
              x = (ix-1)*DPX
              coor(1,ip) = x
              coor(2,ip) = y
            enddo
         enddo
         open(9,file='coor.dat')
         ip=0
         do ip=1,NPY*NPX
            write(9,'(i5,2f15.7)') ip,coor(1,ip),coor(2,ip)
         enddo
         close(9)

c        ***
         ip=0
         do iy=1,NPY
            write(6,*) 'set up mapuvp: iy=',iy
            y = -(iy-1)*DPX
            first_on_this_row=.true.
            do ix=1,NPX
               x = (ix-1)*DPX
               if ((660-x.ge.-y).and.(y.lt.-50)) then 
c                 *** we're in the wedge
                  ip=ip+1
                  if (first_on_this_row) then
c                     *** record start of this array in the full grid
                      mapuvp(1,iy)=ix
c                     *** and that in the wedge only grid
                      mapuvp(2,iy)=ip
                      first_on_this_row = .false.
                  endif
                  coors3(1,ip)=x
                  coors3(2,ip)=y
c                 *** make sure last point gets recorded too
                  mapuvp(3,iy)=ip
               endif
            enddo
         enddo
         ncoors3 = ip

c        do i=1,ncoors3
c           write(6,*) 'wedge: ',coors3(1,i),coors3(2,i)
c        enddo

         do iy=1,NPY
            write(6,'(''cooruvp: '',4i5)') mapuvp(1,iy),mapuvp(2,iy),
     v         mapuvp(3,iy),mapuvp(3,iy)-mapuvp(2,iy)+1
         enddo
               
         
        
         write(6,*) 'Interpolate temperature'
         call intcoor ( kmesh, kprob, isol(1,2), temp, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         open(9,file='T.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (temp(1,ip+ix),ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)


c        *** reuse information in map
         iinmap(2)=0
c        *** output dimensional heatflow along the top of the model
         write(6,*) 'Interpolate heatflow'
         call intcoor ( kmesh, kprob, isol(1,5), dtdx, coor,
     v           NDIM , NCOOR, NDIM, iinmap, map)
         open(9,file='dtdx.dat')
c        *** The factor three comes from the dimensionalization
         do ix=NPX,1,-1
            write(9,*) coor(1,NPX-ix+1),-dtdx(2,ix)*3.
         enddo
         close(9)

c        *** Interpolate velocities and pressure to regular grid
c        *** recompute information in map
         iinmap(2)=0
         write(6,*) 'Interpolate velocities',NUNK2
         call intcoor(kmesh,kprob,isol(1,1),uvps3,coors3,
     v           NUNK2,ncoors3,NDIM,iinmap,map)     

         do i=1,ncoors3
            write(6,'(''wedge-uvp: '',5f15.7)') coors3(1,i),
     v       coors3(2,i),uvps3(1,i),uvps3(2,i),uvps3(3,i)
         enddo
 
!        something wrong with mapuvp; length for iy=1 is huge. Initialization?
!        ip=0
!        do iy=1,NPY
!           do ix=1,NPX
!              x = (ix-1)*DPX
!              y = -(iy-1)*DPX
!              if (660-x.le.-y) then 
!                 uvp(1,ip+ix)=-1.541492783
!                 uvp(2,ip+ix)=-1.541492783
!              endif
!           enddo
!           if (mapuvp(1,iy).gt.0) then
!              *** copy info from uvps3 to uvp
!              length = mapuvp(3,iy)-mapuvp(2,iy)+1
!              write(6,*) 'mapuvp: iy,length = ',iy,length
!              ip1=0
!              do ix=mapuvp(1,iy),length
!                 uvp(1,ip+ix) = uvps3(1,mapuvp(2,iy)+ip1)
!                 uvp(2,ip+ix) = uvps3(2,mapuvp(2,iy)+ip1)
!                 uvp(3,ip+ix) = uvps3(3,mapuvp(2,iy)+ip1)
!                 ip1=ip1+1
!              enddo
!           endif
!           ip=ip+NPX
!        enddo

c        *** output velocity in cm/yr
!        open(9,file='vx.dat')
!        ip=0
!        do iy=1,NPY
!           write(9,100) (uvp(1,ip+ix)/slab_velocity*5,ix=NPX,1,-1)
!           ip=ip+NPX
!        enddo
!        close(9)
!        open(9,file='vy.dat')
!        ip=0
!        do iy=1,NPY
!           write(9,100) (uvp(2,ip+ix)/slab_velocity*5,ix=NPX,1,-1)
!           ip=ip+NPX
!        enddo
!        close(9)
!        *** output in pressure MPa: kappa=0.7272e-6, eta=1e21, h=1e3.
!        open(9,file='p.dat')
!        ip=0
!        do iy=1,NPY
!           write(9,100) (uvp(3,ip+ix)*727.2,ix=NPX,1,-1)
!           ip=ip+NPX
!        enddo
!        close(9)
c        *** reuse information in map
         iinmap(2)=0

         write(6,*) 'Interpolate pressure',NUNK1
         call intcoor ( kmesh, kprob, isol(1,7), pressure, coor, 
     v           NUNK2, NCOOR, NDIM, iinmap, map)
         open(9,file='p1.dat')
         ip=0
         do iy=1,NPY
            write(9,100) (pressure(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)

c        *** compute and output dP/dx and dP/dy in MPa/km: 
         write(6,*) 'Interpolate pressure gradients',NUNK1
         call intcoor ( kmesh, kprob, isol(1,8), dpdx, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         call intcoor ( kmesh, kprob, isol(1,9), dpdy, coor, 
     v           NUNK1, NCOOR, NDIM, iinmap, map)
         open(9,file='dpdx.dat') 
         ip = 0
         do iy=1,NPY
            write(9,100) (dpdx(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)
         open(9,file='dpdy.dat') 
         ip = 0
         do iy=1,NPY
            write(9,100) (dpdy(1,ip+ix)*727.2,ix=NPX,1,-1)
            ip=ip+NPX
         enddo
         close(9)

      else if (isequence == 2) then
 
          if (kmesh(28) > 0) then
            ! print info on surfaces
            call ini060text(ibuffr,kmesh(28),'userout:kmeshn')
            ikelmn = iniget(kmesh(28))
            nsurfs = kmesh(12)
            isurf=1
            iptemp = ikelmn+isurf-1
            nodperelem = ibuffr(iptemp)
            iptemp = ikelmn+isurf+nsurfs-1
            elempersurf = ibuffr(iptemp)
            write(6,*) 'surface ',isurf,' has ',nodperelem
            write(6,*) 'points in each element and ',elempersurf
            write(6,*) 'elements '
         endif
         if (kmesh(27) > 0) then
            ! print info on curves
            call ini060text(ibuffr,kmesh(27),'userout:kmeshm')
            ikelmm = iniget(kmesh(27))
            ncurvs = kmesh(11)
            iptemp = ikelmm 
            ninner = ibuffr(iptemp)
            iptemp = ikelmm+1
            ikelmm3 = ikelmm+ibuffr(iptemp)
            iptemp = ikelmm+2
            ikelmm4 = ikelmm+ibuffr(iptemp)
            ikelmm2 = ikelmm+4
            icurve = 1
            write(6,*) 'kmeshm: ',(ibuffr(ikelmm+i),i=0,10)
            call printcurveinfo(ibuffr(ikelmm),ibuffr(ikelmm2),
     v           ibuffr(ikelmm3),icurve,ncurvs,ninner)
        endif
      endif

10    format(3i4,7f6.0)
100   format(111e15.7)
      end subroutine useroutbf
 
      subroutine printcurveinfo(kmeshm,kmeshm2,kmeshm3,icurve,
     v          ncurvs,ninner)
      implicit none
      integer :: kmeshm(*),kmeshm2(*),kmeshm3(*)
      integer :: icurve,ncurvs,ninner,i,j,npoints_here,ip

      write(6,*) 'there are ',ncurvs,' curves'
      write(6,*) 'ninner = ',ninner
      npoints_here = kmeshm2(icurve+1)-kmeshm2(icurve)
      write(6,*) 'curve ',icurve,' has ',npoints_here,' points'
      write(6,*) 'kmeshm2(1): ',kmeshm2(1)
      do i=1,npoints_here
         ip = kmeshm2(icurve)
         write(6,'(2i5)')   i,kmeshm3(ip+i-1)
      enddo
      

      end subroutine printcurveinfo
