c *************************************************************
c *   PECRESEC
c *
c *   Create a file with nodalpoint numbers that are the
c *   neighbours of secondary nodal points.
c *
c *   PvK 081189
c *************************************************************
      subroutine pecresec(nx,ny,fname)
      implicit none
      character*80 fname
      integer nx,ny,nbuf(2,50000)
      integer npx,npy,i,j,ip,nuspnt

      npx = 2*nx + 1
      npy = 2*ny + 1 
c     *** fill points of cat A
      do i=1,ny+1
         do j=2,npx-1,2
            ip = (i-1)*2*npx + j
            nbuf(1,ip) = ip-1
            nbuf(2,ip) = ip+1
c           write(6,*) 'A: ',ip,ip-1,ip+1
         enddo
      enddo
c     *** fill points of cat B            
      do i=1,ny
         do j=npx+1,2*npx,2
            ip = (i-1)*2*npx+j
            nbuf(1,ip) = ip-npx
            nbuf(2,ip) = ip+npx
c           write(6,*) 'B: ',ip,ip-npx,ip+npx
         enddo
      enddo
c     *** fill points of cat C
      do i=1,ny
         do j=npx+2,2*npx-1,2      
            ip = (i-1)*2*npx+j
            nbuf(1,ip) = ip-npx+1
            nbuf(2,ip) = ip+npx-1
c           write(6,*) 'C: ',ip,ip-npx+1,ip+npx-1
         enddo
      enddo

      open(9,file=fname)
      rewind(9)
      write(9,*) npx*npy
      do i=1,npx*npy
         write(9,*) nbuf(1,i),nbuf(2,i)
      enddo
      close(9)
      return
      end
      
      
