program poisson1D

integer, allocatable, dimension (:) :: ibuffr
integer pbuffr, error
parameter ( pbuffr=100000000)
allocate(ibuffr(pbuffr), stat = error)
if (error /= 0) then
  ! space for these arrays could not be allocated
  print *, "error: (sepcompexe) could not allocate space."
  stop
end if  ! (error /= 0)
 call sepcombf ( ibuffr, ibuffr, pbuffr )


end program poisson1D

subroutine userout(kmesh,kprob,isol,isequence,numvec)
implicit none
integer :: kmesh(*),kprob(*),isol(5,*)
integer :: isequence,numvec,nunks,ncoor,ndim,iincop(2)

integer :: i,npoint
real(kind=8),dimension(:),allocatable :: temp,dtdx
real(kind=8) :: dx,x

npoint=kmesh(8)
dx=4.0_8/(npoint-1)
allocate(temp(npoint+5),dtdx(npoint+5))
temp=0
dtdx=0
temp(1)=5+npoint
dtdx(1)=5+npoint

x=2*sqrt(2.0e0_8)
iincop(1)=2
iincop(2)=0
call copyus(iincop,isol,kmesh,kprob,temp)
i=min(npoint,nint(x/dx)+1)
write(6,*) temp(1:10)
write(6,*) i,temp(5+i),temp(5+i+1),4*x-0.5*x*x
!call copyus(iincop,isol(1,2),kmesh,kprob,dtdx)
!write(6,*) 'q1 = ',dtdx(1)
! intcoor doesn't wkkork for 1D
!call intcoor(kmesh,kprob,isol(1,1),temp,coor,nunks,ncoor,ndim,iinmap,map)
end subroutine userout
