!     MYSEPCOMP: replacement for 'sepcomp' when using user functions and
!     subroutines
      program mysepcomp
      implicit none

      call sepcom(0)

      end program mysepcomp

!     FUNCCF: used to specify coefficients as a function of space
      real*8 function funccf(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z
      real*8 pi
      parameter(pi=3.1415926)

      if (ichoice==1) then
         ! heat production 
         funccf = 2*pi*pi*sin(pi*x)*sin(pi*y)
      endif

      end function funccf

!     USEROUT: allows for user interaction and input/output with sepran
!     STRUCTURE block
      subroutine userout(kmesh,kprob,isol,isequence,numvec)
      implicit none
      integer kmesh(*),kprob(*)  ! mesh and problem definition
      integer isol(5,*) ! solution vectors (NB: can be more than 1)
                        ! in this example we have just one - temperature
      integer isequence ! sequence number specified when calling userout
      integer numvec ! number of solution vectors

      ! define interpolation grid 
      integer NPMAX,ncoor
      parameter(NPMAX=1)  ! make this as big as you need it for your
                          ! interpolation grid
      real*8 coor(2,NPMAX) ! 2D grid with x,y
      real*8 solint(NPMAX) ! storage area for 1D array (temperature)
      integer iinmap(5),map(5) ! ignore for now but useful when doing 
                               ! interpolations on many vectors (e.g.,
                               ! with time-dependence) on same grid

      if (isequence == 1) then  ! print 'hello'

         write(6,*) 'hello!'

      else if (isequence == 2) then  ! print 'hello again'

         write(6,*) 'hello again!'

      else if (isequence == 3) then   ! interpolate value in centerpoint and 
         
         ! centerpoint coordinates
         coor(1,1) = 0.5
         coor(2,1) = 0.5 
         ncoor = 1 ! number of coordinates
         if (ncoor > NPMAX) then
            write(6,*) 'PERROR(userout-3): ncoor>NPMAX'
            write(6,*) 'ncoor = ',ncoor
            write(6,*) 'NPMAX = ',NPMAX
            call instop ! note: not just 'stop'
         endif
         iinmap(1)=0
         call intcoor(kmesh,kprob,isol(1,1),solint,coor,1,ncoor,
     v                2,iinmap,map)           
         write(6,*) 'temperature in point ',coor(1,1),coor(2,1)
         write(6,*) 'is                   ',solint(1)
      endif

      end subroutine userout
      

