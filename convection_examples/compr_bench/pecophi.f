c *************************************************************
c *   PECOPHI
c *
c *   Form vector containing radiogenic heat production  
c *   and viscous dissipation. 
c *   iphi     i     Viscous dissipation (assuming eta=1)
c *   user     io    user array 
c *   iright   i     pointer in user to heatproduction vector 
c *   npoint   i     number of points in mesh
c *   DiRa     i     Product of dissipation number and Rayleigh number
c *   coor     i     coordinates
c *   ivisc    i     vector containing viscosity in nodal points
c *   iheat    i     vector containing heatproduction in nodal points
c *
c *   PvK 1997 
c *************************************************************
      subroutine pecophi(iphi,user,iright,npoint,DiRa,iqtype,
     v   coor,ivisc,iheat)
      implicit none
      integer iphi(5),iright,npoint,i,ivisc(5),iheat(5)
      real*8 user(*),DiRa,coor(2,*)
      integer ibuffr,iqtype
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      include 'powerlaw.inc'
      include 'c1visc.inc'
      include 'SPcommon/ctimen'
      include 'pecof900.inc'
      include 'ccc.inc'
      real*8 x,y,z,r,heatav,rho,funccf,factor,yp
      integer inew,ival,iiphi,iivisc,inidgt
      integer ipheat

      do i=1,npoint
         user(iright+i-1) = 0
      enddo

c     *** First copy radiogenic heatproduction
      if (iqtype.gt.0) then
        call ini050(iheat(1),'pecophi: iheat')
        ipheat = inidgt(iheat(1))
        if (compress) then
c          *** variable rho
           do i=1,npoint
             x=coor(1,i)
             y=coor(2,i)
             rho = funccf(3,x,y,z)
             user(iright+i-1) = rho*buffr(ipheat-1+i)
           enddo
        else 
           do i=1,npoint
             user(iright+i-1) = buffr(ipheat-1+i)
           enddo
        endif
      endif

c     *** Add viscous dissipation
      if (DiRa.ne.0) then
        call ini050(iphi(1),'pecophi: iphi')
        iiphi = inidgt(iphi(1))
        if (itypv.ne.0) then
           call ini050(ivisc(1),'pecophi: ivisc')
           iivisc = inidgt(ivisc(1))
        endif
        call pecoph2(buffr(iiphi),buffr(iivisc), 
     v               user(iright),npoint,DiRa)
      endif

      return
      end

c *************************************************************
c *   PECOPH2
c *************************************************************
      subroutine pecoph2(phi1,visc,phi2,npoint,DiRa)
      implicit none
      real*8 phi1(*),phi2(*),visc(*),DiRa
      integer npoint,i
      include 'c1visc.inc'
      real*8 vmin,vmax,phmin,phmax,qmin,qmax,ph2min,ph2max 
      real*8 pp

      vmin=1e37
      phmin=1e37
      qmin=1e37
      ph2min=0
      vmax=0
      phmax=0
      qmax=0
      ph2max=0
c     write(6,*) 'itypv: ',itypv,DiRa
      if (itypv.ne.0) then
         do i=1,npoint
            vmin = min(vmin,visc(i))
            vmax = max(vmax,visc(i))
            phmin = min(phmin,phi1(i))
            phmax = max(phmax,phi1(i))
            qmin = min(qmin,phi2(i))
            qmax = max(qmax,phi2(i))
c           pp = phi2(i)
            phi2(i) = visc(i)*phi1(i)*DiRa + phi2(i)
c           write(6,'(''phi: '',4f12.3)') pp,visc(i),phi1(i),phi2(i)
            ph2min = min(ph2min,phi2(i))
            ph2max = max(ph2max,phi2(i))
         enddo
      else
         do i=1,npoint
            vmin = min(vmin,visc(i))
            vmax = max(vmax,visc(i))
            phmin = min(phmin,phi1(i))
            phmax = max(phmax,phi1(i))
            qmin = min(qmin,phi2(i))
            qmax = max(qmax,phi2(i))
            pp = phi2(i)
            phi2(i) = phi1(i)*DiRa + phi2(i)
c           write(6,'(''phi: '',4f12.3)') pp,phi1(i),phi2(i)
            ph2min = min(ph2min,phi2(i))
            ph2max = max(ph2max,phi2(i))
         enddo
      endif
c     write(6,*) 'vmin/max: ',vmin,vmax
c     write(6,*) 'phmin/max: ',phmin,phmax
c     write(6,*) 'ph2min/max: ',ph2min,ph2max
c     write(6,*) 'qmin/max: ',qmin,qmax
   
      return 
      end

