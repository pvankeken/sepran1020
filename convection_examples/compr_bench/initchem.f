c *************************************************************
c *   INITCHEM
c *
c *   Create initial distribution of chemistry
c *   080302: restructure to do 4 cases with different
c *           depth of degassing zone in parallel
c *           For each case K we keep following information:
c *              chemmark(K-1+1,i)  : has it been degassed recently?
c *              chemmark(K-1+2,i)  : fraction of U,Th left
c *              chemmark(K-1+3,i)  : He4 concentration
c *              chemmark(K-1+4,i)  : fraction of He3 left
c *************************************************************
      subroutine initchem(chemmark,coortrac,kmesh,kprob,isol,
     v        nchem,ncontzone,NTRACMAX) 
      implicit none
      integer nchem,ncontzone
      real*8 chemmark(nchem,*),coortrac(2,*)
      integer kmesh(*),kprob(*),isol(*)
      integer nxtrac,nytrac,i,j,ip,NTRACMAX,ntot,k,kmax
      real*8 x,y,r

      include 'tracer.inc'
      include 'degas.inc'
      include 'powerlaw.inc'
      include 'ccc.inc'

      if (itracoption.eq.2) then
         write(6,*) 'PERROR(initchem): not suited for markerchain'
         write(6,*) 'method'
         call instop
      endif
      kmax=numdegas_depth

      U238  = U238_0
      U235  = U235_0
      Th232 = Th232_0
      K40   = K40_0
      He4   = 0
      Ar40  = 0
      do i=1,4
         He4loss(i)  = 0
         He3loss(i)  = 0
         U235loss(i) = 0
         U238loss(i) = 0
         Th232loss(i)= 0
         Ar40loss(i) = 0
         K40loss(i)  = 0
      enddo

      ntot = 0
      do idist=1,ndist
         ntot=ntot+ntrac(idist)
      enddo
      if (ntot.gt.NTRACMAX) then
         write(6,*) 'PERROR(initchem): ntot > NTRACMAX'
         write(6,*) 'ntot, NTRACMAX: ',ntot,NTRACMAX
         call instop
      endif
 
      do k=1,kmax
        ip=(k-1)*5
        do i=1,ntot
c           **** Start with primitive mantle: no He4, primitive He3, U,Th,K
c           *** Indication if it has been degassed recently
            chemmark(ip+1,i) = 0d0
c           *** Initial fraction of U,Th,K
            chemmark(ip+2,i) = 1d0
c           *** Initial concentration of He4
            chemmark(ip+3,i) = 0
c           *** Initial fraction of He3
            chemmark(ip+4,i) = 1
c           *** Initial concentration of Ar40
            chemmark(ip+5,i) = 0d0
        enddo
      enddo
        
      return
      end
