c *************************************************************
c *   VELINTMARK
c * 
c *   Find the velocity in marker xm,ym. output in u,v.
c *************************************************************
      subroutine velintmark(xm,ym,ikelmc,ikelmi,npoint,u,v,iel,
     v     nelem,user) 
      implicit none
      real*8 xm,ym,u,v,user(*)
      integer ikelmc,ikelmi,iel,k,nelem
      real*8 un(6),vn(6),xn(6),yn(6),shapef(6)
      integer nodno(6),icheckinelem,npoint
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      
      call pedetel(1,xm,ym,iel)
      call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,iel)
      if (icheckinelem(xn,yn,xm,ym).ne.1) then
         write(6,*) 'PERROR(velintmark): sper01 missed this one'
         write(6,*) 'xm, ym, iel: ',xm,ym,iel
         call instop
      endif

      call sper06(user(10),un,nodno)
      call sper06(user(10+npoint),vn,nodno)
      call detshape(xn,yn,xm,ym,shapef)
      u=0
      v=0
      do k=1,6
         u=u+shapef(k)*un(k)
         v=v+shapef(k)*vn(k)
      enddo

      return
      end
