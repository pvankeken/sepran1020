c *************************************************************
c *   PEFBUOY
c *
c *   For Cartesian geometry.
c * 
c *   *Purpose:
c *   *calculate the buoyancy force in the Stokes equation.
c *
c *   * input:
c *   * x,y          - cartesian coordinates of the evaluation point
c *   * temp       - temperature value in the evaluation point
c *
c *   NB: sepran multiplies this term with density. Set variable density
c *   through coefficient 7 in pefilcof.
c *
c *   * output: value of the buoyancy force component
c *************************************************************
      real*8 function pefbuoy(x,y,temp,pressure)
      implicit none
      integer ichois
      real*8 x,y,temp,pressure
      include 'depth_thermodyn.inc'
      include 'cpephase.inc'
      include 'pecof900.inc'
      real*8 aload,prespi,bigGamma,alpha_eff,funccf,z,rho
      real*8 temp_adiabatic,adiabat
      integer iph

 
c     *** thermal buoyancy force
      if (tbar) then
c        *** compute local value of adiabatic reference temperature
         temp_adiabatic = adiabat(y)
         aload =  Ra*(temp-temp_adiabatic)
      else
         aload = Ra*temp
      endif

c     *** Depth dependent expansivity; multiply only with thermal part!
      if (ialphatype.eq.1) then
         alpha_eff = funccf(5,x,y,z)
         aload = aload * alpha_eff
      endif

      if (.not.tala) then
c        *** add effect of pressure term; has opposite sign of Ra*T
         aload = aload - DiG*pressure
      endif
 
c     *** contribution of the phase changes
c     *** This needs careful reevaluation for compressible convection
*     do iph=1,nph
*        prespi = (1-y) - phz0(iph) - gamma(iph)*(temp-pht0(iph))
*        bigGamma = 0.5 + 0.5*tanh(prespi/phdz(iph))
*        aload = aload - Rb(iph)*bigGamma
*     enddo


      pefbuoy = aload

      return
      end
