c *************************************************************
c *    MOVECONT
c *
c *    Rotate the continent formation zone by 
c *************************************************************
       subroutine movecont(toutstep)
       implicit none
       
       real*8 toutstep
       real*8 pi
       parameter(pi=3.1415926 535898)
       include 'tloop.inc'
       include 'pecont.inc'
       real*8 dtheta
       integer i

       dtheta = 2*pi*toutstep/revol_cont
c      *** Move second continent only
       thmincont(2) = thmincont(2) + dtheta
       thmaxcont(2) = thmaxcont(2) + dtheta
c      *** make sure theta of the continent doesn't
c      *** exceed 2*pi. 
       if (thmaxcont(2).ge.2*pi) then
          if (revol_cont.lt.0) then
c            *** continent revolves in counter clockwise fashion
             write(6,*) 'PERROR(movecont): continent should move'
             write(6,*) '    to larger theta values'
             call instop
          endif                
          dtheta = thmaxcont(2)- thmincont(2)
          thmaxcont(2) = thmaxcont(2)+ 0.1 - 2*pi
          thmincont(2) = thmincont(2)+ 0.1 - 2*pi
       endif
       write(6,'(''Movecont '',i5,'' to: '',2f8.3)') 
     v       2,thmincont(2),thmaxcont(2)
 

       return 
       end
