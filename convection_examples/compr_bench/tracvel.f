      subroutine tracvel(kmesh1,kprob1,isol1,user,ichoice)
      implicit none
      integer kmesh1(*),kprob1(*),isol1(*),ichoice
      real*8 user(*)
      include 'ccc.inc'

      if (cyl) then
         call tracvelcyl(kmesh1,kprob1,isol1,user,ichoice)
      else 
         call tracvelcart(kmesh1,kprob1,isol1,user,ichoice)
      endif
      end

c *************************************************************
c *   TRACVEL
c *
c *   Determine velocity in markers. Adapted from t4detvel
c *
c *   PvK 073004 Modified to work with markerchain method (itracoption=2)
c *
c *
c *************************************************************
c *
c *   ichoice     i      
c *                     ichoice=1 : store answer in velmark
c *                     ichoice=2 : store answer in velnewm
c *
c *   PvK 950508/990407
c *   Adapted for use with cylindrical coordinates
c *
c *   PvK/AKM 051501
c *   Corrected for curved elements
c *   All elements are treated as straight element; if an element
c *   is curved a local transformation to a straight element is
c *   performed.
c *************************************************************
      subroutine tracvelcyl(kmesh1,kprob1,isol1,user,ichoice)
      implicit none
      integer kmesh1(*),kprob1(*),isol1(*),ichoice
      real*8 user(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (buffr(1),ibuffr(1))
      include 'petrac.inc'
      include 'tracer.inc'
      include 'c1mark.inc'
      include 'ccc.inc'

      integer ikelmc,ikelmi,npoint,ntot,i,j,k,iel,ikelmo
      real*8 xm,ym,xn(6),yn(6),shapef(6),un(6),vn(6),u,v,done
      integer nodno(6),nelem,iniget,inidgt,nmark
      logical edge
      integer nodlin(3),icorrect,isub,imissed,nelgrp
      real*8 r,th,rl(3)
      logical midflag,dotflag
      real*8 xi,eta
      integer ndist2,ntrac2

c     *** get nodal point and coordinate information from ibuffr()
      call ini050(kmesh1(23),'tracvelc: coordinates')
      call ini050(kmesh1(17),'tracvelc: nodal points')
c     *** kmesho contains the neighboring elements of each element;
c     *** we may use this in the future to improve on the 
c     *** tracer interpolation
      call ini050(kmesh1(29),'tracvelc: kmesh part o')
      npoint = kmesh1(8)
      nelem = kmesh1(9)
      if (periodic) nelem = kmesh1(kmesh1(21))
      nelgrp = kmesh1(5)
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
      ikelmo = iniget(kmesh1(29))

c     *** make velocity available in user()
      done = 1d0
      call pecopy(2,isol1,user,kmesh1,kprob1,10,done)
      call pecopy(3,isol1,user,kmesh1,kprob1,10+npoint,done)

c     *** find total number of markers
      if (itracoption.eq.2) then
         nmark=0
         do ichain=1,nochain
            nmark = nmark + imark(ichain)
         enddo
         ntot=nmark
      else
         ntot=0
         do idist=1,ndist
            ntot = ntot + ntrac(idist)
         enddo
      endif

      if (xi_eta_stored.ne.ichoice) then
c        *** tracer element info in /petrac/ is out of date
         call detelemtrac(ichoice,user,'tracvelc')
      endif
       
      do i=1,ntot
         iel = ielemmark(i) 
         xi  = xi_eta(1,i)
         eta = xi_eta(2,i)
         call sper01(ibuffr(ikelmc),ibuffr(ikelmi),nodno,xn,yn,iel)
         call sper06(user(10),un,nodno)
         call sper06(user(10+npoint),vn,nodno)
         call getshape6_xi_eta(xi,eta,shapef)
         u=0
         v=0
         do k=1,6
            u = u + shapef(k)*un(k)
            v = v + shapef(k)*vn(k)
         enddo
c        write(6,'(''xi,eta: '',2f10.3,i5,4f10.3)') 
c    v         xi,eta,iel,shapef(1),shapef(2),u,v
         if (ichoice.eq.1) then
            velmark(2*i-1) = u
            velmark(2*i  ) = v
         else
            velnewm(2*i-1) = u
            velnewm(2*i  ) = v
         endif
      enddo

      return
      end


c *************************************************************
c *   TRACVEL
c *
c *   Determine velocity in markers. Adapted from t4detvel
c *
c *   PvK 950508/990407
c *************************************************************
      subroutine tracvelcart(kmesh1,kprob1,isol1,user,ichoice)
      implicit none
      integer kmesh1(*),kprob1(*),isol1(*),ichoice
      real*8 user(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      include 'pexcyc.inc'
      include 'petrac.inc'
      include 'c1mark.inc'
      include 'tracer.inc'
      include 'ccc.inc'

      integer ikelmc,ikelmi,npoint,ntot,i,j,k,iel,icheckinelem
      real*8 xm,ym,xn(6),yn(6),shapef(6),un(6),vn(6),u,v,done
      integer nodno(6),nelem,iniget,inidgt,nmark


      call pefilxy(2,kmesh1,kprob1,isol1)

      call ini070(kmesh1(23))
      call ini070(kmesh1(17))
      npoint = kmesh1(8)
      nelem = kmesh1(9)
       if (periodic) nelem=kmesh1(kmesh1(21))
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))

c     *** Copy velocity components into array user
      done = 1d0
      call pecopy(2,isol1,user,kmesh1,kprob1,10,done)
      call pecopy(3,isol1,user,kmesh1,kprob1,10+npoint,done)

      if (itracoption.eq.2) then
         nmark=0
         do ichain=1,nochain
            nmark = nmark+imark(nochain)
         enddo
         ntot = nmark
      else 
         ntot = 0
         do idist=1,ndist
            ntot = ntot + ntrac(idist)
         enddo
      endif

      if (ichoice.eq.1) then
        do i=1,ntot
          xm = coormark(2*i-1)
          ym = coormark(2*i)
          call checkbounds_cart(xm,ym)
          call velintmark(xm,ym,ikelmc,ikelmi,npoint,u,v,iel,
     v                    nelem,user)
          ielemmark(i) = iel

          velmark(2*i-1) = u
          velmark(2*i  ) = v
 
        enddo
      else

        do i=1,ntot
          xm = coornewm(2*i-1)
          ym = coornewm(2*i)
          call velintmark(xm,ym,ikelmc,ikelmi,npoint,u,v,iel,
     v                    nelem,user)
          velnewm(2*i-1) = u
          velnewm(2*i  ) = v
 
         enddo
      endif

      return
      end
  
