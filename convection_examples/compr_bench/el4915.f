      subroutine el4915 ( s, phix, phi, xgauss, eta, w, swork, work,
     +                    mcont )
! ======================================================================
!
!        programmer    Guus Segal
!        version  5.6  date 17-08-2001 Allow mcont = 4
!        version  5.5  date 08-11-1998 Correction for Goertler Equations
!        version  5.4  date 28-09-1998 Extension for Goertler Equations
!                                      Ralf Biertuempfel
!        version  5.3  date 28-08-1998 include commons
!        version  5.2  date 23-12-1996 Layout
!        version  5.1  date 16-12-1994 New call to el3001
!        version  5.0  date 03-09-1994 Extra parameter mcont
!        version  4.0  date 12-08-1993 Extra parameters
!        version  3.0  date 16-07-1993 Complete revision
!        version  2.2  date 10-12-1992 save common; expl. decl.
!        version  2.1  date 22-08-1991 Zdenek: no dimension, cdc, ce
!        version  2.0  date 19-01-1987
!
!   copyright (c) 1987-2001  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     compute the viscosity part of the element matrix for navier stokes
!     and store in matrix s for 2d and 3-d elements
!     This subroutine does not store the swirl part
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/celint'
      include 'SPcommon/celp'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision phix(n,m,ndimlc), phi(n,m), eta(m), w(m),
     +                 s(ndimlc*n,ndimlc*n), swork(n,n), work(*),
     +                 xgauss(m,ndimlc)
      integer mcont

!     eta    i    Array containing the viscosity in the integration points
!     mcont   i    Defines the type of continuity equation
!                  Possible values:
!                  0: incompressible flow
!                  1: compressible flow: div (rho u ) = 0
!                  2: proper orthogonal decompostion for Goertler equations
!                  3: Special form of NS equations
!                  4: the continuity equation contains a pressure term.
!     phi     i    array of length 7 x m containing the values of the
!                  basis functions in the gauss points in the sequence:
!                         k
!                  phi (xg ) = phi (i,k)
!                     i
!                  array phi is only filled when ifirst = 0 and gauss
!                  integration is applied
!     phix    i    Array of length n x m x 3  containing the derivatives
!                  of the basis functions in the sequence:
!                  d phi / dx (xg ) = phix (i,k,1);
!                       i        k
!                  d phi / dy (xg ) = phix (i,k,2);
!                       i        k
!                  d phi / dz (xg ) = phix (i,k,3);
!                       i        k
!                  If the element is a linear tetrahedron (n=4), then the
!                  derivatives are constant and only k = 1 is filled.
!     s      i/o   Element matrix to be filled
!     swork        Work array in which a sub element matrix is stored
!     w       i    array of length m containing the weights for integration
!     work    i    Work array of length 2*m
!     xgauss  i    array of length m x ndimlc containing the co-ordinates of
!                  the gauss points.  array xgauss is only filled when gauss
!                  integration is applied.
!                  xg  = xgauss (i,1);  yg  = xgauss (i,2);  zg  = xgauss (i,3)
!                    i                    i                    i
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      logical symm
      integer jzeros, idimsv, k
      double precision dmult(3)

!     dmult   Array containing multiplication factors
!     idimsv  help variable to save idim
!     jzeros  help variable to save jzero
!     k       general loop variable
!     symm    Indication whether the resulting matrix is symmetrical
!             (true) or not (false)
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL3001  Compute integrals of the shape / u(x).grad phi phi (x) dx
!                                            e -            j   i
!     EL3002  Compute integrals of the shape / beta(x) phi (x) dx
!                                            e            i
!     EL3010  Fill small matrix elemmt into larger matrix s
!     EL3011  Compute contribution to element stiffness matrix corresponding to
!             a part of the second derivative part of an ellipticl equation
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
!   -
! **********************************************************************
!
!                       PSEUDO CODE
!
!     Computation of the stiffness matrix results in a 3x3 block matrix, each
!     consisting of nxn matrices of the following shape:
!            |   S       S      S     |
!            |    uu      uv     uw   |
!            |                        |
!            |   S       S      S     |
!     S =    |    vu      vv     vw   |
!            |                        |
!            |   S       S      S     |
!            |    wu      wv     ww   |
!     The submatrices are defined by:
!              T
!     S    =  S
!      xy      yx
!     Cartesian co-ordinates:
!     S uu =  2  eta phix phix + eta phiy phiy + eta phiz phiz
!     S uv =     eta phix(j) phiy(i)
!     S uw =     eta phix(j) phiz(i)
!     S vu =     eta phiy(j) phix(i)
!     S vv =  2  eta phiy phiy + eta phix phix + eta phiz phiz
!     S vw =     eta phiy(j) phiz(i)
!     S wu =     eta phiz(j) phix(i)
!     S wv =     eta phiz(j) phiy(i)
!     S ww =  2  eta phiz phiz + eta phix phix + eta phiy phiy
!     2D Cylinder co-ordinates (r, z) without swirl
!     S uu =  2 eta phir phir + eta phiz phiz + 2 eta phi phi /r**2
!     S uv =     eta phir(j) phiz(i)
!     S vu =     eta phiz(j) phir(i)
!     S vv =  2  eta phiz phiz + eta phir phir
!     2D Cylinder co-ordinates (r, z) with swirl
!     See without swirl, however with extra term:
!     S ww = eta ( psiz psiz + (psir(j) - psi(j)/r) (psir(i)-psi(i)/r) )
!     2D Polar co-ordinates (r, z)   (in this case z is actually the angle)
!     S uu = eta ( 2 phir phir + phiz phiz/r**2 + 2 phi phi /r**2 )
!     S uv =     eta (phir(j) phiz(i)/r + 2 phiz(j) phi(i)/r**2 -
!                      phi(j) phiz(i)/r**2)
!     S vu =     eta (phiz(j)/r phir(i) + 2 phi(j) phiz(i)/r**2 -
!                      phiz(j) phi(i)/r**2)
!     S vv =  eta ( 2 phiz phiz/r**2 + phir phir + phi phi /r**2 -
!                    phi(i) phir(j)/r - phi(j) phir(i)/r )
!    Goertler equations:
!     S uu =   eta phix phix  +    eta phiy phiy  +    eta phiz phiz
!     S vv =   eta phix phix  +  2 eta phiy phiy  +    eta phiz phiz
!     S ww =   eta phix phix  +    eta phiy phiy  +  2 eta phiz phiz
!     The non-diagonal block matrices are the same as in the 3D cartesian.
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
!     --- Compute matrices eta phix phix, eta phiy phiy and eta phiz phiz
!         and store in Suu, Svv and Sww

      if ( jcart.eq.1 .and. ndimlc.eq.2 ) then

!     --- 2D Cartesian co-ordinates

         if ( mcont.eq.1 .or. mcont.eq.4 ) then

!        --- compressible flow

            dmult(1) = 4d0/3d0
            dmult(2) = 1d0
            dmult(3) = 1d0/3d0

         else

!        --- Pure incompressible flow, apply divergence freedom

            dmult(1) = 2d0
            dmult(2) = 1d0
            dmult(3) = 1d0

         end if

!        --- eta phix phix

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         call el3010 ( s, swork, 2*n, n, 1, 1, 2, .false., dmult(1) )
         call el3010 ( s, swork, 2*n, n, 2, 2, 2, .false., dmult(2) )

!        --- eta phiy phiy

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, 2*n, n, 1, 1, 2, .false., dmult(2) )
         call el3010 ( s, swork, 2*n, n, 2, 2, 2, .false., dmult(1) )

!        --- S uv =  eta phix phiy

         jadd = 0
         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                 symm )

         call el3010 ( s, swork, 2*n, n, 2, 1, 2, .true., dmult(3) )
         call el3010 ( s, swork, 2*n, n, 1, 2, 2, .false., dmult(3) )

      else if ( jcart.eq.1 ) then

!     --- 3D Cartesian co-ordinates
!         eta phix phix

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         if (mcont.eq.2) then

!        --- modified Navier-Stokes equation for Goertler problem

            call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )

         else

!        --- standard Navier-Stokes equations

            call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 2d0 )
            call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., 1d0 )
            call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., 1d0 )

         end if

!        --- eta phiy phiy

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )
         if (mcont.eq.2) jadd=0
         call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., 2d0 )
         call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., 1d0 )

!        --- eta phiz phiz

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 3, 3, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )
         call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., 1d0 )
         call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., 2d0 )

!        --- S uv =  eta phix phiy

         jadd = 0
         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                 symm )

         call el3010 ( s, swork, 3*n, n, 2, 1, 3, .true., 1d0 )
         call el3010 ( s, swork, 3*n, n, 1, 2, 3, .false., 1d0 )

!        --- S uw =  eta phix phiz

         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 1, 3, work,
     +                 symm )

         call el3010 ( s, swork, 3*n, n, 3, 1, 3, .true., 1d0 )
         call el3010 ( s, swork, 3*n, n, 1, 3, 3, .false., 1d0 )

!        --- S vw =  eta phiy phiz

         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 2, 3, work,
     +                 symm )

         call el3010 ( s, swork, 3*n, n, 3, 2, 3, .true., 1d0 )
         call el3010 ( s, swork, 3*n, n, 2, 3, 3, .false., 1d0 )

      else if ( jcart.eq.2 .and. ndimlc.eq.2 .or. jcart.eq.4 ) then

!     --- 2D Cylinder co-ordinates
!         eta phir phir

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- 2 eta phi phi / r**2

         do k = 1, m
            work(k) = 2d0*eta(k)/xgauss(k,1)**2
         end do
         jadd = 0
         jzeros = jzero
         jzero  = 2
         call el3002 ( work, swork, w, phi, m, n, n, work(m+1) )
         jzero  = jzeros

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )

!        --- eta phiz phiz

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 2d0 )

!        --- S uv =  eta phir phiz

         jadd = 0
         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., 1d0 )

      else

!     --- 2D polar co-ordinates
!         eta phir phir

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- eta phi phi / r**2

         do k = 1, m
            work(k) = eta(k)/xgauss(k,1)**2
         end do
         jadd = 0
         jzeros = jzero
         jzero  = 2
         call el3002 ( work, swork, w, phi, m, n, n, work(m+1) )
         jzero  = jzeros

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- eta phiz phiz / r**2

         do k = 1, m
            work(k) = eta(k)/xgauss(k,1)**2
         end do
         jadd = 0
         symm = .true.
         call el3011 ( work, swork, w, phix, m, n, 2, 2, work(m+1),
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 2d0 )

!        --- eta ( - phi(i) phir(j) - phi(j) phir(i) )/ r

         do k = 1, m
            work(k) = -eta(k)/xgauss(k,1)
         end do
         jadd = 0
         idimsv = idim
         idim   = 1
         call el3001 ( work, swork, w, phi, phix, 1, n, work(m+1))

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .true., 1d0 )

!        --- S uv
!            eta phir phiz/r

         do k = 1, m
            work(k) = -work(k)
         end do
         jadd = 0
         symm = .false.
         call el3011 ( work, swork, w, phix, m, n, 1, 2, work(m+1),
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., 1d0 )

!        --- 2 eta phi(i) phiz(j)/r**2

         do k = 1, m
            work(k) = work(k)/xgauss(k,1)
         end do
         jadd = 0
         call el3001 ( work, swork, w, phi, phix(1,1,2), 1, n,
     +                 work(m+1) )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .true., 2d0 )

!        --- eta phi(j) phiz(i)/r**2

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., -1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., -1d0)

         idim = idimsv

      end if

      end
