      integer ichtime
      real*8 wavenumber
      integer metupw,intrule,icoorsystem,iqtype
      common /pecof800/ wavenumber,ichtime,metupw,intrule,
     v    icoorsystem,iqtype,qwithrho,qnondim,Nu_with_Tbar
      logical qwithrho,qnondim,Nu_with_Tbar
      real*8 q_layer,totdens,q_layer_d
      common /peheatprod/ q_layer(10),totdens,q_layer_d(10)
      real*8 heat_flux_in
      integer numnat800
      common /heatfluxbc/ heat_flux_in,numnat800
      
