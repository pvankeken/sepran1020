c *************************************************************
c *   DETELEMTRAC
c *   Determine the element number for each tracer
c *
c *   PvK 021304
c *************************************************************
      subroutine detelemtrac(ic,user,calling_routine)
      implicit none
     
      integer ic
      real*8 user(*)
      character*(*) calling_routine
      
      include 'mysepar.inc'
      include 'petrac.inc'
      include 'tracer.inc'
      include 'cbuoy_trac.inc'
      include 'elem_topo.inc'
      include 'c1mark.inc'
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      real*4 t00,t01,second
      integer npoint,nelem,nelgrp,ikelmc,ikelmi,ikelmo,j,ntot,iel
      integer nodno(6),nodlin(3),inidgt,iniget,imissed,i,isub,ifound
      real*8 xm,ym,xn(6),yn(6),un(6),vn(6),xi,eta
      real*8 rl(3),phiq(6),u,du,func,zm,dutot,dumin,dumax
      integer inaccurate,nmark
      logical guess_first,correct,first
      data first/ .true./
      

      t00 = second()
      call ini050(kmesh1(23),'detelemtrac: coordinates')
      call ini050(kmesh1(17),'detelemtrac: nodalpoints')
      call ini050(kmesh1(29),'detelemtrac: kmesh part o')
      npoint = kmesh1(8)
      nelem  = kmesh1(9)
      nelgrp = kmesh1(5)
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
      ikelmo = iniget(kmesh1(29))

      if (itracoption.eq.1) then
         ntot = 0
         do idist=1,ndist
            ntot=ntot+ntrac(idist)
         enddo
      else if (itracoption.eq.2) then
         nmark = 0
         do ichain=1,nochain
            nmark = nmark + imark(ichain)
         enddo
         ntot = nmark
      endif
      imissed=0
      ifound=0

      if (ic.eq.1.or.ic.eq.2) then 
c        *** Get element number for each tracer stored in coormark
         guess_first=.true.
         if (first) then 
            guess_first=.false.
            first=.false.
         endif
c        *** loop over tracers
         do i=1,ntot
            if (ic.eq.1) then
               xm = coormark(2*i-1)
               ym = coormark(2*i)
            else 
               xm = coornewm(2*i-1)
               ym = coornewm(2*i)
            endif
c           *** try previously determined element first
            iel = ielemmark(i)
            call pedeteltrac(0,xm,ym,ikelmc,ikelmi,ikelmo,nodno,
     v          nodlin,rl,xn,yn,un,vn,user,iel,npoint,nelem,imissed,
     v          ifound,nelgrp,phiq,xi,eta,guess_first,'detelemtrac')
            ielemmark(i)=iel
            xi_eta(1,i)=xi
            xi_eta(2,i)=eta
         enddo
         xi_eta_stored = ic
         if (ic.eq.1) then 
c         write(6,*) 'PINFO(detelemtrac): updated elements for coormark'
c    v          ,' for subroutine ',
c    v            calling_routine(1:lnblnk(calling_routine))
         else
c         write(6,*) 'PINFO(detelemtrac): updated elements for coornewm'
c    v          ,' for subroutine ',
c    v            calling_routine(1:lnblnk(calling_routine))
         endif
c        write(6,*) 'PINFO(detelemtrac): ifound/imissed/nn/ntot: ', 
c    v       ifound,imissed,ntot-ifound-imissed,ntot
         t01=second()
c        write(6,*) 'cpu(detelemtrac): ',t01-t00

      else if (ic.eq.10) then

c        *** test the element determination using func(10) as analytical
c        *** function; the nodal point values should have been stored
c        *** in user(10) before calling this routine
         dutot=0
         dumin=1
         dumax=0
         do i=1,ntot
            xm = coormark(2*i-1)
            ym = coormark(2*i) 
            iel = ielemmark(i)
            xi = xi_eta(1,i)
            eta = xi_eta(2,i)
            call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,iel)
            call getshape6_xi_eta(xi,eta,phiq)
            do j=1,6
               un(j) = func(10,xn(j),yn(j),zm)
            enddo
            u = 0
            do j=1,6
               u = u+un(j)*phiq(j)
            enddo
            du = abs(u - func(10,xm,ym,zm))
            dumax = max(dumax,du)
            dumin = min(dumin,du)
            dutot = du+dutot
            if (du.gt.1e-5) then
               inaccurate = inaccurate+1
                 write(6,*) 'PWARN(detelemtrac): ',
     v            'interpolation is inaccurate ',i,u,func(10,xm,ym,zm),
     v              (phiq(j),j=1,6),xi,eta,
     v            phiq(1)+phiq(2)+phiq(3)+phiq(4)+phiq(5)+phiq(6)
            endif
          enddo
       write(6,*) 'PINFO(detelemtrac) Av/min/max error in interpolation'
     v             ,' using stored xi,eta: ',dutot/ntot,dumin,dumax,
     v              inaccurate

      endif


      return
      end
          
