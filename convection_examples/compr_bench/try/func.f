c *************************************************************
c *   FUNC
c *
c *   PvK 070900
c *************************************************************
      real*8 function func(ichoice,x,y,z)
      implicit none
      integer ichoice
      real*8 x,y,z

      real*8 rerf
      real*8 z_here
      include 'coolcore.inc'
      include 'cjarvis.inc'
      integer iday,imonth,iyear,iarray(3)
      real*4 rand
      integer ifirst

      real*8 ampini,argsin,theta,r,dr,funccf,sint
      real*8 xp,yp,vx,vy
      real*8 pi,psimax,delta,rlamtemp,pert
      parameter(pi=3.1415926 5358979323846,psimax=250d0/(pi*pi))
      real*8 c1,c2,p
      real*8 xr,wpi,zz,ah,rara,u0,v0,qq
      real*8 tu,tl,tr,ts,trarg,tsarg
      real*8 theta0,cost0,sint0,sl
      parameter(theta0=pi*0.25,cost0=0.707106781d0,sint0=cost0)
      integer ntheta
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'pecof800.inc'
      save ifirst
      data ifirst/0/


      if (.not.cyl) then

c *************************************************************
c *     CARTESIAN BOX GEOMETRY
c *************************************************************
        if (ichoice.eq.1) then

c          *** thermal perturbation
           func = 0.2*avT_cond(1)*cos(wavenumber*pi*x)*sin(pi*y)

        else if (ichoice.eq.2) then
 
c          *** linear profile with perturbation
           func = 1-y + 0.1*avT_cond(1)*cos(pi*x)*sin(pi*y)

        else if (ichoice.eq.3) then

c          *** density
           func = funccf(3,x,y,z)

        else if (ichoice.eq.4) then

c          *** horizontal velocity from Appendix C, Van Keken et al., 1997
           func = psimax * sin (pi*x) * cos (pi*y)

        else if (ichoice.eq.5) then

c          *** vertical velocity from Appendix C, Van Keken et al., 1997
           func = - psimax * cos (pi*x) * sin (pi*y)

        else if (ichoice.eq.10) then

           func = x*x + y*y
           return

        else if (ichoice.eq.100) then

c          *** temperature field for benchmark 2 from Van Keken et al., 1997
           xr = x
           if (xr.le.0.00001) xr=0.00001
           if (xr.ge.1.99999) xr=1.99999
           wpi = sqrt(pi)
           zz   = y
           c1=1d0
           c2=2d0
           p=1d0
           ah=2.
           rara=3e5
           u0 = c1*(ah**(7./3.))*((rara*0.5/wpi)**(2./3.))
           u0 = u0 / (( 1.+ah**4.)**(2./3.))
           v0 = c2*u0/ah
           qq  = 2*(ah/(pi*u0))**0.5
           trarg = -    xr  *   xr  *v0/(4d0*( zz+p ))
           tsarg = - (ah-xr)*(ah-xr)*v0/(4d0*(1d0-zz+p))
           ts = 0.5 - 0.5*qq/wpi*sqrt(v0/(1-zz+p))*exp(tsarg)
           tr = 0.5 + 0.5*qq/wpi*sqrt(v0/( zz+p ))*exp(trarg)
           tu =       0.5*rerf(0.5*(1-zz)*(u0/   xr  )**0.5)
           tl =   1 - 0.5*rerf(0.5*  zz  *(u0/(ah-xr))**0.5)
           func = tu+tl+tr+ts - 1.5
           if (func.gt.1d0) func=1d0
           if (func.lt.0d0) func=0d0
           if (y.eq.0) func=1d0
           if (y.eq.1) func=0d0

        else

           write(6,*) 'PERROR(func) unknown option '
           write(6,*) 'ichoice = ',ichoice
           return

        endif

      else
        if (ichoice.eq.1) then
c          *** thermal perturbation
           ntheta=2
           r = sqrt(x*x+y*y)
           dr = (r-R1)/(R2-R1)
           theta = asin(y/r)
           argsin = pi*(1-dr)
           func = 0.10*sin(argsin)*cos(theta*ntheta)

        else if (ichoice.eq.2) then
 
c          *** linear profile with perturbation
           r = sqrt(x*x+y*y)
           dr = (r-R1)/(R2-R1)
           theta = asin(y/r)
           argsin = pi*(1-dr)
           func = 1-dr + 0.10*sin(argsin)*cos(theta*ntheta)
  
        else if (ichoice.eq.3) then

c          *** density 
           func = funccf(3,x,y,z)

        else if (ichoice.eq.4) then

c        *** horizontal velocity from Appendix C of Van Keken et al., JGR, 1997
         xp = x - 0.4192
         yp = y + 0.5
         func = psimax * sin (pi*xp) * cos (pi*yp)
         if (quart) then
c           *** rotate velocity field 45 degrees clockwise
            xp =  sint0*x + cost0*y
            yp = -cost0*x + sint0*y
            xp = xp - 0.4192
            yp = yp  + 0.5
            vx = psimax * sin (pi*xp) * cos (pi*yp)
            vy = - psimax * cos (pi*xp) * sin (pi*yp)
            func = vx*sint0-vy*cost0
         endif

      else if (ichoice.eq.5) then

c        *** vertical velocity from Appendix C of Van Keken et al., JGR, 1997
         xp = x - 0.4192
         yp = y + 0.5
         func = - psimax * cos (pi*xp) * sin (pi*yp)
         if (quart) then
c           *** rotate velocity field 45 degrees clockwise
            xp =  sint0*x + cost0*y
            yp = -cost0*x + sint0*y
            xp = xp - 0.4192
            yp = yp  + 0.5
            vx = psimax * sin (pi*xp) * cos (pi*yp)
            vy = - psimax * cos (pi*xp) * sin (pi*yp)
            func = vx*cost0 + vy*sint0
         endif

      else if (ichoice.eq.10) then
 
c        *** quadratic function to test interpolation accuracy
         func = x*x + y*y
         return
 
      
      else if (ichoice.eq.11) then

c        **** Error function profile. Halfspace cooled for
c        **** 100 Myr. T(z) = Tm*erf(y/(2*sqrt(kappa*t)).
c        **** for t=100 Myr, 2*sqrt(kappa*t)=112 km. 2800/112=25


         r = sqrt(x*x+y*y)
         dr = (r-R1)/(R2-R1)
         theta = pi/2.-asin(y/r)

         delta = 130d0/2885d0 
   

         func = 1-rerf(dr/delta)
         rlamtemp = pi/8.
         ampini=0.5

         pert=0.
         if(theta.le.pi/16.) then
           pert = ampini*cos(pi*theta/rlamtemp)
     v          *max(0d0,1-0.5*dr/delta)
         end if
         func = min(1d0,func + pert)

        else if (ichoice.eq.100) then

c         **** initial temperature condition modified from Appendix A of Van Keken et al., JGR, 1997
          r = sqrt(x*x+y*y)
          theta = asin(y/r)
          ah = 1.570796327
          rara  = 3d5
          wpi   = sqrt(pi)
          zz   = r-r1
          xr = theta
          c1 = 1d0
          c2 = 1.570796327
          p  = 1d0
c         write(6,*) 'func: ',xr,zz
          if (xr.le.0.00001) xr=0.00001
          if (xr.ge.1.570796326) xr=1.570796326
          u0 = c1*(ah**(7d0/3d0))*((rara*0.5/wpi)**(2d0/3d0))
          u0 = u0 / (( 1d0+ah**4d0)**(2d0/3d0))
          v0 = c2*u0/ah
          qq  = 2d0*(ah/(pi*u0))**0.5d0
          trarg = -    xr  *   xr  *v0/(4d0*( zz+p ))
          tsarg = - (ah-xr)*(ah-xr)*v0/(4d0*(1d0-zz+p))
          ts = 0.5 - 0.5*qq/wpi*sqrt(v0/(1-zz+p))*exp(tsarg)
          tr = 0.5 + 0.5*qq/wpi*sqrt(v0/( zz+p ))*exp(trarg)
          tu =       0.5*rerf(0.5*(1-zz)*(u0/   xr  )**0.5)
          tl =   1 - 0.5*rerf(0.5*  zz  *(u0/(ah-xr))**0.5)
          func = tu+tl+tr+ts - 1.5
          if (func.gt.1d0) func=1d0
          if (func.lt.0d0) func=0d0
          if (zz.eq.0.) func=1d0
          if (zz.eq.1.) func=0d0
c         write(6,*) 'func: ',xr,zz,func

       else

         write(6,*) 'PERROR(func): unknown option: ',ichoice
         call instop

       endif

      endif



  
      return
      end

c     *** Compute error function (from Numerical Recipes)
      real*8 function rerf(x)
      implicit none
      real*8 x
      integer iret
      real*8 delx,x0,x1,xx

      if (x.gt.4.) then
            rerf=1.
            goto 20
      endif
         iret=0
         rerf=0.
         delx=0.0001
         x0=0.
10       x1=x0+delx
         if(x1.gt.x) then
            x1=x
            iret=1
         endif
         xx=(x0+x1)/2.
         rerf=rerf+dexp(-xx*xx)*(x1-x0)
         if(iret.eq.1) then
            rerf=rerf*2./dsqrt(3.1415926535d0)
            goto 20
         endif
         x0=x1
         goto 10
c
20     return
         end






