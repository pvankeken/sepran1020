c *************************************************************
c *   PHIFROMGRAD
c * 
c *   Compute viscous dissipation from gradients of velocity
c *   The viscous dissipation needs to be multiplied by viscosity
c *   outside this subroutine
c *   
c *   Replacement of phifromsepran which may be inaccurate in case
c *   of compressible convection
c *
c *   PvK 082708
c *************************************************************
       subroutine phifromgrad(kmesh1,kprob1,isol1,iphi)
       implicit none
       integer kmesh1(*),kprob1(*),isol1(*),iphi(*)
       integer igradv(5),idivv(5),istress(5),iuser(100)
       integer iinder(200)
       real*8 anorm,volint,eps,pressure,user(100)
       real*8 gradv11,gradv12,gradv22,div11,stress11,stress12,stress22
       integer ihelp,nparm,iwork(4,100),i
       real*8 work(100)
       include 'pecof900.inc'   
       include 'solutionmethod.inc'
    
       iuser(1)=100
        user(1)=100d0
       iinder(1)=8
       iinder(2)=1
       iinder(3)=0
       iinder(5)=0
       iinder(6)=0
       iinder(7)=0
       iinder(8)=2
        iuser(6)=7
c     ** itime, modelv, intrule, icoor, mcontv
        iuser(2)=1
        iuser(7)=0
        iuser(8)=1
        iuser(9)=intrule900
       iuser(10)=icoor900
       iuser(11)=mcontv
c      *** eps, rho
       iuser(12)=-6
       if (compress) then
          iuser(13)=3
       else
          iuser(13)=-7
       endif
c      *** omega,f1,f2,f3,eta
       iuser(14)=0
       iuser(15)=0
       iuser(16)=0
       iuser(17)=0 
       iuser(18)=-7
       if (isolmethod.eq.0) then
          user(6)=1d-6
       else
          user(6)=0d0
       endif
       user(7)=1d0


c      write(6,*) 'compute grad v'
c      *** icheld = 2 for grad v
       iinder(4)=2
       call deriv(iinder,igradv,kmesh1,kprob1,isol1,iuser,user)

       do i=1,100
          iwork(1,i)=0
          iwork(2,i)=0
          iwork(3,i)=0
          iwork(4,i)=0
           work(i) = 0d0
       enddo
       iwork(1,1) = 0
       iwork(1,2) = 1
       iwork(1,3) = intrule900 
       iwork(1,4) = icoor900
       iwork(1,5) = mcontv
       if (isolmethod.eq.0) then
c         *** penaltyfunction parameter
          eps = 1d-6
       else
          eps = 0
       endif
c      *** 6: epsilon
       iwork(1,6) = 0
        work(6) = eps
c      *** 7: rho
       if (compress) then
          iwork(1,7) = 3
       else 
          iwork(1,7) = 0
           work(7)   = 1d0
       endif
c      *** 12: eta assumed constant 1 here
       iwork(1,12) = 0
        work(12)   = 1d0

       do i=6,iuser(1)
          iuser(i)=0
       enddo
       do i=6,user(1)
          user(i)=0d0
       enddo
       nparm=12
       call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1)

       call phifromgrad001(kmesh1,kprob1,isol1,igradv,iphi)

       return
       end

       subroutine phifromgrad001(kmesh,kprob,isol,igradv,iphi)
       implicit none
       integer kmesh(*),kprob(*),isol(*),igradv(*),iphi(*)
       integer ibuffr(1)
       common ibuffr
       real*8 buffr(1)
       equivalence(ibuffr(1),buffr(1))
       integer iu1(2)
       real*8 u1(2)
 
       integer iniget,inidgt,ipcoor,ipdiv,ipgrad,ipphi,i
       integer ndef_div,ndef_grad,ndef_phi,npoint,ippres
       integer indprf,nunkp,ipkprf

       
       if (igradv(2).ne.115) then
          write(6,*) 'PERROR(phifromgrad001): igradv(2) <> 115'
          write(6,*) 'igradv: ',(igradv(i),i=1,5)
          call instop
       endif
       if (iphi(2).ne.115) then
          iu1(1)=0
           u1(1)=0d0
          call creavc(0,2,1,iphi,kmesh,kprob,iu1,u1,iu1,u1)         
c         write(6,*) 'PERROR(phifromgrad001): iphi(2) <> 115'
c         write(6,*) 'iphi: ',(iphi(i),i=1,5)
c         call instop
       endif

       call ini070(igradv(1))
       call ini070(iphi(1))
       call ini070(kmesh(23))
       call ini070(isol(1))
       indprf = kprob(19)
       if (indprf.ne.0) then
          call ini050(indprf,'phifromgrad001: kprobf')
          ipkprf = iniget(indprf)
       endif
       npoint = kmesh(8)
       ipcoor = inidgt(kmesh(23))
       ipgrad = inidgt(igradv(1))
       ipphi = inidgt(iphi(1))
       ippres = inidgt(isol(1))
       ndef_grad = igradv(5)/npoint
       nunkp = kprob(4)
c      write(6,*) 'ndef: ',npoint,ndef_div,ndef_grad

       call phifromgrad002(buffr(ipcoor),npoint,
     v          buffr(ipgrad),ndef_grad,buffr(ipphi),
     v          buffr(ippres),nunkp,ibuffr(ipkprf),indprf)
       return
       end

       subroutine phifromgrad002(coor,npoint,grad,ngrad,
     v              phi,uvp,nunkp,kprobf,indprf)
       implicit none
       integer npoint,ndiv,ngrad,nphi,nunkp,indprf,kprobf(*)
       real*8 coor(2,npoint),grad(ngrad,npoint)
       real*8 phi(npoint)
       real*8 uvp(*)
       integer i,j1
       include 'dimensional.inc'
       include 'cpephase.inc'
       include 'pecof900.inc'
       real*8 stress11,stress12,stress21,stress22,divu
       
c      write(6,*) 'phigrad: ',indprf,kprobf(1),kprobf(2)
       if (indprf.ne.0) then
c         *** number of dofs per point is not constant
         do i=1,npoint
c           *** location of upward velocity component
            j1 = kprobf(i)+2
            if (divufromeos) then
              divu = -DiG*uvp(j1)
              stress11 = 2*grad(1,i) - 2d0/3d0*divu
              stress22 = 2*grad(4,i) - 2d0/3d0*divu
              stress12 = grad(2,i)+grad(3,i)
            else
              stress11 = 4d0/3d0*grad(1,i) - 2d0/3d0*grad(4,i)
              stress22 = 4d0/3d0*grad(4,i) - 2d0/3d0*grad(1,i)
              stress12 = grad(2,i) + grad(3,i)
            endif
            phi(i) = stress11*grad(1,i)+stress22*grad(4,i) +
     v                 stress12*stress12
c           write(6,*) 'phi: ',coor(1,i),coor(2,i),phi(i)
        enddo
      else 
c       *** number of dofs per point is constant 
        do i=1,npoint
c           *** location of upward velocity component
            j1 = (i-1)*nunkp+2
            if (divufromeos) then
              divu = DiG*uvp(j1)
              stress11 = 2*grad(1,i) - 2d0/3d0*divu
              stress22 = 2*grad(4,i) - 2d0/3d0*divu
              stress12 = grad(2,i)+grad(3,i)
            else
              stress11 = 4d0/3d0*grad(1,i) - 2d0/3d0*grad(4,i)
              stress22 = 4d0/3d0*grad(4,i) - 2d0/3d0*grad(1,i)
              stress12 = grad(2,i) + grad(3,i)
            endif
              phi(i) = stress11*grad(1,i)+stress22*grad(4,i) +
     v                 stress12*stress12
c             write(6,*) 'phi: ',coor(1,i),coor(2,i),phi(i)
        enddo
      endif
 
      return
      end 

       subroutine phifromgrad003(coor,npoint,grad,ngrad,
     v              phi,uvp,nunkp,kprobf,indprf)
       implicit none
       integer npoint,ndiv,ngrad,nphi,nunkp,indprf,kprobf(*)
       real*8 coor(2,npoint),grad(ngrad,npoint)
       real*8 phi(npoint)
       real*8 uvp(*)
       integer i,j1
       include 'dimensional.inc'
       include 'cpephase.inc'
       real*8 stress11,stress12,stress21,stress22
       
       if (indprf.ne.0) then
         do i=1,npoint
c         *** location of upward velocity component
          j1 = kprobf(i)+2
c         stress11 = 4d0/3d0*grad(1,i) - 2d0/3d0*grad(4,i)
c         stress22 = 4d0/3d0*grad(4,i) - 2d0/3d0*grad(1,i)
          stress11 = 2*grad(1,i) - 2d0/3d0*DiG*uvp(j1)
          stress22 = 2*grad(4,i) - 2d0/3d0*DiG*uvp(j1)
          stress12 = grad(2,i) + grad(3,i)
          phi(i) = stress11*grad(1,i)+stress22*grad(4,i) +
     v             stress12*stress12
c         write(6,*) 'phi: ',coor(1,i),coor(2,i),phi(i)
        enddo
      else 
         do i=1,npoint
c         *** location of upward velocity component
          j1 = 3*(i-1)+2
c         stress11 = 4d0/3d0*grad(1,i) - 2d0/3d0*grad(4,i)
c         stress22 = 4d0/3d0*grad(4,i) - 2d0/3d0*grad(1,i)
          stress11 = 2*grad(1,i) - 2d0/3d0*DiG*uvp(j1)
          stress22 = 2*grad(4,i) - 2d0/3d0*DiG*uvp(j1)
          stress12 = grad(2,i) + grad(3,i)
          phi(i) = stress11*grad(1,i)+stress22*grad(4,i) +
     v             stress12*stress12
c         write(6,*) 'phi: ',coor(1,i),coor(2,i),phi(i)
        enddo
      endif
 
      return
      end 
