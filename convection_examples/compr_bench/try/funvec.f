c *************************************************************
c *   FUNCVEC
c *
c *   PvK 082808
c *************************************************************
      subroutine funvec(rinvec,reavec,nvec,coor,outvec)
      implicit none
      real*8 rinvec(*),reavec(*),coor(*),outvec(*)
      integer nvec
      include 'cpephase.inc'
      real*8 y,adiabat,T0,y1,Temp
      integer ioption

      ioption = nint(rinvec(1))
      if (ioption.eq.2) then
         T0 = rinvec(2)
         if (Di.le.0d0.or.T0.le.0) then
            write(6,*) 'PERROR(funvec): Di <= 0 or T0<=0'
            call instop
         endif
         y1 = coor(2)
         Temp = reavec(1)
         outvec(1) = log( (Temp+T0)/T0 ) - (1-y1)*Di
      endif

      y = coor(2)
      if (iadiabat.eq.2) then
         outvec(1) = reavec(1)+adiabat(y)-adiabat(1d0)
      else if (iadiabat.eq.3) then
         outvec(1) = reavec(1)+adiabat(y)
      endif

      end

