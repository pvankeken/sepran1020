c *************************************************************
c *   PHIFROMSEPRAN
c *
c *   Compute viscous dissipation in the nodal points. Store as
c *   vector of special structure in ivisdip
c *
c *   In this version we assume eta=1 so that you have to
c *   multiply the  computed values with the local viscosity
c *   outside of this subroutine.
c *
c *   PvK 2001
c *************************************************************
      subroutine phifromsepran(kmesh1,kprob1,isol1,ivisdip)
      implicit none 
      integer kmesh1(*),kprob1(*), isol1(*),ivisdip(*)
      integer iinder(20),iuser(100)
      real*8 phiav,user(100)
      integer ielhlp
      include 'pecof900.inc'
      include 'solutionmethod.inc'

      write(6,*) 'PERROR: do not use phifromsepran'
      call instop

      iuser(1)=100
       user(1)=100

      iinder(1)=8
      iinder(2)=1
      iinder(3)=0
      iinder(4)=11
      iinder(5)=0
      iinder(6)=0
      iinder(7)=0
      iinder(8)=2
      iuser( 6)=7
c     ** itime, modelv, intrule, icoor, mcontv
      iuser( 2)=1
      iuser( 7)=0
      iuser( 8)=1
      iuser( 9)=intrule900
      iuser(10)=icoor900
      iuser(11)=mcontv
c     *** eps, rho
      iuser(12)=-6
      if (compress) then
         iuser(13)=3
      else
         iuser(13)=-7
      endif
c     *** omega, f1, f2, f3, eta
      iuser(14)=0
      iuser(15)=0
      iuser(16)=0
      iuser(17)=0
c     *** NB: eta=1
      iuser(18)=-7
      if (isolmethod.eq.0) then
         user(6)=1d-6
      else
         user(6)=0d0
      endif
       user(7) =1d0
      call deriv(iinder,ivisdip,kmesh1,kprob1,isol1,iuser,user)

      return
      end
