c **************************************************************
c *   CONDUCTIVE
c *   Calculate conductive solution for element 800
c *   PvK 070900
c *************************************************************
      subroutine conductive(kmesh2,kprob2,isol2,intmt2,matr2)
      implicit none
      integer kmesh2(*),kprob2(*),isol2(*),intmt2(*),matr2(*)
c     *** COMMON ****
      include 'depth_thermodyn.inc'
      include 'pecof800.inc'

c     *** LOCAL VARIABLES ***
      integer iuser(100),islol2(5),irhs2(5),massmt(5)
      integer iinbld(20),inpsol(20)
      real*8 user(100),rinpsol(5)
      save iuser,user,islol2,irhs2,massmt,iinbld
      save inpsol,rinpsol
      integer i,iread

      iuser(1) = 100
       user(1) = 100

      do i=5,100
         iuser(i)=0
          user(i)=0
      enddo

c *   *** SPECIFY COEFFICIENTS ***

      iuser(2) = 1+numnat800
      iuser(6) = 8
      if (numnat800.eq.1) iuser(7)=40
c     *** type integer info.
c     ( 1) - not yet used
      iuser(8) = 0
c     ( 2) - type of upwinding
      iuser(9) = 0
c     ( 3) - type of numerical integration
      iuser(10) = intrule
c     ( 4) - type of coordinate system 
      iuser(11) = icoorsystem
c     ( 5) - not yet used
      iuser(12) = 0

      if (idiftype.eq.0) then
         iuser(13) = -6
         iuser(16) = -6
      else if (idiftype.eq.1) then
c        *** function of depth
         iuser(13) = 4
         iuser(16) = 4
      else 
         write(6,*) 'PERROR(conductive)'
         write(6,*) 'idiftype = ',idiftype
         write(6,*) '   does not make sense here'
         call instop
      endif
c     *** iuser(18): u=0
c     *** iuser(19): v=0
c     *** iuser(20): w=0
c     *** iuser(21): beta=0
c     *** iuser(22): q

      if (iqtype.ge.1) then
         iuser(23) = -7
          user(7) = q_layer(1)
      endif

      user(6)  = 1d0

      if (numnat800.eq.1) then
         iuser(40) = 2
         iuser(41) = 0
         iuser(42) = 0
         iuser(43) = icoorsystem
         iuser(44) = 0
         iuser(45) = 0
         iuser(46) = -8
          user(8) = heat_flux_in
c       (6-11) Thermal conductivity
        if (idiftype.eq.0) then
            iuser(47) = -6
            iuser(48) = 0
            iuser(49) = 0
            iuser(50) = -6
            iuser(51) = 0
            iuser(51) = 0
        else
c           *** function of depth
            iuser(47) = 4
            iuser(48) = 0
            iuser(49) = 0
            iuser(50) = 4
            iuser(51) = 0
            iuser(51) = 0
        endif
      endif



c     *** BUILD AND SOLVE SYSTEM ***
      iinbld(1)=2
      iinbld(2)=1
      call build(iinbld,matr2,intmt2,kmesh2,kprob2,irhs2,
     v           massmt,isol2,islol2,iuser,user)

      inpsol(1)=3
      inpsol(2)=0
      inpsol(3)=0
      iread=-1
      call solvel(inpsol,rinpsol,matr2,isol2,irhs2,intmt2,
     v            kmesh2,kprob2,iread)
    
      return
      end

