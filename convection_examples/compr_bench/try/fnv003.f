c *************************************************************
c *   FNV003
c *
c *   Viscosity description used with modelv=103.
c *   Temperature is stored as first degree of freedom of
c *   second vector in uold 
c *
c *   This function provides a pathway to pefvis() when
c *   modelv=103 is used.
c *
c *   PvK 2000
c *************************************************************
      real*8 function fnv003(x1,x2,x3,v1,v2,v3,secsqr,
     v                  numold,maxunk,uold)
      implicit none
      integer numold,maxunk
      real*8 x1,x2,x3,v1,v2,v3,secsqr,uold(numold,maxunk)
      real*8 pefvis,secinv

      include 'pesimplerheology.inc'
      include 'vislo.inc'
      include 'c1visc.inc'
      include 'powerlaw.inc'
      include 'peparam.inc'
      real*8 temp
      integer jtypv,ival,ivalfind1

      if (numold.lt.2) then
         write(6,*) 'PERROR(fnv003): numold < 2'
         write(6,*) 'Second vector should contain temperature here'  
         call instop
      endif

      jtypv = abs(itypv)
      if (jtypv.eq.0) then 
         fnv003 = 1d0
         return
      else if (jtypv.eq.1) then
         ival = ivalfind1(x1,x2)
         fnv003 = viscl(ival)
         return
      else 
         temp = uold(2,1)
         ival = ivalfind1(x1,x2)
         fnv003 = pefvis(x1,x2,temp,ival,secsqr)
         return
      endif

      return
      end

