c *************************************************************
c *   PEFILCOF_CYL
c *
c *   Modified for ichois=1 Stokes equation. Use modelv=103
c *
c *   PvK 981108/040900
c *************************************************************
      subroutine pefilcof_cyl(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      implicit none
      integer isol2(*),iuser(*),kmesh1(*),kmesh2(*),kprob1(*)
      integer kprob2(*),isol1(*),islol1(5,*),ichois,iheat(*),idens(*) 
      integer ivisc(*),isecinv(*)
      real*8 user(*),work(100),eps,f2

      integer iwork(4,100),itime,modelv,npoint,iincop(6),i
      integer iinpri(5),icurvs(5),iprhocp,k,nparm,j,inputcr(10)
      integer ishape1,kelmf1
      real*8 rinputcr(10),format
      character*80 text
      
      include 'c1visc.inc'
      include 'peparam.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'solutionmethod.inc'
      real*8 DONE
      parameter(DONE=1d0)

      if (compress) then
         if (.not.tala) then
            write(6,*) 'PERROR(pefilcof_cyl): not suited for'
            write(6,*) 'ALA just yet. '
            call instop
         endif
      endif

      if (ichois.eq.1) then
c        *** Momentum equation type 900 only

c        *** clear work/iwork
         do i=1,100
            do k=1,4
              iwork(k,i) = 0
            enddo
            work(i) = 0d0
         enddo
         do i=1,95
            iuser(5+i)=0
         enddo
         kelmf1 = kmesh1(20)
         ishape1 = kmesh1(kelmf1)


c        *** Coefficients for element 900
c        *** First five, integer info
         modelv=  103
         itime = 0
         iwork(1,1) = itime
         iwork(1,2) = modelv
         if (ishape1.eq.3) then
            iwork(1,3) = intrule900 
         else
            iwork(1,3) = intrule900 + 100*interpol900
         endif
         iwork(1,4) = icoor900
         iwork(1,5) = mcontv
         if (axi.and.icoor900.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif
         if (.not.axi.and.icoor900.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif

         if (isolmethod.eq.0) then
c           *** penaltyfunction parameter
            eps = 1d-6
         else
            eps = 0
         endif
         f2     = 1d0

c        *** 6: epsilon
         iwork(1,6) = 0
          work(6) = eps
c        *** 7: rho
         if (compress) then
            iwork(1,7) = 3
         else 
            iwork(1,7) = 0
             work(7)   = 1d0
         endif
c        *** 8: omega
c        *** 9: f1. First dof of third vector in islold
c        *** This contains only the thermal (+phase change) components
c        *** The chemical buoyancy is added in elm900_pvk.
         iwork(1,9) = 2003
         iwork(2,9) = 3
         iwork(3,9) = 1
c        *** 10: f2. Second dof of third vector in islold.
c        *** This contains only the thermal (+phase change) components
c        *** The chemical buoyancy is added in elm900_pvk.
         iwork(1,10) = 2003
         iwork(2,10) = 3
         iwork(3,10) = 2
c        *** 11: f3
         iwork(1,11) = 0
          work(11) = 0d0
c        *** 12: eta (specified through fnv003)
         iwork(1,12) = 0
         work(12)   = 1d0

         nparm=12


c        *** copy temperature in first dof of 2nd vector islold
         call cofcopy(0,islol1(1,2),isol2,kmesh1,kmesh2,
     v          kprob1,kprob2,DONE)
c        *** copy f1+f2 into 3rd vector in islold
         call f12copy(islol1(1,3),isol2,kmesh1,kmesh2,
     v          kprob1,kprob2)
         call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1)

       else if (ichois.eq.2) then


c *************************************************************
c *      TEMPERATURE EQUATION
c *************************************************************
         if (axi.and.icoorsystem.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (.not.axi.and.icoorsystem.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (Di.gt.0.and.itypv.ne.0) then
               write(6,*) 'determine viscosity for temperature'
               call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,
     v                isol2,ivisc,isecinv,iuser,user)
         endif
         call coef800(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                ivisc,iheat,idens,iuser,user)

       else if (ichois.eq.3) then

         do i=6,100
            iuser(i)=0
         enddo
c        *** Element group 1
         iuser(6)=10
c        *** Boundary element group 1
c        iuser(7)=30

c        *** itime, modelv, numint, icoor, mcont
         iuser(10)=0
         iuser(11)=1
         iuser(12)=intrule
         iuser(13)=1
         iuser(14)=0
c        eps rho omega f1 f2 f3 eta 
         iuser(15)=-6
         iuser(16)=-7
         iuser(17)=0
         iuser(18)=0
         iuser(19)=0
         iuser(20)=0
         iuser(21)=-7
          user(6)=1d-6
          user(7)=1d0

c        *** Boundary element information
c        iuser(30)=2
c        iuser(31)=0
c        iuser(32)=intrule
        

       endif

       return
       end
c *************************************************************
c *   COEF8_2_cyl
c *
c *   Modified for cylindrical/axisymmetric version
c *       Change y->r
c *       Change vervel -> radial velocity
c *   PvK 1997/230400
c *************************************************************
      subroutine coef8_2_cyl(N,horvel,vervel,beta,right,rhocp,temp,coor)
      implicit none 
      integer N
      real*8 horvel(N),vervel(N),beta(N),right(N),rhocp(N)
      real*8 coor(2,N),temp(N)
      include 'cpephase.inc'
      real*8 tratio,bigGamma,dGdpi,alphas,rhocps,rho
      real*8 x,y,funccf,prespi,glRbRa,gl2RbRaDi,r,z
      integer i,iph
      dimension glRbRa(10),gl2RbRaDi(10)
      include 'ccc.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)
      real*8 th,cost,radial_velocity,yold,sint
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'
      include 'pecof900.inc'

      write(6,*) 'PERROR(coef800_2_cyl): needs update'
      call instop
      
      if (nph.gt.2) then
         write(6,*) 'PERROR(coef8_2): nph > 2'
         write(6,*) 'nph = ',nph
         call instop
      endif
      do iph=1,nph
        glRbRa(iph) = gamma(iph)*Rb(iph)/Ra 
        gl2RbRaDi(iph) = gamma(iph)*Di*glRbRa(iph)
        if (phdz(iph).le.0.or.phdz(iph).ge.1) then
           write(6,*) 'PERROR(coef8_2): phdz <= 0 or phdz >= 1'
           write(6,'(''phdz('',i2,'') = '',f12.3)') iph,phdz(iph)
           call instop
        endif
      enddo
    
      yold=99
      do i=1,N
         x = coor(1,i)
         y = coor(2,i)
         r = sqrt(x*x+y*y)
         cost = y/r
         sint = x/r
         if (x.ge.0) then
            th = acos(cost)
         else
            th = 2*pi - acos(cost)
         endif
         radial_velocity = horvel(i)*sin(th) + vervel(i)*cos(th)

c        radial_velocity = horvel(i)*sint+vervel(i)*cost
         dGdpi = 0d0
         if (ialphatype.eq.1) then
            alphas = funccf(5,x,y,z)
         else
            alphas = 1d0
         endif
         if (compress) then
            rho = funccf(3,x,y,z)
            rhocps = rho
         else
            rho    = 1d0
            rhocps = 1d0
         endif
         do iph=1,nph
c          *** make sure r2-r1=1 (for cylindrical/axisymmetric case)
           prespi = (r2-r) - phz0(iph) - gamma(iph)*(temp(i)-pht0(iph))
           tratio = temp(i) + Ts_dim/DeltaT_dim
           bigGamma = 0.5 + 0.5*tanh(prespi/phdz(iph))
c          dGdpi = dGdpi + 2d0/phdz(iph)*bigGamma*(1-bigGamma)
           dGdpi = 2d0/phdz(iph)*bigGamma*(1-bigGamma)
c          if (y.ne.yold.and.iph.eq.1) then
c          write(6,'(''Prespi: '',i5,6f8.3)') iph,y,1-y-phz0(iph),
c    v        prespi,dGdpi,bigGamma,temp(i)
c           yold=y
c          endif

           alphas = alphas + glRbRa(iph)*dGdpi
           rhocps = rhocps + gl2RbRaDi(iph)*dGdpi*tratio
         enddo
c        *** Note: use formulation from Christensen and Yuen, 1985

         beta(i)   = rho*alphas*Di*radial_velocity
         rhocp(i)  = rhocps
c        if (nint(th*1000).eq.1571) then
c          write(6,'(10f8.2)') x,y,r,th,beta(i),right(i),rhocp(i),
c    v        alphas,radial_velocity,horvel(i)
c        endif
      enddo
          
      return
      end
