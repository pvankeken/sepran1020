c *************************************************************
c *   ELEMENTCURVED
c *   Check whether an element is curved or straight
c *************************************************************
      logical function elementcurved(x,y,eps)
      implicit none
      real*8 x(*),y(*),eps
      real*8 fx,fy,dx,dy

      elementcurved = .false.
      fx = abs( x(1)-2*x(2)+x(3) )
      fy = abs( y(1)-2*y(2)+y(3) )
      dx = abs(x(1)-x(3))
      dy = abs(y(1)-y(3))
      if (max(fx,fy).gt.max(dx,dy)*eps) then
         elementcurved = .true.
         return
      endif
      fx = abs( x(3)-2*x(4)+x(5) )
      fy = abs( y(3)-2*y(4)+y(5) )
      dx = abs(x(3)-x(5))
      dy = abs(y(3)-y(5))
      if (max(fx,fy).gt.max(dx,dy)*eps) then
         elementcurved = .true.
         return
      endif
      fx = abs( x(5)-2*x(6)+x(1) )
      fy = abs( y(5)-2*y(6)+y(1) )
      dx = abs(x(5)-x(1))
      dy = abs(y(5)-y(1))
      if (max(fx,fy).gt.max(dx,dy)*eps) then
         elementcurved = .true.
         return
      endif
 
      end

c *************************************************************
c *   PRINTKMESHO
c *   Utility to print neighboring elements
c *************************************************************
      subroutine printkmesho(kmesho1,kmesho2,nelem,nelgrp)
      implicit none
      integer kmesho1,kmesho2(*),nelem,nelgrp
      integer ip,i

      write(6,*) 'kmesho1 (3): ',kmesho1,nelgrp
      ip = 0
      do i=1,nelem
         write(6,*) kmesho2(ip+i),kmesho2(ip+i+1),
     v              kmesho2(ip+i+2)
         ip=ip+3
      enddo
      return
      end


c *************************************************************
c *   CHECK_NEIGHBORS
c *
c *   Utility that is used if the normal element number lookup
c *   mechanism (through ielfromgrid) fails. This can happen
c *   when the interpolation grid (defined in /pielgrid/) cannot 
c *   accurately represent the fine details of the finite element grid.
c *   ielfromgrid() tests 4 different candidate elements (stored in 
c *   iel_now(*); it is likely that the correct element is a neighbor
c *   of one of these elements. This subroutine provides the 
c *   mechanisms to test the boundary elements which are found 
c *   by using kmesh part O.
c *
c *   xm,ym         i     tracer coordinates
c *   kmeshc        i     nodal point numbers associated with elements
c *   coor          i     coordinates of the nodal points
c *   nneighbors    i     Standard number of neighbors for this element
c *                       (3 for a triangle)
c *   kmesho2       i     kmesh part O2. This contains nneighbors*nelem
c *                       entries, where each entry provides the 
c *                       element number of the neighbor and in some
c *                       cases the boundary number as well (see below)
c *   nelem         i     number of elements in the mesh
c *   nodno         o     nodal point numbers of correct element (if found)
c *   xn,yn         o     coordinates of correct elements (if found)
c *   iel_now       i     4 candidate elements provided by ielfromgrid
c *   iel           o     number of correct element (if found)
c *   rl            o     linear shape functions in xm,ym
c *
c *   PvK 012804
c *************************************************************
      subroutine check_neighbors(xm,ym,kmeshc,coor,
     v            nneighbors,kmesho2,nelem,
     v            nodno,nodlin,xn,yn,iel_now,iel,rl,phiq,xi,eta)
      implicit none
      real*8 xm,ym,coor(2,*),xn(*),yn(*)
      real*8 rl(*),phiq(*),xi,eta
      integer kmeshc(*),nneighbors,kmesho2(*),nelem,nodno(*),nodlin(*)
      integer iel_now(*),iel,ip1,ip
      integer inpelm,i,j,k,iel_neighbor,isub,ko
      logical out,checkinelem
      include 'elem_topo.inc'

      inpelm = 6
c     *** loop over the 4 possible elements indicated by
c     *** ielfromgrid
      i=1
100   continue

         if (iel_now(i).gt.0) then
c           *** loop over neighboring elements
            j=1
200         continue
              ip1 = (iel_now(i)-1)*nneighbors+j
              ko = kmesho2(ip1)
c             *** get element number of jth neighbor of ielnow(i)
c             *** kmesho2 contains either ielem or 
c             *** ielem + (nelem+1)*kboundary
c             *** See description of kmesh in Sepran Programmer's Guide
250           continue
                if (ko.gt.nelem) then
                   ko = ko - nelem - 1
                   goto 250
                endif
              iel_neighbor=ko
c             write(6,*) 'iel_neighbor = ',i,j,iel_neighbor,iel_now(i) 
              if (iel_neighbor.gt.0) then
                 ip = (iel_neighbor-1)*inpelm
                 do k=1,inpelm
                    nodno(k) = kmeshc(ip+k)
                    xn(k) = coor(1,nodno(k))
                    yn(k) = coor(2,nodno(k))
                 enddo
                 if (curved_elem(iel_neighbor)) then 
                    out = .not.checkinelem(3,xn,yn,xm,ym,nodno,nodlin,
     v                     rl,xi,eta,phiq,isub)
                 else 
                    out = .not.checkinelem(1,xn,yn,xm,ym,nodno,nodlin, 
     v                     rl,xi,eta,phiq,isub)
                 endif
              else
                 out = .true.
              endif
              if (out) then
                 if (j.lt.3) then
c                   *** continue on inner loop
                    j=j+1
                    goto 200
                 else
c                   *** search for this iel_now element has failed
                    iel = -1
                 endif
              else
c                *** Return correct element number
                 iel = iel_neighbor
                 return
              endif
          endif
          if (i.lt.4) then
c            *** continue on outer loop
             i=i+1
             goto 100
          endif

      return
      end

c *************************************************************
c *   FIND_XI_ETA
c *   Find the barycentric coordinates (xi,eta) of the point
c *   (xr,yr) in the triangle defined by xnodr,ynodr; provide 
c *   quadratic shapefunctions through phiq
c *
c *   Parameters
c *      xnodr,ynodr      i    nodal points coordinates
c *      xr,yr            i    coordinates  of the interpolation point
c *      xi,eta           o    barycentric coordinates 
c *      phiq             o    quadratic shapefunctions
c *
c *   PvK 013004
c *************************************************************
      subroutine find_xi_eta(xnodr,ynodr,xr,yr,xi,eta,phiq,fail)
      implicit none
      real*8 xnodr(*),ynodr(*),xr(*),yr(*),xi,eta,phiq(*)
      logical fail
      real*8 tolx
      integer ntrial 

      ntrial = 30
      tolx = 1e-8
c     At input: xi,eta should contain initial estimate
c     *** use mid point of the triangle for initial estimate
      xi=1d0/3
      eta=1d0/3
      call xi_eta_newton(ntrial,xi,eta,tolx,xnodr,ynodr,xr,yr,phiq,fail)

      return
      end

c *************************************************************
c *   XI_ETA_NEWTON
c *
c *   Find barycentric coordinates (xi,eta) and quadratic 
c *   shapefunctions (phiq) of the point (xr,yr) in the 
c *   curved quadratic triangle defined by (xnodr,ynodr)
c *   using a Newton iteration
c *
c *   PvK 013004
c *************************************************************
      subroutine xi_eta_newton(ntrial,xi,eta,tolx,xnodr,ynodr,
     v              xr,yr,phiq,fail)  
      implicit none
      integer ntrial 
      real*8 xi,eta,tolx,xnodr(*),ynodr(*),xr,yr,phiq(*)
      logical fail
      real*8 p(2)
      integer ND
      parameter(ND=2)
      real*8 fvec(ND),fjac(ND,ND),d,errf
      integer indx(ND),k
      
      k=1
100   continue
        call get_f_and_jac(xi,eta,fvec,fjac,ND,xnodr,ynodr,xr,yr,phiq)
        errf = abs(fvec(1))+abs(fvec(2))
        if (errf.lt.tolx) then
           fail = .false.
           return
        endif

        p(1) = -fvec(1)
        p(2) = -fvec(2)
        call ludcmp(fjac,2,ND,indx,d)
        call lubksb(fjac,2,ND,indx,p)
        xi  = xi  + p(1)
        eta = eta + p(2)
        if (k.lt.ntrial) then
          k = k+1 
          goto 100
        else
          fail = .true.
c         write(6,*) 'PERROR(xi_eta_newton): Newton iteration' 
c         write(6,*) 'failed to converge after ',ntrial,' iterations' 
c         write(6,*) 'xi, eta: ',xi,eta
        endif
 
      return
      end

c *************************************************************
c *   GET_F_AND_JAC
c *
c *   Compute the mismatch vector and Jacobian matrix 
c *   PvK 013004
c *************************************************************
      subroutine get_f_and_jac(xi,eta,fvec,fjac,n,xnodr,ynodr,
     v                xr,yr,phiq) 
      implicit none
      integer n
      real*8 xi,eta,fvec(n),fjac(n,n),xnodr(*),ynodr(*),xr,yr,phiq(*)
      real*8 phil(3),dphidksi(6),dphideta(6)
      integer i,j

      call getshape_quad_xi_eta(xi,eta,phil,phiq,dphidksi,dphideta)
      fvec(1)   = 0
      fvec(2)   = 0
      fjac(1,1) = 0
      fjac(1,2) = 0
      fjac(2,1) = 0
      fjac(2,2) = 0
      do j=1,6
         fvec(1)    =  fvec(1) + phiq(j)*xnodr(j)
         fvec(2)    =  fvec(2) + phiq(j)*ynodr(j)
         fjac(1,1)  =  fjac(1,1) + dphidksi(j)*xnodr(j)
         fjac(1,2)  =  fjac(1,2) + dphideta(j)*xnodr(j)
         fjac(2,1)  =  fjac(2,1) + dphidksi(j)*ynodr(j)
         fjac(2,2)  =  fjac(2,2) + dphideta(j)*ynodr(j)
      enddo   
      fvec(1) = fvec(1) - xr
      fvec(2) = fvec(2) - yr

      return
      end

c *************************************************************
c *   getshape_quad_xi_eta
c *   Get shape functions and derivatives of quadratic reference 
c *   triangle in point (xi,eta)
c *
c *   Parameters:
c *       xi,eta      i    trial point
c *       phil        o    linear shape functions 
c *       phiq        o    quadratic shape functions
c *       dphidksi    o    dphiq/dx in 
c *       dphideta    o    dphiq/dy in
c *       nl          i    number of linear shapefunctions (3)
c *       nq          i    number of quadratic shapefunctions (6)
c *
c *   PvK 013004
c *************************************************************
      subroutine getshape_quad_xi_eta(xc,yc,phil,phiq,dphidksi,
     v                         dphideta)
      implicit none
      real*8 xc,yc
      real*8 phil(3),phiq(6),dphidksi(6),dphideta(6)
      real*8 dldks1,dldks2,dldks3,dldet1,dldet2,dldet3
      integer i
      real*8 a(3),b(3),c(3)
c     *** coefficients for linear shape functions of reference element
      data (a(i),i=1,3)/ 1d0, 0d0, 0d0/
      data (b(i),i=1,3)/-1d0, 1d0, 0d0/
      data (c(i),i=1,3)/-1d0, 0d0, 1d0/

      do i=1,3
         phil(i) = a(i)+b(i)*xc+c(i)*yc
      enddo
c     *** quadratic shapefunctions
      phiq(1) = phil(1)*(2*phil(1)-1)
      phiq(3) = phil(2)*(2*phil(2)-1)
      phiq(5) = phil(3)*(2*phil(3)-1)
      phiq(2) = 4*phil(1)*phil(2)
      phiq(4) = 4*phil(2)*phil(3)
      phiq(6) = 4*phil(3)*phil(1)
c     *** slope of linear shape functions on reference triangle
      dldks1 = b(1)
      dldks2 = b(2)
      dldks3 = b(3)
      dldet1 = c(1)
      dldet2 = c(2)
      dldet3 = c(3)
c     *** derivatives of shape functions with respect to xi
      dphidksi(1) = (4*phil(1)-1d0)*dldks1
      dphidksi(3) = (4*phil(2)-1d0)*dldks2
      dphidksi(5) = (4*phil(3)-1d0)*dldks3
      dphidksi(2) = 4*(dldks1*phil(2) + dldks2*phil(1))
      dphidksi(4) = 4*(dldks2*phil(3) + dldks3*phil(2))
      dphidksi(6) = 4*(dldks3*phil(1) + dldks1*phil(3))
c     *** and with respect to eta
      dphideta(1) = (4*phil(1)-1d0)*dldet1
      dphideta(3) = (4*phil(2)-1d0)*dldet2
      dphideta(5) = (4*phil(3)-1d0)*dldet3
      dphideta(2) = 4*(dldet1*phil(2) + dldet2*phil(1))
      dphideta(4) = 4*(dldet2*phil(3) + dldet3*phil(2))
      dphideta(6) = 4*(dldet3*phil(1) + dldet1*phil(3))

      return
      end

c *************************************************************
c *   getshape_quad_xy
c *   Get shape functions and derivatives of quadratic triangle
c *   in point (xc,yc)
c *
c *   Parameters:
c *       xc,yc       i    trial point
c *       x,y         i    coordinates of nodal points
c *       phil        o    linear shape functions  in (xc,yc)
c *       phiq        o    quadratic shape functions in (xc,yc)
c *       dphidksi    o    dphiq/dx in (xc,yc)
c *       dphideta    o    dphiq/dy in (xc,yc)
c *       nl          i    number of linear shapefunctions (3)
c *       nq          i    number of quadratic shapefunctions (6)
c *
c *   PvK 013004
c *************************************************************
      subroutine getshape_quad_xy(xc,yc,x,y,phil,phiq,dphidksi,
     v                         dphideta,nl,nq)
      implicit none
      integer nl,nq
      real*8 xc,yc,suml
      real*8 x(nq),y(nq),phil(nl),phiq(nq),dphidksi(nq),dphideta(nq)
      real*8 a(3),b(3),c(3),xl(3),yl(3)
      real*8 dldks1,dldks2,dldks3,dldet1,dldet2,dldet3
      integer i

c     *** get coefficients for linear shapefunctions
      xl(1) = x(1)
      yl(1) = y(1)
      xl(2) = x(3)
      yl(2) = y(3)
      xl(3) = x(5)
      yl(3) = y(5)
      call trilin(xl,yl,a,b,c)
      write(6,*) 'xl, yl:',xl(1),yl(1),xl(2),yl(2),xl(3),yl(3) 
c     *** linear shapefunctions 
      suml = 0
      do i=1,3
         phil(i) = a(i)+b(i)*xc+c(i)*yc
         suml = suml + phil(i)
      enddo
      write(6,'(''phil in xy: '',3f10.7,f8.3)') (phil(i),i=1,3),suml
c     *** quadratic shapefunctions
      phiq(1) = phil(1)*(2*phil(1)-1)
      phiq(3) = phil(2)*(2*phil(2)-1)
      phiq(5) = phil(3)*(2*phil(3)-1)
      phiq(2) = 4*phil(1)*phil(2)
      phiq(4) = 4*phil(2)*phil(3)
      phiq(6) = 4*phil(3)*phil(1)
c     *** slope of linear shape functions on reference triangle
      dldks1 = b(1)
      dldks2 = b(2)
      dldks3 = b(3)
      dldet1 = c(1)
      dldet2 = c(2)
      dldet3 = c(3)

c     *** derivatives of shape functions with respect to xi
      dphidksi(1) = (4*phil(1)-1d0)*dldks1
      dphidksi(3) = (4*phil(2)-1d0)*dldks2
      dphidksi(5) = (4*phil(3)-1d0)*dldks3
      dphidksi(2) = 4*(dldks1*phil(2) + dldks2*phil(1))
      dphidksi(4) = 4*(dldks2*phil(3) + dldks3*phil(2))
      dphidksi(6) = 4*(dldks3*phil(1) + dldks1*phil(3))
c     *** and with respect to eta
      dphideta(1) = (4*phil(1)-1d0)*dldet1
      dphideta(3) = (4*phil(2)-1d0)*dldet2
      dphideta(5) = (4*phil(3)-1d0)*dldet3
      dphideta(2) = 4*(dldet1*phil(2) + dldet2*phil(1))
      dphideta(4) = 4*(dldet2*phil(3) + dldet3*phil(2))
      dphideta(6) = 4*(dldet3*phil(1) + dldet1*phil(3))

      return
      end

      subroutine getshape7_xi_eta(xi,eta,phiq)
      implicit none
      real*8 xi,eta,phiq(7)
      real*8 a(3),b(3),c(3),phil(3),phil123
      integer i

c     *** coefficients for linear shape functions of reference element
      data (a(i),i=1,3)/ 1d0, 0d0, 0d0/
      data (b(i),i=1,3)/-1d0, 1d0, 0d0/
      data (c(i),i=1,3)/-1d0, 0d0, 1d0/

      do i=1,3
         phil(i) = a(i)+b(i)*xi+c(i)*eta
      enddo
      phil123 = phil(1)*phil(2)*phil(3)

c     *** shapefunctions of extended quadratic element
      phiq(1) = phil(1)*(2*phil(1)-1) + 3*phil123
      phiq(3) = phil(2)*(2*phil(2)-1) + 3*phil123
      phiq(5) = phil(3)*(2*phil(3)-1) + 3*phil123
      phiq(2) = 4*phil(1)*phil(2) - 12*phil123
      phiq(4) = 4*phil(2)*phil(3) - 12*phil123
      phiq(6) = 4*phil(3)*phil(1) - 12*phil123
      phiq(7) = 27*phil123

      return
      end

      subroutine getshape6_xi_eta(xi,eta,phiq)
      implicit none
      real*8 xi,eta,phiq(6)
      real*8 a(3),b(3),c(3),phil(3)
      integer i

c     *** coefficients for linear shape functions of reference element
      data (a(i),i=1,3)/ 1d0, 0d0, 0d0/
      data (b(i),i=1,3)/-1d0, 1d0, 0d0/
      data (c(i),i=1,3)/-1d0, 0d0, 1d0/

      phil(1) = a(1)+b(1)*xi+c(1)*eta
      phil(2) = a(2)+b(2)*xi+c(2)*eta
      phil(3) = a(3)+b(3)*xi+c(3)*eta

c     *** shapefunctions of quadratic element
      phiq(1) = phil(1)*(2*phil(1)-1) 
      phiq(3) = phil(2)*(2*phil(2)-1) 
      phiq(5) = phil(3)*(2*phil(3)-1) 
      phiq(2) = 4*phil(1)*phil(2) 
      phiq(4) = 4*phil(2)*phil(3)
      phiq(6) = 4*phil(3)*phil(1) 

      return
      end

    
c     *** From Numerical Recipes
      SUBROUTINE ludcmp(a,n,np,indx,d)
      INTEGER n,np,indx(n),NMAX
      REAL*8 d,a(np,np),TINY
      PARAMETER (NMAX=500,TINY=1.0e-20)
      INTEGER i,imax,j,k
      REAL*8 aamax,dum,sum,vv(NMAX)
      d=1.
      do 12 i=1,n
        aamax=0.
        do 11 j=1,n
          if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
11      continue
        if (aamax.eq.0.) stop 'singular matrix in ludcmp'
        vv(i)=1./aamax
12    continue
      do 19 j=1,n
        do 14 i=1,j-1
          sum=a(i,j)
          do 13 k=1,i-1
            sum=sum-a(i,k)*a(k,j)
13        continue
          a(i,j)=sum
14      continue
        aamax=0.
        do 16 i=j,n
          sum=a(i,j)
          do 15 k=1,j-1
            sum=sum-a(i,k)*a(k,j)
15        continue
          a(i,j)=sum
          dum=vv(i)*abs(sum)
          if (dum.ge.aamax) then
            imax=i
            aamax=dum
          endif
16      continue
        if (j.ne.imax)then
          do 17 k=1,n
            dum=a(imax,k)
            a(imax,k)=a(j,k)
            a(j,k)=dum
17        continue
          d=-d
          vv(imax)=vv(j)
        endif
        indx(j)=imax
        if(a(j,j).eq.0.)a(j,j)=TINY
        if(j.ne.n)then
          dum=1./a(j,j)
          do 18 i=j+1,n
            a(i,j)=a(i,j)*dum
18        continue
        endif
19    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software '%1&9p#!.

      SUBROUTINE lubksb(a,n,np,indx,b)
      INTEGER n,np,indx(n)
      REAL*8 a(np,np),b(n)
      INTEGER i,ii,j,ll
      REAL*8 sum
      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if (ii.ne.0)then
          do 11 j=ii,i-1
            sum=sum-a(i,j)*b(j)
11        continue
        else if (sum.ne.0.) then
          ii=i
        endif
        b(i)=sum
12    continue
      do 14 i=n,1,-1
        sum=b(i)
        do 13 j=i+1,n
          sum=sum-a(i,j)*b(j)
13      continue
        b(i)=sum/a(i,i)
14    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software '%1&9p#!.
