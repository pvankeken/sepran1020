      integer NTRACMAX,MAXELEM,NCHEMMAX
      parameter(NTRACMAX= 204 000,NCHEMMAX=1,MAXELEM=100000)
      real*8 coormark(2*NTRACMAX),velmark(2*NTRACMAX),densmark(NTRACMAX)
      real*8 coornewm(2*NTRACMAX),velnewm(2*NTRACMAX)
      real*4 coorreal(2*NTRACMAX)
      real*8 xi_eta(2,NTRACMAX)
      integer ielemmark(NTRACMAX),itracel(NTRACMAX),iitracel(NTRACMAX)
      integer xi_eta_stored
      common /petrac/ coormark,velmark,densmark,coornewm,velnewm,
     v                coorreal,xi_eta,
     v                ielemmark,itracel,iitracel,xi_eta_stored

      real*4 chemmark(NCHEMMAX*NTRACMAX)
      integer nchem
      common /pechem/ chemmark,nchem

      integer tracinelem(NTRACMAX),notracinelem(MAXELEM+1)
      common /reordertrac/ tracinelem,notracinelem
