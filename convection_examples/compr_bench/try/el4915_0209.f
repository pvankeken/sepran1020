      subroutine el4915 ( s, phix, phi, xgauss, eta, w, swork, work,
     +                    mcont, modelv )
! ======================================================================
!
!        programmer    Guus Segal
!        version  6.1  date 15-09-2008 Correction compressible model
!        version  6.0  date 22-02-2006 Extra parameter modelv
!        version  5.6  date 17-08-2001 Allow mcont = 4
!        version  5.5  date 08-11-1998 Correction for Goertler Equations
!        version  5.4  date 28-09-1998 Extension for Goertler Equations
!                                      Ralf Biertuempfel
!        version  5.3  date 28-08-1998 include commons
!        version  5.2  date 23-12-1996 Layout
!        version  5.1  date 16-12-1994 New call to el3001
!        version  5.0  date 03-09-1994 Extra parameter mcont
!
!   copyright (c) 1987-2008  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     compute the viscosity part of the element matrix for navier stokes
!     and store in matrix s for 2d and 3-d elements
!     This subroutine does not store the swirl part
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/celint'
      include 'SPcommon/celp'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision phix(n,m,ndimlc), phi(n,m), eta(m), w(m),
     +                 s(ndimlc*n,ndimlc*n), swork(n,n), work(*),
     +                 xgauss(m,ndimlc)
      integer mcont, modelv

!     eta            i    Array containing the viscosity in the integration
!                         points
!     mcont          i    Defines the type of continuity equation
!                         See elm900
!     modelv         i    Defines the type of viscosity model
!                         See elm900
!     phi            i    array of length n * m, values of shape
!                         functions in integration points in the sequence:
!                         phi_i(xg^k) = phi (i,k)
!                         array phi is only filled when gauss integration is
!                         applied
!     phix           i    Array of length n x m x ndim  containing the
!                         derivatives of the basis functions in the sequence:
!                         d phi / dx (xg ) = phix (i,k,1);
!                              i        k
!                         d phi / dy (xg ) = phix (i,k,2);
!                              i        k
!                         d phi / dz (xg ) = phix (i,k,3);
!                              i        k
!                         If the element is a linear tetrahedron (n=4), then
!                         the derivatives are constant and only k = 1 is filled.
!     s             i/o   Element matrix to be filled
!     swork               Work array in which a sub element matrix is stored
!     w              i    array of length m containing the weights for
!                         integration
!     work           i    Work array of length 2*m
!     xgauss         i    array of length m x ndim containing the co-ordinates
!                         of the gauss points.  array xgauss is only filled when
!                         gauss integration is applied.
!                         xg_i = xgauss (i,1);  yg_i = xgauss (i,2);
!                         zg_i = xgauss (i,3)
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      logical symm, debug
      integer jzeros, idimsv
      double precision dmult(2)

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     dmult          Array containing multiplication factors
!     idimsv         help variable to save idim
!     jzeros         help variable to save jzero
!     symm           Indication whether the resulting matrix is symmetrical
!                    (true) or not (false)
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL3001         Compute integrals of the shape / u(x).grad phi phi (x) dx
!                                                  e  -            j   i
!     EL3002         Compute integrals of the shape / beta(x) phi (x) phi (x) dx
!                                                  e             i       j
!     EL3010         Fill small matrix elemmt into larger matrix s
!     EL3011         Compute contribution to element stiffness matrix
!                    corresponding to a part of the second derivative
!                    part of an elliptic equation
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2984   combination of compressible flow and reduced stress term
!    2985   combination of compressible flow and non-Cartesian coordinates
! **********************************************************************
!
!                       PSEUDO CODE
!
!     Computation of the stiffness matrix results in a 3x3 block matrix, each
!     consisting of nxn matrices of the following shape:
!            |   S       S      S     |
!            |    uu      uv     uw   |
!            |                        |
!            |   S       S      S     |
!     S =    |    vu      vv     vw   |
!            |                        |
!            |   S       S      S     |
!            |    wu      wv     ww   |
!     The submatrices are defined by:
!              T
!     S    =  S
!      xy      yx
!     Cartesian co-ordinates:
!     S uu =  2  eta phix phix + eta phiy phiy + eta phiz phiz
!     S uv =     eta phix(j) phiy(i)
!     S uw =     eta phix(j) phiz(i)
!     S vu =     eta phiy(j) phix(i)
!     S vv =  2  eta phiy phiy + eta phix phix + eta phiz phiz
!     S vw =     eta phiy(j) phiz(i)
!     S wu =     eta phiz(j) phix(i)
!     S wv =     eta phiz(j) phiy(i)
!     S ww =  2  eta phiz phiz + eta phix phix + eta phiy phiy
!     In case of modelv = 10:
!     S uu =  eta phix phix + eta phiy phiy + eta phiz phiz = S vv = S ww
!     Rest is zero
!     2D Cylinder co-ordinates (r, z) without swirl
!     S uu =  2 eta phir phir + eta phiz phiz + 2 eta phi phi /r**2
!     S uv =     eta phir(j) phiz(i)
!     S vu =     eta phiz(j) phir(i)
!     S vv =  2  eta phiz phiz + eta phir phir
!     2D Cylinder co-ordinates (r, z) with swirl
!     See without swirl, however with extra term:
!     S ww = eta ( psiz psiz + (psir(j) - psi(j)/r) (psir(i)-psi(i)/r) )
!     2D Polar co-ordinates (r, z)   (in this case z is actually the angle)
!     S uu = eta ( 2 phir phir + phiz phiz/r**2 + 2 phi phi /r**2 )
!     S uv =     eta (phir(j) phiz(i)/r + 2 phiz(j) phi(i)/r**2 -
!                      phi(j) phiz(i)/r**2)
!     S vu =     eta (phiz(j)/r phir(i) + 2 phi(j) phiz(i)/r**2 -
!                      phiz(j) phi(i)/r**2)
!     S vv =  eta ( 2 phiz phiz/r**2 + phir phir + phi phi /r**2 -
!                    phi(i) phir(j)/r - phi(j) phir(i)/r )
!    Goertler equations:
!     S uu =   eta phix phix  +    eta phiy phiy  +    eta phiz phiz
!     S vv =   eta phix phix  +  2 eta phiy phiy  +    eta phiz phiz
!     S ww =   eta phix phix  +    eta phiy phiy  +  2 eta phiz phiz
!     The non-diagonal block matrices are the same as in the 3D cartesian.
!
!    In case of compressible flow the stress term is changed from
!
!    T = eta ( grad v + grad v^T ) into
!
!    T = eta ( grad v + grad v^T ) - 2/3 eta div v
!
!    with v the velocity
!
!    This means in the Cartesian case that the following extra terms are added:
!
!            |   phi_jx phi_ix   phi_jy phi_ix  phi_jz phi_ix  |
!            |                                                 |
!   -2/3 eta |   phi_jx phi_iy   phi_jy phi_iy  phi_jz phi_iy  |
!            |                                                 |
!            |   phi_jx phi_iz   phi_jy phi_iz  phi_jz phi_iz  |
!            |                                                 |
!    The diagonal terms can be treated by multiplying by 4/3 (=2-2/3)
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'el4915' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from el4915'
         write(irefwr,1) 'jcart, modelv, mcont', jcart, modelv, mcont
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      if ( modelv==10 ) s = 0d0   ! modelv = 10, clear matrix

      if ( mcont==1 .or. mcont==4 ) then

!     --- compressible flow

         if ( modelv==10 ) then

!        --- The combination compressible flow and modelv=10 is not possible

            call errint ( mcont, 1 )
            call errint ( modelv, 2 )
            call errsub ( 2984, 2, 0, 0 )

         end if  ! ( modelv==10 )

         if ( jcart>2 ) then

!        --- The combination compressible flow and polar coordinates is not
!            implemented

            call errint ( mcont, 1 )
            call errint ( jcart, 2 )
            call errsub ( 2985, 2, 0, 0 )

         end if  ! ( jcart/=1 )

      end if  ! ( mcont==1 .or. mcont==4 )

!     --- Compute matrices eta phix phix, eta phiy phiy and eta phiz phiz
!         and store in Suu, Svv and Sww

      if ( jcart==1 ) then

         if ( modelv==10 ) then

!        --- Pure incompressible flow, apply divergence freedom
!            simplified expression

            dmult(1) = 1d0

         else

!        --- Pure incompressible flow, apply divergence freedom

            dmult(1) = 2d0

         end if  ! ( modelv==10 )
         if ( mcont==1 .or. mcont==4 ) then

!        --- compressible flow

            dmult(1) = 4d0/3d0
            dmult(2) = -0.5d0*dmult(1)

         end if  ! ( mcont==1 .or. mcont==4 )

         if ( debug ) call prinrl ( dmult, 3, 'dmult' )

      end if  ! ( jcart==1 )
      if ( ierror/=0 ) go to 1000

      if ( jcart==1 .and. ndimlc==2 ) then

!     --- 2D Cartesian co-ordinates
!         eta phix phix

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )
         if ( debug ) call prinrl1 ( swork, n, n, 'phixx' )

         call el3010 ( s, swork, 2*n, n, 1, 1, 2, .false., dmult(1) )
         call el3010 ( s, swork, 2*n, n, 2, 2, 2, .false., 1d0 )

!        --- eta phiy phiy

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )
         if ( debug ) call prinrl1 ( swork, n, n, 'phiyy' )

         jadd = 1
         call el3010 ( s, swork, 2*n, n, 1, 1, 2, .false., 1d0 )
         call el3010 ( s, swork, 2*n, n, 2, 2, 2, .false., dmult(1) )

         if ( modelv/=10 ) then

!        --- S uv =  eta phi_jx phi_iy

            jadd = 0
            symm = .false.
            call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                    symm )

            call el3010 ( s, swork, 2*n, n, 2, 1, 2, .true., 1d0 )
            call el3010 ( s, swork, 2*n, n, 1, 2, 2, .false., 1d0 )

            if ( mcont==1 .or. mcont==4 ) then

!           --- compressible flow -2/3 eta phi_ix phi_jy

               jadd = 0
               call el3011 ( eta, swork, w, phix, m, n, 2, 1, work,
     +                       symm )

               jadd = 1
               call el3010 ( s, swork, 2*n, n, 2, 1, 2, .true.,
     +                       dmult(2) )
               call el3010 ( s, swork, 2*n, n, 1, 2, 2, .false.,
     +                       dmult(2) )

            end if  ! ( mcont==1 .or. mcont==4 )

         end if  ! ( modelv/=10 )

      else if ( jcart==1 ) then

!     --- 3D Cartesian co-ordinates
!         eta phix phix

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         if (mcont==2) then

!        --- modified Navier-Stokes equation for Goertler problem

            call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )

         else

!        --- standard Navier-Stokes equations

            call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., dmult(1) )
            call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., 1d0 )
            call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., 1d0 )

         end if

!        --- eta phiy phiy

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )
         if (mcont==2) jadd=0
         call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., dmult(1) )
         call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., 1d0 )

!        --- eta phiz phiz

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 3, 3, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, 3*n, n, 1, 1, 3, .false., 1d0 )
         call el3010 ( s, swork, 3*n, n, 2, 2, 3, .false., 1d0 )
         call el3010 ( s, swork, 3*n, n, 3, 3, 3, .false., dmult(1) )

         if ( modelv/=10 ) then

!        --- S uv =  eta phix phiy

            jadd = 0
            symm = .false.
            call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                    symm )

            call el3010 ( s, swork, 3*n, n, 2, 1, 3, .true., 1d0 )
            call el3010 ( s, swork, 3*n, n, 1, 2, 3, .false., 1d0 )

!           --- S uw =  eta phix phiz

            symm = .false.
            call el3011 ( eta, swork, w, phix, m, n, 1, 3, work,
     +                    symm )

            call el3010 ( s, swork, 3*n, n, 3, 1, 3, .true., 1d0 )
            call el3010 ( s, swork, 3*n, n, 1, 3, 3, .false., 1d0 )

!           --- S vw =  eta phiy phiz

            symm = .false.
            call el3011 ( eta, swork, w, phix, m, n, 2, 3, work,
     +                    symm )

            call el3010 ( s, swork, 3*n, n, 3, 2, 3, .true., 1d0 )
            call el3010 ( s, swork, 3*n, n, 2, 3, 3, .false., 1d0 )

            if ( mcont==1 .or. mcont==4 ) then

!           --- compressible flow -2/3 eta phi_ix phi_jy

               jadd = 0
               call el3011 ( eta, swork, w, phix, m, n, 2, 1, work,
     +                       symm )

               jadd = 1
               call el3010 ( s, swork, 3*n, n, 2, 1, 3, .true.,
     +                       dmult(2) )
               call el3010 ( s, swork, 3*n, n, 1, 2, 3, .false.,
     +                       dmult(2) )

!              --- S uw = -2/3 eta phix phiz

               jadd = 0
               call el3011 ( eta, swork, w, phix, m, n, 3, 1, work,
     +                       symm )

               jadd = 1
               call el3010 ( s, swork, 3*n, n, 3, 1, 3, .true.,
     +                       dmult(2) )
               call el3010 ( s, swork, 3*n, n, 1, 3, 3, .false.,
     +                       dmult(2) )

!              --- S vw = -2/3 eta phiy phiz

               jadd = 0
               call el3011 ( eta, swork, w, phix, m, n, 3, 2, work,
     +                       symm )

               jadd = 1
               call el3010 ( s, swork, 3*n, n, 3, 2, 3, .true.,
     +                       dmult(2) )
               call el3010 ( s, swork, 3*n, n, 2, 3, 3, .false.,
     +                       dmult(2) )

            end if  ! ( mcont==1 .or. mcont==4 )

         end if  ! ( modelv/=10 )

      else if ( jcart==2 .and. ndimlc==2 .or. jcart==4 ) then

!     --- 2D Cylinder co-ordinates
!         eta phir phir

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- 2 eta phi phi / r**2

         work(1:m) = 2d0*eta(1:m)/xgauss(1:m,1)**2
         jadd = 0
         jzeros = jzero
         jzero  = 2
         call el3002 ( work, swork, w, phi, m, n, n, work(m+1) )
         jzero  = jzeros

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )

!        --- eta phiz phiz

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 2, 2, work,
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 2d0 )

!        --- S uv =  eta phir phiz

         jadd = 0
         symm = .false.
         call el3011 ( eta, swork, w, phix, m, n, 1, 2, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., 1d0 )

      else

!     --- 2D polar co-ordinates
!         eta phir phir

         jadd = 0
         symm = .true.
         call el3011 ( eta, swork, w, phix, m, n, 1, 1, work,
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- eta phi phi / r**2

         work(1:m) = eta(1:m)/xgauss(1:m,1)**2
         jadd = 0
         jzeros = jzero
         jzero  = 2
         call el3002 ( work, swork, w, phi, m, n, n, work(m+1) )
         jzero  = jzeros

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )

!        --- eta phiz phiz / r**2

         work(1:m) = eta(1:m)/xgauss(1:m,1)**2
         jadd = 0
         symm = .true.
         call el3011 ( work, swork, w, phix, m, n, 2, 2, work(m+1),
     +                 symm )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 1, 1, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 2d0 )

!        --- eta ( - phi(i) phir(j) - phi(j) phir(i) )/ r

         work(1:m) = -eta(1:m)/xgauss(1:m,1)
         jadd = 0
         idimsv = idim
         idim   = 1
         call el3001 ( work, swork, w, phi, phix, 1, n, work(m+1))

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .false., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 2, 2, ncomp, .true., 1d0 )

!        --- S uv
!            eta phir phiz/r

         work(1:m) = -work(1:m)
         jadd = 0
         symm = .false.
         call el3011 ( work, swork, w, phix, m, n, 1, 2, work(m+1),
     +                 symm )

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., 1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., 1d0 )

!        --- 2 eta phi(i) phiz(j)/r**2

         work(1:m) = work(1:m)/xgauss(1:m,1)
         jadd = 0
         call el3001 ( work, swork, w, phi, phix(1,1,2), 1, n,
     +                 work(m+1) )

         jadd = 1
         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .false., 2d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .true., 2d0 )

!        --- eta phi(j) phiz(i)/r**2

         call el3010 ( s, swork, ncomp*n, n, 2, 1, ncomp, .true., -1d0 )
         call el3010 ( s, swork, ncomp*n, n, 1, 2, ncomp, .false., -1d0)

         idim = idimsv

      end if

1000  call erclos ( 'el4915' )

      if ( debug ) then

!     --- Debug information

         call prinrl1 ( s, ndimlc*n, ndimlc*n, 's' )
         write(irefwr,*) 'End el4915'

      end if

      end
