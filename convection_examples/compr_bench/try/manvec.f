      subroutine manvec ( iinvec, rinvec, invec1, invec2, iresvc, kmesh,
     +                    kprob )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.5  date 29-10-2003 Adaptation comment
!        version  2.4  date 07-10-2003 Adaptation comment
!        version  2.3  date 26-09-2001 Extra options 44/45/46
!        version  2.2  date 12-11-2000 Extra option function of vector
!        version  2.1  date 04-09-2000 Extra option limit
!        version  2.0  date 10-04-1999 Replace body by manvecbf
!        version  1.10 date 21-09-1998 Include commons
!        version  1.9  date 03-02-1998 New calll to to0109
!        version  1.8  date 15-04-1995 Extension for ichois = 20, 24,
!                                      new call to to0035
!        version  1.7  date 09-11-1994 Correction iinvec(iskip)
!        version  1.6  date 09-11-1994 Correction iinvec(iskip)
!        version  1.5  date 16-10-1993 Extension with ICHOIS=39
!        version  1.4  date 16-10-1993 Extension with iinvec(2) = 36,37,38
!        version  1.3  date 13-03-1993 New layout
!        version  1.2  date 06-10-1991 extension for iinvec(2)=51
!        version  1.1  date 31-07-1991 extension for iinvec(2)=35, by Zdenek
!        version  1.0  date 03-03-1990
!
!   copyright (c) 1990-2003  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     General subroutine for manipulations with SEPRAN arrays
!
! **********************************************************************
!
!                       KEYWORDS
!
!     algebraic_manipulation
!     manipulation
!     vector
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/blank'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iinvec(*), invec1(*), invec2(*), kmesh(*), kprob(*),
     +        iresvc(*)
      double precision rinvec(*)

!   iinvec    i   In this integer input array it is indicated what type of
!                 manipulations must be preformed by subroutine MANVEC.
!                 For some cases IINVEC acts both as input and
!                 as output array.
!                 For a description of iinvec, see subroutine MANVECBF
!   invec1    i   Integer SEPRAN array of length 5 corresponding to a solution
!                 vector or a vector of special structure.
!                 For some possibilities of ICHOIS, u1 is not only an input
!                 vector but may be also output vector.
!                 In some cases INVEC1 corresponds to more than one vector
!                 ( u1 , u2 , + . + , u n  )
!                 In that case INVEC1 is supposed to be a two-dimensional
!                 array of size  5 x alpha  with  alpha >= n
!                 INVEC1(1:5,1: alpha ).
!                 In INVEC1( i,j ), ( i =1, 2, .. , 5) information about
!                 the  j th input vector must be stored (1 <= j <= n ).
!                 This may be done for example by copying the contents of
!                 other SEPRAN arrays into INVEC1.
!   invec2    i   Integer SEPRAN array of length 5 corresponding to a solution
!                 vector or a vector of special structure.
!                 Restriction: At this momemt INVEC2 must have exactly the same
!                 structure as INVEC1 if it is used.
!                 The corresponding arrays must be both real or both complex.
!   iresvc    o   Integer SEPRAN array of length 5 in which the output of the
!                 algebraic manipulation will be stored.
!                 In  u out  only the  IDEGFD sup th  degree of freedom in the
!                 nodal points gets a value when IDEGFD > 0.
!                 The other degrees of freedom are copied from  u1 .
!   kmesh     i   Standard SEPRAN array containing information of the mesh
!   kprob     i   Standard SEPRAN array containing information of the problem
!   rinvec    i   Double precision input and output array. The contents of
!                 RINVEC depend on the contents of IINVEC.
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS    Resets old name of previous subroutine of higher level
!     EROPEN    Produces concatenated name of local subroutine
!     MANVECBF  Actual body of manvec
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'manvec' )
      call manvecbf ( ibuffr, buffer, iinvec, rinvec, invec1,
     +                invec2, iresvc, kmesh, kprob )
      call erclos ( 'manvec' )
      end
