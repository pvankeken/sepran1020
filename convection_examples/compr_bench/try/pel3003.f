c *************************************************************
c *   PEL3003
c *
c *   Modified from el3003.  Intended for buoyancy given by tracers
c *   in primitive variable formulation.
c *
c *   PvK 990408
c *************************************************************
      subroutine pel3003(g,f,w,phi,mdecl,ndecl,work,x,ichoice)
      implicit none
      integer mdecl,ndecl,ichoice
      real*8 g(mdecl),f(ndecl),w(mdecl),phi(ndecl,mdecl)
      real*8 work(mdecl),x(mdecl,*)

      include 'ccc.inc'
      if (cyl) then
         call pel3003_cyl(g,f,w,phi,mdecl,ndecl,work,x,ichoice)
      else if (ichoice.eq.2) then
         call pel3003_cart(g,f,w,phi,mdecl,ndecl,work,x)
      endif

      return
      end


c *************************************************************
c *   PEL3003_CYL
c *
c *   Modified from el3003.  Intended for buoyancy given by tracers
c *   in primitive variable formulation.
c *   PvK 990408
c *
c *   PvK 101200 Modified for cylindrical coordinates
c *   Calculate contribution to element rhsd by summing over each
c *      tracer that is in this element (as indicated by ielemmark).
c *   Calculate the total contribution once (when ichoice=1)
c *   Then compute contribution to f1 (=f*sin(th)) and f2(=f*cos(th))
c *
c *   PvK 073004 Modified for markerchain method
c *************************************************************
      subroutine pel3003_cyl( g,f,w,phi,mdecl,ndecl,work,x,ichoice)
      implicit none
      integer mdecl, ndecl,ichoice
      real*8 g(mdecl), f(ndecl), w(mdecl), phi(ndecl,mdecl),
     +                 work(mdecl),x(mdecl,*)
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'petrac.inc'
      include 'tracer.inc'
      include 'cbuoy_trac.inc'
      include 'pecof900.inc'
      include 'cpix.inc'
      include 'elem_topo.inc'
      include 'ccc.inc'
      include 'csky.inc'

      real*8 rho_m,drho,dep,rho_ecl


      real*8 pi
      parameter(pi=3.1415926535897932)
      real*8 h, sum, xn(7),yn(7),shapef(7),xm,ym,r(7),sint,cost
      real*8 ftot(7),un(7),vn(7),xi,eta,Rbfactor,pixelsize
      real*8 thpix,rpix,prefac
      integer i, k,itrac,ic,ix,iy,ir,ith
      logical midflag,dotflag,first
      save ftot,r,xn,yn,Rbfactor,first

      data first/.true./

      if (jconsf.eq.1) then
         write(6,*) 'PERROR(pel3003): not suited for jconsf=1'
         call instop
      endif

      do i=1,mdecl
         xn(i) = x(i,1)
         yn(i) = x(i,2)
         un(i) = 0.
         vn(i) = 0.
         r(i) = sqrt(xn(i)*xn(i)+yn(i)*yn(i))
      enddo

c     **** Calculate total buoyancy force (f=f1+f2).
      if (ichoice.eq.1) then
         do i=1,7
           ftot(i)=0
         enddo
c        do ir=1,nrpix
c          do ith=1,nthpix           
c        *** Loop over the pixels that are estimated to be in this element
c        *** the minima/maxima ipix_rmin etc. are evaluated for each
c        *** element in ieltogrid().
         do ir=ipix_rmin(ielem),ipix_rmax(ielem)
           do ith=ipix_thmin(ielem),ipix_thmax(ielem)
             ic = (ir-1)*nthpix+ith
c            *** Test whether 1) pixel is in this element and
c            *** 2) whether pixel carries excess density
             if (ielempix(ic).eq.ielem.and.ipix(ic).gt.0) then
c               *** calculate the shape function in center of pixel
c               write(6,*) 'Pixel in element: ',ielem,ic,ipix(ic),r(1),axi
                xi  = xi_eta_pix(1,ic)
                eta = xi_eta_pix(2,ic)
c               *** prefac contains the size and geometry weight (for
c               *** axisymmetry). See ieltogrid()
                prefac = xi_eta_pix(3,ic)
                call getshape7_xi_eta(xi,eta,shapef)
                do i=1,7
                   ftot(i) = ftot(i)+shapef(i)*prefac
                enddo
c               write(6,*) 'ftot: ',ftot(2),ftot(3),ftot(4),prefac
             endif
           enddo           
         enddo
c        *** calculate f1 component (multiply with -Rb_local)
         do i=1,7
            sint = xn(i)/r(i)
            f(i)=f(i)-ftot(i)*sint*Rb_local
         enddo

      else

c        *** f2 component
         do i=1,7
            cost = yn(i)/r(i)
            f(i)=f(i)-ftot(i)*cost*Rb_local
         enddo

      endif

      return
      end

c *************************************************************
c *   PEL3003_CART
c *
c *   Modified from el3003.  Intended for buoyancy given by tracers
c *   in primitive variable formulation.
c *
c *   Note: neutral layer should be indicated in pixel grid by 
c *   the value '0'; the compositionally distinct layer by 1.
c *   Note that values different than 1 are ignored in this version.
c *
c *   PvK 990408
c *************************************************************
      subroutine pel3003_cart( g,f,w,phi,mdecl,ndecl,work,x)
      implicit none
      integer mdecl, ndecl
      real*8 g(mdecl), f(ndecl), w(mdecl), phi(ndecl,mdecl),
     +                 work(mdecl),x(ndecl,*)

      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'petrac.inc'
      include 'cbuoy_trac.inc'
      include 'pecof900.inc'
      include 'peparam.inc'
      include 'cpix.inc'

      real*8 h, sum, xn(7),yn(7),shapef(7),xm,ym,rl(3),xp,yp
      integer i, k,itrac,pefirst
      integer ip,ipx,ipy,ipxmin,ipxmax,ipymin,ipymax
      integer nodlin(3),nodno(6),isub
      real*8 xcmin,xcmax,ycmin,ycmax
      logical out
      real*8 eps
      parameter(eps=1d-8)
      data pefirst/0/


      if (jadd.eq.0) then
         do i=1,n
            f(i) = 0d0
         enddo
      endif
 
      if (jdiag.eq.1) then
         write(6,*) 'PERROR(pel3003): '
         write(6,*) '      not suited for Newton-Cotes integration'
         call instop
      endif
      if (jconsf.eq.1) then
         write(6,*) 'PERROR(pel3003): not suited for jconsf=1'
         call instop
      endif

      do i=1,ndecl
         xn(i) = x(i,1)
         yn(i) = x(i,2)
      enddo
 

c     *** Assume we have a triangle with straight edges here
c     *** (ok for cartesian box). Determine smallest rectangular
c     *** area around the element
      xcmin = min(xn(1),xn(3),xn(5))
      ycmin = min(yn(1),yn(3),yn(5))
      xcmax = max(xn(1),xn(3),xn(5))
      ycmax = max(yn(1),yn(3),yn(5))
      ipxmin = xcmin/dxpix + 1.0
      ipymin = ycmin/dypix + 1.0
      ipxmax = xcmax/dxpix + 1.5
      ipymax = ycmax/dypix + 1.5
      ipxmin = max(1,ipxmin)
      ipymin = max(1,ipymin)
      ipxmax = min(nxpix,ipxmax)
      ipymax = min(nypix,ipymax)

c     *** loop over pixels in this area; 
c     *** if 1) pixel is in heavy layer and
c     *** 2) pixel is in element then
c     *** compute shapefunction and add to vector
      do ipy=ipymin,ipymax
         yp = (ipy-0.5)*dypix
         do ipx=ipxmin,ipxmax
           xp = (ipx-0.5)*dxpix
           ip = (ipy-1)*nxpix + ipx
c          if (pefirst.eq.0) write(6,*) 'xp,yp: ',xp,yp,ip,ipix(ip)
           if (ipix(ip).eq.1) then
c             *** we are in the compositionally distinct layer
c             *** add buoyancy contribution
              call findrl(xn,yn,xp,yp,rl,isub,nodno,nodlin)
c          if (pefirst.eq.0) write(6,*) '   : ',xp,yp,rl(1),rl(2),rl(3)
              out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.
     v            (rl(3).ge.-eps)
              if (out) then
c                if (pefirst.eq.0) write(6,*) 'in triangle: ',ipx,ipy
c                *** calculate the shape function in (xp,yp)
                 call detshape7(xn,yn,xp,yp,shapef)
                 do i=1,7
                    f(i) = f(i) - Rb_local*shapef(i)*dxpix*dypix
                 enddo
              endif
           endif
         enddo
      enddo

c     if (pefirst.eq.0) then
c         pefirst=1
c         write(6,*) 'pel3003: '
c         write(6,*) 'ipxmin: ',ipxmin,ipxmax
c         write(6,*) 'ipymin: ',ipymin,ipymax
c         write(6,*) 'f: ',(f(i),i=1,7)
c     endif
          
            
      return
      end

cc     subroutine findrl(xn,yn,xp,yp,rl)
c      implicit none
c      real*8 xn(*),yn(*),xp,yp,rl(3)
c      real*8 xl(3),yl(3),a(3),b(3),c(3)
c      integer i
c 
c      do i=1,3
c         xl(i) = xn(2*i-1)
c         yl(i) = yn(2*i-1)
c      enddo
c      call trilin(xl,yl,a,b,c)
c      rl(1) = a(1) + b(1)*xp + c(1)*yp
c      rl(2) = a(2) + b(2)*xp + c(2)*yp
c      rl(3) = a(3) + b(3)*xp + c(3)*yp
c
c      return
c      end
