c *************************************************************
c *   PEDETEL
c *
c *   Determine in which element a point with given coordinates
c *   lies.
c *   ICHOIS
c *       1     Mesh is rectangular, coordinates of the primary
c *             nodal points are given by xc(nx) and yc(ny) 
c *             The element are triangles where the diagonal of
c *             the rectangle has an angle with the positive 
c *             x-axis larger then 90 degrees.
c *
c *       2     Mesh is rectangular, coordinates of the primary
c *             nodal points are given by xc(nx) and yc(ny)
c *             The elements are rectangles.
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine pedetel(ichois,xm,ym,iel)
      implicit none
      integer ichois,iel
      real*8 xm,ym,ydia
      include 'pexcyc.inc'
      integer ix,iy,iel2
      real*8 x1,x2,y1,y2
  
c     write(6,*) 'in pedetel: '
c     write(6,*) 'xc: ',xc(1),xc(2),xc(3),xcmin,xcmax
c     write(6,*) 'yc: ',yc(1),yc(2),yc(3),ycmin,ycmax


c     *** Find (ix,iy) such that  xc(ix-1) <= xm < xc(ix)
c     ***                    and  yc(iy-1) <= ym < yc(iy)
      ix = 1
100   continue
         ix = ix + 1
         if (xc(ix-1).le.xm.and.xm.le.xc(ix)) then
            continue
         else
            if (ix.ge.nx) then
               continue
            else
               goto 100
            endif
         endif
c     write(6,*) 'ix, xm = ',ix,xc(ix-1),xm,xc(ix)
 
      iy = 1
200   continue
         iy = iy + 1
         if (yc(iy-1).le.ym.and.ym.le.yc(iy)) then
            continue
         else
            if (iy.ge.ny) then
               continue
            else
               goto 200
            endif
         endif
c     write(6,*) 'iy, ym = ',iy,yc(iy-1),ym,yc(iy)

      if (ichois.eq.2) then
	 iel = (nx-1)*(iy-2) + (ix-1)
	 return
      endif

c     *** The element number is given by iel2 or iel2+1
c     *** ix and iy are >= 2. The elements in the first row are
c     *** numbered from 1 .. 2*(nx-1), in the second row from
c     *** 2*(nx-1)+1 .. 4*(nx-1) etc.
      iel2 = 2*(nx-1)*(iy-2) + 2*(ix-1) - 1

c     write(6,*) 'iel2 = ',iel2

c     *** Determine position of marker with respect to diagonal.
c     *** Here it is assumed that the diagonal has a negative
c     *** angle with the positive x-axis.
      x1 = xc(ix-1)
      x2 = xc(ix)
      y1 = yc(iy-1)
      y2 = yc(iy)
      ydia = y2 + (xm-x1)/(x2-x1)*(y1-y2)
      if (ym.le.ydia) then
         iel = iel2 + 1
      else
         iel = iel2 
      endif
c     write(6,*) 'PEDETEL: xm,ym,ix,iy,iel2,iel'
c     write(6,'(2f10.3,4i10)') xm,ym,ix,iy,iel2,iel
      return
      end
