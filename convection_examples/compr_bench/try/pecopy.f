c *******************************************************************
c *   PECOPY
c *
c *   Adaption of pecopy/er0035 to allow for scaling a solutionvector
c *
c *   PvK 7-11-89
c *
c *******************************************************************
      subroutine pecopy(ichoice,isol,user,kmesh,kprob,istart,factor)
      implicit none
      integer ichoice,isol(*),kprob(*),kmesh(*),istart,indprf
      real*8 user(*),factor
      logical new
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
 
      integer iprob,iindprh,iisol,npoint,ipkprf,nunkp,ipusol
      integer inidgt,iniget

      if (ichoice.eq.1) then
         write(6,*) 'PERROR(pecopy): option 1 not available'
         call instop
      endif
      if (ichoice.gt.4) then
         write(6,*) 'PERROR(pecopy): options > 4 not available' 
         call instop
      endif

      indprf = kprob(19)
      if (indprf.ne.0) then
         call ini070(indprf)
         ipkprf = iniget(indprf)
      endif
      
      call ini070(isol(1))
      ipusol   = inidgt(isol(1))
      npoint = kmesh(8)
      nunkp   = kprob(4)
      call pecop1(ichoice,npoint,buffr(ipusol),
     v          user(istart),indprf,ibuffr(ipkprf),factor,nunkp) 
      end

      subroutine pecop1(ichoice,npoint,usol,user,
     v                  indprf,kprobf,factor,nunkp)
      implicit none
      integer ichoice,npoint,nunkp,indprf,kprobf(*)
      real*8 usol(*),user(*),factor
      integer i,j1

      if (factor.eq.0d0) factor=1d0

      if (ichoice.eq.0) then

          do i=1,npoint
             user(i)=factor*usol(i)
          enddo

      else if(ichoice.eq.2.or.ichoice.eq.3.or.ichoice.eq.4) then

          if (indprf.eq.0) then
             do i=1,npoint
                j1 = (i-1)*nunkp + (ichoice-1)
                user(i)=factor*usol(j1)
             enddo
          else
             do i=1,npoint
                j1 = kprobf(i) + ichoice-1
                user(i) = factor*usol(j1)
             enddo
          endif

      endif

      end
