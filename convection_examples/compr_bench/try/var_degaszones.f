      subroutine var_degaszones(ichois)
      implicit none
      integer ichois
      include 'degas.inc'
      include 'SPcommon/ctimen'
      integer n,i,k
      real*8 pi,dth
      parameter(pi=3.1415926 535898)
      include 'ccc.inc'
    
      if (.not.half) then
       if (ichois.eq.0) then
c        *** initialize
         dth = thmaxdegas(1,1)-thmindegas(1,1)
         write(6,*) 'dth: ',dth,thmaxdegas(1,1),thmindegas(1,1)
         do k=1,4
           n = min(5,2+k)
           ndegaszone(k)=n
           do i=1,n
              thmindegas(i,k)=pi*0.25-0.5*dth+(i-1)*2*pi/n
              thmaxdegas(i,k)=pi*0.25+0.5*dth+(i-1)*2*pi/n 
           enddo
           write(6,'(''Initialize for case '',i3)') k
           do i=1,n
              write(6,'(''  Degassing zone: '',2f8.3,2x,2f8.3)') 
     v          thmindegas(i,k),thmaxdegas(i,k),
     v          360./(2*pi)*thmindegas(i,k),360./(2*pi)*thmaxdegas(i,k) 
           enddo
         enddo
       else if (ichois.eq.1) then
         if (t*tscale.lt.1) then
            n=5
         else if (t*tscale.ge.1.and.t*tscale.lt.3) then 
c           *** change to four degassing zones
            n=4
         else if (t*tscale.ge.3) then
c           *** change to three 
            n=3
         endif
         ndegaszone(1)=n
         do i=1,n
            thmindegas(i,4)=pi*0.25-0.5*dth+(i-1)*2*pi/n
            thmindegas(i,4)=pi*0.25+0.5*dth+(i-1)*2*pi/n 
         enddo
       endif

      else

c        *** HALF CYLINDER GEOMETRY
       if (ichois.eq.0) then
c        *** initialize
         dth = thmaxdegas(1,1)-thmindegas(1,1)
         write(6,*) 'dth: ',dth,thmaxdegas(1,1),thmindegas(1,1)
         do k=1,4
           n = min(4,k+1)
           ndegaszone(k)=n
           do i=1,n
              thmindegas(i,k)=pi*0.125-0.5*dth+(i-1)*pi/n
              thmaxdegas(i,k)=pi*0.125+0.5*dth+(i-1)*pi/n 
           enddo
           write(6,'(''Initialize for case '',i3)') k
           do i=1,n
              write(6,'(''  Degassing zone: '',2f8.3,2x,2f8.3)') 
     v          thmindegas(i,k),thmaxdegas(i,k),
     v          360./(2*pi)*thmindegas(i,k),360./(2*pi)*thmaxdegas(i,k) 
           enddo
         enddo
       else if (ichois.eq.1) then
         if (t*tscale.lt.1.5) then
            n=4
         else if (t*tscale.ge.1.5.and.t*tscale.lt.2.5) then 
c           *** change to four degassing zones
            n=3
         else if (t*tscale.ge.2.5) then
c           *** change to three 
            n=2
         endif
         ndegaszone(1)=n
         do i=1,n
            thmindegas(i,4)=pi*0.125-0.5*dth+(i-1)*pi/n
            thmindegas(i,4)=pi*0.125+0.5*dth+(i-1)*pi/n 
         enddo
       endif
      endif
       

      return
      end
       
