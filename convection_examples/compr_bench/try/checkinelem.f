c *************************************************************
c *   ICHECKINELEM
c *
c *   Check of point (xm,ym) is in the quadratic triangular
c *   element (xn,yn).  
c *   ichoice   i  1  the barycentric coordinates of the linear triangle 
c *                    made by the vertices should be positive
c *                2  test the barycentric coordinates of the 4 
c *                   linear subtriangles separately
c *                3  assume the element is curved; use Newton iteration
c *                   to obtain barycentric coordinates 
c *   xn,yn     i  Coordinates of the nodal points
c *   xm,ym     i  Coordinates of the tracer
c *   nodno     i  Nodal point numbers
c *   nodlin    i  nodal point numbers of linear sub element (if ichoice=2)
c *   rl        o  barycentric coordinates 
c *   xi,eta    o  barycentric coordinates
c *   phiq      o  quadratic shapefunctions 
c *   isub      o  number of linear sub element (if ichoice=2)
c *
c *   PvK 950508
c *
c *   PvK 970413: modified for cylindrical coordinates. Make sure
c *               to test each of the 4 subtriangles individually
c *   PvK 021004: modified to allow for curved elements
c *************************************************************
      logical function checkinelem(ichoice,xn,yn,xm,ym,
     v             nodno,nodlin,rl,xi,eta,phiq,isub) 
      implicit none
      real*8 xn(6),yn(6),xm,ym,rl(3),xi,eta,phiq(6)
      integer ichoice,isub,nodno(6),nodlin(3)
      real*8 eps
      parameter(eps=1d-6)
      logical out,fail

      if (ichoice.eq.1) then

c        *** check just the barycentric coordinates
         isub=0
         call findrl(xn,yn,xm,ym,rl,isub,nodno,nodlin)
         out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)  
         if (out) then
            checkinelem=.true.
         else
            checkinelem=.false.
         endif
         xi = rl(1)
         eta = rl(2)
         return

      else if (ichoice.eq.2) then          

c        *** check each four of the sub triangles separately
         isub=1
100      continue
           call findrl(xn,yn,xm,ym,rl,isub,nodno,nodlin)
           out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
           if (out) then
               checkinelem=.true.
               xi = rl(1)
               eta = rl(2)
               return
            else if (isub.lt.4) then
               isub=isub+1
               goto 100
            else
               checkinelem=.false.
               return
            endif

      else if (ichoice.eq.3) then

c           *** find local coordinates in reference triangle
c           *** with corners (0,0) (1,0) (0,1)
c           call findrl(xn,yn,xm,ym,rl,isub,nodno,nodlin)
c           xi = rl(1)
c           eta = rl(2)
            call find_xi_eta(xn,yn,xm,ym,xi,eta,phiq,fail)
            if (fail) then 
               checkinelem = .false.
            else if (xi.ge.-eps.and.eta.ge.-eps.and.
     v       (xi+eta).le.1+eps) then 
               checkinelem = .true.
            else
               checkinelem = .false.
            endif
            return
      else 
 
          write(6,*) 'PERROR(checkinelem): unkwown option for ichoice ' 
          write(6,*) 'ichoice = ',ichoice
          call instop
      endif

      return
      end

      subroutine findrl(xn,yn,xm,ym,rl,isub,nodno,nodlin)
      implicit none
      real*8 xn(6),yn(6),xm,ym,rl(3)
      real*8 a(3),b(3),c(3),xl(3),yl(3)
      integer isub,i,nodno(*),nodlin(*)
      integer inod(4,3)
      data (inod(1,i),i=1,3)/1,2,6/
      data (inod(2,i),i=1,3)/2,3,4/
      data (inod(3,i),i=1,3)/2,4,6/
      data (inod(4,i),i=1,3)/4,5,6/

      if (isub.eq.0) then
c        *** Use vertices of quadratic element
         xl(1) = xn(1)
         yl(1) = yn(1)
         xl(2) = xn(3)
         yl(2) = yn(3)
         xl(3) = xn(5)
         yl(3) = yn(5)
      else
c        *** use the ISUBth linear subtriangle
         xl(1) = xn(inod(isub,1))
         yl(1) = yn(inod(isub,1))
         xl(2) = xn(inod(isub,2))
         yl(2) = yn(inod(isub,2))
         xl(3) = xn(inod(isub,3))
         yl(3) = yn(inod(isub,3))
      endif

      call trilin(xl,yl,a,b,c)

      rl(1) = a(1) + b(1)*xm + c(1)*ym
      rl(2) = a(2) + b(2)*xm + c(2)*ym
      rl(3) = a(3) + b(3)*xm + c(3)*ym

      if (isub.eq.0) then
         nodlin(1) = nodno(1)
         nodlin(2) = nodno(3)
         nodlin(3) = nodno(5)
      else
         nodlin(1) = nodno(inod(isub,1))
         nodlin(2) = nodno(inod(isub,2))
         nodlin(3) = nodno(inod(isub,3))
      endif

      return
      end
