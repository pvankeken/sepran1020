c *************************************************************
c *   FUNC
c *
c *   PvK 070900
c *************************************************************
      real*8 function func(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z

      include 'ccc.inc'
      real*8 rerf
      real*8 z_here
      include 'coolcore.inc'
      integer iday,imonth,iyear,iarray(3)
      real*4 rand
      integer ifirst

      real*8 ampini,argsin,theta,r,dr,funccf,sint
      real*8 xp,yp,vx,vy
      real*8 pi,psimax,delta,rlamtemp,pert
      parameter(pi=3.1415926 5358979323846,psimax=250d0/(pi*pi))
      real*8 theta0,cost0,sint0,sl
      parameter(theta0=pi*0.25,cost0=0.707106781d0,sint0=cost0)
      integer ntheta
      include 'cpephase.inc'
      save ifirst
      data ifirst/0/


      if (ichois.eq.1) then
c        *** thermal perturbation
         ntheta=2
         r = sqrt(x*x+y*y)
         dr = (r-R1)/(R2-R1)
         theta = asin(y/r)
         argsin = pi*(1-dr)
         func = 0.10*sin(argsin)*cos(theta*ntheta)

      else if (ichois.eq.3) then

c        *** density 
         func = funccf(3,x,y,z)

      else if (ichois.eq.4) then

c        *** horizontal velocity from Appendix C of Van Keken et al., JGR, 1997
         xp = x - 0.4192
         yp = y + 0.5
         func = psimax * sin (pi*xp) * cos (pi*yp)
         if (quart) then
c           *** rotate velocity field 45 degrees clockwise
            xp =  sint0*x + cost0*y
            yp = -cost0*x + sint0*y
            xp = xp - 0.4192
            yp = yp  + 0.5
            vx = psimax * sin (pi*xp) * cos (pi*yp)
            vy = - psimax * cos (pi*xp) * sin (pi*yp)
            func = vx*sint0-vy*cost0
         endif

      else if (ichois.eq.5) then
c        *** vertical velocity from Appendix C of Van Keken et al., JGR, 1997
         xp = x - 0.4192
         yp = y + 0.5
         func = - psimax * cos (pi*xp) * sin (pi*yp)
         if (quart) then
c           *** rotate velocity field 45 degrees clockwise
            xp =  sint0*x + cost0*y
            yp = -cost0*x + sint0*y
            xp = xp - 0.4192
            yp = yp  + 0.5
            vx = psimax * sin (pi*xp) * cos (pi*yp)
            vy = - psimax * cos (pi*xp) * sin (pi*yp)
            func = vx*cost0 + vy*sint0
         endif

      else if (ichois.eq.10) then
 
c        *** quadratic function to test interpolation accuracy
         func = x*x + y*y
         return
 
      
      else if (ichois.eq.11) then

c        **** Error function profile. Halfspace cooled for
c        **** 100 Myr. T(z) = Tm*erf(y/(2*sqrt(kappa*t)).
c        **** for t=100 Myr, 2*sqrt(kappa*t)=112 km. 2800/112=25


         r = sqrt(x*x+y*y)
         dr = (r-R1)/(R2-R1)
         theta = pi/2.-asin(y/r)
c         argsin = pi*(1-dr)



c     *** Boundary layer thickness
c      delta = 1.7*(Ra)**(-0.333333333)
c ra=2.1d6, ~ 130 km 
c      delta = 5.8*(Ra)**(-0.333333333)
      delta = 130d0/2885d0 
   

c     ***
      func = 1-rerf(dr/delta)
      rlamtemp = pi/8.
      ampini=0.5

c     *** perturb it
c      if (iprtrb.ge.1) then
         pert=0.
         if(theta.le.pi/16.) then
         pert = ampini*cos(pi*theta/rlamtemp)
     v          *max(0d0,1-0.5*dr/delta)
         end if
         func = min(1d0,func + pert)
c         if (theta.le.pi/8.)
c          if(pert.ne.0)
c     v    write(6,'(''F: '',5f12.5)')theta,dr,func,pert,theta/rlamtemp
c           write(6,'(''F2:'',5f12.5)')
c    v      x,y,1-rerf(y/delta),pert,func
c      endif


c         r = sqrt(x*x+y*y)
c         sint = y/r
c         theta = asin(sint)
c         z_here = r2-r
c         ampini=0.05
c         if (z_here.le.0.4) then
c            func = t_bot*(rerf(30d0*z_here))
c            func = min(func,t_bot)
c            func = max(func,0d0)
c         else 
c            func = t_bot
c         endif

      else

         write(6,*) 'PERROR(func): unknown option: ',ichois
         call instop

      endif
  
      return
      end

c     *** Compute error function (from Numerical Recipes)
      real*8 function rerf(x)
      implicit none
      real*8 x
      integer iret
      real*8 delx,x0,x1,xx

      if (x.gt.4.) then
            rerf=1.
            goto 20
      endif
         iret=0
         rerf=0.
         delx=0.0001
         x0=0.
10       x1=x0+delx
         if(x1.gt.x) then
            x1=x
            iret=1
         endif
         xx=(x0+x1)/2.
         rerf=rerf+dexp(-xx*xx)*(x1-x0)
         if(iret.eq.1) then
            rerf=rerf*2./dsqrt(3.1415926535d0)
            goto 20
         endif
         x0=x1
         goto 10
c
20     return
         end






