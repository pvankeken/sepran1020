      subroutine to0061 ( iinvec, invec1, invec2, kmesh, kprob, iconst,
     +                    ichois, idegfd, iskip )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.9  date 19-03-2007 Extension for mapping to type 116
!        version  2.8  date 30-10-2002 Extension of iinvec
!        version  2.7  date 21-09-2002 Extra options 47/48/49
!        version  2.6  date 26-09-2001 Extra options 44/45/46
!        version  2.5  date 12-11-2000 Extra option function of vector
!        version  2.4  date 28-02-2000 Extension with type 127
!        version  2.3  date 19-12-1999 Extra option limit
!        version  2.2  date 08-01-1999 Adapt defaults to manual
!        version  2.1  date 21-09-1998 Include commons
!        version  2.0  date 10-04-1995 Extension with product of two vectors,
!                                      removal of invec2
!        version  1.10 date 17-07-1994 New definition of invec(3)
!
!   copyright (c) 1990-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Put input of subroutine MANVEC into the arrays ICONST and CONST
!     Compute some input parameters
!     Check input
! **********************************************************************
!
!                       KEYWORDS
!
!     manipulation
!     vector
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iinvec(*), invec1(*), kmesh(*), kprob(*), invec2(*),
     +        iconst(*), ichois, idegfd(*), iskip

!   ichois    o   Choice parameter for subroutine TO0035. ICHOIS may be
!                 computed from IINVEC(2), but the sequence is not exactly
!                 the same as in IINVEC(2)
!                 Possible values:
!                   1:    Subtract:  VEC3 = VEC1 - CONST(1)
!                   2:    CONST(1) is average value of VEC1
!                         At output IPOINT is number of points
!                   3:    Linear combination
!                         VEC3 = CONST(1) * VEC1 + CONST(2) * VEC2
!                   4:    VEC1(IDEGFD) in IPOINT is stored in CONST(1)
!                   5:    VEC1(IDEGFD) in IPOINT = CONST(1)
!                   6:    The minimum value of VEC1 is stored in CONST(1)
!                         The maximum value of VEC1 is stored in CONST(2)
!                         Real vectors only
!                   7:    The inner product VEC1 + VEC2 is stored in CONST(1)
!                         The number of points used is stored in IPOINT
!                   8:    VEC3 = CONST(1) * |VEC1|
!                         VEC1 is complex, VEC3 is real
!                   9:    VEC3 = CONST(1) * arg( VEC1 )
!                         VEC1 is complex, VEC3 is real
!                   10:   The inner product VEC1 + VEC2 is stored in CONST(1)
!                         The number of points used is stored in IPOINT
!                         Boundary conditions are excluded
!                   11:   The minimum and maximum values of the array
!                         represented by IVEC1 are computed.
!                         Besides that the co-ordinates for which these
!                         values are reached are stored in const
!                         Restriction: Only one unknown may be considered
!                                      per point
!                         At output CONST(1) contains the minimum, CONST(2)
!                         the maximum value, CONST(3) - (5), contain the
!                         co-ordinates for which the minimum is reached and
!                         CONST(6) - (8), contain the co-ordinates for which
!                         the maximum is reached. Real vectors only.
!                   12:   See 11, however, the minimum and maximum values of the
!                         absolute value of the array are used
!                         (Real and complex)
!                   13:   VEC3 is a function of VEC1 and VEC2. The user
!                         subroutine FUNALG or FUNALC is called.
!                   14:   VEC3(i) = sqrt ( VEC1(idegfd(1) **2 +
!                                                        i
!                                          VEC1(idegfd(2) **2  )
!                                                        i
!                   15:   CONST(1) = || VEC1 - VEC 2 ||
!                         The type of norm must be given in ICONST(1).
!                         Possible values:
!                         1       l1-norm
!                         2       l2-norm
!                         3       max-norm
!                         4       l1-norm / number of degrees of freedom
!                         5       l2-norm / number of degrees of freedom
!                        10       max-norm  the nodal point number
!                                 corresponding to the maximum is stored in
!                                 iconst(2)
!                   16:   CONST(1) = || VEC1 ||
!                   17:   User defined subroutine of n SEPRAN vectors
!                         n must be stored in ICONST(1)
!                         For the type of norms available: see ICHOIS = 15
!                   18:   VEC3 = conjg(VEC1)
!                   19:   VEC3 is a function of VEC1 and VEC2. The SEPRAN
!                         subroutine FUNADA is called.
!                   20:   VEC3 is a map of VEC1 according to a new structure
!                         The number of unknowns and nodal points for VEC1 and
!                         VEC3 are identical
!                   21:   VEC3 is a vector of one unknown in each point
!                         The IDEGFD-th degree of freedom from VEC1 is
!                         used as input
!                         It is supposed that this degree of freedom is
!                         available in each point
!                   22:   VEC3 is Real part of VEC1 multiplied by CONST(1)
!                   23:   VEC3 is Imaginary part of VEC1 multiplied by CONST(1)
!                   24:   Linear combination
!                         VEC3 = CONST(1) * VEC1 + CONST(2) * VEC2
!                         with constraint: vec3(i)>=const(3)
!                   25:   Componentwise product of two vectors:
!                         VEC3(i) = CONST(1) VEC1(i) VEC2(i)
!                   26:   Point-wise inner-product of two vectors:
!                         u out(i) = sum_j u1(i)_j + u2(i)_j
!                   27:   Limit u1 between rinput(1) (minimum) and
!                         rinput(2) (maximum)
!                   43:   vec3 = function of vec1
!                         The type of functions is stored in iconst(1)
!                         Possible values:
!                         1: log
!                   44:   uout(i) is vector with 1 component in the
!                         points where vec1(i,.) is defined
!                         vec1 must be a vector with ndim components
!                         uout(i) = a . vec1(i)
!                         The vector a must be stored in rinvec(1..ndim)
!                         The type of output vector must be defined in
!                         ISPEC (0=solution vector, >0 vector of special
!                         structure)
!                   45:   uout(i) is vector with ndim components in the
!                         points where vec1(i) is defined
!                         vec1 must be a vector with 1 component
!                         uout(i) = vec1(i) a
!                         The vector a must be stored in rinvec(1..ndim)
!                         The type of output vector must be defined in
!                         ISPEC (0=solution vector, >0 vector of special
!                         structure)
!                   46:   uout = u - u.n n
!                         The input vector must be a vector with ndim
!                         components defined on a curve (2d) or a
!                         surface (3d) only
!                         The normal on this curve or surface is computed
!                         and the normal component of u in the n direction
!                         is subtracted from u, thus making u a vector
!                         in the curve or surface only.
!                         ISPEC must contain the curve or surface number
!                   47:   uout = u.n n
!                         See 46, however, in this case the normal velocity
!                         is computed
!                   48:   uout(i) is vector with ndim components in the
!                         points where vec1(i) is defined
!                         vec1 must be a vector with ndim components
!                         vec2 must be a vector
!                         uout(i) = vec1_idegfd(i) vec2_idegfd(i) e_idegfd
!                         The output vector has the same structure as
!                         vec1
!                   49:   uout(i) is vector with 1 component in the
!                         points where vec1(i,.) is defined
!                         vec1 and vec2 must be vectors with ndim components
!                         uout(i) = vec1_idegfd1(i) vec2_idegfd2(i)-
!                                   vec2_idegfd1(i) vec1_idegfd2(i)
!                         The type of output vector must be defined in
!                         ISPEC (0=solution vector, >0 vector of special
!                         structure)
!                   52    Map vector of type 110 or 115 onto vector of type
!                         116, i.e. map a vector defined per node onto
!                         a vector defined per element
!                         Line elements and connection elements are skipped
!                         At this moment the mapping is restricted to
!                         linear triangles only
!   iconst    o   Output integer array. ICONST is used as help array for
!                 subroutine TO0035. It must be filled according to the
!                 description in TO0035, which is an extension of the
!                 old SEPRAN subroutine VECMAN
!   idegfd    o   In this array given the degrees of freedom that are requested
!                 for the special purpose are stored.
!                 In most applications only one degree of freedom per point
!                 is required, and the array may be replaced by an integer
!                 variable but not by a constant.
!                 If the variable has value 0, then all degrees of freedom
!                 are considered.
!                 If the variable has value -1, then all degrees of freedom
!                 are considered, except those corresponding to essential
!                 boundary conditions
!                 The length of the array depends on ichois:
!                 If ichois = 1-13:  Length 1
!                               14:         2 or 3 (ndim=iinvec(5))
!                            15-16:         1
!   iinvec    i   In this integer input array it is indicated what type of
!                 manipulations must be preformed by subroutine MANVEC.
!                 For some cases IINVEC acts both as input and
!                 as output array.
!                 For a description of iinvec, see subroutine MANVECBF
!   invec1    i   Integer SEPRAN array of length 5 corresponding to a solution
!                 vector or a vector of special structure.
!                 For some possibilities of ICHOIS, u1 is not only an input
!                 vector but may be also output vector.
!                 In some cases INVEC1 corresponds to more than one vector
!                 ( u1 , u2 , + . + , u n  )
!                 In that case INVEC1 is supposed to be a two-dimensional
!                 array of size  5 x alpha  with  alpha>=n
!                 INVEC1(1:5,1: alpha ).
!                 In INVEC1( i,j ), ( i =1, 2, .. , 5) information about
!                 the  j th input vector must be stored (1<=j<=n ).
!                 This may be done for example by copying the contents of
!                 other SEPRAN arrays into INVEC1.
!   invec2    i   Second integer SEPRAN array of length 5 corresponding to a
!                 solution vector or a vector of special structure.
!   iskip     o   See IINVEC(7)
!   kmesh     i   Standard SEPRAN array containing information of the mesh
!   kprob     i   Standard SEPRAN array containing information of the problem
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ihelp(11), i, itypvc, ipoint, ispec, ichnrm, ndim, ndtvec,
     +        iprob, nprob, ipkprb, nsurfs, iseq1, iseq2, minval
      logical compli, debug

!     compli         Indication if IVEC1 corresponds to a complex array (true)
!                    or to a real one (false)
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     i              Counting variable
!     ichnrm         Type of norm to be computed
!     ihelp          Help array in which the first 11 positions of IINVEC are
!                    copied and for those positions not filled, defaults are
!                    filled
!     ipkprb         Starting address of array KPROB for actual problem
!                    number - 1
!     ipoint         Nodal point number
!     iprob          Actual problem number
!     iseq1          Sequence number of first input vector in isol
!     iseq2          Sequence number of second input vector in isol
!     ispec          Extra information as stored in IINVEC(5)
!     itypvc         Indication of the type of solution vector:
!                    110: solution vector
!                    115, 116: vectors of special structure
!     minval         Minimum value of ivec for specific situation
!     ndim           Dimension of the space.
!     ndtvec         Number of vectors of special structure
!     nprob          Number of various problems
!     nsurfs         number of surfaces
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS  Resets old name of previous subroutine of higher level
!     EROPEN  Produces concatenated name of local subroutine
!     ERRINT  Put integer in error message
!     ERRSUB  Error messages
!     TO0022  test if vectors are of equal type
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!       1   ICHOIS is incorrect
!      26   Array KPROB has not been filled
!      75   Array KMESH has not been filled
!     224   input vector (INVEC1 or IVEC1) is not complex
!     699   Problem number incorrect
!     803   Input vector (INVEC1 or IVEC1) is complex
!     855   IDEGFD = 0, whereas IINVEC(1) = 2 or 28 (ICHOIS)
!    1153   vector INVEC1 corresponds to a wrong type of vector,
!    1154   Array IVEC1 has type number 119, whereas ICHOIS = 15,
!           16 or ANORM/ DIFFVC has been called
!    1155   IDEGFD has wrong value in combination with type 119
!    1156   IINVEC(1) has incorrect value
!    1157   IINVEC(5) has incorrect value
!    1158   IINVEC(6) has incorrect value
!    1159   IDEGFD (IINVEC(3)) -1 not yet allowed for this value of ICHOIS
!    1160   IINVEC($$) has incorrect value
!    1740   The output vector to be created is a vector of special structure
!           is either less than 0, or too large
! **********************************************************************
!
!                       PSEUDO CODE
!
!   trivial
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0061' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0061'
         write(irefwr,1) 'ichois', ichois
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      itypvc = invec1(2)
      if ( itypvc/=110 .and. itypvc/=115 .and. itypvc/=116 .and.
     +     itypvc/=119 .and. itypvc/=127 ) then
         call errint ( itypvc, 1 )
         call errsub ( 1153, 1, 0, 0 )
      end if
      if ( debug ) write(irefwr,1) 'itypvc', itypvc

!     --- Check if VEC1 is of the correct type

      if ( mod(invec1(3),10)==0 ) then

         compli = .false.

      else

         compli = .true.

      end if

!     --- test input array

      if ( iinvec(1)<=1 .or. iinvec(1)>10000 ) then

!     --- Error 1156: IINVEC(1) incorrect

         call errint ( iinvec(1), 1 )
         call errsub ( 1156, 1, 0, 0 )

      end if

!     --- Set defaults

      ihelp(3)  = 0
      ihelp(4)  = 1
      ihelp(5)  = 0
      ihelp(6)  = 1
      ihelp(7)  = 0
      ihelp(8)  = 1
      ihelp(9)  = 2
      ihelp(10) = 3
      ihelp(11) = 1
      if ( iinvec(1)>1 ) then
         if ( iinvec(2)==33 ) ihelp(6) = 0
      end if

!     --- Copy IINVEC into IHELP

      do i = 3, min ( 11, iinvec(1) )
         if ( iinvec(i)/=0 ) ihelp(i) = iinvec(i)
      end do

      ichois = iinvec(2)
      if ( compli .and. ichois==3 ) then
         call errint ( ichois, 1 )
         call errsub ( 803, 1, 0, 0 )
      end if
      if ( .not. compli .and. ( ichois==29 .or. ichois==30
     +     .or. ichois==34 ) ) then

!     --- Error 224:  Array is not complex

         call errint ( ichois, 1 )
         call errsub ( 224, 1, 0, 0 )

      end if
      if ( debug ) write(irefwr,1) 'ichois', ichois

      idegfd(1) = ihelp(3)
      ipoint    = ihelp(4)
      ispec     = ihelp(5)
      ichnrm    = ihelp(6)
      iskip     = ihelp(7)
      iprob     = ihelp(11)

      if ( kprob(2)/=101 ) then
         call errint ( kprob(2), 1 )
         call errsub ( 26, 1, 0, 0 )
      end if
      nprob  = max ( 1, kprob(40) )
      if ( iprob<1 .or. iprob>nprob )  then
         call errint ( iprob, 1 )
         call errint ( nprob, 2 )
         call errsub ( 699, 2, 0, 0)
      end if
      ipkprb = 1
      do i = 2, iprob
         ipkprb = ipkprb + kprob ( 2+ipkprb )
      end do

!     --- Translate ICHOIS to values corresponding to TO0035

      if ( ichois==1 ) then

!     --- average value

         ichois = 2

      else if ( ichois==2 ) then

!     --- extract value

         ichois = 4
         iconst(1) = ipoint
         iconst(2) = ispec

      else if ( ichois==3 ) then

!     --- minimum and maximum value

         if ( ispec<0 .or. ispec>2 ) then

!        --- Error 1157: ISPEC has wrong value

            call errint ( ispec, 1 )
            call errint ( ichois, 2 )
            call errint ( 0, 3 )
            call errint ( 2, 4 )
            call errsub ( 1157, 4, 0, 0 )

         end if
         if ( ispec==0 ) then
            ichois = 6
         else
            ichois = 10+ispec
         end if

      else if ( ichois==4 ) then

!     --- inner product

         if ( ispec<-1 .or. ispec>0 ) then

!        --- Error 1157: ISPEC has wrong value

            call errint ( ispec, 1 )
            call errint ( ichois, 2 )
            call errint ( -1, 3 )
            call errint ( 0, 4 )
            call errsub ( 1157, 4, 0, 0 )

         end if
         if ( ispec==-1 ) then
            ichois = 10
         else
            ichois = 7
         end if

      else if ( ichois==5 ) then

!     --- Norm of vector

         if ( ichnrm<=0 .or. ichnrm>5 .and. ichnrm/=10 ) then

!        --- Error 1158: ICHNRM has wrong value

            call errint ( ichnrm, 1 )
            call errsub ( 1158, 1, 0, 0 )

         end if
         if ( ispec<0 .or. ispec>1 ) then

!        --- Error 1157: ISPEC has wrong value

            call errint ( ispec, 1 )
            call errint ( ichois, 2 )
            call errint ( 0, 3 )
            call errint ( 1, 4 )
            call errsub ( 1157, 4, 0, 0 )

         end if
         if ( itypvc==119 )
     +      call errsub ( 1154, 0, 0, 0 )
         if ( idegfd(1)==-1 ) then

!        --- Error 1159, idegfd = -1 not yet available

            call errint ( ichois, 1 )
            call errsub ( 1159, 1, 0, 0 )

         end if
         ichois = 16-ispec
         iconst(1) = ichnrm

      else if ( ichois==26 ) then

!     --- subtract constant

         ichois = 1

      else if ( ichois==27 ) then

!     --- Linear combination

         ichois = 3

      else if ( ichois==28 ) then

!     --- Put value in point

         ichois = 5
         iconst(1) = ipoint

      else if ( ichois==29 ) then

!     --- Modulus of complex vector

         ichois = 8

      else if ( ichois==30 ) then

!     --- Phase of complex vector

         ichois = 9
         iconst(1) = ihelp(5)

      else if ( ichois==31 ) then

!     --- User defined function

         ichois = 13

      else if ( ichois==32 ) then

!     --- User defined function

         ichois = 17
         iconst(1) = ispec
         if ( ispec<=0 .or. ispec>10000 ) then

!        --- Error 1157: ISPEC has wrong value

            call errint ( ispec, 1 )
            call errint ( ichois, 2 )
            call errint ( 1,3  )
            call errint ( 10000, 4 )
            call errsub ( 1157, 4, 0, 0 )

         end if

      else if ( ichois==33 ) then

!     --- Length of vector

         ichois = 14
         if ( ispec<0 .or. ispec>3 ) then

!        --- Error 1157: ISPEC has wrong value

            call errint ( ispec, 1 )
            call errint ( ichois, 2 )
            call errint ( 0, 3 )
            call errint ( 3, 4 )
            call errsub ( 1157, 4, 0, 0 )

         end if
         if ( ispec==0 ) then
            ndim = kmesh(6)
         else
            ndim = ispec
         end if
         iconst(1) = ndim
         iconst(2) = ihelp(6)
         do i = 1, ndim
            idegfd(i) = ihelp(7+i)
            if ( idegfd(i)<=0 .or. idegfd(i)>1000 ) then

!           --- Error 1160:  IDEGFD(i) incorrect

               call errint ( 7+i, 1 )
               call errint ( idegfd(i), 2 )
               call errsub ( 1160, 2, 0, 0 )

            end if

         end do

      else if ( ichois== 34 ) then

!     --- complex conjugate of vector

         ichois = 18

      else if ( ichois==35 ) then

!     --- SEPRAN fuction FUNADA for adaptive meshes

         ichois = 19

      else if ( ichois>=36 .and. ichois<=38 ) then

!     --- Extract one unknown, real part or imaginary part

         ichois = ichois-15
         if ( ichois==21 ) then

            if ( idegfd(1)<=0 .or. idegfd(1)>1000 ) then

!           --- Error 1160:  IDEGFD(1) incorrect

               call errint ( 7+i, 1 )
               call errint ( idegfd(i), 2 )
               call errsub ( 1160, 2, 0, 0 )

            end if

         end if

      else if ( ichois>=39 .and. ichois<=42 ) then

!     --- 39: Linear combination with constraint
!         40: Product of two vectors
!         41: Point-wise inner product of two vectors
!         42: limit vector

         ichois = ichois - 15

      else if ( ichois==43 ) then

!     --- 43: function of vector

         iconst(1) = ispec
         if ( ispec<1 .or. ispec>1 ) then
            call errint ( ispec, 1 )
            call errint ( 1, 2 )
            call errint ( 1, 3 )
            call errsub ( 1740, 3, 0, 0 )
         end if

      else if ( ichois==44 .or. ichois==45 .or. ichois==51 ) then

!     --- 44/45: contract or expand vector pointwise
!         51: Map one SEPRAN array onto a new structure

         if ( ichois==51 ) then
            minval = -99    ! in case of mapping negative values are allowed
            ichois = 20     ! output value
         else
            minval = 0
         end if  ! ( ichois==51 )

         ndtvec = kprob(ipkprb+5)

         if ( ispec<minval .or. ispec>ndtvec ) then
            call errint ( ispec, 1 )
            call errint ( ndtvec, 2 )
            call errint ( iprob, 3 )
            call errsub ( 1740, 3, 0, 0 )
         end if

         if ( itypvc/=110 .and. itypvc/=115 ) then
            call errint ( itypvc, 1 )
            call errsub ( 1153, 1, 0, 0 )
         end if
         iconst(1) = ispec
         iconst(2) = iprob

      else if ( ichois>=46 .and. ichois<=47 ) then

!     --- 46: Remove normal component

         iconst(1) = ispec
         nsurfs = kmesh(12)
         if ( ispec<1 .or. ispec>nsurfs ) then
            call errint ( ispec, 1 )
            call errint ( 1, 2 )
            call errint ( nsurfs, 3 )
            call errsub ( 1740, 3, 0, 0 )
         end if

      else if ( ichois==48 ) then

!     --- 48: Special vector

         iconst(1) = idegfd(1)

      else if ( ichois==49 ) then

!     --- 49: Special vector

         iconst(1) = ihelp(8)
         iconst(2) = ihelp(9)
         iconst(3) = ispec
         iconst(4) = iprob

      else if ( ichois==52 ) then

!     --- 52: Map one SEPRAN array onto a new structure defined per element

         if ( itypvc/=110 .and. itypvc/=115 ) then
            call errint ( itypvc, 1 )
            call errsub ( 1153, 1, 0, 0 )
         end if

      else

!     --- Error 1: ICHOIS has wrong value

         call errint ( ichois, 1 )
         call errsub ( 1, 1, 0, 0 )

      end if

      if ( ( idegfd(1)/=0 .or. ichois==11 .or. ichois==12 ) .and.
     +     kmesh(2)/=100 ) then
         call errint ( kmesh(2), 1 )
         call errsub ( 75, 1, 0, 0 )
      end if
      if ( idegfd(1)==0 .and. (ichois==5 .or. ichois==4 ) )
     +     call errsub ( 855, 0, 0, 0 )
      if ( itypvc==119 ) then

!     --- itypvc = 119

         if ( idegfd(1)/=0 .and. idegfd(1)/=1 ) then
            call errint ( idegfd(1), 1 )
            call errsub ( 1155, 1, 0, 0 )
         end if

      end if

      if ( ichois==3  .or. ichois==7 .or. ichois==10 .or.
     +     ichois==13 .or. ichois==15 .or. ichois==29 .or.
     +     ichois>=24 .and. ichois<=26 ) then

!     --- ichois = 3, 7, 10, 13 or 15; hence array invec2 is used
!         Check if arrays invec1 and invec2 have the same structure

         iseq1 = 1
         iseq2 = 2
         if ( iinvec(1)>=12 ) iseq1 = iinvec(12)
         if ( iinvec(1)>=13 ) iseq2 = iinvec(13)
         call to0022 ( invec1, invec2, iseq1, iseq2 )

      end if

1000  call erclos ( 'to0061' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0061'

      end if  ! ( debug )

      end
