c *************************************************************
c *   intermediate_output
c * 
c *   Intermediate output for program BM
c *
c *   Output of iteration number, accuracy, Nusselt number
c *   rms velocity, local temperature gradients and cpu time
c *
c *   PvK 120490
c *************************************************************
      subroutine intermediate_output(kmesh1,kmesh2,kprob1,kprob2,
     v         isol1,isol2,islol1,islol2,iheat,idens,ivisc,
     v         isecinv,iuser,user)
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),islol1(*),islol2(*),iuser(*)
      integer idens(*),iheat(*),ivisc(*),isecinv(*)
      real*8 user(*)
      integer ibuffr(1)
      common ibuffr
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))

      include 'pesteady.inc'
      include 'pecpu.inc'
      include 'peparam.inc'
      include 'cpephase.inc'
      include 'vislo.inc'
      include 'pexcyc.inc'
      include 'c1visc.inc'
      include 'pecof900.inc'
      include 'depth_thermodyn.inc'
      include 'ccc.inc'
      integer NFUNC
      parameter(NFUNC=505)
      integer NUM
      parameter(NUM=20 000)
      integer igradt(5),icurvs(3)
      real*8 funcx(NFUNC),funcy(NFUNC)
      integer ivser(100)
      real*8 vser(NUM),smax,tmax,volint,vrms1,vrms2
      real*8 bounin,gnus,temp1,q1,q2,vrms,anorm,funccf,x,z
      integer nunkp,irule,ihelp,ip,ielhlp,iphi(5),ivisdip(5) 
      integer ifirst,iqtype,ikelmi,npoint,ichois,icheld,ix
      integer ipoint,inputcr(10),jdegfd,ivec,nq_a
      integer ih,im,is,inidgt
      real*8 q_a(20),t_bot,q_a_d(20)
      real*8 phiav,heatdummy,rinputcr(10),rkap1,rkap2,y
      real*4 second
      real*8 DONE,pi
      parameter(DONE=1d0,pi=3.1415926 535898)

      save iphi,ifirst,ivisdip

      data ivser(1),vser(1),funcx(1),funcy(1)/100,NUM,2*NFUNC/
      data ifirst/0/

      t2 = second()
      dcpu = t2-t1
      cput = t2-t00
      t1   = t2

      dif1 = anorm(0,3,0,kmesh1,kprob1,isol1,islol1,ielhlp)
      dif2 = anorm(0,3,0,kmesh2,kprob2,isol2,islol2,ielhlp)
      smax = anorm(1,3,0,kmesh1,kprob1,isol1,isol1,ielhlp)
      tmax = anorm(1,3,0,kmesh2,kprob2,isol2,isol2,ielhlp)
      dif1 = dif1/smax
      dif2 = dif2/tmax
      dif  = dif2

      call pevrms9(isol1,kmesh1,kprob1,iuser,user)
c     write(6,*) 'intermediate_output/volint: ',iuser(1),user(1)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
      vrms = sqrt(abs(vrms2))


      call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
      gnus = q_a(1)
      t_bot = q_a(3)
      q2 = q_a(5)
      q1 = q_a(6)

      if (Di.gt.0d0.or.itypv.ne.0) then
c       write(6,*) 'intermediate output : ',Di,itypv
        call phifromgrad(kmesh1,kprob1,isol1,ivisdip)
        if (itypv.ne.0) then
           call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                  ivisc,isecinv,iuser,user)
        endif
        npoint=kmesh1(8)
        call ini050(kmesh1(23),'intermediate output: coor')
        ikelmi = inidgt(kmesh1(23))

c       *** calculate average viscous dissipation 
        heatdummy = 0d0
        iqtype=0
        call pecophi(ivisdip,user,6,npoint,DONE,
     v         iqtype,buffr(ikelmi),ivisc,iheat)
        iuser(6) = 7
        iuser(7) = intrule900
        iuser(8) = icoor900
        iuser(10) = 2001
        iuser(11) = 6
c       write(6,*) 'intermed,phiav: ',iuser(1),user(1)
        phiav = volint(0,1,1,kmesh1,kprob1,isol1,iuser,user,ielhlp)
        phiav = phiav * Di/Ra / volume
        ifirst=1
      endif


      ih = cput/3600
      im = (cput - ih*3600)/60
      is = cput - ih*3600 -im*60
      write(6,11) niter,dif,gnus,vrms,phiav,t_bot,dcpu,ih,im,is
11    format(i4,f12.8,e15.7,e15.4,e15.7,2e10.2,i5,':',i2.2,':',i2.2)
c     call flush(6)

      return
      end
