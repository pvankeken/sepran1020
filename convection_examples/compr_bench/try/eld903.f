      subroutine eld903 ( coor, elemvc, iuser, user, index1, index3,
     +                    index4, vecold, islold, vecloc, coeffnodes,
     +                    coeffintpnts, phi, psi, phixi, psixi )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.9  date 03-02-2010 New call to el4917
!        version  3.8  date 19-04-2007 New call to el0903
!        version  3.7  date 03-01-2007 New call to el0903
!        version  3.6  date 04-09-2006 Debug statements
!
!   copyright (c) 1997-2010  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element derivative vector and weights for Navier-Stokes
!     equations
!     Two and three dimensional elements
!     ELD903 is a help subroutine for subroutine DERIV (TO0047)
!     it is called through the intermediate subroutine eldns2
!     So:
!     DERIV
!     TO0047
!       -  Loop over elements
!          -  ELD000
!             - ELD903
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  4 node bilinear quadilateral
!         5 node bilinear quadilateral
!         6 node extended triangle
!         7 node extended triangle
!         9 node biquadratic quadilateral
!    3D: 27 node brick
! **********************************************************************
!
!                       KEYWORDS
!
!     derivative
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/cactl'
      include 'SPcommon/celiar'
      include 'SPcommon/celint'
      include 'SPcommon/celp'
      include 'SPcommon/cinforel'
      include 'SPcommon/celwrk'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemvc(*), coor(*), user(*),
     +                 vecold(*), vecloc(*), psi(nvert,m),
     +                 phixi(n,m,ndim), psixi(nvert,m,ndim), phi(n,m),
     +                 coeffnodes(n,*), coeffintpnts(m,*)
      integer iuser(*), index1(*), index3(numold,*), index4(*),
     +        islold(5,*)

!     coeffintpnts  i/o   Real array in which the values of the
!                         coefficients in the integration points are stored
!     coeffnodes    i/o   Real array in which the values of the
!                         coefficients in the nodal points are stored
!     coor           i    array of length ndim x npoint containing the
!                         co-ordinates of the nodal points with respect to the
!                         global numbering
!     elemvc         0    Element vector to be computed,
!                         Length depends on application
!     index1         i    Array of length inpelm containing the point numbers
!                         of the nodal points in the element
!     index3         i    Two-dimensional integer array of length NUMOLD x
!                         NINDEX containing the positions of the "old"
!                         solutions in array
!                         VECOLD with respect to the present element
!                         For example VECOLD(INDEX3(i,j)) contains the j th
!                         unknown with respect to the i th old solution vector.
!                         The number i refers to the i th vector corresponding
!                         to IVCOLD in the call of SYSTM2 or DERIVA
!     index4         i    Two-dimensional integer array of length NUMOLD x
!                         INPELM containing the number of unknowns per point
!                         accumulated in array VECOLD with respect to the
!                         present element.
!                         For example INDEX4(i,1) contains the number of
!                         unknowns in the first point with respect to the
!                         i th vector stored in VECOLD.
!                         The number of unknowns in the j th point with respect
!                         to i th vector in VECOLD is equal to
!                         INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold         i    User input array in which the user puts information
!                         of all preceding solutions
!                         Integer array of length 5 x numold
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     phi           i/o   array of length n * m, values of shape
!                         functions in integration points in the sequence:
!                         phi_i(xg^k) = phi (i,k)
!                         array phi is only filled when gauss integration is
!                         applied
!     phixi         i/o   Array of length n x m x ndim containing the values of
!                         the derivatives of the basis functions in the Gauss
!                         points in the reference element.
!                         Array phi is only filled when ifirst = 0 and must be
!                         kept by the program
!     psi            i    Array of length n x m containing the values of the
!                         modified basis functions in the integration points
!     psixi         i/o   Array of length nvert x m x ndim containing the values
!                         of the derivatives of the basis functions in the Gauss
!                         points in the reference element
!     user           i    Real user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     vecloc         i    Work array in which all old solution vectors for the
!                         integration points are stored
!     vecold         i    In this array all preceding solutions are stored, i.e.
!                         all solutions that have been computed before and
!                         have been carried to system or deriva by the parameter
!                         islold in the parameter list of these main subroutines
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer isub, isup, jelp, jtime,
     +        mconv, modelv, mcont, nveloc, imesh, mdiv,
     +        modelrhsd, ioldm, indkappa, indcoeffgrad, indfdiv,
     +        indcoeffdiv, indetaspecial, indetha, indomega,
     +        indgoertler, indcconv, iseqvel(81), mult, nsave, jcheld,
     +        iseqpres(8)
      double precision cn, clamb, thetdt, tang(1), w(m+n),
     +                 x(n,ndim), xgauss(m,ndim), dphidx(n,m,ndimlc),
     +                 dudx(m,ndim,ncomp), du0dx(m,ndim,ncomp),
     +                 qmat(9,m), ugauss(m,ncomp),
     +                 unew(n,ncomp), unewgs(m,ncomp), uold(n,ncomp),
     +                 uoldm(n,ncomp), dpsidx(nvert,n,ndim), ethaeff(m),
     +                 secinv(m), gradv(9,m)
      logical debug
      save isub, isup, jelp, jtime, mconv, modelv, cn, clamb,
     +     thetdt, mcont, nveloc, imesh, mdiv, jcheld, mult,
     +     modelrhsd, ioldm, iseqvel, iseqpres

!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     dphidx         Array of length n x m x ndim  containing the
!                    derivatives
!                    of the basis functions in the sequence:
!                         d phi_i/ dx_j(xg_k) = dphidx (i,k,j);
!                    If the element is a linear simplex, the
!                    derivatives are constant and only k = 1 is filled.
!     dpsidx         Array of length n x m x l  containing the derivatives
!                    of the linear basis functions in the sequence:
!                    d psi / dx (xg ) = psix (i,k,1);
!                         i        k
!                    d psi / dy (xg ) = psix (i,k,2);
!                         i        k
!                    d psi / dz (xg ) = psix (i,k,3);
!                         i        k
!     du0dx          See dudx, but now for the extra velocity
!     dudx           Real m x ndim  array in which the values of the
!                    derivatives of the solution vector in the integration
!                    points are stored in the sequence:
!                    du / dx  (x ) = dudx ( k, j )
!                      j     k
!                    If jdercn = 0 in common block celint, then only
!                    dudx (1,*) is filled because dudx is then a constant
!                    per element
!                    dudx is only filled if jderiv>0
!     ethaeff        contains effective viscosity
!     gradv          Array containing the gradient vector in
!                    the integration points
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     indcconv       Sequence number of convection coefficients
!     indcoeffdiv    Sequence number of divergence coefficients
!     indcoeffgrad   Sequence number of gradient coefficients
!     indetaspecial  Sequence number of special viscosity
!     indetha        Sequence number of viscosity
!     indfdiv        Sequence number of right-hand side continuity equation
!     indgoertler    Sequence number of goertler coefficients
!     indkappa       Sequence number of kappa
!     indomega       Sequence number of omega
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     iseqpres       Sequence number of pressure with respect to array isol
!     iseqvel        Array containing the positions of the velocities in
!                    the element vector in the sequence required by el2005
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     jcheld         if ( icheld<= 10 ) then icheld else icheld-20
!     jelp           Indication of the type of basis function subroutine
!                    must be called by ELxxxx.
!     jtime          Integer parameter indicating if the mass matrix for the
!                    heat equation must be computed (>0) or not ( = 0)
!                    If jtime = 1, then the coefficient for the mass matrix is
!                    constant, otherwise (jtime = 2) it is variable
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!     mconv          see USER INPUT in subroutine ELM600
!     mdiv           Defines if divergence right-hand side is used (1)
!                    or not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second
!                          invariant
!                    102:  user provided model depending on the second
!                          invariant,
!                          the co-ordinates and the velocity
!     mult           Multiplication factor for the number of unknowns per point
!     nsave          Help parameter to save the value of n
!     nveloc         Number of velocity parameters
!     qmat           transformation matrix global -> local coordinates
!                    in case of a 3D element
!     secinv         Array containing the second invariant in the
!                    integration points
!     tang           In this array the tangential vector is stored
!                    tang(j,i) contains the j th component of the
!                    tangential vector in the i-th integration point
!     thetdt         1/(theta dt) for the case of the theta method
!                    immediately applied
!     ugauss         Preceding solution in Gauss points
!     unew           New solution vector in nodal points
!     unewgs         solution vector in integration points
!     uold           Array containing the previous iteration (old solution)
!     uoldm          Solution at time level m
!     w              array of length m containing the weights for
!                    integration
!     x              array of length n x ndim containing the co-ordinates
!                    of the nodes
!     xgauss         array of length m x ndim containing the co-ordinates
!                    of the gauss points.  array xgauss is only filled when
!                    gauss integration is applied.
!                    xg_i = xgauss (i,1);  yg_i = xgauss (i,2);
!                    zg_i = xgauss (i,3)
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0903         Initialize parameters for subroutines ELD/M/I903
!     EL1005         fill of derivatives and weights in elemvc and elemwg
!                    with respect to subroutine ELD250
!     EL1007         Fill element vector for vertices only
!     EL1014         Restrict values of u, x and dudx to centroid
!     EL1016         Compute pressure by extracting from solution vector
!     EL2005         Compute a local vector and its derivatives from a known
!                    vector
!     EL3009         Fill series of old solutions in array vecloc
!     EL4917         Compute special derivatives for Navier-Stokes
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM900ADDRESS1 Compute starting addresses of subarrays in array
!     ELM900COEFFS   Fill variable coefficients in coefficients array
!                    Fill velocity from prior iteration if necessary
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRININ         print 1d integer array
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  ISEQ:  Sequence number of vector in VECOLD/ISLOLD from which UOLD and
!         derivatives must be computed.
!         If 0 or 1 the first vector is used
!         If more vectors are needed, these vectors are supposed to be stored
!         sequentially, where the first one is indicated by ISEQ
!     2:  type of viscosity model used (modelv)
!         Possible values:
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!     5:  Information about the convective terms (mconv)
!         Possible values:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!     6-20: Information about the equations and solution method
!     6:  -
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-20: not yet used
!     Remark:  the coefficients 2-20 are only used in the case of icheld = 6
!              or 11, i.e. computation of the stress tensor
!              In the case icheld = 7 and an extended quadratic triangle
!              also all parameters are used
!     icheld                    Cartesian            Cylindrical/Polar
!       1                        du        / d
!                                  jdegfd     ix
!       2                        grad u               2D/Cyl: (r,z)
!                                i.e. du1/dx1,        dur/dr, duz/dr,
!                                du2/dx1, ...         dur/dz, duz/dz
!                                                     3D/Cyl: (r,z,phi)
!                                                     dur/dr, duz/dr, duphi/dr
!                                                     dur/dr, duz/dz, duphi/dz
!                                                     dur/rdphi-uphi/r,
!                                                     duz/rdphi,duphi/rdphi+ur/r
!                                                     2D/Pol: (r,phi)
!                                                     dur/dr, duz/dr,
!                                                     dur/rdphi-uphi/r,
!                                                     duphi/rdphi+ur/r
!       3                        - grad u
!       4                        div u
!       5                        curl u                  Cylinder co-or
!                     2D:       du2/dx1-du1/dx2      2D:   duz/dr-dur/dz
!                     3D:       du3/dx2-du2/dx3      3D:   duz/rdphi-duphi/dz
!                               du1/dx3-du3/dx1       duphi/dr+(uphi-dur/dphi)/r
!                               du2/dx1-du1/dx2            dur/dz-duz/dr
!                                                         Polar co-or
!                                                     duphi/dr+(uphi-dur/dphi)/r
!                             | sigma x |                    | sigma r     |
!       6                     | sigma y |                    | sigma z     |
!                     sigma = | sigma z |            sigma = | sigma theta |
!                             | tau xy  |                    | tau rz      |
!                             | tau yz  |                    | tau zphi    |
!                             | tau zx  |                    | tau phir    |
!       7             pressure p
!       8             elongation
!                        ndim = 2, jcart # 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(1)*u(2)*(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(3)**2*gradu(3,3) + u(1)*u(2)*(gradu(1,2) +
!                             gradu(2,1)) + u(1)*u(3)*(gradu(1,3)+gradu(3,1)) +
!                             u(2)*u(3)*(gradu(2,3)+gradu(3,2)))/||u||*2
!       9             deformation
!                        ndim = 2, jcart # 4:
!                        g = (2u(1)u(2)(-gradu(1,1)+gradu(2,2)) +
!                             u(1)**2-u(2)**2(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        g1 = gradu(1,2) + gradu(2,1)
!                        g2 = gradu(2,3) + gradu(3,2)
!                        g3 = gradu(3,1) + gradu(1,3)
!      10             sqrt(secinv)
!      11             viscous dissipation: 1/2 * t:A1
!      21-31:  See 1-11, however, now defined in the vertices instead of the
!              nodal points
!     Structure of output arrays:
!     Solution array:  ndim unknowns per point (IVEC=0)
!     Arrays of special structure:
!     IVEC = 1:   1 unknowns per point
!     IVEC = 2:   2 unknowns per point
!     IVEC = 3:   3 unknowns per point
!     IVEC = 4:   6 unknowns per point
!     IVEC = 5:   ndim unknowns per point
!     IVEC = 6:   1 unknowns per vertex
!     IVEC = 7:   2 unknowns per vertex
!     IVEC = 8:   3 unknowns per vertex
!     IVEC = 9:   6 unknowns per vertex
!     IVEC =10:   ndim unknowns per vertex
!     IVEC =11:   1/3/6 unknowns per point
!     IVEC =12:   0/1/3 unknowns per point
!     IVEC =13:   1/4/9 unknowns per point
!     IVEC =14:   1/3/6 unknowns per vertex
!     IVEC =15:   0/1/3 unknowns per vertex
!     IVEC =16:   1/4/9 unknowns per vertex
!     ICHELD                 IVEC
!       1                      1
!       2                     13
!       3                     13
!       4                      1
!       5                     12
!       6                      4
!       8                      1
!       9                     12
!      10                      1
!      11                      1
!      21                      6
!      22                     16
!      23                     16
!      24                      6
!      25                     15
!      26                      9
!      28                      6
!      29                     15
!      30                      6
!      31                      6
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     if ( ifirst=0 ) then
!        Define element independent quantities
!        Compute pointers in array WORK
!     Compute basis functions and related quantities
!     Fill old solution into array UOLD and if necessary the derivatives
!     Compute variable coefficients if necessary
!     Compute derived quantities
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'eld903' )

      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from eld903'
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         call el0903 ( iuser, user, jelp, jtime, cn, clamb,
     +                 mconv, mcont, modelv, isub, isup, coeffintpnts,
     +                 thetdt, nveloc, iseqvel, iseqpres, mdiv,
     +                 modelrhsd, index4, coeffnodes )
         if ( debug )
     +      write(irefwr,1) 'm, n, jelp, irule', m, n, jelp, irule
         iseqin = iseqi1   ! set iseqin equal to first input vector

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

!        --- Length of elemwg(*) to be filled

         if ( icheld<=11 ) then
            jcheld = icheld
         else if ( icheld<=16 ) then
            jcheld = icheld-10
         else if ( icheld==17 ) then
            jcheld = 11
         else if ( icheld<=19 ) then
            jcheld = icheld-10
         else if ( icheld==20 ) then
            jcheld = 7
         else if ( icheld<=31 ) then
            jcheld = icheld-20
         else
            jcheld = icheld-40
         end if
         if ( jcheld==1 .or. jcheld==4 .or. jcheld==7 .or.
     +        jcheld==8 .or. jcheld==10 .or. jcheld==11 ) then

!        --- icheld = 1, 4, 6, 8, 10 or 11
!            1 unknown per point

            mult = 1

         else if ( jcheld==2 .or. jcheld==3 ) then

!        --- icheld = 2 or 3
!            ndim**2 unknowns per point

            mult = ndim**2

         else if ( jcheld==6 ) then

!        --- icheld = 6
!            lower triangle of tensor

            mult = 6

         else if ( ndim==2 ) then

!        --- icheld = 5 or 9 and ndim = 2,
!            one unknown per point

            mult = 1

         else

!        --- icheld = 5 or 9 and ndim = 3,
!            three unknowns per point

            mult = 3

         end if
         if ( debug )
     +      write(irefwr,1) 'icheld, jcheld, mult', icheld, jcheld, mult

      end if

      if ( jcheld==7 ) then

!     --- Special situation, the pressure is extracted from the solution
!         vector and no other action is performed

         call el1016 ( vecold, uold, index3, iseqpres, elemvc, iseqin )
         go to 1000

      end if

!     --- Compute some sequence numbers of coefficients

      call elm900address1 ( indkappa, indcoeffgrad, indcoeffdiv,
     +                      indetaspecial, indomega, indetha,
     +                      indgoertler, indcconv, mcont, indfdiv )

!     --- compute basis functions and weights for numerical integration

      call elm800basefn ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, tang, jelp, phixi, psi, dpsidx, psixi )
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      if ( ind(50)==1 .or. ind(50)==3 ) then
         call el2005 ( vecold, uold, dudx, phi,
     +                 dphidx, index3, iseqvel, ugauss,
     +                 iseqin, index4 )
      end if

      if ( ind(50)==2 .or. ind(50)==3 )
     +   call el3009 ( vecold, index3, index4, vecloc,
     +                 phi, n, work )

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      call elm900coeffs ( vecold, uoldm, dphidx, index3,
     +                    iseqvel, ioldm, index4, phi, ugauss,
     +                    dudx, unew, du0dx, unewgs,
     +                    uold, mcont, isub, isup,
     +                    imesh, iuser, user, vecloc, x, xgauss,
     +                    coeffnodes, coeffintpnts )

!     --- Fill elemvc, contents depends on icheld

      if ( icheld<=5 .or. icheld>=31 .and.icheld<=35 ) then

!     --- 1<=icheld<=5 or 31<=icheld<=35,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, uold, dudx,
     +                 x, mult, elemvc, m )

      else if ( icheld>=43 .and. icheld<=47 ) then

!     --- 43<=icheld<=47,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, ugauss, dudx,
     +                 xgauss, mult, elemvc, m )

      else if ( icheld<=11 .or. icheld>35 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation

         call el4917 ( jcheld, modelv, coeffintpnts(1,indetha), ethaeff,
     +                 cn, clamb, xgauss, uold, dudx, secinv,
     +                 gradv, mult, elemvc, m, vecloc,
     +                 maxunk, numold, m, mcont )

      else if ( icheld>20 .and. icheld<=25 ) then

!     --- 20<icheld<=25 fill elemvc by derivatives,
!         First fill in all nodes

         call el1005 ( icheld-20, ix, jdegfd, uold, dudx,
     +                 x, mult, work, m )

!        --- Copy in vertices

         call el1007 ( work, elemvc, mult, ishape )

      else if ( icheld>11 .and. icheld<=15 ) then

!     --- 12<=icheld<=15
!         fill elemvc by derivatives
!         First set n equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, dudx, x, ishape )
         m = 1
         call el1005 ( jcheld, ix, jdegfd, uold, dudx,
     +                 x, mult, elemvc, nsave )

!        --- reset n

         m = nsave

      else if ( icheld<=20 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation
!         First set n equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, dudx, x, ishape )
         m = 1
         call el4917 ( jcheld, modelv, coeffintpnts(1,indetha), ethaeff,
     +                 cn, clamb, xgauss, uold, dudx,
     +                 secinv, gradv, mult, elemvc, m,
     +                 vecloc, maxunk, numold, nsave, mcont )

!        --- reset m

         m = nsave

      else

!     --- Compute stress tensor, elongation, shear rate or second invariant
!         First fill in all nodes

         call el4917 ( icheld-20, modelv, coeffintpnts(1,indetha),
     +                 ethaeff, cn, clamb, xgauss, uold, dudx, secinv,
     +                 gradv, mult, work,
     +                 n-jtrans, vecloc, maxunk, numold, m, mcont )

!        --- Copy in vertices

         call el1007 ( work, elemvc, mult, ishape )

      end if

1000  call erclos ( 'eld903' )
      if ( debug ) then

!     --- Debug information

         call prinin ( index1, n, 'index1' )
         call prinrl1 ( elemvc, n, mult, 'elemvc' )
         write(irefwr,*) 'End eld903'

      end if  ! ( debug )

      end
