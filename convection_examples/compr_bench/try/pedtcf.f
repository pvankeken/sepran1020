c *************************************************************
c *   PEDTCF
c *
c *   Determines CFL timestep; for use with elements 100 or 800
c *   for the heat equation.
c *   This version determines size of element using all three sides 
c *   of the element, which is necessary for irregular grids (like
c *   that used in axisymmetric and cylindrical modeling.
c *
c *   PvK 040390/981106
c *************************************************************
      subroutine pedtcf(kmesh,user,dtcfl)
      implicit none
      integer kmesh(*)
      real*8 user(*),dtcfl
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (ibuffr(1),buffr(1))
      include 'ccc.inc'
      integer kelmc,kelmi,nelem,npoint,ikelmc,ikelmi,ioff
      integer inidgt,iniget

      kelmc = kmesh(17)
      kelmi = kmesh(23)
      nelem = kmesh(9)
      if (periodic) then
         nelem = kmesh(kmesh(21))
      endif
      npoint = kmesh(8)
      call ini050(kelmc,'pedtcf')
      call ini050(kelmi,'pedtcf')
      ikelmc = iniget(kelmc)
      ikelmi = inidgt(kelmi)
      dtcfl = 1e30
      ioff = 10
      call pedtc1(nelem,ibuffr(ikelmc),buffr(ikelmi),user(ioff),
     v            user(ioff+npoint),dtcfl)
      return
      end

      subroutine pedtc1(nelem,kmeshc,coor,xvel,yvel,dtcfl)
      implicit none
      integer kmeshc(*),nelem
      real*8 coor(2,*),xvel(*),yvel(*),dtcfl
      integer index(4),i,ipc,j
      real*8 u(4),v(4),dx1,dy1,dx2,dy2,dx,dy,x,y,udx,vdy
      real*8 ubiggest,vbiggest,xsmallest,ysmallest,dx3,dy3
      integer ismallest

      do i=1,nelem
         ipc = 3*(i-1)+1
	 do j=1,3
            index(j) = kmeshc(ipc+j-1)
         enddo
         dx1 = abs(coor(1,index(1)) - coor(1,index(2)))
         dy1 = abs(coor(2,index(2)) - coor(2,index(3)))
         dx2 = abs(coor(1,index(2)) - coor(1,index(3)))
         dy2 = abs(coor(2,index(1)) - coor(2,index(2)))
         dx3 = abs(coor(1,index(3)) - coor(1,index(1)))
         dy3 = abs(coor(2,index(3)) - coor(2,index(1)))
	 dx = max(dx1,dx2,dx3)
	 dy = max(dy1,dy2,dy3)
	 do j=1,3
            u(j) = xvel(index(j))
	    v(j) = yvel(index(j))
	    x = coor(1,index(j))
	    y = coor(2,index(j))
	    if (abs(u(j)).gt.1e-7.and.abs(v(j)).gt.1e-7) then
	        udx = dx/abs(u(j))
	        vdy = dy/abs(v(j))
                dtcfl = min(dtcfl,udx,vdy)
                ismallest = i
                xsmallest = dx
                ysmallest = dy
                ubiggest = u(j)
                vbiggest = v(j)
            endif
         enddo
      enddo
c     write(6,'(''pedctf: '',i5,5f12.7)') ismallest,xsmallest,
c    v           ysmallest,ubiggest,vbiggest,dtcfl

      return
      end

