c *************************************************************
c *   UPDATECHEM
c *
c *   Degassing implemented using tracers. Calculate the loss
c *   of 3He by degassing, and the ingrowth and loss of 4He.
c *   4He is produced by decay of U,Th. U,Th may be lost
c *   by continental formation. Assume 99% extraction efficiency
c *   for U,Th; 90% efficiency for 3,4He.
c *
c *   Logic:
c *      Degas only when a particle has not been previously
c *      degassed (may happen because the particle is likely
c *      to reside in a degassing zone for more than one timestep)
c *      by assuming that any particle that is below the degassing zones
c *      is up for recycling again. 
c *   
c *   080800  Added Ar40 degassing
c *
c *
c *   All information is stored in chemmark(5,*):
c *      chemmark(1,i): 1=particle has been recently degassed
c *      chemmark(2,i): fraction of U,Th,K
c *      chemmark(3,i): He4
c *      chemmark(4,i): fraction of He3
c *      chemmark(5,i): Ar40
c *
c *   PvK 971110/080800
c *************************************************************
      subroutine updatechem(coormark,chemmark,
     v                      kmesh,kprob,isol,
     v                      nchem,NMARMAX)
      implicit none
      integer nchem
      real*8 coormark(2,*),chemmark(nchem,*)
      integer kmesh(*),kprob(*),isol(*),NMARMAX
      include 'tracer.inc'
      integer i,j,k,ntot,ichem,ip
      real*8 dettemp,factor,xm,ym,scaletemp,scalez,solidustemp
      real*8 temperature,presfromprem
      real*8 cost,r,th
      real*8 pi
      parameter(pi=3.1415926 535898)
      include 'SPcommon/ctimen'
      include 'degas.inc'
      include 'pecont.inc'
      integer kmax
      real*8 He3losszone(3),He4losszone(3),Ar40losszone(3)
      save He3losszone,He4losszone,Ar40losszone

      kmax=4

      if (itracoption.eq.2) then
         write(6,*) 'PERROR(updatechem): not yet implemented for'
         write(6,*) 'markerchain method'
         call instop
      endif
      ntot=0
      do idist=1,ndist
         ntot = ntot+ntrac(idist)
      enddo

      if (ntot.gt.NMARMAX) then
         write(6,*) 'PERROR(updatechem): ntot > NMARMAX' 
         write(6,*) 'ntot, NMARMAX: ',ntot,NMARMAX
         call instop
      endif

c     *** What it looks like for an undegassed particle
      dU238 = -rl238*U238*tstep*tscale
      dU235 = -rl235*U235*tstep*tscale
      dTh232 = -rl232*Th232*tstep*tscale
      dK40 = -rl40*K40*tstep*tscale
      U238 = U238 + dU238
      U235 = U235 + dU235
      Th232 = Th232 + dTh232
      K40  = K40 + dK40
      dHe4 = - 8*dU238  - 7 * dU235 - 6 * dTh232
      dAr40 = -K40_eff*dK40
      He4 = He4  + dHe4
      Ar40 = Ar40 + dAr40
c     write(6,*) 'Ar40: ',K40,Ar40,dK40,dAr40

      do k=1,kmax
        nnow(k)=0
        nnowc(k)=0
      enddo

      do i=1,ntot
c        *** First decay U,Th within each tracer
c        *** Then calculate the radiogenic ingrowth of 4He 
c        *** CASE 1:
         do k=1,kmax
            ip=NSPECIES*(k-1)
            dU238 = -rl238*U238*chemmark(ip+2,i)*tstep*tscale
            dU235 = -rl235*U235*chemmark(ip+2,i)*tstep*tscale
            dTh232 = -rl232*Th232*chemmark(ip+2,i)*tstep*tscale
            dK40 = -rl40*K40*chemmark(ip+2,i)*tstep*tscale
            dHe4 = - 8*dU238  - 7 * dU235 - 6 * dTh232
            dAr40= - K40_eff*dK40
            chemmark(ip+3,i) = chemmark(ip+3,i) + dHe4
            chemmark(ip+5,i) = chemmark(ip+5,i) + dAr40
         enddo
      enddo

      He3losszone(1)=0
      He3losszone(2)=0
      He3losszone(3)=0
      He4losszone(1)=0
      He4losszone(2)=0
      He4losszone(3)=0
      Ar40losszone(1)=0
      Ar40losszone(2)=0
      Ar40losszone(3)=0


c     write(6,*) 'degas zones: ',ndegaszone(1),
c    v     (thmindegas(1,j),thmaxdegas(1,j),j=1,ndegaszone(1))

      do i=1,ntot
c        *** Finally, decide if it is time to degas 
         xm = coormark(1,i)
         ym = coormark(2,i)
         r = sqrt(xm*xm + ym*ym)
         do k=1,kmax
           if (r.lt.rdegas(k)) then
c             *** Make particle degassable.
              ip=NSPECIES*(k-1)
              chemmark(ip+1,i) = 0
           else
c             *** Check on degassing only if particle has not
c             *** recently been degassed . This needs to
c             *** be updated for all four cases.
              ip=NSPECIES*(k-1)
              if (chemmark(ip+1,i).eq.0) then
c                *** Particle has not been degassed
                 cost = ym/r
                 if (xm.gt.0) then
                    th = acos(cost)
                 else
                    th = 2*pi - acos(cost)
                 endif
                 do j=1,ndegaszone(1)
                   if (th.ge.thmindegas(1,j).and.th.lt.thmaxdegas(1,j))
     v                     then   
c                    *** Degas! 
c                    write(6,'(''D: '',4f8.4)') xm,ym,r,th
                     chemmark(ip+1,i) = 1
                     He3losszone(j) = He3losszone(j)+
     v                    0.9*chemmark(ip+3,i)
                     He4losszone(j) = He4losszone(j)+
     v                    0.9*chemmark(ip+4,i)
                     Ar40losszone(j) = Ar40losszone(j)+
     v                    0.9*chemmark(ip+5,i)
                     He4loss(k) = He4loss(k) + 0.9*chemmark(ip+3,i)
                     He3loss(k) = He3loss(k) + 0.9*chemmark(ip+4,i)
                     Ar40loss(k) = Ar40loss(k) + 0.9*chemmark(ip+5,i)
                     chemmark(ip+3,i) = 0.1*chemmark(ip+3,i)
                     chemmark(ip+4,i) = 0.1*chemmark(ip+4,i)
                     chemmark(ip+5,i) = 0.1*chemmark(ip+5,i)
                     nnow(k) = nnow(k)+1
                   endif
                 enddo
              endif
            endif
        enddo
      enddo

      do k=1,kmax
         ndegas(k) = ndegas(k)+nnow(k)
         ncont(k)  = ncont(k)+nnowc(k)
      enddo

      write(71,*) (t,He3losszone(j),He4losszone(j),Ar40losszone(j),
     v      j=1,ndegaszone(1))

      return
      end
