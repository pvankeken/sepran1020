c *************************************************************
c *   PEFBUOY1
c *   *Purpose:
c *   *calculate the buoyancy force in the Stokes equation.
c *
c *   Note: Sepran multiplies this with density rho to find
c *   the complete buoyancy terms.
c *
c *   * input:
c *   * ichois     - select first/second component
c *   * x,y        - cartesian coordinates of the evaluation point
c *   * temp       - temperature value in the evaluation point
c *
c *   * output: value of the buoyancy force component
c *************************************************************
      real*8 function pefbuoy1(ichois,x,y,temp)
      implicit none
      integer ichois
      real*8 x,y,temp
      include 'depth_thermodyn.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'pecof900.inc'
      real*8 aload,prespi,bigGamma,alpha_eff,funccf,z
      integer iph
      real*8 r,cost,sint,rho

c     *** spherical coordinates
      r = sqrt(x*x+y*y)
      sint=x/r
      cost=y/r

c     *** thermal buoyancy force
      aload =  Ra*temp

c     *** Depth dependent expansivity
      if (ialphatype.eq.1) then
         alpha_eff = funccf(5,x,y,z)
      else
         alpha_eff = 1
      endif
      aload = aload * alpha_eff

c     *** 
      if (compress) then
c        *** Multiply by reference density (which is one by definition
c        *** in the EBA and BA cases)
         rho = funccf(3,x,y,z)
         aload = aload * rho
      endif

c     *** contribution of the phase changes
c     *** This needs careful reevaluation for compressible convection
c     do iph=1,nph
c        prespi = (r2-r) - phz0(iph) - gamma(iph)*(temp-pht0(iph))
c        bigGamma = 0.5 + 0.5*tanh(prespi/phdz(iph))
c        aload = aload - Rb(iph)*bigGamma
c     enddo


      if (ichois.eq.1) then
         pefbuoy1 = aload * sint
      else if (ichois.eq.2) then
         pefbuoy1 = aload * cost
      else 
        write(6,*) 'PERROR(pefbuoy1): unknown option: ',ichois
        call instop
      endif

      return
      end
