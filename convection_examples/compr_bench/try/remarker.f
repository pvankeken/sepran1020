      subroutine remarker
      include 'ccc.inc' 
      if (cyl) then
         call remarker_cyl
      else
         call remarker_cart
      endif
      return
      end
      
c *************************************************************
c *   REMARKER_CYL
c *
c *   Regrid the markerchain. Check if the interval between
c *   markers has become larger than the tolerance DMAX. If so,
c *   add a marker in the center of this interval using linear
c *   interpolation.
c *
c *   PvK 960830
c *
c *   PvK 072604
c *************************************************************
      subroutine remarker_cyl
      implicit none
      integer nmnew(10)
      include 'c1mark.inc'
      include 'petrac.inc'
      include 'ccc.inc'
      include 'elem_topo.inc'
      integer ipm,ipn,nmark,inew,iold,ip,ip1,inewtot
      real*8 dx,dy,rl,x1,x2,y1,y2,distance,xnew,ynew,rnew,thetanew
      real*8 rm1,rm2,theta1,theta2

      if (.not.quart.and..not.half.and..not.eighth) then
c       *** we need to deal with the case when the old markers
c       *** straddle x=0.
        write(6,*) 'PERROR(remarker_cyl): not yet suited for'
        write(6,*) 'full cylinder geometry'
        call instop
      endif

      ipm = 0
      ipn = 0
      if (nochain.ne.1) stop 'remarker_cyl nochain<>1'
      inewtot=1
      do ichain=1,nochain
         nmark = imark(ichain)
c        write(6,*) 'remarker_cyl: ',coornewm(1),coornewm(2),
c    v     coornewm(2*nmark-1),coornewm(2*nmark)
         ip = 2*(ipn+1)-1
         coormark(ip)   = coornewm(ip)
         coormark(ip+1) = coornewm(ip+1)
         inew=1
         iold=1
10       continue
           inew = inew+1
           inewtot = inewtot + 1
           ip = 2*(ipm+iold)-1
           x1 = coornewm(ip)
           y1 = coornewm(ip+1)
           iold = iold+1
100        continue
            ip = 2*(ipm+iold)-1
            x2 = coornewm(ip)
            y2 = coornewm(ip+1)
            distance  = sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
            if (distance.lt.dm_min(ichain)) then
c             *** tracer can be removed. Continue along the chain
c             *** until distance has become large enough
c            write(6,*) 'distance<dm_min: ',distance,dm_min(ichain),iold
              if (iold.lt.nmark-1) then
                 iold = iold+1
                 goto 100
              endif
            endif
              
           if (distance.gt.dm(ichain)) then
c             *** add a marker
              xnew = 0.5*(x1+x2)
              ynew = 0.5*(y1+y2)
              rnew = sqrt(xnew*xnew+ynew*ynew)
              if (rnew.ge.rtop_threshold.or.rnew.le.rbot_threshold) then
c                *** use cylindrical coordinates for interpolation
                 rm1 = sqrt(x1*x1+y1*y1)
                 rm2 = sqrt(x2*x2+y2*y2)
                 theta1 = acos(y1/rm1)
                 theta2 = acos(y2/rm2)
                 thetanew = 0.5*(theta1+theta2)
                 rnew = 0.5*(rm1+rm2)
                 xnew = rnew*sin(thetanew)
                 ynew = rnew*cos(thetanew)
c                write(6,'(''cyl remark: '',9f9.5)') 
c    v             x1,y1,xnew,ynew,x2,y2,rm1,rm2,rnew
              else
                call checkbounds(0,xnew,ynew)
              endif
              ip = 2*(ipn+inew)-1
              coormark(ip)   = xnew
              coormark(ip+1) = ynew
              inewtot=inewtot+1
              inew=inew+1
           endif

           if (inewtot.gt.NTRACMAX) then
c             *** markerchain has become to big: routine failed
              write(6,*) 'PERROR(remarker_cyl): markerchain too big'
              write(6,*) 'inewtot = ',inewtot
              write(6,*) 'NTRACMAX = ',NTRACMAX
              call instop
           endif

           ip = 2*(ipn+inew)-1
           coormark(ip)   = x2
           coormark(ip+1) = y2
         if (iold.le.nmark-1) goto 10

         nmnew(ichain) = inew
         ipm = ipm+nmark
         ipn = ipn+inew
       enddo

c      write(6,*) 'remarker_cyl: nmark(old/new)=',imark(1),nmnew(1),
c    v           dm(1)
       do ichain=1,nochain
          imark(ichain) = nmnew(ichain)
       enddo

       nmark = imark(1)
c      write(6,*) 'remarker_cyl: ',coormark(1),coormark(2),
c    v            coormark(2*nmark-1),coormark(2*nmark)
       xi_eta_stored=0
       return
       end


c *************************************************************
c *   REMARKER_CART
c *
c *   Regrid the markerchain. Check if the interval between
c *   markers has become larger than the tolerance DMAX. If so,
c *   add a marker in the center of this interval using linear
c *   interpolation.
c *
c *   PvK 960830
c *
c *   PvK 072604
c *************************************************************
      subroutine remarker_cart
      implicit none
      integer nmnew(10)
      include 'c1mark.inc'
      include 'petrac.inc'
      integer ipm,ipn,nmark,inew,iold,ip,inewtot
      real*8 dx,dy,rl,x1,x2,y1,y2,distance,xnew,ynew

      ipm = 0
      ipn = 0
      inewtot=1
      do ichain=1,nochain
         nmark = imark(ichain)
         ip = 2*(ipn+1)-1
         coormark(ip) = coornewm(ip)
         coormark(ip+1) = coornewm(ip+1)
         inew=1
         do iold=1,nmark-1
           inew = inew+1
           inewtot = inewtot+1
           ip = 2*(ipm+iold)-1
           x1 = coornewm(ip)
           y1 = coornewm(ip+1)
           x2 = coornewm(ip+2)
           y2 = coornewm(ip+3)
           distance  = sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) )
           if (distance.gt.dm(ichain)) then
c             write(6,*) iold,distance,dm(ichain),ichain
c             *** add a marker
              xnew = 0.5*(x1+x2)
              ynew = 0.5*(y1+y2)
              ip = 2*(ipn+inew)-1
              coormark(ip) = xnew
              coormark(ip+1) = ynew
              inew=inew+1
              inewtot = inewtot+1
           endif
           if (inewtot.gt.NTRACMAX) then
c             *** markerchain has become to big: routine failed
              write(6,*) 'PERROR(remarker_cart): markerchain too big'
              write(6,*) 'inewtot, NTRACMAX: ',inewtot,NTRACMAX
              call instop
           endif
           ip = 2*(ipn+inew)-1
           coormark(ip) = x2
           coormark(ip+1) = y2
         enddo
         nmnew(ichain) = inew
         ipm = ipm+nmark
         ipn = ipn+inew
       enddo

       do ichain=1,nochain
          imark(ichain) = nmnew(ichain)
       enddo
       write(6,*) 'remarker_cart: ',
     v    imark(1),coormark(1),coormark(2*imark(1)-1)

       return
       end



