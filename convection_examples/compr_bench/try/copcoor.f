c *************************************************************
c *   COPCOOR
c *   PvK 990413
c *************************************************************
      subroutine copcoor(ichois)
      implicit none

      include 'petrac.inc'
      include 'tracer.inc'
      include 'c1mark.inc'
      integer nmark,i,j,ip1,ip2,ichois,ntot

      if (itracoption.eq.2) then
c        *** markerchain method
         nmark=0
         do ichain=1,nochain
            nmark=nmark+imark(ichain)
         enddo
         ntot = nmark
      else
         ntot=0
         do idist=1,ndist
            ntot=ntot+ntrac(idist)
         enddo
      endif
      

      if (ichois.eq.1) then
         do i = 1,ntot
            ip1 = 2*i-1
            ip2 = 2*i
            coormark(ip1) = coornewm(ip1) 
            coormark(ip2) = coornewm(ip2) 
c           velmark(ip1) = velnewm(ip1)
c           velmark(ip2) = velnewm(ip2)
         enddo
c        *** If we're keeping track of the element number for each tracer
c        *** we should make sure to keep the bookkeeping up to date.
         if (xi_eta_stored.eq.2) then
            xi_eta_stored=1
         else
            xi_eta_stored=0
         endif
      else
         do i = 1,ntot
            ip1 = 2*i-1
            ip2 = 2*i
            coornewm(ip1) = coormark(ip1) 
            coornewm(ip2) = coormark(ip2) 
c           velnewm(ip1) = velmark(ip1)
c           velnewm(ip2) = velmark(ip2)
         enddo
         if (xi_eta_stored.eq.1) then
            xi_eta_stored=2
         else 
            xi_eta_stored=0
         endif
      endif

      return
      end

