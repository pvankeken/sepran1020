c *************************************************************
c *   PEFVIS
c *
c *   Version for sigma,T dependent rheology in layered case
c *   Find value of ival through /powerlaw/zint(10).
c ************************************************************* 
      real*8 function pefvis(x,y,temp,ival,secinv)
      implicit none
      real*8 x,y,temp,secinv
      integer ival
      include 'powerlaw.inc'
      include 'vislo.inc'
      include 'c1visc.inc'
      include 'cqact.inc'
      include 'dimensional.inc'
      include 'ccc.inc'
      include 'csky.inc'
      real*8 z,E,V,t0,z0,eta0prime,A,A0,etanon,etalayer,etatemp
      real*8 beta,secsqr,pow,radius,eps
      parameter(eps=1e-7)
      integer ivalfind1,ifirst
      data ifirst/0/
      save ifirst,eta0prime

      if (itypv.eq.0) then
         pefvis=0
         return
      endif

c     *** pefvis for PEPI01 Table V 'benchmark'
      if (itypv.eq.2) then
c sky
           pefvis = exp(-log(10.**vis_ord)*temp)
c         pefvis = exp(1.66d0/(temp+0.2)-1.66d0/1.2)
         return
      endif
      

c     if (ifirst.eq.0)  then
c        ifirst=1
c        A0 = aqact(1)
c        if (Ts_dim+DeltaT_dim.gt.0) then
c           eta0prime = exp(-A0/(Ts_dim+DeltaT_dim))
c        else
c           write(6,*) 'PERROR(pefvis): set Ts_dim and DeltaT_dim'
c           write(6,*) 'if you use this version of pefvis'
c           call instop
c        endif
c        pefvis = eta0prime * exp( A0/(Ts_dim+1d0*DeltaT_dim))
c        if (pefvis.ne.1d0) then
c           write(6,*) 'PERROR(pefvis): something wrong'
c           write(6,*) 'pefvis: ',pefvis,pefvis.ne.1d0
c           write(6,*) 'A0: ',A0
c           write(6,*) 'eta0prime: ',eta0prime
c           write(6,*) 'T: ',1d0,Ts_dim+1d0*DeltaT_dim
c           call instop
c        endif
c     endif

  

      if (abs(itypv).eq.0) then
        pefvis = 1d0 
        return
      else if (abs(itypv).eq.1) then
        ival = ivalfind1(x,y)
        pefvis = viscl(ival)
        return
      endif

      etanon=1d0
      etalayer=1d0
      etatemp=1d0


c     *** Find prefactor from array viscl (to keep things
c     *** consistent with the logic of layer (depth) dependent
c     *** properties
      if (ivl) etalayer = viscl(ival)
      if (ivt) then
         radius = sqrt(x*x+y*y)
         z = (r2-radius)/(r2-r1)
         A0 = aqact(1)
         if (z.gt.zqact(1)) then
            A = A0 * ( 1 + aqact(2)*(z-zqact(1))/(1-zqact(1)) )
         else
            A = A0
         endif
         etatemp = exp( A/(temp+temp0)) * exp(-A0/temp0)
      endif


      if (ivn) then
         stop 'pefvis: ivn not possible yet'
         pow = spw(ival)
         secinv = sqrt(secsqr)
         etanon = secinv**pow
         etanon = 1d0/(beta + 1d0/etanon)
c        write(6,'(''V: '',8f8.3)') x,y,temp,pow,secinv,etanon,
c    v       pefvis,beta
      endif

      pefvis = etalayer * etatemp * etanon

      pefvis = min(pefvis,1d1)

c     write(6,*) 'x,y,pefvis: ',x,y,temp,ival,pefvis

      if (pefvis.le.0d0) then
         write(6,*) 'PERROR(pefvis): viscosity <= 0'
         write(6,*) 'x,y         ',x,y
         write(6,*) 't,ival      ',temp,ival
         write(6,*) 'secinv      ',secinv,secsqr
         write(6,*) 'viscl(ival) ',viscl(ival)
         write(6,*) 'etalayer:   ',etalayer
         write(6,*) 'etatemp:    ',etatemp
         write(6,*) 'etanon:     ',etanon
         write(6,*) 'pefvis      ',pefvis
         stop
      endif
    
      return
      end
