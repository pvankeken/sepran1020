c *************************************************************
c *   Time_int_output
c * 
c *   Intermediate output for time integration.
c *   Called at each time step.
c *   Output of time, Nusselt number, rms velocity.
c *   Store some information on chemistry (if required) in arrays.
c *
c *   PvK 071900
c *************************************************************
      subroutine time_int_output(kmesh1,kmesh2,kprob1,kprob2,
     v         isol1,isol2,islol1,islol2,iheat,idens,
     v         ivisc,isecinv,iuser,user) 
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*)
      integer isol2(*),islol1(*),islol2(*),iuser(*)
      integer ivisc(*),iheat(*),idens(*),isecinv(*),icompwork(5)
      real*8 user(*)

      integer ibuffr(1)
      common ibuffr
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      include 'pecpu.inc'
      include 'peparam.inc'
      include 'dimensional.inc'
      include 'petrac.inc'
      include 'cpephase.inc'
      include 'vislo.inc'
      include 'pexcyc.inc'
      include 'c1visc.inc'
      include 'degas.inc'
      include 'SPcommon/ctimen'
      include 'SPcommon/cmachn'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'depth_thermodyn.inc'
      include 'perealdata.inc'
      include 'tracer.inc'
      include 'coolcore.inc'
      include 'averages.inc'
      include 'ccc.inc'
      include 'bound.inc'
      include 'c1mark.inc'
      integer NFUNC
      parameter(NFUNC=505)
      integer NUM
      parameter(NUM=20 000)
      integer igradt(5),icurvs(3)
      real*8 funcx(NFUNC),funcy(NFUNC)
      integer ivser(100)
      real*8 vser(NUM),smax,tmax,volint,vrms1,vrms2,vrms_d
      real*8 bounin,temp1,vrms,anorm,funccf,x,z
      integer nunkp,irule,ihelp,ip,ielhlp,iphi(5),ivisdip(5) 
      integer ifirst,ikelmi,npoint,ichois,icheld,ix
      integer ipoint,inputcr(10),jdegfd,ivec,nq_a,inout,i
      integer iqtype_dum
      real*8 q_a(20),q_a_d(20),t_d
      real*8 phiav,heatdummy,rinputcr(10),rkap1,rkap2,y,heatav
      real*4 second
      real*8 DONE,pi
      parameter(DONE=1d0,pi=3.1415926 535898)
      real*8 heatprod1,heatprod2,veloc(2),veloc_d(2),avwork(2)
      integer ih,im,is,inidgt,ipheat,kmax,k,ntot

      save iphi,ifirst,ivisdip

      data ivser(1),vser(1),funcx(1),funcy(1)/100,NUM,2*NFUNC/
      data ifirst/0/

      kmax=4

      t2 = second()
      dcpu = t2-t1
      cput = t2-t00
      t1   = t2

c     *** Compute RMS velocity
      call pevrms9(isol1,kmesh1,kprob1,iuser,user)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
      vrms = sqrt(abs(vrms2))
      vrms_d = vrms*rkappa_dim/height_dim*100*year_dim

c     *** Compute heat flow through top and bottom
      call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
  
c     *** Interpolate temperature to regular grid
      call averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                 user,nth_output,nr_output,-1)

      if (Di.gt.0d0.or.itypv.ne.0) then
c       *** compute viscous dissipation
c       call phifromsepran(kmesh1,kprob1,isol1,ivisdip)
        call phifromgrad(kmesh1,kprob1,isol1,ivisdip)
        if (itypv.ne.0) then
c          *** compute viscosity
           call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                  ivisc,isecinv,iuser,user)
        endif

       call compressionwork(kmesh1,kmesh2,kprob1,kprob2,
     v                     isol1,isol2,icompwork)
        call averageT(3,icompwork,kmesh2,kprob2,iuser,user,avwork)

c       *** calculate average viscous dissipation
        npoint=kmesh1(8)
        call ini050(kmesh1(23),'intermediate output: coor')
        ikelmi = inidgt(kmesh1(23))
        heatdummy = 0d0
        iqtype_dum=0
        call pecophi(ivisdip,user,6,npoint,DONE,
     v         iqtype_dum,buffr(ikelmi),ivisc,iheat)
        iuser(2) = 1
        iuser(6) = 7
        iuser(7) = intrule900
        iuser(8) = icoor900
        iuser(9) = 0
        iuser(10) = 2001
        iuser(11) = 6
        phiav = volint(0,1,1,kmesh1,kprob1,isol1,iuser,user,ielhlp)
        phiav = phiav * DiRa / volume
        avwork(1)=avwork(1)*Di / volume

c       *** calculate average radiogenic heatproduction
        if (iqtype.eq.0) then
           heatav=0
        else 
           call ini050(iheat(1),'cylstart: iheat')
           ipheat=inidgt(iheat(1))
           do i=1,npoint
              user(6+i-1) = buffr(ipheat+i-1)
           enddo
           heatav = volint(0,1,1,kmesh2,kprob2,iheat,iuser,user,ielhlp)
           heatav = heatav/volume
        endif
      endif

c     *** Compute average and maximum surface velocity
      call surfacevel(kmesh1,kprob1,isol1,itop,veloc,veloc_d)

c     *** Store relevant info in arrays
!     nodata = nodata+1
!     tr(nodata) = t
!     vr(nodata) = vrms
!     q_ca(1,nodata) = q_a(1)
!     q_ca(2,nodata) = q_a(2)
!     q_ca(3,nodata) = q_a(3)
!     t_bota(nodata) = t_bot
!     heatproda(1,nodata) = heatprod1
!     heatproda(2,nodata) = heatprod2
!     phiava(nodata) = phiav
!     heatava(nodata) = heatav
!     avt_a(1,nodata) = avT
!     avt_a(2,nodata) = avTl
!     avt_a(3,nodata) = avTu
!     veloc1(nodata) = veloc(1)
!     veloc2(nodata) = veloc(2)

!     nconta(nodata) = ncont(2)
!     do k=1,kmax
!        ndegasa(k,nodata) = ndegas(k)
!        a238loss(k,nodata) = U238loss(k)
!        a235loss(k,nodata) = U235loss(k)
!        a232loss(k,nodata) = Th232loss(k)
!        aHe3loss(k,nodata) = He3loss(k)
!        aHe4loss(k,nodata) = He4loss(k)
!        aK40loss(k,nodata) = K40loss(k)
!        aAr40loss(k,nodata) = Ar40loss(k)
!     enddo


c     *** Output files at regular intervals
!     if (nodata.gt.50) call flushdatafile


c     *** Finally, output intermediate information
      ih = cput/3600
      im = (cput - ih*3600)/60
      is = cput - ih*3600 -im*60
      write(6,11) t,q_a(1),q_a(2),vrms,veloc(1),veloc(2),phiav,heatav 
     v        ,t_bot,dcpu,ih,im,is
      t_d = t/tscale_dim
!     write(6,21) t_d,q_a_d(1),q_a_d(2),vrms_d,veloc_d(1),veloc_d(2)
11    format(3x,e13.5,2e13.5,3e13.5,2e10.2,f10.3,f10.2,
     v       i5,':',i2.2,':',i2.2) 
21    format('D: ',e13.5,2e13.5,2e13.5,2e13.5,2e13.5)
      if (itracoption.ne.0) then
         ntot=0
         if (itracoption.eq.2) then
            do ichain=1,nochain
               ntot = ntot+imark(ichain)
            enddo
         else 
            do idist=1,ndist
               ntot=ntot+ntrac(idist)
            enddo
         endif
         write(6,22) coormark(1),coormark(2),ntot
      endif
22    format('T: ',2f12.7,i10)
      if (ifollowchem.ne.0) then
         write(6,13) nnow(1),ndegas(1),t*tscale,U238,U235,Th232, 
     v             K40,He4,Ar40 
c        do k=1,kmax
c           write(6,15) nnowc(k),ncont(k),U238loss(k),U235loss(k),
c    v         Th232loss(k),K40loss(k),
c    v         He4loss(k),He3loss(k),Ar40loss(k)
         do k=1,kmax
            write(6,16) nnow(k),ndegas(k),
     v         He4loss(k),He3loss(k),Ar40loss(k)
         enddo
      endif

      write(20,*) t,vrms
      write(21,*) t,q_a(1)
      write(23,*) t_d,vrms_d
      write(24,*) t_d,q_a_d(1)
      if (Di > 0.0d0) then
         write(25,*) t,phiav
         write(26,*) t,avwork(1)
      endif

13    format(8x,i5,i7,8f9.3)
15    format(8x,i5,i7,7e9.2)
16    format(8x,i5,i7,3e9.2)

      return
      end
