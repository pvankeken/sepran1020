c *************************************************************
c *   PEPLOTIT
c *   PvK 200700
c *************************************************************
      subroutine peplotit(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v         islol1,ivisc,iheat,isecinv,iuser,user,inout)
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol1(*),isol2(*)
      integer iuser(*),inout,ivisc(*),iheat(*),islol1(*),isecinv(*)
      real*8 user(*)

      include 'SPcommon/cplot'
      include 'SPcommon/cactl'
      include 'pecof800.inc'
      include 'c1visc.inc'
      include 'vislo.inc'
      include 'plotpvk.inc'
      include 'dimensional.inc'
      include 'cpephase.inc'

      integer ncoorc(200),iplots(20),input(200)
      real*8 coorcn(200),rplots(20),factor
      real*8 contln,format,yfaccn,rinvec(10)
      character*80 plottext,command
      integer icheld,ix,jdegfd,ivec,ichois,iinder(20),iphi(5)
      integer iuser_dum(100),iinvec(10),ientropy(5)
      save iphi

      iuser_dum(1)=100

      yfaccn=1
      factor=0d0
      format=15

c     call system('rm -f sepplot.*')
      jkader=-1
c     jtimes=1
      iplots(1)=2
      iplots(2)=1
      rplots(1)=format
      rplots(2)=yfaccn
      write(plottext,'(''V+T: '',i3)') inout
      write(fname,'(''PLOTS/TEMP.'',i3.3)') inout

!     call plotcn(kmesh2,kprob2,isol2,iplots,rplots,contln,
!    v                  input,plottext,ncoorc,coorcn)
c     jtimes=3
c     factor=0d0
c     call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,yfaccn,factor)
c     write(command,'(''mv sepplot.* TEMP.'',i3.3)') inout
c     call system(command)
c     write(command,'(''gzip -f TEMP.'',i3.3)') inout
c     call system(command)

c     jtimes=0
c     jkader=-1
      jtimes=1
      iplots(1)=2
      iplots(2)=1
      rplots(1)=format
      rplots(2)=yfaccn
      write(plottext,'(''V+T: '',i3)') inout
      call plotcn(kmesh2,kprob2,isol2,iplots,rplots,contln,
     v                  input,plottext,ncoorc,coorcn)
      jtimes=3
      factor=0d0
      call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,yfaccn,factor)
      jtimes=0
c     write(command,'(''mv sepplot.* TEMPOLD.'',i3.3)') inout
c     call system(command)
c     write(command,'(''gzip -f TEMPOLD.'',i3.3)') inout
c     call system(command)

      jtimes=0

      if (Di>0) then
         ! compute entropy for Jarvis and McKenzie 1980
         ! DeltaS = ln[ (T+T0)/T ] - (1-z)*Di
         ! eq 59, page 554
         iinvec(1)=5
         iinvec(2)=32  ! use funvec to create vector
         iinvec(3)=0
         iinvec(4)=0
         iinvec(5)=1
         rinvec(1)=2  ! option 2 is used in funvec
         rinvec(2)=Ts_dim
         call manvec(iinvec,rinvec,isol2,isol2,ientropy,kmesh2,kprob2)
!        write(6,*) 'entropy: ',ientropy

         write(fname,'(''PLOTS/ENTROPY.'',i3.3)') inout
         call plotcn(kmesh2,kprob2,ientropy,iplots,rplots,contln,
     v       input,plottext,ncoorc,coorcn)
      endif

      if (iqtype.gt.1.or.qwithrho) then
        write(fname,'(''GMT/HEAT.'',i3.3)') inout
        call plotcn(kmesh1,kprob1,iheat,iplots,rplots,contln,input,
     v            'heat',ncoorc,coorcn)
c       write(command,'(''mv sepplot.* HEAT.'',i3.3)') inout
c       call system(command)
c       write(command,'(''gzip -f HEAT.'',i3.3)') inout
c       call system(command)
      endif

      if (itypv.ne.0) then
        write(fname,'(''GMT/VISC.'',i3.3)') inout
        call logit(1,ivisc,kmesh1,kprob1)
        call plotcn(kmesh1,kprob1,ivisc,iplots,rplots,contln,input,
     v            'log10(visc)',ncoorc,coorcn)
        call logit(2,ivisc,kmesh1,kprob1)
c       write(command,'(''mv sepplot.* VISC.'',i3.3)') inout
c       call system(command)
c       write(command,'(''gzip -f VISC.'',i3.3)') inout
c       call system(command)

        if (ivn.or.tackley) then
          write(fname,'(''GMT/SECINV.'',i3.3)') inout
          call logit(1,isecinv,kmesh1,kprob1)
          call plotcn(kmesh1,kprob1,isecinv,iplots,rplots,contln,input, 
     v            'log10(secinv)',ncoorc,coorcn)
          call logit(2,isecinv,kmesh1,kprob1)
c         write(command,'(''mv sepplot.* SECINV.'',i3.3)') inout
c         call system(command)
c         write(command,'(''gzip -f SECINV.'',i3.3)') inout
c         call system(command)
        endif
      endif


      return
      end

c *************************************************************
c *   Take log10 from vector values (or do reverse)
c *************************************************************
      subroutine logit(ichois,ivec,kmesh,kprob)
      implicit none
      integer ichois,ivec(*),kmesh(*),kprob(*)
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1) 
      equivalence(buffr(1),ibuffr(1))
      integer npoint,iivec,inidgt

      npoint = kmesh(8)
      call ini050(ivec(1),'logit: ivec')
      iivec = inidgt(ivec(1))

      call logit00(ichois,buffr(iivec),npoint)
 
      return
      end
 
      subroutine logit00(ichois,vec,npoint)
      implicit none
      real*8 vec(*)
      integer npoint,ichois
      integer i

      if (ichois.eq.1) then
         do i=1,npoint
            vec(i)=log10(vec(i))
         enddo
      else
         do i=1,npoint
            vec(i) = 10d0**vec(i)
         enddo 
      endif

      return
      end

 
 

      
