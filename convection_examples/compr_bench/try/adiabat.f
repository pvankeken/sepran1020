      real*8 function adiabat(y)
      implicit none
      real*8 y
      include 'cpephase.inc'
      include 'dimensional.inc'

      if (iadiabat.eq.1) then
         adiabat = 0d0
      else if (iadiabat.eq.2) then
         adiabat = Tso_dim * (exp (Di * (1-y))-1d0) / DeltaT_dim
      else if (iadiabat.eq.3) then
         adiabat = Tso_dim * exp (Di * (1-y)) / DeltaT_dim
      endif

      return
      end

      real*8 function d2adiabatdz2(y)
      implicit none
      real*8 y
      include 'cpephase.inc'
      include 'dimensional.inc'

      if (iadiabat.eq.1) then
         d2adiabatdz2 = 0d0
      else if (iadiabat.eq.2.or.iadiabat.eq.3) then
         d2adiabatdz2 = Tso_dim*Di*Di*exp(Di * (1-y))/ DeltaT_dim
      endif

      return
      end
