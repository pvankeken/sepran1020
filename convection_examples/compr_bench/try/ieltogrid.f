c *************************************************************
c *   IELTOGRID
c *
c *   Find the mapping of element numbers to a regular Cartesian grid.
c *   This greatly facilitates tracer interpolation  (see ielfromgrid)
c *
c *   PvK 970406
c *
c *   Rewritten for curved elements. This should at some point be made
c *   more efficient by an iterative scheme: 1) fill regular grid using
c *   assumption of straight elements; 2) update regular grid using
c *   curved elements.
c * 
c *   PvK 020304
c *
c *   Modified for cylindrical lookup table (for itracoption=2).
c *   Still keep Cartesian one around for tracer integration.
c *   Put (r,th) pixel grid together for buoyancy computation.
c *   PvK 092204
c *************************************************************
      subroutine ieltogrid(kmesh)
      implicit none
      integer kmesh(*)
      integer ikelmc,ikelmi,ielem
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      
      real*8 pi
      parameter(pi=3.1415926 535898)
c     *** /pieltogrid/ contains information on regular grid
      include 'pieltogrid.inc'
      include 'ccc.inc'
      include 'bound.inc'
      include 'dimensional.inc'
      include 'elem_topo.inc'
      include 'cpix.inc'
      include 'tracer.inc'

      real*8 xn(6),yn(6)
      real*8 a(3),b(3),c(3),x,y,rl(3)
      real*8 xcmin,xcmax,ycmin,ycmax
      real*8 dx1,dy1,dx2,dy2,dx3,dy3,dr1,dr2,dr3,rmin,rmax
      integer ipx,ipy,i,ip
      integer NELEMMAX
      parameter(NELEMMAX=40 000)
      integer nodno(6),isub,ifillelem(NELEMMAX)
      integer iniget,inidgt,nodlin(3)
      logical checkinelem,correct
      integer npoint,nelem,ikelmo,nelgrp,imissed,ifound,ix,iy
      integer ipxmin1,ipxmax1,ipymin1,ipymax1
      real*8 xi,eta,phiq(6),dr_elem,un(6),vn(6),user,shapef(7)
      logical out,check,flip,guess_first
      logical on_mesh,off_mesh
      integer ielx,iely,iela(4,2),ielnow,ir,ith
      real*8 xm,ym,rm,theta,prefac,rpix,thpix
      real*8 th1,th2,th3,th4,rm1,rm2,rm3,rm4,thmin,thmax
      real*8 xmin,xmax,ymin,ymax
      
c     if (.not.quart) then
c        write(6,*) 'PERROR(ieltogrid): this needs some work for'
c        write(6,*) 'anything but quarter geometry'
c        call instop
c     endif

c     *** prescribe the dimensions of the regular grid
      if (axi) then
         if (quart.or.eighth) then
            NXEL = NXELM
            NYEL = NYELM
            dxel = r2/(NXEL-1)
            dyel = r2/(NYEL-1)
         else 
            NXEL = (NXELM-1)/2+1
            NYEL = NYELM
            dxel = r2/(NXEL-1)
            dyel = 2*r2/(NYEL-1)
         endif
         write(6,*) 'PINFO(ieltogrid): axi: ',NXEL,NYEL,dxel,dyel
      else
         if (quart.or.eighth) then
            NXEL = NXELM
            NYEL = NYELM
            dxel = r2/(NXEL-1)
            dyel = r2/(NYEL-1)
         else if (half) then
            NXEL = (NXELM-1)/2+1
            NYEL = NYELM
            dxel = r2/(NXEL-1)
            dyel = 2*r2/(NYEL-1)
         else
            NXEL = NXELM
            NYEL = NYELM
            dxel = 2*r2/(NXELM-1)
            dyel = 2*r2/(NYELM-1)
         endif
         write(6,*) 'PINFO(ieltogrid): cyl: ',NXEL,NYEL,dxel,dyel
      endif

      rmin = r2
      rmax = 0
      if (axi) then
         if (quart.or.eighth) then
            xoff = 0
            yoff = 0
            grid_height=r2
            grid_width =r2
         else 
            xoff=0
            yoff=r2
            grid_height=2*r2
            grid_width =r2
         endif
      else
         if (quart.or.eighth) then
            xoff = 0
            yoff = 0
            grid_height=r2
            grid_width =r2
         else if (half) then
            xoff=0
            yoff=r2
            grid_height=2*r2
            grid_width =r2
         else
            xoff=r2
            yoff=r2
            grid_height=2*r2
            grid_width =2*r2
         endif
      endif
      write(6,*) 'PINFO(ieltogrid): grid width/height: ',
     v      grid_width,grid_height
      write(6,*) '    xoff yoff                      : ',
     v      xoff,yoff
      write(6,*) '    axi  eighth  quart  half       : ',
     v      axi,eighth,quart,half


c     *** Get the coordinates and nodal points of the quadratic mesh
      call ini050(kmesh(23),'iel: coordinates')
      call ini050(kmesh(17),'iel: nodalpoints')
      call ini050(kmesh(29),'iel: kmesh part o')
      npoint = kmesh(8)
      nelem  = kmesh(9)
      if (periodic) nelem=kmesh(kmesh(21))
      nelgrp = kmesh(5)
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      ikelmo = iniget(kmesh(29))

c     *** initialize element numbers in regular grid to aid error check
      do ipy = 1,NYEL
         do ipx = 1,NXEL
            iel(ipx,ipy) = -1
         enddo
      enddo

      if (nrpix.le.60) open(99,file='elements.dat')

c     *** loop over elements
      do ielem=1,nelem

c        *** determine coordinates for nodal points of this element
         call sper01(ibuffr(ikelmc),buffr(ikelmi),nodno,xn,yn,ielem)
         check = .false.
         if (nrpix.le.60) then
           if ((ielem/10)*10.eq.ielem) then
             do i=1,6
                write(99,*) xn(i),yn(i)
             enddo
             write(99,*) xn(1),yn(1)
             write(99,'(''> '')') 
           endif
         endif

c        *** Cartesian lookup grid
         xcmin = min(xn(1),xn(2),xn(3),xn(4),xn(5),xn(6))
         xcmax = max(xn(1),xn(2),xn(3),xn(4),xn(5),xn(6))
         ycmin = min(yn(1),yn(2),yn(3),yn(4),yn(5),yn(6))
         ycmax = max(yn(1),yn(2),yn(3),yn(4),yn(5),yn(6))
         dx1 = xn(1)-xn(3)
         dy1 = yn(1)-yn(3)
         dx2 = xn(3)-xn(5)
         dy2 = yn(3)-yn(5)
         dx3 = xn(5)-xn(1)
         dy3 = yn(5)-yn(1)
         dr1 = sqrt(dx1*dx1+dy1*dy1)
         dr2 = sqrt(dx2*dx2+dy2*dy2)
         dr3 = sqrt(dx3*dx3+dy3*dy3)
         rmin = min(rmin,dr1,dr2,dr3)
         rmax = max(rmax,dr1,dr2,dr3)
c        *** translate this to grid element values
         ipxmin(ielem) = max(1.0,(xcmin+xoff)/dxel+1.0)
         ipxmax(ielem) = min(NXEL*1.0,(xcmax+xoff)/dxel+1.0)
         ipymin(ielem) = max(1.0,(ycmin+yoff)/dyel+1.5)
         ipymax(ielem) = min(NYEL*1.0,(ycmax+yoff)/dyel+1.5)

         if (curved_elem(ielem)) then
c          write(6,*) 'element ',ielem,' is curved '
c          **** It's possible that the curved boundaries extend beyond
c          **** the (ipx,ipy) interval determined above
c          **** extend it a bit
           dr_elem =  min(dr1,dr2,dr3)
           ipxmin(ielem) = ipxmin(ielem) - 0.5*dr_elem/dxel
           ipxmax(ielem) = ipxmax(ielem) + 0.5*dr_elem/dxel
           ipymin(ielem) = ipymin(ielem) - 0.5*dr_elem/dyel
           ipymax(ielem) = ipymax(ielem) + 0.5*dr_elem/dyel
           ipxmin(ielem) = max(1,ipxmin(ielem))
           ipxmax(ielem) = min(NXEL,ipxmax(ielem))
           ipymin(ielem) = max(1,ipymin(ielem))
           ipymax(ielem) = min(NYEL,ipymax(ielem))
c          *** The ipxmin/ipxmax etc. values are overestimates.
c          *** use ipxmin1 etc. to reduce the values.
           ipxmin1 = ipxmax(ielem)
           ipxmax1 = ipxmin(ielem)
           ipymin1 = ipymax(ielem)
           ipymax1 = ipymin(ielem)
           do ipy = ipymin(ielem),ipymax(ielem)
              do ipx = ipxmin(ielem),ipxmax(ielem)
                 x = (ipx-1)*dxel - xoff
                 y = (ipy-1)*dyel - yoff
                 correct = checkinelem(3,xn,yn,x,y,nodno,nodlin,rl,
     v                xi,eta,phiq,isub)
                 if (correct) then
c                 *** this grid point is in the element
                    iel(ipx,ipy) = ielem
c                   *** improve estimate of min/max pixel extent
                    ipxmin1=min(ipx,ipxmin1)
                    ipxmax1=max(ipx,ipxmax1)
                    ipymin1=min(ipy,ipymin1)
                    ipymax1=max(ipy,ipymax1)
                 endif
               enddo
           enddo        
           ipxmin(ielem) = ipxmin1
           ipxmax(ielem) = ipxmax1
           ipymin(ielem) = ipymin1
           ipymax(ielem) = ipymax1

         else 

c          *** Element is straight
           do ipy = ipymin(ielem),ipymax(ielem)
              do ipx = ipxmin(ielem),ipxmax(ielem)
                 x = (ipx-1)*dxel - xoff
                 y = (ipy-1)*dyel - yoff
                 correct = checkinelem(1,xn,yn,x,y,nodno,nodlin,rl, 
     v                 xi,eta,phiq,isub)
                 if (correct) then
c                   *** this grid point is in the element
                    iel(ipx,ipy) = ielem
                 endif
               enddo
            enddo        
         endif
      enddo   
      close(99)

      write(6,'(''PINFO(ieltogrid): '')') 
      write(6,'(''  Minimum size of quadratic elements  : '',
     v          f8.5,'' ('',f8.1,'' km)'')')
     v           rmin,rmin*height_dim*1e-3
      write(6,'(''  Maximum size of quadratic elements  : '',
     v          f8.5,'' ('',f8.1,'' km)'')')
     v           rmax,rmax*height_dim*1e-3
      write(6,'(''  Resolution of regular grid          : '',
     v          f8.5,'' ('',f8.1,'' km)'')')
     v           dxel,dxel*height_dim*1e-3

      if (itracoption.eq.2) then
c          *** prepare the pixel look up grid for  markerchain buoyancy
c          *** note: iel(ipx,ipy) corresponds to lower left corner of pixel
c          *** drpix = size of pixel in radial dimension
c          *** dthpix = size of pixel in azimuthal dimension (in radians)
           if (drpix.le.0.or.dthpix.le.0) then
              write(6,*) 'PERROR(ieltogrid): drpix <= 0 or dthpix <=0 '
              write(6,*) 'drpix = ',drpix
              write(6,*) 'dthpix = ',dthpix
              call instop 
           endif
           do ielem=1,nelem
              ipix_rmin(ielem) = nrpix
              ipix_rmax(ielem) = 1
              ipix_thmin(ielem) = nthpix
              ipix_thmax(ielem) = 1
           enddo
           write(6,*) 'PINFO(ieltogrid): Prepare pixel look up grid ', 
     v                 'for buoyancy'
           write(6,*) 'Resolution: nrpix, nthpix = ',nrpix,nthpix
           write(6,*) '            drpix, dthpix = ',drpix,dthpix

c          *** convert iel information to pixel grid 
c          *** set pixel value to 0 when the pixel is on the mesh
           ip = 0
           do ir=1,nrpix
              rm = r1 + (ir-0.5)*dr_pixel
              do ith=1,nthpix
                 ip = (ir-1)*nthpix+ith
                 theta = (ith-0.5)*dthpix
c                *** coordinates of centerpoint in pixel
                 xm = rm*sin(theta)
                 ym = rm*cos(theta)
                 ix = (xm+xoff)/dxel + 1
                 iy = (ym+yoff)/dyel + 1
                 ielnow = iel(ix,iy)
                 if (ielnow.gt.0) then
                    call sper01(ibuffr(ikelmc),buffr(ikelmi),
     v                          nodno,xn,yn,ielnow)
                    if (curved_elem(ielnow)) then
                      correct = checkinelem(3,xn,yn,x,y,nodno,
     v                        nodlin,rl, xi,eta,phiq,isub)
                    else 
                      correct = checkinelem(1,xn,yn,x,y,nodno,
     v                        nodlin,rl, xi,eta,phiq,isub)
                    endif
                 else 
                    correct = .false.
                 endif
                 if (.not.correct) then
c                  *** Cartesian grid is a bit too coarse. Find correct
c                  *** element
                   guess_first=.false.
                   call pedeteltrac(0,xm,ym,ikelmc,ikelmi,ikelmo,
     v                        nodno,nodlin,rl,xn,yn,un,vn,user,ielnow,
     v                        npoint,nelem,imissed,ifound,nelgrp,
     v                        shapef,xi,eta,guess_first,'ieltogrid')
c                  write(6,*) 'but I found it: ',ielnow,imissed,ifound
                 endif
                 ipix(ip) = 0
                 xi_eta_pix(1,ip) = xi
                 xi_eta_pix(2,ip) = eta
                 if (axi) then
c                   *** Evaluate the integration weight 2*pi*r' for
c                   *** axisymmetric geometry (where r'=r*sin theta
c                   *** is the distance to the axis of symmetry)
                    rpix = r1+(ir-0.5)*drpix
                    thpix = (ith-0.5)*dthpix
                    prefac = 2*pi*rpix*sin(thpix)
                 else
c                   *** This weight is 1 in cylindrical geometry
                    prefac=1d0
                 endif
c                *** Multiply prefactor by the volume of the pixel
                 prefac = prefac*dr_pixel*dth_pixel(ir)
                 xi_eta_pix(3,ip) = prefac
                 ielempix(ip) = ielnow
c                *** pixel extrema in cylindrical geometry
                 ipix_rmin(ielnow)  = min(ipix_rmin(ielnow),ir)
                 ipix_rmax(ielnow)  = max(ipix_rmax(ielnow),ir)
                 ipix_thmin(ielnow) = min(ipix_thmin(ielnow),ith)
                 ipix_thmax(ielnow) = max(ipix_thmax(ielnow),ith)
               enddo
           enddo
      endif

      if (nrpix.le.60) then
        do ir=1,nrpix
           ip=(ir-1)*nthpix
           write(6,*) (ielempix(i),i=ip+1,ip+nthpix)
        enddo
      endif
 
      if (nrpix.le.60) then
        open(99,file='iel.dat')
        open(98,file='ipix_elem.dat')
        do ielem=10,nelem,10
           xmin=ipxmin(ielem)*dxel
           ymin=ipymin(ielem)*dyel
           xmax=ipxmax(ielem)*dxel
           ymax=ipymax(ielem)*dyel
           write(99,*) xmin,ymin
           write(99,*) xmin,ymax
           write(99,*) xmax,ymax
           write(99,*) xmax,ymin
           write(99,*) xmin,ymin
           write(99,'(''>'')')
           rmin = r1 + (ipix_rmin(ielem)-0.5)*dr_pixel
           rmax = r1 + (ipix_rmax(ielem)-0.5)*dr_pixel
           thmin = (ipix_thmin(ielem)-0.5)*dthpix 
           thmax = (ipix_thmax(ielem)-0.5)*dthpix 
           write(98,*) rmin*sin(thmin),rmin*cos(thmin)
           write(98,*) rmax*sin(thmin),rmax*cos(thmin)
           write(98,*) rmax*sin(thmax),rmax*cos(thmax)
           write(98,*) rmin*sin(thmax),rmin*cos(thmax)
           write(98,*) rmin*sin(thmin),rmin*cos(thmin)
           write(98,'(''>'')') 
        enddo
        close(98)
        close(99)
      endif

      return
      end


