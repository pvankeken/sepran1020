c *************************************************************
c *   DETRMINMAX
c *
c *   For a cylindrical mesh with inner boundary IBOT and
c *   outer boundary ITOP, determine the minimum/maximum radius
c *   of any point on the curve. 
c *
c *   PvK 970414
c *   PvK 020304
c *************************************************************
      subroutine detrminmax(kmesh,kprob,isol)
      implicit none
      integer kmesh(*),kprob(*),isol(*)
      integer NUM,NC
      parameter(NUM=10000,NC=10)
      real*8 funcx(NUM),funcy(NUM),x1,y1,x2,y2,r,th,x,y
      integer i,npcurv
      integer npoint,nelem,nelgrp,ikelmc,ikelmi,iniget,inidgt
      integer len_nodals,ikelmm,ncurvs,ikelmd
      parameter(len_nodals=10 000)
      integer nodals(len_nodals),ielem,ipn
      real*8 xn(6),yn(6),rmax,rmin
      real*8 ds,dsbot_min,dstop_min,dstop_av,dsbot_av
      integer nodno,icurvs(NC),numcurvs,num_nodals(NC),ip_nodals(NC) 

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      include 'bound.inc'
      include 'crminmax.inc'
      include 'ccc.inc'
      include 'elem_topo.inc'
      include 'dimensional.inc'
      include 'tracer.inc'

      funcx(1)=NUM
      funcy(1)=NUM

      icurvs(1)=0
      icurvs(2)=ibot
      call compcr(-1,kmesh,kprob,isol,0,icurvs,funcx,funcy)

      npcurv = funcx(5)/2
      radius_min=r1
      do i=1,npcurv
         x = funcx(4+2*i)
         y = funcx(5+2*i)
         r = sqrt(x*x+y*y)
         radius_min= max(r,radius_min)
      enddo
      radius_min = radius_min+1d-6

      dsbot_min=1e3
      dsbot_av=0
      do i=1,npcurv-1
         x1 = funcx(4+2*i)
         y1 = funcx(5+2*i)
         x2 = funcx(6+2*i)
         y2 = funcx(7+2*i)
         ds = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
         dsbot_min = min(dsbot_min,ds)
         dsbot_av  = dsbot_av + ds
      enddo
      dsbot_av = dsbot_av/(npcurv-1)

      icurvs(1)=0
      icurvs(2)=itop
      call compcr(-1,kmesh,kprob,isol,0,icurvs,funcx,funcy)

      npcurv = funcx(5)/2
      radius_max= 0
      do i=1,npcurv
         x = funcx(4+2*i)
         y = funcx(5+2*i)
         r = sqrt(x*x+y*y)
         radius_max = max(r,radius_max)
      enddo
      radius_max = radius_max-1d-6

      dstop_min=1e3
      dstop_av=0
      do i=1,npcurv-1
         x1 = funcx(4+2*i)
         y1 = funcx(5+2*i)
         x2 = funcx(6+2*i)
         y2 = funcx(7+2*i)
         ds = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
         dstop_min = min(dstop_min,ds)
         dstop_av  = dstop_av + ds
      enddo
      dstop_av = dstop_av/(npcurv-1)

      if (itracoption.eq.1) then
c        *** keep center of tracers a bit away from the edge
c         eps_tracer_bound= 1d-8
c sky
         eps_tracer_bound= 1d-6
      else if (itracoption.eq.2) then
c        *** For the marker chain we should keep the boundary tracers
c        *** at the boundary
         eps_tracer_bound= 0d0
      endif
      eps_elem = eps_tracer_bound

c     **** Check which elements are curved
      call ini050(kmesh(23),'detrminmax: coordinates')
      call ini050(kmesh(17),'detrminmax: nodalpoints')
      npoint = kmesh(8)
      nelem  = kmesh(9)
      nelgrp = kmesh(5)
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      call check_curved_elements(nelem,npoint,ibuffr(ikelmc),
     v        buffr(ikelmi))

      if (nelem.gt.NELEM_TOPO) then
        write(6,*) 'PERROR(detrminmax): nelem>NELEM_TOPO'
        write(6,*) 'increase NELEM_TOPO in elem_topo.inc'
        write(6,*) 'to at least ',nelem
        write(6,*) 'and recompile'
        call instop
      endif

c     *** We will keep track of top and bottom row of elements by raising a 
c     *** flag in the corresponding element numbers in boundary_elem(NELEM)
c     *** First, set all to false
      do i=1,nelem
         boundary_elem(i)=.false.
      enddo
      
c     *** Get topology of top and bottom row of elements. Save for more
c     *** accurate particle tracking.
c     *** NCURVS = total number of curves
      ncurvs=kmesh(11)
c     *** KMESH part M contains the nodal point numbers in the curves
      call ini050(kmesh(27),'detrminmax: nodal points in curves')
      ikelmm = iniget(kmesh(27))
c     *** NUMCURVS = number of curves at top or bottom (use composite, if necessary)
      numcurvs=2
      icurvs(1)=itop
      icurvs(2)=ibot
      call det_nods_boundary(ibuffr(ikelmm),ncurvs,numcurvs,icurvs,
     v               nodals,len_nodals,num_nodals,ip_nodals) 
c     *** KMESH part D contains the elements associated with nodal points
      call ini050(kmesh(18),'detrminmax: elements with nods in curves') 
      ikelmd = iniget(kmesh(18))
      call det_elems_boundary(nodals,num_nodals,ibuffr(ikelmd),
     v                        npoint,nelem,numcurvs,ip_nodals)

      write(6,*) 'top boundary has # points   : ',num_nodals(1)
c     write(6,'(100i6,$)') (nodals(i),i=1,num_nodals(1))
c     write(6,*) 
      write(6,*) 'bottom boundary has # points: ',num_nodals(2)
c     ipn = ip_nodals(2)
c     write(6,'(100i6,$)') (nodals(i+ipn-1),i=1,num_nodals(2))

c     *** Check how large the elements at top and bottom are and
c     *** figure out the threshold at which the tracing switches
c     *** to cylindrical coordinates (see predcoort and mark4c).
      rbot_max = 0d0
      rbot_min = 2d0
      rtop_max = 0d0
      rtop_min = 2d0
      do ielem=1,nelem
         if (boundary_elem(ielem)) then
            call get_elem_coords(ielem,ibuffr(ikelmc),buffr(ikelmi),
     v               nodno,xn,yn)
            do i=1,5,2
               r = sqrt(xn(i)*xn(i)+yn(i)*yn(i))
               if (r.gt.(r2+r1)/2) then
c                 *** top boundary
                  rtop_max = max(r,rtop_max)
                  rtop_min = min(r,rtop_min)
               else
c                 *** bottom boundary
                  rbot_max = max(r,rbot_max)
                  rbot_min = min(r,rbot_min)
               endif
            enddo
         endif
      enddo

c     *** rtop_threshold is the radius above which we will use
c     *** cylindrical coordinates for particle tracing
      rtop_threshold = rtop_min+(rtop_max-rtop_min)*0.5d0
      write(6,'(''Min/max radius of elements of top element row :'', 
     v        2f12.7)') rtop_max,rtop_min
      write(6,'(''     Threshold used for switch to cylindrical ''
     v        ''tracing  : '',f12.7)') rtop_threshold
c     *** rbot_threshold is the radius above which we will use
c     *** cylindrical coordinates for particle tracing
      rbot_threshold = rbot_min+(rbot_max-rbot_min)*0.5d0
      write(6,'(''Min/max radius of elements of bottom element row :'', 
     v        2f12.7)') rbot_max,rbot_min
      write(6,'(''     Threshold used for switch to cylindrical ''
     v        ''tracing  : '',f12.7)') rbot_threshold

      write(6,'(''     Minimum size of elements along top '',
     v          '' boundary '',f12.7,'' ('',f12.7,'' km)'')') 
     v          dstop_min,dstop_min*height_dim*1e-3
      write(6,'(''     Average size of elements along top '',
     v          '' boundary '',f12.7,'' ('',f12.7,'' km)'')') 
     v          dstop_av,dstop_av*height_dim*1e-3
      write(6,'(''     Minimum size of elements along bottom '',
     v          '' boundary '',f12.7,'' ('',f12.7,'' km)'')') 
     v          dsbot_min,dsbot_min*height_dim*1e-3
      write(6,'(''     Average size of elements along bottom '',
     v          '' boundary '',f12.7,'' ('',f12.7,'' km)'')') 
     v          dsbot_av,dsbot_av*height_dim*1e-3

      return
      end

c *************************************************************
c *   GET_ELEM_COORDS
c *   Get nodal point numbers and coordinates for element ielem
c *   PvK 020304
c *************************************************************
      subroutine get_elem_coords(ielem,kmeshc,coor,nodno,xn,yn)
      implicit none
      integer ielem 
      integer kmeshc(*),nodno(*)
      real*8 coor(2,*),xn(*),yn(*)
      integer i,ip

      ip = (ielem-1)*6
      do i=1,6
         nodno(i) = kmeshc(ip+i)
         xn(i)    = coor(1,nodno(i))
         yn(i)    = coor(2,nodno(i))
      enddo

      return
      end
 
c *************************************************************
c *   DET_ELEMS_BOUNDARY
c *   Determine the elements that are associated with the
c *   nodal points in nodals. Store in /elem_topo/ boundary_elems
c *
c *   nodals       i   nodal point numbers associated with curves
c *   num_nodals   i   array containing number of nodal points per curve
c *   kmeshd       i   KMESH part D (see Sepran PG)
c *   npoint       i   number of points in mesh
c *   nelem        i   number of elements in mesh
c *   numcurvs     i   number of curves associated with nodals
c *   ip_nodals    i   starting infomation for each curve
c *   
c *   PvK 020304
c *************************************************************
      subroutine det_elems_boundary(nodals,num_nodals,kmeshd,
     v            npoint,nelem,numcurvs,ip_nodals)
      implicit none
      integer num_nodals(*),npoint,nelem,nodals(*),kmeshd(*)
      integer ip_nodals(*),numcurvs
      integer i,nn,ne,ip,id2,ielem,j,ic
      include 'elem_topo.inc'
      
c     *** point to kmeshd part 2
      id2 = npoint+1
c     *** ip keeps track of the number of elements found
      ip = 0
c     *** loop over nodal points at this boundary
      do ic=1,numcurvs
         do i=1,num_nodals(ic)
            ip=ip+1
c           *** nodal point number         
            nn = nodals(ip)
c           *** associated number of elements
            ne = kmeshd(nn+1)-kmeshd(nn)
            do j=1,ne
               ielem = kmeshd(id2+kmeshd(nn)+j)
               boundary_elem(ielem)=.true.
            enddo
         enddo
      enddo

      return
      end

c *************************************************************
c *   DET_NODS_BOUNDARY
c *   Determine the nodal points associated with the the curves 
c *   icurvs.  Store in nodals(num_nodals).
c *
c *   kmeshm     i    kmesh part m (see Sepran PG)
c *   ncurvs     i    Number of curves stored in icurvs
c *   icurvs     i    array containing curve numbers
c *   nodals     o    nodal point numbers for all curves in icurvs
c *   num_nodals o    array containing number of nodal points in each curve
c *
c *   PvK 020304
c *************************************************************
      subroutine det_nods_boundary(kmeshm,ncurvs,numcurvs,icurvs,
     v        nodals,len,num_nodals,ip_nodals)
      implicit none
      integer kmeshm(*),ncurvs,icurvs(*),numcurvs,len,nodals(len)
      integer ninner,ip3,ip4,len4,num_nodals(*),ip
      integer i,ic,ip_nodals(*),ipn,num_nodals_tot,icurv

      ninner = kmeshm(1)
      ip3 = kmeshm(2)
      ip4 = kmeshm(3)
      len4 = kmeshm(4)
      ip_nodals(1)=0
      num_nodals_tot=0
      ipn = 1

      do ic=1,numcurvs
         ip_nodals(ic) = ipn
         icurv = icurvs(ic)
c        *** total number of points along ICURV
         num_nodals(ic) = kmeshm(4+icurv+1)-kmeshm(4+icurv)
c        *** position of first point of ICURV in kmeshm
         ip = ip3+kmeshm(4+icurv)-1
c        *** check to make sure the total number of nodal points 
c        *** doesn't exceed declared length of nodals(len)
         num_nodals_tot = num_nodals_tot + num_nodals(ic)
         if (num_nodals_tot.gt.len) then
            write(6,*) 'PERROR(det_topo_boundary): num_nodals>len'
            write(6,*) 'num_nodals: ',num_nodals_tot
            write(6,*) 'len       : ',len
            call instop
         endif
         do i=1,num_nodals(ic)
            nodals(i+ipn-1) = kmeshm(ip+i-1)
         enddo
         ipn = ipn + num_nodals(ic)
      enddo

      return
      end   

c *************************************************************
c *   CHECK_CURVED_ELEMENTS
c *   Check how many elements are curved. Set flags in 
c *   /elem_topo/ curved_elem(NELEM)
c *
c *   PvK 020304
c *************************************************************
      subroutine check_curved_elements(nelem,npoint,nodno,coor)
      implicit none
      integer nelem,npoint,nodno(*)
      real*8 coor(2,*),x(6),y(6)
      integer ielem,inpelm,i,icurved,ip
      real*8 dx,dy,fx,fy,eps
      logical curved
      include 'elem_topo.inc'

      eps = 1d-8
      inpelm = 6
      icurved = 0
      ip=0
      do ielem=1,nelem
         curved=.false.
         do i=1,inpelm
            x(i) = coor(1,nodno(ip+i))
            y(i) = coor(2,nodno(ip+i))
         enddo
         ip = ip+inpelm

         fx = abs( x(1)-2*x(2)+x(3) )
         fy = abs( y(1)-2*y(2)+y(3) )
         dx = abs(x(1)-x(3))
         dy = abs(y(1)-y(3))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.
         fx = abs( x(3)-2*x(4)+x(5) )
         fy = abs( y(3)-2*y(4)+y(5) )
         dx = abs(x(3)-x(5))
         dy = abs(y(3)-y(5))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.
         fx = abs( x(5)-2*x(6)+x(1) )
         fy = abs( y(5)-2*y(6)+y(1) )
         dx = abs(x(5)-x(1))
         dy = abs(y(5)-y(1))
         if (max(fx,fy).gt.max(dx,dy)*eps) curved=.true.

         if (curved) then
            curved_elem(ielem) = .true.
            icurved = icurved + 1
c           write(6,*) 'curved element: ',ielem
c           do i=1,6
c              write(6,'(5x,2f12.8)') x(i),y(i) 
c           enddo
         else
            curved_elem(ielem) = .false.
         endif
      enddo

      write(6,*) 'Percentage of curved elements: ',icurved/nelem*100, 
     v   ' out of  ',nelem

  
      return
      end
            
