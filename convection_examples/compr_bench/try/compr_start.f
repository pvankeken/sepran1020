c *************************************************************
c *   COMPRESS_START
c * 
c *   Starts application.
c *      Read user input
c *      Initializes SEPRAN
c *      Compute or read initial T condition
c *      Compute consistent velocity
c *
c *   PvK 070900/030904
c *************************************************************
      subroutine compr_start(iuser,user,NBUFDEF,NPMAX)
      implicit none
      integer iuser(*),NBUFDEF,NPMAX
      real*8 user(*)
      real*8 pi
      parameter(pi=3.141592 6535898)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
 
      integer NVMAX
      parameter(NVMAX=40 100)
      include 'mysepar.inc'
      include 'SPcommon/cbuffr'
      include 'petrac.inc'     
      include 'tracer.inc'
      include 'degas.inc'
      include 'pecont.inc'
      include 'pecof900.inc'
      include 'pecof800.inc'
      include 'powerlaw.inc'
      include 'peiter.inc'
      include 'pesteady.inc'
      include 'c1visc.inc'
      include 'vislo.inc'
      include 'petime.inc'
      include 'cpephase.inc'
      include 'depth_thermodyn.inc'
      include 'averages.inc'
      include 'dimensional.inc'
      include 'SPcommon/ctimen'
      include 'cbuoy_trac.inc'
      include 'compression.inc'
      include 'coolcore.inc'
      include 'perealdata.inc'
      include 'SPcommon/cmacht'
      include 'SPcommon/cmachn'
      include 'ccc.inc'
      include 'bound.inc'
      include 'crminmax.inc'
      include 'solutionmethod.inc'
      include 'cimage.inc'
      include 'penoniter.inc'
      include 'cqact.inc'
      include 'colimage.inc'
      include 'bstore.inc'
      include 'SPcommon/cplot'
      include 'plotpvk.inc'
      include 'cpix.inc'
      include 'c1mark.inc'
      include 'hegrid.inc'
      include 'pexcyc.inc'
      include 'cfilxy.inc'
c sky
      include 'csky.inc'

c     *** LOCAL VARIABLES
      integer iinput,nsup1,nsup2,kamt1,kamt2,i
      integer npoint,iinvec(2),ntot,nelem,inputcr(10)
      real*8 y0,rinputcr(10),volume_cyl
      real*8 volint,bounin,funccf,vrms2,vrms,vrms_d
      real*8 rincre(10),rinvec(2),q_a(20),q_a_d(20)
      integer irestart,inpcre(10),ipert(5),nq_a,inout,ir
      integer jsmoot,ielhlp,kmeshdum(NDEF),iincommt(10),ihelp,mcont
      integer no,ip,ipheat,inidgt,iiqtype,jtypv,kfollowchem
      character*80 fimage,command,rname
      real*8 format,contln,sumheat,rkap1,rkap2,heat_av
      integer ktypv,iall,ibp,numarr,j,inpelm,iiiqtype
      logical restart,start_from_newtonian,start_from_oldstorage
      logical read_velocity
      real*8 veloc(20),veloc_d(20),tstart,factor,yfaccn,dr_min
      real*4 t00,t01,second
      integer ntot2,ndist2,ntrac2,iout
      real*8 x2,y2,dtout2,tmax2,xp,yp,rho1,rho2
      integer mmkmesho,itracoption2
      include 'cjarvis.inc'

      data kmesh1(1),kmesh2(1),kprob1(1),kprob2(1),kmeshdum(1)
     v      /5*NDEF/
      data (iincommt(i),i=1,10)/10*0/
      data (rincre(i),i=1,10)/10*0d0/


      write(6,'(''************************************************'')') 
      write(6,'(''* Dimensional constants (from dimensional.inc) *'')')
      write(6,'(''* Expansivity          '',e12.5)') alpha_dim
      write(6,'(''* Diffusivity          '',e12.5)') rkappa_dim
      write(6,'(''* Conductivity         '',e12.5)') rk_dim
      write(6,'(''* <rho> mantle         '',e12.5)') rho_dim
      write(6,'(''* Radius CMB           '',e12.5)') R1_dim
      write(6,'(''* Radius Earth surface '',e12.5)') R2_dim
      write(6,'(''* rho core             '',e12.5)') rhoc_dim
      write(6,'(''* cp core              '',e12.5)') cp_dim
      write(6,'(''* BSE heating          '',e12.5)') QBSE_dim
      write(6,'(''* gravitational accel. '',e12.5)') g_dim
      write(6,'(''* specific heat        '',e12.5)') cp_dim
      write(6,'(''* Potential T at z=0   '',e12.5)') Tso_dim
      write(6,'(''* Timescale in Byr     '',e12.5)') tscale_dim
      write(6,'(''* velocity scale       '',e12.5)') velocity_scale
      write(6,'(''* pressure scale       '',e12.5)') pressure_scale
      write(6,'(''************************************************'')') 
   
c     *************************************************************
c     * USER INPUT
c     *************************************************************
      read(5,*) isolution_type,axi,cyl
      read(5,*) isolmethod,iluprec,ipreco,maxiter,iprint,keep,cgeps
      read(5,*) nsteadymax,eps,relax
      read(5,*) ncor,tfac,dtout_d,tmax_d,nbetween
      if (tfac.lt.0) then
c        *** negative value indicates output time steps are non-dimensional
         dtout_d = dtout_d/tscale_dim
         tmax_d  = tmax_d /tscale_dim
         tfac    = -tfac
      endif
      read(5,*) metupw,sky_filter
      if (axi.and.cyl) then
         write(6,*) '***************************************'
         write(6,*) '*   AXISYMMETRIC SPHERICAL GEOMETRY   *'
         write(6,*) '***************************************'
      else if (.not.axi.and.cyl) then
         write(6,*) '****************************'
         write(6,*) '*   CYLINDRICAL GEOMETRY   *'
         write(6,*) '****************************'
      else if (axi.and..not.cyl) then
         write(6,*) '****************************'
         write(6,*) '*   AXISYMMETRIC GEOMETRY  *'
         write(6,*) '****************************'
      else
         write(6,*) '****************************'
         write(6,*) '*   CARTESIAN GEOMETRY  *'
         write(6,*) '****************************'
      endif
      if (isolution_type.lt.0) then
c        *** steady state iteration
         steady = .true.
         write(6,899) nsteadymax,eps,relax
      else 
         steady=.false.
         write(6,900) ncor,tfac,dtout_d,tmax_d,nbetween
      endif


      read(5,*) Ra,Rb_local
      write(6,905) Ra,Rb_local

      read(5,*) nlay,(zint(i),i=1,nlay-1)
      write(6,910) nlay,(zint(i),i=1,nlay-1)

      read(5,*) ibot,itop,imid,r1,r2,rmid,half,quart,eighth,
     v          periodic,insulbot 

      if (.not.cyl) then
         if (r1.ne.0d0.or.r2.ne.1d0) then
            write(6,*) 'PERROR(compr_start): set r1=0 and r2=1 for '
            write(6,*) 'Cartesian geometry '
            call instop
         endif
      endif

      read(5,*) iclc,(icloc(i),i=1,iclc),nbp,(iboundpoints(i),i=1,nbp)

      read(5,*) jtypv,(viscl(i),i=1,nlay)
      if (itypv.ge.0) then
        itypv = jtypv-(jtypv/100)*100
        tackley = (jtypv.ge.100)
      else
        itypv = jtypv
        tackley = .false.
      endif
      write(6,915) itypv,tackley,(viscl(i),i=1,nlay)
      if (itypv.ge.4) then
         write(6,*) 'PERROR(compress): not suited for non-Newtonian yet'
         call instop
      endif
      read(5,*) (zqact(i),i=1,3),temp0,(aqact(i),i=1,4),sigma
      write(6,1195) (zqact(i),i=1,3),temp0,(aqact(i),i=1,4),sigma

      read(5,*) itracoption,ndist,kfollowchem,dr,dr_min,npix_radial
      ifollowchem = kfollowchem - (kfollowchem/100)*100
      chemwithrho = (kfollowchem/100)*100.eq.1
      if (chemwithrho) then
        write(6,*) 'PERROR(compress:chemwithrho): figure this out first'
        call instop 
      endif
c     *** itracoption   Option for tracer logic. See inittrac()
c     ***               1= distributions of tracers defined by dr and ndist
c     ***               2= one markerchain with marker distance dr
c     ***               9= synthetic tracer test without time integration
c     *** ndist         Number of tracer distributions/markerchains
c     *** ifollowchem   Option to follow chemistry in each tracer
c     *** dr            Average distance between particles
c     *** dr_min          remove markers that are closer to neighbor than dr_min
c     *** chemwithrho   Take compressibility into account?
      if (itracoption.eq.2) then
         nochain=1
         dm(1)=dr
         dm_min(1)=dr_min
      endif
      if (itracoption.eq.1.and.ndist.gt.NDISTMAX) then
         write(6,*) 'PERROR(compress): number of tracer distributions'
         write(6,*) 'too large: ',idist,' > ndistmax = ',NDISTMAX
         call instop
      endif

      write(6,1112) itracoption,ndist,ifollowchem,dr,dr_min,chemwithrho

c     *** read in specification of initial tracer distribution.
c     *** x0-x1 indicates the range in r, y0-y1 the range in 
c     *** theta. If itracoption=2 we use x1 to define the initial
c     *** radius of the markerchain.
      if (itracoption.eq.1) then
         ntot = 0
         do idist=1,ndist
            read(5,*) x0tr(idist),x1tr(idist),
     v                y0tr(idist),y1tr(idist)
c           *** ntrac         Number of tracers
c           *** x0tr ...      Coordinates for tracer distribution
            write(6,1191) idist,x0tr(idist),x1tr(idist),
     v                  y0tr(idist),y1tr(idist)
         enddo
      else if (itracoption.eq.2) then
         nochain = ndist
         ntot = 0
         do ichain = 1,nochain
            read(5,*) y0m(ichain),ainit(ichain)
            write(6,1196) ichain,y0m(ichain),ainit(ichain)
         enddo
      endif

c     *** numdegas_depth : number of different depths of degassing zones
c     ***                  this allows one to estimate the effects
c     ***                  of deep vs shallow melting
c     *** zdegas(i)      : array containing degassing depths (in SI)
      read(5,*) numdegas_depth,(zdegas(i),i=1,numdegas_depth)
      if (numdegas_depth.gt.NDEGAS_DEPTHS) then
        write(6,*) 'PERROR(compress): numdegas_depth is too large'
        write(6,*) 'numdegas_depth = ',numdegas_depth
        write(6,*) 'NDEGAS_DEPTHS = ',NDEGAS_DEPTHS
      endif
      write(6,1192) numdegas_depth
1192  format('Number of different degassing depths .... ',i10)
      do i=1,numdegas_depth
          rdegas(i)=r2*(R2_dim-zdegas(i))**2/(R2_dim**2)
          write(6,1193) rdegas(i),zdegas(i)*1e-3
      enddo
1193  format('    Degassing radius: ',f8.3,' (depth = ',f8.1,' km)')

c     *** for each different degassing depth range the number of
c     *** degassing zones is constant so we only read in ndegaszone(1)
c     *** (see initchem() and updatechem()).
      read(5,*) ndegaszone(1),vardegaszones,
     v      (thmindegas(1,j),thmaxdegas(1,j),j=1,ndegaszone(1))
      if (ndegaszone(1).gt.MAXDEGASZONE) then
          write(6,*) 'PERROR(compr_start): ndegaszone > MAXDEGASZONE'
          write(6,*) 'ndegaszone: ',ndegaszone(i)
          write(6,*) 'idegas_depth: ',i
          write(6,*) 'MAXDEGASZONE: ',MAXDEGASZONE
          call instop()
      endif
      write(6,1113) ndegaszone(1),vardegaszones
      do j=1,ndegaszone(1)
          write(6,1114) thmindegas(1,j),thmaxdegas(1,j),
     v         (thmaxdegas(1,j)-thmindegas(1,j))*R2_dim*1e-3
      enddo
      if (vardegaszones) then
         write(6,*) 'PERROR(compr_start): check var_degaszones()' 
         write(6,*) ' and recompile before using this option'
      endif

c     *** Graphical output of degassing zones
      call degaszone2dat()

      ifollowdegas=0
      if (ndegaszone(1).gt.0) ifollowdegas=1
c     *** Degassing logic:
c     *** ndegaszone - number of degassing zones
c     *** rdegaszone - depth of degassing zones
c     *** tmindegas,thmaxdegas - angular extent of degassing zones

c     *** ncontzone :  choice parameter for continent formation
c     ***              0   no continent formation (no U,Th extraction)
c     ***              >0  number of continent formation zones
c     ***              <0  start with mantle above interface zint(-ncontzone)
c     ***                  with all U,Th extracted at t=0 (as to mimic a two
c     ***                  step model of continent formation)
c     *** rcont     :    depth of cf zones
c     *** thmincont, thmaxcont: angular extent
c     *** imovecont  :   flag to move continent:
c     ***                0 = keep position fixed
c     ***               >0 = move after every imovecontth output timestep
      read(5,*) ncontzone,rcont,
     v              (thmincont(i),thmaxcont(i),i=1,ncontzone),
     v              imovecont,revol_cont
      write(6,1116) ncontzone,
     v              imovecont,revol_cont,rcont,
     v              (thmincont(i),thmaxcont(i),i=1,ncontzone)

      if (ncontzone.ne.0) then
         write(6,*) 'Do not use continent formation zones yet'
         call instop
      endif

      if (ncontzone.gt.1) then
         write(6,*) 'PERROR(compr_start): use ncontzone <= 1'
         call instop
      endif
      thmincont(2) = thmincont(1)
      thmaxcont(2) = thmaxcont(1)

      read(5,*) iiqtype,(q_layer_d(i),i=1,nlay)
c     *** iiqtype :   type of internal heating
c     ***         if >  10: interpret q_layer_d is non-dimensional value
c     ***         if > 100: multiply heating by local density
c     *** iqtype  :   iqtype = 1   constant heating
c     ***                      2   layer dependent heating
c     ***                      3   heating depends on tracer concentration
c     ***                      4   time-dependent heating based on BSE
c     ***             See findheatgen() for specifics
c     *** q_layer_d : fraction of BSE heating to be used
c     ***             BSE = 4.8 pW/kg or 19.2 TW for whole mantle
c     ***             Note: scaling depends on DeltaT_dim (see below)
      qwithrho = (iiqtype/100.eq.1)
      iiiqtype = iiqtype - (iiqtype/100)*100
      qnondim = (iiiqtype/10).eq.1
      iqtype = iiqtype - (iiqtype/100)*100 - (iiiqtype/10)*10
      write(6,920) iqtype,qwithrho,qnondim,(q_layer_d(i),i=1,nlay) 

      read(5,*) krestart,nsubmax,subeps
      if (krestart.lt.0) then
         irestart = krestart
      else
         irestart = krestart - 100*(krestart/100)
         irestart = irestart - 10*(irestart/10) 
      endif
      start_from_oldstorage = (krestart/100).eq.1
      start_from_newtonian = (krestart/10-100*(krestart/100)).eq.1
      read_velocity = .true.
c sky
c      if (krestart.ge.1000) read_velocity=.false.
      if (krestart.ge.1000) read_velocity=.false.
      
      restart = irestart.eq.1
      write(6,1103) irestart,restart,start_from_newtonian,
     v           start_from_oldstorage,nsubmax,subeps 
c     *** irestart   (1) the problem is restarted
c     ***            (0) initial run: use solution in file inicon
c     ***            (-1) initial run: use conductive profile
c     ***            (-2) initial run: use 
c     ***            (-100) initial run: use temperature field from benchmark 2 in Van Keken et al., 1997
c     *** start_from_newtonian: for non-Newtonian runs

      read(5,*) mcont,cool_core,t_bot,heat_flux_in
      write(6,1118) mcont,cool_core,t_bot,heat_flux_in
c     *** mcont: specifies how compressibility should be taken into account
c     ***        (see Sepran Standard Problems manual Chapter 7)
c     ***         mcont = 0 :  incompressible fluid
c     ***         mcont = 1 :  use div (rho u) = 0)
c     *** cool_core: take thermal evolution of core into  account?
c     *** t_bot:  initial non-dimensional temperature at CMB
c     *** heat_flux_in : non-dimension heat flow when
c     ***                natural boundary condition is used

      read(5,*) nth_output,nr_output,rav_output
c     *** nth_output,nr_output: resolution of regular (r,theta) grid
c     ***                       for output of temperature and viscosity
c     *** rav_output: flag specifying whether regular output should be made

      read(5,*) idiftype,ialphatype,pdif,palpha
      write(6,1121) idiftype,pdif,ialphatype,palpha
c     *** Depth dependent thermodynamical properties
c     *** idiftype (0/1) - 1 = depth dependent thermal conductivity
c     *** ialphatype (0/1) - 1 = depth dependent thermal expansity
c     ***   both dependences are like rho**exponent, where
c     *** pdif and palpha are the exponents, rho = exp(Di*z)

      open(9,file='pvk.in')
      read(9,*) iadiabat,addnabla2Tbar,divufromeos,Nu_with_Tbar
      read(9,*) intrule,intrule900
      read(9,*) ichtime,wavenumber
      close(9)


c     *** Sky's input for eclogite density and FK viscosity law
      write(6,*)'maximum viscosity change = ? order of magnitude'
      read(5,*) vis_ord
      write(6,*)'eclogite density calculation'
c upper mantle density      : rho_u_0
c density in transition zone: rho_u_tr
c lower mantle density      : rho_l
c initial deviation from transition zone  : drho0

c depth   for 1st density jump            : dep1
c density for 1st density jump            : drho1

c final depth for gradual density increase: dep2
c                 total   density increase: drho2

c density at cmb : rho_cmb

      read(5,*) rho_u_0,rho_u_tr,rho_l,rho_ecl0,rho_ecl_cmb
      read(5,*) dep1,drho1,dep2,drho2
c nondimensionalize
      dep1 = dep1/2885e3
      dep2 = dep2/2885e3
      drho_s1 = drho2/(dep2-dep1)
      drho_s2 = (rho_ecl_cmb-rho_ecl0-drho2-drho1)/(r2-r1-dep2)

c     *** Thermodynamic and phase change information
c     *** Important: scaling depends on dimensional value of DeltaT_dim
c     *** 
c     *** Rb - Phase boundary Rayleigh numbers
c     *** Di - dissipation number
c     *** DeltaT_dim - dimensional temperature contrast (K)
c     *** Tso_dim  - potential temperature at surface (K)
c     *** nph - number of phase changes
c     *** gamma - Clapeyron slope
c     *** phz0  - non-dimensional depth of the phase change
c     ***         ( NB!!!  surface=0, CMB=1)
c     *** phdz  - depth extent of the phase change
c     *** pht0 - temperature at the phase change
      read(5,*) Di,Grueneisen,DeltaT_dim,Tso_dim,nph,tala,tbar
      write(6,1115) Di,Grueneisen,DeltaT_dim,nph,tala,tbar
      if (DeltaT_dim.le.0) then
         write(6,*) 'PERROR(compr_start): DeltaT_dim <= 0'
         write(6,*) 'Set DeltaT_dim > 0 even if Di=0'
         call instop
      endif
      if (Di.gt.0.and.Tso_dim.eq.0) then
         write(6,*) 'PERROR(compr_start): Tso_dim = 0 and Di>0'
         call instop
      endif
      if (nph.gt.0) then
c        *** Code needs to be thoroughly evaluated for use with phase
c        *** changes and compressible convection
         write(6,*) 'PERROR(compr_start): nph > 0'
         write(6,*) '  not yet suited for phase changes '
         call instop
      endif
      if (nph.gt.0) then
         write(6,925) irestart
         write(6,'(''   i      gamma         phz0        phdz       '',
     v          ''pht0          P'')')
      endif
      do i=1,nph
         read(5,*) gamma_d(i),phz0_d(i),phdz_d(i),pht0_d(i),drho_rel(i)
c        *** Phase buoyancy number
         Rb(i) = Ra*drho_rel(i)/(alpha_dim*DeltaT_dim)
c        *** non-dimensional Clapeyron slope
         gamma(i)=gamma_d(i)*DeltaT_dim/(rho_dim*g_dim*height_dim) 
c        *** non-dimensional thickness of phase transition
         phdz(i) = phdz_d(i)/height_dim
c        *** non-dimensional temperature
         pht0(i) = pht0_d(i)/DeltaT_dim
c        *** non-dimensional depth of phase transition (make sure to
c        *** perform correct scaling for cylinder geometry; see
c        *** eq. 6 in Van Keken, PEPI, 2001.
         if (cyl.and..not.axi) then
            phz0(i)=r2*(1-(R2_dim-phz0_d(i))**2/(R2_dim**2))
         else 
            write(6,*) 'PERROR(compr_start): implement correct phase'
            write(6,*) 'change depth calculation'
            call instop
         endif
         if (Ra.eq.0) then
           write(6,'(i4,5f12.4)') i,gamma(i),phz0(i),phdz(i),pht0(i),0 
         else
           write(6,'(i4,5f12.4)') i,gamma(i),phz0(i),phdz(i),pht0(i),
     v            gamma(i)*Rb(i)/Ra
         endif
      enddo
      if (Ra.eq.0) then
         DiRa = 0
      else 
         DiRa = Di/Ra
      endif

c     *** Normal non-dimensionalization of time
      tmax = tmax_d * tscale_dim
      dtout = dtout_d * tscale_dim
      write(6,*) 'Dimensional model time (Byr) : ',tmax_d
      write(6,*) 'Non-dimensional model time   : ',tmax
      write(6,*) 'Dimensional output time (Byr): ',dtout_d
      write(6,*) 'Non-dimensional output time  : ',dtout


      if (Di.eq.0.and.(idiftype.ne.0.or.ialphatype.ne.0)) then
         write(6,*) 'PERROR(compr_start): Di=0 but alpha,k(z) specified'
         call instop
      endif

c     if (itracoption.ge.1.and.ncor.eq.0) then
c        write(6,*) 'PERROR(compr_start): itracoption>=1 and ncor=0'
c        write(6,*) 'Set ncor>0 when using tracers'
c        write(6,*) 'ncor=0 leads to quite inaccurate results'
c        call instop
c     endif

      rho_av = 1d0
      rho_z0 = 1d0
      if (Di.ne.0) then

        mcontv=10*mcont
        if (mcontv/10.eq.1) then
           write(6,*) 'Use compressible formulation '
           compress=.true.
           write(6,*) 'Di = ',Di
           rho1 = funccf(3,0d0,r1,0d0)
           rho2 = funccf(3,0d0,r2,0d0)
           write(6,*) 'rho = ',rho1,rho2
        else
           compress=.false.
        endif
        if (Grueneisen.ne.0) then 
           DiG = Di/Grueneisen
        else 
           DiG = Di
        endif
    
  
        if (cyl) then
          if (.not.axi) then
c         *** <rho> in cylindrical
             rho_av = (r2/DiG + 1d0/(DiG*DiG))*exp(-DiG*r2)
             rho_av = rho_av -(r1/DiG +1d0/(DiG*DiG))*exp(-DiG*r1)
             rho_av = -2*rho_av *exp(DiG*r2)/(r2*r2-r1*r1)
          else
c         ***  <rho> in axisymmetric spherical
             rho_av = (1d0/DiG*r1*r1 + 2d0/(DiG*DiG)*r1 +
     v                   2d0/(DiG*DiG*DiG))*exp(-DiG*r1) -
     v             (1d0/DiG*r2*r2 + 2d0/(DiG*DiG)*r2 +
     v                   2d0/(DiG*DiG*DiG))*exp(-DiG*r2)
             rho_av = 3*exp(DiG*r2)/(r2*r2*r2-r1*r1*r1)*rho_av
          endif
        else 
c         *** Cartesian geometry
          rho_av  = exp(DiG)/DiG - 1d0/DiG
        endif
        write(6,*) 'Average normalized density (rho0=1) is ',rho_av
        rho1 = funccf(3,0d0,r1,0d0)
        rho2 = funccf(3,0d0,r2,0d0)
        rho_z0 = rho2
        write(6,*) 'Dimensional density at top and bottom: ',
     v       rho_dim*rho1,rho_dim*rho2,r1,r2

      endif


      if (idiftype.ne.0) then
         rkap1 = funccf(4,r1,0d0,0d0)
         rkap2 = funccf(4,r2,0d0,0d0)
         write(6,*) 
         write(6,*) 'Diffusivity is variable. '
         write(6,'(''Non-dimensional range of values: '',2f12.3)') 
     v         rkap1,rkap2 
         write(6,'(''Dimensional range of values    : '',2e12.3)') 
     v         rkap1*rkappa_dim,rkap2*rkappa_dim
      endif
      if (ialphatype.ne.0) then
         write(6,*)
         write(6,*) 'Expansivity is variable. '
         write(6,'(''Non-dimensional range of values: '',2f12.3)')
     v       funccf(5,r1,0d0,0d0),funccf(5,r2,0d0,0d0)
         write(6,'(''Dimensional range of values: '',2e12.3)')
     v     alpha_dim*funccf(5,r1,0d0,0d0),alpha_dim*funccf(5,r2,0d0,0d0) 
      endif

c *************************************************************
c *   Specify internal heating; compute non-dimensional form if necessary
c *************************************************************
      if (iqtype.eq.1) then
         write(6,*) 
         write(6,*) 'Radiogenic heating is constant' 
         if (qnondim) then
            q_layer(1) = q_layer_d(1)
         else 
            write(6,*) 'Fraction of BSE heating: ',q_layer_d(1)
            q_layer(1) = q_layer_d(1)*QBSE_DIM*height_dim**2 /
     v             (cp_dim*rkappa_dim*DeltaT_dim)
         endif
         write(6,*) 'Non-dimensional heating: ',q_layer(1)
      else if (iqtype.eq.2) then
         write(6,*) 
         write(6,*) 'Radiogenic heating is depth dependent' 
         if (qnondim) then
            do i=1,nlay
              write(6,*) 'Layer: ',i
              q_layer(i) = q_layer_d(i)
              write(6,*) 'Non-dimensional heating: ',q_layer(i)
            enddo
         else
            do i=1,nlay
              write(6,*) 'Layer: ',i
              write(6,*) 'Fraction of BSE heating: ',q_layer_d(i) 
              q_layer(i) = q_layer_d(i)*QBSE_dim*height_dim**2 /
     v                  (cp_dim*rkappa_dim*DeltaT_dim)
              write(6,*) 'Non-dimensional heating: ',q_layer(i)
            enddo
         endif
      else if (iqtype.eq.3) then
         write(6,*) 
         write(6,*) 'PERROR(compr_start): iqtype = 3'
         write(6,*) 'The code has not been thoroughly tested for'
         write(6,*) 'this option yet'
         call instop
      else if (iqtype.eq.4) then
         write(6,*) 
         write(6,*) 'Radiogenic heating follows BSE heating starting'
         write(6,*) '4 Byr ago.'  
         write(6,*) 'PERROR(compr_start): iqtype = 4'
         write(6,*) 'has not been thorougly tested yet'
         call instop
      endif

      if (insulbot.and.cool_core) then
         write(6,*) 'PERROR(compr_start): you have specified the '
         write(6,*) 'mutually exclusive options of cooling the core' 
         write(6,*) 'while having an insulated bottom boundary.'
         write(6,*) 'Check input for insulbot and cool_core'
         call instop
      endif

        
c     ********************************************************
c     *   CONSTANTS specification
c     ********************************************************

c     *** Integer coefficients for Stokes and heat equations
c     *** see pefilcof
      if (isolmethod.eq.0) then
c        *** Penalty function method on quadratic elements
         interpol900 = 1
      else 
         interpol900 = 0
      endif
c     *** read from pvk.in
c     intrule900 = 0
c     intrule = 0
      if (axi) then
c        **** Axisymmetric geometry
         icoor900 = 1
         icoorsystem = 1
      else
         icoor900 = 0
         icoorsystem = 0
      endif

      if (icoorsystem.ne.icoor900) then
         write(6,*) 'PERROR(compr_start): icoorsystem <> icoor900'
         call instop
      endif

c     *** /cbuoy_trac/ ibuoy_trac
c     *** ibuoy_trac :  1  = use tracers stored in coormark
c     ***               2  = use tracers stored in coornewm (see pel3003.f)

c     *** mass matrix is diagonal (idia=1) or banded (idia=2)
      idia=2
      write(6,*) 'Shape of mass matrix (idia) ...... ',idia
      write(6,*) 'Type of upwinding (metupw) ....... ',metupw
c     *** raster image (dimension, colors) info
      ipmax = 400
      nsteps = 240
      nulstep=10

      tstart = 0

      jtypv=abs(itypv)
      ivl = jtypv.eq.1.or.jtypv.eq.3.or.jtypv.eq.5.or.jtypv.eq.7 
      if (ivl) jtypv = jtypv-1
      ivt = jtypv.eq.2.or.jtypv.eq.6
      if (ivt) jtypv = jtypv-2
      ivn = jtypv.eq.4
      write(6,*) 'Type of viscosity law: ',itypv
      write(6,*) 'IVL, IVT, IVN: ',ivl,ivt,ivn

c     *** /petrac/
      xi_eta_stored = 0

      call system('mkdir -p GMT averages PLOTS solutions images')

c     *************************************************
c     * SEPRAN START AND INITIALIZATION
c     *************************************************
      call start(0,1,-1,0)
      nbuffr=NBUFDEF

c     *** Read mesh information
      if (isolmethod.eq.0) then
c        *** Element 900, ISHAPE=6
c        *** direct solution method.
c        *** Read mesh files generated by e.g., program meshcyl
c        write(6,*) 'open unformatted mesh files: mesh1'
c        open(99,file='mesh1',form='unformatted')
c        call meshrd(-2,-99,kmesh1)
c        close(99)
c        write(6,*) 'open unformatted mesh files: mesh1'
c        open(99,file='mesh2',form='unformatted')
c        call meshrd(-2,-99,kmesh2)
c        close(99)
         if (periodic) then
           write(6,*) 'open formatted mesh files: meshoutput'
           open(99,file='meshoutput')
           call meshrd(-2,99,kmesh1)
           close(99)
           call nstmsh_periodic(kmesh1,kmesh2,0)
         else
           write(6,*) 'open formatted mesh files: mesh1'
           open(99,file='mesh1')
           call meshrd(-2,99,kmesh1)
           close(99)
           write(6,*) 'open formatted mesh files: mesh1'
           open(99,file='mesh2')
           call meshrd(-2,99,kmesh2)
           close(99)
         endif
      else 
         if (periodic) then
            write(6,*) 'PERROR(compr_start): periodic=.true.'
            write(6,*) 'needs some mods in compr_start for 903'
            call instop
         endif
c        *** Element 903, ISHAPE=3; 
c        *** Read linear mesh for velocity and temperature
         write(6,*) 'open formatted mesh file: mesh2'
         open(99,file='mesh2')
         call meshrd(-2,99,kmesh1)
         close(99)
         write(6,*) 'open formatted mesh file: mesh2'
         open(99,file='mesh2')
         call meshrd(-2,99,kmesh2)
         close(99)
      endif

      if (cyl) then
         call iniactmk(ibuffr,1,29,kmesh1,kmesh1,mmkmesho)
         call iniactmk(ibuffr,1,29,kmesh2,kmesh2,mmkmesho)
      endif


c     *** Check for correct grid if particle tracing is required
      inpelm = kmesh1(4)
      if (itracoption.ne.0.and.inpelm.ne.6) then
         write(6,*) 'PERROR(compr_start): particle tracing can only'
         write(6,*) 'be used with quadratic triangles for velocity'
         write(6,*) 'but the mesh for the Stokes equations has'
         write(6,*) 'elements with ',inpelm,' nodal points'
         call instop() 
      endif

c     *** Define problem 
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)
 
      numnat800 = kprob2(12)

c     *** Define type of matrix
      if (isolmethod.eq.-1) then
c        *** use element 903 with direct solution method
         write(6,*) 'Use element 903 with direct method'
c           *** matrix is asymmetric if compressibility is used
         if (compress) then
            write(6,*) 'Use compressible formulation'
         else
            write(6,*) 'Use incompressible formulation'
         endif
         call commat(2,kmesh1,kprob1,intmt1)
      else 
         if (isolmethod.eq.0) then
c           *** element 900; direct method. S is symmetric for 
c           *** incompressible flow 
            write(6,*) 'Use element 900 with direct method'
            iincommt(1) = 2
            if (compress) then
               write(6,*) 'PERROR(compr_start): '
               write(6,*) 'Element 900 is only suited for '
               write(6,*) 'penalty function method and div u = 0'
               call instop()
            else
               write(6,*) 'Use incompressible formulation'
               iincommt(2) = 1
            endif
         else
c           *** element 903; 
c           *** 6=compact non symmetric. 12=row/column compressed
            write(6,*) 'Use element 903 with iterative method'
            iincommt(1) = 4
            iincommt(2) = 6
            iincommt(4) = iluprec
         endif
         call matstruc(iincommt,kmesh1,kprob1,intmt1)
      endif
c     *** Define matrix for heat equation
      call commat(2,kmesh2,kprob2,intmt2)
c     *** Mass matrix has same structure as stiffness matrix for heat equation
c     *** (if idia=2)
      do i=1,5
         intmt2(i,2) = intmt2(i,1)
      enddo

c     *** Check to make sure mesh is not too big for allocated memory
      nsup2 = kprob2(29)
      nsup1 = kprob1(29)
      kamt1 = intmt1(5)
      kamt2 = intmt2(5,1)
      if (nsup1.gt.0.or.nsup2.gt.0) then
         write(6,'(''nsuper = '',2i10)') nsup1,nsup2
         write(6,'(''kamat  = '',2i10)') kamt1,kamt2
         call instop
      endif
      npoint  = kmesh2(8)
      nelem = kmesh1(9)
      if (periodic) nelem=kmesh1(kmesh1(21))
      if (npoint.gt.NPMAX) then
         write(6,*) 'PERROR(compr_start) npoint > npmax'
         write(6,*) 'npmax: ',npmax
         write(6,*) 'npoint: ',npoint
         call instop
      endif
      if (nelem.gt.MAXELEM) then
         write(6,*) 'PERROR(compr_start) nelem > MAXELEM'
         write(6,*) 'nelem: ',nelem
         write(6,*) 'MAXELEM: ',MAXELEM
         write(6,*) 'periodic: ',periodic
         call instop
      endif

c     *** Create solution vectors, including arrays islol1(1,2+3) that
c     *** are used for transport of information between problems
      call create(0,kmesh1,kprob1,isol1)
      call create(0,kmesh1,kprob1,islol1(1,1))
      call create(0,kmesh1,kprob1,islol1(1,2))
      call create(0,kmesh1,kprob1,islol1(1,3))
      call create(0,kmesh2,kprob2,isol2)

c     *** Find volume and surface area for this geometry
      iuser(2)=1
      iuser(6)=7
      iuser(7)=0
      iuser(8)=icoorsystem
      iuser(9)=0
      iuser(10)=-6
       user(6)=1d0
      volume = volint(0,1,1,kmesh2,kprob2,isol2,iuser,user,ielhlp)
      if (cyl) then
        if (axi) then
c         *** surf1 = r1, surf2=r2
          surf(1) = volume*3d0*r1*r1/(r2*r2*r2-r1*r1*r1)
          surf(2) = surf(1)*r2*r2/(r1*r1)
c         Use cylindrical volume to find frac (which represents an
c         relative angular fraction of the annulus, rather than a 
c         volume fraction).
          iuser(2)=1
          iuser(6)=7
          iuser(7)=0
c         *** Change coordinate system to Cartesian
          iuser(8)=0
          iuser(9)=0
          iuser(10)=-6
           user(6)=1d0
          volume_cyl=volint(0,1,1,kmesh2,kprob2,isol2,iuser,user,ielhlp)
          frac = volume_cyl/(pi*r2*r2-pi*r1*r1)
c         *** This would be the volume fraction:
c         *** frac = volume/(4d0/3d0*pi*(r2*r2*r2-r1*r1*r1))
c         *** modify frac to be consistent with cylindrical definition
c         *** frac = 0.5*frac
c         *** (end volume fraction)

          if (frac.gt.0.5) then
c            *** Can't use more than half an annulus for axisymmetric coordinates
             write(6,*) 'PERROR(compr_start): frac > 0.5 and axi' 
             write(6,*) '   cyl, axi   : ',cyl,axi
             write(6,*) '   frac       : ',frac
             call instop
          endif
          write(6,'(''icoorsystem                    : '',2i10)') 
     v                              icoorsystem,icoor900
          write(6,'(''volume                         : '',f8.4)') volume
          write(6,'(''surface area                   : '',2f8.4)') 
     v                                  surf(1),surf(2) 
          write(6,'(''frac (0.5=full spherical shell): '',f8.4)') frac

        else

          surf(2) = volume*2d0*r2/(r2*r2-r1*r1)
          surf(1) = surf(2)*r1/r2
          frac = volume/(pi*r2*r2-pi*r1*r1)
          write(6,'(''icoorsystem            : '',2i10)') icoorsystem,
     v                           icoor900
          write(6,'(''volume                 : '',f8.4)') volume
          write(6,'(''surface area           : '',2f8.4)') 
     v         surf(1),surf(2)   
          write(6,'(''frac (1=full cylinder) : '',f8.4)') frac
        endif
        dth_output = 2*frac*pi/nth_output
        dr_output  = (r2-r1)/nr_output
      else
c       *** Rectangular geometry; axisymmetric or Cartesian
        if (r2-r1.ne.1d0) then
           write(6,*) 'PERROR(compr_start): r2-r1 <> 1'
           write(6,*) 'r2-r1: ',r2-r1
           call instop
        endif
        surf(1) = volume
        surf(2) = volume
        if (iclc.ne.4) then
           write(6,*) 'PERROR(compr_start): iclc should be 4'
           write(6,*) 'for Cartesian models (cyl=.false.)'
           call instop
        endif
        ifilchoice=0
        do i=1,4
           ncurvn(i)=1
           icurv(i,1) = icloc(i)
        enddo
        call pefilxy(2,kmesh1,kprob1,isol1)
        rlampix = (xcmax-xcmin)/(ycmax-ycmin)
        frac=1d0
        write(6,'(''icoorsystem            : '',2i10)') icoorsystem,
     v                           icoor900
        write(6,'(''volume                 : '',f8.4)') volume
        write(6,'(''surface area           : '',2f8.4)') 
     v         surf(1),surf(2)   
        write(6,'(''rlampix                : '',f8.4)') rlampix
      endif

c     if (cyl) then
c       *** Create a (r,th) grid with approximately equal volume cells
c       *** similar to the bins used in Van Keken and Ballentine, 1998, 1999.
c       nrdiv=25
c       call make_div()
c     endif


c        *************************************************************
c        * Initialize temperature by a conductive solution (irestart=-1)
c        * or through a function (irestart=-2).
c        *************************************************************
c        *** Note: velocity b.c. are set in stokes()
c        *** set boundary conditions for temperature; t_bot is variable
c        *** in time if thermal cooling models are used.
         if (.not.insulbot) then
            call bvalue(0,2,kmesh2,kprob2,isol2,t_bot,ibot,ibot,1,0)
            write(6,*) 'set temperature at curve ',ibot,' to ',t_bot
         endif
         call bvalue(0,2,kmesh2,kprob2,isol2,0d0,itop,itop,1,0)
         write(6,*) 'set temperature at curve ',itop,' to ',0

         call conductive(kmesh2,kprob2,isol2,intmt2,matr2)
         call averageT(1,isol2,kmesh2,kprob2,iuser,user,avT_cond)
         write(6,*) 'Average T for conductive state: ',avT_cond(1)
c        *** compute heat flow through top and bottom curves.
         call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
         write(6,'(''       conductive solution q_a        : '',
     v                      3f8.2)') q_a(1),q_a(2),q_a(3)
         write(6,'(''       dimensional values (mW/m^2, C) : '',
     v               2f8.2,f8.0)')
     v                 q_a_d(1)*1000,q_a_d(2)*1000,q_a_d(3)

      if (irestart.le.-1) then
         if (irestart.eq.-1) then

            write(6,*) '               Add perturbation '
            write(6,*) 'irestart = -1: find conductive solution'
c           *** Compute and add perturbation. 
c           *** Total number of entries
            inpcre(1)=10
c           *** Number of vectors to be created
            inpcre(2)=1
c           *** Type of vector  (1=solution vector)
            inpcre(3)=1
c           *** problem number
            inpcre(4)=1
c           *** sequence number of array of special structure
            inpcre(5)=0
c           *** 0=real vector
            inpcre(6)=0
c           *** IFILL: filling is defined by IFILL commands
            inpcre(7)=3
c           *** use all degrees of freedom
            inpcre(8)=0
c           *** fill using FUNC with ichoice=
            inpcre(9)=1
c           *** fill all nodes
            inpcre(10)=0
            call creatv(kmesh2,kprob2,ipert,inpcre,rincre)
c           *** Add perturbation to conductive solution 
            iinvec(1)=2
            iinvec(2)=27
            rinvec(1)=1d0
            rinvec(2)=1d0
            call manvec(iinvec,rinvec,isol2,ipert,isol2,kmesh2,kprob2) 
c           *** compute heat flow through top and bottom curves.
            call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d) 
            write(6,'(''       conductive + pert,          q_a: '',
     v                             3f8.2)') q_a(1),q_a(2),q_a(3)
            write(6,'(''       dimensional values (mW/m^2, C) : '',
     v               2f8.2,f8.0)')
     v                 q_a_d(1)*1000,q_a_d(2)*1000,q_a_d(3)
  
         else if (irestart.eq.-2.or.irestart.eq.-100) then

c          *** Compute initial condition through func(11).
           write(6,*) 'irestart=-2 or -100: compute initial condition '
           write(6,*) '   from func'
c          *** length_inpre, no_vec, type_of_vec, iprob, ivec, icompl, ifill, 
c          *** Total number of entries
           inpcre(1)=10  
c          *** Number of vectors to be created
           inpcre(2)=1
c          *** Type of vector  (0 or 1=solution vector)
           inpcre(3)=0
c          *** problem number
           inpcre(4)=1
c          *** sequence number of array of special structure
           inpcre(5)=0
c          *** 0=real vector
           inpcre(6)=0
c          *** IFILL: filling is defined by IFILL commands
           inpcre(7)=1
c          *** fill first degree of freedom
           inpcre(8)=0
c          *** fill using FUNC with ichoice=100
           inpcre(9)=-irestart
c          *** fill all nodes
           inpcre(10)=0
           call creatv(kmesh2,kprob2,isol2,inpcre,rincre)
           call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
           write(6,*) '                 q_a            : ',
     v                    q_a(1),q_a(2),q_a(3),q_a(4)
            write(6,*) ' dimensional values (mW/m^2, C): ',
     v             q_a_d(1)*1000,q_a_d(2)*1000,q_a_d(3)
         endif

         if (itracoption.eq.0) then        
c           *** Calculate consistent velocity; see below for itractoption.ne.0
            write(6,*) 'Stokes : '
            call stokes(1,iuser,user)
            call pevrms9(isol1,kmesh1,kprob1,iuser,user)
            vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
            vrms = sqrt(abs(vrms2))
            write(6,*) 'Compute consistent Stokes solution, vrms: ',vrms
            vrms_d = vrms*rkappa_dim/height_dim*100*year_dim
            write(6,*) '                        vrms_d (cm/yr): ',vrms_d
 
            write(6,*)
c           *** Calculate consistent velocity; see below for itractoption.ne.0
            write(6,*) 'Stokes : '
            call stokes(1,iuser,user)
            call pevrms9(isol1,kmesh1,kprob1,iuser,user)
            vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
            vrms = sqrt(abs(vrms2))
            write(6,*) 'Compute consistent Stokes solution, vrms: ',vrms
            vrms_d = vrms*rkappa_dim/height_dim*100*year_dim
            write(6,*) '                        vrms_d (cm/yr): ',vrms_d
         endif

      else if (irestart.ge.0) then

c        *** Read solution from file
         f2name='start.back'
         namef2='start.back'
         call openf2(.false.)
c        *** Make sure to read the solution vectors in the right order.
         if (start_from_oldstorage) then
            write(6,*) 'read from old storage file: ',f2name
            call readbs(1,isol1,kprob1)
            call readbs(2,isol2,kprob2)
         else
            write(6,*) 'read from new storage file: ',f2name
            call readbs(1,isol2,kprob2)
            if (read_velocity) then 
               call readbs(2,isol1,kprob1)
            endif

         endif

c        *** enforce boundary conditions (note: Stokes b.c. are set 
c        *** in stokes)
         if (.not.insulbot) then
           call bvalue(0,2,kmesh2,kprob2,isol2,t_bot,ibot,ibot,1,0)
         endif
         call bvalue(0,2,kmesh2,kprob2,isol2,0d0,itop,itop,1,0)
c        *** compute consistent velocity
         write(6,*) 'Compute consistent velocity'
         call stokes(1,iuser,user)

         call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
         call pevrms9(isol1,kmesh1,kprob1,iuser,user)
         vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
         vrms = sqrt(abs(vrms2))

         write(6,*) 
         write(6,*) 'Initial condition read from ',
     v        f2name(1:30)
         write(6,*) 'q_a              : ',q_a(1),q_a(2),q_a(3)
         write(6,*) 'q_a_d (mW/m^2; C) : ',
     v                  q_a_d(1)*1000,q_a_d(2)*1000,q_a_d(3)
         write(6,*) 'vrms             : ',vrms
         vrms_d = vrms*rkappa_dim/height_dim*100*year_dim
         write(6,*) 'vrms_d (cm/yr)   : ',vrms_d
         call surfacevel(kmesh1,kprob1,isol1,1,veloc,veloc_d)
         write(6,*) 'surface velocity : ',veloc(1),veloc(2)

      endif


      if (cyl) call detrminmax(kmesh1,kprob1,isol1)

c     *** Create initial condition for markers/tracers
      if (itracoption.ne.0) then
         if (cyl) then
            write(6,'(''radius(min/max): '',2f10.7)') 
     v         radius_min,radius_max
            do idist=1,ndist
              if (x0tr(idist).lt.radius_min) x0tr(idist)=radius_min+1e-4
              if (x1tr(idist).gt.radius_max) x1tr(idist)=radius_max-1e-4
            enddo
         endif
         if (restart) then
            call readtrac('tracers.start')
         else
            call inittrac(coormark,kmesh1,kprob1,isol1)
         endif
      endif

      if (itracoption.eq.2) then
         if (cyl) then
c          *** Initialize radial pixel grid
c          *** Number of pixels in radial direction
           nrpix = npix_radial
           drpix = (r2-r1)/nrpix
           dr_pixel = drpix
c          *** Number of pixels in middle of the cylinder r=(r1+r2)/2
           nthpix = frac*pi*(r1+r2)*nrpix
c          *** dxpix is the angular extent of each pixel
           write(6,*) '***********************************************'
           write(6,*) 'Radial pixel grid specifications: '
           write(6,*) 'Extent : ',nthpix,' by ',nrpix
           write(6,*) 'Radial dimension of pixels: ',dr_pixel
           dthpix = frac*2*pi/nthpix
           do ir=1,nrpix
              dth_pixel(ir) = (r1+(ir-0.5)*drpix)*dthpix
           enddo
           write(6,*) 'Angular extent: ', dthpix
           write(6,*) 'Which translates to an average length of: '
           do ir=1,nrpix,10
              write(6,*) '     ',ir,dth_pixel(ir)
           enddo
           write(6,*) '***********************************************'
c          call mardiv(coormark,rname,iout)
 
         else 

           nypix = npix_radial
           nxpix = nypix * rlampix
           dxpix = rlampix/nxpix
           dypix = 1d0/nypix
           write(6,*) '**********************************************'
           write(6,*) 'Cartesian pixel grid specifications: '
           write(6,*) 'Extent : ',nxpix,' by ',nypix
           write(6,*) 'Dimension of pixels : ',dxpix,dypix
           write(6,*) '**********************************************'

         endif
    

         if (ifollowchem.ne.0) then
            nchem = NCHEMMAX
            if (restart) then
               call readchem(coorreal,chemmark,nchem,'chem.start')
            else
               call initchem(chemmark,coormark,kmesh2,kprob2,isol2,
     v            nchem,ncontzone,NTRACMAX)
            endif
         endif 
      endif

c     *** Create vectors for viscosity, tracers density and heatproduction
      inputcr(1)=7
      inputcr(2)=1
      inputcr(3)=2
      inputcr(4)=1
      inputcr(5)=1
      inputcr(6)=0
      inputcr(7)=-1
      if (ivn.or.tackley) then 
c        *** create vector containing the strain rate 
         write(6,*) 'Create strain rate vector'
         call creatv(kmesh2,kprob2,isecinv,inputcr,rinputcr)
      endif
      if (abs(itypv).ge.1) then
c        *** create vector containing the viscosity
         write(6,*) 'Create viscosity vector'
         call creatv(kmesh2,kprob2,ivisc,inputcr,rinputcr)
         call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,
     v                 isol2,ivisc,isecinv,iuser,user)
      endif
      if (iqtype.ge.1) then
c        *** create vector containing heat production
         write(6,*) 'Create heat production vector'
         call creatv(kmesh2,kprob2,iheat,inputcr,rinputcr)
      endif
      if (iqtype.eq.3) then
c        *** create vector containing tracer density
         write(6,*) 'Create tracer density vector'
         call creatv(kmesh2,kprob2,idens,inputcr,rinputcr)
      endif


      if (cyl) then
c        *** Provide mapping of current mesh element numbers onto
c        *** an equidistant (r,theta) grid. This will make it easier
c        *** to update the position of the tracers.
         write(6,'(''Create regular interpolation grid 
     v               (ieltogrid): '')') 
         t00 = second()
         call ieltogrid(kmesh1)
         t01 = second()
         write(6,*) 'ieltogrid took ',t01-t00,' seconds'
      endif

c     *** Test tracer interpolation scheme 
c     *** First, find elements and local coordinates (xi,eta)
      if (itracoption.ne.0) then
          write(6,*) '********************************************'
          write(6,*) '** Test tracer accuracy '
          write(6,*) '** 1: interpolation of analytical function '
          t00 = second()
          if (cyl) then
            ibuoy_trac=1
            call detelemtrac(1,user,'compr_start')
c           *** Then, check accuracy using a quadratic test function (see func(10)).
            call detelemtrac(10,user,'compr_start')
            t01 = second()
            write(6,*) '**    detelemtrac(2x) took ', t01-t00,' seconds'
          else
c            *** In Cartesian case, generate quadratic trial function using func(10)
             call copyvc(isol1,islol1(1,1))
c            *** Synthetic velocity; create velocity vector
c            *** Total number of entries
             inpcre(1)=10
c            *** Number of vectors to be created
             inpcre(2)=1
c            *** Type of vector  (1=solution vector)
             inpcre(3)=1
c            *** problem number
             inpcre(4)=1
c            *** sequence number of array of special structure
             inpcre(5)=0
c            *** 0=real vector
             inpcre(6)=0
c            *** IFILL: filling is defined by IFILL commands
             inpcre(7)=3
c            *** fill first degree of freedom
             inpcre(8)=1
c            *** fill using FUNC with ichoice=10
            inpcre(9)=10
c           *** fill all nodes
            inpcre(10)=0
            call creatv(kmesh1,kprob1,isol1,inpcre,rincre)
            call tracvel(kmesh1,kprob1,isol1,user,1)
            call check_accuracy_tracer_interpolation()
            t01 = second()
            write(6,*) '** check of accuracy of tracer interpolation 
     v                   took ', t01-t00,' seconds.'
            call copyvc(islol1(1,1),isol1)
          endif
       endif
       if (itracoption.ne.0) then
         if (.not.cyl.and.rlampix.ge.1d0.or.
     v        (.not.axi.and.r1.eq.0.4292.and..not.eighth)) then
            write(6,*) '**  '
            write(6,*) '** 2: Van Keken et al., JGR, 1997, benchmark'
c           *** Keep copies of initial conditions 
            call copyvc(isol1,islol1(1,1))
            x2 = coormark(1)
            y2 = coormark(2)
            dtout2 = dtout
            tmax2 = tmax
            ntot2 = ntot
            itracoption2=itracoption
            itracoption=1
            ndist2 = ndist
            ntrac2 = ntrac(1)
            dtout = 0.2203712
            tmax = 0.2203712
            if (cyl) then
               coormark(1) = 0.5       + 0.4192
               coormark(2) = 0.0159221 - 0.5
               if (quart) then
c                 *** rotate 45 degrees counterclockwise
                  xp = (coormark(1)-coormark(2))*sqrt(0.5d0)
                  yp = (coormark(1)+coormark(2))*sqrt(0.5d0)
                  coormark(1) = xp
                  coormark(2) = yp
               endif
            else
               coormark(1) = 0.5
               coormark(2) = 0.0159221
            endif
            ntot = 1
            ndist = 1
            ntrac(1) = 1
            if (cyl) call detelemtrac(1,user,'compr_start')
 
c           *** Synthetic velocity; create velocity vector
c           *** Total number of entries
            inpcre(1)=10
c           *** Number of vectors to be created
            inpcre(2)=1
c           *** Type of vector  (1=solution vector)
            inpcre(3)=1
c           *** problem number
            inpcre(4)=1
c           *** sequence number of array of special structure
            inpcre(5)=0
c           *** 0=real vector
            inpcre(6)=0
c           *** IFILL: filling is defined by IFILL commands
            inpcre(7)=3
c           *** fill first degree of freedom
            inpcre(8)=1
c           *** fill using FUNC with ichoice=4
            inpcre(9)=4
c           *** fill all nodes
            inpcre(10)=0
            call creatv(kmesh1,kprob1,isol1,inpcre,rincre)
c           *** fill second degree of freedom
            inpcre(8)=2
c           *** fill using FUNC with ichoice=5
            inpcre(9)=5
c           *** fill all nodes
            inpcre(10)=0
            call creatv(kmesh1,kprob1,isol1,inpcre,rincre)
            write(fname,'(''SYNVEL.000'')') 
            factor=0d0
            yfaccn=1d0
            format=10d0
            call plotvc(1,2,isol1,isol1,kmesh1,kprob1,format,yfaccn,
     v         factor)
            call advance_tracers_JGR97(iuser,user)
            write(6,*) '********************************************'

c           *** Restore values
            ntot = ntot2
            coormark(1) = x2
            coormark(2) = y2
            dtout = dtout2
            tmax = tmax2
            t = tstart
            ndist = ndist2
            ntrac(1) = ntrac2
            itracoption=itracoption2
            call copyvc(islol1(1,1),isol1)
         endif
      endif

      if (itracoption.eq.9) then
c        *** We're done since the above code portion executes the tracer test
         write(6,*) 'PINFO(compr_start): stop after tracer test ',
     v              'when itracoption=9'
         call instop()
      endif

      do i=6,iuser(1)
         iuser(i) =0
      enddo
      do i=6,user(1)
         user(i) = 0
      enddo
      call pevrms9(isol1,kmesh1,kprob1,iuser,user)
      vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
      vrms = sqrt(abs(vrms2))
      write(6,*) 'vrms before stokes: ',vrms
      if (itracoption.ne.0) then
c        *** Calculate consistent velocity
         call stokes(1,iuser,user)
         call pevrms9(isol1,kmesh1,kprob1,iuser,user)
         vrms2 = volint(0,1,1,kmesh1,kprob1,isol1,iuser,
     v               user,ihelp)/volume
         vrms = sqrt(abs(vrms2))
         write(6,*) 'Compute consistent Stokes solution with tracers, ',
     v      'vrms: ',vrms
         vrms_d = vrms*rkappa_dim/height_dim*100*year_dim
         write(6,*) '                          vrms_d (cm/yr): ',vrms_d
      endif


c     *** Check the initial heat production if it is based on the tracers
      if (itracoption.ne.0.and.iqtype.eq.3) then
           call tracheat(kmesh1,kprob1,coormark,chemmark,q_layer,
     v                 idens,iheat)
           iuser(2)=1
           iuser(6)=7
           iuser(7)=0
           iuser(8)=icoorsystem
           iuser(10)=2001
           iuser(11)=6
           call ini050(iheat(1),'compr_start: idens')
           ipheat=inidgt(iheat(1))
           do i=1,npoint
              user(6+i) = buffr(ipheat+i-1)
           enddo
           heat_av = volint(0,1,1,kmesh2,kprob2,iheat,iuser,user,ielhlp)
           write(6,*) 'average heat production: ',heat_av/volume
      endif

c     *** compute initial heat production
      if (iqtype.ne.0) then
c        *** Determine radiogenic heatproduction in nodal points
         call findheatgen(kmesh1,kprob1,coormark,chemmark,
     v        idens,iheat)
      endif

      call copyvc(isol1,islol1(1,1))
      call copyvc(isol2,islol2)

      open(20,file='vrms.dat')
      open(21,file='nusselt.dat')
      open(22,file='steadyval.dat')
      open(23,file='vrms_d.dat')
      open(24,file='q_d.dat')
      if (Di > 0.0d0) then
         open(25,file='phi.dat')
         open(26,file='work.dat')
      endif

      if (.not.steady) then
c        *** First output of time-dependent calculation
         if (.not.restart) then 
            inout=0
            call peplotit(kmesh1,kmesh2,kprob1,kprob2,
     v            isol1,isol2,islol1,ivisc,iheat,
     v            isecinv,iuser,user,inout)

            write(fimage,'(''images/temp.'',i5.5)') inout
            call image(kmesh2,kprob2,isol2,fimage)

c           *** Initialize output file file temporal data ; make
c           *** sure binary record length is consistent and stored in the
c           *** header 
!           open(66,file='compress.data',form='unformatted',
!    v           access='direct',recl=15*lenwor)
!           write(66,rec=1) 0,15
!           nodata = 0
!           close(66)
!           open(66,file='degas.data',form='unformatted',
!    v           access='direct',
!    v           recl=35*lenwor)
!           write(66,rec=1) 0,35
!           close(66)

c           *** output initial averages
            if (rav_output) then
              if ((10+npoint+nth_output*nr_output).gt.NPMAX) then
                 write(6,*) 'PERROR(compr_start): array user is too'
                 write(6,*) 'small at ',NPMAX
                 write(6,*) 'should be ',10+npoint+nth_output*nr_output 
                 return
              endif
              call averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                       user,nth_output,nr_output,0)
            endif
 
c           *** output initial tracer distribution
            if (itracoption.ne.0) then
              no=0
              call tracerout(coormark,coorreal,no)
            endif
            if (itracoption.eq.2) then
               iout=-1
               rname='tracerpix.000'
               call mardiv(coormark,rname,iout)
            endif

c           *** output initial chemistry in tracers
            if (ifollowchem.ne.0) then
               no=0
               call chemout(chemmark,coorreal,nchem,no)
            endif

         endif

      endif


899   format('STEADY STATE ITERATION ',/,
     v       'nsteadymax ............................... ',i8,/,
     v       'accuracy ................................. ',e8.3,/,
     v       'relaxation factor ........................ ',f8.3)
900   format('TIME DEPENDENT SOLUTION ',/,
     v       'ncor ..................................... ',i8,/,
     v       'tfac ..................................... ',f8.3,/,
     v       'dtout_d .................................. ',f8.3,/,
     v       'tmax_d ................................... ',f8.3,/,
     v       'nbetween ................................. ',i8  )
905   format('Ra ....................................... ',e8.3,/,
     v       'Rb_local ................................. ',e8.3,/,
     v       'Take variable rho into account in rhs? ... ',L8)
910   format('nlay ..................................... ',i8,/,
     v       'zint ..................................... ',10f8.3:)
915   format('itypv .................................... ',i8,/,
     v       'Use Tackley-s yield stress criterion? .... ',L8,/,
     v       'viscl .................................... ',10f8.3:)
920   format('iqtype ................................... ',i8,/,
     v       'Multiply heatproduction with rho(z)? ..... ',L10,/,
     v       'Is q_layer nondimensional? ............... ',L10,/,
     v       'q_layer .................................. ',10f8.3:)
925   format('irestart ................................. ',i8)
930   format('alpha        (y=0; y=1) .................. ',2f8.3,/,
     v       'conductivity (y=0; y=1) .................. ',2f8.3)
1000  format('Number of tracers: ',i7,' by ',i7,' = ',i7,/,
     v       'Top of the dense layer: ',f8.3)
1103  format(/,'Restart option ................. ',i10,/,
     v         'Restart ?  ..................... ',L10,/,
     v         'Start from Newtonian solution .. ',L10,/,
     v         'Start from old storage file (u,T)',L10,/,
     v         'Maximum number of subiterations  ',i10,/,
     v         'Accuracy of subiterations ...... ',f8.5)
1112  format(/,'Tracer option .................. ',i10,/,
     1         'Number of tracer distributions.. ',i10,/,
     2         'Choice for following chemicals.... ',i10,/,
     3         'Distance between particles ....... ',f12.7,/,
     3         'Minimum distance between particles ',f12.7,/,
     4         'Take compressiblity into account?  ',L10)
1113  format(/,'Number of degassing zones..... ...... ',i10,/,
     1         'Vary number and position of zones ... ',L10)
1114  format('    Angular extent  : ',2f9.5,' ( ',f8.1,' km)') 
1121  format(/,'Depth dependence of thermodynamical properties: ',
     v       /,'Diffusivity (0=constant) ...................... ',i8,
     v       /,'Powerlaw exponent for diffusivity ............. ',f8.3,
     v       /,'Thermal expansivity (0=constant) .............. ',i8,
     v       /,'Powerlaw exponent for expansivity ............. ',f8.3)
1115  format(/'Extended Boussinesq parameters: ',/,
     v        '    Di    ..................... ',f8.3,/,
     v        '    Gamma ..................... ',f8.3,/,
     v        '    DeltaT_dim ................ ',f8.3,/,
     v        '    Number of phase changes ... ',i8,/,
     v        '    Truncated Anelastic Liquid? ',L8,/,
     v        '    include adiabatic T-bar?    ',L8)
1116  format(/'Number of continent zones ....... ',i10,/,
     v        'Move continent option ........... ',i10,/,
     v        'revol_cont, rcont ............... ',2f8.3,/,
     v        'Angular extent .................. ',10f8.3:)
1118  format( 'Mcont (1=compressible convection) ',i10,/,
     v        'Secular cooling of the core ....? ',L10,/,
     v        'Initial CMB temperature ......... ',f8.3,/,
     v        'Heat flux input ................. ',f8.3)
1191  format('   Distribution ',  i5,
     v       '  Interval for r ............ ',2f12.3,/,
     1       '                ',  5x,
     2       '  Interval for theta ........ ',2f12.3)
1196  format('   Markerchain  ',  i5,/
     1       '      y0      = ',f12.3,/,
     2       '      ainit   = ',f12.3)
1195  format('Perovskite rheology: ',
     v       '    zqact    .............. ',3f8.4,/,
     v       '    temp0    .............. ',f8.4,/,
     v       '    aqact    .............. ',4e9.4,/,
     v       '    Tackley-s sigma parameter ',e12.3)
    
1200  format(/,'DIMENSIONAL REFERENCE VALUES: ',/,
     v       'Thermal expansivity ................... ',e15.7,/,
     v       'Density (mantle) ...................... ',e15.7,/,
     v       'Specific heat (mantle) ................ ',e15.7,/,
     v       'Surface temperature ................... ',e15.7,/,
     v       'Temperature difference across mantle .. ',e15.7,/,
     v       'Thermal conductivity .................. ',e15.7,/,
     v       'Thermal diffusivity ................... ',e15.7,/,
     v       'Gravity ............................... ',e15.7,/,
     v       'Dynamic viscosity ..................... ',e15.7,/,
     v       'Present day heat generation (W/kg) .... ',e15.7,/,
     v       'Density (core) ........................ ',e15.7,/,
     v       'Specific heat (core) .................. ',e15.7,/,
     v       'Outer radius (mantle) ................. ',e15.7,/,
     v       'CMB radius ............................ ',e15.7)


      end

      subroutine degaszone2dat()
      implicit none
      include 'degas.inc'
      include 'ccc.inc'
      integer i,j,k
      character*80 fname
      real*8 xd(4),yd(4)

      write(6,*) 'degaszone2dat: # depths = ',numdegas_depth,
     v       (ndegaszone(i),i=1,numdegas_depth)
      do i=1,numdegas_depth
         write(fname,'(''degas_zone.'',i1.1,''.dat'')') i
         open(44,file=fname)
         do j=1,ndegaszone(i)
           xd(1) = r2*sin(thmindegas(1,j))
           yd(1) = r2*cos(thmindegas(1,j))
           xd(2) = r2*sin(thmaxdegas(1,j))
           yd(2) = r2*cos(thmaxdegas(1,j))
           xd(3) = (r2-rdegas(i))*sin(thmaxdegas(1,j))
           yd(3) = (r2-rdegas(i))*cos(thmaxdegas(1,j))
           xd(4) = (r2-rdegas(i))*sin(thmindegas(1,j))
           yd(4) = (r2-rdegas(i))*cos(thmindegas(1,j))
           write(44,*) 
           write(44,*) (xd(k),yd(k),k=1,4),xd(1),yd(1)
           close(44)
        enddo
      enddo

      return
      end

      subroutine check_accuracy_tracer_interpolation()
      implicit none
      include 'petrac.inc'
      include 'c1mark.inc'
      include 'tracer.inc'
      integer nmark,ntot,i
      real*8 xm,ym,zm,func,uana,uint,du,dumax,dumin,dutot
      integer inaccurate
      
      if (itracoption.eq.2) then
         nmark = 0
         do ichain=1,nochain
            nmark = nmark + imark(nochain)
         enddo
         ntot = nmark
      else
         ntot = 0
         do idist=1,ndist
            ntot = ntot + ntrac(idist)
         enddo
      endif

      if (ntot.le.0) then
        write(6,*) 'PWARN(check_accuracy_tracer_interpolation): ntot<=0'
        return
      endif

      dutot=0
      dumin=1e6
      dumax=0
      inaccurate=0
      do i=1,ntot 
         uint = velmark(2*i-1)
         xm = coormark(2*i-1)
         ym = coormark(2*i)
         uana = func(10,xm,ym,zm)
         du = abs(uana - uint)
         dumax = max(dumax,du)
         dumin = min(dumin,du)
         dutot = du+dutot
         if (du.gt.1e-5) then 
             inaccurate = inaccurate + 1
             write(6,*) 'PWARN(check_accuracy): interpolation is 
     v           inaccurate: ',i,uana,uint,du
         endif
      enddo
      write(6,*) 'PINFO(check_accuracy): Av/min/max error in'
     v          ,' interpolation of quadratic function: ',
     v          dutot/ntot,dumin,dumax,inaccurate       

      return
      end
