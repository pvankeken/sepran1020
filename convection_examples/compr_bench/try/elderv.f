c *************************************************************
c *   ELDERV
c *
c *   Calculates viscous heating in the nodal points for element 400
c *   Calculate ONLY that contribution of the velocity gradients.
c *   For non-isoviscous flow one has to multiply this with the viscosity
c *   to get the viscous dissipation.
c *  
c *   Call using element number 41, icheld=1
c *
c *   This version specifically for axisymmetric cylindrical coordinates
c *
c *   PvK 9506
c *************************************************************
      subroutine elderv(icheld,ix,jdegfd,coor,elemvc,elemwg,iuser,user,
     v                  vectr0,vectr1,index1,index2)
      implicit none
      integer icheld,ix,jdegfd,iuser,index1(*),index2(*)
      real*8 coor(*),elemvc(*),elemwg(*),user(*),vectr0(*),vectr1(*)
      real*8 funccf
      include 'SPcommon/cactl'
      include 'pecof900.inc'
      integer i,j,k,ip
      real*8 un(6),vn(6),xn(6),yn(6),phi(6)

c     write(6,*) 'iuser: ',iuser
      if (itype.ne.41) then
         write(6,*) 'PERROR(elderv): itype <> 41'
         write(6,*) 'itype = ',itype
         call instop
      endif
      if (icheld.ne.1.and.icheld.ne.2) then
         write(6,*) 'PERROR(elderv): icheld <> 1,2'
         write(6,*) 'icheld= ',icheld
         call instop
      endif

      do k=1,6
         ip = index1(k)
         xn(k) = coor(2*(ip-1)+1)
         yn(k) = coor(2*(ip-1)+2)
         ip = index2(2*k-1)
         un(k) = vectr0(ip)
         ip = index2(2*k)
         vn(k) = vectr0(ip)
c        write(6,'(''u: '',6f15.7)') xn(k),yn(k),un(k),
c    v           funccf(1,xn(k),yn(k),0d0),
c    v           vn(k),funccf(2,xn(k),yn(k),0d0)
      enddo
      if (icoor900.eq.1) then
         call axivisdip(un,vn,xn,yn,elemvc,icheld)
      else
         call pfvisdip(un,vn,xn,yn,elemvc,icheld)
      endif


      do k=1,6
         elemwg(k) = 1d0
      enddo

      return
      end

c *************************************************************
c *   AXIVISDIP
c *
c *   Determine viscous dissipation in nodal points from velocity
c *   components for axisymmetric coordinates.
c *
c *   xn,yn   Coordinates of nodal points
c *   un,vn   Velocity components in nodal points
c *   phi     viscous dissipation in nodal points
c *   icheld  1=viscous dissipation
c *           2=log (viscous dissipation)
c *
c *   PvK 9506
c *   AKM 9812
c *************************************************************
      subroutine axivisdip(un,vn,xn,yn,phi,icheld)
      implicit none
      real*8 un(*),vn(*),xn(*),yn(*),phi(*)
      real*8 dpx,dpy
      dimension dpx(6),dpy(6)
      real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,dudx,dudy,dvdx,x,y
      real*8 dvdy,phic,phia
      integer k,i,j,icheld

      a1 = xn(3)*yn(5) - yn(3)*xn(5)
      a2 = -xn(1)*yn(5) + yn(1)*xn(5)
      a3 = xn(1)*yn(3) - yn(1)*xn(3)
      b1 = yn(3) - yn(5)
      b2 = yn(5) - yn(1)
      b3 = yn(1) - yn(3)
      c1 = xn(5) - xn(3)
      c2 = xn(1) - xn(5)
      c3 = xn(3) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      do k=1,6
        x = xn(k)
        y = yn(k)
c       *** Derivatives of quadratic shapefunctions x,y
        dpy(1) = 4*c1*c1*y + 4*a1*c1 - c1 + 4*b1*c1*x
        dpy(3) = 4*c2*c2*y + 4*a2*c2 - c2 + 4*b2*c2*x
        dpy(5) = 4*c3*c3*y + 4*a3*c3 - c3 + 4*b3*c3*x
        dpy(2) = 4*(2*c1*c2*y + a1*c2 + a2*c1 + b1*c2*x + b2*c1*x)
        dpy(4) = 4*(2*c2*c3*y + a2*c3 + a3*c2 + b2*c3*x + b3*c2*x)
        dpy(6) = 4*(2*c3*c1*y + a3*c1 + a1*c3 + b3*c1*x + b1*c3*x)

        dpx(1) = 4*b1*b1*x + 4*a1*b1 - b1 + 4*b1*c1*y
        dpx(3) = 4*b2*b2*x + 4*a2*b2 - b2 + 4*b2*c2*y
        dpx(5) = 4*b3*b3*x + 4*a3*b3 - b3 + 4*b3*c3*y
        dpx(2) = 4*(2*b1*b2*x + a1*b2 + b1*a2 + b2*c1*y + b1*c2*y)
        dpx(4) = 4*(2*b2*b3*x + a2*b3 + b2*a3 + b3*c2*y + b2*c3*y)
        dpx(6) = 4*(2*b3*b1*x + a3*b1 + b3*a1 + b1*c3*y + b3*c1*y)

c       do i=1,6
c          un(i) = xn(i)*yn(i)
c          vn(i) = yn(i)
c       enddo

        dudx = 0.
        dudy = 0.
        dvdx = 0.
        dvdy = 0.
        do i=1,6
           dudx = dudx + un(i)*dpx(i)
           dudy = dudy + un(i)*dpy(i)
           dvdx = dvdx + vn(i)*dpx(i)
           dvdy = dvdy + vn(i)*dpy(i)
        enddo
c       *** Viscous dissipation (if viscosity eta=1)
c       if (xn(k).gt.1e-7) then
c         phi(k) = 2*dudx*dudx + (dudy+dvdx)*(dudy+dvdx)+
c    v           2*dvdy*dvdy + 2*(un(k)*un(k)/(xn(k)*xn(k)))
c       else
c         phi(k) = 2*dudx*dudx + (dvdx+dudy)*(dudy+dvdx) +
c    v           2*dvdy*dvdy + 2*dudx*dudx
c       endif
        phi(k) = 2.0*dudx*dudx + (dvdx+dudy)*(dudy+dvdx)
     v  +2.0*dvdy*dvdy+2*(un(k)/x)**2
        if (x.eq.0) then
        phi(k) = 2.0*dudx*dudx + (dvdx+dudy)*(dudy+dvdx)
     v  +2.0*dvdy*dvdy+2*(dudx)**2
        endif
        if (icheld.eq.2) phi(k)=log10(phi(k))

        phic = 4*dudx*dudx + (dudy+dvdx)*(dudy+dvdx)
c       phia = 4*yn(k)*yn(k) + 2 + xn(k)*xn(k)

c       if (nint(y*100).eq.0) then
c       write(6,'('' phi: '',2f7.2,6f12.7,2f7.2)') 
c    v     xn(k),yn(k),(dudx+un(k)/x),dudy,dvdx,dvdy,
c    v     (dudx+un(k)/x+dvdy)/dudx,
c    v     phi(k)/phic,phi(k)/phia
c       write(6,*) 'phi: ',phi(k)/phic,phi(k)/phia
c       endif
      enddo

      return
      end

c *************************************************************
c *   PFVISDIP
c *
c *   Determine viscous dissipation in nodal points from velocity
c *   components.
c *
c *   xn,yn   Coordinates of nodal points
c *   un,vn   Velocity components in nodal points
c *   phi     viscous dissipation in nodal points
c *
c *   PvK 9506
c *************************************************************
      subroutine pfvisdip(un,vn,xn,yn,phi,icheld)
      implicit none
      real*8 un(*),vn(*),xn(*),yn(*),phi(*)
      integer icheld
      real*8 dpx,dpy
      dimension dpx(6),dpy(6)
      real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,dudx,dudy,dvdx,x,y
      integer k,i,j

      a1 = xn(3)*yn(5) - yn(3)*xn(5)
      a2 = -xn(1)*yn(5) + yn(1)*xn(5)
      a3 = xn(1)*yn(3) - yn(1)*xn(3)
      b1 = yn(3) - yn(5)
      b2 = yn(5) - yn(1)
      b3 = yn(1) - yn(3)
      c1 = xn(5) - xn(3)
      c2 = xn(1) - xn(5)
      c3 = xn(3) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      do 10 k=1,6
        x = xn(k)
        y = yn(k)
c       *** Derivatives of quadratic shapefunctions x,y
        dpy(1) = 4*c1*c1*y + 4*a1*c1 - c1 + 4*b1*c1*x
        dpy(3) = 4*c2*c2*y + 4*a2*c2 - c2 + 4*b2*c2*x
        dpy(5) = 4*c3*c3*y + 4*a3*c3 - c3 + 4*b3*c3*x
        dpy(2) = 4*(2*c1*c2*y + a1*c2 + a2*c1 + b1*c2*x + b2*c1*x)
        dpy(4) = 4*(2*c2*c3*y + a2*c3 + a3*c2 + b2*c3*x + b3*c2*x)
        dpy(6) = 4*(2*c3*c1*y + a3*c1 + a1*c3 + b3*c1*x + b1*c3*x)

        dpx(1) = 4*b1*b1*x + 4*a1*b1 - b1 + 4*b1*c1*y
        dpx(3) = 4*b2*b2*x + 4*a2*b2 - b2 + 4*b2*c2*y
        dpx(5) = 4*b3*b3*x + 4*a3*b3 - b3 + 4*b3*c3*y
        dpx(2) = 4*(2*b1*b2*x + a1*b2 + b1*a2 + b2*c1*y + b1*c2*y)
        dpx(4) = 4*(2*b2*b3*x + a2*b3 + b2*a3 + b3*c2*y + b2*c3*y)
        dpx(6) = 4*(2*b3*b1*x + a3*b1 + b3*a1 + b1*c3*y + b3*c1*y)

        dudx = 0.
        dudy = 0.
        dvdx = 0.
        do 20 i=1,6
           dudx = dudx + un(i)*dpx(i)
           dudy = dudy + un(i)*dpy(i)
           dvdx = dvdx + vn(i)*dpx(i)
20      continue
c       *** Viscous dissipation (if viscosity eta=1)
        phi(k) = 4*dudx*dudx + (dudy+dvdx)*(dudy+dvdx)
c       write(6,'('' phi: '',6f15.7)') xn(k),yn(k),dudx,dudy,dvdx,phi(k)
10    continue

      return
      end
