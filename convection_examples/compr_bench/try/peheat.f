c *************************************************************
c *   PEHEAT
c *
c *   Solves the heat equation
c *************************************************************
      subroutine peheat(kmesh,kprob,intmat,isol,islold,user,iuser,
     v                  matrs,irhsd)
      implicit none
      integer matrs(*),intmat(*),kmesh(*),kprob(*),isol(*)
      integer iuser(*),irhsd(*),islold(*),ielhlp
      real*8 user(*)

      call systm0(1,matrs,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matrs,isol,irhsd,intmat,kprob)

      return
      end

