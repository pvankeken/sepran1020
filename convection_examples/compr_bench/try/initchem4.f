c *************************************************************
c *   INITCHEM
c *
c *   Create initial distribution of chemistry
c *   971208: restructure to do 4 cases with different
c *           initial chemical distribution/continental zone
c *           distribution etc. in parallel.
c *           Each run now does 4 models in parallel:
c *              nocont:  no continents, just three ocean degassing zones
c *              norot :  a fixed continental zone
c *              rot   : a moving continental zone
c *              um    : form all continents at t=0
c *           For each case K we keep following information:
c *              chemmark(K-1+1,i)  : has it been degassed recently?
c *              chemmark(K-1+2,i)  : fraction of U,Th left
c *              chemmark(K-1+3,i)  : He4 concentration
c *              chemmark(K-1+4,i)  : fraction of He3 left
c *************************************************************
      subroutine initchem(chemmark,coortrac,kmesh,kprob,isol,
     v        nchem,ncontzone,NTRACMAX) 
      implicit none
      integer nchem,ncontzone
      real*8 chemmark(nchem,*),coortrac(2,*)
      integer kmesh(*),kprob(*),isol(*)
      integer nxtrac,nytrac,i,j,ip,NTRACMAX,ntot
      real*8 x,y,r,rdepleted

      include 'tracer.inc'
      include 'degas.inc'
      include 'powerlaw.inc'
      include 'ccc.inc'

      U238 = U238_0
      U235 = U235_0
      Th232 = Th232_0
      He4 = 0
      do i=1,4
         He4loss(i) = 0
         He3loss(i) = 0
         U235loss(i)= 0
         U238loss(i)= 0
         Th232loss(i)= 0
      enddo

      ntot = 0
      do idist=1,ndist
         ntot=ntot+ntrac(idist)
      enddo
      if (ntot.gt.NTRACMAX) then
         write(6,*) 'PERROR(initchem): ntot > NTRACMAX'
         write(6,*) 'ntot, NTRACMAX: ',ntot,NTRACMAX
         call instop
      endif
 
      rdepleted = zint(2)
      if (rdepleted.lt.r1.or.rdepleted.gt.r2) then
          write(6,*) 'PERROR(initchem): something wrong with '
          write(6,*) ' prescription of an initial depleted upper zone' 
          write(6,*) ' ncontzone       : ',ncontzone
          write(6,*) ' rdepleted       : ',rdepleted
          write(6,*) ' r2,r1           : ',r2,r1
          call instop
      endif

      do i=1,ntot
c           *** First three cases:
c           **** Start with primitive mantle: no He4, primitive He3, U,Th
c           *** Indication if it has been degassed recently
            chemmark(1,i) = 0d0
c           *** Initial fraction of U,Th
            chemmark(2,i) = 1d0
c           *** Initial concentration of He4
            chemmark(3,i) = 0
c           *** Initial fraction of He3
            chemmark(4,i) = 1
 
            chemmark(5,i) = 0d0
            chemmark(6,i) = 1d0
            chemmark(7,i) = 0
            chemmark(8,i) = 1

            chemmark(9,i) = 0d0
            chemmark(10,i) = 1d0
            chemmark(11,i) = 0
            chemmark(12,i) = 1

c           ***  Fourth case: start with depleted upper layers
            x = coortrac(1,i)
            y = coortrac(2,i) 
            r = sqrt(x*x+y*y)
            if (r.gt.rdepleted) then
c              *** Indication if it has been degassed recently (here: yes!)
               chemmark(13,i) = 1
               chemmark(14,i) = 0
               chemmark(15,i) = 0
               chemmark(16,i) = 0
            else
               chemmark(13,i) = 0
               chemmark(14,i) = 1
               chemmark(15,i) = 0
               chemmark(16,i) = 1
            endif
      enddo

      return
      end
