c *************************************************************
c *   MARDIV
c *   Create a pixel representation of the layer below a markerchain
c *   This subroutine combines the cylindrical and Cartesian box versions
c *   PvK 071504
c *************************************************************
      subroutine mardiv(coormark,rname,iout)
      implicit none
      real*8 coormark
      character*(*) rname
      integer iout
      include 'ccc.inc'
     
      if (cyl) then
         call mardiv_cyl(coormark,rname,iout)
      else
         call mardiv_cart(coormark,rname,iout)
      endif
     
      return
      end
 
c *************************************************************
c *   MARDIV_CYL
c *
c *   Create a (r,theta) raster file in which the value of the pixels (or bins)
c *   indicate the layer in which it is positioned.
c *   The layerboundaries are represented by the boundaries
c *   of the geometry and the markerchains
c *   (common c1mark)
c *   The algorithm is described in PVK 040690
c *
c *   PvK 050690/300790
c *   PvK 921013 : correction for y-direction: midpoint
c *   PvK 072704: change pixel grid coordinates to 'normal' x,y orientation
c *               with iy=1 corresponding to y=0.
c *   PvK 073004: adapted for cylindrical/axisymmetric spherical geometry
c *************************************************************
      subroutine mardiv_cyl(coormark,rname,iout)
      implicit none
      character*80 rname
      integer iout
      real*8 coormark(*)
      include 'cpix.inc'
      include 'ccc.inc'
      include 'pieltogrid.inc'
      include 'c1mark.inc'
      include 'hegrid.inc'
      include 'bound.inc'

      integer ip,ntot,nmark,iadd,ic
      real*8 x1,x2,y1,y2,ymid,xmid,rm1,rm2,th1,th2,rm,thm
      real*8 xend,yend,xm,ym
      integer NXMAX
      parameter(NXMAX=5 000)
      real*8 cx(NXMAX)
      integer icx(NXMAX),ir,ith
      integer ncmb,next,icut,ix,iy,im,i,j,ncut
      real*8 xstart,ystart,dth,theta,dr,temp
      real*8 pi
      parameter(pi=3.1415926 535897932)
      integer icol(256)
      data icol(1),icol(2),icol(3),icol(4),icol(5)/128,1,180,200,255/

      if (nochain.ne.1) then
         write(6,*) 'PERROR(mardiv): this subroutine is not'
         write(6,*) ' suited yet for anything but nochain=1'
         call instop
      endif
c     if (.not.quart) then
c        write(6,*) 'PERROR(mardiv): only suited for quarter '
c        write(6,*) 'geometry right now'
c        call instop
c     endif 

c     *** Determine which pixels are inside each markerchain
      ip = 0
      do ichain = 1,nochain
         nmark = imark(ichain)
c        write(6,*) 'Markerchain : ',ichain,nmark
c        write(6,*) 'Coordinates: '
c        do i=1,nmark
c           write(6,*) coormark(ip+2*i-1),coormark(ip+2*i)
c        enddo
         xstart=coormark(ip+1)
         ystart=coormark(ip+2)
         xend  =coormark(ip+2*nmark-1)
         yend  =coormark(ip+2*nmark)

c        *** Make the image by coloring the pixels below the
c        *** markerchain. For the cylindrical case we follow
c        *** lines of constant radius (y) so that theta (x)
c        *** moves more quickly

         if (nthpix*nrpix.gt.NPIXMAX) then
            write(6,*) 'PERROR(mardiv): nthpix*nrpix > NPIXMAX'
            write(6,*) 'nrpix*nthpix = ',nthpix*nrpix
            write(6,*) 'NPIXMAX     = ',NPIXMAX
            call instop
         endif

         do ir=1,nrpix
            do ith=1,nthpix
               ip = (ir-1)*nthpix+ith
               ipix(ip) = 0
            enddo
         enddo

         ip=0
c        *** Loop over lines of equal radius (centered at pixels)
         do ir=1,nrpix
            rm = r1 + (ir-0.5)*drpix
c           write(6,*) 'rm, ir: ',rm,ir

c           *** Find the locations where these lines intersect the 
c           *** markerchain.
c           *** First follow the active part of the markerchain
            icut = 0
            do im=2,nmark
               x1 = coormark(ip+2*im-3)
               y1 = coormark(ip+2*im-2)
               x2 = coormark(ip+2*im-1)
               y2 = coormark(ip+2*im)
               rm1 = sqrt(x1*x1+y1*y1)
               rm2 = sqrt(x2*x2+y2*y2)
               if (rm1.eq.0.or.rm2.eq.0) then
                 write(6,*) 'PERROR(mardiv):'
                 write(6,*) 'radius of marker is 0: ',rm1,rm2
                 write(6,*) '1: ',x1,y1
                 write(6,*) '2: ',x2,y2
                 write(6,*) 'im, nmark: ',im,nmark
                 call instop
               endif
               if (rm1.le.rm.and.rm2.gt.rm.
     v             or.rm1.ge.rm.and.rm2.lt.rm) then
c                 *** the markerchain is intersecting the current plane
c                 *** find the colatitude of the intersection
                  th1 = acos(y1/rm1)
                  th2 = acos(y2/rm2)
                  thm = th1+(rm-rm1)/(rm2-rm1)*(th2-th1)
                  icut = icut+1
                  cx(icut) = thm
               endif
            enddo
c           *** Add an intersection with the line theta=0 if this 
c           *** level is below the chain
            x1 = coormark(ip+1)
            y1 = coormark(ip+2)
            rm1 = sqrt(x1*x1+y1*y1)
            if (rm.lt.rm1) then
               icut=icut+1
               cx(icut) = 0
            endif
c           *** same if it is below the chain at theta=thetamax
            x1 = coormark(ip+2*nmark-1)
            y1 = coormark(ip+2*nmark)
            rm1 = sqrt(x1*x1+y1*y1)
            if (rm.lt.rm1) then
               icut=icut+1
               cx(icut) = frac*2*pi
            endif
c           *** sort this array
            ncut=icut
            do i=1,ncut
               do j=2,ncut
                  if (cx(j-1).gt.cx(j)) then
                     temp = cx(j-1)
                     cx(j-1) = cx(j)
                     cx(j) = temp
                  endif
               enddo
            enddo

c           *** add 1 to those pixels that are within the markerchain
            iadd=1
            if (ncut.gt.0) then
               icut=1
               do ith=1,nthpix
                  thm = (ith-0.5)*dthpix
c                 *** first test to see if we're to the right of 
c                 *** the markerchain     
                  if (thm.le.cx(1)) then
                      icx(ith)=0
                  else if (thm.ge.cx(ncut)) then
                      icx(ith)=0
                  else
c                     *** if not, count down through the layers
111                   continue
                      if (thm.le.cx(icut+1).and.thm.ge.cx(icut)) then
c                      *** pixel is within the volume
                       icx(ith) = iadd
                      else 
c                      *** try next interval
                       icut = icut + 1
                       iadd = mod(iadd+1,2)
                       if (icut.lt.ncut) then
                          goto 111
                       endif
                      endif
                    endif
               enddo

c              *** add the row to IPIX
               do ith=1,nthpix 
                  ic = (ir-1)*nthpix+ith
                  ipix(ic)=ipix(ic)+icx(ith)
               enddo
            endif

c       *** end loop over lines of equal radius
        enddo
c       write(6,'(200i1,:)') (ipix(ic),ic=1,nrpix*nthpix,nthpix)

c     *** end loop over chains
      enddo

      if (nrpix.le.60) then
        do ir=nrpix,1,-1
           ic = (ir-1)*nthpix
           write(6,'(200i1,:)') (ipix(i),i=ic+1,ic+nthpix)
        enddo
      endif

      if (iout.eq.-1) then
         ntot = nrpix*nthpix
         do ip=1,ntot
            ipix(ip) = icol(ipix(ip)+1)
         enddo
         call rasout(ntot,ipix,rname)
      endif

      return
      end

c *************************************************************
c *   MARDIV_CART (formerly known as MARRAS)
c *
c *   Create a rasterfile in which the value of the pixels
c *   indicate the layer in which it is positioned.
c *   The layerboundaries are represented by the boundaries
c *   of the geometry ([0,rlampix]x[0,1]) and the markerchains
c *   (common c1mark)
c *   The algorithm is described in PVK 040690
c *
c *   PvK 050690/300790
c *   PvK 921013 : correction for y-direction: midpoint
c *   PvK 072704: change pixel grid coordinates to 'normal' x,y orientation
c *               with iy=1 corresponding to y=0.
c *   PvK 071705: reverse pixel values: 1=bottom layer; 0=top layer.
c *               This is in general more efficient since the bottom layer
c *               is typically thinner. 
c *************************************************************
      subroutine mardiv_cart(coormark,rname,iout)
      implicit none
      real*8 coormark(*)
      character*(*) rname
      integer iout
      integer NYMAX
      parameter(NYMAX=5 000)
      real*8 cy(NYMAX)
      integer icy(NYMAX)

      include 'cpix.inc'
      include 'c1mark.inc'
      real*8 dyl,xm,ym,x1,x2,y1,y2,temp
      integer i,ip,nmark,ix,iy,j,it,iregio,icut,im,ic,ntot,iadd
      integer icol(256)
      data icol(1),icol(2),icol(3),icol(4),icol(5)/128,1,180,200,255/

      if (rlampix.eq.0) stop 'mardiv_cart: rlam=0'
      if (nxpix.eq.0) stop 'mardiv_cart: nxpix=0'
      dyl = 1d0/nypix
c     *** Initialize
      do i=1,nxpix*nypix
         ipix(i) = 0
      enddo
      ip = 0
      do ichain=1,nochain
         nmark = imark(ichain)
c        write(6,*) 'ichain, nmark: ',ichain,nmark
c        *** Loop over columns
         do ix=1,nxpix
c           *** Determine in which pixels the markerchain
c           *** intersects the column. Start with the lower
c           *** boundary (y=0,iy=1) and finish with the
c           *** upper one (y=1,iy=nypix) 
            xm = (0.5+(ix-1))*rlampix/nxpix
            icut=1
            cy(1)=0d0
            do im=2,nmark
               x1 = coormark(ip+2*im-3)
               x2 = coormark(ip+2*im-1)
               if (x1.le.xm.and.x2.gt.xm.or.x1.ge.xm.and.x2.lt.xm) then
c                 *** the markerchain is intersecting the current column
                  y1 = coormark(ip+2*im-2)
                  y2 = coormark(ip+2*im)
                  ym = y1+(xm-x1)/(x2-x1)*(y2-y1)
                  icut = icut+1
                  cy(icut) = ym
c                 write(6,*) 'ix, icut, ym: ',ix,icut,ym
               endif
            enddo
            icut=icut+1
            cy(icut)=1d0
c           *** sort this array
            do i=2,icut-1
               do j=3,icut-1
                  if (cy(j-1).gt.cy(j)) then
                     temp = cy(j-1)
                     cy(j-1) = cy(j)
                     cy(j) = temp
                  endif
               enddo
            enddo
c           if (icut.gt.3) write(6,*) 'ix, cy: ',icut,(cy(j),j=1,icut)
c           *** Figure out which pixels are below the markerchain
            iadd=0
c           *** go from the top down; top layer will be indicated by '0', bottom layer '1'
c           *** Change the above line to 'iadd=1' for the reverse
            do iy=nypix,1,-1
               ym = (iy-0.5)*dyl
111            continue
                 if (ym.le.cy(icut).and.ym.ge.cy(icut-1)) then
c                   *** pixel lies in current interval
                    icy(iy) = iadd
                 else
c                   *** Try next interval
                    icut = icut-1
                    iadd = mod(iadd+1,2)
                    if (icut.ge.2) then
                        goto 111
                    else 
                        write(6,*) 'problem in mardiv'
                        call instop
                    endif
                 endif
c                write(6,*) '   ',ym,icut,cy(icut),cy(icut+1),icy(iy)
 
            enddo
c           *** Now add this to IPIX
            do iy=1,nypix
               ic = (iy-1)*nxpix + ix
               ipix(ic)=ipix(ic)+icy(iy)
            enddo
c           if (ix.eq.1) then
c              write(6,'(''pix:'',60i1,:)') 
c    v           (ipix((iy-1)*nxpix+1),iy=1,nypix)
c           endif
         enddo
         ip = ip+2*nmark
      enddo

      if (iout.eq.-1) then
         ntot = nxpix*nypix
c        *** Represents the layers 1,2,3 ... with colors stored in
c        *** icol
         do ip=1,ntot
            ipix(ip) = icol(ipix(ip)+1)
         enddo
         call rasout(ntot,ipix,rname)
      endif

      return
      end


