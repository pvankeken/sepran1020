c *************************************************************
c *   PEMAPINTERPOLATE
c *   
c *   Find values of TEMP in barycenter (store in rmap)
c *************************************************************
      subroutine pemapinterpolate(ichois,kmeshc,coord,sol,map,rmap,  
     v              indprf,kprobf,nunkp,nelem)
      implicit none
      integer ichois,kmeshc(*),map(*),nelem
      real*8 coord(2,*),sol(*),rmap(*)
      integer nodno(7),ielem,i,nodlin(3),ip,indprf,kprobf(*)
      integer nunkp,j1,j2,j
      real*8 xlin(6),ylin(6),tlin(3),xbar,ybar,tbar,u,v,uint,vint
      real*8 ulin(2,6),xm,ym,shapef(6),sum

      if (ichois.eq.1) then
c        *** interpolate temperature for Stokes equation
         do ielem = 1,nelem
            do i=1,7
               nodno(i) = kmeshc((ielem-1)*7+i)
            enddo
c           *** values of temperature in innermost linear element
            do i=2,6,2
               ip = map(nodno(i))
               if (ip.le.0) then
                  write(6,*) 'PERROR(pemapint) mapping is incorrect: '
                  write(6,*) 'ielem = ',ielem
                  write(6,*) 'nodno: ',nodno
                  write(6,*) 'map: ',(map(nodno(j)),j=1,7)
                  call instop
               endif
               tlin(i/2) = sol(ip)
            enddo   
            tbar = (tlin(1)+tlin(2)+tlin(3))/3d0
c           *** Store tbar in appropriate location
            rmap(ielem)=tbar
         enddo
      else if (ichois.eq.2) then
c        *** interpolate u*u+v*v in barycenter for VRMS calculation
         if (indprf.eq.0) then
            write(6,*) 'this piece of code is incomplete'
            write(6,*) 'in pemapinterpolate'
            call instop
            do ielem = 1,nelem
               do i=1,7
                  nodno(i) = kmeshc((ielem-1)*7+i)
               enddo
               do i=2,6,2
                  ip = map(nodno(i))
                  if (ip.le.0) then
                    write(6,*) 'PERROR(pemapint) mapping is incorrect: '
                    write(6,*) 'ielem = ',ielem
                    write(6,*) 'nodno: ',nodno
                    write(6,*) 'map: ',(map(nodno(j)),j=1,7)
                    call instop
                  endif
                  j1 = (i-1)*nunkp+1
                  j2 = j1+1
                  u = sol(j1)
                  v = sol(j2)
                  ulin(i,1) = u
                  tlin(i) = v
               enddo
               rmap(ielem) = uint*uint+vint*vint
            enddo
         else
            do ielem=1,nelem
               do i=1,7
                  nodno(i) = kmeshc((ielem-1)*7+i)
               enddo
               do i=1,7
                  xlin(i) = coord(1,nodno(i))
                  ylin(i) = coord(2,nodno(i))
               enddo
               xm = coord(1,nodno(7))
               ym = coord(2,nodno(7))
c              write(6,*) 'nodno: ',(nodno(i),i=1,7)
c              write(6,*) 'x    : ',(xlin(i),i=1,6),xm
c              write(6,*) 'y    : ',(ylin(i),i=1,6),ym
               do i=1,6
                  ip = map(nodno(i))
                  if (ip.le.0) then
                    write(6,*) 'PERROR(pemapint) mapping is incorrect: '
                    write(6,*) 'ielem = ',ielem
                    write(6,*) 'nodno: ',nodno
                    write(6,*) 'map: ',(map(nodno(j)),j=1,7)
                    call instop
                  endif
                  j1 = kprobf(ip)+1
                  j2 = j1+1
                  u = sol(j1)
                  v = sol(j2)
                  ulin(1,i) = u
                  ulin(2,i) = v
c                 write(6,'(''u,v: '',4i5,2f8.3)') ielem,ip,j1,j2,
c    v                u,v,i-1,i
               enddo
               call detshape6(xlin,ylin,xm,ym,shapef)
               sum=0
               do i=1,6
                  sum = sum+shapef(i)
               enddo
c              write(6,*) 'shapef: ',(shapef(i),i=1,6),sum
               uint=0
               vint=0
               do i=1,6
                  uint = uint + ulin(1,i)*shapef(i)
                  vint = vint + ulin(2,i)*shapef(i)
               enddo
               rmap(ielem) = uint*uint+vint*vint
c              write(6,*) 'pemapinterpolate: ',
c    v           ielem,uint,vint,rmap(ielem)
            enddo 
         endif
      else
         write(6,*) 'PERROR(pemapinterpolate)'
         write(6,*) 'ichois should be between 1 and 2'
         write(6,*) 'ichois= ',ichois
         call instop
      endif

      return 
      end

c *************************************************************
c *   LININT
c *   Linear interpolation of T on triangle defined by (xn,yn)
c *   PvK 991019
c *************************************************************
      subroutine linint(xn,yn,t,xm,ym,tm)
      implicit none
      real*8 xn(3),yn(3),t(3),xm,ym,tm
      real*8 a1,b1,c1,a2,b2,c2,a3,b3,c3,delta,rl1,rl2,rl3

      a1 =  xn(2)*yn(3) - yn(2)*xn(3)
      a2 = -xn(1)*yn(3) + yn(1)*xn(3)
      a3 =  xn(1)*yn(2) - yn(1)*xn(2)
      b1 =  yn(2) - yn(3)
      b2 =  yn(3) - yn(1)
      b3 =  yn(1) - yn(2)
      c1 =  xn(3) - xn(2)
      c2 =  xn(1) - xn(3)
      c3 =  xn(2) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      rl1 = a1 + b1*xm + c1*ym
      rl2 = a2 + b2*xm + c2*ym
      rl3 = a3 + b3*xm + c3*ym

      tm = rl1*t(1)+rl2*t(2)+rl3*t(3)
     
      return 
      end

c *************************************************************
c *   PRINTMAP
c *
c *   Print temperature as mapped and interpolated onto extended
c *   triangle mesh (ISHAPE=7)
c *   PvK 991019
c *************************************************************
      subroutine printmap(map,rmap,temp,coord,npoint)
      implicit none
      real*8 rmap(*),temp(*),coord(2,*),t
      integer map(*),npoint,i,ip

      do i=1,npoint
         ip = map(i)
         if (ip.lt.0) then
            t = rmap(-ip)
         else
            t = temp(ip)
         endif
c        write(6,'(2i5,4f12.7)') i,ip,coord(1,i),coord(2,i),t, 
c    v         coord(1,i)+coord(2,i)
      enddo
      return
      end

c *************************************************************
c *   DETSHAPE6
c *
c *   Determine value of shapefunctions of 
c *   quadratic triangular element (xn,yn) at point (xm,ym)
c *
c *   PvK 990408
c *************************************************************
      subroutine detshape6(xn,yn,xm,ym,shapef)
      implicit none
      real*8 xn(6),yn(6),xm,ym,shapef(6)
      real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,rl1,rl2,rl3
      real*8 rl123

      a1 = xn(3)*yn(5) - yn(3)*xn(5)
      a2 = -xn(1)*yn(5) + yn(1)*xn(5)
      a3 = xn(1)*yn(3) - yn(1)*xn(3)
      b1 = yn(3) - yn(5)
      b2 = yn(5) - yn(1)
      b3 = yn(1) - yn(3)
      c1 = xn(5) - xn(3)
      c2 = xn(1) - xn(5)
      c3 = xn(3) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      rl1 = a1 + b1*xm + c1*ym     
      rl2 = a2 + b2*xm + c2*ym     
      rl3 = a3 + b3*xm + c3*ym     
         
      shapef(1) = rl1*(2*rl1-1) 
      shapef(3) = rl2*(2*rl2-1) 
      shapef(5) = rl3*(2*rl3-1) 
      shapef(2) = 4*rl1*rl2 
      shapef(4) = 4*rl2*rl3 
      shapef(6) = 4*rl3*rl1 
 
      return
      end

