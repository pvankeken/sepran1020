c *************************************************************
c *   COOLCORE
c *
c *   Implementation of core cooling in mantle convection models.
c *   Solves heat balance for the core, assuming no thermal inertia
c *   in the core. In dimensional terms:
c *
c *   rho_c * c_c * V_core * dT_c / dt = - q_c * A_cmb               (1)
c *
c *   where rho_c = mean density of the core (11 g/cm^3)
c *           c_c = mean heat capacity of the core ( 500 J/kgK)
c *        V_core = volume of the core
c *        A_cmb  = surface of the CMB
c *          q_c  = average heat flow through the CMB 
c *
c *   This can be rewritten as
c *
c *    dT_c / dt = -3*q_c /( rho_c * c_c * R_cmb)                    (2)
c * 
c *   or in non-dimensional form
c *
c *    dT_c / dt = -3 q_c * h * rho_m * c_m / (rho_c * c_c * R_cmb)  (3)
c *
c *   where rho_m  = mean density of the mantle (4.5 g/cm^3)
c *           c_m  = mean heat capacity of the mantle (1250 J/kgK)
c *             h  = depth of the mantle (2800 km)
c *         R_cmb  = radius of the core (3600 km).
c *   In (3), the quantities T_c, q_c and t are non-dimensional.
c *
c *
c *   In spherical coordinates this yields
c *      dT_c / dt = -2.539  * q_c
c *        
c *   PvK 990903
c *************************************************************
      subroutine coolcore(kmesh2,kprob2,isol2,iuser,user)
      implicit none
      integer kmesh2(*),kprob2(*),isol2(*),iuser(*)
      real*8 user(*)
      include 'SPcommon/ctimen'
      include 'coolcore.inc'
      include 'bound.inc'
      include 'ccc.inc'
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'
      real*8 x,y,z,q_a(10),factor,q_a_d(10)
      integer nq_a
     
      call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
         
      q_c = q_a(2)

c     write(6,*) 't_bot before: ',t_bot
      factor =  3*rho_dim*cp_dim*height_dim/
     v            (rhoc_dim*cpc_dim*R1_dim)
c     write(6,*) 'coolcore factor: ',factor,2.5392
      t_bot = t_bot - factor * q_c * tstep 

      call bvalue(0,2,kmesh2,kprob2,isol2,t_bot,ibot,ibot,1,0)

      return
      end
      
      

      

