c *************************************************************
c *    SURFACEVEL
c *
c *    Calculate average surface velocity 
c *
c *    kmesh, kprob    i  Standard Sepran mesh, problem arrays
c *    isol            i  Velocity solution vector
c *    icurv           i  Curve number of the top boundary
c *    veloc           o  veloc(1): maximum absolute velocity
c *                       veloc(2): average absolute velocity
c *    veloc_d         o  as veloc in dimensional form (cm/yr)
c *
c *    PvK 971111
c *************************************************************
       subroutine surfacevel(kmesh,kprob,isol,icurv,veloc,veloc_d)
       implicit none
       integer kmesh(*),kprob(*),isol(*),icurv
       real*8 veloc(*),veloc_d(*)
       integer icurvs(2),i,ncoord,npoint,iintbn(20),ichoice
       real*8 funcx(8000),funcy(8000),x,y,rintbn(20),vel,velmax
       real*8 avvel,vx,vy
       real*8 velint,xold,yold,vold,dx,dy,dist,circ,rmid
       include 'dimensional.inc'
       include 'pecof900.inc'
       real*8 pi
       parameter(pi=3.1415926 535898)
       data funcx(1),funcy(1)/2*8000./

       icurvs(1)=0
       icurvs(2)=icurv
c      write(6,*) 'velocity along curve : ',icurv
c      call compcr(ichoice,kmesh,kprob,isol,0,icurvs,funcx,funcy)
c      ncoord = funcx(5)/2
c      npoint = funcy(5)/2
c      do i=1,10
c         x=funcx(4+2*i)
c         y=funcx(5+2*i)
c         vx = funcy(4+2*i)
c         vy = funcy(5+2*i)
c         write(6,'(5x,2f15.7,5x,3f15.7)') x,y,vx,vy,sqrt(vx*vx+vy*vy)
c      enddo

c      *** note that we compute the tangential velocity component here
       ichoice=0
       call compcr(ichoice,kmesh,kprob,isol,-2,icurvs,funcx,funcy)
       ncoord = funcx(5)/2
       npoint = funcy(5)
       if (ncoord.ne.npoint) then
          write(6,*) 'something weird in surfacevel' 
          write(6,*) 'ncoord, npoint: ',ncoord,npoint
       endif
       velmax=0
       avvel=0
c      *** cylindrical version
       do i=1,npoint
          x=funcx(4+2*i)
          y=funcx(5+2*i)
          vel = funcy(5+i)
          velmax = max(velmax,abs(vel))
          avvel = avvel+abs(vel)
       enddo
       veloc(1) = velmax

c      *** compute average velocity
       xold=funcx(6)
       yold=funcx(7)
       vold=funcy(6)
       velint=0
       avvel=0
       circ=0
       do i=2,npoint
          x=funcx(4+2*i)
          y=funcx(5+2*i)
          vel = funcy(5+i)
          dx = x-xold
          dy = y-yold
          rmid = sqrt((x+0.5*dx)*(x+0.5*dx)+(y+0.5*dy)*(y+0.5*dy)) 
          if (icoor900.eq.0) then
             dist = sqrt(dx*dx+dy*dy)
             circ = circ+dist
             velint = velint+0.5*(vold+vel)*dist
          else if (icoor900.eq.1) then
             dist = sqrt(dx*dx+dy*dy)
             circ = circ+dist*2*pi*rmid
             velint = velint+0.5*(vold+vel)*dist*2*pi*rmid
          endif
          yold=y
          xold=x
          vold=vel
       enddo
       avvel = velint/circ
       veloc(2) = avvel

c      *** dimensionalize velocities (cm/yr)
       veloc_d(1) = veloc(1)*rkappa_dim/(R2_dim-R1_dim)*100*year_dim
       veloc_d(2) = veloc(2)*rkappa_dim/(R2_dim-R1_dim)*100*year_dim

       return
       end
    
 
