      integer NPIXMAX,NRPIXMAX
c sky
c      parameter(NPIXMAX=1201*1201,NRPIXMAX=1201)
      parameter(NPIXMAX=1601*1601,NRPIXMAX=1601)
      real*8 rlampix,pix,dxpix,dypix,xi_eta_pix,drpix,dthpix
      integer ipix,nxpix,nypix,ielempix,nrpix,nthpix,npix_radial
      real*8 dr_pixel,dth_pixel
      common /cpix/ rlampix,dxpix,dypix,pix(NPIXMAX),
     v              drpix,dthpix,dr_pixel,dth_pixel(NRPIXMAX),
     v              xi_eta_pix(3,NPIXMAX),ielempix(NPIXMAX),
     v              ipix(NPIXMAX),nxpix,nypix,nrpix,nthpix,
     v              npix_radial

