c *************************************************************
c *   INITTRAC
c *   Create initial tracer distribution
c *   Combines cylindrical and cartesian versions
c *   PvK 071505
c *************************************************************
      subroutine inittrac(kmesh,kprob,isol)
      implicit none
      integer kmesh,kprob,isol
      include 'ccc.inc'
      if (cyl) then
         call inittrac_cyl(kmesh,kprob,isol)
      else
         call inittrac_cart(kmesh,kprob,isol)
      endif
      return
      end

c *************************************************************
c *   INITTRAC
c *
c *   Create initial tracer distribution. 
c *
c *   itracoption=1
c *     Adapted to give uniform distribution in cylindrical (r,theta) space
c *     Tracer coordinates in coormark are in (r,theta)
c *   itracoption=2
c *     Provide markerchain from theta0-theta1 radius r1
c *
c *   PvK 040597
c *************************************************************
      subroutine inittrac_cyl(kmesh,kprob,isol)
      implicit none
      integer kmesh(*),kprob(*),isol(*)
      integer nxtrac,nytrac,i,j,ip,ntot,ny_now
      real*8 rlamtr,dx,dy,dxn,dyn,x,y,r,arclength,xm,ym,volumel
      real*8 He3losszone(3),He4losszone(3),Ar40losszone(3)
      real*8 xp,yp,yend,xa
      integer ip1
      real*8 pi
      parameter(pi=3.1415926535897932)
      save He3losszone,He4losszone,Ar40losszone
    

      include 'tracer.inc'
      include 'dimensional.inc'
      include 'petrac.inc'
      include 'pecof900.inc'
      include 'ccc.inc'
      include 'bound.inc'
      include 'c1mark.inc'

      if (itracoption.eq.9) then
         ntot = 1
         ndist = 1
         ntrac(1) = 1
         coormark(1) = 0.5 + 0.4192
         coormark(2) = 0.0159221 - 0.5
         if (quart) then 
c           *** rotate 45 degrees counterclockwise
            xp = (coormark(1)-coormark(2))*sqrt(0.5d0)
            yp = (coormark(1)+coormark(2))*sqrt(0.5d0)
            coormark(1) = xp
            coormark(2) = yp
         endif
      endif


      if (itracoption.eq.1) then
c        *** Uniform tracer distribution in [r0,r1]x[theta0,theta1]
         ntot = 0
         do idist=1,ndist
            dx = x1tr(idist)-x0tr(idist)
            dy = (y1tr(idist)-y0tr(idist))*
     v            0.5*(x1tr(idist)+x0tr(idist))
            if (dx.eq.0.or.dy.eq.0) then
               write(6,*) 'PERROR(inittrac_cyl): dx or dy = 0'
               write(6,*) '  dx = ',dx,' dy = ',dy
               call instop
            endif
            nxtrac = dx/dr
            dxn = dx/nxtrac 
            write(6,'(''INITTRAC: Distribute particles uniformly'')')
            write(6,'(''          between r     = '',f9.6,
     v       '' and '',f9.6)') x0tr(idist),x1tr(idist)
            write(6,'(''          and     theta = '',f9.6,
     v       '' and '',f9.6)') y0tr(idist),y1tr(idist)
            write(6,'(''          with spacing  dr = '',f9.5,
     v       '' ('',f9.1,'' km) '')') dxn,dxn*height_dim*1e-3
            volumel=(x1tr(idist)*x1tr(idist)-x0tr(idist)*x0tr(idist)) *
     v                (y1tr(idist)-y0tr(idist)) * 0.5
            write(6,'(''INITTRAC: volume of this distribution ='',
     v               f9.5)') volumel 

            ip = ntot
            ip1 = ip
c *****************************************************************
c *         The calculation of the coordinates of the tracers.
c *         (x,y) take the role of (r,theta). coormark is filled
c *         with (r,theta)
c *************************************************************
c           write(6,*) 'x0tr(idist),dxn: ',idist,x0tr(idist),dxn,nxtrac
            do i=1,nxtrac 
c              *** radius of current arc
               r = x0tr(idist) + (i-0.5)*dxn
               dyn = dxn/r
c              *** length of arc
               arclength = r*(y1tr(idist)-y0tr(idist))
               ny_now = arclength/dyn/r
c              write(6,'(''arclength: '',3f8.3,i10)') arclength,dyn,
c    v               r,ny_now
               if (ip+ny_now.gt.NTRACMAX) then
                  write(6,*) 'PERROR(inittrac_cyl): number of tracers' ,
     v                       '    too large: ',ip+ny_now,NTRACMAX
                  write(6,*) 'idist: ',idist
                  write(6,*) 'dr, nxtrac: ',dr,nxtrac
                  call instop
               endif
               x = r
               do j=1,ny_now
                  y = y0tr(idist) + (j-0.5)*dyn
                  xm = x*sin(y)
                  ym = x*cos(y)
                  call checkbounds(1,xm,ym)
                  coormark(2*(ip+j)-1) = xm
                  coormark(2*(ip+j)) = ym
c                 write(6,'(''I: '',2i5,2f8.3i,10)') i,j,x,y,ip+j 
               enddo
               ip = ip+ny_now
            enddo
            ntrac(idist) = ip - ntot
            ntot = ntot + ntrac(idist)
c           write(6,'(''inittrac_cyl: '',5i8)') 
c    v          ip,ny_now,idist,ntrac(idist),ntot
            if (ntot.gt.NTRACMAX) then
               write(6,*) 'PERROR(inittrac_cyl): ntot > NTRACMAX'
               call instop
            endif

c           *** scale density in tracers with volume of tracers
            volumel = volumel/ntrac(idist)
            do i=1,ntrac(idist)
               densmark(ip1+i) = -Rb_local*volumel
            enddo

         enddo
         write(6,'(''INITTRAC: ntrac: '',i7,2x,10i7,:)') 
     v       ntot,(ntrac(idist),idist=1,ndist)
      endif
      if (ntot.gt.NTRACMAX) then
         write(6,*) 'PERROR(inittrac_cyl): ntot > NTRACMAX' 
         write(6,*) 'ntot, NTRACMAX: ',ntot,NTRACMAX
         call instop
      endif

      if (itracoption.eq.2) then
c        *** markerchain at r=r1
         if (ndist.gt.1) then
            write(6,*) 'PERROR(cylaxm/inittrac_cyl): itracoption=2' 
            write(6,*) '  but ndist > 1. The markerchain method works' 
            write(6,*) '  only for one markerchain right now.'
            call instop
         endif
c        *** Note: x is radius r and y is angle theta here
         x  = y0m(1)
c        *** dy is length of markerchain 
         dy = 2*pi*frac*x
         write(6,*) 'PINFO(inittrac_cyl): radius/length of chain: ',x,dy 
         if (dy.eq.0) then
             write(6,*) 'PERROR(inittrac_cyl): dy = 0'
             write(6,*) 'itracoption = 2'
             write(6,*) 'y0m = ',y0m
             write(6,*) 'frac = ',frac
             call instop
         endif
         if (dm(1).eq.0) then
            write(6,*) 'PERROR(cylaxm/inittrac_cyl): itracoption=2'
            write(6,*) 'but dm=0; set dr to desired dm'
            call instop
         endif
c        *** ntrac = Number of tracers in this chain
         ntrac(1) = dy/dm(1)
         imark(1)=ntrac(1)
         dyn = dy/(x*(ntrac(1)-1))
         write(6,*) 'PINFO(inittrac_cyl): ntrac/imark: ',
     v         ntrac(1),imark(1)
         write(6,*) 'PINFO(inittrac_cyl): ainit(1): ',ainit(1),dyn
c        *** dyn = angular separation between tracers
c        write(6,*) dy,dyn,nxtrac-1,x
         if (ntrac(1).gt.NTRACMAX) then
            write(6,*) 'PERROR(inittrac_cyl): ntrac > NTRACMAX'
            write(6,*) '  ntrac    = ',ntrac
            write(6,*) '  NTRACMAX = ',NTRACMAX
            call instop
         endif
c        *** start chain at theta=0
         yend = (ntrac(1)-1)*dyn
         do j=1,ntrac(1)
            y = (j-1)*dyn
            xa = x+ainit(1)*cos(pi*y/yend)
            xm = xa*sin(y)
            ym = xa*cos(y)
            call checkbounds(1,xm,ym)
            coormark(2*j-1) = xm
            coormark(2*j) = ym
         enddo
         ntot = 0
         imark(1)=ntrac(1)
         do ichain=1,nochain
            ntot = ntot + imark(ichain)
         enddo
         write(6,*) 'PINFO(inittrac_cyl): nmark = ',ntot
         write(6,*) 'PINFO(inittrac_cyl): first marker: ',
     v        coormark(1),coormark(2)
         write(6,*) 'PINFO(inittrac_cyl): last marker : ',
     v        coormark(2*ntot-1),coormark(2*ntot)
      endif
      if (imark(1).gt.NTRACMAX) then
         write(6,*) 'PERROR(inittrac_cyl): ntot > NTRACMAX' 
         write(6,*) 'ntot, NTRACMAX: ',imark(1),NTRACMAX
         call instop
      endif
         
      return
      end

      subroutine inittrac_cart(kmesh,kprob,isol)
      implicit none
      integer kmesh(*),kprob(*),isol(*)
      include 'tracer.inc'
      include 'c1mark.inc'
      include 'cpix.inc'
      include 'petrac.inc'
      real*8 pi
      parameter(pi=3.1415926535897932)
      integer ip,i,ntot
      real*8 piw,ym,xm

      if (itracoption.eq.9) then
         ntot = 1
         ndist = 1
         ntrac(1) = 1
         coormark(1) = 0.5 
         coormark(2) = 0.0159221 

      else if (itracoption.eq.2) then

         if (nochain.gt.1) then
            write(6,*) 'PERROR(inittrac_cart): nochain>1'
            call instop
         endif

         ntot = 0
         ichain=1
         ip=0
         piw = pi/rlampix
         imark(ichain) = rlampix/dm(ichain)+1
         ntrac(ichain) = imark(ichain)
         dm(ichain) = rlampix/(ntrac(ichain)-1)
         ntot = ntot+ntrac(ichain)
         if (ntot.gt.NTRACMAX) then
            write(6,*) 'PERROR(inittrac_cart): ntot>NTRACMAX'
            write(6,*) 'ntrac, NTRACMAX: ',ntot,NTRACMAX
            call instop
         endif
         do i=1,ntrac(ichain)
            xm=dm(ichain)*(ntrac(ichain)-i)
            ym=y0m(ichain)+ainit(ichain)*cos(piw*xm)
            coormark(ip+2*i-1) = xm
            coormark(ip+2*i) = ym
         enddo
         write(6,*) 'PINFO(inittrac_cart): nmark = ',ntot
         write(6,*) 'PINFO(inittrac_cart): first marker: ',
     v        coormark(1),coormark(2)
         write(6,*) 'PINFO(inittrac_cart): last marker : ',
     v        coormark(2*ntot-1),coormark(2*ntot)

      else if (itracoption.eq.1) then
     
         write(6,*) 'PERROR(inittrac_cart): itracoption=1 ',
     v            'not yet implemented'
         call instop()
    
      else 
   
         write(6,*) 'PERROR(inittrac_cart): unknown option'
         write(6,*) 'itracoption = ',itracoption
         call instop
 
      endif

      return
      end
