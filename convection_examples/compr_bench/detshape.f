c *************************************************************
c *   DETSHAPE
c *
c *   Determine value of quadratic shapefunction of quadratic
c *   triangular element (xn,yn) at point (xm,ym)
c *
c *   PvK 950508
c *************************************************************
      subroutine detshape(xn,yn,xm,ym,shapef)
      implicit none
      real*8 xn(6),yn(6),xm,ym,shapef(6)
      real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,rl1,rl2,rl3

      a1 = xn(3)*yn(5) - yn(3)*xn(5)
      a2 = -xn(1)*yn(5) + yn(1)*xn(5)
      a3 = xn(1)*yn(3) - yn(1)*xn(3)
      b1 = yn(3) - yn(5)
      b2 = yn(5) - yn(1)
      b3 = yn(1) - yn(3)
      c1 = xn(5) - xn(3)
      c2 = xn(1) - xn(5)
      c3 = xn(3) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      rl1 = a1 + b1*xm + c1*ym     
      rl2 = a2 + b2*xm + c2*ym     
      rl3 = a3 + b3*xm + c3*ym     
         
      shapef(1) = rl1*(2*rl1-1)
      shapef(3) = rl2*(2*rl2-1)
      shapef(5) = rl3*(2*rl3-1)
      shapef(2) = 4*rl1*rl2
      shapef(4) = 4*rl2*rl3
      shapef(6) = 4*rl3*rl1
 
      return
      end

c *************************************************************
c *   DETSHAPE7
c *
c *   Determine value of quadratic shapefunction of 
c *   extended quadratictriangular element (xn,yn) at point (xm,ym)
c *   This assumes that the sides of the triangle (1,3,5) are straight!
c *
c *   PvK 990408
c *************************************************************
      subroutine detshape7(xn,yn,xm,ym,shapef)
      implicit none
      real*8 xn(7),yn(7),xm,ym,shapef(7)
      real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,delta,rl1,rl2,rl3
      real*8 rl123

      a1 = xn(3)*yn(5) - yn(3)*xn(5)
      a2 = -xn(1)*yn(5) + yn(1)*xn(5)
      a3 = xn(1)*yn(3) - yn(1)*xn(3)
      b1 = yn(3) - yn(5)
      b2 = yn(5) - yn(1)
      b3 = yn(1) - yn(3)
      c1 = xn(5) - xn(3)
      c2 = xn(1) - xn(5)
      c3 = xn(3) - xn(1)
      delta = -c3*b1 + b3*c1
      a1 = a1/delta
      a2 = a2/delta
      a3 = a3/delta
      b1 = b1/delta
      b2 = b2/delta
      b3 = b3/delta
      c1 = c1/delta
      c2 = c2/delta
      c3 = c3/delta

      rl1 = a1 + b1*xm + c1*ym     
      rl2 = a2 + b2*xm + c2*ym     
      rl3 = a3 + b3*xm + c3*ym     
      rl123 = rl1 * rl2 * rl3
         
      shapef(1) = rl1*(2*rl1-1) + 3 * rl123
      shapef(3) = rl2*(2*rl2-1) + 3 * rl123
      shapef(5) = rl3*(2*rl3-1) + 3 * rl123
      shapef(2) = 4*rl1*rl2 - 12 * rl123
      shapef(4) = 4*rl2*rl3 - 12 * rl123
      shapef(6) = 4*rl3*rl1 - 12 * rl123
      shapef(7) = 27*rl123
 
      return
      end

