c *************************************************************
c *   Determine layer in which (x,y) sits
c *************************************************************
      integer function ivalfind1(x,y)
      implicit none
      include 'powerlaw.inc'
      real*8 r,sint,cost,x,y
      integer ilay


      r = sqrt(x**2 + y**2)

      ilay=1
100   continue
        if (r.gt.zint(ilay)) then
           ivalfind1=ilay
           return
        else
           ilay=ilay+1
           if (ilay.lt.nlay) then
              goto 100
           endif
           ivalfind1=ilay
      endif

      return
      end
