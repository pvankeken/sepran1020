c *************************************************************
c *   IMAGE
c *   
c *   Create Imagetool rasterfile from Sepran solution vector
c *   
c *   Limitations: 
c *      Triangles with 3 nodalpoints and linear shapefunctions are 
c *      used. Maximum number of pixels in either direction is 500
c *      Solution has to contain only one degree of freedom
c *
c *   PvK 970325: adapted for use with periodic boundary conditions
c *************************************************************
      subroutine image(kmesh,kprob,isol,nimage)
      implicit none
      integer kmesh(*),kprob(*),isol(*)
      character*(*) nimage

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      include 'cimage.inc'
      include 'colimage.inc'
      real*8 tmaxpref
      integer ivideo,itmax
      common /cvideo/ tmaxpref,ivideo,itmax
      include 'ccc.inc'
      
      integer NPIXMAX
      parameter(NPIXMAX=500 000)
      integer ipix(NPIXMAX),jpix(NPIXMAX)
      real*8 a(3),b(3),c(3),xn(3),yn(3),tn(3),phi(3)
      integer iinvec(10),iresvc(10)
      integer iniget,inidgt,ikelmc,ikelmi,iusol,npoint
      integer ipx,ipy,i1,ipoint,ipxmin,ipxmax,ipymin,ipymax
      integer ielem,n2x,n2y,n2tot,nordx,nordy,jpx,jpy,jp
      integer i,npx,npy,ntot,nelem,ip
      real*8 rlam,tmin,tmax,p,q,xcmin,xcmax,ycmin,ycmax
      real*8 rlx,rly,t,x,y
      logical out
      

      

c     *** Find information on the nodalpoint numbers of the
c     *** elements (KMESH part c), the coordinates of the
c     *** nodalpoints (KMESH part i) and the numbering of
c     *** of dof's of the solutionvector (KPROB part h)
      call ini050(kmesh(23),'image: coordinates')
      call ini050(kmesh(17),'image: nodalpoints')
      call ini050(isol(1),'image: isol')
c     *** Find the pointers in ibuffr
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      iusol  = inidgt(isol(1))
      nelem = kmesh(9)
      if (periodic) nelem=kmesh(kmesh(21))

c     *** determine x and y dimensions of the mesh. The longest side
c     *** will be discretized with the ipmax pixels 
      npoint = kmesh(8)
      call imag02(buffr(ikelmi),npoint)
      rlam = (ximax-ximin)/(yimax-yimin)
      if (rlam.lt.1.0) then
         npy = ipmax
         npx = ipmax*rlam+0.5
      else
         npx = ipmax
         npy = ipmax*1.0/rlam+0.5
      endif

c     *** total number of pixels
      ntot = npx*npy
      if (ntot.gt.NPIXMAX) then
         write(6,*) 'PERROR(image): ntot>NPIXMAX'
         write(6,*) 'ntot, NPIXMAX = ',ntot,NPIXMAX
         call instop
      endif

c     *** determine minimum and maximum (TMIN,TMAX) of solution vector
      call algebr(6,1,isol,i1,i1,kmesh,kprob,tmin,tmax,p,q,ipoint)
c     print *,'tmin , tmax = ',tmin,tmax
      if (itmax.eq.1) then
c         print *,'tmax corrected to ',tmaxpref
          tmax=tmaxpref
      endif

c     *** determine number of pixels per unit length
      rlx = (npx-1)*1./(ximax-ximin)
      rly = (npy-1)*1./(yimax-yimin)
      do ielem=1,nelem
c        *** determine coordinates of nodalpoints (xn,yn)
c        *** and nodalpoint values of isol (tn)
         call imag01(ibuffr(ikelmc),buffr(ikelmi),buffr(iusol),
     v               xn,yn,tn,ielem)
c        *** determine smallest rectangular area around element
c        *** Only the pixels in this area are scanned. If the
c        *** pixel is in the element a linear interpolation is
c        *** performed to determine the pixel value of the 
c        *** solutionvector
         xcmin = min(xn(1),xn(2),xn(3))
         ycmin = min(yn(1),yn(2),yn(3))
         xcmax = max(xn(1),xn(2),xn(3))
         ycmax = max(yn(1),yn(2),yn(3))
c        *** Translate this to pixelvalues
         ipxmin = (xcmin-ximin)*rlx + 1.5
         ipymin = (ycmin-yimin)*rly + 1.5
         ipxmax = (xcmax-ximin)*rlx + 1.5
         ipymax = (ycmax-yimin)*rly + 1.5
         if (ipxmin.lt.1.or.ipymin.lt.1) then
            write(6,*) 'PERROR(image) ipmin < 1'
            write(6,*) 'ipxmin,ipymin  ',ipxmin,ipymin
            stop
         endif
         if (ipymax.gt.ipmax.or.ipxmax.gt.ipmax) then
            write(6,*) 'PERROR(image) pxmax > max'
            write(6,*) 'ipxmax,ipymax  ',ipxmax,ipymax
            stop
         endif
c        *** determine coefficients a,b,c of shapefunctions
         call trilin(xn,yn,a,b,c)
c        *** Loop over the pixels in the rectangular area
         do ipy = ipymin,ipymax
            do ipx = ipxmin,ipxmax
               x = ximin + (ipx-1)/rlx
               y = yimin + (ipy-1)/rly
               out = .true.
c              *** If the pixel is positioned in the element
c              *** then the values of all basisfunctions is 
c              *** larger or equal than 0
               do i=1,3
                  phi(i) = a(i)+b(i)*x+c(i)*y                
                  out = phi(i).ge.-1d-5.and.out
               enddo
               if (out) then
c              *** the pixel is in the element
                  t = tn(1)*phi(1)+tn(2)*phi(2)+tn(3)*phi(3)
c                 ** Imagetools origin is positioned in the
c                 ** upper left corner; we need to transform
c                 ** the coordinates
                  ip = ntot - ipy*npx + ipx
                  ipix(ip) = (t-tmin)/(tmax-tmin)*nsteps+nulstep
                  if (t.gt.1.) t=1.
                  if (t.lt.0.) t=0.
               endif
            enddo
         enddo
      enddo

c     *** Now dump the pixelvalues to rasterfile
      if (ivideo.eq.1) then
c        *** take options for video: rotate rasterfile
         n2x = 700
         n2y = 350
         n2tot = n2x*n2y
         do ip=1,n2tot
            jpix(ip)=0
         enddo 
         if (n2tot.gt.NPIXMAX) stop 'too much for me'
         write(6,*) 'n2tot: ',n2tot
         nordx = (n2x-npy)/2
         nordy = (n2y-npx)/2
c        *** rotate NPX*NPY frame and translate origin
         do ipx=1,npx
            do ipy=1,npy
               ip = (ipy-1)*npx + ipx
               jpx = ipy + nordx
               jpy = npx - ipx + nordy
               jp = (jpy-1)*n2x + jpx 
               jpix(jp) = ipix(ip)
            enddo
         enddo
         n2tot = n2x*n2y
         call rasout(n2tot,jpix,nimage)
      else 
        call rasout(ntot,ipix,nimage)
      endif

      return
      end

c *************************************************************
c *   IMAG01
c * 
c *   Determines nodalpoints values, coordinates and solution in
c *   the nodalpoints of element iel
c *
c *   PvK 170590
c *************************************************************
      subroutine imag01(kmeshc,coor,usol,xn,yn,tn,ielem)
      implicit none
      integer kmeshc(*),ielem
      real*8 coor(2,*),usol(*),xn(*),yn(*),tn(*)
      integer nodno(3),inpelm,ip,i
 
      inpelm = 3
      ip = (ielem-1)*inpelm
c     *** nodalpoint numbers in kmesh part c
      do  i=1,inpelm
          nodno(i) = kmeshc(ip+i)
      enddo
c     *** coordinates in coor
      do i=1,inpelm
         xn(i) = coor(1,nodno(i))
         yn(i) = coor(2,nodno(i))
      enddo
c     *** solution in usol
      do i=1,inpelm
         ip = nodno(i)
         tn(i) = usol(ip)
      enddo

      return
      end

c *************************************************************
c * IMAG02
c * 
c * Determine extrema of x- and y-coordinates
c *
c * PvK 260590
c *************************************************************
      subroutine imag02(coor,npoint)
      implicit none
      integer npoint
      real*8 coor(2,npoint)
      include 'cimage.inc'
      integer i
      real*8 x,y

      ximin = coor(1,1)
      ximax = coor(1,1)
      yimin = coor(2,1)
      yimax = coor(2,1)
      do i=2,npoint
         x = coor(1,i)
         y = coor(2,i)
         if (x.gt.ximax) ximax=x
         if (x.lt.ximin) ximin=x
         if (y.lt.yimin) yimin=y
         if (y.gt.yimax) yimax=y
      enddo
      return
      end
