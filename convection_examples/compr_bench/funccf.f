c *************************************************************
c *   FUNCCF
c *
c *   Determines values of position dependent coefficients
c *
c *   used here in particular for variable density, expansivity and
c *   diffusivity following an exp(Di*z) behavior.
c *
c *************************************************************
      real*8 function funccf(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z
      include 'SPcommon/cactl'
      include 'compression.inc'
      include 'depth_thermodyn.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      integer ibuffr
      common ibuffr(1)
      integer k1,k2,ipvisco,iipvisco,ioff,iipbuoy
      real*8 pefrombuf,zr,rho,r,rho_n

      save k1,k2
      data k1,k2/1,1/

      if (ichois.eq.3) then

c        *** density
         if (cyl) then
            r = sqrt(x*x+y*y)
            zr = r2-r
         else
            zr = 1-y
         endif
c        rho = 1d0
         funccf = rho_n(zr)
c        write(6,*) 'zr, funccf: ',zr,funccf

      else if (ichois.eq.4) then

c        *** diffusivity
         if (cyl) then
            r = sqrt(x*x+y*y)
            zr = r2-r
         else
            zr = 1-y
         endif
         funccf = rho_n(zr)**pdif

      else if (ichois.eq.5) then

c        *** expansivity
         if (cyl) then
            r = sqrt(x*x+y*y)
            zr = r2-r
         else
            zr = 1-y
         endif
         funccf = rho_n(zr)**palpha

      endif

      return
      end

      real*8 function rho_n(z)
      implicit none
      real*8 z
      include 'pecof900.inc' 
      include 'cpephase.inc'
      include 'compression.inc'
   
c     write(6,*) 'rho_n: ',compress,z,Di,rho_av
      if (compress) then
c        rho_n = exp(Di*z)/rho_av
c        *** Jarvis & McKenzie, 1980
         rho_n = exp(Di*z/Grueneisen)
      else
         rho_n = 1d0
      endif
   
      return
      end
