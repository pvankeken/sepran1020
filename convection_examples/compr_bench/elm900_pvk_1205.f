      subroutine elm900 ( coor, elemmt, elemvc, elemms, iuser, user,
     +                    index1, index3, index4, vecold, islold,
     +                    vecloc, wrk1, symm )
! ======================================================================
!
!        programmer    Guus Segal/Jaap van der Zanden
!        version  2.18 date 16-07-2004 Correction rhside in case of penalty +
!                                      non-zero divergence
!        version  2.17 date 23-02-2004 Extra parameters w, x, xgauss
!        version  2.16 date 13-02-2004 Extra debug statements
!        version  2.15 date 04-01-2004 Debug statements
!        version  2.14 date 04-07-2003 Remove common celwrk
!        version  2.13 date 19-05-2003 New call to elm800basefn
!        version  2.12 date 07-05-2003 Do not make symm false for 901/2
!        version  2.11 date 16-04-2003 Use elm800basefn instead of elp routines
!        version  2.10 date 03-03-2003 New call to el2005
!        version  2.9  date 06-12-2002 Correction imesh for integrated method.
!                                      Allow iterative solution procedures for
!                                      dynamic analyses (JdH)
!        version  2.8  date 08-11-2002 New call to el0900
!        version  2.7  date 18-09-2002 New call to el3007
!        version  2.6  date 10-07-2002 Extended by David Bessems for shape 16
!        version  2.5  date 07-06-2002 Correction mass matrix for type 902 and
!                                      27-node hexahedron
!        version  2.4  date 17-08-2001 Incorporate pressure term in continuity
!                                      equation (Gerard Haagh)
!        version  2.3  date 11-05-2001 Extra option: debug
!        version  2.2  date 15-04-2001 New: special form of equations
!        version  2.1  date 26-06-2000 New call to elp624
!        version  2.0  date 24-03-2000 Extra parameter symm
!
!   copyright (c) 1993-2004  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element matrix and vector for the  navier-stokes equations
!     incompressible flow, using the penalty method or the integrated method
!     Two and three-dimensional elements
!     ELM900 is a help subroutine for subroutine BUILD (TO0050)
!     it is called through the intermediate subroutine elmns2
!     So:
!     BUILD
!     TO0050
!       -  Loop over elements
!          -  ELMMEC
!             - ELM900
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  4 node quadrilateral (Types 900, 925)
!         5 node quadrilateral (types 901, 926 integrated method)
!         6 node triangle (Types 900, 925 and 400, 402, 404 and 406)
!         penalty method
!         This element is treated internally as a seven-point element
!         Type 400: Cartesian, 402 Polar, 404 Axi-symmetric,
!              406: Axi-symmetric with swirl
!         7 node triangle (Type 901,902)
!         integrated method
!         901: elimination of centroid
!         902: no elimination
!         9 node quadrilateral (Type 900, penalty method
!                               Type 901 integrated method)
!    3D: 8 node or 27 node brick (Types 900 and 410)
!         penalty method
!        27 node brick (Type 902)
!         integrated method
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/celiar'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/cellog'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cinforel'
      include 'SPcommon/celwrk'
c     *** PvK
      include 'cbuoy_trac.inc'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemmt(*), elemvc(*), coor(*), user(*),
     +                 elemms(*), vecold(*), vecloc(*), wrk1(*)
      integer iuser(*), index1(*), index3(*), index4(*), islold(5,*)
      logical symm

!     coor     i     array of length ndim x npoint containing the co-ordinates
!                    of the nodal points with respect to the global numbering
!                    x  = coor (1,i);  y  = coor (2,i);  z  = coor (3,i);
!                     i                 i                 i
!     elemms   o     Element mass matrix to be computed (Diagonal matrix)
!     elemmt   o     Element matrix to be computed
!                    At this moment the element is assumed to be zero
!     elemvc   o     Element vector to be computed
!     index1   i     Array of length inpelm containing the point numbers
!                    of the nodal points in the element
!     index3   i     Two-dimensional integer array of length NUMOLD x NINDEX
!                    containing the positions of the "old" solutions in array
!                    VECOLD with respect to the present element
!                    For example VECOLD(INDEX3(i,j)) contains the j th
!                    unknown with respect to the i th old solution vector.
!                    The number i refers to the i th vector corresponding to
!                    IVCOLD in the call of SYSTM2 or DERIVA
!     index4   i     Two-dimensional integer array of length NUMOLD x INPELM
!                    containing the number of unknowns per point accumulated
!                    in array VECOLD with respect to the present element.
!                    For example INDEX4(i,1) contains the number of unknowns
!                    in the first point with respect to the i th vector stored
!                    in VECOLD.
!                    The number of unknowns in the j th point with respect to
!                    i th vector in VECOLD is equal to
!                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold   i     User input array in which the user puts information
!                    of all preceding solutions
!                    Integer array of length 5 x numold
!     iuser    i     Integer user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     symm    i/o    Indicates whether the matrix symmetric (true) or not
!     user     i     Real user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     vecloc         Work array in which all old solution vectors for the
!                    integration points are stored
!     vecold   i     In this array all preceding solutions are stored, i.e.
!                    all solutions that have been computed before and
!                    have been carried to system or deriva by the parameter
!                    islold in the parameter list of these main subroutines
!     wrk1           work array of length 163 + 238m**3 + 6m + 15n**2 + 10n
!                    contents:
!                    Starting address    length   Name:
!                    1                  32*27     ARRAY
!                    ipugs               3*27     UGAUSS
!                    ipdudx              9*27     DUDX
!                    ipphix              27*27*3  PHIKSI for elp633
!                    ipetef              27       ETHA_EFFECTIVE
!                    ipseci              27       SECOND_INVARIANT
!                    ipdetd              27       DERIVATIVE of
!                                                 SECOND_INVARIANT
!                    ipsp                81*4     SP_matrix (Gradient P)
!                    ipdiv               81*4     DIV_matrix (Divergence matrix)
!                    ippp                4*4      Pressure matrix
!                    ipwork              27*31    Work space
!                    ipelem              81*81    Copy of part of element matrix
!                    ipelvc              81*3     Copy of part of element vector
!                    iptran              12*2     Transformation matrix based
!                                                 on div
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    iptrnp              12*2     Transformation matrix based
!                                                 on sp
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    ipmass              4*7*7    Mass matrix in case of a
!                                                 transformation
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    ipunew               3*27     Unew
!                    ipunewgs             3*27     Unew_gauss
!                    ipdudxnw             9*27     DUDX_unew
!                    Total length: 14353
!                    The subarrays contain the following contents:
!                    ARRAY:
!                    array in which the values of the coefficients
!                    in the integration points are stored in the sequence:
!                         2D                  3D
!                    1:   rho              rho
!                    2:   omega            omega
!                    3:   f1               f1
!                    4:   f2               f2
!                    5:                    f3
!                    6:   eta/E            eta/E
!                    7:   rho in nodal points (mcont=1 only)
!                    7:   G_L              G_L (mcont=2 only)
!                    8:   kappa            kappa
!                    9:   fdiv             fdiv
!                   10:   fstress          fstress
!                   11:   c_div_1
!                   12:   c_div_2
!                   13:   -
!                   14:   c_grad
!                   15:   c_conv_1
!                   16:   c_conv_2
!                   17:   c_conv_3
!                   18:   c_conv_4
!                   19:   -
!                   20:   -
!                   21:   -
!                   22:   -
!                   23:   -
!                   24:   c_visc_1
!                   25:   c_visc_2
!                   26:   c_visc_3
!                   27:   c_visc_4
!                   28:   -
!                   29:   -
!                   30:   -
!                   31:   -
!                   32:   -
!                    Each coefficient requires exactly m (is number of
!                    integration points) positions
!                    DPHIDX:
!                    Array of length inpelm x m x ndim containing the
!                    derivatives of the basis functions in the sequence:
!                    d phi / dx = dphidx (i,k,1);  d phi / dy = dphidx (i,k,2);
!                         i                             i
!                    d phi / dz = dphidx (i,k,3)
!                         i
!                      in node k
!                    WORK
!                    General work space
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer i, isub, isup, jelp, jtime, ipend, ipugs, jdiags, ippp,
     +        ipwork, ipphix, mconv, modelv, ipdudx, ipsp, ipetef,
     +        ipseci, ipdetd, ipelem, ipelvc, mcont, ipdiv, iptran,
     +        iptrnp, ipelcp, iprho, j, nveloc, ipmass, npres, iprhoc,
     +        imesh, ipunew, ipcor, ipdudxnw, ipunewgs, mdiv, mcoefconv,
     +        modelrhsd, ioldm, ipphi
      double precision qmat(81), penalp, cn, clamb, thetdt, adams,
     +                 rhsdiv(4), rhsdivtr(3), uoldm(81), tang(1),
     +                 dphidx(3*27*27), x(81), w(27), xgauss(81),
     +                 uold(81)
      logical debug
      save isub, isup, jelp, jtime, ipwork, ipphix, ipugs,
     +     mconv, modelv, penalp, cn, clamb, ipdudx, ipsp, ipetef,
     +     ipseci, ipdetd, thetdt, ipelem, ipelvc, mcont, ipdiv, iptrnp,
     +     ipelcp, iprho, nveloc, iptran, ipmass, npres, iprhoc, imesh,
     +     ipunew, ipdudxnw, ipunewgs, mdiv, mcoefconv, ippp, modelrhsd,
     +     ioldm, ipphi

!     adams          Multiplication factor for the Adams-Bashfort method
!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     dphidx         Array of length n x m x ndim  containing the
!     derivative
!                    of the basis functions in the sequence:
!                    d phi / dx (xg ) = dphidx (i,k,1);
!                         i        k
!                    d phi / dy (xg ) = dphidx (i,k,2);
!                         i        k
!                    d phi / dz (xg ) = dphidx (i,k,3);
!                         i        k
!                    If the element is a linear simplex, the
!                    derivatives are constant and only k = 1 is filled.
!     i              Counting variable
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     ipcor          Starting address in array wrk1 of corrected velocity
!     ipdetd         Starting address in array wrk1 of detdsc
!     ipdiv          Starting address in array wrk1 of div
!     ipdudx         Starting address in array wrk1 of dudx
!     ipdudxnw       Starting address in array wrk1 of du0dx
!     ipelcp         Starting address in array wrk1 of copy of element matrix
!     ipelem         Starting address in array wrk1 of copy of element matrix
!     ipelvc         Starting address in array wrk1 of copy of element vector
!     ipend          Last position in array
!     ipetef         Starting address in array wrk1 of etheff
!     ipmass         Starting address in array wrk1 of mass matrix before
!                    transformation
!     ipphi          Starting address in array wrk1 of phi
!     ipphix         Starting address in array wrk1 of phiksi
!     ippp           Starting address in array wrk1 of pressure matrix
!     iprho          Starting address in array wrk1 of rho
!     iprhoc         Starting address in array wrk1 of rho multiplied by w
!     ipseci         Starting address in array wrk1 of secinv
!     ipsp           Starting address in array wrk1 of sp
!     iptran         Starting address in array wrk1 of trans with respect to
!                    div
!     iptrnp         Starting address in array wrk1 of trans with respect to
!                    grad
!     ipugs          Starting address in array wrk1 of ugauss
!     ipunew         Starting address in array wrk1 of unew (u0)
!     ipunewgs       Starting address in array wrk1 of u0 in Gauss points
!     ipwork         Starting address in array wrk1 of work array
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     j              Counting variable
!     jdiags         Help variable to save jdiag
!     jelp           Indication of the type of basis function subroutine must
!                    be called by ELM100. Possible vaues:
!                     1:  Subroutine ELP610  (Linear line element)
!                     2:  Subroutine ELP611  (Quadratic line element)
!                     3:  Subroutine ELP620  (Linear triangle)
!                     4:  Subroutine ELP624  (Extended quadratic triangle)
!                     5:  Subroutine ELP621  (Bilinear quadrilateral)
!                     6:  Subroutine ELP623  (Biquadratic quadrilateral)
!                    11:  Subroutine ELP630  (Linear tetrahedron)
!                    12:  Subroutine ELP631  (Quadratic tetrahedron)
!                    13:  Subroutine ELP632  (Trilinear brick)
!                    14:  Subroutine ELP633  (Triquadratic brick)
!    jtime           Integer parameter indicating if the mass matrix for the
!                    time-dependent equation must be computed (>0) or not ( = 0)
!                    Possible values:
!                      0:  no mass matrix
!                      1:  diagonal mass matrix with constant coefficient
!                      2:  diagonal mass matrix with variable coefficient
!                     11:  full mass matrix with constant coefficient
!                     12:  full mass matrix with variable coefficient
!                    101:  refers to the special possibility of the theta
!                          method directly applied
!                    102:  refers to the special possibility of the theta
!                          method directly applied with variable rho
!     mcoefconv      Defines how the convetive term is treated:
!                    0: standard case
!                    1: special case: the convective term is defined as
!                          c_conv_1 u du/dx + c_conv_2 v du/dy
!                          c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!                       Not applicable with penalty function method
!                    2: Goertler equations
!                    3: Special form of NS equations
!                    4: the continuity equation contains a pressure term.
!     mconv          Defines the treatment of the convective terms
!                    Possible values:
!                    0: convective terms are skipped (Stokes flow)
!                    1: convective terms are linearized by Picard iteration
!                    2: convective terms are linearized by Newton iteration
!                    3: convective terms are linearized by the incorrect Picard
!                       iteration
!                    4: convective terms are linearized by successive
!                       substitution
!     mdiv           Defines if divergence right-hand side is used (1) or
!                    not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     npres          Number of pressure parameters
!     nveloc         Number of velocity parameters
!     penalp         Penalty parameter
!     qmat           transformation matrix global -> local coordinates
!                    in case of a 3D element
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     rhsdivtr       Transformed divergence right-hand side (if mdiv=1)
!     tang           Dummy
!     thetdt         1/(theta dt) for the case of the theta method immediately
!                    applied
!     uold           Array to store the old solution in the nodal points
!     uoldm          Velocity vector at the end of the previous time
!                    step. The rhs contribution of the fluid mass is
!                    computed with this vector for iterative dynamical
!                    analyses. Standard case (no iterations): uold is used.
!     w              array of length m containing the weights for the
!                    numerical integration
!     x              array of length inpelm x 2 containing the x and
!                    y-coordinates of the nodal points with respect to the
!                    local numbering in the sequence:
!                    x  = x (i,1);  y  = x (i,2);
!                     i              i
!     xgauss         Array of length m x 2 containing the co-ordinates of
!                    the gauss points.  Array xgauss is only filled when Gauss
!                    integration is applied.
!                    xg  = xgauss (i,1);  yg  = xgauss (i,2);
!                      i                    i
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      double precision ddot

!     DDOT           (CDOT)     Inner product of two vectors
!     EL0410         Initialize parameters for subroutines ELD900/ELI900/ELM900
!                    with respect to type 410
!     EL0900         Initialize parameters for subroutines ELD900/ELI900/ELM900
!     EL1000         Compute the divergence matrix for Navier-Stokes
!     EL1000SP       Compute the divergence matrix for Navier-Stokes
!                    (special form)
!     EL1006         Fill element vector for vertices only
!     EL1010         Compute matrix vector multiplication
!     EL1015         Subtract mesh velocity from velocity
!     EL2000         Compute the transformation matrix to be used in the
!                    elimination process of the centroid point for
!                    incompressible Navier-Stokes
!     EL2005         Compute a local vector and its derivatives from a known
!                    vector
!     EL2010         compute  convective terms of Navier-stokes
!                    for Picard or newton iteration   (k-d)
!     EL2010G        compute  convective terms of Goertler equations
!                    for Picard or newton iteration   (k-d)
!     EL2010SP       compute  convective terms of Navier-stokes
!                    for Picard or newton iteration   (k-d)
!                    special form of Navier-Stokes equations
!     EL2011         compute convective terms of Navier-stokes
!                    for newton iteration   (k-d)  (right-hand side)
!     EL2011G        compute convective terms of Goertler equations
!                    for newton iteration   (k-d)  (right-hand side)
!     EL2011SP       compute convective terms of Goertler equations
!                    for newton iteration   (k-d)  (right-hand side)
!                    (special form)
!     EL2107         Eliminate the centroid point for the incompressible
!                    Navier- Stokes equations in case of an extended quadratic
!                    triangle
!     EL2201         Eliminate the centroid point for the incompressible
!                    Navier-Stokes equations in case of an
!                    extended quadratic triangle
!     EL2203         Put stiffness matrix and part due to gradient and
!                    divergence into element matrix for 27-point
!                    Navier-Stokes equation
!     EL3000         Compute a local vector and its derivatives from a known
!                    vector
!     EL3002         Compute integrals of the shape / beta(x) phi (x) phi (x) dx
!                                                   e            i       j
!     EL3003         Compute integrals of the shape / f(x) phi (x) dx
!                                                   e         i
!     EL3007         Fills old solution and derivatives
!     EL3009         Fill series of old solutions in array vecloc
!     EL3010         Fill small matrix elemmt into larger matrix s
!     EL4029         Fill element vector for newton linearization of power law
!                    viscosity terms
!     EL4900         compute penalty right-hand side for incompressible
!                    Navier-Stokes
!     EL4914         Compute contribution of Coriolis term
!     EL4915         compute the viscous matrix for 3-dim Navier stokes
!     EL4915SP       compute the viscous matrix for 3-dim Navier stokes
!                    (special form)
!     EL4920         penalty matrix is added to the element matrix; 27-node
!                    brick
!     EL4921         Put gradient and divergence matrix into large matrix
!     ELFLR7         Fill variable coefficients in array ARRAY
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM900CHECK    Check element matrix/vector by matix vector multiplication
!     ELMASS27       Fill the element mass matrix for the special case of 27
!                    node brick with 3 unknowns per point except for the
!                    centroid, where there are 7 unknowns due to the presence
!                    of the pressure
!     ELSTRRHS       Compute contribution of initial stress to right-hand side
!     ELVISC         Compute effective viscosity depending on the viscosity
!                    model
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     INSTOP         Stop the program
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  information about the time-integration
!         This position consists of 2 parts:  jtime_old + 10*ioldm
!         Possible values for jtime_old
!         0: if imas in BUILD=0 then time-independent flow
!            else the mass matrix is built according to imas
!         1: The theta method is applied directly in the following way:
!            the mass matrix divided by theta delta is added to the stiffness
!            matrix
!            the mass matrix divided by theta delta and multiplied by u(n) is
!            added to the right-hand-side vector
!         ioldm indicates the sequence number of velocity at end of previous
!               time step in input vector.
!               If 0 no iterative procedure is adopted for dynamical
!               analyses.
!     2:  type of viscosity model used (modelv) as well as given stress
!         tensor as right-hand side accordinag to
!         modelv + 1000*model_rhsd
!         Possible values for modelv
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!         5:    special case: the viscous term is defined as
!               c_visc_1 d^2 u /dx^2 + c_visc_2 d^2 u /dy^2
!               c_visc_3 d^2 v /dx^2 + c_visc_4 d^2 v /dy^2
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!         Possible values for model_rhsd:
!         0:    if there is a given stress tensor the reference to this
!               tensor is stored in position 19
!         1:    It is supposed that there are 6 given stress tensor components
!               stored in positions 19-24
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!         2: Polar co-ordinates
!     5:  Information about the convective terms (mconv)
!         and continuity equation (mcont) according to
!         mconv+10*mcont+100*mcoef_conv
!         Possible values for mconv:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!         4: convective terms are linearized by successive substitution
!         Possible values for mcont:
!         0: Standard situation, the continuity equation is div u = 0
!         1: A special continuity equation div ( rho u ) = 0, which rho
!            variable is solved. This is not the standard continuity equation
!            for incompressible flow. In fact we have a time-independent
!            compressible flow description.
!            This possiblility can not be applied with the penalty function
!            approach
!         2: The so-called Goertler equations are solved
!         3: The continuity equation is equal to:
!            c_div_1 du/dx +  c_div_2 dv/dy = 0
!            The gradient of the pressure is multiplied by c_grad
!         4: the continuity equation contains a pressure term.
!         Possible values for mcoef_conv:
!         0: standard convective terms
!         1: special case: convective terms are defined by:
!            c_conv_1 u du/dx + c_conv_2 v du/dy
!            c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     6-50: Information about the equations and solution method
!     6:  Parameter eps for the penalty function method
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15:  imesh_velocity   (integer, refers to sequence number of mesh-velocity)
!                          If unequal to zero the convective term is adapted by
!                          -u_mesh * grad(u)
!                In case of the Goertler equations imesh_velocity refers to
!                the velocity field U_0
!    16:  G_L    Parameter for the Goertler equations
!    17:  kappa  Parameter for the Goertler equations
!                Parameter for the pressure term in the continuity equation
!                (MCONT=4)
!    18:  fdiv   Right-hand side for continuity equation
!    19:  stress If model_rhsd = 0 then
!                Given stress tensor used in the right-hand side vector
!                At this moment it is supposed that the stress is defined
!                per element and that in each element all components are
!                constant
!                If model_rhsd = 1 then
!                The six components of the stress tensors are stored in
!                positions 19 to 24 in the order: xx, yy, zz, xy, yz, zx
!                This option can not be used in cooporation with the
!                dimensionless coefficients stored in positions 20-41
!    20:  c_div_1
!    21:  c_div_2
!    22:  -
!    23:  c_grad
!    24-32:  c_conv 1 ... 9
!    33-41:  c_visc 1 ... 9
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     550   array length too short
! **********************************************************************
!
!                       PSEUDO CODE
!
!    if ( first call ) then
!       initialize standard parameters
!    compute basis functions
!    fill old solutions if necessary
!    if ( matrix ) then
!       fill the viscosity in the corresponding array
!       Compute the viscous terms
!       if ( mconv>0 ) then
!          compute convective terms
!       Compute the divergence matrix
!       Compute the pressure matrix
!       Compute the penalty contribution
!    if ( vector ) then
!       Compute contribution of body forces
!       if ( mconv=2 ) then
!          compute contribution of convective terms
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm900' )
      debug = .false.
      if ( ifirst.eq.0 )  then

!     --- ifirst = 0,  compute element independent quantities

         icheld = 0
         if ( itype.lt.500 ) then

!        --- Type numbers 400, 402, 404, 406, 410

            call el0410 ( iuser, user, jelp, jtime, penalp, cn,
     +                    clamb, mconv, modelv, isub, isup, wrk1,
     +                    thetdt )
            mcont = 0
            imesh = 0
            mdiv = 0
            mcoefconv = 0
            modelrhsd = 0
            ioldm = 0

         else

!        --- Type numbers 900 - 902, 925-927

            call el0900 ( iuser, user, jelp, jtime, penalp, cn, clamb,
     +                    mconv, mcont, modelv, isub, isup, wrk1,
     +                    thetdt, nveloc, npres, imesh, index4, mdiv,
     +                    mcoefconv, modelrhsd, ioldm )

         end if
         if ( debug ) then

            write(irefwr,*) 'mconv, mcont, modelv, isub, isup ',
     +                       mconv, mcont, modelv, isub, isup
            write(irefwr,*) 'nveloc, npres, imesh ',
     +                       nveloc, npres, imesh
            write(irefwr,*) 'mdiv, mcoefconv, modelrhsd, ioldm ',
     +                       mdiv, mcoefconv, modelrhsd, ioldm

         end if

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

         ipphi  = 1 + 32*m
         ipugs  = ipphi+m*n
         if ( mcont.eq.1 .and. jdiag.ne.1 .and.
     +       ( ind(1).gt.0 .or. ist(1).gt.0 ) ) then
            iprho = 1+6*m
         else
            iprho = 1
         end if
         ipdudx = ipugs  + ncomp*m
         ipphix = ipdudx + ncomp**2*m
         ipetef = ipphix + ndim*n*m
         ipseci = ipetef + m
         ipdetd = ipseci + m
         ipsp   = ipdetd + m
         if ( mcont.eq.0 ) then
            ipdiv = ipsp
         else
            ipdiv = ipsp + n*ndim*(ndim+1)
         end if

!        --- set pointer for pressure matrix

         if ( mcont.eq.4 ) then
            ippp = ipdiv + n*ndim*(ndim+1)
            ipwork = ippp + (ndimlc+1)**2
         else
            ippp = ipdiv
            ipwork = ippp + n*ndim*(ndim+1)
         end if
         ipelem = ipwork + n*n+max(n*(1+ndim),4*m)
         if ( jtrans.eq.1 ) then
            ipelcp = ipelem + (n*ndim)**2
         else
            ipelcp = ipelem
         end if
         ipelvc = ipelcp + (n*ndim)**2
         iptran = ipelvc + n*ndim
         if ( mcont.eq.0 ) then
            iptrnp = iptran
         else
            iptrnp = iptran + ndim**2*(n-1)
         end if
         iprhoc = iptrnp+(n-1)*ndim**2
         ipmass = iprhoc+m
         if ( imas.eq.2 ) then
            do i = 1, (ndim*n)**2
               wrk1(ipmass+i-1) = 0d0
            end do
         end if
         ipunew = ipmass+(ndim*n)**2
         ipunewgs = ipunew+ndim*n
         ipdudxnw = ipunewgs+ndim*m
         ipend = ipdudxnw+9*27-1
         if ( ipend.gt.20815 ) then

!        --- array wrk1 too small

            call errint ( ipend, 1 )
            call errint ( 20815, 2 )
            call errchr ( 'wrk1', 1 )
            call errsub ( 550, 2, 0, 1 )
            go to 1000

         end if

      end if

!     --- compute basis functions and weights for numerical integration

      call elm800basefn ( coor, x, w, dphidx, xgauss, wrk1(ipphi),
     +                    index1, qmat, tang, jelp, wrk1(ipphix), work,
     +                    work, work )
      if ( ierror.ne.0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold and uoldm (for dyn. iterative analyses) if necessary

      if ( ind(50).eq.1 .or. ind(50).eq.3 ) then

!     --- velocity vector at the end of previous timestep

         if ( ioldm.ne.0 )
     +      call el2005 ( vecold, uoldm, wrk1(ipdudx), wrk1(ipphi),
     +                    dphidx, index3, index1, wrk1(ipugs),
     +                    ioldm, index4 )

!        --- most recently computed velocity vector

         call el2005 ( vecold, uold, wrk1(ipdudx), wrk1(ipphi), dphidx,
     +                 index3, index1, wrk1(ipugs), iseqin, index4 )
         if ( debug ) then
            call prinrl1 ( uold, ndim, n, 'uold' )
         end if

      end if

!     --- Store extra velocity field for Goertler in wrk1
!         This includes the derivatives

      if ( mcont.eq.2 )
     +   call el2005 ( vecold, wrk1(ipunew), wrk1(ipdudxnw),
     +                 wrk1(ipphi), dphidx, index3, index1,
     +                 wrk1(ipunewgs), imesh, index4 )

!     --- Store extra velocity for ALE mesh in wrk1

      if ( imesh.ne.0 )
     +   call el2005 ( vecold, wrk1(ipunew), wrk1(ipdudxnw),
     +                 wrk1(ipphi), dphidx, index3, index1,
     +                 wrk1(ipunewgs), imesh, index4 )

      if ( ind(50).eq.2 .or. ind(50).eq.3 )
     +   call el3009 ( vecold, index3, index4, vecloc,
     +                 wrk1(ipphi), n, wrk1(ipwork) )

      do i = isub, isup

         if ( ind(i).gt.0 .or. ist(i).gt.0 ) then

!        --- Coefficient is variable

            jdiags = jdiag
            if ( mcont.eq.1 .and. i.eq.1 .and. jdiag.ne.1 ) jdiag = 1
            call elflr7 ( i, user, wrk1(1+(i-1)*m), index1, x, xgauss,
     +                    m, n, wrk1(ipphi), vecold, wrk1(ipwork), 1,
     +                    index1, index3, index4, numold, iuser,
     +                    wrk1(ipugs), vecloc, maxunk )
            if ( mcont.eq.1 .and. i.eq.1 .and. jdiags.ne.1 ) then

!           --- rho requires special attention in case of compressible flow
!               Copy rho to part 7 and compute rho in Gauss points

               do j = 1, n
                  wrk1(iprho-1+j) = wrk1(j)
               end do
               do j = 1, m
                  wrk1(j) = ddot( n, wrk1(iprho), 1,
     +                            wrk1(ipphi+(j-1)*m), 1 )
               end do
               jdiag = jdiags

            end if

         end if

      end do

!     --- Compute the divergence and gradient matrix

      if ( mcont.eq.3 ) then

!     --- mcont = 3, special divergence and gradient matrix

         call el1000sp ( wrk1(ipsp), wrk1(ipdiv), dphidx, w,
     +                   x, xgauss, wrk1(ipwork),
     +                   rhsdiv, wrk1(1+8*m), mdiv, ishape,
     +                   wrk1(1+13*m), wrk1(1+10*m) )

      else

!     --- Standard divergence and gradient matrix

         call el1000 ( wrk1(ipsp), wrk1(ipdiv), dphidx,
     +                 wrk1(ippp), wrk1(ipphi), w,
     +                 x, xgauss, mcont, wrk1(iprho), wrk1(7*m+1),
     +                 wrk1(ipwork), rhsdiv, wrk1(1+8*m), mdiv, ishape )

      end if

      if ( jtrans.eq.1 ) then

!     --- Compute the transformation matrix
!         Compute the velocity in the centroid and the derivative of u

         call el2000 ( wrk1(iptran), wrk1(ipdiv), mdiv, rhsdivtr,
     +                 rhsdiv )
         if ( mcont.ne.0 )
     +      call el2000 ( wrk1(iptrnp), wrk1(ipsp), 0, rhsdivtr,
     +                    rhsdiv )
         if ( (mconv.gt.0 .or. modelv.gt.1) .and.
     +        (ind(50).eq.1 .or. ind(50).eq.3) )
     +      call el3000 ( uold, wrk1(ipdudx), wrk1(ipphi), dphidx,
     +                    wrk1(iptran), wrk1(ipugs), mdiv, rhsdivtr )

      end if

!     --- Fill parts of matrix if necessary

      if ( matrix ) then

!     --- matrix = true    compute element matrix

         if ( modelv.eq.5 ) then

!        --- Special case the coefficients are given explicitly

            call el4915sp ( wrk1(ipelem), dphidx, wrk1(ipphi), xgauss,
     +                      wrk1(1+23*m), w, wrk1(ipwork),
     +                      wrk1(ipwork+n*n) )

         else

!        --- Standard case: fill the viscosity in array etaeff

            call elvisc ( modelv, wrk1(1+5*m), wrk1(ipetef), cn, clamb,
     +                    xgauss, wrk1(ipugs), wrk1(ipdudx),
     +                    wrk1(ipseci), wrk1(ipdetd), wrk1(ipwork),
     +                    vecloc, maxunk, numold )

!           --- Compute the viscous terms

            call el4915 ( wrk1(ipelem), dphidx, wrk1(ipphi), xgauss,
     +                    wrk1(ipetef), w, wrk1(ipwork),
     +                    wrk1(ipwork+n*n), mcont )

         end if

         if ( ind(2).ne.0 ) then

!        --- Coriolis accelaration

            call el4914 ( wrk1(ipelem), wrk1(ipphi), wrk1, w, wrk1(m+1),
     +                    wrk1(ipwork), wrk1(ipwork+n*n) )

         end if

         if ( mconv.gt.0 .and. mconv.ne.4 ) then

!        --- compute convective terms

            symm = .false.
            if ( imesh.eq.0 .or. mcont.eq.2 ) then

!           --- Standard situation

               ipcor = ipugs

            else

!           --- Subtract mesh velocity from velocity

               ipcor = ipunew
               call el1015 ( wrk1(ipugs), wrk1(ipunewgs), wrk1(ipunew),
     +                       m, ndim )

            end if

            if ( mcoefconv.gt.0 ) then

!           --- compute convective terms where the coefficients are stored
!               in the special way

               call el2010sp ( wrk1(ipelem), dphidx, wrk1(ipphi),
     +                         wrk1(1+14*m), w, mconv, wrk1(ipcor),
     +                         wrk1(ipdudx), xgauss,
     +                         wrk1(ipwork), wrk1(ipwork+n*n) )

            else if ( mcont.eq.2 ) then

!           --- compute convective terms in case of Goertler equations

               call el2010g ( wrk1(ipelem), dphidx, wrk1(ipphi), wrk1,
     +                        w, mconv, wrk1(ipugs), wrk1(ipdudx),
     +                        xgauss, wrk1(ipwork), wrk1(ipwork+n*n),
     +                        wrk1(ipunew), wrk1(ipdudxnw), wrk1(6*m+1),
     +                        wrk1(7*m+1) )

            else

!           --- compute convective terms in standard case

               if ( debug ) call prinrl1 ( wrk1(ipcor), ndim, m, 'u' )
               call el2010 ( wrk1(ipelem), dphidx, wrk1(ipphi), wrk1, w,
     +                       mconv, wrk1(ipcor), wrk1(ipdudx), xgauss,
     +                       wrk1(ipwork), wrk1(ipwork+n*n) )

            end if

         else if ( imesh.gt.0 ) then

!        --- No convection, only mesh velocity

            mconv = 1
            do i = 0, nveloc-1
               wrk1(ipugs+i) = 0
            end do
            call el1015 ( wrk1(ipugs), wrk1(ipunewgs), wrk1(ipunew), m,
     +                    ndim )
            call el2010 ( wrk1(ipelem), dphidx, wrk1(ipphi), wrk1, w,
     +                    mconv, wrk1(ipunew), wrk1(ipdudx), xgauss,
     +                    wrk1(ipwork), wrk1(ipwork+n*n) )
            mconv = 0

         end if

      end if

      if ( vector .and. notvec.eq.0 ) then

!     --- vector = true
!         First set element vector equal to zero

         do i = 1, ndim*n
            wrk1(ipelvc+i-1)=0d0
         end do

!        --- Next compute contribution due to external forces
!            Multiply weights by density rho and store in wrk1 from
!            position iprhoc

         if ( debug ) call prinrl ( wrk1, m, 'rho' )
         if ( ind(3).ne.0 .or. ind(4).ne.0 .or. ind(5).ne.0 ) then

!        --- At least one body force vector available

            do i = 1, m
               wrk1(iprhoc+i-1) = w(i) * wrk1(i)
            end do

         end if

!        --- First component

         if ( ind(3).ne.0 ) then

!        --- f1 # 0  store contribution temporary in array wrk1

            jadd = 0
            call el3003 ( wrk1(2*m+1), wrk1(ipwork), wrk1(iprhoc),
     +                    wrk1(ipphi), m, n, wrk1(ipwork+n) )          
c           *** PvK: then add tracer contribution
            jadd = 1
            if (ibuoy_trac.ge.1) then
               call pel3003( wrk1(2*m+1), wrk1(ipwork), wrk1(iprhoc),
     +                    wrk1(ipphi), m, n, wrk1(ipwork+n),x,1)
            endif
            do i = 1, n
               wrk1(ipelvc+ndim*(i-1)) = wrk1(ipwork+i-1)
            end do

         end if

!        --- Second component

         if ( ind(4).ne.0 ) then

!        --- f2 # 0  store contribution temporary in array wrk1

            jadd = 0
            call el3003 ( wrk1(3*m+1), wrk1(ipwork), wrk1(iprhoc),
     +                    wrk1(ipphi), m, n, wrk1(ipwork+n) )
c           *** PvK: then add tracer contribution
            jadd = 1
            if (ibuoy_trac.ge.1) then
               call pel3003( wrk1(3*m+1), wrk1(ipwork), wrk1(iprhoc),
     +                    wrk1(ipphi), m, n, wrk1(ipwork+n),x,2)
            endif
            do i = 1, n
               wrk1(ipelvc+ndim*(i-1)+1) = wrk1(ipwork+i-1)
            end do

         end if

!        --- Third component

         if ( ind(5).ne.0 ) then

!        --- f3 # 0  store contribution temporary in array wrk1

            jadd = 0
            call el3003 ( wrk1(4*m+1), wrk1(ipwork), wrk1(iprhoc),
     +                    wrk1(ipphi), m, n, wrk1(ipwork+n) )
            do i = 1, n
               wrk1(ipelvc+ndim*(i-1)+2) = wrk1(ipwork+i-1)
            end do

         end if

         if ( ind(10).ne.0 ) then

!        --- Stress term corresponding to right-hand side

            call elstrrhs ( wrk1(9*m+1), dphidx, wrk1(ipelvc),
     +                      w, m, n, ndimlc, wrk1(ipphi), xgauss,
     +                      modelrhsd  )

         end if
         if ( mconv.eq.2 .or. mconv .eq.4 ) then

!        --- compute contribution of convective terms

            if ( mconv.eq.4 ) then
               if ( jtime.gt.0 ) then
                  adams = -23d0/12d0
               else
                  adams = -1d0
               end if
            else
               adams = 1d0
            end if

            if ( mcoefconv.gt.0 ) then

!           --- compute convective terms where the coefficients are stored
!               in the special way

               call el2011sp ( adams, wrk1(ipelvc), wrk1(ipphi),
     +                         wrk1(1+14*m), w, wrk1(ipugs),
     +                         wrk1(ipdudx), xgauss,
     +                         wrk1(ipwork), wrk1(ipwork+n) )

            else if ( mcont.eq.2 ) then

!           --- compute convective terms in case of Goertler equations

               call el2011g ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                        wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                        wrk1(ipwork), wrk1(ipwork+n),
     +                        wrk1(6*m+1), wrk1(7*m+1) )
            else

!           --- compute convective terms in standard case

               call el2011 ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                       wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                       wrk1(ipwork), wrk1(ipwork+n) )

            end if

         end if

         if ( modelv.eq.2 .and. cn.gt.1d0 ) then

!        --- modelv = 2, and cn>1:  Newton linearization

            call el4029 ( wrk1(ipelvc), xgauss, wrk1(ipugs),
     +                    wrk1(ipdudx), wrk1(ipseci), wrk1(ipdetd),
     +                    wrk1(ipwork), dphidx, w )
         end if

      end if

      if ( jtime.gt.0 ) then

!     --- jtime > 0, i.e. the mass matrix must be computed
!         First compute the mass matrix for one velocity component
!         The non-diagonal mass matrix for this component is stored in
!         wrk1 positions ipwork - ipwork+n*n-1
!         The diagonal matrix in wrk1 pos. ipwork+n*n - ipwork+n*n+n-1

         jadd = 0
         if ( jtime.gt.10 ) then

!        --- non-diagonal mass matrix

            jzero = 2
            call el3002 ( wrk1, wrk1(ipwork), w, wrk1(ipphi), m, n, n,
     +                    wrk1(ipwork+n*n) )

         end if
         if ( imas.eq.1 ) then

!        --- diagonal mass matrix

            jconsf = jtime
            call el3003 ( wrk1, wrk1(ipwork+n*n), w, wrk1(ipphi), m ,n,
     +                    wrk1(ipwork+n+n*n) )

         end if

         if ( imas.eq.1 ) then

!        --- imas = 1, build diagonal mass matrix

            do i = 1, n
               do j = 1, ndim
                  elemms(ndim*(i-1)+j) = wrk1(ipwork+n*n+i-1)
               end do
            end do

         else if ( imas.eq.2 ) then

!        --- imas = 2, build non-diagonal mass matrix

            jadd = 0
            if ( ndim.eq.3 .and. inpelm.eq.27 .and. itype.eq.902 ) then

!           --- Special case, because centroid is not the last point

               call elmass27 ( elemms, wrk1(ipwork) )

            else

!           --- Standard case

               do i = 1, ndim
                  if ( jtrans.eq.1 ) then
                     call el3010 ( wrk1(ipmass), wrk1(ipwork), ndim*n,
     +                             n, i, i, ndim, .false., 1d0 )
                  else
                     call el3010 ( elemms, wrk1(ipwork), icount, n, i,
     +                             i, ndim, .false., 1d0 )
                  end if
               end do
            end if

         end if
         if ( jtime.gt.100 ) then

!        --- jtime > 100, explicit theta method
!            Update matrix, i.e. add 1/(theta dt) M to the stiffness matrix

            jadd = 1
            do i = 1,ndim
               call el3010 ( wrk1(ipelem), wrk1(ipwork), ndim*n, n,
     +                       i, i, ndim, .false., thetdt )
            end do

!           --- Compute 1/(theta dt) M u(n) and add to right-hand side

            jadd = 1
            do i = 1,ndim

               if (ioldm.ne.0) then

!              --- iterative procedure adopted for dynamic analysis:
!                  use velocity vector at end of previous time step

                  call el1010 ( wrk1(ipwork), uoldm((i-1)*n+1),
     +                          wrk1(ipelvc), thetdt, i, ndim )

               else

!              --- standard case, no iterations: uold = uoldm

                  call el1010 ( wrk1(ipwork), uold((i-1)*n+1),
     +                          wrk1(ipelvc), thetdt, i, ndim )

               end if

            end do

         end if

         if ( mconv.eq.4 ) then

!        --- successive substitution

            call el3007 ( vecold, uold, wrk1(ipdudx), wrk1(ipphi),
     +                    dphidx, m, n, ndimlc, ncomp, index3, index1,
     +                    work, wrk1(ipugs), jtrans, numold, 2, index4 )
            adams = 16d0/12d0

            if ( mcont.eq.2 ) then

!           --- compute convective terms in case of Goertler equations

               call el2011g ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                        wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                        wrk1(ipwork), wrk1(ipwork+n),
     +                        wrk1(6*m+1), wrk1(7*m+1) )
            else

!           --- standard Navier Stokes equation

               call el2011 ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                       wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                       wrk1(ipwork), wrk1(ipwork+n) )
            end if

            call el3007 ( vecold, uold, wrk1(ipdudx), wrk1(ipphi),
     +                    dphidx, m, n, ndimlc, ncomp, index3, index1,
     +                    work, wrk1(ipugs), jtrans, numold, 3, index4 )
            adams = -5d0/12d0

            if ( mcont.eq.2 ) then

!           --- compute convective terms in case of Goertler equations

               call el2011g ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                        wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                        wrk1(ipwork), wrk1(ipwork+n),
     +                        wrk1(6*m+1), wrk1(7*m+1) )
            else

!           --- standard Navier Stokes equation

               call el2011 ( adams, wrk1(ipelvc), wrk1(ipphi), wrk1, w,
     +                       wrk1(ipugs), wrk1(ipdudx), xgauss,
     +                       wrk1(ipwork), wrk1(ipwork+n) )
            end if

         end if

      end if

      if ( matrix ) then

!     --- Put divergence and gradient part into matrix
!         Eliminate centroid if necessary
!         Compute penalty contribution if necessary

         if ( itype.eq.901 .or. itype.eq.902 ) then

!        --- itype = 901 or 902 integrated method

            if ( jtrans.eq.1 ) then

!           --- jtrans = 1, eliminate centroid and new element matrix into
!                           wrk1(ipelcp)

               call el2107 ( wrk1(ipelcp), wrk1(ipelem), wrk1(iptran),
     +                       wrk1(iptrnp) )

            end if

!           --- Compute the complete matrix

            if ( inpelm.eq.27 ) then

!           --- inpelm = 27, special case since centroid is not the end point

               call el2203 ( elemmt, wrk1(ipelcp), wrk1(ipsp),
     +                       wrk1(ipdiv), wrk1(ippp), penalp, mcont,
     +                       w, x, xgauss )

            else

!           --- inpelm # 27, standard situation
!               First put the velocity stiffness matrix into the large matrix

               jadd = 0
               call el3010 ( elemmt, wrk1(ipelcp), icount,
     +                       ndim*(n-jtrans), 1, 1, 1, .false., 1d0 )

!              --- Next put gradient, divergence and pressure matrix into
!                  elemmt

               call el4921 ( elemmt, wrk1(ipsp), wrk1(ipdiv),
     +                       wrk1(ippp), nveloc, npres, penalp, mcont,
     +                       w, x, xgauss )

            end if

         else

!        --- Compute the penalty contribution

            call el4920 ( elemmt, wrk1(ipelem), wrk1(ipsp),
     +                    wrk1(ipdiv), w, x, xgauss, penalp )
            if ( jtrans.eq.1 ) then

!           --- 2D extended 6-point quadratic triangles

               call el2107 ( elemmt, wrk1(ipelem), wrk1(iptran),
     +                       wrk1(iptrnp) )

            end if

         end if

      end if
      if ( itype.ne.901 .and. jtrans.eq.1 .and. imas.eq.2 ) then

!     --- Eliminate centroid in case of 2D quadratic elements
!         The resulting matrix is stored in elemms

          call el2107 ( elemms, wrk1(ipmass), wrk1(iptran),
     +                  wrk1(iptrnp) )

      end if
      if ( vector ) then

!     --- Copy element vector in elemvc

         if ( jtrans.eq.1 ) then

!        --- Eliminate centroid in case of 2D quadratic elements
!            The resulting vector is stored in elemvc

            call el2201 ( elemvc, wrk1(ipelvc), wrk1(iptrnp), mdiv,
     +                    wrk1(ipelem), rhsdivtr )

         else

!        --- Copy vector

            if ( itype.eq.902 .and. inpelm.eq.27 ) then

!           --- 27-point element, integrated method

               call el1006 ( elemvc, wrk1(ipelvc) )

            else

!           --- Standard situation

               do i = 1, ndim*n
                  elemvc(i) = wrk1(ipelvc+i-1)
               end do

            end if

         end if
         if ( mdiv.eq.1 ) then

!        --- mdiv = 1, divergence right-hand side

            if ( itype.eq.900 ) then

!           --- Penalty formulation

               call el4900 ( elemvc, wrk1(ipsp), rhsdiv, w, x, xgauss,
     +                       penalp )

            else if ( inpelm.eq.27 ) then

!           --- 27-point element

               do i = 1, npres
                  elemvc(i+ndim*13) = rhsdiv(i)
               end do

            else

!           --- Standard case

               do i = 1, npres
                  elemvc(icount-npres+i) = rhsdiv(i)
               end do

            end if

         end if

      end if

1000  if ( ierror.ne.0 ) call instop

      call erclos ( 'elm900' )

      if ( debug ) then

!     --- debug = true

         write(irefwr,*) 'element ', ielem, ' itype ',itype,
     +                   ' jtime ', jtime
         call prinrl(elemvc,icount,'elemvc')
         call prinrl1(elemmt,icount,icount,'elemmt')
         if ( jtime.gt.0 ) then

!        --- jtime > 0, mass matrix

             if ( imas.eq.1 ) then

!            --- imas = 1, diagonal mass matrix

               call prinrl(elemms,icount,'elemms')

             else

!            --- imas = 2, non-diagonal mass matrix

                call prinrl1(elemms,icount,icount,'elemms')

             end if
         end if

         call elm900check ( x, elemmt, npres, nveloc, elemvc )

      end if

      end
