c *************************************************************
c *   CYLSTOKES
c *
c *   Solve Stokes equation in cylindrical geometry or Cartesian geometry
c * 
c *   In case of cylindrical geometry: 
c *     Make sure to enforce boundary conditions and change
c *     the local transformations back to global transformations
c *
c *     The local transformations provide the mechanism to impose
c *     boundary conditions normal and/or tangential to the grid
c *     boundary (see UM 3.2.2). Proper calls to loctrn() are essential 
c *     when using non-Newtonian rheology.
c * 
c *   PvK 071405
c *************************************************************
      subroutine stokes(ichoice,iuser,user)
      implicit none 
      integer ichoice,iuser(*)
      real*8 user(*)

      include 'mysepar.inc'
      include 'vislo.inc'
      include 'c1visc.inc'
      include 'penoniter.inc'
      include 'cbuoy_trac.inc'
      include 'petrac.inc'
      include 'pecof900.inc'
      include 'c1mark.inc'
      include 'tracer.inc'
      include 'ccc.inc'

      real*8 rmax,rmax2,subdif,anorm,vrms,volint,vrms2
      integer ielhlp,ihelp,npoint,nelem,nelgrp,ikelmc,ikelmi,ikelmo
      integer iout
      real*4 t00,t01,second
      character*80 fname,rname

c     *** Set flag indicating whether to use coormark (1) or coornewm (1)
c     *** in computing buoyancy force, if necessary
      if (Rb_local.ne.0) then
         ibuoy_trac=ichoice
      else
         ibuoy_trac=0
      endif

      nsub(ichoice)=0
100   continue
c       *** make sure essential boundary conditions are enforced
        if (cyl) call velocity_bc(0)
c       *** fill coefficients in arrays user/iuser
        call pefilcof(1,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                 islol1,iheat,idens,ivisc,isecinv,iuser,user)
c       *** Solve Stokes equations
        if (itracoption.eq.2) then
           iout=0
           if (ibuoy_trac.eq.1) then
c             write(6,*) 'mardiv called from stokes with coormark'
              call mardiv(coormark,rname,iout)
           else if (ibuoy_trac.eq.2) then
c             write(6,*) 'mardiv called from stokes with coornewm'
              call mardiv(coornewm,rname,iout)
           endif
        endif
        call bmstokes(kmesh1,kprob1,intmt1,iuser,user,
     v                isol1,islol1,matr1)

c     *** non-Newtonian iteration
        if (ivn.or.tackley) then
            if (cyl) call loctrn(1,isol1,kprob1)
            write(6,*) 'PERROR(stokes): ivn.or.tackley'
            call instop
            nsub(ichoice) = nsub(ichoice)+1
            rmax = anorm(1,1,0,kmesh1,kprob1,isol1,isol1,ielhlp)
            rmax2 = anorm(1,1,0,kmesh1,kprob1,islol1(1,1),
     v                islol1(1,1),ielhlp)
            subdif = anorm(0,1,0,kmesh1,kprob1,isol1,
     v                   islol1(1,1),ielhlp)
            if (rmax.gt.0) subdif = subdif/rmax
            write(6,'(''N: '',2i3,3f15.3)')
     v               1,nsub(ichoice),subdif/rmax,rmax,rmax2
            if (subdif/rmax.gt.subeps.and.nsub(ichoice).lt.nsubmax) then
                call copyvc(isol1,islol1)
                goto 100
            endif
         if (cyl) call loctrn(2,isol1,kprob1)
        endif

      return
      end

c *************************************************************
c *   BMSTOKESNEW
c *************************************************************
      subroutine bmstokes(kmesh,kprob,intmat,iuser,user,
     v                    isol,islol,matr)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),iuser(*)
      real*8 user(*)
      integer isol(*),islol(5,*),matr(*)
      integer irhsd(5),matrback(5),iinbld(20),matrm(5)
      integer inpsol(30),iread,ipoint
      real*8 rinsol(10),a,b,p,qu
      save irhsd,matrback,iinbld,matrm
      logical rebuild

      include 'peparam.inc'
      include 'c1visc.inc'
      include 'solutionmethod.inc'
      include 'cpix.inc'
      include 'pecof900.inc'
      include 'tracer.inc'

      integer ifirst,i
      save ifirst
      data ifirst/0/

      do i=1,20
         iinbld(i) = 0
      enddo

      if (itypv.gt.1.or.ifirst.eq.0.or.isolmethod.ne.0) then
c        *** Build system: A and f
         rebuild = .true.
         iinbld(1) = 10
         iinbld(2) = 1
         iinbld(10) = 3
         write(6,*) 'bmstokes: matr(4): ',matr(4)
         call build(iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,
     v              islol,iuser,user)
c        call prinrv(irhsd,kmesh,kprob,5,0,'rhsd')
c        call prinrv(islol(1,3),kmesh,kprob,5,0,'islol3')
c        write(6,*) 'bmstokes: matr(4): ',matr(4)
c        call copymt(matr,matrback,kprob)
      else
c        *** Build only f
         rebuild = .false.
         iinbld(1) = 10
         iinbld(2) = 2
         iinbld(10) = 3
         call build(iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,
     v              islol,iuser,user)
      endif

      if (isolmethod.eq.-1) then
c       *** Element 903; direct solution method
        call solve(1,matr,isol,irhsd,intmat,kprob)
      else
        if (isolmethod.eq.0) then
c          *** Penalty function method, direct, iposst=1
           inpsol(1) = 3
c          *** profile method
           inpsol(2) = 1
c          *** solution method
           inpsol(3) = 0
        else
           inpsol(1) = 14
c          *** ipos
           inpsol(2) = 0
c          write(6,*) 'isolmethod: ',isolmethod
c          *** solution_method 1=CG 2=CGS 3=GMRES 4=GMRESR
           inpsol(3) = isolmethod
c          *** ipreco 1=diag 2=eisenstat 3=ILU
           inpsol(4) = ipreco
           inpsol(5) = maxiter
           inpsol(6) = iprint
c          *** matrix is symmetric(1) or not (0)?
           inpsol(7) = 0
c          *** dimension of Krylov space for GMRES
           inpsol(8) = 20
c          *** ISTART: 1=starts with given vector
           inpsol(9) = 1 
c          *** KEEP: 1=keep preconditioning matrix; 
           if (rebuild.and.keep.eq.1) then
              inpsol(10)= 1
           else if (keep.eq.1.and.ifirst.eq.1) then
c             *** use old matrix
              inpsol(10) = 2
           else
              inpsol(10) = 1
           endif
!          inpsol(10)=0  !! PvK
c          write(6,*) 'KEEP: ',inpsol(10)
           inpsol(11)= 2
           inpsol(12)= 0
c          *** NTRUNC: for GMRESR: maximum number of search directions
           inpsol(13)= 5
c          *** NINNER: for GMRESR: maximum number of inner loop iters
           inpsol(14)= 5
c          *** EPS: required accuracy for iteration process
           rinsol(1) = cgeps
c          write(6,*) 'cgeps: ',cgeps
        endif
        iread = -1
        call solvel(inpsol,rinsol,matr,isol,irhsd,intmat,kmesh,
     v              kprob,iread)
      endif
      ifirst=1

      return
      end


