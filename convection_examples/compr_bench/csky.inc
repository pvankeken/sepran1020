      real*8 rho_u_0,rho_u_tr,rho_l,rho_ecl0,rho_ecl_cmb
      real*8 dep1,drho1,dep2,drho2,drho_s1,drho_s2
      integer krestart
      logical sky_filter
      common /skydenecl/rho_u_0,rho_u_tr,rho_l,rho_ecl0,rho_ecl_cmb,
     v               dep1,drho1,dep2,drho2,drho_s1,drho_s2
      common /skyrestart/ krestart,sky_filter

      real*8 vis_ord
      common /svis/ vis_ord
