      integer NRDIVMAX,NDIVMAX
      parameter(NRDIVMAX=1001,NDIVMAX=100 000)
      real*8 drgrid,dthgrid,rgrid
      integer nrdiv,nthgrid,ithgrid,ngridtot,idiv
      common /hegrid/ drgrid,dthgrid(NRDIVMAX),rgrid(NRDIVMAX),   
     v                ithgrid(NRDIVMAX),nthgrid(NRDIVMAX), 
     v                ngridtot,nrdiv
      common /cdiv/ idiv(NDIVMAX)

