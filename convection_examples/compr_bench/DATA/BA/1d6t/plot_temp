#!/bin/bash
# Plots color image + contours of temperature
if [ $# -ne 1 ]; then
   echo "supply the time step as first argument as 001, 002, etc."
   exit 1
fi
file=TEMP_$1   # NB: you need to supply snapshot number 001, 002, ...
name=`basename $PWD`  # shows current directory (without full path)
portrait=-P

# Find the region from the coordinate file that compr_bench made in GMT
region=`cat GMT/coor.dat | awk '{print $2 , $3}' | minmax -I0.1`
# 5cm represents non-dimensional 1
projection=x5c

# Plot frame; -K indicates more postscript info will be added later
psbasemap -J$projection $region -X5c -K $portrait -Ba0.2f0.1:"x":/a0.2f0.1:"y"::."$name":WSne  > $file.eps

# Make grd file from temperature data. You need to know the spacing dx
dx=`tail -1 GMT.log | awk '{print $1}'`
echo "dx = $dx"
echo "xyz2grd GMT/T"$1".dat -GT.grd -Z -I$dx $region"
xyz2grd GMT/T"$1".dat -GT.grd -Z -I$dx $region 

# Make a color palette. This red-white-blue is much better than rainbow
makecpt -Cpolar -T0/1/0.05 -Z > colors.cpt

# Make image of grid file; -O indicates it is a continuation of a PS file
grdimage T.grd -Ccolors.cpt -E200 $region -J$projection -K $portrait -O >> $file.eps
grdcontour T.grd -C0.1 -J$projection $region -K $portrait -O >> $file.eps

# Plot a color scale
psscale -D-3/2.5/2.5/1 -A -Bf0.2g0.1a0.2 -Ccolors.cpt $portrait -K -O >> $file.eps

# This is necessary to make sure the postscript file is terminated
echo "showpage" >> $file.eps

# Create PDF and display
ps2pdf $file.eps
evince $file.pdf

