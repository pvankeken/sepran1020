c *************************************************************
c *   TRACDENS
c *
c *   Calculate density of tracers in nodal points
c *   Parameters:
c *        kmesh1   i  mesh definition for velocity
c *        kprob1   i  problem definition for velocity
c *        coormark i  array containing tracers
c *        idens   o  vector of special structure containing density
c * 
c *
c *   PvK 080300
c *************************************************************
      subroutine tracdens(kmesh1,kprob1,coormark,idens) 
      implicit none
      integer kmesh1(*),kprob1(*),idens(*)
      real*8 coormark(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      include 'tracer.inc'
      include 'ccc.inc'

      integer ikelmc,ikelmi,npoint,ntot,i,j,k,iel
      real*8 xm,ym,xn(6),yn(6),un(6),vn(6),u,v,done
      integer nodno(6),imissed,inidgt,iniget,isub,nelem,ikelmo,ifound
      integer findsubelem,icorrect,ipdens,index,nodlin(3),nelgrp
      real*8 r,th,rl(3),xl(3),yl(3),tracdens04,user(1),shapef(6)
      real*8 xi,eta
      logical guess_first

      if (itracoption.eq.2) then
         write(6,*) 'PERROR(tracdens): not suited for markerchain'
         call instop
      endif
      npoint = kmesh1(8)
      nelem = kmesh1(9)
      nelgrp = kmesh1(5)
      if (periodic) nelem = kmesh1(kmesh1(21))

c     *** check whether idens exists
      if (idens(1).eq.0) then
         write(6,*) 'PERROR(tracdens): vector idens does not exist'
         call instop
      endif
      if (idens(2).ne.115) then
         write(6,*) 'PERROR(tracdens): vector dens exists'
         write(6,*) 'but is not of type VECTR: ',idens(2)
         call instop
      endif
      if (idens(4).ne.1) then
          write(6,*) 'PERROR(tracdens): vector dens exists'
          write(6,*) 'and is of type VECTOR'
          write(6,*) 'but IVEC is wrong: ',idens(4)
          call instop
      endif
      if (idens(5).ne.npoint) then
          write(6,*) 'PERROR(tracdens): vector dens exists'
          write(6,*) 'and is of type VECTOR with IVEC=1'
          write(6,*) 'but NUMDEGFD is not NPOINT: ',idens(5)
          call instop
      endif

      call ini050(idens(1),'tracdens: dens')
      call ini050(kmesh1(23),'tracdens: coordinates')
      call ini050(kmesh1(17),'tracdens: nodal points')
      call ini050(kmesh1(29),'tracdens: kmesh part o')

      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
      ikelmo = iniget(kmesh1(29))
      ipdens = inidgt(idens(1))

c     *** Set vector to zero
      call tracdens02(buffr(ipdens),npoint)


      ntot = 0
      do idist=1,ndist
         ntot = ntot+ntrac(idist)
      enddo
      write(6,*) 'volume, ntot: ',volume,ntot

      guess_first=.false.
      imissed = 0
      ifound=0
      do i=1,ntot
         xm = coormark(2*i-1)
         ym = coormark(2*i)
c *************************************************************
c *      Find the element in which (xm,ym) lies
c *      Look it up in the table 
c *************************************************************
         call pedeteltrac(0,xm,ym,ikelmc,ikelmi,ikelmo,nodno,nodlin,
     v        rl,xn,yn,un,vn,user,iel,npoint,nelem,imissed,ifound,
     v        nelgrp,shapef,xi,eta,guess_first,'tracdens') 

c        *** Now that we have the right element, subdivide it into
c        *** four linear ones and find correct subtriangle
         isub = findsubelem(xn,yn,xm,ym,xl,yl,rl,nodno,nodlin)

c        write(6,*) 'rl: ',(rl(j),j=1,3),(nodlin(j),j=1,3)
         call tracdens01(buffr(ipdens),nodlin,rl)

      enddo

c     *** print the vector
c     call tracdens03(buffr(ipdens),npoint)
      write(6,*) 'sum of dens: ',tracdens04(buffr(ipdens),npoint)

c     write(6,*) 'Missed about ',imissed,' points'
      if (imissed.gt.0.01*ntot) then
         write(6,*) 'PWARN(tracdens): interpolation of velocity'
         write(6,*) '  is inefficient for more than 1% of the tracers'
         write(6,*) '  Time to rethink the mesh gridding strategy?'
         write(6,*) '  imissed = ',imissed
      endif

      return
      end
  

      integer function findsubelem(xn,yn,xm,ym,xl,yl,rl,nodno,nodlin) 
      implicit none
      real*8 xn(6),yn(6),xm,ym
      real*8 xl(3),yl(3),a(3),b(3),c(3)
      real*8 eps,rl(3),r
      integer isub,nodlin(3),nodno(6),iprint
      parameter(eps=1d-4)
      logical out

      iprint=0
100   continue
      isub=1

      xl(1)=xn(1)
      yl(1)=yn(1)
      xl(2)=xn(2)
      yl(2)=yn(2)
      xl(3)=xn(6)
      yl(3)=yn(6)
      nodlin(1)=nodno(1)
      nodlin(2)=nodno(2)
      nodlin(3)=nodno(6)
      
      call trilin(xl,yl,a,b,c)
      rl(1) = a(1)+b(1)*xm+c(1)*ym
      rl(2) = a(2)+b(2)*xm+c(2)*ym
      rl(3) = a(3)+b(3)*xm+c(3)*ym
      out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
      if (iprint.eq.1) write(6,'(i5,3f8.3,L10)') 
     v       isub,rl(1),rl(2),rl(3),out
      if (out) then
         findsubelem = isub
         return
      endif

      isub=2

      xl(1)=xn(2)
      yl(1)=yn(2)
      xl(2)=xn(3)
      yl(2)=yn(3)
      xl(3)=xn(4)
      yl(3)=yn(4)
      nodlin(1)=nodno(2)
      nodlin(2)=nodno(3)
      nodlin(3)=nodno(4)
      
      call trilin(xl,yl,a,b,c)
      rl(1) = a(1)+b(1)*xm+c(1)*ym
      rl(2) = a(2)+b(2)*xm+c(2)*ym
      rl(3) = a(3)+b(3)*xm+c(3)*ym
      out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
      if (iprint.eq.1) write(6,'(i5,3f8.3,L10)') 
     v       isub,rl(1),rl(2),rl(3),out
      if (out) then
         findsubelem = isub
         return
      endif

      isub=3

      xl(1)=xn(4)
      yl(1)=yn(4)
      xl(2)=xn(5)
      yl(2)=yn(5)
      xl(3)=xn(6)
      yl(3)=yn(6)
      nodlin(1)=nodno(4)
      nodlin(2)=nodno(5)
      nodlin(3)=nodno(6)
      call trilin(xl,yl,a,b,c)
      rl(1) = a(1)+b(1)*xm+c(1)*ym
      rl(2) = a(2)+b(2)*xm+c(2)*ym
      rl(3) = a(3)+b(3)*xm+c(3)*ym
      out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
      if (iprint.eq.1) write(6,'(i5,3f8.3,L10)') 
     v       isub,rl(1),rl(2),rl(3),out
      if (out) then
         findsubelem = isub
         return
      endif

      isub=4

      xl(1)=xn(2)
      yl(1)=yn(2)
      xl(2)=xn(4)
      yl(2)=yn(4)
      xl(3)=xn(6)
      yl(3)=yn(6)
      nodlin(1)=nodno(2)
      nodlin(2)=nodno(4)
      nodlin(3)=nodno(6)
      
      call trilin(xl,yl,a,b,c)
      rl(1) = a(1)+b(1)*xm+c(1)*ym
      rl(2) = a(2)+b(2)*xm+c(2)*ym
      rl(3) = a(3)+b(3)*xm+c(3)*ym
      out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
      if (iprint.eq.1) write(6,'(i5,3f8.3,L10)') 
     v       isub,rl(1),rl(2),rl(3),out
      if (out) then
         findsubelem = isub
         return
      endif
    
      if (iprint.eq.1) then
        xl(1)=xn(1)
        yl(1)=yn(1)
        xl(2)=xn(3)
        yl(2)=yn(3)
        xl(3)=xn(5)
        yl(3)=yn(5)
        call trilin(xl,yl,a,b,c)
        rl(1) = a(1)+b(1)*xm+c(1)*ym
        rl(2) = a(2)+b(2)*xm+c(2)*ym
        rl(3) = a(3)+b(3)*xm+c(3)*ym
        out = (rl(1).ge.-eps).and.(rl(2).ge.-eps).and.(rl(3).ge.-eps)
        write(6,*) 'quad: '
        write(6,'(i5,3f8.3,L10)') 
     v       isub,rl(1),rl(2),rl(3),out
      endif

      if (iprint.eq.0) then
        r= sqrt(xm*xm+ym*ym)
        write(6,*) 'PWARN(findsubelem): tracer is not in '
        write(6,*) ' current element based on subdivision'
        write(6,*) 'xm, ym: ',xm,ym,r
c       *** If this happens,
c       *** make sure to ignore the contribution of this tracer
        rl(1)=0
        rl(2)=0
        rl(3)=0
        iprint=1
        goto 100
      endif
  
      return 
      end

      subroutine tracdens01(dens,nodlin,rl)
      implicit none
      real*8 dens(*),rl(*)
      integer nodlin(*)
      integer k

      do k=1,3
         dens(nodlin(k)) = dens(nodlin(k)) + rl(k)
      enddo

      return
      end

      subroutine tracdens02(dens,npoint)
      implicit none
      real*8 dens(*)
      integer npoint,i

      do i=1,npoint
         dens(i)=0
      enddo
  
      return
      end

      subroutine tracdens03(dens,npoint)
      implicit none
      real*8 dens(*)
      integer npoint,i,ip
      do ip=1,npoint,100
         write(6,'(10f8.3,:)') (dens(i),i=ip,max(npoint,ip+100),10) 
      enddo
      return
      end

      real*8 function tracdens04(dens,npoint)
      implicit none
      real*8 dens(*)
      integer npoint,i

      tracdens04=0
      do i=1,npoint
         tracdens04 = tracdens04 + dens(i)
      enddo

      return
      end
