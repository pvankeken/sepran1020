      subroutine el4917 ( icheld, modelv, etha, etheff, cn, clamb, x, u,
     +                    dudx, secinv, gradv, ndcomp, elemvc, nelemvc,
     +                    vecloc, maxunk, numold, mdecl , mcont)
! ======================================================================
!
!        programmer    Guus Segal/Jaap van der Zanden
!        version  5.1  date 27-09-2000 Correction clear elemvc
!        version  5.0  date 16-05-1999 Extra parameter mdecl
!        version  4.1  date 14-03-1999 Use integration points
!        version  4.0  date 23-09-1998 Extra parameters
!        version  3.1  date 05-05-1997 Correction shear stress if u=0
!        version  3.0  date 05-02-1997 Extra parameter nelemvc
!        version  2.3  date 23-04-1994 Correction viscous dissipation
!        version  2.2  date 19-02-1994 Correction integration points
!        version  2.1  date 22-10-1993 Extension with viscous dissipation
!        version  2.0  date 15-07-1993 Complete redefinition, replaces el4026
!        version  1.1  date 22-08-1991 Zdenek: no dimension, cdc, ce
!        version  1.0  date 22-02-1985
!
!   copyright (c) 1993-2000  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     This subroutine computes the derivative quantities for the Navier-Stokes
!     equations in the cases icheld = 6, 8, 9, 10 and 11
!     Remark: if this subroutine is changed, also elvisc must be changed
! **********************************************************************
!
!                       KEYWORDS
!
!     derivative
!     navier_stokes_equation
!     viscosity
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/celp'
      include 'SPcommon/cmcdpr'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer modelv, ndcomp, icheld, nelemvc, numold, maxunk, mdecl
      integer mcont
      double precision etha(*), etheff(*), cn, clamb, x(mdecl,*),
     +                 u(mdecl,*), dudx(mdecl,ndimlc,*), secinv(*),
     +                 gradv(9,mdecl), elemvc(ndcomp,nelemvc),
     +                 vecloc(numold,maxunk,*)

!     clamb   i      Parameter lambda with respect to viscosity model
!     cn      i      Power in power law model
!     dudx    i      Real m x ndimlc x ncomp array in which the values of the
!                    derivatives of the solution vector in the integration
!                    points are stored in the sequence:
!                    du / dx  (x ) = dudx ( k, j, i )
!                      i    j   k
!     elemvc  o      element vector containing the derivatives for subroutine
!                    deriva
!     etha    i      Array containing the viscosity in the integration points
!     etheff  o      Array containing the computed effective viscosity in
!                    the integration points
!     gradv   o      Array containing the gradient vector in
!                    the integration points
!     icheld  i      Choice parameter to distinguish between the various
!                    possibilities of this subroutine
!                    Definitions:
!                     6:   compute the tensor t: t=etha(grad(u)+gradT(u))
!                          t11 = 2 etha gradu(1,1)
!                          t22 = 2 etha gradu(2,2)
!                          t33 = 2 etha gradu(3,3)
!                          t12 =   etha(gradu(1,2)+gradu(2,1))
!                          t23 =   etha(gradu(2,3)+gradu(3,2))
!                          t31 =   etha(gradu(3,1)+gradu(1,3))
!                     8:   compute the rate of elongation
!                          ndimlc = 2, jcart # 4:
!                          e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                               u(1)*u(2)*(gradu(1,2)+gradu(2,1)))/||u||*2
!                          ndimlc = 3 or jcart = 4:
!                          e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                               u(3)**2*gradu(3,3) + u(1)*u(2)*(gradu(1,2) +
!                               gradu(2,1)) + u(1)*u(3)*(gradu(1,3)+gradu(3,1))+
!                               u(2)*u(3)*(gradu(2,3)+gradu(3,2)))/||u||*2
!                     9:   compute the shear rate gamma
!                          ndimlc = 2, jcart # 4:
!                          g = (2u(1)u(2)(-gradu(1,1)+gradu(2,2)) +
!                               u(1)**2-u(2)**2(gradu(1,2)+gradu(2,1)))/||u||*2
!                          ndimlc = 3 or jcart = 4:
!                          g1 = gradu(1,2) + gradu(2,1)
!                          g2 = gradu(2,3) + gradu(3,2)
!                          g3 = gradu(3,1) + gradu(1,3)
!                    10:   compute the square root of the second invariant
!                    11:   compute the viscous dissipation: 1/2 t:A1
!                          A1 = grad v + grad T v
!                          So: viscous dissipation = eta II
!     maxunk  i      Maximum number of unknowns per point
!     mdecl   i      Formal declaration parameter for the arrays (=m)
!     modelv  i      Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     ndcomp  i      Indicates the number of quantities that must be filled
!                    in elemvc and elemwg per nodal point
!     nelemvc i      Length parameter for elemvc
!     numold  i      Number of vectors that are stored in ISLOLD
!     secinv  o      Array containing the second invariant in the integration
!     node
!     u       i      array of length m x ncomp in which the values of
!                    u in the integration points are stored the sequence:
!                    u  (x ) = u(j,i)  j = 1(1)m, i = 1 (1) ncomp
!                     i   j
!     vecloc  i      In this array the values of the function in the integration
!                    points is stored.
!                    vecloc(i,j,k) corresponds to the kth integration point,
!                    the ith vector and the jth unkwon corresponding to that
!                    vector
!     x        i     array containing the coordinates of the nodal points
!                    according to:
!                    x  = x (i,1);  y  = x (i,2);  z  = x (i,3)
!                     i              i              i
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer k, i
      double precision term, expn, epsbng, x3, u3, u1, u2,divv
      logical compsc

!     compsc  Indicates if the second invariant must be computed (true) or
!             not
!     epsbng  Lower threshold value for the value of a parameter
!     expn    Value of an exponent
!     i       Counting variable
!     k       Counting variable
!     term    Value of a term in an expression
!     u1      First component of velocity vector
!     u2      Second component of velocity vector
!     u3      Third component of velocity vector
!     x3      Third component of co-ordinate vector
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      double precision fnv000, fnv001, fnv002, fnv003

!     FNV000  User-defined viscosity model (modelv=100)
!     FNV001  User-defined viscosity model (modelv=101)
!     FNV002  User-defined viscosity model (modelv=102)
!     FNV003  User-defined viscosity model (modelv=103)
! **********************************************************************
!
!                       I/O
!
!     none
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!   trivial
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
!     --- Compute the gradient tensor and store in gradv
!         If necessary also the second inverse

      compsc = icheld.eq.10 .or. icheld.eq.11 .or.
     +         icheld.eq.6 .and. modelv.ne.100 .and. modelv.gt.1

      if ( jcart.eq.1 ) then

!     --- Cartesian co-ordinates

         if ( ndimlc.eq.2 ) then

!        --- ndimlc = 2

            do k = 1, m
               gradv(1,k) = dudx(k,1,1)
               gradv(2,k) = dudx(k,1,2)
               gradv(3,k) = dudx(k,2,1)
               gradv(4,k) = dudx(k,2,2)
            end do

            if ( compsc ) then

!           --- fill secinv

               do k = 1, m
                  secinv(k) = 2d0*(gradv(1,k)**2+gradv(4,k)**2) +
     +                        (gradv(2,k)+gradv(3,k))**2
               end do

            end if

         else

!        --- ndimlc = 3

            do k = 1, m
               gradv(1,k) = dudx(k,1,1)
               gradv(2,k) = dudx(k,1,2)
               gradv(3,k) = dudx(k,1,3)
               gradv(4,k) = dudx(k,2,1)
               gradv(5,k) = dudx(k,2,2)
               gradv(6,k) = dudx(k,2,3)
               gradv(7,k) = dudx(k,3,1)
               gradv(8,k) = dudx(k,3,2)
               gradv(9,k) = dudx(k,3,3)
            end do

            if ( compsc ) then

!           --- fill secinv

               do k = 1, m
                  secinv(k) = 2d0*(gradv(1,k)**2+gradv(5,k)**2+
     +                             gradv(9,k)**2) +
     +                        (gradv(2,k)+gradv(4,k))**2 +
     +                        (gradv(3,k)+gradv(7,k))**2 +
     +                        (gradv(6,k)+gradv(8,k))**2
               end do

            end if

         end if

      else if ( jcart.eq.2 ) then

!     --- Cylinder co-ordinates

         if ( ndimlc.eq.2 ) then

!        --- ndimlc = 2

            do k = 1, m
               gradv(1,k) = dudx(k,1,1)
               gradv(2,k) = dudx(k,1,2)
               gradv(3,k) = dudx(k,2,1)
               gradv(4,k) = dudx(k,2,2)
               if ( abs(x(k,1)).gt.epsmac ) then
                  gradv(5,k) = u(k,1)/x(k,1)
               else
                  gradv(5,k) = dudx(k,1,1)
               end if
            end do

            if ( compsc ) then

!           --- fill secinv

               do k = 1, m
                  secinv(k) = 2d0*(gradv(1,k)**2+gradv(4,k)**2+
     +                        gradv(5,k)**2) +(gradv(2,k)+gradv(3,k))**2
               end do

            end if

         else

!        --- ndimlc = 3, store in sequence r, z, phi
!                      It is assumed that the solution is stored in the
!                      sequence: ur, uz, uphi
!                      Remark: in the old version it is stored in the
!                              sequence r, phi, z
!                      the co-ordinate system should be in the same sequence

            do k = 1, m
               gradv(1,k) = dudx(k,1,1)
               gradv(2,k) = dudx(k,1,2)
               gradv(3,k) = dudx(k,1,3)
               gradv(4,k) = dudx(k,2,1)
               gradv(5,k) = dudx(k,2,2)
               gradv(6,k) = dudx(k,2,3)
               if ( abs(x(k,1)).gt.epsmac ) then
                  gradv(7,k) = (dudx(k,3,1)-u(k,3))/x(k,1)
                  gradv(8,k) = dudx(k,3,2)/x(k,1)
                  gradv(9,k) = (dudx(k,3,3)+u(k,1))/x(k,1)
               else
                  gradv(7,k) = -dudx(k,1,3)
                  gradv(8,k) = 0d0
                  gradv(9,k) = dudx(k,1,1)
               end if
            end do

            if ( compsc ) then

!           --- fill secinv

               do k = 1, m
                  secinv(k) = 2d0*(gradv(1,k)**2+gradv(5,k)**2+
     +                             gradv(9,k)**2) +
     +                        (gradv(2,k)+gradv(4,k))**2 +
     +                        (gradv(3,k)+gradv(7,k))**2 +
     +                        (gradv(6,k)+gradv(8,k))**2
               end do

            end if

         end if

      else if ( jcart.eq.3 ) then

!     --- Polar co-ordinates

         do k = 1, m
            gradv(1,k) = dudx(k,1,1)
            gradv(2,k) = dudx(k,1,2)
            if ( abs(x(k,1)).gt.epsmac ) then
               gradv(3,k) = (dudx(k,2,1)-u(k,2))/x(k,1)
               gradv(4,k) = (dudx(k,2,2)+u(k,1))/x(k,1)
            else
               gradv(3,k) = -dudx(k,1,2)
               gradv(4,k) = dudx(k,1,1)
            end if
         end do

         if ( compsc ) then

!        --- fill secinv

            do k = 1, m
               secinv(k) = 2d0*(gradv(1,k)**2+gradv(4,k)**2) +
     +                     (gradv(2,k)+gradv(3,k))**2
            end do

         end if

      else if ( jcart.eq.4 ) then

!     --- Cylindrical co-ordinates with swirl (2D),
!         store in sequence r, phi, z
!         It is assumed that the solution is stored in the sequence:
!         ur, uz, uphi

         do k = 1, m
            gradv(1,k) = dudx(k,1,1)
            gradv(2,k) = dudx(k,1,2)
            gradv(3,k) = dudx(k,1,3)
            gradv(4,k) = dudx(k,2,1)
            gradv(5,k) = dudx(k,2,2)
            gradv(6,k) = dudx(k,2,3)
            gradv(8,k) = 0d0
            if ( abs(x(k,1)).gt.epsmac ) then
               gradv(7,k) = -u(k,3)/x(k,1)
               gradv(9,k) = u(k,1)/x(k,1)
            else
               gradv(7,k) = -dudx(k,1,3)
               gradv(9,k) = dudx(k,1,1)
            end if
         end do

         if ( compsc ) then

!        --- fill secinv

            do k = 1, m
               secinv(k) = 2d0*(gradv(1,k)**2+gradv(5,k)**2+
     +                          gradv(9,k)**2) +
     +                     (gradv(2,k)+gradv(4,k))**2 +
     +                     (gradv(3,k)+gradv(7,k))**2 +
     +                      gradv(6,k)**2
            end do

         end if

      end if

      if ( icheld.eq.6 .or. icheld.eq.11 ) then

!     --- icheld = 6, 11 fill etheff and compute stress tensor or viscous
!                        dissipation

         do k = 1, nelemvc
            do i = 1, ndcomp
               elemvc(i,k) = 0d0
            end do
         end do

         if ( modelv.eq.1 ) then

!        --- modelv = 1, newtonian fluid
!            copy etha into etheff

            do k = 1, m
               etheff(k) = etha(k)
            end do

         else

!        --- modelv > 1, non-newtonian fluid
!            Fill array gradv

            if ( modelv.ge.100 ) then

!           --- modelv >= 100, user functions

               go to ( 400, 450, 500, 530 ), modelv-99

!              --- modelv = 100, user function fnv000

400               do k = 1, m
                     etheff(k) = fnv000 ( gradv(1,k) )
                  end do
                  go to 800

!                 --- modelv = 101, user function fnv001

450               do k = 1, m
                     etheff(k) = fnv001 ( secinv(k) )
                  end do
                  go to 800

!                 --- modelv = 102, user function fnv002

500               x3 = 0d0
                  u3 = 0d0
                  do k = 1, m
                     if ( ndimlc.eq.3 ) then

!                    --- ndimlc = 3, fill third component of x and u

                        x3 = x(k,3)
                        u3 = u(k,3)

                     else if ( jcart.eq.4 ) then

!                    --- jcart = 4, cylindrical co-ordinates with swirl

                        u3 = u(k,3)

                     end if
                     etheff(k) = fnv002 ( x(k,1), x(k,2), x3,
     +                                    u(k,1), u(k,2), u3, secinv(k))
                  end do
               go to 800

530            x3 = 0d0
               u3 = 0d0
               do k = 1, m
                  if ( ndimlc.eq.3 ) then

!                 --- ndimlc = 3, fill third component of x and u

                     x3 = x(k,3)
                     u2 = u(k,2)
                     u3 = u(k,3)

                  else if ( jcart.eq.4 ) then

!                 --- jcart = 4, cylindrical co-ordinates with swirl

                     u2 = u(k,3)
                     u3 = u(k,2)

                  else

                     u2 = u(k,2)

                  end if
                  etheff(k) = fnv003 ( x(k,1), x(k,2), x3,
     +                                 u(k,1), u(k,2), u3, secinv(k),
     +                                 numold, maxunk, vecloc(1,1,k) )
               end do
               go to 800

            else

!           --- modelv < 100, power law models

               go to ( 600, 650, 700 ), modelv-1

!              --- modelv = 2, power law model

600               expn = 0.5d0 * ( cn-1d0 )
                  if ( expn.le.0 ) then

!                 --- expn <= 0, set secinv to max(epsmac, secinv)

                     do k = 1, m
                        secinv(k) = max ( secinv(k), epsmac )
                     end do

                  end if

                  do k = 1, m
                     etheff(k) = etha(k) * secinv(k) ** expn
                  end do

                  go to 800

!                 --- modelv = 3, three parameter carreau liquid

650               expn = 0.5d0 * ( cn-1d0 )
                  do k = 1, m
                     etheff(k) = etha(k) * (1d0+clamb*secinv(k)) ** expn
                  end do
                  go to 800

!                 --- modelv = 3, three parameter plastico-viscous liquid

700               expn = 0.5d0/cn
                  epsbng = 100d0 * epsmac
                  do k = 1, m
                     if ( secinv(k).lt.epsbng ) then

!                    --- secinv nearly zero

                        if ( secinv(k).gt.2d0*epsmac ) then
                           secinv(k) = ( secinv(k)-epsmac ) ** 2 /
     +                                 ( 2d0 * (epsbng-epsmac) )
                        else
                           secinv(k) = epsmac
                        end if

                     end if
                     term = ( clamb**2/(etha(k)**2*secinv(k)) ) ** expn
                     etheff(k) = etha(k) * ( 1d0 + term ) ** cn
                  end do
                  go to 800

            end if

         end if

800      if ( icheld.eq.6 ) then

!        --- icheld = 6, Fill array elemvc with the stress tensor

            if ( ndimlc.eq.2 .and. (jcart.eq.1 .or. jcart.eq.3) ) then

!           --- ndimlc = 2, Cartesian or Polar co-ordinates

c              *** compressible version
               if (mcont.eq.1) then
                do k = 1, nelemvc
                  divv = gradv(1,k)+gradv(4,k)
                  elemvc(1,k) = 2d0*etheff(k)*gradv(1,k)-2d0/3d0*divv
                  elemvc(2,k) = 2d0*etheff(k)*gradv(4,k)-2d0/3d0*divv
                  elemvc(4,k) = etheff(k)*(gradv(2,k)+gradv(3,k))
                end do
               else
                do k = 1, nelemvc
                  elemvc(1,k) = 2d0*etheff(k)*gradv(1,k)
                  elemvc(2,k) = 2d0*etheff(k)*gradv(4,k)
                  elemvc(4,k) = etheff(k)*(gradv(2,k)+gradv(3,k))
                end do
               endif
            else if ( ndimlc.eq.2 .and. jcart.eq.2 ) then

!           --- ndimlc = 2, Cylinder co-ordinates

               do k = 1, nelemvc
                  elemvc(1,k) = 2d0*etheff(k)*gradv(1,k)
                  elemvc(2,k) = 2d0*etheff(k)*gradv(4,k)
                  elemvc(3,k) = 2d0*etheff(k)*gradv(5,k)
                  elemvc(4,k) = etheff(k)*(gradv(2,k)+gradv(3,k))
               end do
            else

!           --- ndimlc = 3

               if ( itype.eq.410 ) then

!              --- itype = 410, the sequence of the stress tensor is defined
!                  different from usual

                  do k = 1, nelemvc
                     elemvc(1,k) = 2d0*etheff(k)*gradv(1,k)
                     elemvc(4,k) = 2d0*etheff(k)*gradv(5,k)
                     elemvc(6,k) = 2d0*etheff(k)*gradv(9,k)
                     elemvc(2,k) = etheff(k)*(gradv(2,k)+gradv(4,k))
                     elemvc(3,k) = etheff(k)*(gradv(3,k)+gradv(7,k))
                     elemvc(5,k) = etheff(k)*(gradv(6,k)+gradv(8,k))
                  end do

               else

!              --- standard situation

                  do k = 1, nelemvc
                     elemvc(1,k) = 2d0*etheff(k)*gradv(1,k)
                     elemvc(2,k) = 2d0*etheff(k)*gradv(5,k)
                     elemvc(3,k) = 2d0*etheff(k)*gradv(9,k)
                     elemvc(4,k) = etheff(k)*(gradv(2,k)+gradv(4,k))
                     elemvc(5,k) = etheff(k)*(gradv(6,k)+gradv(8,k))
                     elemvc(6,k) = etheff(k)*(gradv(3,k)+gradv(7,k))
                  end do

               end if

            end if

         else

!        --- icheld = 11, compute viscous dissipation

            do k = 1, nelemvc
               elemvc(1,k) = etheff(k) * secinv(k)
            end do

         end if

      else if ( icheld.eq.8 ) then

!     --- icheld = 8, compute the elongation

         if ( ndimlc.eq.2 .and. jcart.ne.4 ) then

!        --- ndimlc = 2, except cylindrical co-ordinates with swirl

            do k = 1, nelemvc
               if ( abs(u(k,1)).le.sqreps .and. abs(u(k,2)).le.sqreps )
     +            then
                  elemvc(1,k) = 0d0
               else
                  elemvc(1,k) = (u(k,1)**2*gradv(1,k) +
     +                           u(k,1)*u(k,2)*(gradv(2,k)+gradv(3,k)) +
     +                           u(k,2)**2*gradv(4,k) ) /
     +                          (u(k,1)**2+u(k,2)**2)
               end if
            end do

         else

!        --- ndimlc = 3 or cylindrical co-ordinates with swirl

            do k = 1, nelemvc
               if ( abs(u(k,1)).le.sqreps .and. abs(u(k,2)).le.sqreps
     +              .and. abs(u(k,3)).le.sqreps ) then
                  elemvc(1,k) = 0d0
               else
                  elemvc(1,k) = (u(k,1)**2*gradv(1,k) +
     +                           u(k,2)**2*gradv(5,k) +
     +                           u(k,3)**2*gradv(9,k) +
     +                           u(k,1)*u(k,2)*(gradv(2,k)+gradv(4,k)) +
     +                           u(k,1)*u(k,3)*(gradv(3,k)+gradv(7,k)) +
     +                           u(k,2)*u(k,3)*(gradv(6,k)+gradv(8,k)))/
     +                          (u(k,1)**2+u(k,2)**2+u(k,3)**2)
               end if
            end do

         end if

      else if ( icheld.eq.9 ) then

!     --- icheld = 9, compute the deformation

         if ( ndimlc.eq.2 .and. jcart.ne.4 ) then

!        --- ndimlc = 2, except cylindrical co-ordinates with swirl

            do k = 1, nelemvc
               u1 = u(k,1)
               u2 = u(k,2)
               if ( abs(u1).le.sqreps .and. abs(u2).le.sqreps )
     +            then
                  elemvc(1,k) = 0d0
               else
                  elemvc(1,k) = (2d0*u1*u2*(-gradv(1,k)+
     +                           gradv(4,k)) + (u1**2-u2**2)*
     +                           (gradv(2,k)+gradv(3,k)) ) /
     +                          (u1**2+u2**2)
               end if
            end do

         else

!        --- ndimlc = 3 or cylindrical co-ordinates with swirl

            if ( itype.eq.410 ) then

!           --- itype = 410, the sequence of the shear rate is defined
!               different from usual

               do k = 1, nelemvc
                  elemvc(1,k) = gradv(2,k)+gradv(4,k)
                  elemvc(3,k) = gradv(6,k)+gradv(8,k)
                  elemvc(2,k) = gradv(3,k)+gradv(7,k)
               end do

            else

!           --- standard situation

               do k = 1, nelemvc
                  elemvc(1,k) = gradv(2,k)+gradv(4,k)
                  elemvc(2,k) = gradv(6,k)+gradv(8,k)
                  elemvc(3,k) = gradv(3,k)+gradv(7,k)
               end do

            end if

         end if

      else

!     --- icheld = 10, compute the sqrt(secinv)

         do k = 1, nelemvc
            elemvc(1,k) = sqrt(secinv(k))
         end do

      end if
      end
