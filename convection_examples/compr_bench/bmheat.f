c *************************************************************
c *   BMHEAT
c *   
c *   PvK 120490
c *************************************************************
      subroutine bmheat(kmesh,kprob,intmat,iuser,user,isol,islold,matr)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),iuser(*),islold(*),isol(*)
      integer matr(*)
      real*8 user(*)
      integer irhsd(5),ielhlp
      include 'pecpu.inc'
      real*4 second

      t2 = second()
      call copyvc(isol,islold)
      call systm0(1,matr,intmat,kmesh,kprob,irhsd,isol,
     v            iuser,user,islold,ielhlp)
      call solve(1,matr,isol,irhsd,intmat,kprob)
      t3 = second()
      cpu3 = t3-t2

      return
      end

