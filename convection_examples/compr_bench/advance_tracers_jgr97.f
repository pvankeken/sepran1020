c *************************************************************
c *   ADVANCE_TRACERS_JGR97
c *
c *   Compute tracer advection in fixed velocity field
c *   to simulate benchmark from Van Keken et al., JGR, 1997
c *
c *   Do for NOUT steps
c *      tout = tout+dtout
c *      While (t<tout)
c *          compute CFL time step 
c *          Integrate over new time step
c *          output intermediate information
c *      end
c *      Output plots, write solution to file etc.
c *   Done
c *
c *   PvK 220104
c *************************************************************
      subroutine advance_tracers_JGR97(iuser,user)
      implicit none
      integer iuser(*)
      real*8 user(*)

      integer ibuffr(1)
      common ibuffr
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
 
c     **** COMMON DECLARATIONS
      include 'SPcommon/cbuffr'
      include 'SPcommon/cmacht'
      include 'SPcommon/ctimen'
c     *** /mysepar/ most sepran arrays (kmesh/kprob etc).
      include 'mysepar.inc'
c     *** /petrac/ tracer logic
      include 'petrac.inc'
c     *** /tracer/ more tracer logic
      include 'tracer.inc'
c     *** /peiter/ logic for integration
      include 'peiter.inc'
c     *** /petime/ time integration info
      include 'petime.inc'
c     *** /dimensional/ info about scaling to dimensional parameters
      include 'dimensional.inc'
c     *** /ccc/ /bound/ boundary and curve info
      include 'ccc.inc'
      include 'bound.inc'

c     *** LOCAL VARIABLES
      real second,t00,t01,t11,t10,cpu
      real*8 tnew,contln(10),format,yfaccn
      integer niter,inout,iall,irhs2(5),matrm(5),jsmoot,numarr
      integer inbetween,ibp,imoved,npoint,ielhlp,ipoint
      real*8 rmax,subdif,anorm,rmax2,p,q,dx,dy,func,x0,y0
      real*8 xlmin,xlmax,ylmin,ylmax
      logical output
      character*80 fimage,command

      save irhs2,matrm

      niter = 0
      t=0
      cpu=0
      inout = 1
      inbetween = 0
      imoved = 0
      npoint = kmesh1(8)

      t00 = second()
      tout = t + dtout
      output = .false.

      open(9,file='JGR97_path.dat')

c     *** calculate time step; pefilcof stores velocity in user
c     *** which then is used in pedtcf.
      call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v              isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user)
      call pedtcf(kmesh2,user,dtcfl)
      t00 = second()
      write(6,'(''   Initial condition, dtcfl: '',3f12.7)') coormark(1), 
     v        coormark(2),dtcfl
      x0 = coormark(1)
      y0 = coormark(2)
      write(9,*) x0,y0
      xlmax = x0
      xlmin = x0
      ylmax = y0
      ylmin = y0
100   continue
         niter = niter+1
         
c        *** based time step on CFL criterion
         tstep = dtcfl * tfac
c        *** make sure new time doesn't exceed output limits or maximum time
         tnew = t+tstep
         if (tnew.ge.tmax) then
            tstep = tmax-t
            t = tmax
            output=.true.
         else if (tnew.ge.tout) then
            tstep = tout-t
            t = tout
            output = .true.
         else
            t = tnew
            output = .false.
         endif

c        *** predict position of markers
         if (itracoption.ne.0) then
c           *** interpolate velocity in old positions of markers
            call tracvel(kmesh1,kprob1,isol1,user,1)
c           *** Advance tracers using explicit Euler
            call predcoort(coormark,velmark,coornewm)
c        write(6,'(''P: '',7f12.7)') t,coormark(1),coormark(2),
c    v     velmark(1),velmark(2),
c    v     func(4,coormark(1),coormark(2),0d0),
c    v     func(5,coormark(1),coormark(2),0d0)
         endif

         if (ncor.eq.0) then
            call copcoor(1)
         else if (ncor.eq.1) then
c           *** correct position of markers using 4th order RK
            call mark4(kmesh1,kprob1,isol1,isol1,user)
            call tracvel(kmesh1,kprob1,isol1,user,2)
            call copcoor(1)
         endif
c        write(6,'(''C: '',5f12.7)') t,coormark(1),coormark(2),
c    v     velmark(1),velmark(2)
         xlmax = max ( xlmax , coormark(1) )
         xlmin = min ( xlmin , coormark(1) )
         ylmax = max ( ylmax , coormark(1) )
         ylmin = min ( ylmin , coormark(1) )
 
         write(9,*) coormark(1),coormark(2)

      if (t.lt.tmax.and.niter.lt.nitermax) goto 100
      t01 = second()
      write(6,'(''    Final time: '',f12.7)') t
      write(6,'(''    Final x,y : '',2f12.7)') 
     v           coormark(1),coormark(2)
      dx = (coormark(1)-x0)
      dy = (coormark(2)-y0)
      write(6,'(''    Error     : '',2f12.7)') dx,dy
      write(6,'(''    CPU       : '',f12.2)') t01-t00
      write(6,'(''    Tracer path stored in JGR97_path.dat'')')
      close(9)
      write(6,*) 'Path x min/max: ',xlmin,xlmax
      write(6,*) 'Path y min/max: ',ylmin,ylmax

      return
      end

