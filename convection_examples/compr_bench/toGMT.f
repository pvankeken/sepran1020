c *************************************************************
c *   TOGMT
c *   Interpolate solution to regular grid for output with GMT
c *   PvK 012809
c *************************************************************
      subroutine toGMT(jchoice,kmesh,kprob,rlam,nres,isol,map,fname)
      implicit none
      character*(*) fname
      integer jchoice,kmesh(*),kprob(*),isol(*),map(*),nres
      real*8 rlam
      integer NPMAX
      parameter(NPMAX=100*100)
      character*80 fname2
      real*8 solint(NPMAX),coor(2,NPMAX),dx,dy
      real*8 x,y,xmax,ymax,average
      integer iinmap(10),ncoor,i,j,ip,ichoice,Jarvis_offset
      logical do_averages

      do_averages = jchoice.ge.10
      ichoice = jchoice - (jchoice/10)*10
c     **** With Jarvis flux b.c. don't plot bottom line
      Jarvis_offset=0

c     *** figure out x/ymax and size of interpolation grid
      xmax = rlam
      ymax = 1d0
      ncoor = rlam*(nres+1-Jarvis_offset)*(nres+1)
      if (ncoor.gt.NPMAX) then
         write(6,*) 'PERROR(toGMT): ncoor > NPMAX'
         write(6,*) 'ncoor, NPMAX: ',ncoor,NPMAX
         call instop
      endif

c     *** set up coordinates of interpolation grid
      dx = 1d0/nres
      dy = 1d0/nres
      ip=0
      do j=1+Jarvis_offset,nres+1
         y=(j-1)*dy
         do i=1,rlam*(nres+1)
            x = (i-1)*dx
            ip=ip+1
            coor(1,ip)=x
            coor(2,ip)=y
         enddo
      enddo

      iinmap(1)=2
      if (ichoice.eq.0) then
         iinmap(2)=1
      else
         iinmap(2)=2
      endif
      call intcoor(kmesh,kprob,isol,solint,coor,1,ncoor,2,iinmap,map)

      open(11,file=fname)
      do i=1,ncoor
         write(11,*) solint(i)
      enddo
      close(11)

      if (do_averages) then
         fname2 = 'AVG_'//fname
         open(11,file=fname2)
         ip=0
         do j=1+Jarvis_offset,nres+1
            average=0d0
            do i=1,nres+1
               ip=ip+1
               average = average + solint(ip)
            enddo
            write(11,*) average/(nres+1),(j-1)*1d0/(nres+1)
         enddo
 
         close(11)
      endif
         
 

      return
      end
