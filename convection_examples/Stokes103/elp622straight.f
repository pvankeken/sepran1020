      subroutine elp622straight ( iweigo, x, index1, intrule,
     +                            xgauss, w, pi2, dphidx, dpsidx,
     +                            comppsi, delta )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 27-02-2009
!
!   copyright (c) 2009-2009  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Compute basis functions and related quantities for quadratic
!     triangle (straight element)
! **********************************************************************
!
!                       KEYWORDS
!
!     basis_function
!     element
!     quadratic
!     triangle
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/celp'
      include 'SPcommon/cgaus'
      include 'SPcommon/cinforel'
      include 'SPcommon/cdebug_routs'
      include 'SPcommon/consta'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iweigo, index1(6), intrule
      double precision x(6,2), xgauss(m,2), w(m), pi2, dphidx(6,m,ndim),
     +                 dpsidx(3,m,2), delta
      logical comppsi

!     comppsi        i    if true the linear basis functions must be evaluated
!     delta          o    jacobian of element
!     dphidx         o    Array of length n x m x ndim  containing the
!                         derivatives
!                         of the basis functions in the sequence:
!                         d phi / dx (xg ) = dphidx (i,k,1);
!                              i        k
!                         d phi / dy (xg ) = dphidx (i,k,2);
!                              i        k
!     dpsidx         o    Array of length n x m x l  containing the derivatives
!                         of the linear basis functions in the sequence:
!                         d psi / dx (xg ) = psix (i,k,1);
!                              i        k
!                         d psi / dy (xg ) = psix (i,k,2);
!                              i        k
!     index1         i    Array of length inpelm containing the point numbers
!                         of the nodal points in the element
!     intrule        i    Actual integration rule
!     iweigo         i    if 1 the weights must be filled
!     pi2            i    Help variable to store 2 pi for each call
!     w              o    array of length m containing the weights for
!                         integration
!     x              o    array of length 9 x ndim containing the x- and
!                         y-coordinates of the nodal points with respect to the
!                         local numbering in the sequence:
!                         x_i = x (i,1);  y_i = x (i,2);
!     xgauss         o    array of length m x ndim containing the co-ordinates
!                         of the gauss points.  array xgauss is only filled when
!                         gauss integration is applied.
!                         xg_i = xgauss (i,1);  yg_i = xgauss (i,2);
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer i, k, j
      double precision e(3,2), sum, h, rc
      logical debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     e              work array of length 3 x 2, to store the factors
!                     ij
!                    e.        see method
!     h              Help variable to store a constant temporarily
!     i              Counting variable
!     j              Counting variable
!     k              Counting variable
!     rc             Help variable to store part of a weight for
!                    axi-symmetric co-ordinates
!     sum            Help parameter to compute a sum
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     PRININ         print 1d integer array
!     PRINRL2        Print a real two-dimensional array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     105   Jacobian of element is zero
!    1540   Jacobian zero or changing sign
! **********************************************************************
!
!                       PSEUDO CODE
!
!        to compute the derivatives of the basis functions, the jacobian
!        of the element and the weights, the following definitions are used:
!         ij        i+1    i+2    i+1
!        e   =  (-1)    ( x    - x    )   cyclic: i=1,2,3 ;   j=1,2
!                          j      j
!                    32  11    12 31
!        delta   =  e   e   - e  e
!                             ij
!        d lambda  / dx   =  e   /  delta
!                i     j
!        phi  =  lambda  (2 lambda - 1)        i=1,2,3   (basis functions:1,2,3)
!           i          i          i
!        phi  = 4  lambda   lambda             1<=i<j<=3 (basis functions:4,5,6)
!           ij           i        j
!        grad phi  = grad lambda  ( 4 lambda  - 1 )
!                i              i           i
!        grad phi  = 4 ( grad lambda  lambda  + grad lambda  lambda  )
!                ij                 i       j              j       i
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elp622straight' )
      debug = .false. .and. ioutp>=0
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elp622straight'

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      if ( iweigo==1 .or. ideriv==1 ) then

!     --- ideriv = 1  compute derivatives or iweigo = 1: weights
!                                   ij
!         compute the quantities  e   and delta
!                                    ij
!         store d lambda  / d x  in e
!                       i      j

         e(1,1) = x(3,2) - x(5,2)
         e(2,1) = x(5,2) - x(1,2)
         e(3,1) = x(1,2) - x(3,2)
         e(1,2) = x(5,1) - x(3,1)
         e(2,2) = x(1,1) - x(5,1)
         e(3,2) = x(3,1) - x(1,1)

         delta = e(3,1)*e(1,2) - e(1,1)*e(3,2)
         if ( checksign .and. delta<0d0 ) then
            write(irefwr,*) 'area of element ', ielem, ' is negative'
         end if

!        --- check if delta is non-zero

         if ( abs(delta)<=0d0 ) then
            if ( isubr==0 ) then
               call errint ( ielem, 1 )
               call errint ( itype, 2 )
               call errint ( ielgrp, 3 )
               call errsub ( 105, 3, 0, 0 )
            else
               call errint ( ielgrp, 1 )
               call errint ( ielem, 2 )
               call errchr ( 'surface', 1 )
               call errsub ( 1540, 2, 0, 1 )
            end if
            call prinin(index1,6,'node numbers')
            call prinrl2(x,6,2,'co-ordinates')
            go to 1000
         end if

         e = e / delta

      end if
      if ( comppsi .and. ideriv==1 ) then

!     --- ideriv = 1   fill derivatives

         if ( comppsi ) then

!        --- Compute linear basis functions

            do j = 1, m
               do k = 1,2
                  dpsidx(1,j,k) = e(1,k)
                  dpsidx(3,j,k) = e(3,k)
                  dpsidx(2,j,k) = -e(1,k) - e(3,k)
               end do
            end do

         end if

      end if
      if ( igausp==1 .or. intrule>=2 .and. jcart==2 ) then

!     --- gauss points must be filled

         select case (intrule)

         case(1)

!        --- intrule = 1  newton cotes rule

            xgauss = x

         case(2)

!        --- intrule = 2  four-point gauss rule

            do j = 1, ndim
               do k = 1, 4
                  sum = 0d0
                  do i = 1,3
                     sum = sum + alam2(i,k) * x(2*i-1,j)
                  end do
                  xgauss(k,j) = sum
               end do
            end do

         case(3)

!        --- intrule = 3  seven-point gauss rule

            do j = 1, 2
               do k = 1, 7
                  sum = 0d0
                  do i = 1, 3
                     sum = sum + alam3(i,k) * x(2*i-1,j)
                  end do
                  xgauss(k,j) = sum
               end do
            end do

         end select  ! case (intrule)

      end if
      if ( iweigo==1 ) then

!     --- iweigo = 1  fill weights

         select case (intrule)

         case(1)

!        --- intrule = 1  newton-cotes rule

            if ( jcart==1 ) then

!           --- jcart = 1  cartesian co-ordinates

               h = abs(delta)/6d0
               do i = 2, 6, 2
                  w(i)=h
               end do
               do i = 1, 5, 2
                  w(i) = 0d0
               end do

            else if ( jcart<=3 ) then

!           --- jcart = 2, 3  axi-symmetric or polar co-ordinates

               if ( jcart==2 ) then
                  h = delta/60d0*pi
               else
                  h = delta/120d0
               end if
               rc = x(1,1) + x(3,1) + x(5,1)
               do i = 1, 5, 2
                  w(i) = h* ( 3d0*x(i,1)-rc )
               end do
               rc = 2d0*rc
               h  = 4d0*h
               w(2) = h * (rc-x(5,1))
               w(4) = h * (rc-x(1,1))
               w(6) = h * (rc-x(3,1))
            end if

         case(2)

!        --- intrule = 2  four points gauss rule

            w = wgs2 * abs(delta)

            if ( jcart==2 ) then

!           --- jcart = 2  axi-symmetric co-ordinates

               w = w * xgauss(1:4,1) * pi2

            else if ( jcart==3 ) then

!           --- jcart = 3  polar co-ordinates

               w = w * xgauss(1:4,1)

            end if

         case(3)

!        --- intrule = 3  seven points gauss rule

            w = wgs3 * abs(delta)

            if ( jcart==2 ) then

!           --- jcart = 2  axi-symmetric co-ordinates

               w = w * xgauss(1:7,1) * pi2

            else if ( jcart==3 ) then

!           --- jcart = 3  polar co-ordinates

               w = w * xgauss(1:7,1)

            end if

         end select  ! case (intrule)

      end if
      if ( ideriv==1 ) then

!     --- ideriv = 1   fill derivatives

         select case (intrule)

         case(1)

!        --- intrule  =  1  newton-cotes rule

            do k = 1, 2
               dphidx(1,2,k) =  e(1,k)
               dphidx(1,4,k) = -e(1,k)
               dphidx(1,6,k) =  e(1,k)
               dphidx(3,2,k) =  e(2,k)
               dphidx(3,4,k) =  e(2,k)
               dphidx(3,6,k) = -e(2,k)
               dphidx(5,2,k) = -e(3,k)
               dphidx(5,4,k) =  e(3,k)
               dphidx(5,6,k) =  e(3,k)
               dphidx(2,2,k) = -2d0*e(3,k)
               dphidx(2,4,k) =  2d0*e(1,k)
               dphidx(2,6,k) =  2d0*e(2,k)
               dphidx(4,2,k) =  2d0*e(3,k)
               dphidx(4,4,k) = -2d0*e(1,k)
               dphidx(4,6,k) =  2d0*e(2,k)
               dphidx(6,2,k) =  2d0*e(3,k)
               dphidx(6,4,k) =  2d0*e(1,k)
               dphidx(6,6,k) = -2d0*e(2,k)
            end do

            if ( m>3 ) then

!           --- m = 6:  fill derivatives in vertices

               do k = 1, 2
                  dphidx(1,1,k) = 3d0*e(1,k)
                  dphidx(1,3,k) =    -e(1,k)
                  dphidx(1,5,k) =    -e(1,k)
                  dphidx(3,1,k) =    -e(2,k)
                  dphidx(3,3,k) = 3d0*e(2,k)
                  dphidx(3,5,k) =    -e(2,k)
                  dphidx(5,1,k) =    -e(3,k)
                  dphidx(5,3,k) =    -e(3,k)
                  dphidx(5,5,k) = 3d0*e(3,k)
                  dphidx(2,1,k) = 4d0*e(2,k)
                  dphidx(2,3,k) = 4d0*e(1,k)
                  dphidx(2,5,k) = 0d0
                  dphidx(4,1,k) = 0d0
                  dphidx(4,3,k) = 4d0*e(3,k)
                  dphidx(4,5,k) = 4d0*e(2,k)
                  dphidx(6,1,k) = 4d0*e(3,k)
                  dphidx(6,3,k) = 0d0
                  dphidx(6,5,k) = 4d0*e(1,k)
               end do

            end if

         case(2)

!        --- intrule  =  2  gauss 4-point rule

            do j = 1, 2
               do i = 1, 3
                  do k = 1, 4
                     dphidx(2*i-1,k,j) = e(i,j)*(4d0*alam2(i,k)-1d0)
                  end do
               end do
               do k = 1, 4
                  dphidx(2,k,j) = 4d0*(alam2(1,k)*e(2,j)+
     +                            alam2(2,k)*e(1,j))
                  dphidx(4,k,j) = 4d0*(alam2(2,k)*e(3,j)+
     +                            alam2(3,k)*e(2,j))
                  dphidx(6,k,j) = 4d0*(alam2(3,k)*e(1,j)+
     +                            alam2(1,k)*e(3,j))
               end do
            end do

         case(3)

!        --- intrule  =  3  gauss 7-point rule

            do j = 1, 2
               do i = 1, 3
                  do k = 1, 7
                     dphidx(2*i-1,k,j) = e(i,j)*(4d0*alam3(i,k)-1d0)
                  end do
               end do
               do k = 1, 7
                  dphidx(2,k,j) = 4d0*(alam3(1,k)*e(2,j)+
     +                            alam3(2,k)*e(1,j))
                  dphidx(4,k,j) = 4d0*(alam3(2,k)*e(3,j)+
     +                            alam3(3,k)*e(2,j))
                  dphidx(6,k,j) = 4d0*(alam3(3,k)*e(1,j)+
     +                            alam3(1,k)*e(3,j))
               end do
            end do

         end select  ! case (intrule)

      end if

1000  call erclos ( 'elp622straight' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End elp622straight'

      end if  ! ( debug )

      end
