program stokes
integer,parameter :: NBUFDEF=50000000
integer ibuffr(NBUFDEF)
call sepcom(NBUFDEF)

end program stokes

real(kind=8) function funccf(ichoice,x,y,z)
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8), parameter :: pi=3.1415926538

funccf=1d3*(1-y+0.1*cos(pi*x)*sin(pi*y))

end function funccf
