      subroutine elm903parms ( iuser, ipcoef, ipcoefnodes,
     +                         ipphi, ippsi, ipphixi, ippsixi, larray )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 20-02-2010
!
!   copyright (c) 2010-2010  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Compute parameters irule, m and n for type elements of type 900
!
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
!     initialization
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/celint'
      include 'SPcommon/comcons3'
      include 'SPcommon/cnamtb'
      include 'SPcommon/cactl'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iuser(*), ipcoef, ipcoefnodes,
     +        ipphi, ippsi, ipphixi, ippsixi, larray

!     ipcoef         o    Starting address of array coeffintpnts
!     ipcoefnodes    o    Starting address of array coeffnodes
!     ipphi          o    Starting address of array phi
!     ipphixi        o    Starting address of array phixi
!     ippsi          o    Starting address of psi
!     ippsixi        o    Starting address of array psixi
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     larray         i    length of array array in element subroutines
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer interpol, ioldm, ishift, jcheld, jelp, jtime, kchois,
     +        nveloc, mcont, modelrhsd, modelv, mrule, ncoef, ipend
      double precision thetdt
      logical debug, compress

!     compress       If true compressible has been read
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     interpol       If 0 the standard interpolation is applied,
!                    if 1 the interpolation is restricted to linear
!                    subelements
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     ipend          Last position in array
!     ishift         Shift of specific variable with respect to diagonal
!     jcheld         if ( icheld<= 10 ) then icheld else icheld-20
!     jelp           Indication of the type of basis function subroutine
!                    must be called by ELxxxx.
!     jtime          Integer parameter indicating if the mass matrix for
!                    the time-dependent equation must be computed (>0)
!                    or not ( = 0)
!     kchois         Indication what type of parameters must be computed
!     mcont          Defines the type of continuity equation
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!     modelv         Defines the type of viscosity model
!     mrule          Type of integration rule as given by the user
!     ncoef          Number of coefficients in the equation
!     nveloc         Number of velocity parameters
!     thetdt         1/(theta dt) for the case of the theta method
!                    immediately applied
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ELM903PARMS1   Compute parameters irule, m and n for type elements of
!                    type 903
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     550   array length too short
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm903parms' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elm903parms'

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      write(6,*) 'elm903parm: ',ielem

!     --- First part, compute parameters

      call elm903parms1 ( iuser, interpol, ioldm, ishift, jcheld,
     +                    jelp, jtime, kchois, nveloc, mcont,
     +                    modelrhsd, modelv, mrule, ncoef,
     +                    thetdt, compress )

!     --- Set starting addresses

      ipcoef = 1
      ipcoefnodes = ipcoef+32*m
      ipphi = ipcoefnodes+32*n
      ippsi = ipphi+n*m
      ipphixi = ippsi+m*nvert
      ippsixi = ipphixi+n*m*ndimlc
      ipend = ippsixi+n*nvert*ndimlc
      if ( ipend>larray ) then

!     --- array wrk1 too small

         call errint ( ipend, 1 )
         call errint ( larray, 2 )
         call errchr ( 'wrk1', 1 )
         call errsub ( 550, 2, 0, 1 )
         go to 1000

      end if

1000  call erclos ( 'elm903parms' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End elm903parms'

      end if  ! ( debug )

      end
