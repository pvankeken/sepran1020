      subroutine elm800basefn ( coor, x, w, dphidx, xgauss, phi, index1,
     +                          qmat, tang, jelp, phiksi, psi, dpsidx,
     +                          psiksi )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.4  date 27-02-2010 Extra debug
!        version  2.3  date 23-08-2006 Debug information
!        version  2.2  date 15-02-2004 New calls to elp623/4
!        version  2.1  date 04-07-2003 Remove common celwrk
!        version  2.0  date 19-05-2003 Extra parameters dpsidx, psiksi
!
!   copyright (c) 2002-2010  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Compute the basis functions phi, its derivatives and other related
!     quantities for various element shapes
!     Also the weights for numerical integration are computed
!     Help subroutine for elm800
! **********************************************************************
!
!                       KEYWORDS
!
!     elliptic_equation
!     parabolic_equation
!     basis_function
!     element
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/celint'
      include 'SPcommon/cconst'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer index1(*), jelp
      double precision coor(*), x(*), w(*), dphidx(*), xgauss(*),
     +                 phi(*), qmat(*), tang(*), phiksi(*), psi(*),
     +                 dpsidx(*), psiksi(*)

!     coor           i    array of length ndim x npoint containing the
!                         co-ordinates of the nodal points with respect to the
!                         global numbering
!     dphidx         o    Array of length n x m x ndim  containing the
!                         derivatives
!                         of the basis functions in the sequence:
!                         d phi / dx (xg ) = dphidx (i,k,1);
!                              i        k
!                         d phi / dy (xg ) = dphidx (i,k,2);
!                              i        k
!                         d phi / dz (xg ) = dphidx (i,k,3);
!                              i        k
!                         If the element is a linear simplex, the
!                         derivatives are constant and only k = 1 is filled.
!     dpsidx         o    Array of length n/2 x m x ndim  containing the
!                         derivatives of the linear basis functions
!     index1         i    Array of length inpelm containing the point numbers
!                         of the nodal points in the element
!     jelp           i    Indication of the type of basis function subroutine
!                         must be called by ELxxxx. Possible values:
!                          1:  Subroutine ELP610  (Linear line element)
!                          2:  Subroutine ELP611  (Quadratic line element)
!                          3:  Subroutine ELP620  (Linear triangle)
!                          4:  Subroutine ELP624  (Extended quadratic triangle)
!                          5:  Subroutine ELP621  (Bilinear quadrilateral)
!                          6:  Subroutine ELP623  (Biquadratic quadrilateral)
!                         11:  Subroutine ELP630  (Linear tetrahedron)
!                         12:  Subroutine ELP631  (Quadratic tetrahedron)
!                         13:  Subroutine ELP632  (Trilinear brick)
!                         14:  Subroutine ELP633  (Triquadratic brick)
!     phi            o    array of length n * m, values of shape
!                         functions in integration points in the sequence:
!                         phi_i(xg^k) = phi (i,k)
!                         array phi is only filled when gauss integration is
!                         applied
!     phiksi        i/o   array to store the values of dphi/dxsi
!     psi                 array of length n x m containing the values of the
!                         linear basis functions in the gauss points
!     psiksi        i/o   array to store the values of dpsi/dxsi
!     qmat           o    transformation matrix global -> local coordinates
!                         in case of a 3D element
!     tang           o    In this array the tangential vector is stored
!                         tang(j,i) contains the j th component of the
!                         tangential vector in the i-th integration point
!     w              o    array of length m containing the weights for
!                         integration
!     x              o    x co-ordinate of point
!     xgauss         o    array of length m x ndim containing the co-ordinates
!                         of the gauss points.  array xgauss is only filled when
!                         gauss integration is applied.
!                         xg_i = xgauss (i,1);  yg_i = xgauss (i,2);
!                         zg_i = xgauss (i,3)
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      double precision dphidxi(81), dphideta(81)
      logical debug
      save dphidxi, dphideta

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     dphideta       Array containing dphi_i/deta in a point
!     dphidxi        Array containing dphi_i/dxi in a point
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ELP610         Compute basis functions and weights for linear line element
!     ELP611         Compute basis functions and weights for quadratic line
!                    element
!     ELP616         Compute basis functions and weights for quadratic line
!                    element R2
!     ELP620         Compute basis functions and weights for linear triangle
!     ELP621         Compute basis functions and weights for bilinear
!                    quadrilateral
!     ELP622         Compute basis functions and weights for quadratic triangle
!     ELP623         Compute basis functions and weights for biquadratic
!                    quadrilateral
!     ELP624         Compute basis functions and weights for extended quadratic
!                    triangle
!     ELP625         Compute basis functions and weights for extended linear
!                    triangle
!     ELP626         Compute basis functions and weights for extended bi-linear
!                    quadrilateral
!     ELP630         Compute basis functions and weights for linear tetrahedron
!     ELP631         Compute basis functions and weights for quadratic
!                    tetrahedron
!     ELP632         Compute basis functions and weights for trilinear brick
!     ELP633         Compute basis functions and weights for triquadratic brick
!     ELP635         Compute basis functions and weights for extended linear
!                    tetrahedron
!     ELP636         Compute basis functions and weights for extended tri-linear
!                    hexahedron
!     ELP637         Compute basis functions and weights for extended
!                    quadratic tetrahedron
!     ELP638         Compute basis functions and weights for trilinear prism
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     800   Shape number not available
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm800basefn' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elm800basefn'
         write(irefwr,1) 'jelp', jelp
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

!     --- Switch with respect to the basis functions
!         At this moment shape 15 points to shape 12 because it has not yet
!         been  implemented.
!         This makes only sense for the volume of the element

      write(6,*) 'elm800basefn: jelp=',jelp
      select case ( jelp )

         case( :-1, 8, 19, 20, 22: )

!        --- Shape number not available (<0 or 8 or 19 or 20 >21)

            call errint ( jelp, 1 )
            call errsub ( 800, 1, 0, 0 )

         case(1)

!        --- Shape number 1

            call elp610 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, tang )

         case(2)

!        --- Shape number 2

            if ( ndimlc==2 ) then

!           --- ndim = 2

               call elp616 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                       qmat, tang, dphidxi )

            else

!           --- ndim = 1 or 3

               call elp611 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                       qmat )

            end if

         case(3)

!        --- Shape number 3

            call elp620 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat )

         case(4)

!        --- Shape number 4
            write(6,*) 'call elp622: ',jelp
            call elp622 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi, dpsidx )
      go to 1000

         case(5)

!        --- Shape number 5

            call elp621 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat )

         case(6)

!        --- Shape number 6

            call elp623 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi, dpsidx, dphidxi, dphideta )

         case(7)

!        --- Shape number 7, Extended quadratic triangle

            write(6,*) 'call elp624: ',jelp
            call elp624 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi, dpsidx, dphidxi, dphideta )

         case(9)

!        --- Shape number 9

            call elp626 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi )

         case(10)

!        --- Shape number 10

            call elp625 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi )

         case(11)

!        --- Shape number 11

            call elp630 ( coor, x, w, dphidx, xgauss, phi, index1 )

         case(12,15)

!        --- Shape number 12,15

            call elp631 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    psi, dpsidx )

         case(13)

!        --- Shape number 13

            call elp632 ( coor, x, w, dphidx, xgauss, phi, index1 )

         case(14)

!        --- Shape number 14

            call elp633 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    phiksi, psi, dpsidx, psiksi )

         case(16)

!        --- Shape number 16

            call elp637 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    psi )

         case(17)

!        --- Shape number 17

            call elp636 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    psi )

         case(18)

!        --- Shape number 18

            call elp635 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    psi )

         case(21)

!        --- Shape number 21

           call elp638 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                   phiksi )

      end  select  ! case ( jelp )

1000  call erclos ( 'elm800basefn' )
      if ( debug ) then
         call prinrl1 ( x, ndimlc, n, 'x' )
         write(irefwr,*) 'End elm800basefn'
      end if  ! ( debug )

      end
