      subroutine elm903 ( coor, elemmt, elemvc, elemms, iuser, user,
     +                    index1, index3, index4, vecold, islold,
     +                    vecloc, symm, jmethod, coeffnodes,
     +                    coeffintpnts, phi, psi, phixi, psixi )
! ======================================================================
!
!        programmer    Guus Segal
!        version  4.0  date 27-02-2010 Change of parameter list
!        version  3.0  date 21-12-2008 Extra parameter jmethod
!        version  2.10 date 17-09-2008 Extension with pressure mass matrix
!        version  2.9  date 14-11-2007 Extension with diagonal velocity m matrix
!
!   copyright (c) 1996-2010  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element matrix and vector for the  navier-stokes equations
!     incompressible flow, using the integrated method (Taylor-Hood)
!     Two and three-dimensional elements
!     ELM903 is a help subroutine for subroutine BUILD (TO0050)
!     it is called through the intermediate subroutine elmns2
!     So:
!     BUILD
!     TO0050
!       -  Loop over elements
!          -  ELMMEC
!             - ELM903
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  3 node triangle
!         4 node triangle
!         6 node triangle
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/cellog'
      include 'SPcommon/cinforel'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemmt(*), elemvc(*), coor(*), user(*),
     +                 elemms(*), vecold(*), vecloc(*), psi(nvert,m),
     +                 phixi(n,m,ndim), psixi(nvert,m,ndim), phi(n,m),
     +                 coeffnodes(n,*), coeffintpnts(m,*)
      integer iuser(*), index1(*), index3(*), index4(*), islold(5,*),
     +        jmethod
      logical symm

!     coeffintpnts  i/o   Real array in which the values of the
!                         coefficients in the integration points are stored
!     coeffnodes    i/o   Real array in which the values of the
!                         coefficients in the nodal points are stored
!     coor           i    array of length ndim x npoint containing the
!                         co-ordinates of the nodal points with respect to the
!                         global numbering
!     elemms         o    Element mass matrix
!     elemmt         o    Element matrix
!     elemvc         o    Element vector
!     index1         i    Array of length inpelm containing the point numbers
!                         of the nodal points in the element
!     index3         i    Two-dimensional integer array of length NUMOLD x
!                         NINDEX containing the positions of the "old"
!                         solutions in array
!                         VECOLD with respect to the present element
!                         For example VECOLD(INDEX3(i,j)) contains the j th
!                         unknown with respect to the i th old solution vector.
!                         The number i refers to the i th vector corresponding
!                         to IVCOLD in the call of SYSTM2 or DERIVA
!     index4         i    Two-dimensional integer array of length NUMOLD x
!                         INPELM containing the number of unknowns per point
!                         accumulated in array VECOLD with respect to the
!                         present element.
!                         For example INDEX4(i,1) contains the number of
!                         unknowns in the first point with respect to the
!                         i th vector stored in VECOLD.
!                         The number of unknowns in the j th point with respect
!                         to i th vector in VECOLD is equal to
!                         INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold         i    User input array in which the user puts information
!                         of all preceding solutions
!                         Integer array of length 5 x numold
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     jmethod        i    Indicates type of storage scheme for the matrix
!                         In case jmethod between 13 and 16 extra arrays are
!                         created to store the scaled pressure mass matrix,
!                         the diagonal of the velocity and the pressure mass
!                         matrix
!     phi           i/o   array of length n * m, values of shape
!                         functions in integration points in the sequence:
!                         phi_i(xg^k) = phi (i,k)
!                         array phi is only filled when gauss integration is
!                         applied
!     phixi         i/o   Array of length n x m x ndim containing the values of
!                         the derivatives of the basis functions in the Gauss
!                         points in the reference element.
!                         Array phi is only filled when ifirst = 0 and must be
!                         kept by the program
!     psi           i/o   Array of length n x m containing the values of the
!                         modified basis functions in the integration points
!     psixi         i/o   Array of length nvert x m x ndim containing the values
!                         of the derivatives of the basis functions in the Gauss
!                         points in the reference element
!     symm          i/o   Indicates whether the matrix symmetric (true) or not
!     user           i    Real user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     vecloc              Work array in which all old solution vectors for the
!                         integration points are stored
!     vecold         i    In this array all preceding solutions are stored, i.e.
!                         all solutions that have been computed before and
!                         have been carried to system or deriva by the parameter
!                         islold in the parameter list of these main subroutines
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer isub, isup, jelp, jtime, iflipflop,
     +        mconv, modelv, mcont, nveloc, imesh, mdiv, mcoefconv,
     +        modelrhsd, ioldm, signdiv, indkappa, indcoeffgrad,
     +        indcoeffdiv, indetaspecial, indetha, indomega,
     +        indgoertler, indcconv, iseqvel(81), indfdiv,
     +        iseqpres(8), irow(n), icol(n), i, j, k
      double precision cn, clamb, thetdt, usolcheck(35),
     +                 rhsdiv(npsi), tang(m,2), rhsdcheck(35), w(m+n*2),
     +                 x(n,ndim), xgauss(m,ndim), dphidx(n,m,ndimlc),
     +                 dudx(m,ndim,ncomp), du0dx(m,ndim,ncomp),
     +                 pp(ndimlc+1,ndimlc+1), qmat(9,m),
     +                 sp(ndimlc*n,npsi), ugauss(m,ncomp),
     +                 unew(n,ncomp), unewgs(m,ncomp), uold(n,ncomp),
     +                 uoldm(n,ncomp), dpsidx(nvert,m,ndim), ethaeff(m),
     +                 elemvctmp(n*ndim), elemmtmp((n*ndim)**2),
     +                 elemmstmp((n*ndim)**2), secinv(m),
     +                 psiupw(n,m), detdsc(m), viscrho(m),
     +                 div(ndimlc*n,npsi), gradv(9,m)
      logical debug, second, check, extradebug
      character (len=10) text
      save isub, isup, jelp, jtime, mconv, modelv, cn, clamb,
     +     thetdt, mcont, nveloc, imesh, mdiv, mcoefconv,
     +     modelrhsd, ioldm, iseqvel, iseqpres

!     check          Indicates if checks must be performed or not
!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     detdsc         Derivative of second invariant per integration point
!     div            Divergence matrix
!                    div(i,k) corresponds to the k-th row and i-th column
!                    of the matrix, The k refers to the basis functions of
!                    the pressure, i to the basis functions of the velocity
!                    in the sequence of the solution vector
!     dphidx         Array of length n x m x ndim  containing the
!                    derivatives
!                    of the basis functions in the sequence:
!                         d phi_i/ dx_j(xg_k) = dphidx (i,k,j);
!                    If the element is a linear simplex, the
!                    derivatives are constant and only k = 1 is filled.
!     dpsidx         Array of length n x m x l  containing the derivatives
!                    of the linear basis functions in the sequence:
!                    d psi / dx (xg ) = psix (i,k,1);
!                         i        k
!                    d psi / dy (xg ) = psix (i,k,2);
!                         i        k
!                    d psi / dz (xg ) = psix (i,k,3);
!                         i        k
!     du0dx          See dudx, but now for the extra velocity
!     dudx           Real m x ndim  array in which the values of the
!                    derivatives of the solution vector in the integration
!                    points are stored in the sequence:
!                    du / dx  (x ) = dudx ( k, j )
!                      j     k
!                    If jdercn = 0 in common block celint, then only
!                    dudx (1,*) is filled because dudx is then a constant
!                    per element
!                    dudx is only filled if jderiv>0
!     elemmstmp      Work array to store element mass matrix temporarily
!     elemmtmp       Work array to store element matrix temporarily
!     elemvctmp      Work array to store element vector temporarily
!     ethaeff        contains effective viscosity
!     extradebug     If true extra debugging statements are carried out
!     gradv          Array containing the gradient vector in
!                    the integration points
!     i              Counting variable
!     icol           Help array to store column numbers
!     iflipflop      Work array to prevent flip-flop behaviour in non-
!                    linear upwind
!                    The status of the element with respect to upwind is
!                    stored in this array
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     indcconv       Sequence number of convection coefficients
!     indcoeffdiv    Sequence number of divergence coefficients
!     indcoeffgrad   Sequence number of gradient coefficients
!     indetaspecial  Sequence number of special viscosity
!     indetha        Sequence number of viscosity
!     indfdiv        Sequence number of right-hand side continuity equation
!     indgoertler    Sequence number of goertler coefficients
!     indkappa       Sequence number of kappa
!     indomega       Sequence number of omega
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     irow           Help array to store row numbers
!     iseqpres       Contains the sequence numbers of the pressure in the
!                    element vector
!     iseqvel        Array containing the positions of the velocities in
!                    the element vector in the sequence required by el2005
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     j              Counting variable
!     jelp           Indication of the type of basis function subroutine must
!                    be called by ELM100.
!     jtime          Integer parameter indicating if the mass matrix for the
!                    time-dependent equation must be computed (>0) or not ( = 0)
!                    Possible values:
!                      0:  no mass matrix
!                      1:  diagonal mass matrix with constant coefficient
!                      2:  diagonal mass matrix with variable coefficient
!                     11:  full mass matrix with constant coefficient
!                     12:  full mass matrix with variable coefficient
!                    101:  refers to the special possibility of the theta
!                          method directly applied
!                    102:  refers to the special possibility of the theta
!                          method directly applied with variable rho
!     k              Counting variable
!     mcoefconv      Defines how the convective term is treated:
!                    0: standard case
!                    1: special case: the convective term is defined as
!                          c_conv_1 u du/dx + c_conv_2 v du/dy
!                          c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!                       Not applicable with penalty function method
!                    4: the continuity equation contains a pressure term.
!     mconv          Defines the treatment of the convective terms
!                    Possible values:
!                    0: convective terms are skipped (Stokes flow)
!                    1: convective terms are linearized by Picard iteration
!                    2: convective terms are linearized by Newton iteration
!                    3: convective terms are linearized by the incorrect Picard
!                       iteration
!                    4: convective terms are linearized by successive
!                       substitution
!     mdiv           Defines if divergence right-hand side is used (1) or
!                    not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     nveloc         Number of velocity parameters
!     pp             Pressure matrix
!     psiupw         Array of size n*m containing the adapted basis
!                    functions for upwind
!     qmat           transformation matrix global -> local coordinates
!                    in case of a 3D element
!     rhsdcheck      Help parameter to store a right-hand side vector for
!                    checking purposes
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     secinv         Array containing the second invariant in the
!                    integration points
!     second         Indicates if second derivative term is important in
!                    case of upwinding
!     signdiv        Multiplication sign of "continuity equations"
!                    if -1 the stokes matrix is symmetric
!                    if 1 all eigenvalues are in the positive half plane
!     sp             Gradient of pressure matrix
!     tang           In this array the tangential vector is stored
!                    tang(j,i) contains the j th component of the
!                    tangential vector in the i-th integration point
!     text           String variable containing some text
!     thetdt         1/(theta dt) for the case of the theta method
!                    immediately applied
!     ugauss         Preceding solution in Gauss points
!     unew           New solution vector
!     unewgs         solution vector in integration points
!     uold           Array containing the previous iteration (old solution)
!     uoldm          Solution at time level m
!     usolcheck      Help parameter to store a solution vector for
!                    checking purposes
!     viscrho        viscosity / rho
!     w              array of length m containing the weights for
!                    integration
!     x              array of length n x ndim containing the co-ordinates
!                    of the nodes
!     xgauss         array of length m x ndim containing the co-ordinates
!                    of the gauss points.  array xgauss is only filled when
!                    gauss integration is applied.
!                    xg_i = xgauss (i,1);  yg_i = xgauss (i,2);
!                    zg_i = xgauss (i,3)
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0903         Initialize parameters for subroutines ELD/M/I903
!     EL1004         Compute gradient and divergence matrix (Taylor-Hood)
!     EL1008         Fill complete matrix for Navier-Stokes (Taylor-Hood)
!     ELFILLSOL01    Fill solution vector for an element (testing purposes)
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM800UPWBASE  Compute the modified basis functions psi due to upwinding
!     ELM900ADDRESS1 Compute starting addresses of subarrays in array
!     ELM900COEFFS   Fill variable coefficients in coefficients array
!                    Fill velocity from prior iteration if necessary
!     ELM900MASSMAT  Fill element mass matrix for the incompressible
!                    Navier-Stokes equations
!     ELM900MATAL    Store extra element matrices which are necessary for
!                    schur complement type methods
!     ELM900MATRIX   Fill element matrix for the incompressible Navier-Stokes
!                    equations
!     ELM900RHSD     Compute right-hand side vector for Navier-Stokes element
!     ELM903UPW      Update stiffness matrix with upwind term due to viscosity
!                    Update gradient matrix with upwind term
!     ELMATMULT      Matrix vector multiplication for a full element matrix
!     ELVISC         Compute effective viscosity depending on the viscosity
!                    model
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRSUB         Error messages
!     PRININ         print 1d integer array
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
!     PRINRL1_IND    Print parts of a full element matrix by indicating rows and
!                    columns
!     PRINRL2        Print a real two-dimensional array
!     TODIVVEC       Divide 2 vectors components wise, i.e. y_i = x1_i / x2_i
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  information about the time-integration
!         Possible values:
!         0: if imas in BUILD=0 then time-independent flow
!            else the mass matrix is built according to imas
!         1: The theta method is applied directly in the following way:
!            the mass matrix divided by theta delta is added to the stiffness
!            matrix
!            the mass matrix divided by theta delta and multiplied by u(n) is
!            added to the right-hand-side vector
!     2:  type of viscosity model used (modelv) as well as given stress
!         tensor as right-hand side accordinag to
!         modelv + 1000*model_rhsd
!         Possible values for modelv
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!         Possible values for model_rhsd:
!         0:    if there is a given stress tensor the reference to this
!               tensor is stored in position 19
!         1:    It is supposed that there are 6 given stress tensor components
!               stored in positions 19-24
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!         2: Polar co-ordinates
!     5:  Information about the convective terms (mconv)
!         and continuity equation (mcont) according to mconv+10*mcont
!         Possible values for mconv:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!         Possible values for mcont:
!         0: Standard situation, the continuity equation is div u = 0
!         1: A special continuity equation div ( rho u ) = 0, which rho
!            variable is solved. This is not the standard continuity equation
!            for incompressible flow. In fact we have a time-independent
!            compressible flow description.
!            This possiblility can not be applied with the penalty function
!            approach
!         4: the continuity equation contains a pressure term.
!         5: Special case: the pressure mass matrix with coefficient 1/mu
!            is stored, with mu the viscosity
!            This option supposes that there is no compressiblity or
!            penalty term. It is meant for special iterative solvers
!     6-20: Information about the equations and solution method
!     6:  -
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-16: not yet used
!    17:  kappa  Parameter for the pressure term in the continuity equation
!                (MCONT=4)
!    18:  fdiv   Right-hand side for continuity equation
!    19:  stress If model_rhsd = 0 then
!                Given stress tensor used in the right-hand side vector
!                At this moment it is supposed that the stress is defined
!                per element and that in each element all components are
!                constant
!                If model_rhsd = 1 then
!                The six components of the stress tensors are stored in
!                positions 19 to 24 in the order: xx, yy, zz, xy, yz, zx
!                This option can not be used in cooporation with the
!                dimensionless coefficients stored in positions 20-41
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2873   upwind not yet implemented
! **********************************************************************
!
!                       PSEUDO CODE
!
!    if ( first call ) then
!       initialize standard parameters
!    compute basis functions
!    fill old solutions if necessary
!    if ( matrix ) then
!       fill the viscosity in the corresponding array
!       Compute the viscous terms
!       if ( mconv>0 ) then
!          compute convective terms
!       Compute the divergence matrix
!       Compute the pressure matrix
!       Compute the penalty contribution
!    if ( vector ) then
!       Compute contribution of body forces
!       if ( mconv=2 ) then
!          compute contribution of convective terms
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm903' )
      debug = .false.
      extradebug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elm903'
         write(irefwr,1) 'ielem', ielem
         write(irefwr,1) 'n, m, nvert, npsi, ncomp, ndim',
     +                    n, m, nvert, npsi, ncomp, ndim
         call prinin ( index1, inpelm, 'index1' )
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         icheld = 0
         call el0903 ( iuser, user, jelp, jtime, cn, clamb,
     +                 mconv, mcont, modelv, isub, isup, coeffintpnts,
     +                 thetdt, nveloc, iseqvel, iseqpres, mdiv,
     +                 modelrhsd, index4, coeffnodes )
         if ( ierror/=0 ) go to 1000
         if ( debug ) then

!        --- debug information

            call prinin ( iseqvel, ndim*n, 'iseqvel' )
            call prinin ( iseqpres, npsi, 'iseqpres' )
            write(irefwr,1) 'metupw', metupw

         end if

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

      end if  ! ( ifirst==0 )

      rhsdiv = 0d0

      signdiv = -1

!     --- Compute some sequence numbers of coefficients

      call elm900address1 ( indkappa, indcoeffgrad, indcoeffdiv,
     +                      indetaspecial, indomega, indetha,
     +                      indgoertler, indcconv, mcont, indfdiv )

!     --- compute basis functions and weights for numerical integration

      if ( jelp<9 .or. jelp>10 .and. jelp<17 ) jdiag = jdiag+10

      write(6,*) 'jelp = ',jelp
      call elm800basefn ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, tang, jelp, phixi, psi, dpsidx, psixi )
      if ( extradebug ) then
         call prinrl1 ( x, ndim, n, 'x' )
         call prinrl1 ( xgauss, ndim, m, 'xgauss' )
         call prinrl ( w, m, 'w' )
         call prinrl1 ( phi, m, n, 'phi' )
         call prinrl1 ( dphidx, m, n, 'dphi/dx' )
         call prinrl1 ( dphidx(1,1,2), m, n, 'dphi/dy' )
         call prinrl1 ( psi, m, npsi, 'psi' )
      end if  ! ( debug )

      if ( jelp<9 .or. jelp>10 .and. jelp<17 ) jdiag = jdiag-10
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      call elm900coeffs ( vecold, uoldm, dphidx, index3,
     +                    iseqvel, ioldm, index4, phi, ugauss,
     +                    dudx, unew, du0dx, unewgs,
     +                    uold, mcont, isub, isup,
     +                    imesh, iuser, user, vecloc, x, xgauss,
     +                    coeffnodes, coeffintpnts )

!     --- Compute the divergence and gradient matrix

      call el1004 ( sp, div, psi, dphidx, phi, w,
     +              xgauss, mcont, coeffintpnts(1,1),
     +              pp, coeffintpnts(1,indkappa), rhsdiv,
     +              coeffintpnts(1,indfdiv), mdiv, coeffnodes(1,1) )

      if ( extradebug ) then

!     --- extra debug

         call prinrl2 ( sp, ndimlc*n, npsi, 'Grad-matrix' )
         call prinrl1 ( div, npsi, ndimlc*n, 'Div-matrix' )

      end if  ! ( extradebug )

      if ( modelv/=5 ) then

!     --- Compute effective viscosity in case of modelv # 5

         call elvisc ( modelv, coeffintpnts(1,indetha), ethaeff, cn,
     +                 clamb, xgauss, ugauss, dudx,
     +                 secinv, detdsc, gradv, vecloc, maxunk, numold )
         if ( extradebug ) write(irefwr,*) 'after elvisc'

      end if  ! ( modelv/=5 )

      if ( metupw>0 ) then

!     --- Upwind method, compute upwind basis functions
!         First compute viscosity/rho, which is the parameter eps

         call todivvec ( viscrho, ethaeff, coeffintpnts(1,1), m )
         second = .true.
         call elm800upwbase ( metupw, jelp, viscrho,
     +                        ugauss, phi, psiupw,
     +                        x, second, dphidx,
     +                        dudx, iflipflop, dpsidx )
         if ( extradebug ) then

!        --- extra debug

            call prinrl1 ( psiupw, m, n, 'psi_upw' )

         end if  ! ( extradebug )

      end if  ! ( metupw>0 )

!     --- Fill parts of matrix if necessary

      if ( matrix ) then

!     --- matrix = true    compute element matrix
!         fill the viscosity in array etaeff

         call elm900matrix ( elemmtmp, dphidx, phi, xgauss,
     +                       coeffintpnts(1,indetaspecial),
     +                       w, ethaeff, mcont, coeffintpnts(1,1),
     +                       coeffintpnts(1,indomega), mconv, imesh,
     +                       coeffintpnts(1,indcconv), ugauss, unewgs,
     +                       unew, modelv, symm, mcoefconv,
     +                       nveloc, du0dx, dudx,
     +                       coeffintpnts(1,indgoertler),
     +                       coeffintpnts(1,indkappa), psiupw )
         if ( extradebug ) then

!        --- extra debug

            call prinrl1 ( elemmtmp, ndimlc*n, ndimlc*n,
     +                     'stiffness matrix' )

         end if  ! ( extradebug )

      end if

      if ( vector .and. notvec==0 ) then

!     --- vector = true, compute element vector
         write(6,*) 'call elm900rhsd',ielem
         call elm900rhsd ( elemvctmp, coeffintpnts(1,1),
     +                     phi, coeffintpnts(1,3),
     +                     coeffintpnts(1,10),
     +                     coeffintpnts(1,indcconv),
     +                     ugauss, dudx, coeffintpnts(1,indgoertler),
     +                     coeffintpnts(1,indkappa), detdsc, w,
     +                     dphidx, cn, mconv, jtime, mcoefconv,
     +                     mcont, modelv, modelrhsd, secinv,
     +                     xgauss, psiupw , x)
         if ( extradebug ) then

!        --- extra debug

            call prinrl ( elemvctmp, ndimlc*n, 'rhs' )

         end if  ! ( extradebug )

      end if

      if ( metupw>0 ) then

!     --- Upwind method, extend gradient matrix and stiffness matrix
!         with upwind terms

         call elm903upw ( sp, phi, psiupw,
     +                    dpsidx, w )

         if ( extradebug ) then

!        --- extra debug

            call prinrl2 ( sp, ndimlc*n, npsi, 'Grad-matrix/2' )

         end if  ! ( extradebug )

      end if  ! ( metupw>0 )

      if ( jtime>0 ) then

!     --- jtime>0, i.e. the mass matrix must be computed
!         First compute the mass matrix for one velocity component
!         The non-diagonal mass matrix for this component is stored in
!         work positions ipwork - ipwork+n*n-1
!         The diagonal matrix in work pos. ipwork+n*n - ipwork+n*n+n-1

         if ( metupw>0 ) then

!        --- Upwind not yet implemented

            call errchr ( 'Mass matrix', 1 )
            call errsub ( 2873, 0, 0, 1 )

         end if  ! ( metup==0 )

         call elm900massmat ( elemms, coeffintpnts(1,1), w,
     +                        phi, elemmstmp, elemmtmp, uoldm,
     +                        elemvctmp, thetdt, uold, jtime, mconv,
     +                        dphidx, dudx,
     +                        coeffintpnts(1,indgoertler),
     +                        coeffintpnts(1,indkappa), index1,
     +                        index3, index4, mcont, ugauss,
     +                        vecold, xgauss, ioldm, iseqvel )
         if ( extradebug ) write(irefwr,*) 'after elm900massmat'

      end if

!     --- Construct final matrix and vector from various parts
!         Eliminate centroid if necessary

      call el1008 ( elemmt, elemmtmp, sp, div,
     +              elemvc, elemvctmp, iseqvel, iseqpres,
     +              pp, mcont, rhsdiv, signdiv )
      if ( extradebug ) write(irefwr,*) 'after el1008'

      if ( jmethod>=13 .and. jmethod<=16 ) then

!     --- Special case for Schur type elements
!         Some extra matrices are required, that are stored in array
!         element, behind the standard element matrix

         call elm900matal ( elemmt, w, npsi, ethaeff,
     +                      iseqvel, psi, .false. )

      end if  ! ( jmethod>=13 .and. jmethod<=16 )

1000  call erclos ( 'elm903' )

      if ( debug ) then

!     --- debug = true

         write(irefwr,*) 'element ', ielem, '  itype ',itype
         call prinrl(elemvc,icount,'elemvc')
         call prinrl1(elemmt,icount,icount,'elemmt')
         if ( notmas==0 ) then
            if ( imas==1 ) then
               call prinrl(elemms,icount,'elemms')
            else
               call prinrl1(elemms,icount,icount,'elemms')
            end if  ! ( imas==1 )
         end if  ! ( notmas==0 )
         check = .false.
         if ( check ) then

!        --- Check is true, Print subarrays

            do i = 1, ndim
               do k = 1, n
                  irow(k) = iseqvel(i+ndim*(k-1))
               end do
               do j = 1, ndim
                  do k = 1, n
                     icol(k) = iseqvel(j+ndim*(k-1))
                  end do
                  write(text,1010) i, j
1010              format('S_',2i1)
                  call prinrl1_ind ( elemmt, icount, icount, irow, icol,
     +                               n, n, text )
               end do
               write(text,1020) i
1020           format('L^t_',i1)
               call prinrl1_ind ( elemmt, icount, icount, irow,
     +                            iseqpres, n, npsi, text )
               write(text,1030) i
1030           format('L_',i1)
               call prinrl1_ind ( elemmt, icount, icount, iseqpres,
     +                            irow, npsi, n, text )

            end do
            call prinrl1_ind ( elemmt, icount, icount, iseqpres,
     +                         iseqpres, npsi, npsi, 'Zero-mat' )

         end if
         write(irefwr,*) 'End elm903'

      end if
      check = .false.
      if ( check ) then

!     --- Check is true, extra debugging

         call elfillsol01 ( 1, usolcheck, iseqvel, iseqpres, ndim,
     +                      n, npsi )
         call elmatmult ( elemmt, usolcheck, rhsdcheck, icount )
         call prinrl ( rhsdcheck, icount, 'rhsdcheck' )

      end if

      end
