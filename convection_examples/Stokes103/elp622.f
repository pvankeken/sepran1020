      subroutine elp622 ( coor, x, w, dphidx, xgauss, phi, index1,
     +                    qmat, psi, dpsidx )
! ======================================================================
!
!        programmer    Guus Segal
!        version  7.3  date 14-11-2007 Extension with diagonal mass matrix
!        version  7.2  date 09-02-2007 Debug statements
!        version  7.1  date 04-07-2003 Remove common celwrk
!        version  7.0  date 19-05-2003 Extra parameter dpsidx (RvL)
!
!   copyright (c) 1989-2009  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     compute basis functions and related quantities for quadratic triangle
!     computed are:     weights for numerical integration
!                       values of derivatives of basis functions
!                       values of basis functions in integration points
!     elp622 is called by several types of element subroutines
! **********************************************************************
!
!                       KEYWORDS
!
!     basis_function
!     element
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cactl'
      include 'SPcommon/cconst'
      include 'SPcommon/celp'
      include 'SPcommon/celint'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cmcdpr'
      include 'SPcommon/cinforel'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer index1(6)
      double precision coor(ndim,*), x(6,ndim), w(*), dphidx(6,m,ndim),
     +                 xgauss(m,ndim), phi(6,m), qmat(3,*), psi(3,m),
     +                 dpsidx(3,m,2)

!     coor           i    array of length 2 x number of nodal points containing
!                         the co-ordinates of the nodal points with respect to
!                         the global numbering
!                         x  = coor (1,i);  y  = coor (2,i);
!                          i                 i
!     dphidx         o    array of length 6 x m x 2  containing the derivatives
!                         of the basis functions in the sequence:
!                                      k
!                         d phi / dx (x )= dphidx (i,k,1);
!                              i
!                                      k
!                         d phi / dy (x )= dphidx (i,k,2);
!                              i
!                         if a newton cotes rule is used and m=3 then the
!                         derivatives are filled in the midside points,
!                         else if m=6 the derivatives are filled in all nodes
!     dpsidx         o    Array of length n/2 x m x ndim  containing the
!                         derivatives of the linear basis functions
!     index1         i    array of length 6 containing the nodal point numbers
!                         of the element
!     phi            o    array of length 6 x m containing the values of the
!                         basis functions in the gauss points in the sequence:
!                                k
!                         phi (xg ) = phi (i,k)
!                            i
!                         array phi is only filled when ifirst = 0 and gauss
!                         integration is applied
!     psi            o    array of length 3 x m containing the values of the
!                         linear basis functions in the gauss points
!     qmat           o    transformation matrix global -> local coordinates
!                         in case of a 3D element
!     w              o    array of length m containing the weights for the
!                         numerical integration
!     x             i/o   array of length 6 x 2 containing the x- and
!                         y-coordinates of the nodal points with respect to the
!                         local numbering in the sequence:
!                         x  = x (i,1);  y  = x (i,2);
!                          i              i
!     xgauss         o    array of length m x 2 containing the co-ordinates of
!                         the gauss points.  array xgauss is only filled when
!                         gauss integration is applied.
!                         xg  = xgauss (i,1);  yg  = xgauss (i,2);
!                           i                    i
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      double precision phixsi(6,6), phieta(6,6), h, pi2, sum
      double precision xorg(3), delta(7), dxdxsi(7,3,3),
     +                 dpdxsi(6,7), dpdeta(6,7), h1, h2
      logical curved, comppsi, debug, compqmat
      integer i, k, nedge, iweigo, iqmat, intrule
      save pi2, phixsi, phieta, iweigo, iqmat, dpdxsi, dpdeta

!     comppsi        if true the linear basis functions must be evaluated
!     compqmat       if true qmat must be evaluated
!     curved         indication if the element is curved (true) or not (false)
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     delta          work array of length 6 to store the values of the jacobian
!                    in the nodes
!     dpdeta         contains derivatives in reference element for gauss rule
!                                                   k
!                    dpdeta(i,k) = d phi  / d eta (x )
!                                       i
!     dpdxsi         contains derivatives in reference element for gauss rule
!                                                   k
!                    dpdxsi(i,k) = d phi  / d xsi (x )
!                                       i
!     dxdxsi         work array of length 7x3x3 to store the values of dx/dxsi,
!                    dx/deta, dy/dxsi and dy/deta according to:
!                             k
!                    dx/dxsi(x ) = dxdxsi(k,1,1)
!                             k
!                    dx/deta(x ) = dxdxsi(k,1,2)
!                             k
!                    dy/dxsi(x ) = dxdxsi(k,2,1)
!                             k
!                    dy/deta(x ) = dxdxsi(k,2,2) etc.
!     h              help variable to store a quantity temporarily
!     h1             Help variable to compute a quantity temporarily
!     h2             Help variable to compute a quantity temporarily
!     i              counting variable
!     intrule        Actual intergration rule
!     iqmat          if 1 the qmat matrix must be filled
!     iweigo         if 1 the weights must be filled
!     k              counting variable
!     nedge          Number of edges in the element
!     phieta         array of length 6 x 6 to store the values of dphi/deta
!                    according to:
!                                k
!                    dphi /deta(x ) = phieta(k,i)
!                        i
!     phixsi         array of length 6 x 6 to store the values of dphi/dxsi
!                    according to:
!                                k
!                    dphi /dxsi(x ) = phixsi(i,k)
!                        i
!     pi2            2 pi
!     sum            help variable to compute a sum
!     xorg           Contains the original coordinates of the first node
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ELP622CURVED   Compute basis functions and related quantities for
!                    quadratic triangle (curved element)
!     ELP622FIRST    Fill element independent quantities for a quadratic
!                    triangle
!     ELP622STRAIGHT Compute basis functions and related quantities for
!                    quadratic triangle (straight element)
!     ELPMAPTANG     Map three-dimensional array dphidx onto array dphids on
!                    surface using mapping array qmat
!     ELPQMAT        Fill array qmat with unit vectors in tangential directions
!                    and normal
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
!     QLCSYS         Perform transformation from 3D co-ordinates to local (2D)
!                    co-ordinates
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data phixsi / -3d0,  4d0, -1d0,  0d0,  0d0,  0d0,
     +              -1d0,  0d0,  1d0,  0d0,  0d0,  0d0,
     +               1d0, -4d0,  3d0,  0d0,  0d0,  0d0,
     +               1d0, -2d0,  1d0,  2d0,  0d0, -2d0,
     +               1d0,  0d0, -1d0,  4d0,  0d0, -4d0,
     +              -1d0,  2d0, -1d0,  2d0,  0d0, -2d0 /
      data phieta / -3d0,  0d0,  0d0,  0d0, -1d0,  4d0,
     +              -1d0, -2d0,  0d0,  2d0, -1d0,  2d0,
     +               1d0, -4d0,  0d0,  4d0, -1d0,  0d0,
     +               1d0, -2d0,  0d0,  2d0,  1d0, -2d0,
     +               1d0,  0d0,  0d0,  0d0,  3d0, -4d0,
     +              -1d0,  0d0,  0d0,  0d0,  1d0,  0d0 /
! ======================================================================
!
      call eropen ( 'elp622' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elp622'
         write(irefwr,1) 'ndim', ndim
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000
      iweigo = mod(iweigh,10)
      iqmat  = iweigh/10
      comppsi = jdiag/10==1
      intrule = mod(irule,100)
      if ( debug ) then
         write(irefwr,1) 'iweigo, iqmat, intrule, iweigh, ifirst',
     +                    iweigo, iqmat, intrule, iweigh, ifirst
         write(irefwr,*) 'comppsi', comppsi
      end if  ! ( debug )
      compqmat = .false.

!     --- Check if we need to compute qmat

      if (iqmat==3) then

!     --- Special case: iqmat = 3

         compqmat = .true.

!        --- reset iqmat!!

         iqmat = 0

      end if

      if ( ifirst==0 ) then

!     --- ifirst = 0, make some initializations

         call elp622first ( intrule, w, pi2, dpdxsi, dpdeta, phixsi,
     +                      phieta, iweigo, phi, psi, comppsi )

      end if
      if ( debug ) write(irefwr,1) 'icoor', icoor

      if ( icoor>=1 ) then

!     --- icoor = 1   fill co-ordinates of nodes in array x

         if ( iqmat==1 ) then

!        --- Special case for VIP
!            Triangle in three-dimensional space
!            transform co-ordinates

            nedge = 3
            call qlcsys( coor, inpelm, nedge, index1, x, xorg, qmat)

         else

!        --- Standard case

            do k = 1, ndim
               do i = 1, 6
                if (icoor==1)  x(i,k) = coor(k,index1(i))
                if (icoor==2)  x(i,k) = coor(k,i)
               end do
            end do

         end if

      end if

!     --- check if element is straight or curved

      if ( ndim>2 .and. iqmat==0 ) then

!     --- Standard 3D case. Treat as curved

         curved = .true.

      else

!     --- 2D case, compute curved

         curved = .false.
         if ( abs(x(2,1)+x(4,1)+x(6,1)-x(1,1)-x(3,1)-x(5,1)) >
     +        sqreps ) then
            curved = .true.
         else if ( abs(x(2,2)+x(4,2)+x(6,2)-x(1,2)-x(3,2)-x(5,2)) >
     +             sqreps ) then
            curved = .true.
         end if

      end if
      if ( debug ) write(irefwr,*) 'curved', curved

      if ( .not. curved ) then

!     --- curved = false,  straight triangle

         call elp622straight ( iweigo, x, index1, intrule,
     +                         xgauss, w, pi2, dphidx, dpsidx,
     +                         comppsi, delta(1) )

      else

!     --- curved = true,  curved triangle

         call elp622curved ( iweigo, x, index1, intrule,
     +                       xgauss, w, pi2, phieta, phixsi,
     +                       dphidx, dpdxsi, dpdeta, dxdxsi,
     +                       iqmat, phi, delta )

         if ( compqmat ) then

!        --- Fill array qmat
!            Loop over integration points

            call elpqmat ( dxdxsi, qmat )
            call elpmaptang ( dphidx, qmat )  ! map grad phi onto surface

         end if

      end if  ! ( .not. curved )

      if ( itype>=902 .and. itype<=903 ) then

!     --- Special case of Navier-Stokes element
!         w(1..m ) contains the weights
!         w(m+1 .. m+n) the diagonal of the mass matrix
!         It may not be lumped, since the lumped mass matrix is singular

         if ( intrule>2 ) then

!        --- intrule>2 Gauss rule, use w and phi

            do i = 1, 6
               sum = 0d0
               do k = 1, m
                  sum = sum+w(k)*phi(i,k)**2
               end do  ! k = 1, m
               w(m+i)= sum
            end do  ! i = 1, 6

         else

!        --- intrule<3, we have to use an approximation
!            Since this vector is only used for scaling,
!            it does not have to be very accurate

            if ( curved ) then
               h = 0d0
               do k = 1, m
                  h = h+abs(delta(k))
               end do
               h = h/m
            else
               h = abs(delta(1))
            end if  ! ( curved )
            h1 = h/60d0        ! vertices
            h2 = 4d0*h/45d0    ! mid points
            if ( jcart==1 ) then

!           --- jcart = 1, Cartesian coordinates

               do k = 1, 3
                  w(m+2*k-1) = h1
                  w(m+2*k) = h2
               end do  ! k = 1, 3

            else

!           --- jcart = 2, Axi-symmetric coordinates

               do k = 1, 3
                  w(m+2*k-1) = h1*x(2*k-1,1)
                  w(m+2*k) = h2*x(2*k,1)
               end do  ! k = 1, 3

            end if  ! ( jcart==1 )

         end if  ! ( intrule>2 )
         if ( debug ) call prinrl ( w(m+1), 6, 'diag mass matrix' )

      end if  ! ( itype>=900 .and. itype<=903 )

1000  call erclos ( 'elp622' )
      if ( debug ) then

!     --- Debug information

         call prinrl ( w, m, 'w' )
         call prinrl1 ( dphidx(1,1,1), m, 6, 'dphidx' )
         call prinrl1 ( dphidx(1,1,2), m, 6, 'dphidy' )
         write(irefwr,*) 'End elp622'

      end if  ! ( debug )

      end
