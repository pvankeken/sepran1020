c *   *** pecof800
c *   *** Specification of coefficients for heat equation (type 800)
c *   *** heatproduction :  constant heat production rate
c *   *** metupw         :  type of upwinding
c *   *** intrule        :  type of numerical integration
c *   *** icoorsystem    :  type of coordinate system
c *   *** iqtype         :  type of internal heating
      real*8 heatproduction
      integer metupw,intrule,icoorsystem,iqtype
      common /pecof800/ heatproduction,metupw,intrule,
     v    icoorsystem,iqtype
