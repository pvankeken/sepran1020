      subroutine prcgst ( n, imat, amat, premat, locrhsd, usol, eps1,
     +                    eps2, nmax, inpsol, r, r0bar, p, v, t, umin,
     +                    pbar, jmetod, blockunkinfo, sendinfo,
     +                    innercontr )
c ======================================================================
c
c        programmer    Zdenek Vlasek,
c        version  3.0  date 03-12-2001 Parallel version
c        version  2.6  date 14-07-2000 inpsol is filled when iter>nmax
c        version  2.5  date 23-06-2000 Adaptation of output
c        version  2.4  date 09-11-1998 Minor correction error message
c        version  2.3  date 26-06-1998 Include common
c        version  2.2  date 20-03-1998 Adaptation default criterion
c        version  2.1  date 11-02-1998 Better messages for imin<iter
c        version  2.0  date 27-01-1998 Extra parameter umin
c        version  1.14 date 23-01-1998 Random start introduced
c        version  1.13 date 09-01-1997 Stabilization due to v/dVorst
c        version  1.12 date 13-11-1996 Test on machine accuracy
c        version  1.11 date 18-03-1996 Replace icontr by inpsol
c        version  1.10 date 01-03-1996 Kees V. Better termination
c        version  1.9  date 20-02-1996 Extension with icontr(3)=2
c        version  1.8  date 07-01-1995 Perform at least one iteration
c        version  1.7  date 12-02-1993 new type of error messages
c        version  1.6  date 27-11-1991 new formats
c        version  1.5  date 26-07-1991 Notation, better iter counting
c        version  1.4  date 21-10-1990 max renamed to nmax
c        version  1.3  date 19-10-1990 bug in rferr
c        version  1.2  date 08-10-1990 Eisenstat preconditioning
c        version  1.1  date 28-08-1990 Bi-CGSTAB with post-precond.
c        version  1.0  date 22-08-1990 Bi-CGSTAB adapted from prcgs
c
c   copyright (c) 1990-2001  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c **********************************************************************
c
c                       DESCRIPTION
c
c     Apply the Bi-CGSTAB preconditioned method of conjugate gradients to solve
c     the linear system of equations AMAT USOL = RHSD      ( A x = b )
c     where the matrix A and the preconditioning matrix M may be unsymmetric
c        ( see Report of Henk A. Van der Vorst, 8 July 1990 )
c                                                    T
c     matrix A and the preconditioning matrix K = L U  may be unsymmetric.
c     This subroutine contains the following algorithms:
c     -  Standard Bi-CGSTAB method
c     -  Right-preconditioned Bi-CGSTAB method
c     -  Eisenstat version of the left-right-preconditioned Bi-CGSTAB method
c     Both in the case of the preconditioned Bi-CGSTAB method
c     as in the case of the Eisenstat implementation, the matrix and right-hand
c     side are supposed to be premultiplied by the inverse of the diagonal D
c     of the triangular factors from the preconditioning matrix in the
c     following way:  ( A x = b )
c               -1        -1
c          A = D   A     D
c                   orig
c               -1
c          b = D   b
c                   orig
c     If the initial estimate x is given it must also be premultiplied by D,
c     before PRCGST is called:
c          x = D  x
c                  orig
c                                                        -1
c     At output the solution x must be premultiplied by D   in order to get
c     the solution of the original problem, after go to 1000 from PRCGST
c                  -1
c         x     = D   x
c          orig
c     In the case of Eisenstat the matrix amat is supposed to contain the
c     matrix H = D  - 2I in the first n positions, where D  is the diagonal
c                 A                                       A
c     of A after the premultiplication
c        algorithm:  Bi-CGSTAB  ( Henk A. Van der Vorst, Report July 8, 1990 )
c                    left-right-preconditioning for Eisenstat version,
c                    right-preconditioning      for other cases
c    adapted from the CGS-subroutine prcgs of
c    Guus Segal, Rik Kaasschieter, Henk van der Vorst
c **********************************************************************
c
c                       KEYWORDS
c
c     conjugate_gradients
c     iterative
c     linear_solver
c     preconditioning
c     parallel
c **********************************************************************
c
c                       MODULES USED
c
      implicit none
c **********************************************************************
c
c                       COMMON BLOCKS
c
      include 'SPcommon/cmcdpr'
      include 'SPcommon/cmcdpi'

c **********************************************************************
c
c                       INPUT / OUTPUT PARAMETERS
c
      integer n, imat(*), nmax, inpsol(*), jmetod(*),
     +        blockunkinfo(*), sendinfo(*),  innercontr(*)
      double precision amat(*), premat(*), locrhsd(n), usol(n),
     +                 eps1, eps2, r(n), r0bar(n), p(n), v(n), t(n),
     +                 umin(n), pbar(n)

c     amat    i    Matrix to be solved. The storage must be in accordance
c                  with the description of array imat. In the case of
c                  preconditioning amat must have been premultiplied as
c                  described in Purpose.
c     blockunkinfo   i    Array with global block information for surface
c                         number isurnr. In this case it is related to
c                         non-prescribed degrees of freedom and not to nodes
c                         Contents:
c                         Pos. 1  m ( number of neighbour blocks)
c                         Pos. 2*i   block number of i-th neighbour
c                         Pos. 2*i+1 starting address of i-th neighbour in
c                                    array sendinfo
c     eps1    i    Help variable for the termination criterion of the CG-process
c                  If eps1 = 0, the termination criterion is based upon the
c                  relative error
c     eps2    i    Help variable for the termination criterion of the CG-process
c                  If eps2 = 0, the termination criterion is based upon the
c                  absolute error
c     imat    i    Integer array describing the stucture of the large matrix
c     innercontr     i    Array of size nrusol indicating if an unknown in the
c                         block must be used in a global inner product or not
c                         innercontr(i) = 1: take unknown i into account
c                         innercontr(i) = 0: take unknown i not into account
c                         The unknown in the block with smallest number is taken
c                         into account
c     inpsol i/o   Contains integer information concerning the solution
c                  process. This array may be filled by subroutine READ03,
c                  which reads information from the standard input file
c                  Array INPSOL must be filled as follows:
c                  Pos. 1  Contains the number of iterations at output
c                  Pos. 2  If an error has been occured: 1 else 0
c                  Pos. 3  METHOD  Indicates the type of solver used
c                                Possible values:
c                                1: CG (conjugate gradients) or BiCGstab
c                                   In case of a non-symmetric matrix
c                                   Bi-CGSTAB is used
c                                2: CGS is used
c                                3: GMRES is used
c                                4: GMRESR is used
c                  Pos. 4  IPRECO  Is used only for iterative methods
c                                Control parameter concerning the type of
c                                preconditioning
c                                Possible values
c                                0: No preconditioning.
c                                1: The matrix is premultiplied by a diagonal
c                                   matrix, such that all diagonal elements
c                                   become 1. (Diagonal scaling)
c                                2: Incomplete LU-decomposition using the
c                                   diagonal as only changed elements
c                                   Efficient Eisenstat implementation
c                                3: Incomplete LU-decomposition
c                                4: Symmetric Gauss-Seidel
c                                5: Incomplete LU-decomposition using a pre-
c                                   assembled decomposition according to
c                                   a new method of Rik Kaasschieter
c                                6: a standard (=default) preconditioner,
c                                7: See 2, however, MILU is applied
c                  Pos. 5  MAXITR  Maximum number of iterations to be performed
c                  Pos. 6  IPRINT  Print level of subroutine
c                                Possible values:
c                               -1   No print output
c                                0   Only error messages are printed
c                                1   A little amount of information of the
c                                    iteration method is printed
c                                2   The first and last iterations are
c                                    printed
c                                3   A maximal amount of information of the
c                                    iteration method is printed
c                                4   See 3, in this case also the matrix and
c                                    right-hand side are printed
c                  Pos. 7  MATSYM  Is used only when nonsymmetric storage
c                                scheme jmetod=9,10,11,12 is used.
c                                It tells that the matrix is symmetric, so the
c                                CG methods can be used. Possible values:
c                                0: The matrix is supposed to be nonsymmetric
c                                1: The matrix is symmetric.
c                  Pos. 8  MGMRES  Dimension of Krylov space for GMRES
c                  Pos. 9  ISTART  Control parameter concerning the start of
c                                the conjugate process. (ISTART).
c                                Possibilities:
c                                0: The process starts with vector 0
c                                1: The process starts with an explicitly
c                                       given start vector.
c                  Pos. 10 KEEP  With this parameter the user may indicate
c                                whether the preconditioning matrix must be
c                                destroyed, kept or that an old preconditioning
c                                matrix must be used. Possibilities:
c                                0 The preconditioning matrix is destroyed.
c                                1 The preconditioning matrix is kept.
c                                  A reference is written in MATR(4).
c                                2 The preconditioning matrix as computed
c                                  before is used.
c                                  It is supposed that the reference for
c                                  this matrix has been stored in MATR(4).
c                  Pos. 11 IRELER: Indicates which criterium should be used
c                                for the termination of the process
c                                Possible values:
c                                -1: The relative error is taken with respect
c                                    to ||b||.
c                                 0: The relative error is taken with respect
c                                    to ||x||.
c                                 1: The relative error is taken with respect
c                                    to ||res0||, where res0=initial residual
c                                 2: The absolute error is taken
c                                10: Use, default value, i.e. 1 except for
c                                    CG where 0 is used
c                  Pos. 12 IRESID: Indicates whether the residual must be
c                                computed and printed after computation of the
c                                solution. Possible values:
c                                0:  Do not compute the residual
c                                1:  Compute and print the 2-norm of the
c                                    residual
c                                2:  Compute and print the 2-norm of the
c                                    residual divided by the 2-norm of the
c                                    right-hand side
c                  Pos. 13 NTRUNK: This parameter is only used if GMRESR is
c                                used. It indicates the maximum number of search
c                                directions in the GMRESR process. NTRUNK should
c                                be much smaller than MGMRES
c                  Pos. 14 NINNER: This parameter is only used if GMRESR is
c                                used. It indicates the maximum number of
c                                iterations in the inner loop of the GMRESR
c                                process.
c                  Pos. 15 Not used
c                  Pos. 16 Not used
c                  Pos. 17 Not used
c                  Pos. 18 Not used
c                  Pos. 19 Not used
c                  Pos. 20 Not used
c                  Pos. 21 Not used
c                  Pos. 22 AT_ERROR: Defines what to do in case of an error:
c                                Possible values:
c                                0: stop
c                                1: return
c     jmetod  i    Indicates type of storage scheme for the matrix
c                  Possible values:
c                   1:  Symmetric profile matrix
c                   2:  Non-symmetric profile matrix
c                   3:  Symmetric complex profile matrix
c                   4:  Non-symmetric complex profile matrix
c                   5:  Symmetric compact storage (Real matrix)
c                   6:  Non-symmetric compact storage (Real matrix)
c                   7:  Symmetric compact storage (Complex matrix)
c                   8:  Non-symmetric compact storage (Complex matrix)
c                   9:  Non-symmetric structure compact storage (Real matrix)
c                  10:  Non-symmetric structure compact storage (Complex matrix)
c                  11:  Compact storage for vectorprocessors (Real matrix)
c                  12:  Compact storage for vectorprocessors (Complex matrix)
c                  30:  Special matrix for 9-point molecule
c                       structured 2D grid
c                  For jmetod(1)=30, jmetod(2) and (3) must also be filled
c                  as follows:
c                  Pos. 2:  4
c                  Pos. 3:  nx-1
c     locrhsd i    Real vector of length n containing the right-hand-side
c                  vector of the system of equations. In the case of
c                  preconditioning amat must have been premultiplied as
c                  described in Purpose. In the parallel case this
c                  contains only the local part of rhsd.
c     n       i    The number of rows in the matrix A. (size of the matrix)
c     nmax    i    The maximum number of iterations that may be performed
c     p            Work array of length n to store the direction vector
c     pbar         Work array of length n to store the vector pbar,
c                  needed only for iprec = 2, 3 or 5,
c                  the vector sbar(*) shares this memory
c     premat  i    Preconditioning of the matrix amat. Is only used if the
c                  preconditioned CG method is used.
c     r            Work array of length n to store the residual vector r,
c                  the vector s(*) shares this memory
c     r0bar        Work array of length n to store the vector r0bar.
c     sendinfo       i    Array containing information of the transport of
c                         unknowns between neighbouring blocks
c                         The starting address of each neighbour block is
c                         stored in array blockuninfo
c                         For each neighbour block the following information
c                         is stored
c                         Pos. 1 number of common unknowns n on common side of
c                                both blocks
c                         Pos. 2 ... n+1  unknown numbers of the unknowns to be
c                                sent to or received from block i
c                         If a common point in one block contains more
c                         unknowns than in the other block only the minimum
c                         of both is transported since the other ones do
c                         not belong to both blocks
c     t            Work array of length n to store the vector t
c     umin         Work array of length n to store the vector usol with
c                  the smallest residual norm
c     usol   i/o   Solution vector of length n. Depending on the array INPSOL
c                  usol must be filled by the user or not. At output the last
c                  iteration is stored in usol.
c     v            Work array of length n to store the vector v
c **********************************************************************
c
c                       LOCAL PARAMETERS
c
      logical eistat, precon, repeat
      logical restart
      integer iprint, iprec, j, iter, ifiter, imin, priter(10)
      double precision rferr, eps, sigma, alpha, beta, rho, ss,
     +                 rhoold, rnorm, omega, tt, r0norm, rmin,
     +                 residu(10), rhsd(n)

c     alpha   Factor alpha in the CG-process ( (r,r) / (p,Ap) )
c     beta    Factor beta in the CG-process ( (r ,r ) / (r   ,r   ) )
c                                                 i  i      i-1  i-1
c     eistat  Logical parameter, indicating if the Eisenstat implementation
c             is used (true) or not (false)
c     eps     Square of required accuracy (including relative and absolute
c               error )
c     ifiter  Starting value of parameter iter
c     imin    Iteration with minimal norm
c     iprec   Type of preconditioning used. Possible values:
c             0    No preconditioning
c             1    Diagonal scaling
c             2    Eisenstat preconditioning
c             3    Incomplete LU decomposition
c             4    Symmetric Gauss-Seidel preconditioning
c             5    Preassembled preconditioning according to Kaasschieter
c             7    Eisenstat preconditioning (MILU variant)
c     iprint  Indication of the amount of print output required.
c             Possible values:
c             <0   No output
c             0    Only fatal errors are printed
c             1    Some minimal information concerning the iteration
c                  process is printed
c             2    Gives a maximal amount of output concerning the iteration
c                  process
c     iter    Loop variable
c     j       Counting variable
c     omega   ( t, s ) / ( t, t )
c     precon  Logical parameter, indicating if a preconditioning is used
c             (not Eisenstat) (true) or not (false)
c     priter  Output array to print residuals
c     r0norm  norm of start residual
c     repeat  Logical variable indicating if the accuracy check must be
c             carried out twice (true) or once (false), because the
c             norm of the solution could not be computed
c     residu  Output array to print residuals
c     restart If true the recomputation of rho has not been started
c     rferr   Maximal accuracy possible for this matrix, due to round-off
c     rho     Inner product ( r0bar, r )
c     rhoold  Inner product ( r0bar, r ) from the previous iteration
c     rhsd    Real vector of length n containing the right-hand-side
c             vector of the system of equations. In the case of
c             preconditioning amat must have been premultiplied as
c             described in Purpose. In the parallel case this contains
c             the global part of rhsd.
c     rmin    minimal norm of residual
c     rnorm   norm of residual
c     sigma   Factor sigma in the CG-process (inner product of p and Ap)
c     ss      ( s, s )
c     tt      ( t, t )
c **********************************************************************
c
c                       SUBROUTINES CALLED
c
      double precision prpardnrm2, prparddot

c     DAXPY          Overwrite double precision dy with double precision
c                    da*dx + dy
c     DCOPY          Copy double precision dx to double precision dy
c     ERCLOS         Resets old name of previous subroutine of higher level
c     EROPEN         Produces concatenated name of local subroutine
c     ERREAL         Put real in error message
c     ERRINT         Put integer in error message
c     ERRSUB         Error messages
c     ERRWAR         Print warnings
c     PRPARDDOT      Parallel Blas routine to compute inner product of two
c                    vectors
c     PRPARDNRM2     Parallel Blas routine to compute the 2-norm of a vector
c     PRPARMATVEC    Parallel matrix-vector multiplication
c     PRPARPREC      computes the product of M(inv) times vector, where M is the
c                    preconditioning matrix
c     PRPARRANDM     Computes random number
c     PRPARRHSD      computes the global rhsd from the local rhsd (parallel)
c **********************************************************************
c
c                       I/O
c
c    none
c **********************************************************************
c
c                       ERROR MESSAGES
c
c     676   The CG-process has terminated because the number of iterations
c           exceeds the maximum value
c     677   The maximal accuracy with respect to the condition of the
c           matrix is reached
c    1063   Bi-cgstab looks divergent
c    1400   Bi-CGSTAB process has terminated because sigma = 0
c    1401   Bi-CGSTAB process has terminated because norm of vector t = 0
c **********************************************************************
c
c                       PSEUDO CODE
c
c     If no preconditioning is used then K = I in the right-preconditioned
c     process.
c     The right-preconditioned Bi-CGSTAB process
c                       ( see Henk A. Van der Vorst, Report July 8, 1990 )
c     and the Eisenstat version of left-right-preconditioned Bi-CGSTAB
c     process can be written as:
c     first approximation x is given
c                                             -1
c     r = b - A x ,  but for Eisenstat:  r = L  * ( b - A x )
c     r0bar = r
c    Iteration: k = 1 (1) nmax  while ||r|| > eps
c       rho = ( r0bar, r )
c       If ( k = multiple of sqrt(n) )
c          check error level
c       End
c       If ( k=1 )
c          p = r
c       Else ( k>1 )
c          beta = ( rho / rhoold ) * ( alpha / omega )
c          p = r + beta * ( p - omega * v )
c       End
c               -1                      T                           -T
c       pbar = K  * p ,  where K = L * U ,   but Eisenstat: pbar = U   * p
c                                                  -1
c       v = A * pbar,   but Eisenstat: v = pbar + L  * ( H * pbar + p )
c       sigma = ( r0bar, v )
c       alpha = rho / sigma
c       First update of x:  x = x + alpha * pbar
c       s = r - alpha * v
c               -1                      T                           -T
c       sbar = K  * s ,  where K = L * U ,   but Eisenstat: sbar = U   * s
c                                                  -1
c       t = A * sbar,   but Eisenstat: t = sbar + L  * ( H * sbar + s )
c       omega = ( t, s ) / (||t|| ||s||)
c       omega = sign(omega)max(|omega|,0.7) ||s||/||t||
c       Second update of x:  x = x + omega * sbar
c       r = s - omega * t
c       rhoold = rho
c    End
c    Check error level:
c                         2
c            2       (k/n)                2
c    If ||r||  <  exp       || Ax -b -r ||   ,  but Eisenstat:
c                               -1              2
c         ...      ...      || L  ( Ax -b) -r ||  ,
c    then maximal accuracy has been reached.
c    Notice:  s           can share memory with r,
c             sbar        can share memory with pbar,
c             sbar, pbar  are not needed when K = 1
c **********************************************************************
c
c                       DATA STATEMENTS
c
c ======================================================================
c
c     --- Set some constants

      call eropen ( 'prcgst' )

      iprint = inpsol(6)
      iprec  = inpsol(4)
      eistat = iprec.eq.2 .or. iprec.eq.7
      precon = iprec.eq.3 .or. iprec.eq.5
      restart = .true.

      call prparrhsd ( n, locrhsd, rhsd, blockunkinfo, sendinfo )

      if ( eistat ) then

c     --- Use norm of L^(-1) * b.

         call prparprec ( 1, jmetod, n, imat, amat, rhsd, r,
     +                    blockunkinfo, sendinfo )
         rferr  = 100*prpardnrm2 ( n, r, 1, innercontr ) * epsmac

      else

         rferr  = 100*prpardnrm2 ( n, rhsd, 1, innercontr ) * epsmac

      end if

c     --- Print heading if iprint > 0

      if ( iprint.gt.0 ) write ( irefwr,100 )
100   format(//' Output for the Bi-CGSTAB method',
     +         ' (non-symmetric matrix)'//)

      ifiter = 1

c     --- Set residual r = b, it will be updated when x # 0, label 110
c         is used when the method is restarted

110   call dcopy ( n, rhsd, 1, r, 1 )

      if ( ifiter.eq.1 .and. inpsol(9).eq.0 ) then

c     --- istart = 0,  Set start vector x  = 0

         do j = 1, n
            usol (j) = 0d0
         end do

      else

c     --- istart # 0,  Start vector x  is given by the user,
c                      compute residual vector  r = b - A x ,
c                      use scratch array t(*) to store A * x

         if ( ifiter.eq.1 .and. inpsol(9).eq.2 ) then

c        --- Start vector is random

            iter = 0

            call prparrandm ( n, usol, blockunkinfo, sendinfo )

         end if

         if ( ifiter.eq.1 ) then
            call dcopy ( n, usol, 1, umin, 1 )
         end if

         call prparmatvec ( jmetod, n, imat, amat, usol, t,
     +                      blockunkinfo, sendinfo )

         if ( eistat ) then

c        --- In the case of Eisenstat there is  amat = A - 2 * I,
c            so t(*) must be corrected

            call daxpy ( n, 2d0, usol, 1, t, 1 )

         end if

c        --- Update r = r - t

         call daxpy ( n, -1d0, t, 1, r, 1 )

      end if

c     --- Premultiply residual by L^(-1) in the case of Eisenstat

      if ( eistat ) then
         call prparprec ( 1, jmetod, n, imat, amat, r, r,
     +                    blockunkinfo, sendinfo )
      end if

c     --- Copy r into r0bar

      call dcopy ( n, r, 1, r0bar, 1 )

c     --- Set constants
c         Norm of the initial residual r

      rnorm = prpardnrm2 ( n, r, 1, innercontr )
      rmin  = rnorm
      imin  = ifiter

      if ( ifiter.eq.1 ) then

c     --- iprint >= 1,  Print information

         if ( iprint.ge.1 ) write ( irefwr, 125 ) rnorm
125      format ( ' The 2-norm of the initial residual', ss,1pe17.2e2 )

c        --- iprint >= 2,  More output

         if ( iprint.ge.2 ) write ( irefwr, 130 ) 0, rnorm
130      format ( ' Iteration number  residual'  // i10, ss,1pe15.2e2 )

      end if

c     --- First estimate of eps  ( eps = eps1 + ||b|| eps2 )

      if ( ifiter.eq.1 ) then

         r0norm = rnorm
         if ( abs(eps2).le.0d0 ) then

            eps = eps1
            repeat = .false.

         else

            if ( inpsol(11).eq.0 ) then

               eps = eps1 + prpardnrm2 ( n, rhsd, 1, innercontr )*eps2
               repeat = .true.

            else if ( inpsol(11).gt.0 ) then

               eps = eps1 + r0norm * eps2
               repeat = .false.

            else

               if ( eistat ) then

c              --- Use norm of L^(-1) * b,  t(*) is a scratch array

                  call prparprec ( 1, jmetod, n, imat, amat, rhsd, t,
     +                    blockunkinfo, sendinfo )
                  eps = eps1 + prpardnrm2 ( n, t, 1, innercontr )*eps2

               else

                  eps = eps1 + prpardnrm2 ( n, rhsd, 1,
     +                  innercontr ) * eps2

               end if

               repeat = .false.

            end if

         end if

      end if

c     --- Check error level.

      if ( eps.lt.rferr ) then

c     --- eps is smaller than maximal accuracy possible
c         It is useless to end do the process after rnorm
c         is less than rferr

         eps = rferr

         if ( iprint.ge.0 ) then
            call erreal ( rferr, 1 )
            call errwar ( 677, 0, 1, 0 )
         end if

      end if

c     --- While loop check on accuracy ( nmax+1 is used because
c         the residual of the previous iteration is always checked )

      do 400 iter = ifiter, nmax+1

c     --- Check on accuracy reached in iter-1

140      if ( iter.gt.ifiter .and. rnorm.le.eps .or. rnorm.le.0d0 ) then

c        --- The accuracy of the process seems to be reached
c            Check if eps must be recomputed

            if ( repeat ) then

c           --- repeat is true, recompute eps

               repeat = .false.
               eps = eps1 + prpardnrm2 ( n, usol, 1,
     +                    innercontr ) * eps2

c              --- Check error level.

               if ( eps.lt.rferr ) then

c              --- eps is smaller than maximal accuracy possible
c                  It is useless to end do the process after rnorm
c                  is less than rferr

                  eps = rferr

                  if ( iprint.ge.0 ) then
                     call erreal ( rferr, 1 )
                     call errwar ( 677, 0, 1, 0 )
                  end if

               end if

c              --- return to the check if rnorm is less than

                   eps = rferr

               goto 140

            end if

c           --- Due to rounding errors it is possible that the updated
c               residual differs from the true residual, for this
c               reason the norm of the true residual is computed,
c               if it does not satisfy the termination criterion the
c               method is restarted.
c               Set residual r = b

            call dcopy ( n, rhsd, 1, r, 1 )

c           --- Compute residual vector  r = b - A x ,
c               use scratch array t(*) to store A * x

            call prparmatvec ( jmetod, n, imat, amat, usol, t,
     +                      blockunkinfo, sendinfo )

            if ( eistat ) then

c           --- In the case of Eisenstat there is  amat = A - 2 * I,
c               so t(*) must be corrected

               call daxpy ( n, 2d0, usol, 1, t, 1 )

            end if

c           --- Update r = r - t

            call daxpy ( n, -1d0, t, 1, r, 1 )

c           --- Premultiply residual by L   in the case of Eisenstat

            if ( eistat ) then
               call prparprec ( 1, jmetod, n, imat, amat, r, r,
     +                    blockunkinfo, sendinfo )
            end if

c           --- Check if the norm of the true residual satisfies the
c               termination criterion

            rnorm = prpardnrm2 ( n, r, 1, innercontr )

            if ( rnorm.gt.eps .and. iter.le.nmax ) then
               ifiter = iter
               rmin   = rnorm
               imin   = iter

c              --- the method is restarted since the norm of
c                  the true residual is not less than eps

               go to 110
            end if

            if ( iprint.eq.2 .and. iter.gt.10 ) then

c           --- iprint = 2 Print final residuals

               do j = max(1,22-iter), 10
                  write ( irefwr, 350 ) priter(j), residu(j)
               end do

            end if

            if ( iprint.ge.1 ) then

c           --- iprint >= 1  Print information

               write ( irefwr,150 ) iter-1, rnorm
150            format(' The number of executed iterations in the',
     +                ' Bi-CGSTAB process is', i5/
     +                ' The 2-norm of the residual is:', ss,1pe12.2e2 )

            end if

            inpsol(1) = iter - 1
            inpsol(2) = 0

c           --- this is a regular termination

            go to 1000

         end if

c        --- Accuracy not yet reached

         if ( rnorm.gt.eps/epsmac .and. ifiter.lt.iter ) then

c        --- rnorm  overflow, Process restartes or terminates

            call errint ( iter, 1 )

            if ( inpsol(22).eq.1 .and. imin.gt.1 ) then

               call errwar ( 1063, 1, 0, 0 )
               call dcopy ( n, umin, 1, usol, 1 )

               if ( imin.gt.ifiter ) then

                  ifiter = iter

c                 --- overflow occurs, however the norm is reduced since
c                     the last restart so it is useful to restart the
c                     method

                  go to 110

               else

                  inpsol(1) = iter - 1
                  inpsol(2) = 0

               end if

            else

               call errsub ( 1063, 1, 0, 0 )

            end if

            if ( iprint.ge.1 ) then

c           --- iprint >= 1  Print information

               write ( irefwr,150 ) iter-1, rmin
               write ( irefwr,160 ) imin-1
160            format (' The minimal residual is obtained at',
     +                 ' iteration', i5 )

            end if

c           --- overflow occurs and the norm is not reduced since
c               the last restart so it is not useful to restart the
c               method, or it is not requested to restart

            go to 1000

         end if

c        --- do not start (nmax+1)-th iteration

         if ( iter.gt.nmax ) goto 400

c        --- Compute rho = ( r0bar, r )

         rho = prparddot ( n, r0bar, 1, r, 1, innercontr )

         if ( iter.gt.1 ) then

c        --- rhoold is available

c            Correction similar to correction in PRCGS of June 23, 1997

            if ( restart .and. abs(rhoold)*epsmac*1d2.gt.abs(rho)
     +          ) then

c           --- rho too small, restart the process

               ifiter = iter

               restart = .false.
               go to 110

            end if

         end if

         if ( iter.eq.ifiter ) then

c        --- the method is (re)started,  copy r into p

            call dcopy ( n, r, 1, p, 1 )

         else

c        --- k > 1
c            beta = ( rho / rhoold ) * ( alpha / omega )
c            p = r + beta * ( p - omega * v )

            beta = ( rho / rhoold ) * ( alpha / omega )

            do j = 1, n
               p (j) = r (j) + beta * ( p (j) - omega * v (j) )
            end do

         end if

c        --- pbar = K^(-T) * p ,  but Eisenstat:  pbar U^(-T) * p
c            v = A * pbar  ,  but Eisenstat:  v = pbar + L^(-1) * ( H*pbar*p )

         if ( eistat ) then

            call prparprec ( 2, jmetod, n, imat, amat, p, pbar,
     +                    blockunkinfo, sendinfo )

            do j = 1, n
               v (j) = amat (j) * pbar (j) + p (j)
            end do

            call prparprec ( 1, jmetod, n, imat, amat, v, v,
     +                    blockunkinfo, sendinfo )

            call daxpy ( n, 1d0, pbar, 1, v, 1 )

         else

c        --- Normal situation

            if ( precon ) then

               call prparprec ( 0, jmetod, n, imat, premat, p, pbar,
     +                    blockunkinfo, sendinfo )
               call prparmatvec ( jmetod, n, imat, amat, pbar, v,
     +                      blockunkinfo, sendinfo )

            else

c           --- no  pbar,  pbar is the same as p

               call prparmatvec ( jmetod, n, imat, amat, p, v,
     +                      blockunkinfo, sendinfo )

            end if

         end if

c        --- Compute sigma = ( r0bar, v ),  alpha = rho / sigma

         sigma = prparddot ( n, r0bar, 1, v, 1, innercontr )
         if ( abs(sigma).le.0d0 ) then

c        --- Sigma = 0, Process terminates

            call errsub ( 1400, 0, 0, 0 )

            inpsol(1) = iter
            inpsol(2) = 2

            go to 1000

         end if

         alpha = rho / sigma

c        --- First update of x:  x = x + alpha * pbar , to relase pbar

         if ( eistat .or. precon ) then
            call daxpy ( n, alpha, pbar, 1, usol, 1 )
         else
            call daxpy ( n, alpha, p, 1, usol, 1 )
         end if

c        --- Compute s = r - alpha * v  ,
c            r  can be overwritten by s(*)

         call daxpy ( n, -alpha, v, 1, r, 1 )
         ss = prparddot ( n, r, 1, r, 1, innercontr )

c        --- sbar = K^(-1) * s ,  but Eisenstat:  sbar = U^(-T) * s
c            t = A * sbar  ,  but Eisenstat:  t = sbar + L^(-1) * ( H*sbar + s )
c            sbar is stored in pbar(*),
c            s    is stored in r(*)

         if ( eistat ) then

            call prparprec ( 2, jmetod, n, imat, amat, r, pbar,
     +                    blockunkinfo, sendinfo )

            do j = 1, n
               t (j) = amat (j) * pbar (j) + r (j)
            end do

            call prparprec ( 1, jmetod, n, imat, amat, t, t,
     +                    blockunkinfo, sendinfo )

            call daxpy ( n, 1d0, pbar, 1, t, 1 )

         else

c        --- Normal situation

            if ( precon ) then

               call prparprec ( 0, jmetod, n, imat, premat, r, pbar,
     +                    blockunkinfo, sendinfo )
               call prparmatvec ( jmetod, n, imat, amat, pbar, t,
     +                      blockunkinfo, sendinfo )

            else

c           --- no  sbar,  sbar is the same as  s

               call prparmatvec ( jmetod, n, imat, amat, r, t,
     +                      blockunkinfo, sendinfo )

            end if

         end if

c        --- Compute omega  = ( t, s ) / ( t, t ),  s is stored in r(*),

         tt = prparddot ( n, t, 1, t, 1, innercontr )
         if ( abs(tt).le.0d0 ) then

c        --- t = 0,  process terminates

            call errsub ( 1401, 0, 0, 0 )

            inpsol(1) = iter
            inpsol(2) = 2

            go to 1000

         end if

c        --- Stabilization as described in Sleijpen and v/d/ Vorst (1995)

         omega = prparddot ( n, t, 1, r, 1, innercontr ) / sqrt(tt*ss)
         omega = sign(1d0,omega) * max(abs(omega),0.7d0) * sqrt(ss/tt)

c        --- Second update of x:  x_k = x_k + omega_k * sbar ,
c            sbar is stored in pbar(*),  s is stored in r(*)

         if ( eistat .or. precon ) then
            call daxpy ( n, omega, pbar, 1, usol, 1 )
         else
            call daxpy ( n, omega, r, 1, usol, 1 )
         end if

c        --- Update r_k = s - omega_k * t,   s is stored in r(*)

         call daxpy ( n, -omega, t, 1, r, 1 )

c        --- Compute norm of residual

         rnorm = prpardnrm2 ( n, r, 1, innercontr )

         if ( rnorm.lt.rmin ) then
            call dcopy ( n, usol, 1, umin, 1 )
            rmin = rnorm
            imin = iter+1
         end if

         if ( iprint.eq.2) then

c        --- iprint = 2, print the first and last residuals

            if ( iter.le.10 ) then

               write ( irefwr, 350 ) iter, rnorm
               priter(iter) = iter
               residu(iter) = rnorm

            else

               do j = 2, 10

                  priter(j-1) = priter(j)
                  residu(j-1) = residu(j)
               end do

               priter(10) = iter
               residu(10) = rnorm

            end if
         end if
         if ( iprint.ge.3 ) then

c        --- iprint >= 3,  Maximal output

            write ( irefwr, 350 ) iter, rnorm
350         format( i10, ss,1pe15.2e2 )

         end if

         rhoold = rho

400   continue

c     --- If the subroutine reaches this stage, too much  iterations have
c         been carried out

      if ( iprint.eq.2 .and. iter.gt.10 ) then

c     --- iprint = 2 Print final residuals

         do j = max(1,23-iter) , 10
            write ( irefwr, 350 ) priter(j), residu(j)
         end do

      end if

      if ( iprint.ge.1 ) then

c     --- iprint >= 1  Print information

         write ( irefwr,150 ) nmax, rmin
         write ( irefwr,160 ) imin-1

      end if

      call errint ( nmax, 1 )

      inpsol(1)=nmax
      inpsol(2)=1

      if ( inpsol(22).eq.1 ) then

         call errwar ( 676, 1, 0, 0 )

         if ( inpsol(22).ge.1 .and. imin.gt.1 ) then

            call dcopy ( n, umin, 1, usol, 1 )

         end if

      else

         call errsub ( 676, 1, 0, 0 )

      end if

1000  call erclos ( 'prcgst' )

      end
