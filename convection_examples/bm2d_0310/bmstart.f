c *************************************************************
c *   BMSTART
c *
c *   Pseudocode:
c *
c *     Read user input
c *
c *     Create mesh for Stokes equation (mesh)
c *     Derive mesh for heat equation by subdivision of Stokes grid (nstmsh)
c *        If necessary: provide mapping information (pemap1to1/2)
c *
c *     Create problem definition (problem)
c *     Create information on the large matrices (commat/matstruc)
c * 
c *     Create initial condition (creavc) or read from file (readbs)
c *     Prescribe boundary conditions (presdf)
c *
c *************************************************************
c *     Arrays:
c *        kmesh1, kmesh2 :   mesh definition (for Stokes/heat)
c *        kprob1, kprob2 :   problem definition ( ,, / ,,)
c *        intmt1, intmt2 :   info on structure of large matrix
c *        matr1 , matr2  :   coefficients of large matrix
c *        iuser , user   :   user arrays 
c *        isol1 , isol2  :   solution vectors                    
c *        islol1, islol2 :   old (previous) solution vectors
c *        ipemap11, ipemap12: vectors containing info on maps between
c *                            different meshes
c *        nbufdef, num   :   declared size of IBUFFR and NUM
c *
c *   PvK 080100
c *************************************************************
      subroutine bmstart(kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2,
     v                   matr1,matr2,iuser,user,isol1,isol2,
     v                   islol1,ipemap11,ipemap12,nbufdef,num)
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),intmt1(*)
      integer intmt2(*),matr1(*),matr2(*),iuser(*),isol1(*),isol2(*)
      integer islol1(5,*),nbufdef,num,kmeshdum(400),ipemap11(5)
      integer ipemap12(5)
      real*8 user(*)
      include 'SPcommon/cbuffr'
      include 'bmiter.inc'
      include 'peparam.inc'
      include 'vislo.inc'
      include 'c1visc.inc'
      include 'SPcommon/cmacht'
      real*8 wavel,ampini
      common /inival/ wavel,ampini
      include 'pexcyc.inc'
      include 'powerlaw.inc'
      include 'SPcommon/cplot'
      include 'pecpu.inc'
      integer iu1(3)
      include 'pesimplerheology.inc'
      include 'pecof900.inc'
      include 'pecof800.inc'
      integer ishape,iinput,jtypv,nsup1,nsup2,npoint,iincommt(5)
      integer irestart,iu2,i,idummy,idum
      include 'solutionmethod.inc'
      real*8 rinput,u2,rl,u1
      real*4 second

      kmesh1(1)  = 400
      kmesh2(1)  = 400
      kmeshdum(1)  = 400
      kprob1(1)  = 400
      kprob2(1)  = 400
      iuser(1)   = 100
       user(1)   = num+5
      ipemap11(1)  = 5
      ipemap12(1)  = 5

c *************************************************************
c ***        USER INPUT
c *************************************************************

c *   *** Nitermax: maximum number of iterations 
c *   *** eps     : relative accuracy required for convergence
c *   *** relax   : relaxation parameter 
c *   *** irestart: indication for restart (1) or not (0)
      read(5,*) nitermax,eps,relax,irestart

c *   *** Rayleigh: Thermal rayleigh number
c *   *** wavel   : Wavelength of initial perturbation
c *   *** ampini  : amplitude of initial perturbation
c *   *** itypv   : Type of viscosity law:
c *   ***           0 = isoviscous
c *   ***           1 = layer dependent
c *   ***           2 = temperature, pressure dependent
c *   ***           4 = non-Newtonian 
c *   ***           (see pefvis.f for actual specification of viscosity)
      read(5,*) rayleigh,wavel,ampini,itypv

c *   *** Coefficients used in viscosity law:
c *   *** avisc   :  nondimensional activation energy
c *   *** bvisc   :  activation volume
c *   *** wvisc   :  ??
      read(5,*) avisc,bvisc,wvisc

c *   *** Specification of layering (for viscosity)
c *   *** nlay    : number of layers
c *   *** prefac  : prefactors in viscosity law
c *   *** zint    : depth of the interface (z=1-y)
      read(5,*) nlay,(prefac(i),i=1,nlay),(zint(i),i=1,nlay-1)
 
c *   *** q       : internal heating rate
      read(5,*) q
      if (q.gt.0) iqtype=1

c *   *** Specification of some coefficients for differential equations
c *   *** See SP guide, chapters 3 and 7.
c *   *** metupw  : Type of upwinding (0=none, 2=Il'in)
c *   *** intrule : integration rule for heat equation 
c *   ***           (0=default, 1=NC, 3=4 point Gauss)
c *   *** icoorsystem: coordinate system (0=Cartesian)
c *   *** intrule900: integration rule for Stokes equations
c *   *** interpol900: for interpolation of quadratic elements
      read(5,*) metupw,intrule,icoorsystem,intrule900,interpol900
c
c *   *** Information on solution method (see PG Chapter 8)
c *   *** isolmethod : type of solution method
c *   ***              -1: old fashioned direct method
c *   ***               0: direct method (new approach)
c *   ***               1: CG/BiCGSTAB
c *   ***               2: GMRES
c *   *** iluprec    :  set to 1 if you want to keep the preconditioning matrix
c *   *** ipreco     : type of preconditioner (for isolmethod>0)
c *   ***               0: none
c *   ***               1: diagonal
c *   ***               2: ILU (eisenstat)
c *   ***               3: ILU
c *   *** iprint     : controls amount of info printed during iteration process
c *   *** keep       : set to 1 to keep precond matrix
c *   *** cgeps      : accuracy of iterative solution
      read(5,*) isolmethod,iluprec,ipreco,maxiter,iprint,keep,cgeps


c *************************************************************
c *   SEPRAN INITIALIZATION
c *************************************************************

c *   *** General starting routine for Sepran
      call start(0,1,0,0)
      nbuffr = nbufdef

c *   *** Define mesh for Stokes equations. Option 1: read from input
c *   call mesh(0,iinput,rinput,kmesh1)

c *   *** Define mesh for Stokes equations. Option 2: read from mesh file
c *   *** that is generated, for example, by makemesh
      open(99,file='mesh1')
      call meshrd(-2,99,kmesh1)
      close(99)
      open(99,file='mesh2')
      call meshrd(-2,99,kmesh2)
      close(99)

c *   *** Plot meshes. Set jmark=0 if you want nodal point and
c *   *** element numbers plmtted (use only for small meshes)
      jkader=-1
      jmark=5
      call plotm2(0,kmesh1,iuser,10d0,1d0)

c     *** Determine coordinates of nodal points along
c     *** x=0 and y=0.
      ishape=2
      call pefilxy(ishape,kmesh1,kprob1,idummy)
c     *** aspect ratio
      rlam = xcmax-xcmin

c     *** Problem definition
      call probdf(0,kprob1,kmesh1,iinput)
      call probdf(0,kprob2,kmesh2,iinput)

c     *** Compute structure of large matrices
      if (isolmethod.eq.-1) then
c        *** penalty function method
         call commat(1,kmesh1,kprob1,intmt1)
      else
         if (isolmethod.eq.0) then
c           *** penalty function method, symmetric
            iincommt(1) = 2
            iincommt(2) = 1
         else
c           *** 6=compact non symmetric. 12=row/column compressed
            iincommt(1) = 3
            iincommt(2) = 6
            iincommt(4) = iluprec
         endif
         call matstruc(iincommt,kmesh1,kprob1,intmt1)
         if (iluprec.eq.1) then
            intmt1(1) = 6 + 100*iluprec
         endif
      endif
      call commat(2,kmesh2,kprob2,intmt2)
      write(6,*) 'kamat: ',intmt1(5),intmt2(5)
     
c     *** Set logicals for viscosity law
      jtypv=abs(itypv)
      if (jtypv.gt.2) then
          write(6,*) 'PERROR(bmstart): itypv>2'
          call instop
      endif
      if (jtypv.eq.1.or.jtypv.eq.3) ivl=.true.
      if (jtypv.eq.2) ivt=.true.

c     *** See if matrix is too large for current size of IBUFFR
      nsup1  = kprob1(29)
      nsup2  = kprob2(29)
      npoint = kmesh1(8)
      if (nsup1.ne.0.or.nsup2.ne.0) then
	 write(6,*) 'NSUPER: ',nsup1,nsup2
	 stop
      endif
      if (npoint.gt.num) then
	 write(6,*) 'PERROR(bmstart): Number of points too large '
	 write(6,*) 'for user arrays. Npoint = ',npoint
         stop
      endif

c     
      if (irestart.gt.0) then
c        *** Read from file
	 namef2 = 'restart.back'
	 call openf2(.false.)
         call readbs(1,isol1,kprob1)
	 call readbs(2,isol2,kprob2)
      else
c        *** Define temperature field with func(ichois=1).
	 iu1(1) = 1
	 call creavc(0,1,idum,isol2,kmesh2,kprob2,iu1,u1,iu2,u2)
c        *** Make velocity 0
	 iu1(1) = 0
	 iu1(2) = 0
	 call creavc(0,1,idum,isol1,kmesh1,kprob1,iu1,u1,iu2,u2)
      endif
      iu1(1) = 0
      iu1(2) = 0
      call creavc(0,1,idum,islol1(1,1),kmesh1,kprob1,iu1,u1,iu2,u2)
      call creavc(0,1,idum,islol1(1,2),kmesh1,kprob1,iu1,u1,iu2,u2) 

c     *** Prescribe boundary conditions
      call presdf(kmesh1,kprob1,isol1)
      call presdf(kmesh2,kprob2,isol2)

      t0 = second()

      return
      end

