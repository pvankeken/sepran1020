      subroutine msh000 ( kmesh, iinput, irenum, ichois, intarmsh,
     +                    ipointsurf )
! ======================================================================
!
!        programmer    Guus Segal
!        version  8.15 date 23-09-2009 Extension with maxratio
!        version  8.14 date 20-10-2008 Allow isurf = 24
!        version  8.13 date 30-03-2007 Use mshfillshape1
!        version  8.12 date 15-11-2006 itrans = 7
!        version  8.11 date 29-06-2006 Initialize iinp15
!        version  8.10 date 10-03-2006 Extension with surface interface elems
!
!   copyright (c) 1982-2009  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     fill some parameters from array iinput
!     If the length of arrays in this subroutine is changed, adapt all
!     calling subroutines, including pipelayers !!!
! **********************************************************************
!
!                       KEYWORDS
!
!     mesh
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cplot'
      include 'SPcommon/cplaf'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iinput(*), kmesh(*), irenum(*), ichois, intarmsh(*),
     +        ipointsurf(*)

!     ichois         i    Choise parameter JCHOIS in subroutine MESH
!                         Possible values
!                          0 standard method,  input from standard input file
!                            the mesh is created for the first time
!                          1 input is read from the arrays iinput and rinput
!                            the mesh is created for the first time
!                          2 input is read from the standard input file and
!                            stored in the arrays iinput and rinput
!                            the mesh is created for the first time
!                          3 input is read from the arrays iinput and rinput
!                            the co-ordinates of the existing mesh are changed
!                            the topological description remains unchanged
!                          4 input is read from the arrays iinput and rinput.
!                            the coordinates and the topological description of
!                            the mesh are changed, however the input as stored
!                            in iinput and rinput is the same as in a preceding
!                            call of mesh.
!                            The number of points and curves remains the same,
!                            as well as the number of elements along the curves.
!                            The coordinates of the user points and the curves
!                            are changed by a call of subroutine chanbn and as a
!                            consequence the number elements in the surfaces may
!                            be changed.
!                            This possibility must be preceded by calls of
!                            subroutine mesh and subroutine chanbn
!                          5 See jchois = 4, however, the topological
!                            description remains unchanged
!                          6 Only the arrays iinput and rinput are filled
!                            The subroutine is returned without actually
!                            creating a mesh
!                          7 The input must be stored in the arrays IINPUT and
!                            RINPUT
!                            The subroutine creates the curves and stores the
!                            information of curves and user points via KMESH
!                            After this step the subroutine returns
!                            This possibility is meant for program SEPGEOM
!                         10 The boundary of the mesh has been changed by
!                            subroutine ADAPBN. Information with respect to this
!                            change has been stored in the arrays ICURVS,
!                            CURVES,IINPUT and RINPUT, which are all part of
!                            array KMESH
!                            The input/output parameters IINPUT and RINPUT are
!                            not used.
!                            The topological description of the mesh may be
!                            changed
!                         11 See ICHOIS=10, however, the topological description
!                            may not be changed
!     iinput         i    Standard integer input array for subroutine MESH
!     intarmsh      i/o   General array to store constants for subroutine MESH
!                         For a description, see subroutine MESH
!     ipointsurf          Array containing the relative starting addresses
!                         of all surfaces in the part of iinput referring
!                         to surfaces
!     irenum         o    Array of length 10 with information about the
!                         renumbering.
!                         Array must be filled as follows:
!                          1: if>=0, only position 1 is filled.
!                             This position contains the parameter method
!                             according to the old SEPRAN definition, i.e.
!                             jrenum = 9: Use default depending on mesh
!                             else:
!                             jrenum = ispecial*100 + method + 3 * ipref
!                             ispecial = 0: standard
!                                        1: ivertx=1, imidp=1, icentr=3
!                             If<0, -jrenum(1) contains the sequence number
!                             of the last entry filled by the user
!                             All other values are filled with defaults
!                          2: method Indication of the renumbering method used
!                                    The algorithms are compared to the original
!                                    numbering:
!                                    0  No renumbering
!                                    1  Cuthill-Mckee algorithm
!                                    2  Sloan algorithm
!                                    3  Both algorithms,  the best is used
!                                    9  Choose depending on mesh
!                             Default: 9
!                          3: ipref  Indication of the criterion to be used
!                                    to find the best algorithm:
!                                    0  Choose the smallest profile
!                                    1  Choose the smallest band width
!                                    2  Use only the renumbered algorithm
!                                       (method=1 or 2)
!                             Default: 0
!                          4: iprint Indicates if information about the
!                                    renumbering must always be printed
!                                    (1) or only depending on ioutp (0)
!                             Default: 0
!                          5: ivertx Priority for vertex nodes (1-3)
!                             Default: 3
!                          6: imidp  Priority for mid point nodes (1-3)
!                             Default: 3
!                          7: icentr Priority for centroids (1-3)
!                             Default: 3
!                          8: ilevel Indicates if renumbering of specia nodes
!                                    must be performed per level (1) or not (0)
!                             Default: 0
!                          9: istmet Indicates how the start of the renumbering
!                                    algorithm must be performed
!                                    0  Automatic
!                                    1  Start user point given
!                                    2  Start curve given
!                                    3  Start surface given
!                             Default: 0
!                         10: istval Value of start point, curve f surface
!                             Default: 1
!     kmesh         i/o   Standard SEPRAN array containing information of the
!                         mesh
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer jmaxin, i, icurve, ndefsf, iprec, kinp, k, is,
     +        iinp, ifirst, ilast, istart, ndefvl,
     +        length, ndim, nuspnt, ncurvs, nsurfs, nvolms, iplot,
     +        jrenum, iinp9, nspec, iinp8, maxpnt, maxcrv, maxsrf,
     +        maxvlm, iinp10, irefin, itrans, icheck, numsub, iorien,
     +        nsurel, nvolel, irnp1, irnp2, jcoars, irnp3, iinp1,
     +        iinp3, iinp5, isurf, ishape, iinp7, ivol, iinp2, iinpsav,
     +        iinp4, iinp6, nsurvl, nelgrp, nlines, notopol, iinp11,
     +        maxlencur, maxlensur, maxlenvol, irnp4, nintprop,
     +        nrealprop, iinp12, irnp5, iinp13, iinp14, numobst,
     +        nchancoor, iwritfaces, itype_scale, iscale, iparallel,
     +        iinp15, nelgrpdum
      logical part4, debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     i              Counting variable
!     icheck         Level of check of elements
!                    0:  no special checks (default)
!                    1:  check volume of elements
!                    2:  check volume of elements and print minimum
!                        and maximum value per surface/volume
!     icurve         Type of curve
!     ifirst         First curve (surface) number read
!     iinp           last position used in array iinput
!     iinp1          first pos. iinput definition curves
!     iinp10         first pos. iinput extra information
!     iinp11         first pos. iinput interface elements
!     iinp12         first pos. iinput with respect to properties
!     iinp13         first pos. iinput with respect to change_coordinates
!     iinp14         first pos. iinput with respect to obstacles
!     iinp15         first pos. iinput dummy element groups
!     iinp2          first pos. iinput definition line elements
!     iinp3          first pos. iinput definition surfaces
!     iinp4          first pos. iinput definition surface elements
!     iinp5          first pos. iinput definition volumes
!     iinp6          first pos. iinput definition volume elements
!     iinp7          first pos. iinput definition connected elements
!     iinp8          last position of iinput
!     iinp9          first pos. iinput definition intermediate points
!     iinpsav        Help variable to save the value of iinpx
!     ilast          Last curve (surface) number read
!     iorien         Defines orientation of the base vectors in 3D plots
!                    1 x-y-z
!                    2 z-x-y
!                    3 y-z-x
!     iparallel      Indicates if the mesh must be subdivided for parallel
!                    processing or not.
!                    Possible values:
!                    0:  no parallelization
!                    1:  subdivide because of parallelization
!     iplot          paramater iplot,  see programmer's guide
!                    (plotting: >0,  no plots  = 0)
!     iprec          Surface number of surface to be copied etc.
!     irefin         Number of times that the mesh should be refined
!     irnp1          first pos. rinput definition of coordinates of points
!     irnp2          first pos. in rinput definition of factors
!     irnp3          last position in rinput
!     irnp4          first pos. in rinput definition of factors for paver
!     irnp5          first pos. in rinput corresponds to real properties
!     is             ?
!     iscale         Indicates the type of scaling that must be applied
!                    Possible values:
!                    0: no scaling
!                    1: always scaling
!                    2: scaling for specified sufraces and volumes
!     ishape         Shape number of elements
!     istart         Starting value - 1 of plot information in IINPUT
!     isurf          Type of surface with respect to mesh generator
!     itrans         Indicates if the linear triangular mesh must
!                    be transformed to elements of type itrans (>0) or not (0)
!     itype_scale    Indicates how the scaling must be performed
!                    Possible values:
!                    0: scaling in x, y and z directions
!                    1: scaling in x-direction only
!                    2: scaling in y-direction only
!                    3: scaling in z-direction only
!                    4: scaling depends on direction of mesh and size of
!                       boundary elements
!     ivol           Defines type of volume generator
!     iwritfaces     If 1 also the edges and faces must be written to the file
!                    meshoutput
!     jcoars         If 1 coarseness is given, if 0 not
!     jmaxin         if>0 its value indicates where the maximum numbers of
!                    points, curves, surfaces and volumes are stored in iinput
!     jrenum         Parameter jrenum for TOPOL
!     k              Counting variable
!     kinp           ?
!     length         Length of part
!     maxcrv         Maximal number of curves allowed
!     maxlencur      Maximum length of information for one curve
!     maxlensur      Maximum length of information for one surface
!     maxlenvol      Maximum length of information for one volume
!     maxpnt         Maximal number of nodal points allowed
!     maxsrf         Maximal number of surfaces allowed
!     maxvlm         Maximal number of volumes allowed
!     nchancoor      Number of parts with change coordinates
!     ncurvs         Number of curves in mesh
!     ndefsf         Number of curves in surface
!     ndefvl         Number of surfaces in volume
!     ndim           Dimension of the space.
!     nelgrp         Number of element groups
!     nelgrpdum      Number of dummy element groups
!     nintprop       Number of integer properties per element group
!     nlines         number of parts where line elements are created
!     notopol        If 1 the topology is skipped
!     nrealprop      Number of real properties per element group
!     nspec          Number of points at the boundary of a spectral element
!     nsurel         number of parts where surface elements are defined
!     nsurfs         Number of surfaces in mesh
!     nsurvl         total number submeshes where surface elements
!                    and volume elements are defined
!     numobst        Number of obstacles
!     numsub         Number of submeshes to be skipped in plotting
!     nuspnt         Number of user points
!     nvolel         number of parts where volume elements are defined
!     nvolms         number of volumes
!     part4          If true rinput part 4 is used
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     MSH046         Raise iinp as function of icurve
!     MSH049         Compute starting address of specific surface in array
!                    IINPUT
!     MSH050         Raise iinp as function of ivolm
!     MSHFILLSHAPE1  Fill shape numbers of a surface that has not been
!                    filled yet
!     PRININ         print 1d integer array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!      15   jmark has in correct value
!     119   The number of curves is incorrect
!     120   The number of nodal points or the nodal point number read is
!           incorrect
!     121   number of line elements<=0 whereas ndim = 1
!     150   The number of surfaces is incorrect
!     154   The number of volumes is incorrect
!     298   code for generation of volumes incorrect
!     379   Incorrect type of surface generator found
!    1001   Parameter notopol out of range
!    1240   The number of times the mesh should be refined is out of range
!    1328   The type number read after transform has incorrect value
!    1947   TRANSF or refine not permitted for changing mesh
!    2147   Parameter out of range
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'msh000' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from msh000'
         call prinin ( iinput, 21, 'iinput' )
  1      format ( a, 1x, (10i6) )

      end if

      part4 = .false.
      ndim = iinput(1)
      nuspnt = iinput(2)
      ncurvs = iinput(3)
      nsurfs = iinput(4)
      nvolms = iinput(5)
      iplot  = iinput(9)
      jrenum = iinput(10)
      if ( debug ) then
         write(irefwr,1) 'ndim, nuspnt, ncurvs, nsurfs, nvolms',
     +                    ndim, nuspnt, ncurvs, nsurfs, nvolms
         write(irefwr,1) 'iplot, jrenum', iplot, jrenum
      end if  ! ( debug )

      if ( jrenum>=0 ) then
         irenum(1) = jrenum
      else
         irenum(1) = -10
         do i = 1, 9
            irenum(1+i) = iinput(-iinput(10)-6+i)
         end do
      end if
      jmaxin = iinput(13)-5
      if ( iinput(14)==0 ) then
          iinp9 = 0
          nspec = 0
      else
          iinp9 = iinput(14)-5
          nspec = 1
      end if
      iinp8 = iinp9
      if ( jmaxin>0 ) then
         maxpnt = iinput(jmaxin)
         maxcrv = iinput(jmaxin+1)
         maxsrf = iinput(jmaxin+2)
         maxvlm = iinput(jmaxin+3)
         iinp8 = max(iinp8,jmaxin+3)
      else
         maxpnt = 5000
         maxcrv = 1000
         maxsrf = 1000
         maxvlm = 500
      end if

      if ( debug ) then
         write(irefwr,1) 'maxpnt, maxcrv, maxsrf, maxvlm',
     +                    maxpnt, maxcrv, maxsrf, maxvlm
         write(irefwr,1) 'iinp8, iinp9',iinp8, iinp9
      end if  ! ( debug )

      iinp10 = 0
      iinp11 = 0
      irefin = 0
      icheck = 0
      itrans = 0
      notopol = 0
      maxlencur = 9
      maxlensur =10
      maxlenvol = 0
      iinp12 = 0
      iinp13 = 0
      iinp14 = 0
      iinp15 = 0
      irnp5 = 0
      nintprop = 0
      nrealprop = 0
      iwritfaces = 0
      itype_scale = 0
      iscale = 0
      iparallel = 0
      nelgrpdum = 0
      if ( iinput(15)>0 ) then

!     --- iinput(15)>0, hence extra information

         iinp10 = iinput(15)-5
         irefin = iinput(iinp10)
         itrans = iinput(iinp10+1)
         icheck = iinput(iinp10+2)
         notopol = iinput(iinp10+3)
         iinp11 = max(0,iinput(iinp10+4)-5)
         iinp12 = max(0,iinput(iinp10+5)-3)
         iinp13 = max(0,iinput(iinp10+6)-5)
         iinp14 = max(0,iinput(iinp10+7)-5)
         iwritfaces = iinput(iinp10+8)
         iscale = iinput(iinp10+9)
         itype_scale = iinput(iinp10+10)
         iparallel = iinput(iinp10+11)
         iinp15 = max(0,iinput(iinp10+12)-5)

         if ( debug ) then

!        --- Debug information

            write(irefwr,*) 'iinp10, irefin, itrans, icheck, notopol ',
     +                       iinp10, irefin, itrans, icheck, notopol
            write(irefwr,*) 'iinp11, iinp12, iinp13, iinp14 ',
     +                       iinp11, iinp12, iinp13, iinp14
            write(irefwr,*) 'iwritfaces, iscale, itype_scale ',
     +                       iwritfaces, iscale, itype_scale
            write(irefwr,*) 'iparallel, iinp15 ',
     +                       iparallel, iinp15

         end if

         if ( iscale<0 .or. iscale>1 ) then
            call errchr ( 'iscale', 1 )
            call errint ( iscale, 1 )
            call errint ( 0, 2 )
            call errint ( 1, 3 )
            call errsub ( 2147, 3, 0, 1 )
         end if
         if ( itype_scale<0 .or. itype_scale>0 ) then
            call errchr ( 'itype_scale', 1 )
            call errint ( itype_scale, 1 )
            call errint ( 0, 2 )
            call errint ( 1, 3 )
            call errsub ( 2147, 3, 0, 1 )
         end if
         if ( iparallel<0 .or. mod(iparallel,100)>5 .or.
     +        iparallel/100>1024 ) then
            call errchr ( 'iparallel', 1 )
            call errint ( iparallel, 1 )
            call errint ( 0, 2 )
            call errint ( 5, 3 )
            call errsub ( 2147, 3, 0, 1 )
         end if
         iinp8 = max(iinp8,iinp10+19)
         if ( irefin<0 .or. irefin>=100 ) then
            call errint ( irefin, 1 )
            call errsub ( 1240, 1, 0, 0 )
         end if
         if ( itrans/=0 .and. (itrans<4 .or. itrans>7) ) then
            call errint ( itrans, 1 )
            call errsub ( 1328, 1, 0, 0 )
         end if
         if ( notopol<0 .or. notopol>1 ) then
            call errint ( notopol, 1 )
            call errsub ( 1001, 1, 0, 0 )
         end if
         if ( debug ) write(irefwr,1) 'iinp11, iinp8', iinp11, iinp8
         if ( iinp11>0 ) then

!        --- interface elements found

            iinp8 = iinp11+4*(iinput(iinp11+1)+iinput(iinp11+2))
            if ( debug ) then
               write(irefwr,1) 'iinp8', iinp8
               write(irefwr,1) 'interface_curves, interfaces_surfs',
     +                          iinput(iinp11+1), iinput(iinp11+2)
            end if  ! ( debug )

         end if
         if ( iinp12>0 ) then

!        --- properties found

            nintprop = iinput(iinp12-2)
            nrealprop = iinput(iinp12-1)

         end if
         if ( iinp15>0 ) then

!        --- dummy elements found

            nelgrpdum = iinput(iinp15)
            iinp8 = iinp15+2*nelgrpdum

         end if

      end if

      if ( iplot>0 ) then

!     --- iplot>0, old situation

         numsub = iplot/10
         jmark = iplot-10*numsub-1
         iorien = 1

      else if ( iplot<0 ) then

!     --- iplot<0, new situation

         istart = -iinput(9)-6
         jmark  = iinput(istart+1)
         numsub = iinput(istart+2)
         intarmsh(88) = iinput(istart+3)
         intarmsh(89) = iinput(istart+4)
         intarmsh(90) = iinput(istart+5)
         dcolor = iinput(istart+6)
         intarmsh(99) = iinput(istart+7)
         intarmsh(108) = iinput(istart+8)
         intarmsh(109) = iinput(istart+9)
         iorien = iinput(istart+10)
         iinp8 = max(iinp8,istart+10)
         if ( debug ) then

!        --- Debug information

            write(irefwr,1) 'istart, jmark, numsub, dcolor',
     +                       istart, jmark, numsub, dcolor
            write(irefwr,1) 'iorien, iinp8', iorien, iinp8
            call prinin ( iinput(istart), 11, 'iinput_plot' )

         end if  ! ( debug )

      else

!     --- iplot = 0

         numsub = 0
         jmark  = 0
         iorien = 0

      end if
      if ( ichois>=3 .and. ichois<=5 .or. ichois>=10 ) then

!     --- mesh must be changed

         intarmsh(108) = 0
         iplot = 0
         if ( itrans/=0 .or. irefin/=0 ) then

!        --- Refine not implemented

            call errsub ( 1947, 0, 0, 0 )

         end if

      end if
      if ( jmark==-1) jmark = 0
      if ( jmark>5 ) then
         call errint ( jmark, 1 )
         call errint ( 5, 2 )
         call errsub ( 15, 2, 0, 0 )
      end if
      if ( ncurvs<=0 .or. ncurvs>maxcrv ) then
         call errint ( ncurvs, 1 )
         call errint ( maxcrv, 2 )
         call errsub ( 119, 2, 0, 0 )
      end if
      if ( nuspnt<=0 .or. nuspnt>maxpnt ) then
         call errint ( nuspnt, 1 )
         call errint ( maxpnt, 2 )
         call errsub ( 120, 2, 0, 0 )
      end if
      if ( nsurfs<0 .or. nsurfs>maxsrf ) then
         call errint ( nsurfs, 1 )
         call errint ( maxsrf, 2 )
         call errsub ( 150, 2, 0, 0 )
      end if
      if ( nvolms<0 .or. nvolms>maxvlm ) then
         call errint ( nvolms, 1 )
         call errint ( maxvlm, 2 )
         call errsub ( 154, 2, 0, 0 )
      end if
      if ( ierror/=0 ) go to 1000

      kmesh(6) = ndim
      kmesh(10) = nuspnt
      kmesh(11) = ncurvs
      kmesh(12) = nsurfs
      kmesh(13) = nvolms

      nlines = iinput(6)
      nsurel = iinput(7)
      nvolel = iinput(8)
      jcoars = intarmsh(44)
      irnp1 = 3
      irnp2 = (ndim+jcoars)*nuspnt+2*jcoars+irnp1
      irnp3 = irnp2+3*ncurvs-1+4*nsurfs
      irnp4 = irnp3+1
      if ( ndim==3 .and. intarmsh(99)/=0 ) irnp3 = irnp3+3
      if ( debug ) then
         write(irefwr,1) 'ndim, nuspnt, ncurvs, nsurfs, nvolms',
     +                    ndim, nuspnt, ncurvs, nsurfs, nvolms
         write(irefwr,1) 'nlines, nsurel, nvolel, jcoars',
     +                    nlines, nsurel, nvolel, jcoars
      end if  ! ( debug )

!     --- When these starting positions are changed, also consider chanbn!!!!

      iinp1 = 16
      iinp3 = iinp1
      do i = 1,ncurvs
         icurve = iinput(iinp3)
         iinpsav = iinp3
         call msh046(iinput,iinp3,icurve)
         maxlencur = max ( maxlencur, iinp3-iinpsav )
      end do
      if ( debug ) write(irefwr,1) 'iinp1, iinp3', iinp1, iinp3

      iinp5 = iinp3

!     --- surface elements

      do i = 1, nsurfs
         ipointsurf(i) = iinp5
         isurf = iinput(iinp5)
         iinpsav = iinp5
         call msh049 ( iinput, iinp5, isurf )
         maxlensur = max ( maxlensur, iinp5-iinpsav )
      end do
      iinpsav = iinp5

      if ( debug ) then
         write(irefwr,1) 'iinp5sav', iinpsav
         call prinin ( ipointsurf, nsurfs, 'ipointsurf' )
      end if  ! ( debug )

      do i = 1, nsurfs
         iinp5 = ipointsurf(i)
         isurf = iinput(iinp5)

         if ( isurf<0 .or. isurf==6 .or. isurf>24 ) then

!        --- Error 379:  Type of surface incorrect

            call errint ( isurf, 1 )
            call errint ( i, 2 )
            call errsub ( 379, 2, 0, 0)
            go to 1000

         end if

         if ( isurf==0 ) then
            go to 400
         else if ( isurf==19 ) then
            part4 = .true.
         end if
         ishape = iinput(iinp5+1)
         if ( debug )
     +      write(irefwr,1) 'iinp5, isurf, ishape', iinp5, isurf, ishape

         if ( ishape==0 ) then

!        --- ishape = 0   either error or ishape must be copied

            call  mshfillshape1 ( iinput, ipointsurf, isurf, iinp5, i )

         end if

400   end do
      iinp5 = iinpsav

      iinp7 = iinp5
      if ( debug ) write(irefwr,1) 'iinp5', iinp5

!     --- volume elements

      do i = 1, nvolms

         ivol = iinput(iinp7)
         if ( ivol==0 ) then
            iinp7 = iinp7 + 1
            go to 500
         end if
         if ( debug ) write(irefwr,1) 'i, ivol, iinp7', i, ivol, iinp7

         if ( ivol<0 .or. ivol>11 ) then
            call errint ( ivol, 1 )
            call errsub ( 298, 1, 0, 0 )
            go to 1000
         end if

         if ( ivol>5 .and. ivol<9 ) then

!        --- Place shape number by copying from volume to be
!            translated, rotated or reflected

            ndefvl = iinput(iinp7+2)
            iprec  = iinput(iinp7+3+ndefvl)
            kinp   = iinp5
            do k = 1, iprec-1
                is = iinput(kinp)
                call msh050(iinput, kinp, is)
            end do
            ishape = iinput( kinp+1 )
            iinput(iinp7+1) = ishape

         end if

         iinpsav = iinp7
         call msh050 ( iinput, iinp7, ivol )
         maxlenvol = max ( maxlenvol, iinp7-iinpsav )

500   end do

      iinp2 = iinp7
      iinp4 = iinp2+4*nlines
      iinp6 = iinp4+4*nsurel
      iinp7 = iinp6+4*nvolel
      if ( debug )
     +   write(irefwr,1) 'iinp2, iinp4, iinp6, iinp7',
     +                    iinp2, iinp4, iinp6, iinp7

      if ( nlines<=0 .and. nsurel<=0 .and. nvolel<=0 ) call
     +     errsub ( 121, 0, 0, 0 )

      iinp8 = max(iinp8,iinp7-1)
      if ( iinput(12)/=0 ) then

!     --- Connected elements found

         length = iinp7-1 + 3*iinput(iinp7) + 4*iinput(iinp7+1)
         ndefsf = iinput(iinp7+2)
         do i = 1, ndefsf
            length = length + 5 + max(iinput(length+5),0)
         end do
         iinp8 = max(iinp8,length)

      end if

      iinp8 = max ( iinp10+1, iinp8 )
      if ( iinput(9)<0 ) iinp8 = max ( iinp8, -iinput(9) )
      if ( iinput(14)>0 ) iinp8 = max ( iinp8, iinput(14)-1 )

      nsurvl = 0
      iinp = iinp4
      do i = 1,nsurel
         ifirst = iinput(iinp+2)
         ilast = iinput(iinp+3)
         nsurvl = nsurvl+ilast-ifirst+1
         iinp = iinp+4
      end do
      iinp = iinp6
      do i = 1,nvolel
         ifirst = iinput(iinp+2)
         ilast = iinput(iinp+3)
         nsurvl = nsurvl+ilast-ifirst+1
         iinp = iinp+4
      end do

!     --- Compute nelgrp for standard elements

      if ( nvolel>0 ) then

!     --- nvolel>0

         nelgrp = iinput(iinp6+1+(nvolel-1)*4)

      else if ( nsurel>0 ) then

!     --- nsurel>0, nvolel = 0

         nelgrp = iinput(iinp4+1+(nsurel-1)*4)

      else

!     --- nlines>0, nvolel = nsurel = 0

         nelgrp = iinput(iinp2+1+(nlines-1)*4)

      end if
      nelgrp = nelgrp+nelgrpdum

      if ( jmaxin>0 ) iinp8 = max ( iinp8, jmaxin+3 )
      if ( part4 ) irnp3 = irnp3+4*ncurvs
      if ( iinp12>0 ) then

!     --- properties found

         nintprop = iinput(iinp12-2)
         iinp8 = max(iinp8,iinp12+nintprop*nelgrp)
         nrealprop = iinput(iinp12-1)
         irnp5 = irnp3+1
         irnp3 = irnp3+nrealprop*nelgrp

      end if
      if ( iinp13>0 ) then

!     --- change_coordinates found

         nchancoor = iinput(iinp13)
         iinp8 = max(iinp8,iinp13+nchancoor*4)

      end if
      if ( iinp14>0 ) then

!     --- obstacles found

         numobst = iinput(iinp14)
         kmesh(42) = numobst
         iinp8 = max(iinp8,iinp14+numobst*2)

      end if

      intarmsh(1) = ndim
      intarmsh(2) = nuspnt
      intarmsh(3) = ncurvs
      intarmsh(4) = nsurfs
      intarmsh(5) = nvolms
      intarmsh(6) = iplot
      intarmsh(7) = jrenum
      intarmsh(8) = numsub
      intarmsh(9) = iinp1
      intarmsh(10) = iinp2
      intarmsh(11) = iinp3
      intarmsh(12) = iinp4
      intarmsh(13) = iinp5
      intarmsh(14) = iinp6
      intarmsh(15) = iinp7
      intarmsh(16) = iinp8
      intarmsh(17) = iinp9
      intarmsh(18) = iinp10
      intarmsh(19) = iinp11
      intarmsh(20) = iinp12
      intarmsh(21) = iinp13
      intarmsh(22) = iinp14
      intarmsh(23) = iinp15
      intarmsh(26) = irnp1
      intarmsh(27) = irnp2
      intarmsh(28) = irnp3
      if ( part4 ) then
         intarmsh(29) = irnp4
      else
         intarmsh(29) = 0
      end if
      intarmsh(30) = irnp5
      intarmsh(38) = iwritfaces
      intarmsh(41) = nlines
      intarmsh(42) = nsurel
      intarmsh(43) = nvolel
      intarmsh(45) = nsurvl
      intarmsh(46) = nelgrp
      intarmsh(81) = maxpnt
      intarmsh(82) = maxcrv
      intarmsh(83) = maxsrf
      intarmsh(84) = maxvlm
      intarmsh(85) = nspec
      intarmsh(100) = notopol
      intarmsh(101) = irefin
      intarmsh(102) = itrans
      intarmsh(107) = icheck
      intarmsh(110) = iorien
      intarmsh(113) = maxlencur
      intarmsh(114) = maxlensur
      intarmsh(115) = maxlenvol
      intarmsh(116) = nintprop
      intarmsh(117) = nrealprop
      intarmsh(121) = iscale
      intarmsh(122) = itype_scale
      intarmsh(123) = iparallel
      intarmsh(124) = nelgrpdum

1000  call erclos ( 'msh000' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,1) 'iinp1, iinp2, iinp3, iinp4, iinp5',
     +                    iinp1, iinp2, iinp3, iinp4, iinp5
         write(irefwr,1) 'iinp6, iinp7, iinp8, iinp9, iinp10',
     +                    iinp6, iinp7, iinp8, iinp9, iinp10
         write(irefwr,1) 'iinp11, iinp12, iinp13, iinp14, iinp15',
     +                    iinp11, iinp12, iinp13, iinp14, iinp15
         write(irefwr,1) 'irnp1, irnp2, irnp3, irnp4, irnp5',
     +                    irnp1, irnp2, irnp3, irnp4, irnp5
         call prinin ( iinput, iinp8, 'iinput' )
         write(irefwr,*) 'End msh000'

      end if

      end
