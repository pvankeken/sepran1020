c *   *** bmiter.inc
c *   *** Information on iteration process  
c *   *** eps  : required relative accuracy
c *   *** relax: relaxation parameter
c *   *** dif1, dif2, dif: difference in solution with previous
c *   ***       iteration (Stokes, heat, maximum)
c *   *** nitermax: maximum number of iterations
c *   *** niter   : current iteration number
      real*8 eps,relax,dif1,dif2,dif
      integer nitermax,niter
      common /bmiter/ eps,relax,dif1,dif2,dif,nitermax,niter
