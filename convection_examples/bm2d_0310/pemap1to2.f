c *************************************************************
c *   PEMAP1TO2
c *
c *   Subroutine that calculates mapping from a mesh defined
c *   with quadratic(ISHAPE=6) or linear (ISHAPE=3) triangles
c *   to a mesh defined with extended quadratic (ISHAPE=7) triangles.
c *   Necessary in the transport of coefficients from the Stokes 
c *   equation solved by the integrated method (element 902) and
c *   the heat equation solved by linear/quadratic elements 
c *   (element 800).
c * 
c *   KMESH1  Reference array of type KMESH, contains extended mesh
c *           for element 902
c *   KMESHdum  Reference array of type KMESH, contains 'normal' mesh
c *           for element 900 based on quadratic elements
c *   IPEMAP  Reference array of type PvK.
c *           (1) = declared length of ipemap
c *           (2) = -99 to indicate type PvK
c *           (3) = iipemap: pointer to iinfor for actual information
c *           (4) = length of actual information (length NPOINT1)
c *           (5) = irpemap: pointer to iinfor for actual real 
c *                 information
c *           ibuffr(infor(1,iipemap)) contains:
c *               For each nodal point in KMESH1 sequentially:
c *                  if >1 it indicates the corresponding nodal point
c *                        in KMESH2
c *                  if <1 it indicates its sequence number in
c *                        the array of interpolated values
c *           ibuffr(infor(1,irpemap) contains:
c *               For each barycenter the interpolated temperature
c *               Can be calculated from isol2 with subroutine 
c *               pemapinterpolate
c * 
c *   PvK 101999
c *************************************************************
      subroutine pemap1to2(kmesh1,kmeshd,ipemap)
      implicit none
      integer kmesh1(*),kmeshd(*),ipemap(*)
      integer npoint,iipemap,ikelmc1,ikelmcd,nelem1,nelem2,i
      integer irpemap,inumber
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      integer iniget

      if (ipemap(1).lt.5) then
         write(6,*) 'PERROR(pemap1to2): ipemap array is too short'
         write(6,*) 'This array should be declared with at least'
         write(6,*) 5,' integers.'
         call instop
      endif
      if (kmesh1(2).ne.100) then
         write(6,*) 'PERROR(pemap1to2): kmesh1 is not of type kmesh' 
         call instop
      endif
      if (kmeshd(2).ne.100) then
         write(6,*) 'PERROR(pemap1to2): kmeshd is not of type kmesh' 
         call instop
      endif

      npoint = kmesh1(8)
      nelem1 = kmesh1(9)
      nelem2 = kmeshd(9)

      if (nelem1.ne.nelem2) then
         write(6,*) 'PERROR(pemap1to2): meshes are not compatible'
         write(6,*) '  nelem1 = ',nelem1
         write(6,*) '  nelemd = ',nelem2
         call instop
      endif
 
      ipemap(2) = -99
      call ini091(ipemap(3),npoint,'map1to2')
      write(6,*) 'PWARN(pemap1to2): create reference array'
      write(6,*) 'of length: ',npoint, 'reference no: ',ipemap(3)
      ipemap(4) = npoint
      iipemap = iniget(ipemap(3))

c     *** Activate KMESH part C
      call ini050(kmesh1(17),'pemap1to2: kmesh1(17)')
      call ini050(kmeshd(17),'pemap1to2: kmeshd(17)')
      ikelmc1 = iniget(kmesh1(17))
      ikelmcd = iniget(kmeshd(17))
   
      call pemap001(ibuffr(ikelmc1),ibuffr(ikelmcd),ibuffr(iipemap), 
     v              nelem1,inumber) 
c     *** create space for interpolation info
      call ini090(irpemap,inumber,'map1to2')
      ipemap(5) = irpemap
      write(6,*) 'PWARN(pemap1to2): create interpolation array'
      write(6,*) 'of length: ',inumber, 'reference no: ',irpemap
c >>>>
c     do i=1,npoint
c        write(6,*) i,ibuffr(iipemap+i-1)
c     enddo
c     stop
c >>>>

      return
      end

      subroutine pemap001(kmeshc1,kmeshcd,map,nelem,inumber)
      implicit none
      integer kmeshc1(*),kmeshcd(*),map(*),nelem,inumber
      integer ielem,i,j,nodno(6),ip1,ip2,i1
     
      ip1 = 0
      ip2 = 0
      inumber = -1
      do ielem=1,nelem
         do i=1,6
            i1 = kmeshc1(ip1+i)
            map(i1) = kmeshcd(ip2+i)
         enddo
         i1 = kmeshc1(ip1+7) 
c        write(6,'(i5,'': '',7i5)') ielem,(kmeshc1(ip1+i),i=1,7)
         map(i1) = inumber
         inumber = inumber-1
         ip1 = ip1+7
         ip2 = ip2+6
      enddo
      inumber = -inumber-1
      return
      end
