c *   *** peparam.inc
c *   *** specification of some parameters for convection equations
c *   *** rayleigh: thermal rayleigh number
c *   *** rlam    : aspect ratio
c *   *** q       : internal heating rate
c *   *** visc0,b,c: obsolete
      real*8 rayleigh,rlam,q,visc0,b,c
      common /peparam/ rayleigh,rlam,q,visc0,b,c
 
