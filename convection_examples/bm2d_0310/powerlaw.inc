c *   *** Specification of layer dependent creep laws
c *   *** zint   : depth of interfaces
c *   *** spw    : local powerlaw index (well, (n-1)/n if all is well)
c *   *** prefac : local value of pre-exponential factor
c *   *** isoplind: depth dependence or not?
      real*8 zint,spw,prefac
      integer nlay
      logical isoplind
      common /powerlaw/ zint(10),spw(11),prefac(11),nlay,isoplind

