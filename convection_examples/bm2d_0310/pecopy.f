c *******************************************************************
c *   PECOPY
c *
c *   Copy information from a solution vector to a user array or
c *   vice versa.
c *
c *   Adaption of pecopy/er0035 to allow for scaling a solutionvector
c *
c *   PvK 7-11-89
c *
c *   Extension with ichois >= 10:
c *         10   copy first dof 
c *         11   copy second dof
c *         12   copy third dof
c *******************************************************************
      subroutine pecopy(ichois,isol,user,kprob,istart,factor)
      implicit none
      integer ichois,isol(*),kprob(*),istart
      real*8 user(*),factor
      logical new
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      include 'SPcommon/comcons3'
      include 'SPcommon/carray'
      integer iprob,iindprh,iisol,nusol,nunkp,nphys,ikprbp
      integer indprp,inidgt,iniget

      if (ichois.eq.1) then
	 write(6,*) 'PERROR(pecopy): option 1 not available'
         stop
      endif
      iprob=isol(4)/1000+1
      if (iprob.gt.1) then
	 write(6,*) 'PERROR(pecopy): iprob>1'
	 stop
      endif
      call ini050(isol(1),'pecopy: isol')
      iisol   = inidgt(isol(1))
      nusol   = kprob(5)
      nunkp   = kprob(4)
      if (ichois.lt.10) then
        call pecop1(ichois,nusol,buffr(iisol),
     v              user(istart),factor,nunkp)
      else
	  nphys  = kprob(33)
	  nusol  = kprob(5)
	  if (ichois-9.gt.nphys.or.nphys.le.1) then
	     write(6,*) 'PERROR(pecopy): ichois too large'
          endif
	  call ini050(kprob(35),'pecopy: kprob p')
	  ikprbp = iniget(kprob(35))
	  call pecop2(ichois-10,buffr(iisol),ibuffr(ikprbp),
     v                user(istart),factor,nusol,nphys)
       endif
      end

      subroutine pecop2(ichois,usol,kprobp,user,factor,nusol,nphys)
      implicit none
      real*8 usol(*),user(*),factor
      integer kprobp(*),ichois,nusol,nphys
      integer i,npoint,iof

      npoint = nusol/nphys
      do i=1,npoint
	 iof = ichois*npoint + i
	 user(i) = factor*usol(kprobp(iof))
      enddo

      return
      end

      subroutine pecop1(ichois,nusol,usol,user,
     v                  factor,nunkp)
      implicit none
      integer ichois,nusol,nunkp
      real*8 usol(*),user(*),factor
      integer i

      if (factor.eq.0d0) factor=1d0
      if (ichois.eq.0) then
        do i=1,nusol
           user(i)=factor*usol(i)
        enddo
      else if(ichois.eq.2 .or. ichois.eq.3) then
        do i=ichois-1,nusol,2
           user((i+1)/2)=factor*usol(i)
        enddo
      else if (ichois.ge.10) then
        do i=ichois-9,nusol,nunkp
           user((i+nunkp-1)/nunkp)=factor*usol(i)
        enddo
      endif
      end
