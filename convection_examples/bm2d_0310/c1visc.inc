c *   *** c1visc.inc
c *   *** Information on viscosity description
c *   *** viscl, ctd, cpd, nl: unused
c *   *** itypv: type of viscosity (0=iso; 2=T,p)
      real*8 viscl,ctd,cpd
      integer itypv,nl
      common /c1visc/ viscl(11),ctd,cpd,itypv,nl
