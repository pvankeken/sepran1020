      subroutine prpcgrad ( ibuffr, buffer, iu0, iun, matfe, kmesh,
     +                      kprob, intmat, irhsd, iuser, user, iinsol,
     +                      rinsol, inpsol, rinpsol )
! ======================================================================
!
!        programmer    Peter Minev
!        version  3.0  date 11-02-2004 Completely rewritten for parallel
!                                      computing
!        version  2.0  date 30-12-2003 Renamed pcgradbf
!        version  1.0  date 08-11-1999
!
!   copyright (c) 1999-2004  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     finite-element preconditioned conjugate-gradient solver
!
! **********************************************************************
!
!                       KEYWORDS
!
!     conjugate_gradients
!     preconditioning
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cactl'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cinout'
      include 'SPcommon/cconst'
      include 'SPcommon/cbuffr'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer kmesh(*), kprob(*), iuser(*), intmat(*), irhsd(*), iu0(*),
     +        iun(*), matfe(*), ibuffr(*), iinsol(*), inpsol(*)
      double precision rinsol(*), user(*), buffer(*),
     +                 rinpsol(*)

!     buffer        i/o   Real buffer array in which all large data is stored
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     iinsol         i    Contains information concerning the solver
!                         Contents
!                         1: maxiter i.e. maximum number of iterations
!                         2: iprint, defines the amount of output
!                            Possible values:
!                            0: no output
!                            1: the number of iterations is printed
!                            2: the convergence of the iteration process is
!                               showed
!     inpsol         i    Integer input array for subroutine solvel
!                         See solvelbf for a description
!     intmat         i    Contains information about the structure of the
!                         matrix
!     irhsd          i    Contains information about the right-hand-side vector
!     iu0            i    Contain information of the initial approximation
!     iun            o    Contain information of the solution
!     iuser          i    Communication array for the element subroutines
!     kmesh          i    Output of the mesh generator - spectral mesh
!     kprob          i    Contains information of the problem definition
!     matfe          i    Contains information of the finite-element matrix
!     rinpsol        i    Real input array for subroutine solvel
!                         See solvelbf for a description
!     rinsol         i    Contains information concerning the solver
!                         Contents
!                         1: eps, i.e. accuracy with respect to residual
!     user           i    Double precision communication array for the element
!                         subroutines
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer iprob, ipkprb, length,
     +        nbound, npboun, indprh, nrusol, nbufol, iprn, ipzn,
     +        ipvn, ippn, ipkprh, ipkprhcp, iptemp
      double precision timebefore
      logical debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     indprh         indication if kprob part h has been filled (>0)
!                    or not (0)
!     ipkprb         Starting address of array KPROB for actual problem
!                    number - 1
!     ipkprh         starting address of array kprob part h
!     ipkprhcp       starting address of copy of array kprob part h
!     ippn           starting address of array pn
!     iprn           starting address of array rn
!     iprob          Actual problem number
!     iptemp         Starting address of temporary array in IBUFFR
!     ipvn           starting address of array vn
!     ipzn           starting address of array zn
!     length         Help parameter to indicate the length of arrays
!     nbound         Number of essential boundary conditions
!     nbufol         help variable to store the old value of nbuffr
!     npboun         Number of periodical boundary conditions
!     nrusol         Number of free unknowns
!     timebefore     Time at entrance of subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer iniprb, inigettext, inidbl

!     ERCLMN         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERSETTIME      Get the CPU time at start of subroutine
!     INI000         Copy integer array to other integer array possibly itself
!     INI060TEXT     Reactivate existing array in IBUFFR
!     INI069         Reserve space at the end of IBUFFR for a semi-permanent
!                    array
!     INIDBL         Transform pointer for IBUFFR to pointer for BUFFER
!     INIGETTEXT     Compute pointer of array in IBUFFR
!     INIPRB         Get value of ipkprb as function of iprob
!     INIRST         Reset BUFFER to old value after a call to ini079
!     PRPCGRAD1      finite-element preconditioned conjugate-gradient solver
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'prpcgrad' )
      if ( itime.gt.0 ) call ersettime ( timebefore )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from prpcgrad'

      end if
      if ( ierror.ne.0 ) go to 1000

!     --- Create temporary arrays at the end of ibuffr
!         It concerns the arrays R^n, Z^n, V^n, P^n and temp^n
!         They contain the degrees of freedom without boundary conditions
!         Also copy kprobh to end of ibuffr

      iprob = intmat(1)/1000+1
      ipkprb = iniprb(iprob,kprob)
      nusol  = kprob(ipkprb+5)
      nbound = kprob(ipkprb+10)
      npboun = kprob(ipkprb+34)
      indprh = kprob(ipkprb+50)
      nrusol = nusol-nbound-npboun
      length = 5*nrusol*intlen
      ipkprh = 1
      if ( indprh.gt.0 ) then
         call ini060text ( ibuffr, -indprh, 'kprobh' )
         length = length + nusol
      end if
      call ini069 ( ibuffr, ipkprhcp, length, nbufol )
      if (  indprh.gt.0 ) then
         iprn = ipkprhcp+nusol
      else
         iprn = iprn
      end if
      iprn = inidbl(iprn)
      ipzn = iprn+nrusol
      ipvn = ipzn+nrusol
      ippn = ipvn+nrusol
      iptemp  = ippn+nrusol
      if ( indprh.gt.0 ) ipkprh = inigettext ( indprh, 'kprobh' )

!     --- Copy second part of kprobh

      call ini000 ( ibuffr, ibuffr, ipkprh+nusol, ipkprhcp, nusol, 0 )

!     --- Call to subroutine

      call prpcgrad1 ( ibuffr, buffer, iu0, iun, matfe, kmesh,
     +                 kprob, intmat, irhsd, iuser, user, iinsol,
     +                 rinsol, inpsol, rinpsol, buffer(iprn),
     +                 buffer(ipzn), buffer(ipvn), buffer(ippn),
     +                 buffer(iptemp), ibuffr(ipkprhcp) )

!     --- Reset nbuffr, i.e. free end of buffer

      call inirst ( nbufol )

1000  call erclmn ( 'prpcgrad', timebefore )

      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End prpcgrad'

      end if

      end
