      subroutine to0092 ( ibuffr, buffer, isubr, kmesh, kprob, intmat,
     +                    nbuf, iread, lenmesh, lenprob, maxmesh )
! ======================================================================
!
!        programmer    Guus Segal
!        version  5.18 date 07-11-2007 Jump in case of error
!        version  5.17 date 08-10-2007 Extension of lniincommat
!        version  5.16 date 26-09-2007 New call to mshrdparmesh
!        version  5.15 date 13-12-2006 option: read sepcomp.out
!        version  5.14 date 28-11-2006 extension of manag
!        version  5.13 date 25-10-2006 Minor adaptation to suppress output
!        version  5.12 date 20-03-2006 Do not write first part of sepcomp.out
!                                      in case of simalex
!        version  5.11 date 16-01-2006 princons in case of simalex replaced
!        version  5.10 date 12-12-2005 Jump in case of error
!        version  5.9  date 15-04-2005 Decrease priorities
!        version  5.8  date 02-03-2005 New call to readal
!        version  5.7  date 31-01-2005 Remove file 17
!        version  5.6  date 05-01-2005 New call to simsetconst
!        version  5.5  date 20-12-2004 Extra call to simsetconst
!        version  5.4  date 15-11-2004 Debug statements
!        version  5.3  date 29-03-2004 Extension with mshextra
!        version  5.2  date 12-03-2004 Adaptation for simalex
!        version  5.1  date 26-01-2004 New call to readsp
!        version  5.0  date 24-12-2003 Extra parameters
!
!   copyright (c) 1993-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!    Undersubroutine of SEPSTR and SEPSTL
!    The actual tasks of SEPSTR/SEPSTL are carried out
!    The mesh is supposed to be made by program SEPMESH and stored in the
!    file meshoutput
!    The input for subroutine PROBDF is stored in array iinput and written
!    to the file sepcomp.inf
!    The files meshoutput, sepcomp.inf and sepcomp.out are opened,
!    the file meshoutput is closed.
!    The machine dependent reference numbers iref10, iref73 and iref74 get
!    a value
! **********************************************************************
!
!                       KEYWORDS
!
!     read
!     start
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cmachn'
      include 'SPcommon/cmacht'
      include 'SPcommon/cbuffr'
      include 'SPcommon/csepmain'
      include 'SPcommon/cparallel'
      include 'SPcommon/cpackage'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/comcons3'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer lenmesh, lenprob, maxmesh, kmesh(lenmesh,maxmesh),
     +        kprob(lenprob), intmat(5,*), nbuf, isubr, iread, ibuffr(*)
      double precision buffer(*)

!     buffer        i/o   Buffer array
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     intmat         o    Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     iread          i    Indicates if all SEPRAN input until end
!                         of file or END_OF_SEPRAN_INPUT is read (1) or that
!                         only the input as described for SEPSTR is read (0)
!     isubr          i    Indicates the type of calling subroutine
!                         Possible values
!                         1:  SEPSTR, nbuf does not have a value
!                         2:  SEPSTL, nbuf has a value
!                         3:  SEPSTN, nbuf may have a value
!                             All SEPRAN input is read in this subroutine
!                             provided iread = 1
!                         4:  See 3, now called by sepcomp or sepfree
!                         11: simalex
!     kmesh          o    Standard SEPRAN array containing information of the
!                         mesh
!     kprob          o    Standard SEPRAN array, containing information of
!                         the problem
!     lenmesh        i    Declared length of array KMESH (first entry)
!     lenprob        i    Length of array kprob
!     maxmesh        i    Maximum number of meshes allowed
!     nbuf           i    Length of nbuffr as declared by the user.
!                         The user must declare ibuffr himself in the main
!                         program
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer lniinp, lnrinp, lniincommat
      parameter ( lniinp=5000, lnrinp=10, lniincommat=25 )
      integer i, iprob, nprob, iinput(lniinp), iincommt(lniincommat,10),
     +        iref, jstart, iseqnr, ipmetd, idummy, j, ihelp(10),
     +        ishift, lastps, lastps1, manag(lenmanag), nextrecord,
     +        iseqprob, irefsav
      double precision rinput(lnrinp)
      logical check, debug, callcommat
      character (len=100) filename, filename1, name

!     callcommat     If true subroutine commat must be called
!     check          Indication if a file has been opened (true) or not (false)
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     filename       Name of file
!     filename1      Name of file
!     i              General counting variable
!     idummy         Dummy parameter
!     ihelp          Work array to store input from the input file
!     iincommt       Input array for subroutine incommat.
!                    Defines the type of matrix to be used. (See User's Manual)
!                    iincommt is an array of length 10, which means that at most
!                    10 problems may be solved at one time
!     iinput         Output array of subroutine PROBDF. A fixed length of 500
!                    has been declared. The input for subroutine PROBDF is
!                    written to array iinput and this array is written to the
!                    file sepcomp.inf
!                    In this way the output program can call PROBDF again with
!                    the same input
!     ipmetd         Starting address of array JMETOD in IBUFFR
!     iprob          Actual problem number
!     iref           Local reference number
!     irefsav        Help parameter to save the value of iref
!     iseqnr         Dummy parameter
!     iseqprob       Sequence number of problem input
!     ishift         Shift in mesh sequence number
!     j              General counting variable
!     jstart         Absolute value of first parameter in the call of START
!     lastps         Last non-blank position in filename
!     lastps1        Last non-blank position in filename1
!     lniincommat    Length of first index of iincommat
!     lniinp         Length of array iinput
!     lnrinp         Available space for array RINPUT
!     manag          Manager array that sets the parameters in subroutine
!                    SEPSTL
!                    See subroutine readsp
!     name           Name of file
!     nextrecord     Defines last record read by readsp
!                    Possible values:
!                    0: none
!                    1: PROBLEM
!                    2: POSTPROCESSING
!                    3: MESH
!                    4: READ MESH
!                    5: EXTRA_MESH_INPUT
!     nprob          Number of problems to be solved
!     rinput         Real input array
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer inigettext

!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     FEMESHBF       creates a linear element mesh based on the nodes
!                    of a spectral element mesh
!     INCOMMAT       Actual body of COMMAT and MATSTRUC
!     INI015         Extract infor entries of integer and real input arrays from
!                    ISEPIN
!     INICHINPSOL    Adapt input array with respect to variables
!     INIDBG         read file sepran.dbg and set debug parameters
!     INIFIL         Open files
!     INIGETTEXT     Compute pointer of array in IBUFFR
!     INIMPI         Start MPI
!     INIPRI         Decrease priorities of arrays
!     INIRMF         Remove file if it exists
!     INITCB         Initializes SEPRAN machine-independent common blocks
!     INITMDBF       Set machine-dependent quantities
!     MSH071         Read information of the mesh from the file indicated by
!                    iref
!     MSHEXTRA       Read extra input for mesh and apply the actions required
!                    by this extra input
!     MSHRDPARMESH   Read information parallel information about global mesh
!                    from file meshoutput_par.000
!     PRINCONS       Print all constants, variables and vector names
!     PROBDFEXT      Reads and stores problem definition
!     READ00         Reads input for subroutine COMMAT from standard input
!                    file.
!     READ74SUB      Read names of vectors from the sepcomp.out file
!     READAL         Read all SEPRAN input for the computational program except
!                    problem definition and preceding information
!     READPROBIINP   Read information concerning the input for probdfbf from
!                    file sepcomp.out and call subroutine probdfbf
!     READSP         Reads information for subroutine SEPSTL
!     SIMSETCONST    Set constants defined in the input file, which depend on
!                    the mesh
!     STARTBF        Standard SEPRAN starting subroutine
!     WRITPROBIINP   Write arrays iinput and rinput corresponding to kprob to
!                    file sepcomp.out
! **********************************************************************
!
!                       I/O
!
!    Input is read from the standard input file
!    See the various subroutines that perform this action
!    Furthermore the file meshoutput is read
!    File sepcomp.inf is created
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     719   Too much problems are to be created at one time
!    1669   file does not exist
!    2860   The sepcomp.in and sepcomp.out file have the same name
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      debug = .false.

!     --- Start the program

      iinput(1) = lniinp
      do i = 2, 5
         iinput(i) = 0
      end do
      rinput(1) = dble(lnrinp)
      do i = 2, 5
         rinput(i) = 0d0
      end do
      call initcb
      if ( debug ) write(6,*) 'after initcb'
      if ( isubr==11 ) mainsubr = 102   ! simalex only

!     --- Check if parallel computations must be carried out

      inquire ( file = 'sepran_par.input', exist = parallel )

      if ( parallel ) then

!     --- File sepran_par.input has been found, i.e. parallel computations
!         initialize mpi

         call inimpi
         cputiming = .false.

      else

!     --- Serial computions

         cputiming = .true.

      end if  ! ( parallel )

      call initmdbf ( nbuf )
      if ( debug ) write(irefwr,*) 'after initmdbf'

!     --- Open read and write files

      call inifil(5)
      if ( parallel ) then

!     --- Parallel, we rename irefwr to 26 and couple a name to
!         the output file

         irefwr = 26
         name(1:11) = 'sepran_out.'
         write ( name(12:14),100 ) inodenr
100      format(i3.3)
         open ( unit = irefwr, file = name(1:14) )
         write(irefwr,*) 'node number is ',inodenr

      else

!     --- Serial computation, standard

         call inifil(6)

      end if

!     --- Open standard elements file and error message file

      call inifil(1)
      call inifil(4)
      if ( nbuf>0 ) nbuffr = max(nbuffr,nbuf)
      call inidbg ( ibuffr )
      if ( debug ) write(irefwr,*) 'after inifil'

      if ( parallel ) then

!     --- File sepran_par.input has been found, i.e. parallel computations
!         First set input file

         open ( unit=irefre, file = 'sepran_par.input' )

      end if  ! ( parallel )

!     --- Read array manag (defaults)

      manag(5) = -2
      if ( isubr>=4 .and. isubr/=11 ) then
         manag(5) = manag(5)-1000
         mainsubr = 2
      end if
      call readsp ( ibuffr, manag, lenmanag, nextrecord )
      if ( debug ) write(irefwr,*) 'after readsp'

      if ( parallel ) then

!     --- create name of general parallel mesh input file

         lastps = index ( name10, ' ' )-1
         filename = name10
         filename(lastps+1:lastps+8) = '_par.000'
         inquire ( file = filename(1:lastps+8), exist = check )
         if ( .not. check ) then

!        --- The file meshoutput_par.000 does not exist

            call errchr ( filename(1:lastps+8), 1 )
            call errsub ( 1669, 0, 0, 1 )
            go to 1000

         end if

!        --- Set name of mesh input file

         write(filename(lastps+1:lastps+8),110) inodenr
110      format('_par.',i3.3)
         name10 = filename(1:lastps+8)

         lastps1 = index ( name74, ' ' )-5
         filename1 = name74
         write(filename1(lastps1+1:lastps1+8),110) inodenr
         name74 = filename1(1:lastps1+8)

      end if

!     --- Print a list of all constants, variables, reals and vector names

      if ( mainsubr/=102 ) call princons
      if ( debug ) write(irefwr,*) 'after princons'

!     --- Standard SEPRAN start subroutine

      jstart = abs(manag(1))
      if ( mod(jstart/100,10)==0 ) jstart = jstart + 100
      if ( manag(1)<0 ) then
         manag(1) = -jstart
      else
         manag(1) = jstart
      end if
      if ( mainsubr==102 ) manag(3) = -1   ! simalex only
      call startbf ( ibuffr, manag(1), manag(2), manag(3), manag(4),
     +               nbuf )
      if ( debug ) write(irefwr,*) 'after startbf'
      if ( isubr==1 ) then
         call eropen( 'sepstr' )
      else if ( isubr==2 ) then
         call eropen( 'sepstl' )
      else
         call eropen( 'sepstn' )
      end if
      call eropen ( 'to0092' )
      if ( nbuf>0 ) nbuffr = max(nbuffr,nbuf)

!     --- Open files meshoutput, sepcomp.out and read mesh

      call inifil ( -10 )
      if ( debug ) write(irefwr,*) 'after inifil ( -10 )'
      if  ( manag(11)==1 ) then

!     --- manag(11)>0, read from sepcomp.xxx

         if ( iref74<0 ) iref73 = -abs(iref73)  ! sepcomp.in has same
                                                ! structure as sepcomp.out
         call inifil ( -73 )

      end if  ! ( manag(11)==1 )

      if ( manag(7)==1 ) then

!     --- manag(7) = 1, fill files 74 (sepcomp.out)

         if ( manag(11)==0 ) then

!        --- manag(11)=0, standard

            call inirmf ( 73 )  ! remove sepcomp.inf
            call inirmf ( 74 )  ! remove sepcomp.out

         end if  ! ( manag(11)==0 )

!        --- sepcomp.out is not opened in case of simalex

         if ( mainsubr/=102 ) then

!        --- Not simalex, open file sepcomp.out

            if ( manag(7)==1 ) then

!           --- manag(7) = 1, write to sepcomp.out

               call inifil ( 74 )
               if ( manag(11)>0 ) then

!              --- manag(11)>0, in and output files must have
!                  different names

                  if ( name74==name73 ) then

!                 --- sepcomp.in and sepcomp.out have the same name

                     call errchr ( name73, 1 )
                     call errsub ( 2860, 0, 0, 1 )
                     go to 1000

                  end if  ! ( namef74==namef73 )

               end if  ! ( manag(11)>0 )

            end if  ! ( manag(7)==1 )

         end if  ! ( mainsubr/=102 )

         iref = 22
         inquire ( file='sepcomp.freq', exist=check )
         if ( check ) then

            open ( unit = iref, file = 'sepcomp.freq',
     +             status = 'unknown' )
            close ( unit = iref, status = 'delete' )

         end if

      end if  ! ( manag(7)==1 )

      if ( debug ) write(irefwr,*) 'before msh071'
      call msh071 ( ibuffr, buffer, manag(5), iref10, kmesh, ihelp )
      if ( debug ) write(irefwr,*) 'after msh071'
      iref = abs(iref10)
      close ( unit = iref )

      if ( mainsubr==102 ) then

!     --- mainsubr = 102: i.e. simalex
!         Special call to set constants in the constants block depending
!         on the mesh

         call simsetconst ( ibuffr, buffer, kmesh )
         call princons
         if ( debug ) write(irefwr,*) 'na simsetconst'
         if ( ierror/=0 ) go to 1000

      end if  ! ( mainsubr==102 )

      if ( parallel ) then

!     --- parallel is true, hence parallel computing
!         Read file meshoutput_par.000 and some information from meshoutput

         call mshrdparmesh ( ibuffr, buffer, kmesh )

      end if

!     --- Check if spectral mesh has been found
!         If so call femesh

      if ( isubr==4 .and. kmesh(38,1)/=0 ) then

!     --- Spectral mesh: fill fem mesh in kmesh(.,2)

         iref = 22
         write(6,*) 'call femeshbf: '
         call femeshbf ( ibuffr, buffer, kmesh(1,1), kmesh(1,2),
     +                   iref, 1 )
         close ( unit = iref )
         if ( debug ) write(irefwr,*) 'after femeshbf'

      end if

      if ( nextrecord==5 ) then

!     --- Read extra mesh input and perform corresponding actions

         call mshextra ( ibuffr, buffer, kmesh )
         if ( debug ) write(irefwr,*) 'after mshextra'

      end if  ! ( nextrecord==5 )

!     --- Read problem definition

      if ( manag(11)==0 ) then

!     --- manag(11)=0, standard case

         call probdfext ( ibuffr, buffer, manag(6), kprob, kmesh,
     +                    iinput, rinput, iseqprob )

      else

!     --- manag(11)=1, read problem definition

         irefsav = iref74    ! save iref74
         iref74 = iref73     ! replace by iref73
         call readprobiinp ( ibuffr, buffer, kprob, kmesh, iinput,
     +                       .false. )

!        --- Read names of vectors

         call read74sub
         iref74 = irefsav    ! reset iref74
         iseqprob = 1

      end if  ! ( manag(11)==0 )
      if ( ierror/=0 ) go to 1000

      if ( debug ) then
         write(irefwr,*) 'after probdfext'
         write(irefwr,200) 'iseqprob', iseqprob
200      format ( a, 1x, 100(i6,1x) )
      end if  ! ( debug )

      if ( manag(7)==1 .and. mainsubr/=102 ) then

!     --- Write array iinput to file sepcomp.out
!         This part is skipped in case of simalex

         call writprobiinp ( ibuffr, buffer, kprob )
         if ( debug ) write(irefwr,*) 'after writprobiinp'

      end if

      nprob = max ( 1, kprob(40) )
      if ( nprob>10 ) then

!     --- nprob>10 not yet implemented

         call errint ( nprob, 1 )
         call errint ( 10, 2 )
         call errsub ( 719, 2, 0, 0 )
         go to 1000

      end if

      callcommat = .true.  ! default commat must be called

      if ( iread==1 ) then

!     --- isubr = 1, Read rest of the SEPRAN input

         call readal ( ibuffr, buffer, kmesh, kprob, iseqprob, manag )
         if ( debug ) write(irefwr,*) 'after readal'
         if ( ierror/=0 ) go to 1000

!        --- Find first input of incommat and store in iincommt

         call ini015 ( ibuffr, buffer, 3, 1, iinput(1), idummy )
         if ( ierror/=0 ) go to 1000
         if ( iinput(1)>0 ) then

!        --- iinput(1)>0, hence input for commat is defined

            ipmetd = inigettext ( iinput(1), 'iinput' )
            do i = 1, nprob
               do j= 1, lniincommat
                  iincommt(j,i) = ibuffr(ipmetd-1+j+(i-1)*lniincommat)
               end do
            end do

         else

!        --- iinput(1)=0, hence input for commat is not defined

            callcommat = .false.  ! commat must not be called

         end if  ! ( iinput(1)>0 )

      else

!     --- Read iincommt and call commat

         iseqnr = 1
         call read00 ( iincommt, nprob, iseqnr, 0, kmesh, lniincommat )
         if ( debug ) write(irefwr,*) 'after read00'

      end if
      if ( ierror/=0 ) go to 1000

!     --- Adapt variables

      call inichinpsol ( iincommt, lniincommat*nprob )
      if ( debug ) write(irefwr,*) 'after inichinpsol'
      if ( ierror/=0 ) go to 1000

      if ( callcommat ) then

!     --- callcommat true, call subroutine commat

         do iprob = 1, nprob
            ishift = iincommt(9,iprob)
            call incommat ( ibuffr, iincommt(1,iprob),
     +                      kmesh(1,1+ishift), kprob, intmat(1,iprob) )
            intmat(2,iprob) = intmat(2,iprob)+1000*ishift
         end do
         if ( debug ) write(irefwr,*) 'after incommat'

      end if  ! ( callcommat )

!     --- Decrease priorities

      call inipri

1000  call erclos ( 'to0092' )
      if ( isubr==1 ) then
         call erclos( 'sepstr' )
      else if ( isubr==2 ) then
         call erclos( 'sepstl' )
      else
         call erclos( 'sepstn' )
      end if

      end
