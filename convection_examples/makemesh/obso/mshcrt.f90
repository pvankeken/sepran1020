      subroutine mshcrt ( jnew, ichois, iinput, rinput, krenum, kcoars )
! ======================================================================
!
!        programmer    Guus Segal
!        version 30.5  date 12-03-2015 Extra debug
!        version 30.4  date 22-05-2014 Extra debug
!        version 30.3  date 26-11-2013 New call to mshanalconnect
!
!   copyright (c) 1984-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     create mesh, fill part of array kmesh
!     .
!     Remark
!     ------
!     .
!     If a new surface type is introduced, the following subroutines should
!     be checked since they are a candidate for changes:
!     .
!     msh000, msh011, msh015, msh019, msh033, msh043, msh049, msh055
!     msh062, msh063, msh064, msh069, mshcrt, mshrd2, mshrd5, mshfil
!     mshfl2, mshsrf
! **********************************************************************
!
!                       KEYWORDS
!
!     mesh
!     mesh_generation
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulework
      use sepmodulemain
      use sepmoduleconsts
      use sepmoduleplot
      use sepmoduledebug
      use sepmodulekmesh
      use sepmodulemesh
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      logical jnew
      integer ichois, iinput(*), krenum(*), kcoars
      double precision rinput(*)

!     ichois     i    Choice parameter JCHOIS in subroutine MESH
!                     Possible values
!                     0  standard method,  input from standard input file
!                        the mesh is created for the first time
!                     1  input is read from the arrays iinput and rinput
!                        the mesh is created for the first time
!                     2  input is read from the standard input file and stored
!                        in the arrays iinput and rinput
!                        the mesh is created for the first time
!                     3  input is read from the arrays iinput and rinput
!                        the co-ordinates of the existing mesh are changed
!                        the topological description remains unchanged
!                     4  input is read from the arrays iinput and rinput.
!                        the coordinates and the topological description of the
!                        mesh are changed, however the input as stored in iinput
!                        and rinput is the same as in a preceding call of mesh.
!                        The number of points and curves remains the same, as
!                        well as the number of elements along the curves.
!                        The coordinates of the user points and the curves are
!                        changed by a call of subroutine chanbn and as a
!                        consequence the number elements in the surfaces may be
!                        changed.
!                        This possibility must be preceded by calls of
!                        subroutine mesh and subroutine chanbn
!                     5  See jchois = 4, however, the topological description
!                        remains unchanged
!                     6  Only the arrays iinput and rinput are filled
!                        The subroutine is returned without actually creating a
!                        mesh
!                     7  The input must be stored in the arrays IINPUT and
!                        RINPUT
!                        The subroutine creates the curves and stores the
!                        information of curves and user points via KMESH
!                        After this step the subroutine returns
!                        This possibility is meant for program SEPGEOM
!                    10  The boundary of the mesh has been changed by subroutine
!                        ADAPBN. Information with respect to this change
!                        has been stored in the arrays ICURVS, CURVES,IINPUT
!                        and RINPUT, which are all part of array KMESH
!                        The input/output parameters IINPUT and RINPUT are not
!                        used.
!                        The topological description of the mesh may be changed
!                    11  See ICHOIS=10, however, the topological description
!                        may not be changed
!                    12  See ICHOIS=10, however, also the description of the
!                        surfaces has been changed
!     iinput     i    integer input array, standard for subroutine MESH
!                     The starting address in MESH is 1
!     jnew       i    indication whether a new mesh must be created (jnew=true)
!                     or an old one must be changed (coordinates only:
!                     jnew=false)
!     kcoars     i    If 1 coarseness is used
!     krenum     o    Array of length 10 with information about the
!                     renumbering.
!                     Array must be filled as follows:
!                      1: if>=0, only position 1 is filled.
!                         This position contains the parameter method according
!                         to the old SEPRAN definition, i.e.
!                         krenum = 9: Use default depending on mesh
!                         else:
!                         krenum = ispecial*100 + method + 3 * ipref
!                         ispecial = 0: standard
!                                    1: ivertx=1, imidp=1, icentr=3
!                         If<0, -krenum(1) contains the sequence number
!                         of the last entry filled by the user
!                         All other values are filled with defaults
!                      2: method Indication of the renumbering method used
!                                The algorithms are compared to the original
!                                numbering:
!                                0  No renumbering
!                                1  Cuthill-Mckee algorithm
!                                2  Sloan algorithm
!                                3  Both algorithms,  the best is used
!                                9  Choose depending on mesh
!                         Default: 9
!                      3: ipref  Indication of the criterion to be used to find
!                                the best algorithm:
!                                0  Choose the smallest profile
!                                1  Choose the smallest band width
!                                2  Use only the renumbered algorithm
!                                   (method=1 or 2)
!                         Default: 0
!                      4: iprint Indicates if information about the renumbering
!                                must always be printed (1) or only depending on
!                                ioutp (0)
!                         Default: 0
!                      5: ivertx Priority for vertex nodes (1-3)
!                         Default: 3
!                      6: imidp  Priority for mid point nodes (1-3)
!                         Default: 3
!                      7: icentr Priority for centroids (1-3)
!                         Default: 3
!                      8: ilevel Indicates if renumbering of special nodes must
!                                be performed per level (1) or not (0)
!                         Default: 0
!                      9: istmet Indicates how the start of the renumbering
!                                algorithm must be performed
!                                0  Automatic
!                                1  Start user point given
!                                2  Start curve given
!                                3  Start surface given
!                         Default: 0
!                     10: istval Value of start point, curve f surface
!                         Default: 1
!     rinput     i    real input array, standard for subroutine MESH
!                     The starting address in MESH is 1
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ipkmsxa, i, lengc, lengc1, lengpm, &
              ikelmc, curves, lengmx, &
              iintrn(4), icurvs, iinplt(14), inodpbef, &
              lengcbef, idum(1), lengxa, lenkmeshscp
      integer, allocatable :: iseqsurfs(:), ipointsurf(:), infel(:)
      double precision rinplt(5)
      logical activatm, holes
      type (mapping) :: maploc

!     activatm       If true kmesh part m must be activated
!     curves         Sequence number with respect to array INFOR for CURVES
!     holes          If true, a hole in the surface has been found
!     i              Counting variable
!     icurvs         Memory management reference number of array icurvc
!     idum           General dummy variable
!     iinplt         Input array for subroutine plotms:
!                     1:  Number of entries filled
!                     2:  Plot element numbers (1) or not (0)
!                     3:  Plot nodal point numbers (1) or not (0)
!                     4:  Mark nodal point numbers (1) or not (0)
!                     5:  Contract elements (1) or not (0) (2D only)
!                     6:  Plot outer boundaries double (1) or not (0)
!                     7:  Skip element groups (>0) or not (0)
!                     8:  Plot renumbered node numbers (1) or original ones (0)
!                     9:  Make a filled coloured mesh plot (1) or a line plot
!                         (0)
!                    10:  Plot axis (1) or not (0)
!                    11:  Type of projection.
!                    14:  Orientation of base vectors
!     iintrn         Input array for subroutine transf. iintrn is filled as
!                    follows
!                    1:  Number of entries filled
!                    2:  If topol must be called:0 else 1
!                    3:  Type of renumbering
!                    4:  Type of transformation
!     ikelmc         position in kmesh part c
!     infel          array containing some information of how the elements
!                    created are stored on file 3
!     inodpbef       First number to be used for new nodes in interface elements
!     ipkmsxa        Starting address of array KMESH part xa into array IBUFFR
!     ipointsurf     Array containing the relative starting addresses
!                    of all surfaces in the part of iinput referring
!                    to surfaces
!     iseqsurfs      Contains new sequence of surface numbers
!     lengc          computed length of kmesh part c
!     lengc1         computed length of extended kmesh part c
!     lengcbef       Length of kmesh part c before interface elements
!     lengmx         Maximum length of local array kmeshc
!     lengpm         computed length of kmesh part m
!     lengxa         Length of array KMESH part xa
!     lenkmeshscp    Length of kmeshs parts 1 + 2 or 3
!     maploc         contains information of the mapping
!     rinplt         Input array for subroutine plotms:
!                    1:    plotfm
!                    2:    yfact
!                    3-5:  eye-point
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERALLOC        Produce error message in case allocate went wrong
!     ERCLOS         Resets old name of previous subroutine of higher level
!     ERDEALLOC      Produce error message in case deallocate went wrong
!     EROPEN         Produces concatenated name of local subroutine
!     MSH000         Fill some parameters from array IINPUT
!     MSH001         Fill element shapes in array KMESH
!     MSH020         fill arrays coor and kmeshc using the information of kmeshh
!                    and infel
!     MSH028         initialize kmeshn and compute the length of this array
!     MSH034         fill array kmeshn for the positions corresponding to curves
!                    with the negative values of the corresponding positions in
!                    kmeshm
!     MSH038         Check the input for connected elements
!     MSH041         fill array kmeshc for connected elements
!     MSH045         Fill array kmeshm for curves of curves and
!                    kmeshn for surfaces of surfaces
!     MSH054         Check if all user points, curves and surfaces have been
!                    filled
!     MSH055SORT     Sort the outer boundary of composite surfaces in the case
!                    that this outer boundary contains holes
!     MSHACTMPR      Activate subarrays of kmesh part h for subroutine MSHMPR
!     MSHANALCONNECT Analyse connections between volumes, surfaces and so on
!     MSHCHACR       Change coordinates in array coor by calls to FUNCCOOR
!     MSHCRTCURV     Create curves for the mesh generator
!     MSHCRTINTERF   Fill interface elements in kmeshc
!                    Adapt array coor if necessary
!     MSHCRTSURF     Create surfaces for the mesh generator
!     MSHCRTVOLM     Create volumes for the mesh generator
!     MSHDELKMESHH   Delete kmesh part h and subarrays
!     MSHFILLKMQS    Fill array kmesh parts q, r, s as well as
!                    array volume_surfs
!     MSHFILLSHAPE   Fill shape numbers of surfaces that have not been
!                    filled yet
!     MSHFLKMP       Fill array kmesh part p
!     MSHINTERFACE   checks the input for interface elements. computes the
!                    required length of these elements with respect to
!                    KMESH part c
!     MSHLNS         fill the line elements in kmesh part c  and fill first
!                    part of coordinate array coor
!     MSHMPR         fill the arrays mapar, kmesh part g, kmeshl, kmeshm, kmeshn
!     MSHOBSIN       Compute information of obstacles and fill in array
!                    kmesh part x
!     MSHOBSIN3D     Compute information of obstacles and fill in array
!                    kmesh part x (R^3)
!     MSHREFCHECK    Check volume of all elements corresponding to surfaces and
!                    volumes after refinement
!     MSHSEQCURVS    Compute sequence in which curves must be created
!     MSHSEQSURFS    Compute sequence in which surfaces must be created
!     PLAFEP         General SEPRAN low level plotting subroutine
!     PLAFP7         Plot text using absolute co-ordinates
!     PLSCAL         Plot scales
!     PRININ         print 1d integer array
!     REFINENEW      Refine mesh
!     SEPCLEAR       Clear mapping array
!     SEPCREAKMHSUR  allocate kmhsur
!     SEPFILLKMESHC  Make relation between kmeshc and Topgr
!     SEPPLOTMS      plot mesh
!     SEPPOINTKMESHC Fill array pointkmsc with starting addresses of element
!                    groups in kmeshc (minus 1)
!     SEPPRINKMESH   Print array KMESH
!     SEPPRINKMESHC  print kmesh part c
!     SEPRELKMESH    Make the relation between kmref and the pointers
!                    in sepmodulekmeshp
!     SEPTRANSF      Transform linear triangular mesh to other type
!     SPECTR         Generate spectral elements from ordinary ones
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     fill some parameters from array iinput
!     Check if there are surfaces that must be created by new curves and
!     surfaces
!     If ( new curves must be created ) then
!        split iinput, rinput into subarrays
!        Store which parts are mapped
!        Create new arrays iinput and rinput
!        fill some parameters from array iinput
!     Create mesh
!     If ( new curves have been created ) then
!        Remove extra user points, curves and surfaces
!        Map subarrays of kmesh back to correct size
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'mshcrt' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from mshcrt'
         write(irefwr,1) 'npointlc, nelemlc', npointlc, nelemlc
  1      format ( a, 1x, (10i6) )

      end if

!     --- fill some parameters from array iinput

      holes = .false.
      jcoars = kcoars
      nsurfs = iinput(4)
      call sepclear ( maploc )

!     --- fill some parameters from array iinput

      if ( debug ) write(irefwr,*) 'before msh000'
      call msh000 ( iinput, krenum, ichois )
      if ( ierror/=0 ) go to 1000

      if ( iinp13/=0 ) then

!     --- iinp13 # 0   change coordinates of user points

         isurnr = 0
         call mshchacr ( iinput(iinp13), rinput(3), idum, idum, 1 )

      end if

      if ( jnew ) then

!     --- jnew = true
!         Fill element shapes in kmesh
!         Fill kmesh parts a, b, f and w
!         This subroutine uses memory management subroutines

         if ( debug ) write(irefwr,*) 'before msh001'
         call msh001 ( iinput, rinput )

      else if ( ichois==4 .or. ichois==10 .or. ichois==12 ) then

!     --- ichois = 4, 10, new topological description without changing iinput
!                         and rinput

         if ( nelgrp>1 ) kmeshg = 0

      end if
      if ( ierror/=0 ) go to 1000

!     --- Detect sequence of curves and store in array iseqcurvs

      allocate ( iseqcurvs(ncurvs), ipointcurv(ncurvs), stat = error )
      if ( error/=0 ) call eralloc ( error, 2*ncurvs, 'iseqcurvs' )
      if ( ierror/=0 ) go to 1000

      alloccurv = .true.

      call mshseqcurvs ( iinput )

      lengmx = 0

      i = max(1,nsurfs)

      allocate ( iseqsurfs(i), ipointsurf(i), stat = error )
      if ( error/=0 ) call eralloc ( error, 2*i, 'iseqsurfs' )
      if ( ierror/=0 ) go to 1000

      if ( nsurfs>0 ) then

!     --- Detect sequence of surfaces and store in array iseqsurfs

         if ( debug ) write(irefwr,*) 'before mshseqsurfs'
         call mshseqsurfs ( iinput(iinp3), nsurfs, iseqsurfs, &
                            ipointsurf )

!        --- Fill ishape for those surfaces that have not been filled yet

         if ( debug ) write(irefwr,*) 'before mshfillshape'
         call mshfillshape ( iinput(iinp3), iseqsurfs, &
                             ipointsurf, nsurfs )

      end if  ! ( nsurfs>0 )

!     --- Fill arrays part q, r and s as well array volume_surfs

      if ( jnew .or. ichois==12 ) then

!     --- new mesh
!         This subroutines uses memory management subroutines

         if ( debug ) write(irefwr,*) 'before mshfillkmqs'
         call mshfillkmqs ( iinput, rinput, iseqsurfs, ipointsurf )

      end if

!     --- Analyse connection between volumes surfaces and curves

      if ( debug ) write(irefwr,*) 'before mshanalconnect'
      call mshanalconnect ( iinput )

!     --- kmesh part l   clear when new

      if ( kelml/=nuspnt ) then
         if ( km(iseqkm)%kmesh(26)>0 ) deallocate(km(iseqkm)%kmeshl)
         allocate ( km(iseqkm)%kmeshl(nuspnt) )
         kmeshl => km(iseqkm)%kmeshl
         kmeshlglob => kmeshl
         kelml = nuspnt
      end if  ! ( kelml/=nuspnt )

      if ( ierror/=0 ) go to 1000
      if ( jnew .or. nsurvl==0 .or. ichois==4 .or. ichois==10 .or. &
           ichois==12 ) kmeshl = 0

!     --- kmesh part h

      if ( kelmh==0 ) then

!     --- kelmh = 0,  kmesh part h has not been created before.
!         Reserve space and clear array

         if ( associated(km(iseqkm)%kmeshh) ) call mshdelkmeshh
         kelmh = nsurfs+nvolms+1
         allocate ( km(iseqkm)%kmeshh(6,kelmh) )
         kmeshh => km(iseqkm)%kmeshh
         kmeshh = 0

         call sepcreakmhsur

      end if
      kmhsur => km(iseqkm)%kmhsur

!     --- Fill curves

      if ( debug ) write(irefwr,*) 'before mshcrtcurv'
      call mshcrtcurv ( ichois, iinput, rinput, activatm, jnew )
      if ( ichois==7 ) go to 1000

!     --- fill surfaces and write to file 3

      lengmx = 0

      if ( nsurfs>0 ) then

!     --- nsurfs>0; fill surfaces

         if ( debug ) write(irefwr,*) 'before mshcrtsurf'
         lenkmeshscp = size(kmeshs)-(nsurfs+1)-8*nsurfs
         call mshcrtsurf ( holes, jnew, activatm, ipointsurf, &
                           iinput, rinput, lengmx, lenkmeshscp, iseqsurfs )

      end if
      if ( ierror/=0 ) go to 1000

      deallocate ( iseqsurfs, ipointsurf, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'iseqsurfs' )
      if ( ierror/=0 ) go to 1000

!     --- fill volumes and write to file 3

      if ( nvolms>0 ) then

!     --- nvolms>0; fill volumes

         if ( debug ) write(irefwr,*) 'before mshcrtvolm'
         call mshcrtvolm ( iinput, jnew, activatm, rinput )

      else

!     --- nvolms = 0; set npomax

         npomax = npomxs

      end if
      if ( ierror/=0 ) go to 1000

      if ( debug ) write(irefwr,1) 'nvolel', nvolel
      if ( nvolel>0 ) then

!     --- nvolel>0  initialize kmeshn

         if ( debug ) write(irefwr,*) 'jnew ', jnew
         if ( jnew ) then

            allocate ( kmeshn(2*nsurfs), stat = error )
            if ( error/=0 ) call eralloc ( error, 2*nsurfs, 'kmeshn' )
            if ( ierror/=0 ) go to 1000
            call msh028 ( iinput(iinp3), length, kmeshn )

            if ( km(iseqkm)%kmesh(28)>0 ) deallocate(km(iseqkm)%kmeshn)
            allocate ( km(iseqkm)%kmeshn(length) )
            km(iseqkm)%kmeshn(2*nsurfs+1:length) = 0
            km(iseqkm)%kmeshn(1:2*nsurfs) = kmeshn(1:2*nsurfs)
            kelmn = length

            deallocate ( kmeshn, stat = error )
            if ( error/=0 ) call erdealloc ( error, 'kmeshn' )
            if ( ierror/=0 ) go to 1000

            kmeshn => km(iseqkm)%kmeshn
            kmeshnglob => kmeshn

!           --- Activate some arrays for msh034

            call msh034 ( iinput(iinp3) )
            if ( debug ) call prinin ( kmeshn, length, 'kmeshn' )
         end if

      end if

      if ( ierror/=0 ) go to 1000

!     --- fill line elements and write to file 3 when nlines>0

      allocate ( infel(nsurvl+4), stat = error )
      if ( error/=0 ) call eralloc ( error, nsurvl+4, 'infel' )
      if ( ierror/=0 ) go to 1000

!     --- Clear array infel

      infel = 0
      lengc = 0
      lengpm = 0
      inodp = 1
      iselem = 1

      if ( associated(kmeshc) ) nullify(kmeshc)
      allocate ( kmeshc(2*lencrv), stat = error )
      if ( error/=0 ) call eralloc ( error, 2*lencrv, 'kmeshc' )
      if ( ierror/=0 ) go to 1000
      if ( jnew ) then

!     --- jnew = true   new mesh

         allocate ( coor(ndim,lencrv), stat = error )
         if ( error/=0 ) call eralloc ( error, ndim*lencrv, 'coor' )
         if ( ierror/=0 ) go to 1000

      end if

      call mshlns ( iinput, infel, lengc, jnew, rinput(3) )

!     --- Store arrays kmeshc and coor temporarily in ibuffr
!         The mm indices are stored in infel(3) and infel(1)
!         Later on they will be removed

      if ( infel(4)>0 ) then

!     --- infel(4)>0, hence coor has length>0

         allocate ( coorline(ndim,infel(4)))
         coorline = coor(:,1:infel(4))
         if ( jnew ) then
            deallocate ( coor, stat = error )
            if ( error/=0 ) call erdealloc ( error, 'coor' )
            if ( ierror/=0 ) go to 1000
         end if  ! ( jnew )

      end if

      if ( jnew .and. infel(2)>0 ) then

!     --- infel(2)>0, hence kmeshc has length>0

         allocate ( kmeshcline(infel(2)))
         kmeshcline = kmeshc(1:infel(2))

      end if

      deallocate ( kmeshc, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'kmeshc' )
      if ( ierror/=0 ) go to 1000

!     --- Activate arrays with respect to mshmpr

      if ( debug ) write(irefwr,*) 'Before mshactmpr'
      call mshactmpr ( iinput, jnew )

!     --- fill the arrays mapar, kmesh part g, kmeshl, kmeshm, kmeshn, kmesht
!         raise inodp and iselem
!         a part of array infel is filled

      if ( debug ) write(irefwr,*) 'Before mshmpr'
      call mshmpr ( iinput, infel, lengc, lengpm, lengmx, jnew )

      if ( jnew .and. iinp11/=0 ) then

!     --- iinp11 # 0   interface elements

         lengcbef = lengc
         inodpbef = inodp
         if ( debug ) write(irefwr,*) 'Before mshinterface'
         call mshinterface ( iinput, lengc, activatm )

      end if

!     --- fill arrays coor, kmeshc, kmeshl, kmeshm

      if ( jnew ) then

!     --- jnew = true

         if ( km(iseqkm)%kmesh(23)>0 ) deallocate(km(iseqkm)%coor)
         allocate ( km(iseqkm)%coor(ndim,npoint), stat = error )
         if ( error/=0 ) call eralloc ( error, npoint*ndim, 'coor' )
         if ( ierror/=0 ) go to 1000
         coor => km(iseqkm)%coor
         coorglob => coor
         npointglob = npoint
         kelmi = npoint*ndim

      end if
      if ( .not. activatm ) then

!     --- kmesh part m is new

         allocate ( kmeshm(lengpm) )
         kmeshm = km(iseqkm)%kmeshm(1:lengpm)
         if ( km(iseqkm)%kmesh(27)>0 ) deallocate(km(iseqkm)%kmeshm)
         allocate ( km(iseqkm)%kmeshm(lengpm), stat = error )
         if ( error/=0 ) call eralloc ( error, lengpm, 'kmeshm' )
         if ( ierror/=0 ) go to 1000

         km(iseqkm)%kmeshm = kmeshm(1:lengpm)

         deallocate(kmeshm)
         kmeshm => km(iseqkm)%kmeshm
         kmeshmglob => kmeshm
         kelmm = lengpm

      end if

!     --- Fill kmeshm and kmeshn for curves of curves and surfaces of surfaces

      call msh045

      if ( debug ) then
         write(irefwr,*) 'jnew ', jnew
         write(irefwr,1) 'kelmc, lengc', kelmc, lengc
      end if  ! ( debug )

      if ( jnew ) then
         if ( km(iseqkm)%kmesh(17)>0 ) deallocate(km(iseqkm)%kmeshc)
         allocate ( km(iseqkm)%kmeshc(lengc), stat = error )
         if ( error/=0 ) call eralloc ( error, lengc, 'kmeshc' )
         if ( ierror/=0 ) go to 1000
         kmeshc => km(iseqkm)%kmeshc
         kelmc = lengc

      end if

!     --- fill arrays coor and kmeshc using the information of kmeshh
!         and infel

      if ( debug ) write(irefwr,*) 'Before msh020'
      call msh020 ( infel, jnew )
      if ( ierror/=0 ) go to 1000

!     --- Remove coor and kmeshc as stored in infel

      if ( jnew .and. infel(2)>0 ) deallocate ( kmeshcline )
      if ( infel(4)>0 ) deallocate ( coorline )

      deallocate ( infel, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'infel' )
      if ( ierror/=0 ) go to 1000

      if ( jnew .and. iinp11/=0 ) then

!     --- iinp11 # 0   interface elements

         ikelmc = lengcbef
         if ( debug ) write(irefwr,*) 'Before mshcrtinterf'
         call mshcrtinterf ( iinput(iinp11), ikelmc, inodpbef )

      end if

!     --- Check if all user points, curves and surfaces have been filled with
!         nodal point numbers
!         If not give warnings

      if ( jnew ) then

!     --- jnew = true

         if ( debug ) write(irefwr,*) 'Before msh054'
         call msh054

      end if

      if ( holes ) then

!     --- There are holes in the outer boundary; it is necessary to redo
!         the sorting of the outer boundary

         if ( debug ) write(irefwr,*) 'Before msh055sort'
         call msh055sort

      end if

!     --- Put iinput and rinput in KMESH

      if ( kmiinput==0 ) then
         kmiinput = iinp8
         kmrinput = irnp3
         if ( associated(km(iseqkm)%iinputkm) ) &
            deallocate(km(iseqkm)%iinputkm, km(iseqkm)%rinputkm)
         allocate ( km(iseqkm)%iinputkm(iinp8), km(iseqkm)%rinputkm(irnp3), &
                    stat = error )
         if ( error/=0 ) call eralloc ( error, iinp8+irnp3, 'iinput' )
         if ( ierror/=0 ) go to 1000
         iinputkm => km(iseqkm)%iinputkm
         rinputkm => km(iseqkm)%rinputkm

      end if  ! ( kmiinput==0 )

!     --- Next copy

      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'iinp8, irnp3 ',iinp8, irnp3
         call prinin ( iinput, iinp8, 'iinput' )

      end if
      iinputkm = iinput(1:iinp8)
      rinputkm = rinput(1:irnp3)
      if ( ierror/=0 ) go to 1000

      if ( .not. associated(pointkmsc) ) allocate ( pointkmsc(nelgrp+1) )

!     --- Refine if necessary

      if ( debug )write(irefwr,*) 'jnew, irefin ', jnew, irefin
      if ( jnew .and. irefin>0 ) then

!     --- Mesh must be refined

         call sepfillkmeshc   ! Make relation between kmeshc and Topgrp
         call seppointkmeshc  ! Fill array pointkmsc
         call refinenew ( irefin+1100, iinput, &
                          rinput, .true., maploc, .false. )
         if ( icheck>0 ) then

!        --- Check refined mesh

            call mshrefcheck ( icheck )

         end if

      end if  ! ( jnew .and. irefin>0 )

!     --- Call transf if necessary

      if ( debug )write(irefwr,*) 'jnew, itrans ', jnew, itrans
      if ( jnew .and. itrans>0 ) then

         iintrn(1) = 4
         iintrn(2) = 1
         iintrn(3) = 0
         iintrn(4) = itrans
         call sepfillkmeshc   ! Make relation between kmeshc and Topgrp
         call seppointkmeshc  ! Fill array pointkmsc
         call septransf ( iintrn )
         if ( ierror/=0 ) go to 1000

      end if

!     --- Call spectral if necessary

      if ( debug ) write(irefwr,*) 'nspec ', nspec
      if ( nspec>0 ) then

!     --- User has given command intermediate points

         call sepfillkmeshc   ! Make relation between kmeshc and Topgrp
         call seppointkmeshc  ! Fill array pointkmsc
         if ( debug )write(irefwr,*) 'befpre spectr'
         call spectr ( iinput, rinput )
         if ( ierror/=0 ) go to 1000
         call seprelkmesh(kmesh)

      end if

      if ( debug )write(irefwr,*) 'iinput(12) ', iinput(12)
      if ( jnew .and. iinput(12)/=0 ) then

!     --- iinput(12) # 0 connection elements

         lengc1 = size ( kmeshc )
         lengc  = lengc1

         if ( debug ) write(irefwr,*) 'Before msh038'

         call msh038 ( iinput(iinp7), lengc )
         if ( ierror/=0 ) go to 1000

!        --- Extend array kmeshc with space for connection elements

         if ( lengc>size(kmeshc) ) then

!        --- lengc > length, extend

            length = size(kmeshc)
            allocate ( iwork(length))
            iwork = kmeshc
            deallocate (km(iseqkm)%kmeshc )
            allocate (km(iseqkm)%kmeshc(lengc) )
            km(iseqkm)%kmeshc(1:length) = iwork
            kmeshc => km(iseqkm)%kmeshc
            kelmc = lengc
            deallocate ( iwork )

         end if  ! ( lengc>length )

!        --- Define work array of length npoint + ncurvs

         ikelmc = lengc1
         if ( ierror/=0 ) go to 1000
         call msh041 ( iinput(iinp7), ikelmc )

      end if
      if ( ierror/=0 ) go to 1000

      call sepfillkmeshc   ! Make relation between kmeshc and Topgrp
      call seppointkmeshc  ! Fill array pointkmsc

      if ( debug ) call sepprinkmeshc

      if ( ndim>=2 .and. ichois<=2 .and. irefin==0 ) then

!     --- ndim = 2 and ichois<=2
!         Fill array KMESH part P

         if ( associated(km(iseqkm)%elsize) ) deallocate(km(iseqkm)%elsize)
         allocate ( km(iseqkm)%elsize(2,ncurvs), stat = error )
         if ( error/=0 ) call eralloc ( error, 2*ncurvs, 'elsize' )
         if ( ierror/=0 ) go to 1000
         elsize => km(iseqkm)%elsize

         kelmp = ncurvs
         if ( debug ) write(irefwr,*) 'Before mshflkmp'
         call mshflkmp ( coor, kmeshm, elsize, &
                         ncurvs, 1, icurvsmsh, curvesmsh )

      end if
      if ( ierror/=0 ) go to 1000

      if ( debug ) write(irefwr,*) 'Before change coordinates'
      if ( iinp13/=0 ) then

!     --- iinp13 # 0   change coordinates

         if ( debug ) write(irefwr,*) 'Before mshchacr'
         call mshchacr ( iinput(iinp13), coor, kmeshc, idum, 5 )

      end if
      if ( ierror/=0 ) go to 1000

      if ( debug ) write(irefwr,*) 'Before obstacles'
      if ( ichois<=2 .and. numobst>0 ) then

!     --- numobst>0, fill information concerning the obstacles in kmesh

         if ( ndim==2 ) then

!        --- ndim = 2, obstacle consists of curves

            call mshobsin ( iinput )

         else

!        --- ndim = 3, obstacle consists of surfaces

            call mshobsin3d ( iinput )

         end if

      end if
      if ( ierror/=0 ) go to 1000

      deallocate ( iseqcurvs, ipointcurv, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'ipointcurv' )
      if ( ierror/=0 ) go to 1000

      alloccurv = .false.

      call seprelkmesh ( kmesh )

!     --- plot mesh

      if ( debug ) write(irefwr,*) 'Before plot'
      if ( jnew .and. nomesh==0 .and. iplot/=0 .and. &
           (ndim<=2 .or. i3d==1) ) then

!     --- Plot mesh

         iinplt(1) = 14
         iinplt(2) = 0
         iinplt(3) = 0
         iinplt(4) = 0
         iinplt(5) = 0
         iinplt(6) = 0
         iinplt(7) = 0
         iinplt(8) = 0
         iinplt(9) = 0
         iinplt(10) = 0
         iinplt(11) = 0
         iinplt(12) = 0
         iinplt(13) = 0
         iinplt(14) = iorien
         if ( jmark<3 .or. jmark==6 ) iinplt(2) = 1
         if ( mod(jmark,3)==0 .or. jmark==7 ) iinplt(3) = 1
         if ( jmark<=1 .or. jmark==3 .or. jmark==4 ) iinplt(4) = 1
         rinplt(1:2) = rinput(1:2)
         if ( abs(rinplt(1))<=0.1d0 ) rinplt(1) = defplo
         if ( abs(rinplt(2))<=0d0 ) rinplt(2) = 1d0
         if ( i3d==1 ) rinplt(3:5) = rinput(irnp3-2:irnp3)

         if ( debug ) write(irefwr,*) 'Before sepplotms'
         call sepplotms ( iinplt, rinplt, idum )
         if ( ierror/=0 ) go to 1000
         if ( debug ) write(irefwr,*) 'After sepplotms'

         if ( jtimes==2 ) then

!        --- jtimes = 2, plot the text "mesh" and close the plot

            call plscal
            call plafp7 ( ale*0.495d0, 0.01d0, 'MESH', 0d0, &
                          0.5d0, 4 )
            jtimes = 3
            call plafep ( 2, 0d0, 0d0, 0d0, 0d0, 0 )
            jtimes = 1

         end if

      end if

      if ( jnew .and. ioutp>=0 ) write(irefwr,900) npoint, nelem
900   format(' number of nodal points', i10/ &
             ' number of elements', i10)
1000  if ( debug ) then

!     --- Debug information

         call sepprinkmesh ( 1 )
         call sepprinkmesh ( 38 )
         write(irefwr,*) 'End mshcrt'

      end if  ! ( debug )
      call erclos ( 'mshcrt' )

      end subroutine mshcrt
