      subroutine msh012 ( jnew )
! ======================================================================
!
!        programmer    Guus Segal
!        version  8.1  date 25-11-2012 Long integers
!        version  8.0  date 02-09-2010 Remove intarmsh
!        version  7.6  date 27-10-2008 Extra debug
!
!   copyright (c) 1984-2012  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Make local information of surface isurnr permanent
!     fill positions of kmesh part h
! **********************************************************************
!
!                       KEYWORDS
!
!     mesh
!     surface
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulekmesh
      use sepmodulemesh
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      logical jnew
      integer :: i,j

!     jnew           i    indication whether the mesh is new (true) or old
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
! **********************************************************************
!
!                       SUBROUTINES CALLED
!

!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRININ         print 1d integer array
!     PRININ1        print 2d integer array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'msh012' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from msh012'
         write(irefwr,1) 'ndim, isurf, isurnr, npointlc', &
                          ndim, isurf, isurnr, npointlc
         write(irefwr,*) 'jnew', jnew
  1      format ( a, 1x, (10i6) )

      end if

!     --- Make local array coor permanent

      if ( associated(km(iseqkm)%kmhsur(isurnr)%coorsur) ) &
         deallocate(km(iseqkm)%kmhsur(isurnr)%coorsur)
      kmeshh(1,isurnr+1) = npointlc
         allocate(km(iseqkm)%kmhsur(isurnr)%coorsur(ndim,npointlc) )

      if ( ndim==3 .and. ( isurf>=1 .and. isurf<=4 .or. &
           (isurf==17 .or. isurf==19 .or. isurf==99) ) ) &
         then
         kmhsur(isurnr)%coorsur = coor3d(1:ndim,1:npointlc)
      else
         !kmhsur(isurnr)%coorsur = coor2d(1:ndim,1:npointlc)
         do i=1,npointlc
            do j=1,ndim
               kmhsur(isurnr)%coorsur(j,i)=coor2d(j,i)
            enddo
         enddo
      end if
      if ( debug ) then

         write(irefwr,1) 'isurnr, nbndpt', isurnr, nbndpt
         write(irefwr,1) 'npointlc', npointlc

      end if

      if ( jnew ) then

!     --- jnew = true   new mesh

         if ( debug ) then
            write(irefwr,1) 'ncurvs, isurf, nbndpt, inpelm, nelemlc', &
                             ncurvs, isurf, nbndpt, inpelm, nelemlc
            write(irefwr,1) 'nextrapnts, lenextracrvs', &
                             nextrapnts, lenextracrvs
         end if  ! ( debug )
         nbndpt = nbndpt+lenextracrvs+nextrapnts  ! kbndpt is extended

!        --- Make local array kmeshclc permanent

         kmeshh(6,isurnr+1) = inpelm
         kmeshh(2,isurnr+1) = nelemlc
         if ( kmhsur(isurnr)%filled ) &
            deallocate ( kmhsur(isurnr)%kmeshcsur )
         allocate ( kmhsur(isurnr)%kmeshcsur(inpelm*nelemlc) )
!orig        
         do i=1,inpelm*nelemlc
            kmhsur(isurnr)%kmeshcsur(i) = kmeshclc(i)
         enddo
         kmhsur(isurnr)%filled = .true.
         if ( .false. .and. debug ) &
            call prinin1 ( kmeshclc, nelemlc, inpelm, 'kmeshclc' )

!        --- Make local array kbndpt permanent

         if ( nbndpt>0 ) then

!        --- nbndpt>0, boundary
!            kbndpt is extended with one extra position
!            In the last position we store lenextracrvs

            kbndptlc(nbndpt+1) = lenextracrvs+nextrapnts
            if ( associated(kmhsur(isurnr)%kbndptsur) ) &
               deallocate (kmhsur(isurnr)%kbndptsur )
            allocate (kmhsur(isurnr)%kbndptsur(nbndpt+1) )
            kmeshh(4,isurnr+1) = nbndpt
            kmhsur(isurnr)%kbndptsur = kbndptlc(1:nbndpt+1)
            if ( debug ) call prinin ( kbndptlc, nbndpt, 'kbndptlc' )

         end if  ! ( nbndpt>0 )

         if ( kmeshh(5,isurnr+1)>0 ) &
            deallocate(km(iseqkm)%kmhsur(isurnr)%renumsur)
         if ( isurf==2 ) then

!        --- Surface is of type general, store irenum

            allocate(km(iseqkm)%kmhsur(isurnr)%renumsur(npointlc) )
            kmhsur(isurnr)%renumsur = irenum(1:npointlc)
            kmeshh(5,isurnr+1) = npointlc

         else if ( isurf==7 .or. isurf==11 .or. isurf==12 ) then

!        --- Surface is of type surfaces of surfaces, store irenum of length
!            ncurvs

            allocate(km(iseqkm)%kmhsur(isurnr)%renumsur(ncurvs) )
            kmhsur(isurnr)%renumsur = irenum(1:ncurvs)
            kmeshh(5,isurnr+1) = ncurvs

         end if

      end if
      if ( debug ) write(irefwr,*) 'End of msh012'
      call erclos ( 'msh012' )

      end subroutine msh012
