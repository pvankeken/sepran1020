      subroutine msh051 ( curveusp, ncurvs, iinput, ndim, jcoars, &
                          iseqcurvs, ipointcurv, lencurveusp )
! ======================================================================
!
!        programmer    Guus Segal
!        version  5.6  date 10-02-2015 Extension with ccircle
!        version  5.5  date 11-12-2014 Extension with lines with extra user pnts
!        version  5.4  date 14-05-2014 Change of error message
!
!   copyright (c) 1987-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill array curveusp with the user point numbers corresponding to the
!     curves
! **********************************************************************
!
!                       KEYWORDS
!
!     curve
!     mesh
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulecomio
      implicit none 
! **********************************************************************
!
!                       COMMON BLOCKS
!

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ncurvs, iinput(*), curveusp(*), ndim, jcoars, &
              iseqcurvs(*), ipointcurv(*), lencurveusp

!     curveusp       o    array containing the user points corresponding to
!                         the curves in the following way:
!                         curveusp(1)=ncurvs+1
!                         number of user points in curve i:
!                         curveusp(i+1)-curveusp(i)
!                         user points in curve i are stored in curveusp from
!                         position curveusp(i)+1 to curveusp(i+1)
!     iinput         i    integer input array containing information of the
!                         curves
!     ipointcurv     i    Array containing the relative starting addresses
!                         of all curves in the part of iinput referring
!                         to curves
!     iseqcurvs      i    Contains new sequence of curve numbers
!     jcoars         i    If 1 coarseness is given, if 0 not
!     lencurveusp   i/o   Length of array curveusp
!                         At input: estimated length
!                         At output: computed length
!     ncurvs         i    Number of curves in mesh
!     ndim           i    Dimension of the space.
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer iinp, i, icurve, nuserp, istart, ncrvs, ic, jcurve, &
              kcurve, nj, j, iprec, icprec, jinp, nprec, icurnr
      logical :: spline

!     i              Counting variable
!     ic             Relative subcurve number
!     icprec         Type of curve to be "copied"
!     icurnr         curve sequence number
!     icurve         Type of curve
!     iinp           last position used in array iinput
!     iprec          Curve number of curve to be copied
!     istart         starting position of curve iprec in kmeshm
!     j              Counting variable
!     jcurve         Curve number
!     jinp           last position used in array iinput with respect to iprec
!     kcurve         abs(jcurve)
!     ncrvs          Number of curves
!     nj
!     nprec
!     nuserp         Number 0f user points
!     spline         Indicates if preceding curve is a splin (true) or not
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     MSHFILNUMUSP   Fill array iwork with number of user points per curve
!     PRININ         print 1d integer array
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     124   code for generation of curves (icurve) incorrect
!     550   array length too short
!    1482   End user points of spline and spline to be copied not the same
! **********************************************************************
!
!                       PSEUDO CODE
!
!   trivial
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'msh051' )
      debug = .false. .and. ioutp>=0
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from msh051'
         write(irefwr,1) 'ncurvs', ncurvs
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

!     --- First fill number of user points in array curveusp

      call mshfilnumusp ( ncurvs, iseqcurvs, ipointcurv, ndim, &
                          jcoars, curveusp, iinput )

      if ( debug ) call prinin ( curveusp, ncurvs, 'curveusp' )

!     --- Construct curveusp positions 1 to ncurvs+1

      istart = ncurvs + 1
      do i = 1, ncurvs
         nuserp = curveusp(i)
         curveusp(i) = istart
         istart = istart+nuserp
      end do
      curveusp(ncurvs+1) = istart
      if ( istart>lencurveusp ) then

!     --- Error: Array lencurveusp too small

         call errint ( istart, 1 )
         call errint ( lencurveusp, 2 )
         call errchr ( 'curveusp', 1 )
         call errsub ( 550, 2, 0, 1 )
         go to 1000

      end if
      lencurveusp = istart

!     --- Finally fill rest of curveusp

      do i = 1, ncurvs
         icurnr = iseqcurvs(i)
         iinp = ipointcurv(icurnr)
         icurve = abs(iinput(iinp))
         nuserp = 0
         if ( icurve==0 .or. icurve==20 ) go to 750
         if ( icurve>=10000 .and. icurve<=10003 ) then
            nuserp = 2
            istart = iinp+2
            goto 750
         end if

         select case ( icurve )

         case ( 1, 3, 6, 8, 9, 15, 16, 22, 23 )

!        --- icurve = 1, 3, 5, 6, 8, 9, 15, 16, 22, 23

            nuserp = 2
            istart = iinp+3

         case ( 2 )

!        --- icurve = 2

            nuserp = iinput(iinp+2)+1
            istart = iinp+3

         case ( 4, 12, 21 )

!        --- icurve = 4, 12: splines
!            icurve = 21: framecurve

            nuserp = iinput(iinp+5)
            istart = iinp+5

         case ( 5 )

!        --- icurve = 5

            nuserp = iinput(iinp+3)
            istart = iinp+3

         case ( 7,14 )

!        --- icurve = 7, 14   curve of curves

            if ( icurve==7 ) then
               ncrvs = iinput(iinp+3)
            else
               ncrvs = iinput(iinp+6)
            end if
            nuserp = 0
            do ic = 1, ncrvs
               if ( icurve==7 ) then
                  jcurve = iinput(iinp+ic+3)
               else
                  jcurve = iinput(iinp+ic+6)
               end if
               kcurve = abs(jcurve)
               nj = curveusp(kcurve+1)-curveusp(kcurve)
               if ( jcurve>0 ) then

!              --- Copy forwards   jcurve>0

                  do j = 1, nj
                     curveusp(curveusp(icurnr)+j+nuserp)  = &
                        curveusp(curveusp(jcurve)+j)
                  end do

               else

!              --- Copy backwards   jcurve<0

                  do j = 1, nj
                     curveusp(curveusp(icurnr)+j+nuserp)  = &
                         curveusp(curveusp(kcurve+1)-j+1)
                  end do

               end if
               nuserp = nuserp+nj-1

            end do

            goto 800

         case ( 10, 11, 13 )

!        --- icurve = 10, 11 or 13 : translate, rotate or reflect

            nuserp = iinput(iinp+3)
            iprec  = iinput(iinp+4)
            istart = iinp+4

!           --- Determine type of preceding curve

            jinp = ipointcurv(iprec)
            icprec = iinput(jinp)
            nprec  = curveusp(iprec+1)-curveusp(iprec)
            spline = icprec==4 .or. icprec==12
            if ( nuserp<nprec .and. spline ) then

!           --- curve to be copied is a spline
!               In this case only the first and last user point correspond
!               to a nodal point

               if ( iinput(istart+nuserp)>0 ) then

!              --- Error: end points do not coincide

                  call errint ( icurnr, 1 )
                  call errint ( nuserp, 2 )
                  call errint ( iprec, 3 )
                  call errint ( nprec, 4 )
                  call errint ( icurnr, 5 )
                  call errint ( iprec, 6 )
                  call errsub ( 1482, 6, 0, 0 )

               end if
            end if

         case ( 17 )

!        --- icurve = 17 : circle

            nuserp = 1
            istart = iinp+4

         case ( 18 )

!        --- icurve = 18 : part of circle

            nuserp = 2
            istart = iinp+4

         case ( 19 )

!        --- icurve = 19 : arc for sphere

            nuserp = 2
            istart = iinp+5

         case ( 24 )

!        --- icurve = 24 : ccircle

            nuserp = iinput(iinp+3)-1
            istart = iinp+4

         case default

!        --- incorrect value, error

            call errint ( icurve, 1 )
            call errint ( 23, 2 )
            call errsub ( 124, 2, 0, 0 )
            go to 1000

         end select  ! case ( icurve )

!        --- Fill array curveusp

750      do j = 1, nuserp
            curveusp(curveusp(icurnr)+j) = iinput(istart+j)
         end do

800   end do

1000  call erclos ( 'msh051' )
      if ( debug ) then

!     --- Debug information

         call prinin ( curveusp, curveusp(ncurvs+1), 'curveusp' )
         write(irefwr,*) 'End msh051'

      end if  ! ( debug )

      end
