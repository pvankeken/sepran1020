      subroutine msh020 ( infel, jnew )
! ======================================================================
!
!        programmer    Guus Segal
!        version  7.5  date 15-11-2012 Long integers
!        version  7.4  date 18-08-2008 Fortran 90
!        version  7.3  date 07-11-2002 Skip empty surfaces
!
!   copyright (c) 1984-2012  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     fill arrays coor and kmeshc using the information of kmeshh
!     and infel
! **********************************************************************
!
!                       KEYWORDS
!
!     mesh
!     surface
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulekmesh
      use sepmodulemesh
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer infel(*)
      logical jnew

!     infel          i    array containing some information of how the elements
!                         created are stored on file 3
!     jnew           i    indication whether the mesh is new (true) or old
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ikelmc, icount, jsurnr, numnodes, &
              npoloc, nelloc, i, j, k, istart, &
              istmsr, nsubsf, isubsf, ielgeg(8), istore(10)
      integer, allocatable :: mapar(:)
      double precision, allocatable :: coorlc(:,:)

!     coorlc         local array containing the coordinates of submeshes
!     i              Counting variable
!     icount         Counter
!     ielgeg         array with number of nodes for each element shape
!                    ielgeg(i) corresponds to ishape-2
!     ikelmc         position in kmesh part c
!     istart         Starting address in KMESH part n
!     istmsr         Starting address in KMESH part r
!     istore         Work array to store the nodes of one element
!     isubsf         Sequence number of subsurface
!     j              Counting variable
!     jsurnr         Surface sequence number
!     k              Counting variable
!     mapar          mapping array: contains the mapping from local to
!                    global numbering of the nodal points
!     nelloc         Local number of elements
!     npoloc         Number of nodes in subsurface
!     nsubsf         Number of subsurfaces
!     numnodes       Number of nodes
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     DCOPY          Copy double precision dx to double precision dy
!     ERALLOC        Produce error message in case allocate went wrong
!     ERCLOS         Resets old name of previous subroutine of higher level
!     ERDEALLOC      Produce error message in case deallocate went wrong
!     EROPEN         Produces concatenated name of local subroutine
!     PRININ         print 1d integer array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data ielgeg / 3, 6, 4, 9, 7, 10, 5, 4 /
! ======================================================================
!
      call eropen ( 'msh020' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from msh020'
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000
      ikelmc = 1
      numnodes = 0
      if ( nvolms==0 ) npomax = npomxs

      allocate ( mapar(npomax), coorlc(ndim,npomax), stat = error )
      if ( error/=0 ) call eralloc ( error, npomax, 'mapar' )
      if ( ierror/=0 ) go to 1000

      if ( jnew .and. infel(2)>0 ) then

!     --- infel(2)>0   line elements

         length = infel(2)
         kmeshc(ikelmc:ikelmc+length-1) = kmeshcline(1:length)
         ikelmc = ikelmc+length
         numnodes = infel(4)
         coor(:,1:numnodes) = coorline

      else

!     --- Line elements in changing mesh

         numnodes = infel(4)

      end if

      if ( debug ) write(irefwr,1) 'numnodes', numnodes

!     --- surface and volume elements

      if ( nsurvl>0 ) then

!     --- nsurvl>0, surface and/or volume elements

         do icount = 1, nsurvl
            isurnr = infel(icount+4)

            if ( debug ) then

!           --- debugging statements

               write(irefwr,1) 'icount, isurnr, filled', &
                                icount, isurnr, kmhsur(isurnr)%filled

            end if
            if ( isurnr==0 ) go to 100

            if ( kmhsur(isurnr)%filled ) then

!           --- Kmeshh(2,isurnr+1)>0, i.e. standard surface or volume

               npoloc = kmeshh(1,isurnr+1)
               mapar(1:npoloc) = kmhsur(isurnr)%maparsur(1:npoloc)
               if ( debug ) then

!              --- debugging statements

                  call prinin ( mapar, npoloc, 'mapar_2' )

               end if
               if ( jnew ) then

!              --- jnew = true   new mesh

                  nelloc = size(kmhsur(isurnr)%kmeshcsur)
!orig               kmeshc(ikelmc:ikelmc+nelloc-1) = kmhsur(isurnr)%kmeshcsur
                  do i=1,nelloc
                     kmeshc(ikelmc+i-1) = kmhsur(isurnr)%kmeshcsur(i)
                  enddo

!                 --- change contents of kmeshc

                  ikelmc = ikelmc-1
                  do i = 1, nelloc
                     kmeshc(ikelmc+i) = mapar(kmeshc(ikelmc+i))
                  end do

                  ikelmc = ikelmc+1
                  ikelmc = ikelmc+nelloc

               end if
               length = kmeshh(1,isurnr+1)*ndim
               call dcopy ( length, kmhsur(isurnr)%coorsur, 1, coorlc, 1 )
               do i = 1, npoloc
                  j = mapar(i)
                  if ( j>numnodes ) then

!                 --- j>numnodes

                     numnodes = numnodes+1
                     do k = 1, ndim
                        coor(k,numnodes) = coorlc(k,i)
                     end do

                  end if

               end do

            else if ( nvolms>0 ) then

!           --- Kmeshh(2,isurnr+1) = 0, i.e. surface of surfaces
!               All information has already been stored in array KMESHN
!                  Since volumes are present kmeshn may be used

               if ( jnew ) then

!              --- Mesh is new, fill kmeshc

                  inpelm = kmeshn(isurnr)
                  nelloc = kmeshn(isurnr+nsurfs)

!                 --- Compute starting address in array KMESHN with respect to
!                     this surface

                  istart = 2*nsurfs
                  do i = 1, isurnr-1
                     istart = istart + kmeshn(i) * kmeshn(i+nsurfs)
                  end do

!                 --- Copy information from kmeshn into kmeshc

                  do i = 1, nelloc*inpelm
                     kmeshc(ikelmc-1+i) = kmeshn(istart+i)
                  end do

!                 --- Raise ikelmc

                  ikelmc = ikelmc + inpelm * nelloc

               end if
               istmsr = kmeshr(isurnr)
               nsubsf = kmeshr(isurnr+1) - istmsr
               if ( nsubsf>0 ) then

!              --- nsubsf>0: surfaces of surfaces
!                  Loop over subsurfaces

                  do isubsf = 1, nsubsf

!                 --- Read local mapping array into iwork

                     isurnr = abs( kmeshr(istmsr+isubsf) )
                     nelloc = kmeshn(isurnr+nsurfs)
                     npoloc = kmeshh(1,isurnr+1)
                     call dcopy ( npoloc*ndim, kmhsur(isurnr)%coorsur, &
                                  1, coorlc, 1 )
                     mapar(1:npoloc) = kmhsur(isurnr)%maparsur(1:npoloc)

                     do i = 1, npoloc
                        j = mapar(i)
                        numnodes = max(numnodes,j)
                        do k = 1, ndim
                           coor(k,j) = coorlc(k,i)
                        end do

                     end do

                  end do

               end if

            else

!           --- Kmeshh(2,isurnr+1) = 0, i.e. surface of surfaces
!               Since no volumes are present kmeshn may not be used

               if ( jnew ) then

!              --- Mesh is new, fill kmeshc

                  istmsr = kmeshr(isurnr)
                  nsubsf = kmeshr(isurnr+1) - istmsr
                  do isubsf = 1, nsubsf

!                 --- Read local mapping array into iwork

                     jsurnr = kmeshr(istmsr+isubsf)
                     isurnr = abs(jsurnr)
                     npoloc = kmeshh(1,isurnr+1)
                     mapar(1:npoloc) = kmhsur(isurnr)%maparsur(1:npoloc)

                     if ( debug ) then

!                    --- debugging statements

                        write(irefwr,*) 'isubsf, jsurnr, npoloc ', &
                                         isubsf, jsurnr, npoloc

                     end if

                     if ( jnew ) then

!                    --- jnew = true   new mesh

                        nelloc = size(kmhsur(isurnr)%kmeshcsur)
                        kmeshc(ikelmc:ikelmc+nelloc-1) = &
                           kmhsur(isurnr)%kmeshcsur

!                       --- change contents of kmeshc

                        if ( jsurnr>0 .or. ndim==2 ) then

!                       --- Standard case, subsurface in positive direction
!                           In case of R^2 all surfaces have been ordered
!                           counter-clockwise

                           ikelmc = ikelmc-1
                           do i = 1, nelloc
                              kmeshc(ikelmc+i) = mapar(kmeshc(ikelmc+i))
                           end do
                           ikelmc = ikelmc+1
                           ikelmc = ikelmc+nelloc

                        else

!                       --- Special case, subsurface in negative direction
!                           Store kmeshc in reversed direction

                           ishape = kmeshs(nsurfs+1+(isurnr-1)*8+2)
                           inpelm = ielgeg(ishape-2)
                           ikelmc = ikelmc-1

                           if ( debug ) then

!                          --- debugging statements

                              write(irefwr,*) 'ishape, inpelm, nelloc ', &
                                               ishape, inpelm, nelloc

                           end if

                           do i = 1, nelloc/inpelm
                              if ( ishape<=5 ) then

!                             --- ishape = 3, 4, 5 all points must be reversed
!                                 start with first one

                                 istore(1) = mapar(kmeshc(ikelmc+1))
                                 do j = 2, inpelm
                                    istore(inpelm+2-j) = &
                                       mapar(kmeshc(ikelmc+j))
                                 end do

                              else

!                             --- ishape>5 last point must be kept

                                 istore(1) = mapar(kmeshc(ikelmc+1))
                                 do j = 2, inpelm-1
                                    istore(inpelm+1-j) = &
                                       mapar(kmeshc(ikelmc+j))
                                 end do
                                 istore(inpelm) = &
                                       mapar(kmeshc(ikelmc+inpelm))

                              end if
                              do j = 1, inpelm
                                 kmeshc(ikelmc+j) = istore(j)
                              end do
                              ikelmc = ikelmc+inpelm
                           end do
                           ikelmc = ikelmc+1

                        end if

                     end if
                     length = kmeshh(1,isurnr+1)*ndim
                     call dcopy ( length, kmhsur(isurnr)%coorsur, 1, coorlc, 1 )
                     do i = 1, npoloc
                        j = mapar(i)
                        if ( j>numnodes ) then

!                       --- j>numnodes

                           numnodes = numnodes+1
                           do k = 1, ndim
                              coor(k,numnodes) = coorlc(k,i)
                           end do

                        end if

                     end do

                  end do

               end if

            end if

100      end do

      end if

      deallocate ( mapar, coorlc, stat = error )
      if ( error/=0 ) call erdealloc ( error, 'mapar' )

1000  if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End msh020'

      end if  ! ( debug )
      call erclos ( 'msh020' )
      end subroutine msh020
