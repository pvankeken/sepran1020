c *************************************************************
c *   SOLINT
c *
c *   Interpolation of a solutionvector from one mesh to another.
c *   
c *   Input:
c *        ICHOICE        Choice parameter:
c *                       1  Linear triangles, 1 dof
c *                       2  Quadratic triangles, 2 dof's
c *        FILE_SOL1      Name of the bsfile containing the
c *                       solution vector to be interpolated
c *        NREC1          Record number of solution vector on FILE_SOL1
c *        FILE_MESH1     File containing the mesh on which the
c *                       vector is defined
c *        FILE_MESH2     File containing the mesh on which the
c *                       solutionvector has to be interpolated
c *        FILE_SOL2      Name of bsfile on which the output is
c *                       written
c *        NREC2          Corresponding record number
c *
c *   PvK 260690
c *   PvK 040405  Modernized...
c *************************************************************
      program solint
      implicit none
      integer NBUFDEF
      parameter(NBUFDEF=10 000 000)
      integer kmeshold(400),kmeshnew(400),kprobold(400),kprobnew(400)
      integer kmeshdum(400)
      integer isol1(5),isol2(5)
      integer nrec1,nrec2
      character*80 file_mesh1,file_mesh2,file_sol1,file_sol2
      integer ichoice,ncntln,numarr,iinput,jchoice,iprint
      include 'bstore.inc'
      include 'plotpvk.inc'
      real*8 format,yfac,contln
      logical single_mesh_file
      data kmeshold(1),kmeshnew(1),kprobnew(1),kprobold(1)/4*400/
      data kmeshdum(1)/400/

      read(5,*) jchoice
      read(5,*) file_mesh1,file_mesh2
      read(5,*) file_sol1,nrec1
      read(5,*) file_sol2,nrec2

      single_mesh_file = (jchoice/10)*10.eq.1
      ichoice = jchoice-(jchoice/10)*10

      call start(0,1,0,0)
      if (single_mesh_file) then
c        *** Read old mesh file
         open(9,file=file_mesh1,form='formatted')
         call meshrd(-2,9,kmeshdum)
         close(9)
         call nstmsh(kmeshdum,kmeshold,iprint)
c        *** read new mesh file
         open(9,file=file_mesh2,form='formatted')
         call meshrd(-2,9,kmeshdum)
         call nstmsh(kmeshdum,kmeshnew,iprint)
      else 
         open(9,file=file_mesh1,form='formatted')
         call meshrd(-2,9,kmeshold)
         close(9)
         open(9,file=file_mesh2,form='formatted')
         call meshrd(-2,9,kmeshnew)
         close(9)
      endif
         
      call probdf(0,kprobold,kmeshold,iinput)
      call probdf(0,kprobnew,kmeshnew,iinput)
     
      f2name = file_sol1
      call openf2(.false.)
      call readbs(nrec1,isol1,kprobold)
      call intmsh(0,kmeshold,kmeshnew,kprobold,kprobnew,isol1,isol2)
 
      format=10d0
      if (ichoice.eq.1) then
        call plotc1(1,kmeshold,kprobold,isol1,contln,0,format,yfac,0)
        call plotc1(1,kmeshnew,kprobnew,isol2,contln,0,10d0,1d0,0)
      else
        call plotvc(1,2,isol1,isol1,kmeshold,kprobold,format,yfac,0d0)
        call plotvc(1,2,isol2,isol2,kmeshnew,kprobnew,format,yfac,0d0)
      endif
      f2name = file_sol2
      if (nrec2.eq.1) then
        call openf2(.true.)
      else
        call openf2(.false.)
      endif
      call writbs(0,nrec2,numarr,'isol2',isol2,kprobnew)
      call writb1

      end

