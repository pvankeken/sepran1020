c *************************************************************
c *   PEFMESH
c *   Fill arrays iinput and rinput to be used in the mesh
c *   creation. The mesh is built by meshgenerator RECTANGLE
c *   for a rectangular geometry ([0,xl]x[0,yl]). To include
c *   refinements in the 
c *************************************************************
      subroutine pefmesh(iinput,rinput,nx,ny,xl,yl,fx,fy,ichois)
      implicit none
      integer iinput(*),nx,ny,ichois
      real*8 rinput(*),xl,yl,fx,fy
      real*8 x(2000),y(2000)
      integer ipsurf,nuspnt,ipbase,n,nrleng,np,irleng,ipcurv,npcurv
      integer ic,iileng,icrv,n1,i,ip,n2,n3
      real*8 xcor,ycor,xx,yy,dytus,dxtus,dy,dx
      data ipsurf/0/
      save ipsurf

      if (fx.eq.0d0) fx=1d0
      if (fy.eq.0d0) fy=1d0
      if (xl.le.0d0.or.yl.le.0d0) then
         write(6,*) 'PERROR(pefmesh): xl <= 0 or yl <= 0'
      endif
      if ((nx/2)*2.ne.nx.or.(ny/2)*2.ne.ny) then 
         write(6,*) 'PERROR(pefmesh): nx and/or ny are odd'
         write(6,*) '                 nx,ny = ',nx,ny
      endif
      if (ichois.eq.2.and.ipsurf.eq.0) then
         write(6,*) 'PERROR(pefmesh): ichois=2 and ipsurf=0'
      endif

      nuspnt    = 4*nx + 4*ny
      iileng = nuspnt + 7 + 8*4 + 20 + 13
      irleng = 7+2*nuspnt
      if (iinput(1).lt.iileng) then
         write(6,*) 'PERROR(pefmesh): array iinput too small'
         write(6,*) '                 computed length = ',iileng
      endif
      if (rinput(1).lt.irleng) then
         write(6,*) 'PERROR(pefmesh): array rinput too small'
         write(6,*) '                 computed length = ',irleng
      endif

      if (ichois.eq.1) then
c        *** Fill iinput and rinput for the first time.
c        *** Eight curves are defined by userpoints; RECTANGLE
c        *** creates one surface with quadratic triangles. 
         do i=2,20
            iinput(i) = 0
         enddo
         iinput(6) = 2
         iinput(7) = nuspnt
         iinput(8) = 8
         iinput(9) = 1
         iinput(12)= 1
	 iinput(15)= 2

         ipcurv = 21
         ipbase = 1
         do icrv = 1,8
c           *** pointer to curve information in iinput
            if (icrv.eq.1.or.icrv.eq.2.or.icrv.eq.5.or.icrv.eq.6) then
               n = nx
            else
               n = ny
            endif
            iinput(ipcurv)   = 2
            iinput(ipcurv+1) = 1
            iinput(ipcurv+2) = n
            iinput(ipcurv+3) = 0
            do ip = 1,n+1
               iinput(ipcurv+3+ip) = ipbase+ip-1
            enddo
            ipcurv = ipcurv + 4 + n + 1
            ipbase = ipbase + n
         enddo
c        *** correct last user point number
         iinput(ipcurv-1) = 1        
         ipsurf = ipcurv
         iinput(ipsurf)   = 1
         iinput(ipsurf+1) = 4
         iinput(ipsurf+2) = 8
         do ic = 1,8
            iinput(ipsurf+2+ic) = ic
         enddo
         iinput(ipsurf+11) = nx
         iinput(ipsurf+12) = ny
         iinput(ipsurf+13) = 4
         iinput(ipsurf+14) = 1
         iinput(ipsurf+15) = 1
         iinput(ipsurf+16) = 1

c        *** Fill RINPUT with the coordinates of the user points
c        *** Calculate the x-coordinates of curve 1 and 
c        *** the y-coordinates of curve 3. The length of the
c        *** largest element can be solved from
c        *** 
c        *** n = nx/2  
c        *** (fx**(n-1) + fx**(n-2) + .. + fx + 1 = xl/2
c        ***
c        *** or
c        ***
c        *** n = ny/2
c        *** (fy**(n-1) + fy**(n-2) + .. + fy + 1 = yl/2
c        ***
         xx = 0d0
         n  = nx/2
         np = nx+1
         do i=1,n
            xx = xx + fx**(i-1)
         enddo
         dx = 0.5*xl/xx
         x(1) = 0.
         xcor = x(1)
         xx   = fx**(n-1)
         ip   = 2
         dxtus= dx*xx
60       continue
            x(ip)   = xcor + dxtus/2
            x(ip+1) = xcor + dxtus
            xcor    = xcor + dxtus
            dxtus   = dxtus/fx
            ip      = ip+2
         if (ip.lt.np) goto 60

         yy = 0d0
         n = ny/2
         np= ny+1
         do  i=1,n
            yy = yy + fy**(i-1)
         enddo
         dy = 0.5*yl/yy
         y(1) = 0.
         ycor = y(1)
         yy   = fy**(n-1)
         dytus= dy*yy
         ip   = 2
80       continue
            y(ip)   = ycor + dytus/2
            y(ip+1) = ycor + dytus
            ycor    = ycor + dytus
            dytus   = dytus/fy
            ip      = ip+2
         if (ip.lt.np) goto 80

c        *** Mirror image the arrays 
         do ip=1,nx
            x(nx+ip+1) = xl - x(nx-ip+1)
         enddo
         do ip=1,ny
            y(ny+ip+1) = yl - y(ny-ip+1)
         enddo
     
         do ip=2,7
            rinput(ip)=0d0
         enddo
         ipbase = 8
         do ip=1,2*nx+1
            xx = x(ip)
            yy = 0d0         
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
            ipbase = ipbase+2
         enddo
         do ip=2,2*ny+1
            xx = xl
            yy = y(ip)
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
            ipbase = ipbase+2
         enddo

         do ip=2*nx,1,-1
            xx = x(ip)
            yy = yl
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
            ipbase = ipbase+2
         enddo
         do ip=2*ny,2,-1
            yy = y(ip)
            xx = 0d0
c           write(6,*) ipbase,xx,yy
            rinput(ipbase)   = xx
            rinput(ipbase+1) = yy
            ipbase = ipbase+2
         enddo


      else if (ichois.eq.2) then
c        *** Use the same nodalpoint distribution and RECTANGLE to
c        *** create mesh with linear triangles
         iinput(ipsurf+1)  = 3
         iinput(ipsurf+11) = nx*2
         iinput(ipsurf+12) = ny*2
         iinput(ipsurf+13) = 3
         iinput(ipsurf+14) = 1
         iinput(ipsurf+15) = 1
         iinput(ipsurf+16) = 1

      else if (ichois.eq.3) then
c        *** Use the same nodal point distribution and RECTANGLE to
c        *** create mesh with bilinear quadrilaterals
         iinput(ipsurf+1)  = 5
         iinput(ipsurf+11) = nx*2
         iinput(ipsurf+12) = ny*2
         iinput(ipsurf+13) = 5
         iinput(ipsurf+14) = 1
         iinput(ipsurf+15) = 1
         iinput(ipsurf+16) = 1
      
      else
         write(6,*) 'PERROR(pefmesh): unknown value for ICHOIS'
         write(6,*) '                 ICHOIS = ',ichois
      
      endif
         
      return
      end
