      program makemesh
      implicit none
      integer iinput,kmesh
      real*8 rinput
      dimension iinput(10000),rinput(10000),kmesh(400)
      include 'SPcommon/cplot'
      integer nx,ny
      real*8 fx,fy
      dimension nx(3),ny(3),fx(3),fy(3)
      character*80 fmame
      real*8 xl,yl,format,yfact
      integer ichois,i,iuser

      iinput(1)=10000
      rinput(1)=10000
      kmesh(1)=400
      
      call start(0,1,-1,0)
      yl=1
      yfact=1d0
      jmark=5
      format=10d0
      read(5,*) xl,ichois
      write(6,*) 'xl, ichois=',xl,ichois
      read(5,*) (nx(i),i=1,3),(fx(i),i=1,2)
      read(5,*) (ny(i),i=1,3),(fy(i),i=1,2)
      write(6,*) 'nx: ',(nx(i),i=1,3),(fx(i),i=1,2)
      write(6,*) 'ny: ',(ny(i),i=1,3),(fy(i),i=1,2)
      call pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,1)
      call mesh(1,iinput,rinput,kmesh)
      call plotm2(0,kmesh,iuser,format,yfact)
      open(9,file='mesh1',form='formatted')
      rewind(9)
      call meshwr(9,kmesh)
      call pefm2(iinput,rinput,nx,ny,xl,yl,fx,fy,2)
      call mesh(1,iinput,rinput,kmesh)
      open(9,file='mesh2')
      rewind(9)
      call meshwr(9,kmesh)

      call finish(0)
      end
