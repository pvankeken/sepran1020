       program convec
       implicit none

       call sepcom(0)
  
       end

       real*8 function func(ichois,x,y,z)
       implicit none
       real*8 x,y,z,pi
       parameter(pi=3.1415926)
       integer ichois

       if (ichois.eq.1) then
          func = 1-y + 0.1*cos(pi*x)*sin(pi*y)
       else if (ichois.eq.2) then
          func = cos(pi*x)*sin(pi*y)
       else
          func = -sin(pi*x)*cos(pi*y)
       endif
  
       return 
       end

       subroutine funalg(val1,val2,val3)
       implicit none
       real*8 val1,val2,val3
       val3 = val1*val1 + val2*val2
 
       return
       end
       
       
