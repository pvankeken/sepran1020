
	rayleigh=1.00e4			# Rayleigh number

	maxstep=10000                    # maximum number of time steps
        finetunedt=1.00			# tuning courant condition

	datafile="ra1e4_ar2sqrt2"
	storage_spacing=25 		# write data every ... output. Set to something meaningful in 
	write_spacing=25                # regard to maxstep

	TDEPV=off			# Turns on/off whether to use rheological parameters
	rheology_T_type=1 		# 1:  visc=n0*exp(-T*T1) r =n0*exp((E+Zz)/T1*(T+T0))
	viscT1=1.0    			# T1 in above expression
	viscN0=1.0                      # N0 in above expression

        dimenx=1.                       # non-dimensional size of the mesh
        mgunitx=2                       # multigrid unit cell
        levels=7

        perturbk=0.50                   # wave number of initial perturbation
        
#      previous_temperature_file="restart.node_data"

#------------------------------------------------------------#

	DESCRIBE=off
	VERBOSE=off    			# on the values as they are read in.	
	verbose=off   			# Verbose behaviour for the code (debugging)
	AVS=off
	CONMAN=off			# Output file for CONMAN geometry 
	COMPRESS=off			#
	see_convergence=5  

# REQUIRED INFORMATION:
	

# 	previous_temperature_file="Citcom-test1-65x65.10000.node_data"   	
# 	previous_water_file=""	
#	previous_chemistry_file=""	

#	datatypes="Temp,Tfft,Strf,Velo,Pres,Velx,Velz,Visc,Strf"
	datatypes="Temp"
 
	averages="Temp,Visc"
	timelog="Nuss,Svav,Vrms"
#	observables="Tpgx,Tpbx,Grvx,Grbx,Geox,Gebx,Vxsf"
        observables=""


	mgunitx=2			# Multigrid unit cell dimensions (elements)
	mgunitz=2			#
	mgunity=2			#
	pmg_levels=3
	

# Output
	
	
	substep_factor=0.2
	maxsub=25

# Velocity
	
	Velocity_x_bc_rect=0
	Velocity_x_bc_rect_aa1=0.0
	Velocity_x_bc_rect_aa2=1.0
	Velocity_x_bc_rect_hw=1.0
	Velocity_x_bc_rect_mag=1.0
	Velocity_x_bc_rect_icpt=0.0
	Velocity_x_bc_rect_norm=Z

# Temperature Field

	Temp_rect=0			# Blank Background
	Temp_rect_x1=0.0
	Temp_rect_x2=0.5
	Temp_rect_z1=0.0
	Temp_rect_z2=0.5
	Temp_rect_hw=0.01
	Temp_rect_mag=0.9
	Temp_rect_ovl="M"


# Initial perturbations
	
	num_perturbations=1		# How many different perturbations in the list
	perturbmag=0.0,0.00		# The premultiplier
#perturbk=1.0,0.25		# Wavenumber (will get multiplied by pi)
	perturbky=0.0,0.0		#


# RHEOLOGICAL DEFINITIONS


	Viscosity=system		# 

	rheological_components=1	# Derives the viscosity field from the system state at each time
						
#viscN0=1.0,0.1585,0.1
#viscT0=0.1,0.1
	viscE=2.026,1.0	
	viscZ=0.0,3.0	

	Trange_min=-0.1,-0.1			# This rheology only applies in this range of temperatures
	Trange_max=2.0,2.0

	VMIN=off,off			# cutoff values
	visc_min=0.001,0.001
	VMAX=off,off
	visc_max=1.0e32,1.0e32


	SDEPV=off   			# Stress dependence of viscosity (valid with viscosity=system)
	sdepv_expt=1.0,10.0		# Exponent in stress/strain-rate reln (1.0, use "off" above)
	sdepv_max_effect=1.0e32

	YIELD_STRESS=off		# Yield stress parameters
	yield_stress_B0=5e5		# Offset at surface
	yield_stress_Bz=1e6		# Depth relation
	yield_hysteresis=0.01


# Compositional stuff

	CHEM=off
	Chem_rect=0
	Chem_rect_x1=0.0,0.0
	Chem_rect_z1=0.0,0.0
	Chem_rect_y1=0.0,0.5 
	Chem_rect_x2=1.0,1.0
	Chem_rect_z2=0.02,0.06
	Chem_rect_y2=0.5,1.0
	Chem_rect_hw=0.0001,0.0001
	Chem_rect_mag=1.0,1.0
	Chem_rect_ovl=R,R
	
	WATER=off
	H_2O_diff=0.0				# diffusivity relative to T
	H_2O_visc_N0=0.01			# delta eta from presence of trace of water
	H_2O_yield_stress_weaken=1.0		# premultiplier for yield stress term	
	H2ODEPV=off				# turn on or off the viscosity effect of water

	H_2O_bc_rect=0				# Boundary conditions on water
	H_2O_bc_rect_aa1=0.0			# location on `the edge in question'
	H_2O_bc_rect_aa2=10.0			#
	H_2O_bc_rect_hw=0.0			# smooth edge of this bc rectangle -> this is half width
	H_2O_bc_rect_mag=1			# value of bc
	H_2O_bc_rect_icpt=0.0			# To determine which edge, what is intercept with the axis
	H_2O_bc_rect_norm=Z			# and which axis is normal to the edge in question

	H_2O_fixed_rect=0			# Similar to above but this is for internal fixed values
	H_2O_fixed_rect_x1=0.0
	H_2O_fixed_rect_x2=10.0
	H_2O_fixed_rect_z1=0.5
	H_2O_fixed_rect_z2=1.0
	H_2O_fixed_rect_hw=0.01
	H_2O_fixed_rect_mag=0.0
	H_2O_fixed_rect_ovl="R"			# This says how to combine the values if they overlap (R=replace)

# GRID POINTS:

#       dimenx=1.0 			# Non-dimensional size of mesh
	dimenz=1.0			#
 	dimeny=2.83			#

	spacing_z=region		# Grid compression
	regions_z=0
	region_offset_z=0.0,1.0
	region_compression_z=0.5,0.5
	region_width_z=0.1,0.1 
	region_edge_width_z=0.1,0.1 

	spacing_x=region
	regions_x=0
	region_offset_x=0.0
	region_compression_x=0.8
	region_width_x=0.08 
	region_edge_width_x=0.05 



# BOUNDARY CONDITIONS
	
	topvbc=0 botvbc=0 		#
	toptbc=1 bottbc=1		#
	topvbxval=0.0 topvbyval=0.0	#
	toptbcval=0.0 bottbcval=1.0	#
 
        Heat_flux_z_bc_rect=0
        Heat_flux_z_bc_rect_aa1=0.0     # lateral extent of boundary condition
        Heat_flux_z_bc_rect_aa2=6.0  
        Heat_flux_z_bc_rect_bb1=0.0     # lateral extent of boundary condition in 3rd dimension
        Heat_flux_z_bc_rect_bb2=1.0  
        Heat_flux_z_bc_rect_hw=0.01     # smoothness of edge
        Heat_flux_z_bc_rect_mag=0.0     # value of boundary condition
        Heat_flux_z_bc_rect_icpt=1.0    # z coord of face on which boundary condition is applied
        Heat_flux_z_bc_rect_norm=Z      # Normal to this face (cf stress bc's which can apply on all boundaries)

	periodicx=off 			# 2D only
	periodicy=off			# Doesn't work

  
	free_upper=off			# Free upper surface (pseudo)
	free_lower=off			#
	initial_isostasy=off		# Set initial topography from stress-balance

         
# INTERNAL HEATING
 
        heating_elements=0              # components of the heat source term
        heating_time_offset=0.0         # time from start of decay
        heating_Qs=4.0                  # value of heating coefficient
        heating_lambdas=0.0             # decay rate for this element
 

# ADVECTION-DIFFUSION PARAMETERS

	Problem=convection		# which built in problem
	Geometry=cart2d 		# cart2d,cart3d at the moment
	Solver=multigrid
	# aug_lagrangian=1.0e4		# zero is off


# other stuff
	

# SOLVER RELATED MATTERS  (HAVE SENSIBLE DEFAULTS & YOU SHOULD BE CAREFUL CHANGING THESE)



	mg_cycle=1			# 1 = V cycle, 2 = W cycle, and so on.
	vel_relaxations=9

	delta_accuracy_factor=1.0	# change in accuracy level->level (>~1 for nonN, <~1 for Newt)

	piterations=100 		# Maximum Uzawa iteration loops.

	accuracy=1.0e-3         	# Desired accuracy of Uzawa algorithm. 


#finetunedt=0.75			# tuning courant condition
