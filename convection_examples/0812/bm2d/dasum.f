      function dasum(n, dx, incx)
c
c        version 1.1    date   22-08-91 (Zdenek: no dimension, cdc, ce)
c
c     returns sum of magnitudes of double precision dx.
c     dasum = sum from 0 to n-1 of abs(dx(1+i*incx))
c
      implicit double precision (a-h,o-z)
      double precision dx(*)

      dasum = 0d0
      if (n.le.0) return
      if (incx.eq.1) go to 20
c
c        code for increments not equal to 1.
c
      ns = n*incx
      do 10 i=1,ns,incx
         dasum = dasum + abs(dx(i))
   10 continue
      return
c
c        code for increments equal to 1.
c
c
c        clean-up loop so remaining vector length is a multiple of 6.
c
   20 m = mod(n,6)
      if (m.eq.0) go to 40
      do 30 i=1,m
         dasum = dasum + abs(dx(i))
   30 continue
      if (n.lt.6) return
   40 mp1 = m + 1
      do 50 i=mp1,n,6
         dasum = dasum + abs(dx(i)) + abs(dx(i+1)) + abs(dx(i+2)) +
     1    abs(dx(i+3)) + abs(dx(i+4)) + abs(dx(i+5))
   50 continue
      return
      end
