
c ************************************************************
c *   PEFILXY
c *
c *   Fill the coordinates of the primary nodes of a rectangular
c *   mesh in common /pexcyc/
c *   
c *
c *   PvK 2/1/99 Updated to allow for more than one curve along
c *              either the bottom or side boundary. Information
c *              on the curve definition should be stored in 
c *              cfilxy.inc
c *************************************************************
      subroutine pefilxy(ishape,kmesh,kprob,isol)
      implicit none
      integer ishape,kmesh(*),kprob(*),isol(*)
      include 'pexcyc.inc'
      include 'cfilxy.inc'

      real*8 funcx,funcy,dx,dy,dxmin,dymin
      integer icurvs,number,i,ic,nc
      dimension funcx(2005),icurvs(12)

      if (ifilchoice.eq.0.or.ifilchoice.eq.1) then
         call oldpefilxy(ishape,kmesh,kprob,isol)
         return
      endif

c     *** Bottom curve first
      nc = ncurvn(1)

      if (nc.eq.1) then
         funcx(1) = 2005
         icurvs(1)=0
         icurvs(2)=icurv(1,1)
      else
         funcx(1) = 2005
         icurvs(1)=nc
         do i=1,nc
            icurvs(i+1) = icurv(1,i)
         enddo
      endif
      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (funcx(5)/2.gt.NXCYCMAX) then
         write(6,*) 'PERROR(pefilxy) Mesh is too large in x-direction.'
         write(6,*) 'Limit number of x-nodal points to NXCYCMAX'
         write(6,*) 'nx       = ',funcx(5)/2
         write(6,*) 'NXCYCMAX = ',NXCYCMAX
      endif
      if (ishape.eq.1) then
c        *** linear elements
         nx = funcx(5)/2
         do i=1,nx
            xc(i) = funcx(4+2*i)
         enddo
      else if (ishape.eq.2) then
c        *** Quadratic elements
         nx = funcx(5)/4+1
         do i=1,nx
            xc(i) = funcx(2+4*i)
         enddo
      endif

c     *** Now the side curve 
      nc = ncurvn(2)
      if (nc.eq.1) then
         funcx(1) = 2005
         icurvs(1)=0
         icurvs(2)=icurv(2,1)
      else
         funcx(1) = 2005
         icurvs(1)=nc
         do i=1,nc
            icurvs(i+1) = icurv(2,i)
         enddo
      endif

      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (funcx(5)/2.gt.NXCYCMAX) then
         write(6,*) 'PERROR(pefilxy) Mesh is too large in y-direction.'
         write(6,*) 'Limit number of y-nodal points to NXCYCMAX'
         write(6,*) 'nx       = ',funcx(5)/2
         write(6,*) 'NXCYCMAX = ',NXCYCMAX
      endif
      if (ishape.eq.1) then
c        *** linear elements
         ny = funcx(5)/2
         do i=1,ny
            yc(i) = funcx(5+2*i)
         enddo
      else if (ishape.eq.2) then
c        *** Quadratic elements
         ny = funcx(5)/4+1
         do i=1,ny
            yc(i) = funcx(3+4*i)
         enddo
      endif

c     *** Finalize the data in /pexcyc/
      xcmin = xc(1)
      ycmin = yc(1)
      xcmax = xc(nx)
      ycmax = yc(ny)

      dxmin = xc(2)-xc(1)
      do ic=2,nx-1
         dx = xc(ic+1)-xc(ic)
         dxmin = min(dxmin,dx)
      enddo
 
      dymin = yc(2)-yc(1)
      do ic=2,ny-1
         dy = yc(ic+1)-yc(ic)
         dymin = min(dymin,dy)
      enddo
      
      if (ishape.eq.2) then 
         dxmin = dxmin/2 
         dymin = dymin/2
      endif


      return
      end

c ************************************************************
c *   OLDPEFILXY
c *
c *   Fill the coordinates of the primary nodes of a rectangular
c *   mesh in common /pexcyc/
c *   
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine oldpefilxy(ishape,kmesh,kprob,isol)
      implicit none
      integer ishape,kmesh(*),kprob(*),isol(*)
      include 'pexcyc.inc'

      real*8 funcx,funcy,dx,dy,dxmin,dymin
      integer icurvs,number,i,ic
      dimension funcx(1005),icurvs(2)

      funcx(1) = 1005
      icurvs(1)=0
      icurvs(2)=1
      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (funcx(5)/2.gt.NXCYCMAX) then
         write(6,*) 'PERROR(pefilxy) Mesh is too large in x-direction.'
         write(6,*) 'Limit number of x-nodal points to NXCYCMAX'
         write(6,*) 'nx       = ',funcx(5)/2
         write(6,*) 'NXCYCMAX = ',NXCYCMAX
      endif
      if (ishape.eq.1) then
c        *** linear elements
         nx = funcx(5)/2
         do 9 i=1,nx
9           xc(i) = funcx(4+2*i)
      else if (ishape.eq.2) then
c        *** Quadratic elements
         nx = funcx(5)/4+1
         do 10 i=1,nx
            xc(i) = funcx(2+4*i)
10          continue
      endif

      icurvs(1)=0
      icurvs(2)=2
      call compcr(-1,kmesh,kprob,isol,number,icurvs,funcx,funcy)
      if (funcx(5)/2.gt.NXCYCMAX) then
         write(6,*) 'PERROR(pefilxy) Mesh is too large in y-direction.'
         write(6,*) 'Limit number of y-nodal points to NXCYCMAX'
         write(6,*) 'ny       = ',funcx(5)/2
         write(6,*) 'NXCYCMAX = ',NXCYCMAX
      endif
      if (ishape.eq.1) then
c        *** linear elements
         ny = funcx(5)/2
         do 19 i=1,ny
19          yc(i) = funcx(5+2*i)
      else if (ishape.eq.2) then
c        *** Quadratic elements
         ny = funcx(5)/4+1
         do 20 i=1,ny
20          yc(i) = funcx(3+4*i)
      endif
      
      xcmin = xc(1)
      ycmin = yc(1)
      xcmax = xc(nx)
      ycmax = yc(ny)

      dxmin = xc(2)-xc(1)
      do 30 ic=2,nx-1
         dx = xc(ic+1)-xc(ic)
         dxmin = min(dxmin,dx)
30    continue
 
      dymin = yc(2)-yc(1)
      do 40 ic=2,ny-1
         dy = yc(ic+1)-yc(ic)
         dymin = min(dymin,dy)
40    continue
      
      if (ishape.eq.2) then 
         dxmin = dxmin/2 
         dymin = dymin/2
      endif
c     write(6,'(''Pefilxy: '',2f12.3)') dxmin,dymin

      return
      end

