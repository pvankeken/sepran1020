      function ddot(n, dx, incx, dy, incy)
c
c        version 1.1    date   22-08-91 (Zdenek: no dimension, cdc, ce)
c
c     returns the dot product of double precision dx and dy.
c     ddot = sum for i = 0 to n-1 of  dx(lx+i*incx) * dy(ly+i*incy)
c     where lx = 1 if incx .ge. 0, else lx = (-incx)*n, and ly is
c     defined in a similar way using incy.
c
      implicit double precision (a-h,o-z)
      double precision dx(*), dy(*)
chpu      on real underflow ignore
      ddot = 0d0
      if (n.le.0) return
      if (incx.eq.incy) if (incx-1) 10, 30, 70
   10 continue
c
c         code for unequal or nonpositive increments.
c
      ix = 1
      iy = 1
      if (incx.lt.0) ix = (-n+1)*incx + 1
      if (incy.lt.0) iy = (-n+1)*incy + 1
      do 20 i=1,n
         ddot = ddot + dx(ix)*dy(iy)
         ix = ix + incx
         iy = iy + incy
   20 continue
      return
c
c        code for both increments equal to 1.
c
c
c        clean-up loop so remaining vector length is a multiple of 5.
c
   30 m = mod(n,5)
      if (m.eq.0) go to 50
      do 40 i=1,m
         ddot = ddot + dx(i)*dy(i)
   40 continue
      if (n.lt.5) return
   50 mp1 = m + 1
      do 60 i=mp1,n,5
         ddot = ddot + dx(i)*dy(i) + dx(i+1)*dy(i+1) + dx(i+2)*dy(i+2)
     1    + dx(i+3)*dy(i+3) + dx(i+4)*dy(i+4)
   60 continue
      return
c
c         code for positive equal increments .ne.1.
c
   70 continue
      ns = n*incx
      do 80 i=1,ns,incx
         ddot = ddot + dx(i)*dy(i)
   80 continue
chpu      on real underflow abort
      return
      end
