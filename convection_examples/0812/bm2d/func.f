c *************************************************************
c *   FUNC
c *  
c *   Define local value of functions used to initiate vectors
c *************************************************************
      real*8 function func(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z
      real*8 pi
      parameter(pi=3.1415926)
      real*8 wavel,ampini
      common /inival/ wavel,ampini
      real*8 wpi
    
      if (ichois.eq.1) then
         wpi = pi/wavel
         func = ampini*sin(pi*y)*cos(wpi*x) + (1-y)
c        func = x + y
      else if (ichois.eq.2) then
	 func = -y
      else if (ichois.eq.3) then
	 func = x
      endif
  
      return
      end
