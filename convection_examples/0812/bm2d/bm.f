      program bm
      implicit none
      integer NBUFDEF,NUM
      parameter(NBUFDEF = 100 000 000)
      parameter(NUM = 100 000)
c *************************************************************
c *   BM
c *
c *   Stationary convection benchmark models
c *
c *   NBUFDEF = size of large buffer. Increase if Sepran complains
c *             about the need to increase size of IBUFFR.
c *   NUM     = defines size of user arrays. Increase for meshes
c *             with lots of nodal points.
c *
c *   Pseudocode:
c *         Start: read user input, create mesh and problem definition
c *         Until convergence
c *               Solve Stokes equations
c *               Solve Stationary Heat equation
c *               Provide some output (convergence, Nu, Vrms, cpu)
c *         Main output (plots, restart files)
c *
c *   PvK 080100
c *************************************************************
      integer kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2,matr1,matr2 
      integer iuser,isol1,isol2,islol1,islol2,ipemap11,ipemap12
      real*8 user
      dimension kmesh1(400),kmesh2(400),kprob1(400),kprob2(400)
      dimension intmt1(5),intmt2(5),matr1(5),matr2(5)
      dimension iuser(105),user(NUM+5),ipemap11(5),ipemap12(5)
      dimension isol1(5),isol2(5),islol1(5,2),islol2(5)
      integer ibuffr
      common ibuffr(NBUFDEF)
      include 'bmiter.inc'
      include 'pecpu.inc'
      real*4 second
      real*8 tstart,tnow


      call bmstart(kmesh1,kmesh2,kprob1,kprob2,intmt1,intmt2,
     v             matr1,matr2,iuser,user,isol1,isol2,
     v             islol1,ipemap11,ipemap12,NBUFDEF,NUM)
      niter=0
      tstart = second()
100   continue
	 niter = niter +1
c        *****************************************************
c        **** STOKES EQUATIONS
c        *****************************************************
         call coefnew(1,kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                    iuser,user,islol1,ipemap12)
	 call bmstokes(kmesh1,kprob1,intmt1,iuser,user,
     v                 isol1,islol1,matr1)

c        *****************************************************
c        **** HEAT EQUATION
c        *****************************************************
         call coefnew(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v                    isol2,iuser,user,islol1,ipemap12)
         call bmheat(kmesh2,kprob2,intmt2,iuser,user,
     v               isol2,islol2,matr2)
      
	 call intermediate_output(kmesh1,kmesh2,kprob1,kprob2,
     v     isol1,isol2,islol1,islol2,iuser,user,ipemap11)

         tnow = second()
	 if (niter.lt.nitermax.and.dif.gt.eps) goto 100

       call bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v             iuser,user,ipemap11)

       end

