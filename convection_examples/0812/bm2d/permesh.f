c *************************************************************
c *   PERMESH
c *
c *   Read mesh from file
c *
c *   PvK 120490
c *************************************************************
      subroutine permesh(kmesh1,kmesh2)
      implicit none
      integer kmesh1(*),kmesh2(*)
      character*80 fsname,ftname

      open(9,file='mesh1')
      rewind(9)
      call meshrd(2,9,kmesh1)
      open(9,file='mesh2')
      rewind(9)
      call meshrd(2,9,kmesh2)
      close(9)

      return
      end
