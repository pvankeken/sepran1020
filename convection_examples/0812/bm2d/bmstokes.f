c *************************************************************
c *   BMSTOKES
c *
c *   Build equations for Stokes equations and solve
c *   Depending on value of isolmethod a direct or iterative 
c *   solver is used.
c *
c *   PvK 080100
c *************************************************************
      subroutine bmstokes(kmesh,kprob,intmat,iuser,user,
     v                    isol,islol,matr)
      implicit none
      integer kmesh(*),kprob(*),intmat(*),iuser(*)
      real*8 user(*)
      integer isol(*),islol(5,*),matr(*)
      integer irhsd(5),matrback(5),iinbld(20),matrm(5)
      integer inpsol(30),iread
      real*8 rinsol(10)
      save irhsd,matrback,iinbld,matrm
      logical rebuild

      include 'peparam.inc'
      include 'c1visc.inc'
      include 'solutionmethod.inc'

      integer ifirst,i
      save ifirst
      data ifirst/0/

      do i=1,20
         iinbld(i) = 0
      enddo

      call copyvc(isol,islol(1,1))
c     write(6,*) 'bmstokes: ifirst', ifirst
      if (itypv.gt.0.or.ifirst.eq.0) then
c        *** Build system: A and f
         rebuild = .true.
         iinbld(1) = 10
         iinbld(2) = 1
         iinbld(10) = 2
         call build(iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,
     v              islol,iuser,user)
      else
c        *** Build only f (useful for isoviscous calculations)
         rebuild = .false.
         iinbld(1) = 10
         iinbld(2) = 2
         iinbld(10) = 2
         call build(iinbld,matr,intmat,kmesh,kprob,irhsd,matrm,isol,
     v              islol,iuser,user)
      endif

      if (isolmethod.eq.-1) then
c       *** penalty function method, direct solution method
        call solve(1,matr,isol,irhsd,intmat,kprob)
      else
        if (isolmethod.eq.0) then
c          *** Penalty function method, direct, iposst=1
           inpsol(1) = 3
c          *** profile method
           inpsol(2) = 1
c          *** solution method
           inpsol(3) = 0
        else
           inpsol(1) = 14
c          *** ipos
           inpsol(2) = 0
c          *** solution_method 1=CG 2=CGS 3=GMRES 4=GMRESR
           inpsol(3) = isolmethod
c          *** ipreco 1=diag 2=eisenstat 3=ILU
           inpsol(4) = ipreco
           inpsol(5) = maxiter
           inpsol(6) = iprint
c          *** matrix is symmetric(1) or not (0)?
           inpsol(7) = 0
c          *** dimension of Krylov space for GMRES
           inpsol(8) = 20
c          *** ISTART: 1=starts with given vector
           inpsol(9) = 1 
c          *** KEEP: 1=keep preconditioning matrix; 
           if (rebuild.and.keep.eq.1) then
              inpsol(10)= 1
           else if (keep.eq.1.and.ifirst.eq.1) then
c             *** use old matrix
              inpsol(10) = 2
           else
              inpsol(10) = 1
           endif
           inpsol(11)= 2
           inpsol(12)= 0
c          *** NTRUNC: for GMRESR: maximum number of search directions
           inpsol(13)= 5
c          *** NINNER: for GMRESR: maximum number of inner loop iters
           inpsol(14)= 5
c          *** EPS: required accuracy for iteration process
           rinsol(1) = cgeps
        endif
        iread = -1
        call solvel(inpsol,rinsol,matr,isol,irhsd,intmat,kmesh,
     v              kprob,iread)
      endif
      ifirst=1

      return
      end


