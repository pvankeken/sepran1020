c *************************************************************
c *   FUNCCF
c *
c *   Specify local value of coefficients 
c *   Current version is obsolete.
c *************************************************************
      real*8 function funccf(ichois,x,y,z)
      implicit none
      integer ichois
      real*8 x,y,z
      include 'SPcommon/cactl'
      include 'SPcommon/celp'
      integer mypointer
      common /cmypointers/ mypointer(100)
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      integer k1,k2,iipvisco,iipbuoy,ioff,inidgt
      real*8 pefrombuf

      save k1,k2
      data k1,k2/1,1/

      if (itype.ne.900.and.itype.ne.901.and.itype.ne.902) then
         if (irule.ne.3) then
          write(6,*) 'PERROR(funccf): itype = 900/901/902 and irule <>3' 
          write(6,*) 'The peprepcof/funccf setup is meant for irule=3'
          call instop 
         endif
      endif

      if (ichois.eq.1) then
c        *** viscosity
         iipvisco = inidgt(mypointer(1))
         ioff = (ielem-1)*7 + k1
         funccf = pefrombuf(buffr(iipvisco),ioff)
 
         k1 = k1+1
         if (k1.gt.7) k1=1

      else

c        *** buoyancy force
         iipvisco=inidgt(mypointer(2))
         ioff = (ielem-1)*7 + k2
         funccf = pefrombuf(buffr(iipbuoy),ioff)

         k2 = k2+1
         if (k2.gt.7) k2=1

      endif
  
      return 
      end


      real*8 function pefrombuf(visco,inum)
      implicit none
      real*8 visco(*)
      integer inum

      pefrombuf = visco(inum)
 
      return
      end
