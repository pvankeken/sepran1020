      subroutine tosepcomsub ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         inpsep, rinsep, lenmsh, lenprob, istart,
     +                         isubr, meshnr, ioutpsav, isolut,
     +                         meshseqs, iseqlp, niterlp, ireadlp,
     +                         timefinished, numveclc, icalltimstr,
     +                         iinmap, irhsd, massmt, mattot, stiffm,
     +                         maxprb  )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.9  date 17-10-2007 Extension of inpsep for prcomm
!        version  2.8  date 23-03-2007 Extension with test_force
!        version  2.7  date 07-03-2007 Plot function
!        version  2.6  date 21-02-2007 extra options linear solver
!        version  2.5  date 28-11-2006 extra option read_solutions
!        version  2.4  date 20-11-2006 Allow writing solution to file
!        version  2.3  date 04-09-2006 Extension with bearing
!        version  2.2  date 04-01-2006 Extension of input for plotmesh
!        version  2.1  date 28-12-2005 New length inpsep for scalar expressions
!        version  2.0  date 27-09-2005 Extra parameters irhsd, ...
!        version  1.12 date 15-06-2005 Extension with pressure correction
!        version  1.11 date 03-05-2005 Extension with enthapy + refinement
!        version  1.10 date 20-04-2005 New call to intpolmbf
!
!   copyright (c) 2003-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Body of subroutines to0094 and tofreesb
!
! **********************************************************************
!
!                       KEYWORDS
!
!     compute_all
!     driving_subroutine
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/comcons1'
      include 'SPcommon/comcons2'
      include 'SPcommon/cuscons'
      include 'SPcommon/clinsl'
      include 'SPcommon/csepcm'
      include 'SPcommon/cplot'
      include 'SPcommon/cmacht'
      include 'SPcommon/cmachn'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/ctimen'
      include 'SPcommon/cpost1'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer maxmesh
      parameter (maxmesh=5)
      integer lenmsh, lenprob, kmesh(lenmsh, maxmesh), ibuffr(*),
     +        kprob(lenprob,maxmesh), intmat(5,10,maxmesh), inpsep(*),
     +        istart, isubr, meshnr, ioutpsav, isolut(5,maxvc1,*),
     +        meshseqs(*), iseqlp, niterlp(10), ireadlp(10), numveclc,
     +        icalltimstr, iinmap(*), irhsd(5,*), maxprb,
     +        massmt(5,*), mattot(5,*), stiffm(5,*)
      double precision rinsep(*), buffer(*)
      logical timefinished

!     buffer        i/o   Real buffer array in which all large data is stored
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     icalltimstr   i/o   Number of calls of loop start
!     iinmap        i/o   Help array for subroutine INTPOLMS
!                         For a descriptoin see INTPOLMS
!     inpsep        i/o   The integer input read is stored in array INPSEP
!                  Array INPSEP is filled as follows:
!                  Pos. 1  NUMVETORS, i.e. number of vectors to be created
!                          Next positions information of the subroutines to be
!                          called with the corresponding information
!                          The subroutines are stored in the sequence as they
!                          are called, which means that the input of this
!                          subroutine is essential for the course of the
!                          program
!                          For each subroutine at least the following
!                          information is stored:
!                  Pos. 1  Subroutine sequence number
!                          Possible values:
!                          -1:  End of input
!                          1:   PRESBC  Read essential boundary conditions
!                          2:   LINPRB  Build system of equations and solves
!                                       linear system of equations
!                          3:   NONLNS  Build system of equations and solves
!                                       non-linear system of equations
!                          4:   CREATN  Create vector
!                          5:   OUTSOL  Write solution to file "sepcomp.out"
!                          6:   MANVEC  Manipulate vectors
!                          7:   PRINT_SCALAR: Print scalar
!                          8:   DERIV   Compute derivatives
!                          9:   PRINTV  Print vector
!                         11:   INTEGR  Compute integral
!                         12:   SCALAR i =  value/func
!                         13:   COMMAT  Change structure of matrix
!                         14:   COPYVC  Copy vectors
!                         15:   PRINT_TEXT: Print text
!                         16:   INTBND  Compute boundary integral
!                         17:   WRITBS  Write array to backing storage
!                         18:   READBS  Read array from backing storage
!                         19:   TIMPRB  Solve time-dependent problem
!                         20:   USEROUT User output subroutine
!                         21:   WHILE   Start of while loop
!                         22:   END_WHILE  End of while loop
!                         23:   START_STAT_FREE_BOUN  Start of stationary free
!                                       boundary loop
!                         24:   END_STAT_FREE_BOUN  End of stationary free
!                                       boundary loop
!                         25:   CHANGE_COEFFICIENTS
!                         26:   START_INSTAT_FREE_BOUN  Start of instationary
!                                      free boundary loop
!                         27:   END_INSTAT_FREE_BOUN  End of instationary free
!                                      boundary loop
!                         28:   INSTFREE, time integration in free boundary loop
!                         29:   INPUT_VECTOR
!                         30:   OPEN_PLOT
!                         31:   CLOSE_PLOT
!                         32:   PLOT_BOUNDARY
!                         33:   REFINE_MESH (mesh refinement)
!                         34:   PRESENT_MESH (set mesh sequence number)
!                         35:   WRITE_MESH (write mesh to file)
!                         36:   INTERPOLATE (interpolate between two meshes)
!                         37:   FOR Start for loop
!                         38:   END_FOR_LOOP
!                         39:   PLOT_VECTOR
!                         40:   PLOT_CONTOUR
!                         41:   CONTACT
!                         42:   INTERCHANGE_MESH
!                         43:   CREATE_FORCE_VECTOR
!                         44:   START_LOOP  Start of loop
!                         45:   END_LOOP    End of loop
!                         46:   START_TIME_LOOP  Start of time loop
!                         47:   END_TIME_LOOP    End of time loop
!                         48:   CHANGE_COORDINATES
!                         49:   COMPUTE_EIGENVALUES
!                         50:   CHANGE_PROBLEM
!                         51:   COMPUTE_CAPACITY
!                         52:   SOLVE_INVERSE_PROBLEM
!                         53:   DEFORM_MESH
!                         54:   PRINT_TIME
!                         55:   COMPUTE_PRINCIPAL_STRESSES
!                         56:   PLOT_COLOURED_LEVELS
!                         57:   PLOT_TENSOR
!                         58:   MOVE_OBSTACLE
!                         59:   MAKE_OBSTACLE_MESH
!                         60:   REMOVE_OBSTACLE_MESH
!                         61:   NO_OUTPUT (no action)
!                         62:   PLOT_MESH
!                         64:   STOP
!                         65:   READ_MESH
!                         66:   SET_TIME
!                         67:   MAKE_LEVELSET_MESH
!                         68:   REMOVE_LEVELSET_MESH
!                         71:   ENTHALPY_INTEGRATION
!                         72:   COMPUTE_ENTHALPY
!                         73:   INTERSECTION
!                         74:   TIME_HISTORY
!                         75:   PROBDFEXT
!                         76:   REF_ENTHALPY_INTEGRATION
!                         77:   PRESSURE_CORRECTION
!                         78:   NAVIER-STOKES
!                  Pos. 2  ISEQNR     Sequence number corresponding to input
!                                     If iseqnr = -1, the next one will be used
!                  Pos. 3  IPROB      Problem sequence number
!                                     If iprob = -1, the next one will be used
!                  Pos. 4  IVECTOR    Vector sequence number (first vector)
!                                     If ivector = -1, the same value as iprob
!                                     will be used
!                          The next positions depend on the subroutine itself:
!                   --- In case of MANVEC the positions 5 - 20+nelgrp are
!                          used:, see subroutine PRMANV
!                   --- In case of PRINT_SCALAR  position 2
!                        refers to the number of scalars  (numscalars)
!                        position 3 refers to the position in the text array
!                        In this case 0, means no text
!                        Position 4 indicates the number of characters in the
!                        text
!                        Position 5 to 4+3*numscalars contain 3 positions per
!                        scalar:
!                         1: type (1=integer, 2=real, 3=scalar)
!                         2: position in incons rlcons or scalars
!                         3: if>0, index of scalar used as array index
!                   --- In case of PRINT_VECTOR position 2
!                        refers to the vector sequence number and
!                        position 3 refers to the position in the text array
!                        In this case 0, means no text
!                        Position 4 indicates the number of characters in the
!                        text
!                       Also the positions 5-13 and possibly more
!                       are used as well as positions in rinsep in the following
!                       way:
!                        5:  ISTRIN (Starting address in rinsep)
!                        6:  ICHOICE (inppri(2), see prinvc)
!                        7:  IDIM    (inppri(3), see prinvc)
!                        8:  IQUANT  (inppri(4), see prinvc)
!                        9:  IDEGFD  (inppri(5), see prinvc)
!                        10: ISEQ    (inppri(6), see prinvc)
!                        11: IREGION (inppri(7), see prinvc)
!                        12: IHEADER (inppri(8), see prinvc)
!                        13: ICOOR   (inppri(9), see prinvc)
!                        14: IFORMAT (inppri(10), see prinvc)
!                        15: NREAD   (number of curves, surfaces etc)
!                            If NREAD>0 followed by NREAD curves etc.
!                        If IREGION#0, 6 positions in rinsep are used
!                   --- In case of NONLINEAR_SOLVER 5 positions are used:
!                         2  ISEQNR  Sequence number corresponding to input
!                                    If iseqnr = -1, the next one will be used
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: IVEC_REACT indicates if reaction forces must be
!                            computed (>0) or not (0)
!                            If >0, IVEC_REACT defines the sequence number
!                            of the vector in which the reaction forces
!                            must be stored
!                   --- In case of LINEAR_SOLVER 8 positions are used:
!                         2: contains the sequence number for the coefficients
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: contains the sequence number for the solver
!                         6: indicates if defect correction should be used:
!                            0: no defect correction
!                            1: defect correction
!                         7: indicates if quadratic elements should be
!                            treated as a cluster of linear subelements
!                            0: no subelements
!                            1: subelements
!                         8: IVEC_REACT indicates if reaction forces must be
!                            computed (>0) or not (0)
!                            If >0, IVEC_REACT defines the sequence number
!                            of the vector in which the reaction forces
!                            must be stored
!                         9: IVEC_FEM is only used in case of a spectral mesh
!                            It indicates if also the fem solution vector
!                            must be stored besides the sem solution vector
!                            if>0, IVEC_FEM defines the sequence number
!                            of the fem solution vector
!                        10: Indicates if a FEM preconditioning must be applied
!                            in case of a spectral mesh
!                            Possible values:
!                            0: no fem preconditioning
!                            1: fem preconditioning
!                   --- In case of DERIV the positions 2 - 5 are used:
!                        2:  Sequence number coefficients
!                        3:  IPROB
!                        4:  IVECTOR Sequence number output vector
!                        5:  ISEQNR Sequence number derivatives
!                   --- In case of INTEGR the positions 2 - 7 are used:
!                        2:  Sequence number coefficients
!                        3:  ISEQNR Sequence number integrals
!                        4:  IVECTOR  Sequence number input vector
!                        5:  ISCALAR1 Sequence number scalar number of integral
!                        6:  Scalar number of min integral per element
!                        7:  Scalar number of max integral per element
!                   --- In case of INTBND the positions 2 - 6 are used:
!                        2:  ISEQNR Sequence number boundary integrals
!                        3:  IVECTOR  Sequence number input vector
!                        4:  ISCALAR1 Sequence number first scalar of integral
!                        5:  ISCALAR2 Sequence number second scalar of integral
!                        6:  ISCALAR3 Sequence number third scalar of integral
!                   --- In case of SCALAR i = value the positions 2-3 are used:
!                        2:  Sequence number scalar
!                        3:  Type of construction
!                            1:  value
!                            2:  func
!                            3:  min
!                            4:  max
!                        4:  Position of value in rinsep (>0) or
!                            parameter IFUNC in FUNCSCAL ( IFUNC, SCALARS)
!                            In case of min/max: number of parameters
!                        5...:  In case of min/max: information of parameters
!                            >0: value in rinsep; <0 -Scalar sequence number
!                   --- In case of COMMAT position 2 is used:
!                        2:  Sequence number COMMAT input
!                   --- In case of COPY VECTOR the positions 2-3 are used:
!                        2:  Sequence number vector1
!                        3:  Sequence number vector2
!                        4:  Degree of freedom vector 1
!                        5:  Degree of freedom vector 2
!                   --- In case of PRINT_TEXT the positions 2-3 are used:
!                        2:  Position in text array
!                        3:  number of characters
!                   --- In case of WRITE_VECTOR or READ_VECTOR the positions
!                        2-4 are used:
!                        2:  array sequence number
!                        3:  sequence number in file 2
!                        4:  save administration (1) or not yet (0)
!                   --- In case of USEROUT at least positions 2 and 3 are used:
!                        2:  Sequence number USEROUT
!                        3:  nextra is number of extra integer positions
!                            If this number is larger than 0, nextra
!                            positions are used after position 2
!                   --- In case of WHILE the positions 2-3 are used:
!                        2:  sequence number k of boolean_expr(k)
!                        3:  address in inpsep just after the while loop
!                   --- In case of END_WHILE only position 2 is used:
!                        2:  starting address of while loop
!                   --- In case of START_(IN)STAT_FREE_BOUN or START_(TIME)_LOOP
!                       the positions 2-3 are used:
!                        2:  sequence number k
!                        3:  address in inpsep just after the loop
!                   --- In case of END_(IN)STAT_FREE_BOUN or END_(TIME)_LOOP
!                       only position 2 is used:
!                        2:  starting address of loop + 3
!                   --- In case of CHANGE_COEFFICIENTS the 4 positions have
!                       the meaning
!                        2:  coefficient sequence number
!                        3:  change coefficient sequence number
!                        4:  iteration sequence number
!                        5:  problem sequence number (if 0, the last one)
!                   --- In case of TIME_INTEGRATION 3 positions are used:
!                        2:  time_method sequence number
!                        3:  vector sequence number
!                   --- In case of INPUT_VECTOR the positions 2-5 are used:
!                        2:  Sequence number of output vector
!                        3:  Number of unknowns per point
!                        4:  Position of file name in text array
!                        5:  number of characters of file name
!                   --- In case of CLOSE_PLOT or OPEN_PLOT
!                       only position 1 is used
!                   --- In case of PLOT_BOUNDARY positions 2-3 are used:
!                        2:  IREGION
!                        3:  Position of yfact
!                        If IREGION#0, 4 positions in rinsep are used
!                   --- In case of PLOT_MESH position 2 is used:
!                        2:  IREGION
!                        3:  Position of yfact
!                        If IREGION#0, 4 positions in rinsep are used
!                   --- In case of MAKE_OBSTACLE_MESH position 2-3 are used:
!                        2:  MESH_ORIG Sequence number of mesh to be adapted
!                        3:  MESH_OBST Sequence number of adapted mesh
!                   --- In case of REMOVE_OBSTACLE_MESH position 2-3 are used:
!                        2:  MESH_ORIG Sequence number of mesh to be adapted
!                        3:  MESH_OBST Sequence number of adapted mesh
!                   --- In case of REFINE_MESH position 2-5 are used:
!                        2:  N (Number of refinements)
!                        3:  MESH_IN Sequence number of mesh to be refined
!                        4:  MESH_OUT Sequence number of refined mesh
!                        5:  ISEQ Sequence number of refine input
!                   --- In case of PRESENT_MESH position 2 is used:
!                        2:  Sequence number of mesh
!                   --- In case of INTERCHANGE_MESH position 2-3 ARE used:
!                        2:  New sequence number of mesh 1
!                        3:  New sequence number of mesh 2
!                   --- In case of INTERPOLATE positions 2-4 are used:
!                        2:  Sequence number V1 (Defines also MESH_IN)
!                        3:  Sequence number V2
!                        4:  MESH_OUT
!                   --- In case of OUTPUT positions 2-4 are used:
!                        2:  ISEQNR
!                        3:  INAME Sequence number of name in text array
!                                  0 = default name
!                        4:  IVECTOR
!                   --- In case of FOR_LOOP positions 2-6 are used:
!                        2:  Lower bound loop
!                        3:  Upper bound loop
!                        4:  Step in loop
!                        5:  Sequence number of variable
!                        6:  address in inpsep just after the for loop
!                   --- In case of END_FOR_LOOP position 2 is used:
!                        2:  starting address in for loop
!                   --- In case of PLOT_VECTOR or PLOT_TENSOR positions 2-7
!                       are used:
!                        2:  IVECTOR
!                        3:  IREGION
!                        If IREGION#0, 4 positions in rinsep are used
!                        4:  IDEGFD1
!                        5:  IDEGFD2
!                        6:  IYFACT
!                        If IYFACT#0, 1 extra position in rinsep is used
!                        7:  IFACTOR
!                        If IFACTOR#0, 1 extra position in rinsep is used
!                         In case of plot tensor position 8 is also used:
!                        8:  IDEGFD3
!                   --- In case of PLOT_CONTOUR or PLOT_COLOURED_LEVELS
!                       positions 2-8 are used:
!                        2:  IVECTOR
!                        3:  IREGION
!                        If IREGION#0, 4 positions in rinsep are used
!                        4:  IDEGFD
!                        5:  NLEVEL
!                        6:  MINLEVEL
!                        If MINLEVEL#0, 1 extra position in rinsep is used
!                        7:  MAXLEVEL
!                        If MAXLEVEL#0, 1 extra position in rinsep is used
!                        8:  IYFACT
!                        If IYFACT#0, 1 extra position in rinsep is used
!                   --- In case of CREATE_FORCE_VECTOR 5 positions are used:
!                         2: contains the sequence number for the coefficients
!                         3  IPROB   Problem sequence number
!                                    If iprob = -1, the next one will be used
!                         4  IVECTOR Vector sequence number (first vector)
!                                    If ivector = -1, the same value as iprob
!                                    will be used
!                         5: indicates if quadratic elements should be
!                            treated as a cluster of linear subelements
!                            0: no subelements
!                            1: subelements
!                   --- In case of CHANGE_COORDINATES 2 positions are used:
!                         2: contains the sequence number for the input
!                   --- In case of EIGENVALUES 5 positions are used:
!                         2: contains the sequence number for the input
!                         3: IVECTOR
!                         4: ISCALAR
!                         5: IPROB
!                         6: NEIGV
!                   --- In case of DEFORM_MESH
!                         2: Sequence number of the vector to add
!                         3: SCALE
!                   --- In case of COMPUTE_PRINCIPAL_STRESSES 5 positions are
!                       used:
!                         2: iseqstress
!                         3: iseqeigval
!                         4: iseqeigvec
!                         5: ichoice
!                   --- In case of MOVE_OBSTACLE 12 positions are used:
!                         2: iobst
!                         3: Sequence number first position in RINSEP
!                         4: if>0 reference to scalar w.r.t. u_veloc
!                         5: if>0 reference to scalar w.r.t. v_veloc
!                         6: if>0 reference to scalar w.r.t. w_veloc
!                         7: if>0 reference to scalar w.r.t. phi_veloc
!                         8: Rotation plane 1: (xy) 2: (yz) 3: (xz)
!                            RINSEP(1-3) contain the velocity vector
!                            RINSEP(4) contains the rotation velocity
!                         9: Indicates if positions 4 to 6 refer to a
!                            velocity (0) or displacement (1)
!                        10: if>0 reference to scalar w.r.t. x_orig
!                        11: if>0 reference to scalar w.r.t. y_orig
!                        12: if>0 reference to scalar w.r.t. z_orig
!                   --- In case of READ_MESH 3 positions are used:
!                         2: meshnr
!                         3: sequence number for name in text array
!                   --- In case of SET_TIME positions 2 is used:
!                        2:  Position of value in rinsep (>0)
!                   Warning: the contents of inpsep and rinsol are yet not
!                           defined in such a way that they are upwards
!                           compatible. In the future the description may be
!                           changed. Please do not take the present description
!                           for granted
!     intmat        i/o   Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     ioutpsav       i    Help parameter to store the initial value of ioutp
!     ireadlp       i/o   Sequence number for reading of loop information
!     irhsd         i/o   Standard SEPRAN array, containing information of the
!                         large vector(s)
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     iseqlp        i/o   Present level of loop
!     isolut        i/o   Standard SEPRAN array containing information of
!                         the solution vector (same as isol)
!     istart        i/o   Starting address in array INPVEC
!     isubr          i    Indicates the type of calling subroutine
!                         See subroutine TO0094 for a description
!     kmesh         i/o   Standard SEPRAN array containing information of the
!                         mesh
!     kprob         i/o   Standard SEPRAN array, containing information of
!                         the problem
!     lenmsh         i    Declared length of array KMESH
!     lenprob        i    Declared length of array KPROB
!     massmt        i/o   SEPRAN array, containing information of the mass
!                         matrix
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     mattot        i/o   SEPRAN array, containing information of the matrix
!                         M/dt + S
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     maxprb         i    Maximum number of coupled problems allowed
!     meshnr        i/o   Mesh sequence number
!     meshseqs      i/o   Contains actual sequence numbers of meshes
!     niterlp       i/o   Array of length 10.
!                         Pos. i contains iteration number for loop i
!     numveclc      i/o   Number of vectors that have been filled
!     rinsep        i/o   Array containing the real input for this subroutine
!                         See subroutine TO0094 for a description
!     stiffm        i/o   SEPRAN array, with information of the stiffness matrix
!                         In the first call position 1 must be 0.
!                         In the rest of the calls this array is refilled
!                         If the mm number is positive it is reused
!     timefinished  i/o   If true the time loop has finished
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      logical debug, complrun, loopready
      double precision rplots(3)
      integer iplots(10), i, iname, iref, map(5), idummy(1), leninpsep,
     +        nelgrp, ivecseq, jmaxsave, k, lenname, meshin, meshout
     +
      character (len=100) namesav
      save complrun, loopready

!     complrun       If true a complete time loop must be carried out, if
!                    false one time step only
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     i              Counting variable
!     idummy         Dummy array
!     iname          Sequence number of name in text array
!     iplots         Integer input array for plot subroutine
!     iref           File reference number
!     ivecseq        Sequence number of vector to be considered with respect
!                    to the solution array (isol or isolut)
!     jmaxsave       Help variable to save the value of jmax
!     k              Counting variable
!     leninpsep      Length of part of array inpsep
!     lenname        Number of significant characters in a text
!     loopready      If true the loop has finished
!     map            Mapping array that is filled if ichoice = 2 or must
!                    be filled if ichoice = 3
!                    map is an integer array of length 5 with the following
!                    contents:
!                    1: mmimap  memory management reference to array imap
!                    2: 130 to indicate the type of array
!                    3: mmrmap  memory management reference to array rmap
!                    4: mmcoor1  memory management reference to array coor1
!                    5: mmcoor2  memory management reference to array coor2
!     maxmesh        Maximum number of meshes allowed
!     meshin         Sequence number of first mesh to be used
!     meshout        Sequence number of last mesh to be used
!     namesav        Help variable to save a name
!     nelgrp         Number of element groups
!     rplots         Real input array for plot subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      logical userbool

!     DEFMSHBF       Deform mesh for solid computation
!     ERCLOS         Resets old name of previous subroutine of higher level
!     ERCRET         Create vector
!     ERLOOPFOR      Perform all actions necessary for the start of a for loop
!     EROPEN         Produces concatenated name of local subroutine
!     EROUTS         Write solution to file "sepcomp.out"
!     ERPRES         Read essential boundary conditions
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     ERRWAR         Print warnings
!     ERSCAL         Print scalar
!     INIFIL         Open files
!     INTPOLMBF      Interpolate solution from one mesh to another
!     MESHWRBF       fill the file meshoutput with the mesh
!     MSHCHCOR       Change coordinates in array coor by calls to FUNCCOOR
!     PLAFEP         General SEPRAN low level plotting subroutine
!     PLOTCU         Plot curves and user points
!     PRBEARING      Perform the complete iteration for one time step
!                    of the mass conservation scheme of Kumar
!     PRBNIN         Compute boundary integrals
!     PRCHANPROB     Change problem number of solution vector
!     PRCOMM         New call to COMMAT
!     PRCOMPCAP      calls the actual capacity subroutine prcompcapacity
!     PRCOMPENTHALPY Compute the enthalpy from the given temperature
!     PRCOMPINVER    calls the actual inverse subroutine prcompinv
!     PRCONTCT       Compute new contact
!     PRCOPY         Copy one SEPRAN vector into another one
!     PRDERV         Compute derivatives
!     PREIGVAL       calls the actual eigenvalue subroutine EIGENVBF
!     PRENTHALPYMAIN Solve the time dependent temperature equation
!                    with  freezing front by a enthalpy method
!     PRENTHREFINE   Solve the time dependent temperature equation
!                    with  freezing front by a enthalpy method and local
!                    refinement
!     PRFIL2         Read solutions from and write to backing storage
!     PRFORCEV       Fill the force vector with a right-hand side vector
!     PRINCIPSTRBF   Computes the principle stresses from a stress tensor
!     PRININ         print 1d integer array
!     PRINTERSECT    Prepare input for intersection subroutine
!     PRINTG         Compute integrals
!     PRLINS         Build system of equations and solves linear system of
!                    equations
!     PRMANV         Vector manipulation subroutine
!     PRMOVEOBSTAC   Move obstacle over a distance for one time step
!     PRMSHLEVMS     Create a new mesh from a fixed mesh with a zero levelset
!                    crossing the mesh
!     PRMSHOBSTMS    Create a new mesh from a fixed mesh with an obstacle
!                    crossing the mesh
!     PRNAVSTOKES    Solve the time dependent Navier-Stokes equations
!     PRNEWPROB      Call subroutine PROBDFEXT to define a new problem
!                    definition
!     PRNLNS         Solves non-linear system of equations
!     PRPLOTCN       Plot contours
!     PRPLOTFN       Plot function along curve
!     PRPLOTMESH     plot mesh
!     PRPLOTVC       Plot vectors
!     PRPRESSCORR    Perform one or more time steps with the pressure correction
!                    method to solve the Navier-Stokes equations
!     PRREADMESH     Read mesh input from file
!     PRREFINE       Refine an existing mesh, compute the corresponding problem
!                    definition and create the corresponding matrix structure
!     PRRMLEVMESH    Delete the levelset mesh created for a zero levelset
!                    crossing the mesh
!     PRRMOBSTMESH   Remove obstacle mesh created from obstacle mesh
!     PRSCLR         Fill scalar corresponding to the input part SCALAR i = ...
!     PRSETTIME      Set time in main program sepcomp or sepfree
!     PRTIMD         Solves time-dependent system of equations
!     PRUSER         Prepare output for userout or userouts subroutine
!     READSOLUTS     Read all solution arrays from sepcomp.in for one time step
!     READVECBF      Read vector from ASCII file
!     TIMEFRBF       Actual body of subroutine TIMEFREE
!     TOLOOP         Perform test in loop
!     USERBOOL       User subroutine corresponding to boolean_expr
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     433   Open plot found, whereas the plot has already been opened
!    1472   Plot file has been opened and closed without making any plot.
!    1473   The plot file has not been opened  before, whereas jtimes = 10.
!    1928   free boundaries not allowed in SEPCOMP
!    2040   Value of isubr has not yet been implemented
!    2107   Mesh has not been filled
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data map /5*0/
! ======================================================================
!
      call eropen ( 'tosepcomsub' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from tosepcomsub'
         write(irefwr,1) 'isubr, istart', isubr, istart
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

      select case ( isubr)

         case(:0, 69, 70, 82:)

!        --- Option has not been programmed

            call errint ( isubr, 1 )
            call errsub ( 2040, 1, 0, 0 )
            go to 1000

         case(1)

!        --- isubr = 1, call to presbc
!                       Fill essential boundary conditions

            if ( debug ) write(irefwr,*) 'essential boundary conditions'
            call erpres ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1, numveclc )
            istart = istart + 4

         case(2)

!        --- isubr = 2, call to linprb
!                       Build and solve system of linear equations
!                       istart is raised by the subroutine itself

            if ( debug ) write(irefwr,*) 'system of linear equations'
            call prlins ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1),
     +                    numveclc, istart )

         case(3)

!        --- isubr = 3, call to nonlns
!                       Build and solve system of non-linear equations

            if ( debug ) write(irefwr,*)'system of non-linear equations'
            call prnlns ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1),
     +                    numveclc )
            istart = istart + 5

         case(4)

!        --- isubr = 4, call to creatn
!                       Create vector

            if ( debug ) write(irefwr,*) 'Create vector'
            call ercret ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1, numveclc )
            istart = istart + 4

         case(5)

!        --- isubr = 5, call to outsol
!                       Write result to file "sepcomp.out"

            if ( debug ) write(irefwr,*) 'call to outsol'
            if ( itimloop==0 .or. ioutact==1 ) then
               numvec = max(numvec,numveclc)
               call erouts ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), isolut(1,1,meshnr),
     +                       inpsep(istart+1), maxvc1, numveclc )
            end if
            istart = istart + 4

         case(6)

!        --- isubr = 6, call to manvec
!                       Manipulate vectors

            if ( debug ) write(irefwr,*) 'Manipulate vectors'
            nelgrp = kmesh(5,meshnr)
            if ( debug ) then

!           --- debug information

               write(irefwr,*) 'Before prmanv'
               call prinin ( inpsep(istart), 21+nelgrp,
     +                       'inpsep from istart' )

            end if
            call prmanv ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), rinsep,
     +                    ivecseq, scalars, numveclc, numscals, nelgrp )
            istart = istart + 21+nelgrp

         case(7,9,15,54)

!        --- isubr = 7, print scalar or isubr = 9, print vector
!            or isubr = 15, print text or isubr = 54, print time

            if ( debug ) then
               write(irefwr,*) 'print scalar/vector'
               write(irefwr,*)  'itimloop, ioutact, isubr ',
     +                           itimloop, ioutact, isubr
            end if  ! ( debug ) then

            if ( itimloop==0 .or. ioutact==1 .or. isubr==7 )
     +         call erscal ( ibuffr, buffer, inpsep(istart), scalars,
     +                       isolut(1,1,meshnr), kmesh(1,meshnr),
     +                       kprob(1,meshnr), rinsep )
            if ( isubr==15 ) then
               istart = istart + 3
            else if ( isubr==7 ) then
               istart = istart + 4 + inpsep(istart+1)*3
            else if ( isubr==54 ) then
               istart = istart + 1
            else
               istart = istart + 21
               istart = istart + inpsep(istart-1)
            end if

         case(8)

!        --- isubr = 8, call to derivs
!                       Compute derivatives

            if ( debug ) write(irefwr,*) 'Compute derivatives'
            call prderv ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1,
     +                    numveclc, scalars, numscals )
            istart = istart + 6

         case(10,25,61,63,64)

!        --- Jump

         case(11)

!        --- isubr = 11, call to integr
!                        Compute integral

            if ( debug ) write(irefwr,*) 'Compute integral'
            call printg ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), scalars, numscals,
     +                    numveclc )
            istart = istart + 7

         case(12)

!        --- isubr = 12, scalar i = value/func

            if ( debug ) write(irefwr,*) 'scalar i = value'
            leninpsep = inpsep(istart+1)
            call prsclr ( inpsep(istart+1), rinsep, numscals )
            istart = istart + leninpsep+2

         case(13)

!        --- isubr = 13, call to COMMAT, i.e. change structure of matrix

            if ( debug ) write(irefwr,*) 'change structure of matrix'
            call prcomm ( ibuffr, kmesh(1,meshnr), kprob(1,meshnr),
     +                    intmat(1,1,meshnr), inpsep(istart+1) )
            istart = istart + 3

         case(14)

!        --- isubr = 14, call to COPYVC, i.e. copy vectors

            if ( debug ) write(irefwr,*) 'copy vectors'
            call prcopy ( ibuffr, buffer, isolut(1,1,meshnr),
     +                    inpsep(istart+1), numveclc, kmesh(1,meshnr),
     +                    kprob(1,meshnr) )
            istart = istart + 5

         case(16)

!        --- isubr = 16, call to intbnd
!                        Compute boundary integral

            if ( debug ) write(irefwr,*) 'boundary integral'
            call prbnin ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), scalars, numscals )
            istart = istart + 6

         case(17,18)

!        --- isubr = 17 or 18, call to PRFIL2
!                              Read or write array to file 2

            if ( debug ) write(irefwr,*) 'array to file 2'
            call prfil2 ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart), maxvc1, numveclc )
            istart = istart + 4

         case(19)

!        --- isubr = 19, call to PRTIMD
!                        Solve time-dependent problem

            if ( debug ) write(irefwr,*) 'time-dependent problem'
            call prtimd ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), maxvc1,
     +                    numveclc, intmat(1,1,meshnr), irhsd, massmt,
     +                    mattot, stiffm, maxprb )
            istart = istart + 4

         case(20)

!        --- isubr = 20, call to USEROUT
!                        User output subroutine

            if ( debug ) write(irefwr,*) 'User output subroutine'
            call pruser ( ibuffr, buffer, kmesh(1,meshnr),
     +                    kprob(1,meshnr), isolut(1,1,meshnr),
     +                    inpsep(istart+1), numveclc,
     +                    intmat(1,1,meshnr) )
            istart = istart + 4 + inpsep(istart+2) + inpsep(istart+3)

         case(21)

!        --- isubr = 21, Start while loop

            if ( debug ) write(irefwr,*) 'Start while loop'
            if ( userbool ( inpsep(istart+1) ) ) then

!           --- While must be executed, go to next step

               istart = istart + 3
               ioutp = -1

            else

!           --- While must not be executed, skip loop

               istart = inpsep(istart+2)
               ioutp = ioutpsav

            end if

         case(22)

!        --- isubr = 22, End while loop

            if ( debug ) write(irefwr,*) 'End while loop'
            istart = inpsep(istart+1)
            ioutp = ioutpsav

         case(23,24,26,27)

!        --- Not implemented

            call errsub ( 1928, 0, 0, 0 )

         case(28)

!        --- isubr = 28, call subroutine TIMEFRBF to perform a time step

            if ( debug ) write(irefwr,*) 'perform a time step'
            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            call timefrbf ( ibuffr, buffer, timefinished,
     +                      kmesh(1,meshnr),
     +                      kprob(1,meshnr), intmat(1,1,meshnr),
     +                      isolut(1,1,meshnr), inpsep(istart+1),
     +                      numveclc, maxvc1, complrun, irhsd, massmt,
     +                      mattot, stiffm, maxprb )
            istart = istart + 3

         case(29)

!        --- isubr = 29, call subroutine READVEC

            if ( debug ) write(irefwr,*) 'subroutine READVEC'
            call readvecbf ( ibuffr, buffer, kmesh(1,meshnr),
     +                     kprob(1,meshnr),
     +                     isolut(1,inpsep(istart+1),meshnr),
     +                     inpsep(istart+2),
     +                     texts(inpsep(istart+3))(1:inpsep(istart+4)) )
            istart = istart + 5

         case(30)

!        --- OPEN PLOT found
!            Check value of jtimes, and set this value to 1

            if ( debug ) write(irefwr,*) 'OPEN PLOT'
            if ( jtimes/=10 ) then

!           --- Error 433:  Plot file has already been opened before

               call errwar ( 433, 0, 0, 0 )
            else
               jtimes = 11
            end if
            istart = istart+1

         case(31)

!        --- CLOSE PLOT found
!            Check value of jtimes, and set this value to 3

            if ( debug ) write(irefwr,*) 'CLOSE PLOT'
            if ( jtimes==11 ) then

!           --- Error 1472:  Plot file is opened and closed, without
!                           making any plot

               call errwar ( 1472, 0, 0, 0 )
               jtimes = 10

            else if ( jtimes==10 ) then

!           --- Error 1473:  Plot file has not been opened

               call errwar ( 1473, 0, 0, 0 )

            else

!           --- Close Plot dataset

               jtimes = 13
               call plafep ( 2, 0d0, 0d0, 0d0, 0d0, 0 )
               jtimes = 10

            end if
            istart = istart+1

         case(32)

!        --- PLOT BOUNDARY found

            if ( debug ) write(irefwr,*) 'PLOT BOUNDARY'
            iplots(1) = 2
            do i = 2, 10
               iplots(i) = 0
            end do
            iplots(3) = 1
            rplots(1) = 15d0                          ! plotfm
            if ( inpsep(istart+2)==0 ) then
               rplots(2) = 1d0                        ! yfact
            else
               rplots(2) = rinsep(inpsep(istart+2))
            end if
            if ( inpsep(istart+1)>0 ) then
               jmaxsave = jmax
               jmax = 1
               xmin = rinsep(inpsep(istart+1))
               xmax = rinsep(inpsep(istart+1)+1)
               ymin = rinsep(inpsep(istart+1)+2)
               ymax = rinsep(inpsep(istart+1)+3)
            end if
            call plotcu ( ibuffr, buffer, kmesh(1,meshnr), iplots,
     +                    rplots, 'x', 'y' )
            if ( inpsep(istart+1)==1 ) jmax = jmaxsave
            istart = istart+3

         case(33)

!        --- REFINE_MESH found

            if ( debug ) write(irefwr,*) 'REFINE_MESH'
            call prrefine ( ibuffr, buffer, kmesh, kprob, intmat,
     +                      isolut, inpsep(istart+1), maxvc1, numveclc,
     +                      meshnr, lenmsh, lenprob, meshseqs, iinmap,
     +                      map )
            istart = istart+5

         case(34)

!        --- PRESENT_MESH found

            if ( debug ) write(irefwr,*) 'PRESENT_MESH'
            meshnr = meshseqs(inpsep(istart+1))
            if ( ioutp>=0 ) write(irefwr,695) meshnr
            if ( kmesh(2,meshnr)/=100 ) then

!           --- Mesh has not been filled

               call errint ( meshnr, 1 )
               call errint ( kmesh(2,meshnr), 2 )
               call errint ( 100, 3 )
               call errsub ( 2107, 3, 0, 0 )
               go to 1000

            end if
695         format ( ' Present mesh number is ', i1 )
            istart = istart+2

         case(35)

!        --- WRITE_MESH found

            if ( debug ) write(irefwr,*) 'WRITE_MESH'
            iname = inpsep(istart+1)
            namesav = name10
            if ( iname>0 ) then

!           --- iname>0, set name of meshoutput file

               name10 = texts(iname)(1:50)

            end if
            call inifil(10)
            lenname = index(name10,' ')-1
            if ( lenname==-1 ) lenname = len(name10)
            if ( ioutp>=0 ) write(irefwr,705) meshnr,
     +           name10(1:lenname)
705         format ( ' Write mesh ', i1, ' to file ', a )
            call meshwrbf ( ibuffr, buffer, iref10, kmesh(1,meshnr) )
            name10 = namesav
            iref = abs(iref10)
            close ( iref )
            istart = istart+2

         case(36)

!        --- INTERPOLATE found

            if ( debug ) write(irefwr,*) 'INTERPOLATE'
            ivecseq  = inpsep(istart+1)
            meshin  = meshseqs(inpsep(istart+2))
            meshout = meshseqs(inpsep(istart+3))
            if ( ioutp>=0 ) write(irefwr,715) ivecseq, meshin, meshout
715         format ( ' Interpolate vector ', i3, ' from mesh ', i1,
     +               ' to mesh ', i1 )
            if ( iinmap(2)==2 ) then

!           --- Check if meshin and meshout correspond to previous map

               if ( iinmap(4)/=meshin .or. iinmap(5)/=meshout )
     +            iinmap(2) = 1

            end if
            call intpolmbf ( ibuffr, buffer, iinmap, kmesh(1,meshin),
     +                       kmesh(1,meshout), kprob(1,meshin),
     +                       kprob(1,meshout), isolut(1,ivecseq,meshin),
     +                       isolut(1,ivecseq,meshout), map, idummy )
            iinmap(2) = 2
            iinmap(4) = meshin
            iinmap(5) = meshout
            istart = istart+4

         case(37)

!        --- isubr = 37, Start for loop

            if ( debug ) write(irefwr,*) 'Start for loop'
            call erloopfor ( istart, inpsep )
            ioutp = -1

         case(38)

!        --- isubr = 38, End for loop

            if ( debug ) write(irefwr,*) 'End for loop'
            istart = inpsep(istart+1)
            ioutp = ioutpsav

         case(39,57)

!        --- PLOT VECTOR or TENSOR found

            if ( debug ) write(irefwr,*) 'PLOT VECTOR or TENSOR'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotvc ( ibuffr, buffer, kmesh,
     +                         kprob(1,meshnr), isolut(1,1,meshnr),
     +                         inpsep(istart), rinsep, meshnr, lenmsh )

            end if
            istart = istart+7
            if ( isubr==57 ) istart = istart+2

         case(40,56)

!        --- PLOT CONTOUR or COLOURED LEVELS found

            if ( debug ) write(irefwr,*) 'PLOT CONTOUR'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotcn ( ibuffr, buffer, isubr, inpsep(istart+1),
     +                         rinsep, lenmsh, kmesh(1,meshnr),
     +                         kprob(1,meshnr), isolut(1,1,meshnr) )

            end if
            istart = istart+10

         case(41)

!        --- CONTACT found

            if ( debug ) write(irefwr,*) 'CONTACT'
            call prcontct ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut(1,1,meshnr),
     +                      inpsep(istart+1), numveclc,
     +                      intmat(1,1,meshnr) )
            istart = istart+4

         case(42)

!        --- INTERCHANGE_MESH found

            if ( debug ) write(irefwr,*) 'INTERCHANGE_MESH'
            k = meshseqs(inpsep(istart+1))
            meshseqs(inpsep(istart+1)) = meshseqs(inpsep(istart+2))
            meshseqs(inpsep(istart+2)) = k
            istart = istart+3

         case(43)

!        --- isubr = 43, call to build
!                       Build right-hand side vector

            if ( debug ) write(irefwr,*) 'Build right-hand side vector'
            call prforcev ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut(1,1,meshnr),
     +                      intmat, inpsep(istart+1), numveclc )
            istart = istart + 5

         case(44)

!        --- isubr = 44, start loop

            if ( debug ) write(irefwr,*) 'start loop'
            iseqlp = iseqlp+1
            ireadlp(iseqlp) = inpsep(istart+1)
            niterlp(iseqlp) = 0
            call toloop ( ibuffr, buffer, niterlp(iseqlp), loopready,
     +                    kmesh(1,meshnr), kprob(1,meshnr),
     +                    isolut(1,1,meshnr), ireadlp(iseqlp), iseqlp )
            ioutp = -1
            istart = istart + 3

         case(45)

!        --- isubr = 45, call subroutine TOLOOP and check on convergence

            if ( debug ) write(irefwr,*) 'TOLOOP'
            call toloop ( ibuffr, buffer, niterlp(iseqlp), loopready,
     +                    kmesh(1,meshnr), kprob(1,meshnr),
     +                    isolut(1,1,meshnr), ireadlp(iseqlp), iseqlp )
            if ( loopready ) then
               istart = istart+2
               niterlp(iseqlp) = 1
               iseqlp = iseqlp-1
               loopready = .false.
               ioutp = ioutpsav
            else
               istart = inpsep(istart+1)
            end if

         case(46)

!        --- isubr = 46, start time loop

            if ( debug ) write(irefwr,*) 'start time loop'
            if ( .not. timefinished ) then

!           --- Not yet ready, go to next step

               istart = istart + 3
               if ( ioutp>=0 .and. nitercom<=1 .and. itimloop<=0 )
     +            write(irefwr,811)
811            format ( /'Start time loop'/)
               icalltimstr = icalltimstr+1
               icalltim = 0

            else

!           --- End of loop

               istart = inpsep(istart+2)
               itimloop = 0
               icalltimstr = 0
               timefinished = .false.
               maxcalltim = 0
               icalltim = 0
               if ( ioutp>=0 ) write(irefwr,812)
812            format ( /'End time loop'/)

            end if

         case(47)

!        --- isubr = 47, End time loop

            if ( debug ) write(irefwr,*) 'End time loop'
            istart = inpsep(istart+1)-3

         case(48)

!        --- isubr = 48, call to mshchcor
!                        Change coordinates

            if ( debug ) write(irefwr,*) 'Change coordinates'
            call mshchcor ( ibuffr, buffer, kmesh(1,meshnr),
     +                      inpsep(istart+1) )
            iinmap(2) = 1
            istart = istart + 2

         case(49)

!        --- isubr = 49, Compute eigenvalues

            if ( debug ) write(irefwr,*) 'Compute eigenvalues'
            call preigval ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), intmat(1,1,meshnr),
     +                      isolut(1,1,meshnr), inpsep(istart+1),
     +                      scalars, maxvc1, numveclc, maxcls )
            istart = istart + 6

         case(50)

!        --- isubr = 50, Change problem

            if ( debug ) write(irefwr,*) 'Change problem'
            call prchanprob ( ibuffr, kprob(1,meshnr), kmesh(1,meshnr),
     +                        isolut(1,1,meshnr), inpsep(istart+1) )
            istart = istart + 4

         case(51)

!        --- isubr = 51, Compute capacity

            if ( debug ) write(irefwr,*) 'Compute capacity'
            call prcompcap ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), intmat(1,1,meshnr),
     +                       isolut(1,1,meshnr), inpsep(istart+1),
     +                       maxvc1, numveclc )
            istart = istart + 4

         case(52)

!        --- isubr = 52, Solve inverse problem

            if ( debug ) write(irefwr,*) 'Solve inverse problem'
            call prcompinver ( ibuffr, buffer, kmesh(1,meshnr),
     +                         kprob(1,meshnr), intmat(1,1,meshnr),
     +                         isolut(1,1,meshnr), inpsep(istart+1),
     +                         maxvc1, numveclc )
            istart = istart + 4

         case(53)

!        --- isubr = 53, Deform mesh

            if ( debug ) write(irefwr,*) 'Deform mesh'
            call defmshbf ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr),
     +                      isolut(1,inpsep(istart+1),meshnr),
     +                      rinsep(inpsep(istart+2)) )
            istart = istart + 3

         case(55)

!        --- isubr = 55, COMPUTE_PRINCIPLE_STRESSES

            if ( debug ) write(irefwr,*) 'COMPUTE_PRINCIPLE_STRESSES'
            call principstrbf ( ibuffr, buffer, isolut(1,1,meshnr),
     +                          inpsep(istart+1), kmesh(1,meshnr) )
            numveclc = max(numveclc, inpsep(istart+3), inpsep(istart+4))
            istart = istart + 5

         case(58)

!        --- isubr = 58, MOVE_OBSTACLE

            if ( debug ) write(irefwr,*) 'MOVE_OBSTACLE'
            call prmoveobstac ( ibuffr, buffer, inpsep(istart+1),
     +                          rinsep, kmesh(1,meshnr),
     +                          kprob(1,meshnr), intmat(1,1,meshnr) )
            istart = istart + 12

         case(59)

!        --- isubr = 59, MAKE_OBSTACLE_MESH

            if ( debug ) write(irefwr,*) 'MAKE_OBSTACLE_MESH'
            call prmshobstms ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, inpsep(istart+1), maxvc1,
     +                         numveclc, meshnr, lenmsh, lenprob,
     +                         meshseqs, iinmap, map )
            istart = istart + 4

         case(60)

!        --- isubr = 60, REMOVE_OBSTACLE_MESH

            if ( debug ) write(irefwr,*) 'REMOVE_OBSTACLE_MESH'
            call prrmobstmesh ( ibuffr, buffer, kmesh, kprob, intmat,
     +                          isolut, inpsep(istart+1), maxvc1,
     +                          numveclc, meshnr, lenmsh, lenprob,
     +                          meshseqs, iinmap, map )
            istart = istart + 3

         case(62)

!        --- PLOT MESH found

            if ( debug ) write(irefwr,*) 'PLOT MESH'
            if ( itimloop==0 .or. ioutact==1 ) then

!           --- Only at certain time steps

               call prplotmesh ( ibuffr, buffer, inpsep(istart+1),
     +                           kmesh(1,meshnr), rinsep )

            end if  ! ( itimloop==0 .or. ioutact==1 )
            istart = istart+5

         case(65)

!        --- isubr = 65, READ_MESH

            if ( debug ) write(irefwr,*) 'READ_MESH'
            call prreadmesh ( ibuffr, buffer, inpsep(istart+1), lenmsh,
     +                        kmesh )
            istart = istart + 3

         case(66)

!        --- isubr = 66, SET_TIME

            if ( debug ) write(irefwr,*) 'SET_TIME'
            call prsettime ( inpsep(istart+1), rinsep )
            istart = istart + 2

         case(67)

!        --- isubr = 67, MAKE_LEVELSET_MESH

            if ( debug ) write(irefwr,*) 'MAKE_LEVELSET_MESH'
            call prmshlevms ( ibuffr, buffer, kmesh, kprob, intmat,
     +                        isolut, inpsep(istart+1), maxvc1,
     +                        numveclc, meshnr, lenmsh, lenprob,
     +                        meshseqs, iinmap, map )
            istart = istart + 5

         case(68)

!        --- isubr = 68, REMOVE_LEVELSET_MESH

            if ( debug ) write(irefwr,*) 'REMOVE_LEVELSET_MESH'
            call prrmlevmesh ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, inpsep(istart+1), maxvc1,
     +                         numveclc, meshnr, lenmsh, lenprob,
     +                         meshseqs, iinmap, map )
            istart = istart + 3

         case(71)

!        --- isubr = 71, ENTHALPY_INTEGRATION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'ENTHALPY_INTEGRATION'
            call prenthalpymain ( ibuffr, buffer, timefinished,
     +                            kmesh(1,meshnr),
     +                            kprob(1,meshnr), intmat(1,1,meshnr),
     +                            isolut(1,1,meshnr), inpsep(istart+1),
     +                            numveclc, maxvc1, complrun, irhsd,
     +                            massmt, mattot, stiffm, maxprb )
            istart = istart + 2

         case(72)

!        --- isubr = 72, COMPUTE_ENTHALPY

            if ( debug ) write(irefwr,*) 'COMPUTE_ENTHALPY'
            call prcompenthalpy ( ibuffr, buffer, inpsep(istart+1),
     +                            isolut(1,1,meshnr), numveclc,
     +                            kmesh(1,meshnr), kprob(1,meshnr) )
            istart = istart + 2

         case(73)

!        --- isubr = 73, INTERSECTION

            if ( debug ) write(irefwr,*) 'INTERSECTION'
            call printersect ( ibuffr, buffer, kmesh(1,meshnr),
     +                         inpsep(istart+1), rinsep,
     +                         scalars, numscals )
            istart = istart + 5

         case(75)

!        --- isubr = 75, New problem description

            if ( debug ) write(irefwr,*) 'NEW_PROBLEM_DESCRIPTION'
            call prnewprob ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), inpsep(istart+1),
     +                       intmat(1,1,meshnr) )
            istart = istart + 2

         case(76)

!        --- isubr = 76, REF_ENTHALPY_INTEGRATION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'REF_ENTHALPY_INTEGRATION'
            call prenthrefine ( ibuffr, buffer, kmesh, kprob, intmat,
     +                          isolut, lenmsh, lenprob, timefinished,
     +                          numvec, maxvc1, complrun,
     +                          inpsep(istart+1), meshnr, maxmesh,
     +                          irhsd, massmt, mattot, stiffm )
            istart = istart + 3

         case(77)

!        --- isubr = 77, PRESSURE_CORRECTION

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'PRESSURE_CORRECTION'
            call prpresscorr ( ibuffr, buffer, kmesh, kprob, intmat,
     +                         isolut, lenmsh, lenprob, timefinished,
     +                         numvec, maxvc1, complrun,
     +                         inpsep(istart+1) )
            istart = istart + 2

         case(78)

!        --- isubr = 78, NAVIER_STOKES

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'NAVIER_STOKES'
            call prnavstokes ( ibuffr, buffer, timefinished,
     +                         kmesh(1,meshnr), kprob(1,meshnr),
     +                         intmat(1,1,meshnr), isolut,
     +                         inpsep(istart+1), numvec, maxvc1,
     +                         irhsd, massmt, mattot, stiffm,
     +                         maxprb, lenmsh, lenprob )
            istart = istart + 2

         case(79)

!        --- isubr = 79, BEARING

            complrun = .false.
            if ( icalltimstr==1 ) maxcalltim = maxcalltim+1
            icalltim = icalltim+1
            if ( debug ) write(irefwr,*) 'BEARING'
            call prbearing ( ibuffr, buffer, kmesh(1,meshnr),
     +                       kprob(1,meshnr), intmat(1,1,meshnr),
     +                       isolut, inpsep(istart+1),
     +                       numvec )
            istart = istart + 2

         case(80)

!        --- isubr = 80, Read solutions

            call readsoluts ( ibuffr, buffer, isolut, kmesh(1,meshnr),
     +                        kprob(1,meshnr) )
            istart = istart + 2

         case(81)

!        --- isubr = 81, Plot function
!                        istart is raised in subroutine itself

            call prplotfn ( ibuffr, buffer, kmesh(1,meshnr),
     +                      kprob(1,meshnr), isolut, inpsep(istart+1),
     +                      istart )

      end select  ! case ( isubr)

1000  call erclos ( 'tosepcomsub' )

      if ( debug ) then
         write(irefwr,1) 'istart', istart
         write(irefwr,*) 'End tosepcomsub'
      end if  ! ( debug )

      end
