      subroutine to0089sem ( ibuffr, buffer, matr, intmat, kmesh,
     +                       kprob, irhsd, massmt, isolut, iuser, user,
     +                       ivectr, iread )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 29-12-2003
!
!   copyright (c) 2003-2003  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Compute SEM solution by FEM preconditioning
!     reset intmat(2)
! **********************************************************************
!
!                       KEYWORDS
!
!     spectral
!     solve
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ibuffr(*), matr(*), intmat(*), kmesh(*), kprob(*),
     +        irhsd(*), massmt(*), isolut(5,*), iuser(*), ivectr, iread
      double precision buffer(*), user(*)

!     buffer        i/o   Buffer array
!     ibuffr        i/o   Integer buffer array in which all large data is stored
!     intmat         i    Standard SEPRAN array of length 5 containing
!                         information concerning the structure of the matrix.
!                         See the Programmer's Guide 13.5 for a
!                         complete description.
!     iread          i    Defines how the input must be read
!                         Possible values:
!                         -1:  No input is required
!                          0:  All SEPRAN input has been read by
!                              subroutine SEPSTN until
!                              END_OF_SEPRAN_INPUT or
!                              end of file has been found
!                              This input is used
!                          1:  The input is read as described for
!                              EIGENVAL
!     irhsd          i    Standard SEPRAN array, containing information of the
!                         large vector
!     isolut        i/o   Standard SEPRAN array containing information of
!                         the solution vector (same as isol)
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     ivectr        i/o   standard sepran array denoting USOL or vector of
!                         special structure defined in nodal points
!     kmesh          i    Standard SEPRAN array containing information of the
!                         mesh
!     kprob          i    Standard SEPRAN array, containing information of
!                         the problem
!     massmt         i    Parameter referring to mass matrix (not used here)
!     matr           i    Standard SEPRAN array, containing information of the
!                         large matrix
!     user           i    Real user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ninpcp, nrincp
      parameter ( ninpcp=140, nrincp=60 )
      integer iinbld(4), inpcop(ninpcp), inpsol(2), nprob
      double precision rincop(nrincp), rinsol(1)
      logical debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     iinbld         User input array for subroutine BUILD
!     inpcop         Copy of integer input array with respect to linear
!                    solver extended with defaults
!     inpsol         Integer input array for subroutine PRPCGRADBF
!     ninpcp         Length of array inpcop
!     nprob          Number of various problems
!     nrincp         Length of array rincop
!     rincop         Copy of real input array with respect to linear
!                    solver extended with defaults
!     rinsol         Real input array for subroutine PRPCGRADBF
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     BUILDBF        assemble a system of equations
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     PRPCGRAD       finite-element preconditioned conjugate-gradient solver
!     PRSOLVEINPUT   Fill arrays iincop and rincop for subroutine solvelbf
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2482   iread has wrong value
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0089sem' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0089sem'

      end if
      if ( ierror.ne.0 ) go to 1000
      iinbld(1) = 4
      iinbld(2) = 2
      iinbld(3) = 1
      iinbld(4) = 0
      call buildbf ( ibuffr, buffer, iinbld, matr, intmat, kmesh,
     +               kprob, irhsd, massmt, isolut(1,ivectr), isolut,
     +               iuser, user )
      if ( ierror.ne.0 ) go to 1000

!     --- Fill input for linear solver into inpcop and rincop

      if ( iread.le.0 ) then

!     --- Only iread > 0, is permitted

         call errint ( iread, 1 )
         call errsub ( 2482, 1, 0, 0 )
         go to 1000

      end if

      nprob = kprob(40)
      call  prsolveinput ( ibuffr, buffer, iread, inpsol, ninpcp,
     +                     nrincp, rincop, rinsol, inpcop, nprob,
     +                     1 )

!     --- Copy specific input for pcgrad into inpsol and rinsol

      inpsol(1) = inpcop(38)
      inpsol(2) = inpcop(37)
      rinsol(1) = rincop(8)
      call prpcgrad ( ibuffr, buffer, isolut(1,ivectr),
     +                isolut(1,ivectr), matr, kmesh, kprob, intmat,
     +                irhsd, iuser, user, inpsol, rinsol, inpcop,
     +                rincop )
      intmat(2) = 1102

1000  call erclos ( 'to0089sem' )

      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0089sem'

      end if

      end
