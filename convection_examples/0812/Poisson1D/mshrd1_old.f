      subroutine mshrd1 ( iinput, rinput, ncurvs, nuspnt, maxcrv,
     +                    ipoint, icode, inpmax, iinp, rinpt1, code,
     +                    ncode, nncode, ncodmx, jcoars, maxpnt,
     +                    intarmsh )
! ======================================================================
!
!        programmer    Guus Segal
!        version 24.5  date 23-06-2004 Extension with own_curve
!        version 24.4  date 20-02-2004 Debug statements
!        version 24.3  date 27-04-2001 Allow iratio = 5, 6, 7, 8
!        version 24.2  date 22-03-2000 Allow curve of curves with 1 curve (BvR)
!        version 24.1  date 05-08-1999 Allow curve number larger than the
!                                      present curve in composite curves
!        version 24.0  date 11-07-1999 Extra parameter intarmsh
!        version 23.2  date 17-06-1999 Extension for profile
!        version 23.1  date 14-09-1997 Extension for SPCURVE
!        version 23.0  date 14-01-1996 Extra parameter jcoars
!        version 22.4  date 17-11-1995 Extension with alpha for splines
!        version 22.3  date 12-01-1995 Replace warning by error
!        version 22.2  date 25-03-1994 Extension for REFLECT
!        version 22.1  date 04-02-1994 Check on double points
!        version 22.0  date 29-12-1993 Extra parameter ncodmx,
!                                      new call of intx00
!        version 21.3  date 05-01-1993 Extra warning: use of nelm in c...
!        version 21.2  date 25-09-1991 Error correction for Cspline
!        version 21.1  date 11-04-1991 Extra error message for compound
!                                      curves
!        version 21.0  date 24-12-1990 New parameter list
!        version 20.0  date 02-12-1990 irecmx replaced by maxcrv
!        version 19.2  date 15-10-1990 Extra warning: double use of curve
!                                      number
!
!   copyright (c) 1982-2004  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!    read cards with curves until a card with mesh or with surfaces
!    is found
!    fill arrays iinput and rinput with the information of the
!    curves and the factors respectively
!    iinput and rinput must be filled from position 1
! **********************************************************************
!
!                       KEYWORDS
!
!     curve
!     mesh
!     read
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/celwrk'
      include 'SPcommon/cmcdpr'
      include 'SPcommon/csepch'
      include 'SPcommon/cseprd'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ncurvs, nuspnt, maxcrv, ipoint(maxcrv), nncode,
     +        icode, inpmax, iinput(inpmax), iinp, ncode(nncode),
     +        ncodmx(nncode), jcoars, maxpnt, intarmsh(*)
      double precision rinput(maxcrv), rinpt1(2,maxcrv)
      character *(*) code(nncode)

!     code           i    Table with keywords
!     icode          o    return code,  possibilities:
!                          0       next card:  surfaces
!                          1       next card:  volumes
!                          2       next card:  meshline
!                          3       next card:  meshsurface
!                          4       next card:  meshvolume
!                          5       next card:  renumber
!                          6       next card:  plot
!                          7       next card:  end
!                          8       next card:  meshconnect
!                          9       next card:  norenumber
!                         10       next card:  intermediate points
!     iinp           o    next free position in iinput
!     iinput         o    integer input array
!     inpmax         i    maximal space for iinput
!     intarmsh       i    General array to store constants for subroutine MESH
!                         For a description, see subroutine MESHBODY
!     ipoint         o    pointer array: contains the starting positions of the
!                         curves in array iinput
!     jcoars         i    Indication if coarseness is defined (>0) or not ( = 0)
!     maxcrv         i    Maximum number of curves permitted
!     maxpnt         i    Maximal number of nodal points allowed
!     ncode          i    Table containing the number of significant charcaters
!                         in the keyword table code
!     ncodmx         i    Table containing the number of maximum characters in
!                         the keyword table code
!     ncurvs         o    number of curves
!     nncode         i    Number of entries in the keyword table
!     nuspnt        i/o   number of points
!     rinpt1         o    in this array the initial and end parameters t0 and t1
!                         for the curves are stored temporarily
!                         storage: t0 for curve i in rinpt1(1,i)
!                                  t1 for curve i in rinpt1(2,i)
!     rinput         o    real input array;  factors are filled from position 1,
!                         hence rinput must be started from 8 + ndim * nuspnt
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ncurtp
      parameter ( ncurtp=18)
      character (len=10) clcode(ncurtp)
      double precision factor, t0, t1, alpha
      integer i, icurnr, inum, maxnum, icurve, ishape, nelm, ncheck,
     +        iratio, nodd, itype, n, it1, it2, iseq, kcurvs, j, ndim,
     +        ncodecl(ncurtp), ncodmxcl(ncurtp), iprofile, ifunc
      logical coarsecurv, speccurv, paramcurv, debug

!     alpha          Parameter alpha with respect to splines
!     clcode         In this table the significant characters of the various
!                    types
!                    of curves are stored.
!     coarsecurv     If true coarse is used to create the distribution of nodes
!                    along the curve
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     factor         Multiplication factor
!     i              Counting variable
!     icurnr         Curve number
!     icurve         Type of curve (See clcode)
!     ifunc          Parameter ifunc for own_curve
!     inum           Number of numbers read in a record
!     iprofile       Defines the type of profile
!                    Possible values:
!                    1: upper part of naca0012
!                    2: lower part of naca0012
!     iratio         Ratio as defined in input
!     iseq           Sequence number in array work to be used for finding other
!                    data than nodal point numbers
!     ishape         Shape number of elements
!     it1            Nodal point number corresponding to first tangent
!     it2            Nodal point number corresponding to second tangent
!     itype          Itype as defined in input
!     j              Counting variable
!     kcurvs         Actual number of curves found (<= ncurvs)
!     maxnum         Maximal number of numbers to be read from the input
!     n              Number of nodes stored in array work
!     ncheck         Number of user points that must be checked
!     ncodecl        Contains the significant number of characters per word in
!                    clcode
!     ncodmxcl       Contains the maximum number of characters per word in
!                    clcode
!     ncurtp         Number of various curve types
!     ndim           Euclidian dimension
!     nelm           Number of elements given by the user
!     nodd           Parameter nodd as defined in input
!     paramcurv      If true a parameter curve is created
!     speccurv       If true a special curve (by contraction of copying)
!                    is created
!     t0             Initial t-value in parameter curve
!     t1             End t-value in parameter curve
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer intx00

!     ASKNXT         Read next item from the standard input file
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRCRD         Prints the last record read and underlines the errorneous
!                    part
!     ERREAL         Put real in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     ERRWAR         Print warnings
!     INTX00         Compare text in string with table (variable length)
!     MSHR00         Read information after the part Ci = LINE ( ..
!     PRININ         print 1d integer array
!     READSR         Read a series of numbers from the input (texts are
!                    neglected)
! **********************************************************************
!
!                       I/O
!
!    Records with respect to the curves are read.
!    The following records are recognized:
!
!    Ci = line j (....)
!    Ci = user j (....)
!    Ci = arc j (....)
!    Ci = spline j (....)
!    Ci = cline j (....)
!    Ci = carc j (....)
!    Ci = curves j (....)
!    Ci = parameter j (....)
!    Ci = cparam j (....)
!    Ci = translate j (....)
!    Ci = rotate j (....)
!    Ci = cspline j (....)
!    Ci = reflect j (....)
!    Ci = spcurve j (....)
!    Ci = profile j (....)
!    Ci = cprofile j (....)
!    Ci = circle j (....)
!    Ci = own_curve j (....)
!
!    A part of the information between the brackets is interpreted by mshr00
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     128   code for generation of elements (ishape) is incorrect
!     139   curve number does not exist
!     142   Internal error, too few space for integers
!     179   point number of user point is incorrect
!     276   Curve has already been filled
!     494   No user point found in curve to be translated
!     495   Less than 3 user points found in curve to be rotated
!     498   Curve number of curve to be translated or rotated too large
!     534   The value of IRATIO is incorrect
!     601   No curves found after curves of curves
!     648   string could not be recognized
!     966   Reflection axis or plane not specified
!     967   Not enough user points specified for reflection
!    1148   Not tangent given for begin and end point of SPLINE record of
!           type 4
!    1191   Tang has been read, whereas type # 4
!    1194   Value of nelm incorrect
!    1197   Parameter out of range
!    1198   t0 >= t1
!    1199   More than 1000 numbers found in curve record
!    1236   Curve number in curve of curves is incorrect
!    1286   Too few user points found
!    1334   Record does not start with a character
!    1335   SEPSTN has not been called with iinstr(2) = 1
!    1336   Ci = is not followed by a text
!    1483   Coinciding user points at one curve
!    1575   Useless parameter found in curve record
!    1616   No coarseness given in relation with carc, cline, cparam or cspline
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data clcode / 'LINE', 'USER', 'ARC', 'SPLINE', 'CLINE', 'CARC',
     +              'CURVES', 'PARAMETER', 'CPARAM', 'TRANSLATE',
     +              'ROTATE', 'CSPLINE', 'REFLECT', 'SPCURVE',
     +              'PROFILE', 'CPROFILE', 'CIRCLE', 'OWN_CURVE' /
      data ncodecl  / 3, 3, 3, 4, 4,    4, 4, 5, 5, 5,
     +                5, 5, 6, 5, 4,    5, 4, 6 /
      data ncodmxcl / 4, 4, 3, 6, 6,    4, 6, 9, 6, 9,
     +                6, 7, 7, 7, 7,    8, 6, 9 /

!     clcode  In this table the significant characters of the various types
!             of curves are stored.
!             The following types of curves are available:
!             1  line
!             2  user
!             3  arc
!             4  spline
!             5  cline
!             6  carc
!             7  curves
!             8  parameter
!             9  cparam
!            10  translate
!            11  rotate
!            12  cspline
!            13  reflect
!            14  spcurve
!            15  profile
!            16  cprofile
!            17  circle
!            18  own_curve
! ======================================================================
!
      call eropen ( 'mshrd1' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from mshrd1'

      end if
      if ( ierror.ne.0 ) go to 1000

!     --- initialize factors and  array ipoint

      do i = 1, maxcrv
         ipoint(i) = 0
         rinput(i) = 1d0
      end do
      do i = 1, maxcrv
         rinpt1(1,i) = 0d0
         rinpt1(2,i) = 0d0
      end do

      ncurvs = 0
      kcurvs = 0
      ncheck = 0
      iinp = 1
      ndim = intarmsh(1)

!     --- read next card

200   ityprd = 1
      call asknxt
      if ( iinp.ge.inpmax ) then

!     --- iinp > =  inpmax

          call errint ( inpmax, 1 )
          call errint ( iinp, 2 )
          call errsub ( 142, 2, 0, 0 )
          go to 1000
      end if

      if ( ityprd.ne.2 .and. ityprd.ne.4 ) then

!     --- error 1334: Record does not start with a character
!                      nor an integer

         call errsub ( 1334, 0, 0, 0 )
         call errcrd
         go to 200

      else if ( ityprd.eq.4 ) then

!     --- Detect the type of record
!         If a main keyword has been found, return to calling subroutine

         icode = intx00 ( code, ncode, nncode, 0, ncodmx ) - 1
         if ( icode.ge.0 ) go to 1000

      end if
      if ( ityprd.eq.4 ) then

!     --- Record starts with a character, this must be C

         if ( string(1:1).ne.'C' ) then
            call errchr ( string(1:1), 1 )
            call errsub ( 648, 0, 0, 1 )
            call errcrd
            go to 200
         end if

!        --- detect the curve card
!            first the curve number is read

         call asknxt

      end if

      if ( ityprd.ne.2 ) then

!     --- Next item is not an integer

         call errsub ( 1335, 0, 0, 0 )
         call errcrd
         go to 200

      end if

      icurnr = nint(realrd)
      ncurvs = max ( ncurvs, icurnr )
      kcurvs = kcurvs + 1
      if ( icurnr.le.0 .or. icurnr.gt.maxcrv ) then

!     --- icurnr has wrong value

         call errint ( icurnr, 1 )
         call errint ( maxcrv, 2 )
         call errsub ( 139, 2, 0, 0 )
         call errcrd
         go to 200

      else if ( ipoint(icurnr).gt.0 ) then

!     --- Curve icurnr has already been filled

         call errint ( icurnr, 1 )
         call errsub ( 276, 1, 0, 0 )
         call errcrd

      end if

      ipoint(icurnr) = iinp

!     --- Detect type of curve

      call asknxt
      if ( ityprd.ne.4 ) then

!     --- Not a text found after Ci =

         call errsub ( 1336, 0, 0, 0 )
         call errcrd
         go to 200

      end if

      icurve = intx00 ( clcode, ncodecl, ncurtp, 1, ncodmxcl )
      if ( jcoars.eq.0 .and. (icurve.eq.5 .or. icurve.eq.6 .or.
     +                        icurve.eq.9 .or. icurve.eq.12) ) then
         call errsub ( 1616, 0, 0, 0 )
      end if
      if ( debug ) write(irefwr,*) 'icurnr, icurve ', icurnr, icurve

      if ( icurve.eq.0 ) goto 200
      if ( icurve.ge.18 ) icurve = icurve+2

      iinput(iinp) = icurve
      iinput(iinp+2) = 0
      iinput(iinp+3) = 0

!     --- Read rest of records depending on the type of curve

      if ( icurve.eq.7 .or. icurve.eq.14 ) then

!     --- curves of curves

         maxnum = 1500
         call readsr ( work, maxnum, inum )
         if ( inum.le.0 ) then
            call errsub ( 601, 0, 0, 0 )
            call errcrd
         end if
         if ( inum.ge.1500 ) then
            call errsub ( 1199, 0, 0, 0 )
            call errcrd
         end if
         iinput(iinp+1) = 1
         if ( icurve.eq.14 ) then

!        --- icurve = 14, 4 extra positions corresponding to nelm, iratio
!                         begin point and end point

            iinput(iinp+2) = 0
            iinput(iinp+3) = 2
            iinput(iinp+4) = 0
            iinput(iinp+5) = 0
            iinp = iinp+3

         end if
         iinput(iinp+3) = inum
         do i = 1, inum
            n = nint(work(i))
            iinput(iinp+3+i) = n
            n = abs(n)
            if ( n.le.0 ) then

!           --- Error 1236: curve in curves of curves < 1

               call errint ( icurnr, 1 )
               call errint ( n, 2 )
               call errsub ( 1236, 2, 0, 0 )

            end if
         end do
         iinp = iinp+4+inum
         goto 200

      else if ( icurve.eq.10 .or. icurve.eq.11 ) then

!     --- Translate or Rotate

         maxnum = 1500
         call readsr ( work, maxnum, inum )
         iinput(iinp+1) = 1
         iinput(iinp+3) = inum-1
         do i = 1, inum
            iinput(iinp+3+i) = nint(work(i))
         end do
         if ( inum.ge.1000 ) then
            call errsub ( 1199, 0, 0, 0 )
            call errcrd
         end if

         if ( icurve.eq.10 ) then
            if ( inum.lt.1) then
               call errsub ( 494, 0, 0, 0 )
               call errcrd
            end if
         else
            if ( inum.lt.3) then
               call errint ( inum, 1 )
               call errsub ( 495, 1, 0, 0 )
               call errcrd
            end if
         end if
         if ( iinput(iinp+4).le.0 ) then
            call errint ( iinput(iinp+4), 1 )
            call errint ( icurnr, 2 )
            call errsub ( 498, 2, 0, 0 )
            call errcrd
         end if
         do i = 1, inum-1
            if ( iinput(iinp+i+4).eq.0 .or. abs(iinput(iinp+i+4)).gt.
     +           nuspnt ) then

!           --- Error 179: User point number <1 or > number of user points

               call errint ( iinput(iinp+i+4), 1 )
               call errint ( nuspnt, 2 )
               call errint ( i, 3 )
               call errint ( icurnr, 4 )
               call errsub ( 179, 4, 0, 0 )

            end if

         end do
         do i = 1, inum-2
            do j = i+1, inum-1
               if ( abs(iinput(iinp+i+4)).eq.abs(iinput(iinp+j+4)) .and.
     +              .not. (i.eq.1 .and. j.eq.inum-1) ) then

!              --- Error 1483: Coinciding user points

                  call errint ( i, 1 )
                  call errint ( icurnr, 2 )
                  call errint ( iinput(iinp+i+4), 3 )
                  call errint ( j, 4 )
                  call errsub ( 1483, 4, 0, 0 )

               end if

            end do
         end do

         iinp = iinp+4+inum
         goto 200

      else if ( icurve.eq.13 ) then

!     --- Reflect
!         Read number of curve to be reflected

         call readsr ( work, 1, inum )

!        --- Place number in iinput

         iinput(iinp+1) = 1
         iinput(iinp+4) = nint(work(1))

!        --- Read whether reflection on axis or on rplane

         call mshr00 ( work, nuspnt )

         inum = nint(work(1))

         iinput(iinp+3) = inum

         do i = 1, inum
            iinput(iinp+4+i) = nint(work(24+i))
         end do

         if ( work(11).lt.0.9d0 .or. work(11).gt.2.1d0 ) then

              call errint( icurnr, 1 )
              call errsub ( 966, 1, 0, 0 )
              call errcrd

         end if

         ndim = nint(work(11)) + 1

         if ( inum.lt.ndim+2 ) then

              call errint( icurnr, 1 )
              call errsub ( 967, 1, 0, 0 )
              call errcrd

         end if

         if ( iinput(iinp+4).le.0 ) then
            call errint ( iinput(iinp+4), 1 )
            call errint ( icurnr, 2 )
            call errsub ( 498, 2, 0, 0 )
            call errcrd
         end if

         iinp = iinp+5+inum

         goto 200

      end if

!     --- Other cases
!         determine shape number

      call asknxt
      if ( ityprd.eq.2 ) then

!     --- shape number found

         ishape = nint(realrd)

         if ( ishape.le.0 .or. ishape.gt.3 ) then
            call errint ( ishape, 1 )
            call errsub ( 128, 1, 0, 0 )
            call errcrd
         end if

      else

!     --- default shape number

         ishape = 1

      end if
      iinput(iinp+1) = ishape

!     --- Detect rest of record

      call mshr00 ( work, nuspnt )

      n      = nint(work(1))
      nelm   = nint(work(2))
      iratio = nint(work(3))
      factor = work(4)
      nodd   = nint(work(5))
      t0     = work(6)
      t1     = work(7)
      itype  = nint(work(8))
      alpha  = work(12)
      iprofile = nint(work(13))
      ifunc  = nint(work(15))
      coarsecurv = icurve.eq.5 .or. icurve.eq.6 .or. icurve.eq.9 .or.
     +             icurve.eq.16
      speccurv = icurve.eq.2 .or. icurve.eq.7 .or.
     +           icurve.ge.10 .and. icurve.le.13
      paramcurv = icurve.eq.8 .or. icurve.eq.9 .or. icurve.eq.15 .or.
     +            icurve.eq.16
      if ( nelm.gt.0 .and. (coarsecurv.or.speccurv) ) then

!     --- Parameter nelm found where it does not make sense

         call errchr ( 'nelm', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( iratio.ne.-100 .and. (coarsecurv.or.speccurv) ) then

!     --- Parameter ratio found where it does not make sense

         call errchr ( 'ratio', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(factor-1d0).gt.1d-5 .and. (coarsecurv.or.speccurv) ) then

!     --- Parameter factor found where it does not make sense

         call errchr ( 'factor', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( nodd.ne.0 .and. .not. coarsecurv ) then

!     --- Parameter nodd found where it does not make sense

         call errchr ( 'nodd', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(t0).gt.1d-5 .and. .not. paramcurv ) then

!     --- Parameter init found where it does not make sense

         call errchr ( 'init', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(t1-1d0).gt.1d-5 .and. .not. paramcurv ) then

!     --- Parameter end found where it does not make sense

         call errchr ( 'end', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( itype.ge.15 .and. itype.le.16 ) then

!     --- profile

         if ( t0.lt.0d0 .or. t0.gt.1d0 ) then

!        --- t0 out of range

            call erreal ( t0, 1 )
            call errchr ( 't0', 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1197, 1, 1, 1 )

         end if
         if ( t1.lt.0d0 .or. t1.gt.1d0 ) then

!        --- t1 out of range

            call erreal ( t1, 1 )
            call errchr ( 't1', 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1197, 1, 1, 1 )

         end if
         if ( t0.ge.t1 ) then

!        --- t0 >= t1

            call erreal ( t0, 1 )
            call erreal ( t1, 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1198, 1, 2, 0 )

         end if

      end if
      if ( itype.ne.1 .and. icurve.ne.4 .and. icurve.ne.12 ) then

!     --- Parameter type found where it does not make sense

         call errchr ( 'type', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if

!     --- Distinguish between the various curves

      select case ( icurve )

         case(:0, 7, 10, 11, 13, 14, 18, 19, 21:)

!        --- This option can not occur

            go to 1000

         case(1)

!        --- icurve = 1, LINE found

            maxnum = 2
            ncheck = 2
            if ( n.ge.3 ) then

!           --- n > 2, more than 2 numbers read

               iseq = 2
               if ( nelm.eq.0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio.eq.-100 .and. iseq.lt.n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0).lt. 10d0*epsmac .and. iseq.lt.n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(2)

!        --- icurve = 2, Userdefined curve found

            maxnum = n
            ncheck = n
            nelm = n-1

         case(3)

!        --- icurve = 3, ARC found

            maxnum = 3
            ncheck = 1
            if ( n.ge.4 ) then

!           --- n > 3, more than 3 numbers read

               iseq = 3
               if ( nelm.eq.0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio.eq.-100 .and. iseq.lt.n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0).lt. 10d0*epsmac .and. iseq.lt.n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(4, 12)

!        --- icurve = 4 or 12, (C)Spline found

            maxnum = n
            ncheck = n

!           --- Check whether Tangent values have been given

            it1 = nint(work(9))
            it2 = nint(work(10))
            if ( itype.eq.4 .and. it2.eq.0 ) then

               call errsub ( 1148, 0, 0, 0 )

            else if ( itype.ne.4 .and. it1.ne.0 ) then

!           --- Tang read, whereas itype # 4

               call errsub ( 1191, 0, 0, 0 )

            end if
            if ( abs(alpha).lt.sqreps ) then
               itype = itype+10
            else if ( abs(alpha-0.5d0).lt.sqreps ) then
               itype = itype+20
            else if ( abs(alpha-1d0).gt.sqreps ) then
               itype = itype+30
            end if

         case(5)

!        --- icurve = 5, CLINE found

            maxnum = 2
            ncheck = 2
            if ( n.ge.3 ) then

!           --- n > 2, more than 2 numbers read

               if ( nodd.eq.0 ) nodd = nint(work(27))

            end if

         case(6)

!        --- icurve = 6, CARC found

            maxnum = 3
            ncheck = 1
            if ( n.ge.4 ) then

!           --- n > 3, more than 3 numbers read

               if ( nodd.eq.0 ) nodd = nint(work(28))

            end if

         case(8)

!        --- icurve = 8, PARAM found

            maxnum = 2
            ncheck = 1
            if ( n.ge.3 ) then

!           --- n > 2, more than 2 numbers read

               iseq = 2
               if ( nelm.eq.0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( nelm.eq.0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio.eq.-100 .and. iseq.lt.n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0).lt. 10d0*epsmac .and. iseq.lt.n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(9)

!        --- icurve = 9, CPARAM found

            maxnum = 2
            ncheck = 1
            if ( n.ge.3 ) then

!           --- n > 2, more than 2 numbers read

               if ( nodd.eq.0 ) nodd = nint(work(27))

            end if

         case(15, 16, 20)

!        --- icurve = 15, 16, 20, PROFILE, CPROFILE, OWN_CURVE found

            maxnum = 2
            ncheck = 2

         case(17)

!        --- icurve = 17, CIRCLE found

         maxnum = ndim
         ncheck = ndim

      end select  ! case ( icurve )

!     --- Fill nodal points in iinput

      if ( iratio.eq.-100 ) iratio = 0
      if ( iratio.eq.3 .or. iratio.eq.4 .or.
     +     iratio.eq.7 .or. iratio.eq.8 ) then

!     --- iratio = 3, 4, 7, 8  special situation: iratio: = iratio-2;
!                        factor: = 1/factor

         iratio = iratio - 2
         if ( abs(factor).gt.epsmac ) factor = 1d0 / factor

      end if
      if ( iratio.le.-4 ) then

!     --- or iratio = -4, -5:  iratio : =  iratio + 2; factor : =  1/factor

         iratio = iratio + 2
         if ( abs(factor).gt.epsmac) factor = 1d0/factor

      end if
      iinput(iinp+3) = iratio
      if ( icurve.ne.8 .and. iratio.lt.0 ) then
         call errint ( iratio, 1 )
         call errint ( icurve, 2 )
         call errsub ( 534, 2, 0, 0 )
      end if
      rinput(icurnr)  = factor
      rinpt1(1,icurnr) = t0
      rinpt1(2,icurnr) = t1
      if ( nelm.lt.0 .or. nelm.gt.10000000 ) then

!     --- nelm out of range

         call errint ( nelm, 1 )
         call errint ( 10000, 2 )
         call errsub ( 1194, 2, 0, 0 )

      end if  ! ( nelm.lt.0 .or. nelm.gt.10000 )

      if ( icurve.le.4 .or. icurve.eq.8 .or. icurve.eq.15 .or.
     +     icurve.eq.17 .or. icurve.eq.20 ) iinput(iinp+2) = nelm
      if ( icurve.eq.4 .or. icurve.eq.12 ) then

!     --- Splines

         if ( itype.eq.4 ) then

!        --- itype = 4, store tangents

            work(25+maxnum) = it1
            work(26+maxnum) = it2
            maxnum = maxnum + 2
            n = n + 2

         end if

         iinput(iinp+4) = itype
         iinput(iinp+5) = n
         iinp = iinp + 2
         rinpt1(1,icurnr) = alpha

      end if
      if ( icurve.eq.15 .or. icurve.eq.16 ) then

!     --- profile

         iinput(iinp+4) = iprofile
         iinp = iinp + 1

      end if
      if ( icurve.eq.20 ) iinput(iinp+3) = ifunc

      if ( n.lt.maxnum ) then

!     --- Too few user points found for curve

         call errint ( icurnr, 1 )
         call errint ( n, 2 )
         call errint ( maxnum, 3 )
         call errsub ( 1286, 3, 0, 0 )

      end if
      do i = 1, maxnum
         iinput(iinp+i+3) = nint(work(24+i))
         if ( iinput(iinp+i+3).eq.0 .or. abs(iinput(iinp+i+3)).gt.
     +        nuspnt ) then

!        --- Error 179: User point number <1 or > number of user points

            call errint ( iinput(iinp+i+3), 1 )
            call errint ( nuspnt, 2 )
            call errint ( i, 3 )
            call errint ( icurnr, 4 )
            call errsub ( 179, 4, 0, 0 )

         end if

      end do
      do i = 1, ncheck-1
         do j = i+1, ncheck
            if ( iinput(iinp+i+3).eq.iinput(iinp+j+3) ) then

!           --- Error 1483: Coinciding user points

               call errint ( i, 1 )
               call errint ( icurnr, 2 )
               call errint ( iinput(iinp+i+3), 3 )
               call errint ( j, 4 )
               call errsub ( 1483, 4, 0, 0 )

            end if

         end do
      end do
      iinp = iinp + 4 + maxnum

      if ( icurve.eq.5 .or. icurve.eq.6 .or. icurve.eq.9 .or.
     +     icurve.eq.12 .or. icurve.eq.16 ) then

!     --- ICURVE = 5, 6, 9, 12, 16  C... found, Fill NODD

         iinput(iinp) = nodd
         iinp = iinp + 1

      end if

      go to 200

1000  call erclos ( 'mshrd1' )
      iinp = iinp + ncurvs - kcurvs

      if ( debug ) then

!     --- Debug information

         call prinin ( iinput, iinp-1, 'iinput' )
         write(irefwr,*) 'End mshrd1'

      end if

      end
