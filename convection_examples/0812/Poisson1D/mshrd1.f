      subroutine mshrd1 ( iinput, rinput, ncurvs, nuspnt, maxcrv,
     +                    ipoint, icode, inpmax, iinp, rinpt1, code,
     +                    ncode, nncode, ncodmx, jcoars )
! ======================================================================
!
!        programmer    Guus Segal
!        version 26.0  date 02-09-2010 Remove intarmsh
!        version 25.2  date 15-08-2009 Extension with ellips
!        version 25.1  date 15-04-2009 Extension with framecurve
!        version 25.0  date 03-03-2008 Remove maxpnt
!
!   copyright (c) 1982-2010  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!    read cards with curves until a card with mesh or with surfaces
!    is found
!    fill arrays iinput and rinput with the information of the
!    curves and the factors respectively
!    iinput and rinput must be filled from position 1
! **********************************************************************
!
!                       KEYWORDS
!
!     curve
!     mesh
!     read
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/celwrk'
      include 'SPcommon/cmcdpr'
      include 'SPcommon/csepch'
      include 'SPcommon/cseprd'
      include 'SPcommon/cseprdnew'
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/celiwork'
      include 'SPcommon/cmesh'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer ncurvs, nuspnt, maxcrv, ipoint(maxcrv), nncode,
     +        icode, inpmax, iinput(inpmax), iinp, ncode(nncode),
     +        ncodmx(nncode), jcoars
      double precision rinput(maxcrv), rinpt1(2,maxcrv)
      character (len=*) code(nncode)

!     code           i    Table with keywords
!     icode          o    return code,  possibilities:
!                          0       next card:  surfaces
!                          1       next card:  volumes
!                          2       next card:  meshline
!                          3       next card:  meshsurface
!                          4       next card:  meshvolume
!                          5       next card:  renumber
!                          6       next card:  plot
!                          7       next card:  end
!                          8       next card:  meshconnect
!                          9       next card:  norenumber
!                         10       next card:  intermediate points
!     iinp           o    next free position in iinput
!     iinput         o    integer input array
!     inpmax         i    maximal space for iinput
!     ipoint         o    pointer array: contains the starting positions of the
!                         curves in array iinput
!     jcoars         i    Indication if coarseness is defined (>0) or not ( = 0)
!     maxcrv         i    Maximum number of curves permitted
!     ncode          i    Table containing the number of significant charcaters
!                         in the keyword table code
!     ncodmx         i    Table containing the number of maximum characters in
!                         the keyword table code
!     ncurvs         o    number of curves
!     nncode         i    Number of entries in the keyword table
!     nuspnt        i/o   number of points
!     rinpt1         o    in this array the initial and end parameters t0 and t1
!                         for the curves are stored temporarily
!                         storage: t0 for curve i in rinpt1(1,i)
!                                  t1 for curve i in rinpt1(2,i)
!     rinput         o    real input array;  factors are filled from position 1,
!                         hence rinput must be started from 8 + ndim * nuspnt
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ncurtp
      parameter ( ncurtp=21 )
      character (len=10) clcode(ncurtp)
      double precision factor, t0, t1, alpha
      integer i, icurnr, inum, maxnum, icurve, ishape, nelm, ncheck,
     +        iratio, nodd, itype, n, it1, it2, iseq, kcurvs, j, ndim,
     +        ncodecl(ncurtp), ncodmxcl(ncurtp), iprofile, ifunc
      logical coarsecurv, speccurv, paramcurv, debug, openfound

!     alpha          Parameter alpha with respect to splines
!     clcode         In this table the significant characters of the various
!                    types
!                    of curves are stored.
!     coarsecurv     If true coarse is used to create the distribution of nodes
!                    along the curve
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     factor         Multiplication factor
!     i              Counting variable
!     icurnr         Curve number
!     icurve         Type of curve (See clcode)
!     ifunc          Parameter ifunc for own_curve
!     inum           Number of numbers read in a record
!     iprofile       Defines the type of profile
!                    Possible values:
!                    1: upper part of naca0012
!                    2: lower part of naca0012
!     iratio         Ratio as defined in input
!     iseq           Sequence number in array work to be used for finding other
!                    data than nodal point numbers
!     ishape         Shape number of elements
!     it1            Nodal point number corresponding to first tangent
!     it2            Nodal point number corresponding to second tangent
!     itype          Indicates the type of action corresponding to keywords
!                    in code
!     j              Counting variable
!     kcurvs         Actual number of curves found (<= ncurvs)
!     maxnum         Maximal number of numbers to be read from the input
!     n              Number of nodes stored in array work
!     ncheck         Number of user points that must be checked
!     ncodecl        Contains the significant number of characters per word in
!                    clcode
!     ncodmxcl       Contains the maximum number of characters per word in
!                    clcode
!     ncurtp         Number of various curve types
!     ndim           Euclidian dimension
!     nelm           Number of elements given by the user
!     nodd           Parameter nodd as defined in input
!     openfound      Indicates if an openings bracket is found before
!                    reading the number or expression
!     paramcurv      If true a parameter curve is created
!     speccurv       If true a special curve (by contraction of copying)
!                    is created
!     t0             Initial t-value in parameter curve
!     t1             End t-value in parameter curve
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer intx00

!     ASKNXT         Read next item from the standard input file
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRCRD         Prints the last record read and underlines the erroneous
!                    part
!     ERREAL         Put real in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     ERRWAR         Print warnings
!     INTX00         Compare text in string with table (variable length)
!     MSHR00         Read information after the part Ci = LINE ( ..
!     PRININ         print 1d integer array
!     READREAL       Read a real or real expression from the input file
!     READSERKEYW    Read number of integers preceded by the same keyword
! **********************************************************************
!
!                       I/O
!
!    Records with respect to the curves are read.
!    The following records are recognized:
!
!    Ci = line j (....)
!    Ci = user j (....)
!    Ci = arc j (....)
!    Ci = spline j (....)
!    Ci = cline j (....)
!    Ci = carc j (....)
!    Ci = curves j (....)
!    Ci = parameter j (....)
!    Ci = cparam j (....)
!    Ci = translate j (....)
!    Ci = rotate j (....)
!    Ci = cspline j (....)
!    Ci = reflect j (....)
!    Ci = spcurve j (....)
!    Ci = profile j (....)
!    Ci = cprofile j (....)
!    Ci = circle j (....)
!    Ci = own_curve j (....)
!    Ci = framecurve j (....)
!    Ci = ellips j (....)
!    Ci = ell_arc j (...)
!    Ci = cell_arc j (...)
!
!    A part of the information between the brackets is interpreted by mshr00
! **********************************************************************
!
!                       ERROR MESSAGES
!
!     128   code for generation of elements (ishape) is incorrect
!     139   curve number does not exist
!     142   Internal error, too few space for integers
!     179   point number of user point is incorrect
!     276   Curve has already been filled
!     494   No user point found in curve to be translated
!     495   Less than 3 user points found in curve to be rotated
!     498   Curve number of curve to be translated or rotated too large
!     534   The value of IRATIO is incorrect
!     601   No curves found after curves of curves
!     648   string could not be recognized
!     966   Reflection axis or plane not specified
!     967   Not enough user points specified for reflection
!    1148   Not tangent given for begin and end point of SPLINE record of
!           type 4
!    1191   Tang has been read, whereas type # 4
!    1194   Value of nelm incorrect
!    1197   Parameter out of range
!    1198   t0>=t1
!    1199   More than 1000 numbers found in curve record
!    1236   Curve number in curve of curves is incorrect
!    1286   Too few user points found
!    1334   Record does not start with a character
!    1336   Ci = is not followed by a text
!    1483   Coinciding user points at one curve
!    1575   Useless parameter found in curve record
!    1616   No coarseness given in relation with carc, cline, cparam or cspline
!    2944   No curve number found following the keyword translate or rotate
!    2945   Wrong string found following reflect
!    2946   No integer number following reflect c
!    2956   No curve number found after the keyword C
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
      data clcode / 'LINE', 'USER', 'ARC', 'SPLINE', 'CLINE', 'CARC',
     +              'CURVES', 'PARAMETER', 'CPARAM', 'TRANSLATE',
     +              'ROTATE', 'CSPLINE', 'REFLECT', 'SPCURVE',
     +              'PROFILE', 'CPROFILE', 'CIRCLE', 'OWN_CURVE',
     +              'FRAMECURVE', 'ELL_ARC', 'CELL_ARC' /
      data ncodecl  / 3, 3, 3, 4, 4,    4, 4, 5, 5, 5,
     +                5, 5, 6, 5, 4,    5, 4, 6, 6, 6,
     +                7 /
      data ncodmxcl / 4, 4, 3, 6, 6,    4, 6, 9, 6, 9,
     +                6, 7, 7, 7, 7,    8, 6, 9,10, 7,
     +                8 /

!     clcode  In this table the significant characters of the various types
!             of curves are stored.
!             The following types of curves are available:
!             1  line
!             2  user
!             3  arc
!             4  spline
!             5  cline
!             6  carc
!             7  curves
!             8  parameter
!             9  cparam
!            10  translate
!            11  rotate
!            12  cspline
!            13  reflect
!            14  spcurve
!            15  profile
!            16  cprofile
!            17  circle
!            18  own_curve
!            19  framecurve
!            20  ell_arc
!            21  cell_arc
! ======================================================================
!
      call eropen ( 'mshrd1' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from mshrd1'
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

!     --- initialize factors and  array ipoint

      ipoint(1:maxcrv) = 0
      rinput(1:maxcrv) = 1d0
      rinpt1(1:2,1:maxcrv) = 0d0

      ncurvs = 0
      kcurvs = 0
      ncheck = 0
      iinp = 1
      ndim = intarmsh(1)

!     --- read next card

200   ityprd = 1
      call asknxt
      if ( debug ) write(irefwr,1) 'ityprd, lastitem', ityprd, lastitem
      if ( iinp>=inpmax ) then

!     --- iinp>=  inpmax

          call errint ( inpmax, 1 )
          call errint ( iinp, 2 )
          call errsub ( 142, 2, 0, 0 )
          go to 1000
      end if

      if ( ityprd/=2 .and. ityprd/=4 ) then

!     --- error 1334: Record does not start with a character
!                      nor an integer

         call errsub ( 1334, 0, 0, 0 )
         call errcrd
         go to 200

      else if ( ityprd==4 .and.
     +          .not. (nchar==1 .and. string(1:1)=='C') ) then

!     --- Detect the type of record
!         Skip this part if the keyword = C
!         If a main keyword has been found, return to calling subroutine

         icode = intx00 ( code, ncode, nncode, 0, ncodmx ) - 1
         if ( debug ) then
            write(irefwr,1) 'icode', icode
            write(irefwr,1) 'ityprd, lastitem/2', ityprd, lastitem
         end if  ! ( debug )
         if ( icode>=0 ) go to 1000

      end if
      if ( ityprd==4 ) then

!     --- Record starts with a character, this must be C

         if ( string(1:1)/='C' ) then
            call errchr ( string(1:1), 1 )
            call errchr ( 'C', 2 )
            call errsub ( 648, 0, 0, 2 )
            call errcrd
            go to 200
         end if

!        --- detect the curve card
!            first the curve number is read

         openfound = .false.
         call readreal ( openfound )

      end if

      if ( ityprd/=2 ) then

!     --- Next item is not an integer

         call errsub ( 2956, 0, 0, 0 )
         call errcrd
         go to 200

      end if

      icurnr = nint(realrd)
      ncurvs = max ( ncurvs, icurnr )
      kcurvs = kcurvs + 1
      if ( icurnr<=0 .or. icurnr>maxcrv ) then

!     --- icurnr has wrong value

         call errint ( icurnr, 1 )
         call errint ( maxcrv, 2 )
         call errsub ( 139, 2, 0, 0 )
         call errcrd
         go to 200

      else if ( ipoint(icurnr)>0 ) then

!     --- Curve icurnr has already been filled

         call errint ( icurnr, 1 )
         call errsub ( 276, 1, 0, 0 )
         call errcrd

      end if

      ipoint(icurnr) = iinp

!     --- Detect type of curve

      call asknxt
      if ( ityprd/=4 ) then

!     --- Not a text found after Ci =

         call errsub ( 1336, 0, 0, 0 )
         call errcrd
         go to 200

      end if

      icurve = intx00 ( clcode, ncodecl, ncurtp, 1, ncodmxcl )
      if ( jcoars==0 .and. (icurve==5 .or. icurve==6 .or.
     +     icurve==9 .or. icurve==12 .or. icurve==19 .or.
     +     icurve==21) ) then
         call errsub ( 1616, 0, 0, 0 )
      end if
      if ( debug ) write(irefwr,1) 'icurnr, icurve', icurnr, icurve

      if ( icurve==0 ) goto 200
      if ( icurve>=18 ) icurve = icurve+2  ! Curve numbers 18 and 19
                                           ! correspond to circle and sphere

      iinput(iinp) = icurve
      iinput(iinp+2) = 0
      iinput(iinp+3) = 0

!     --- Read rest of records depending on the type of curve

      if ( icurve==7 .or. icurve==14 ) then

!     --- curves of curves

         maxnum = 1500
         call readserkeyw ( iwork, maxnum, inum, .false., maxcrv,
     +                      clcode(icurve), 'C' )
         if ( inum<=0 ) then
            call errsub ( 601, 0, 0, 0 )
            call errcrd
         end if
         if ( inum>=1500 ) then
            call errsub ( 1199, 0, 0, 0 )
            call errcrd
         end if
         iinput(iinp+1) = 1
         if ( icurve==14 ) then

!        --- icurve = 14, 4 extra positions corresponding to nelm, iratio
!                         begin point and end point

            iinput(iinp+2) = 0
            iinput(iinp+3) = 2
            iinput(iinp+4) = 0
            iinput(iinp+5) = 0
            iinp = iinp+3

         end if
         iinput(iinp+3) = inum
         do i = 1, inum
            n = iwork(i)
            iinput(iinp+3+i) = n
            n = abs(n)
            if ( n<=0 ) then

!           --- Error 1236: curve in curves of curves<1

               call errint ( icurnr, 1 )
               call errint ( n, 2 )
               call errsub ( 1236, 2, 0, 0 )

            end if
         end do
         iinp = iinp+4+inum
         goto 200

      else if ( icurve==10 .or. icurve==11 ) then

!     --- Translate or Rotate
!         First read Cj

         maxnum = 1
         call readserkeyw ( iwork, maxnum, inum, .true., maxcrv,
     +                      clcode(icurve), 'C' )
         if ( inum<0 ) then

!        --- No curve number found following translate or rotate

            call errint ( icurnr, 1 )
            call errsub ( 2944, 1, 0, 0 )

         end if  ! ( table1(1)<0d0 )

!        --- Next read point numbers

         maxnum = 1499
         call readserkeyw ( iwork(2), maxnum, inum, .false., nuspnt,
     +                      clcode(icurve), 'P' )

         iinput(iinp+1) = 1
         iinput(iinp+3) = inum
         inum = inum+1
         do i = 1, inum
            iinput(iinp+3+i) = iwork(i)
         end do

         if ( icurve==10 ) then
            if ( inum<1) then
               call errsub ( 494, 0, 0, 0 )
               call errcrd
            end if
         else
            if ( inum<3) then
               call errint ( inum, 1 )
               call errsub ( 495, 1, 0, 0 )
               call errcrd
            end if
         end if
         if ( iinput(iinp+4)<=0 ) then
            call errint ( iinput(iinp+4), 1 )
            call errint ( icurnr, 2 )
            call errsub ( 498, 2, 0, 0 )
            call errcrd
         end if

         do i = 1, inum-2
            do j = i+1, inum-1
               if ( abs(iinput(iinp+i+4))==abs(iinput(iinp+j+4)) .and.
     +              .not. (i==1 .and. j==inum-1) ) then

!              --- Error 1483: Coinciding user points

                  call errint ( i, 1 )
                  call errint ( icurnr, 2 )
                  call errint ( iinput(iinp+i+4), 3 )
                  call errint ( j, 4 )
                  call errsub ( 1483, 4, 0, 0 )

               end if

            end do
         end do

         iinp = iinp+4+inum
         goto 200

      else if ( icurve==13 ) then

!     --- Reflect
!         Read number of curve to be reflected

         call asknxt
         if ( ityprd==4 ) then

!        --- string found, check if it is a C

            if ( nchar>1 .or. string(1:1)/='C' ) then

!           --- error not C found

               call errint ( icurnr, 1 )
               call errchr ( string(1:nchar), 1 )
               call errsub ( 2945, 1, 0, 1 )

            end if  ! ( nchar>1 .or. string(1:1)/='C' )
            call asknxt  ! read next item

         end if  ! ( ityprd==4 )

         if ( ityprd/=2 ) then

!        --- Not an integer found following reflect c

            call errint ( icurnr, 1 )
            call errsub ( 2946, 1, 0, 0 )

         end if  ! ( ityprd/=2 )

!        --- Place number in iinput

         iinput(iinp+1) = 1
         iinput(iinp+4) = nint(realrd)

!        --- Read whether reflection on axis or on rplane

         call mshr00 ( work, nuspnt )

         inum = nint(work(1))

         iinput(iinp+3) = inum

         do i = 1, inum
            iinput(iinp+4+i) = nint(work(24+i))
         end do

         if ( work(11)<0.9d0 .or. work(11)>2.1d0 ) then

              call errint( icurnr, 1 )
              call errsub ( 966, 1, 0, 0 )
              call errcrd

         end if

         ndim = nint(work(11)) + 1

         if ( inum<ndim+2 ) then

              call errint( icurnr, 1 )
              call errsub ( 967, 1, 0, 0 )
              call errcrd

         end if

         if ( iinput(iinp+4)<=0 ) then
            call errint ( iinput(iinp+4), 1 )
            call errint ( icurnr, 2 )
            call errsub ( 498, 2, 0, 0 )
            call errcrd
         end if

         iinp = iinp+5+inum

         goto 200

      end if

!     --- Other cases
!         determine shape number

      openfound = .false.
      call readreal ( openfound )
      if ( ityprd==2 ) then

!     --- shape number found

         ishape = nint(realrd)

         if ( ishape<=0 .or. ishape>3 ) then
            call errint ( ishape, 1 )
            call errsub ( 128, 1, 0, 0 )
            call errcrd
         end if

      else

!     --- default shape number

         ishape = 1

      end if
      iinput(iinp+1) = ishape
      if ( debug ) write(irefwr,1) 'ishape', ishape

!     --- Detect rest of record

      call mshr00 ( work, nuspnt )

      n      = nint(work(1))
      nelm   = nint(work(2))
      iratio = nint(work(3))
      factor = work(4)
      nodd   = nint(work(5))
      t0     = work(6)
      t1     = work(7)
      itype  = nint(work(8))
      alpha  = work(12)
      iprofile = nint(work(13))
      ifunc  = nint(work(15))
      coarsecurv = icurve==5 .or. icurve==6 .or. icurve==9 .or.
     +             icurve==16 .or. icurve==23
      speccurv = icurve==2 .or. icurve==7 .or.
     +           icurve>=10 .and. icurve<=13 .or. icurve==21
      paramcurv = icurve==8 .or. icurve==9 .or. icurve==15 .or.
     +            icurve==16
      if ( nelm>0 .and. (coarsecurv .or. speccurv) ) then

!     --- Parameter nelm found where it does not make sense

         call errchr ( 'nelm', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( iratio/=-100 .and. (coarsecurv .or. speccurv) ) then

!     --- Parameter ratio found where it does not make sense

         call errchr ( 'ratio', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(factor-1d0)>1d-5 .and. (coarsecurv .or. speccurv) ) then

!     --- Parameter factor found where it does not make sense

         call errchr ( 'factor', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( nodd/=0 .and. .not. coarsecurv ) then

!     --- Parameter nodd found where it does not make sense

         call errchr ( 'nodd', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(t0)>1d-5 .and. .not. paramcurv ) then

!     --- Parameter init found where it does not make sense

         call errchr ( 'init', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( abs(t1-1d0)>1d-5 .and. .not. paramcurv ) then

!     --- Parameter end found where it does not make sense

         call errchr ( 'end', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if
      if ( itype>=15 .and. itype<=16 ) then

!     --- profile

         if ( t0<0d0 .or. t0>1d0 ) then

!        --- t0 out of range

            call erreal ( t0, 1 )
            call errchr ( 't0', 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1197, 1, 1, 1 )

         end if
         if ( t1<0d0 .or. t1>1d0 ) then

!        --- t1 out of range

            call erreal ( t1, 1 )
            call errchr ( 't1', 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1197, 1, 1, 1 )

         end if
         if ( t0>=t1 ) then

!        --- t0>=t1

            call erreal ( t0, 1 )
            call erreal ( t1, 1 )
            call errint ( iprofile, 1 )
            call errsub ( 1198, 1, 2, 0 )

         end if

      end if
      if ( itype/=1 .and. icurve/=4 .and. icurve/=12 ) then

!     --- Parameter type found where it does not make sense

         call errchr ( 'type', 1 )
         call errint ( icurnr, 1 )
         call errwar ( 1575, 1, 0, 1 )

      end if

!     --- Distinguish between the various curves

      select case ( icurve )

         case(1)

!        --- icurve = 1, LINE found

            maxnum = 2
            ncheck = 2
            if ( n>=3 ) then

!           --- n>2, more than 2 numbers read

               iseq = 2
               if ( nelm==0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio==-100 .and. iseq<n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0)< 10d0*epsmac .and. iseq<n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(2)

!        --- icurve = 2, Userdefined curve found

            maxnum = n
            ncheck = n
            nelm = n-1

         case(3,22)

!        --- icurve = 3, ARC or ELL_ARC found

            maxnum = 3
            ncheck = 1
            if ( n>=4 ) then

!           --- n>3, more than 3 numbers read

               iseq = 3
               if ( nelm==0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio==-100 .and. iseq<n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0)< 10d0*epsmac .and. iseq<n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(4, 12)

!        --- icurve = 4 or 12, (C)Spline found

            maxnum = n
            ncheck = n

!           --- Check whether Tangent values have been given

            it1 = nint(work(9))
            it2 = nint(work(10))
            if ( itype==4 .and. it2==0 ) then

               call errsub ( 1148, 0, 0, 0 )

            else if ( itype/=4 .and. it1/=0 ) then

!           --- Tang read, whereas itype # 4

               call errsub ( 1191, 0, 0, 0 )

            end if
            if ( abs(alpha)<sqreps ) then
               itype = itype+10
            else if ( abs(alpha-0.5d0)<sqreps ) then
               itype = itype+20
            else if ( abs(alpha-1d0)>sqreps ) then
               itype = itype+30
            end if

         case(5)

!        --- icurve = 5, CLINE found

            maxnum = 2
            ncheck = 2
            if ( n>=3 ) then

!           --- n>2, more than 2 numbers read

               if ( nodd==0 ) nodd = nint(work(27))

            end if

         case(6, 23)

!        --- icurve = 6, 23 CARC or CELL_ARC found

            maxnum = 3
            ncheck = 1
            if ( n>=4 ) then

!           --- n>3, more than 3 numbers read

               if ( nodd==0 ) nodd = nint(work(28))

            end if

         case(8)

!        --- icurve = 8, PARAM found

            maxnum = 2
            ncheck = 1
            if ( n>=3 ) then

!           --- n>2, more than 2 numbers read

               iseq = 2
               if ( nelm==0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( nelm==0 ) then
                  nelm = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( iratio==-100 .and. iseq<n ) then
                  iratio = nint(work(25+iseq))
                  iseq = iseq+1
               end if
               if ( abs(factor-1d0)< 10d0*epsmac .and. iseq<n )
     +            then
                  factor = work(25+iseq)
                  iseq = iseq+1
               end if

            end if

         case(9)

!        --- icurve = 9, CPARAM found

            maxnum = 2
            ncheck = 1
            if ( n>=3 ) then

!           --- n>2, more than 2 numbers read

               if ( nodd==0 ) nodd = nint(work(27))

            end if

         case(15, 16, 20)

!        --- icurve = 15, 16, 20, PROFILE, CPROFILE, OWN_CURVE found

            maxnum = 2
            ncheck = 2

         case(17)

!        --- icurve = 17, CIRCLE found

            maxnum = ndim
            ncheck = ndim

         case(21)

!        --- icurve = 21, framecurve found

            maxnum = n
            ncheck = n

      end select  ! case ( icurve )

!     --- Fill nodal points in iinput

      if ( iratio==-100 ) iratio = 0
      if ( iratio==3 .or. iratio==4 .or.
     +     iratio==7 .or. iratio==8 ) then

!     --- iratio = 3, 4, 7, 8  special situation: iratio: = iratio-2;
!                        factor: = 1/factor

         iratio = iratio - 2
         if ( abs(factor)>epsmac ) factor = 1d0 / factor

      end if
      if ( iratio<=-4 ) then

!     --- or iratio = -4, -5:  iratio : =  iratio + 2; factor : =  1/factor

         iratio = iratio + 2
         if ( abs(factor)>epsmac ) factor = 1d0/factor

      end if
      iinput(iinp+3) = iratio
      if ( icurve/=8 .and. iratio<0 ) then
         call errint ( iratio, 1 )
         call errint ( icurve, 2 )
         call errsub ( 534, 2, 0, 0 )
      end if
      rinput(icurnr)  = factor
      rinpt1(1,icurnr) = t0
      rinpt1(2,icurnr) = t1
      if ( nelm<0 .or. nelm>10000 ) then

!     --- nelm out of range

         call errint ( nelm, 1 )
         call errint ( 10000, 2 )
         call errsub ( 1194, 2, 0, 0 )

      end if  ! ( nelm<0 .or. nelm>10000 )

      if ( icurve<=4 .or. icurve==8 .or. icurve==15 .or.
     +     icurve==17 .or. icurve==20 .or. icurve==22 )
     +   iinput(iinp+2) = nelm
      if ( icurve==4 .or. icurve==12 ) then

!     --- Splines

         if ( itype==4 ) then

!        --- itype = 4, store tangents

            work(25+maxnum) = it1
            work(26+maxnum) = it2
            maxnum = maxnum + 2
            n = n + 2

         end if

         iinput(iinp+4) = itype
         iinput(iinp+5) = n
         iinp = iinp + 2
         rinpt1(1,icurnr) = alpha

      else if ( icurve==15 .or. icurve==16 ) then

!     --- profile

         iinput(iinp+4) = iprofile
         iinp = iinp + 1

      else if ( icurve==21 ) then

!     --- framecurve

         iinput(iinp+4) = 1     ! itype (not yet used)
         iinput(iinp+5) = n     ! number of nodes
         iinp = iinp + 2        ! two extra positions

      end if
      if ( icurve==20 ) iinput(iinp+3) = ifunc

      if ( n<maxnum ) then

!     --- Too few user points found for curve

         call errint ( icurnr, 1 )
         call errint ( n, 2 )
         call errint ( maxnum, 3 )
         call errsub ( 1286, 3, 0, 0 )

      end if
      do i = 1, maxnum
         iinput(iinp+i+3) = nint(work(24+i))
         if ( iinput(iinp+i+3)==0 .or. abs(iinput(iinp+i+3))>
     +        nuspnt ) then

!        --- Error 179: User point number <1 or >number of user points

            call errint ( iinput(iinp+i+3), 1 )
            call errint ( nuspnt, 2 )
            call errint ( i, 3 )
            call errint ( icurnr, 4 )
            call errsub ( 179, 4, 0, 0 )

         end if

      end do
      do i = 1, ncheck-1
         do j = i+1, ncheck
            if ( iinput(iinp+i+3)==iinput(iinp+j+3) ) then

!           --- Error 1483: Coinciding user points

               call errint ( i, 1 )
               call errint ( icurnr, 2 )
               call errint ( iinput(iinp+i+3), 3 )
               call errint ( j, 4 )
               call errsub ( 1483, 4, 0, 0 )

            end if

         end do
      end do
      iinp = iinp + 4 + maxnum

      if ( icurve==5 .or. icurve==6 .or. icurve==9 .or.
     +     icurve==12 .or. icurve==16 .or. icurve==21 .or.
     +     icurve==23 ) then

!     --- ICURVE = 5, 6, 9, 12, 16, 21  C... found, Fill NODD

         iinput(iinp) = nodd
         iinp = iinp + 1

      end if

      go to 200

1000  call erclos ( 'mshrd1' )
      iinp = iinp + ncurvs - kcurvs

      if ( debug ) then

!     --- Debug information

         call prinin ( iinput, iinp-1, 'iinput' )
         write(irefwr,*) 'End mshrd1'

      end if

      end
