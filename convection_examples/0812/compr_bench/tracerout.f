c *************************************************************
c *   TRACEROUT
c *
c *   PvK 970413
c *************************************************************
      subroutine tracerout(coormark,coorreal,no)
      implicit none
      real*8 coormark(2,*)
      real*4 coorreal(2,*)
      integer no
      include 'tracer.inc'
      include 'SPcommon/ctimen'
      include 'c1mark.inc'
      character*80 fname
      integer ip,i,ntot
      real*8 x,y

      if (itracoption.eq.1) then
        write(fname,'(''tracers.'',i3.3)') no
        open(88,file=fname,form='unformatted')
        write(88) ndist,(ntrac(idist),idist=1,ndist)

        ntot=0
        do idist=1,ndist
           ntot=ntot+ntrac(idist)
        enddo

        do i=1,ntot
           coorreal(1,i) = coormark(1,i)
           coorreal(2,i) = coormark(2,i)
        enddo

        ip=0
        do idist=1,ndist
          write(88) (coorreal(1,ip+i),coorreal(2,ip+i),i=1,ntrac(idist))
          ip = ip+ntrac(idist)
        enddo
        close(88)
        write(6,*) 'Tracers stored at t = ',t,no
      endif
 
      if (itracoption.eq.2) then
        write(fname,'(''markers.'',i3.3)') no
        open(88,file=fname)
        do i=1,imark(1)
           write(88,*) coormark(1,i),coormark(2,i)
        enddo
        close(88)
        write(6,*) 'Markers stored at t = ',t,no
      endif
  
      return
      end
