      real*8 alpha_dim,rho_dim,cp_dim,Ts_dim,rk_dim,Tso_dim
      real*8 rkappa_dim,g_dim,eta0_dim,rhoc_dim
      real*8 cpc_dim,R2_dim,R1_dim,DeltaT_dim,dtout_d,tmax_d
      real*8 year_dim,tscale_dim,height_dim,QBSE_dim
      real*8 bulkmodulus
      real*8 velocity_scale,pressure_scale

c     *** surface values of alpha, diffusivity, conductivity
      parameter(alpha_dim=3e-5,bulkmodulus=1e11)
      parameter(rkappa_dim=1e-6)
      parameter(rk_dim=4.2)
c     *** average mantle density 
      parameter(rho_dim=3500d0)
c     *** dimensional radii of CMB and Earth's surface. 
      parameter(R1_dim=3486e3,R2_dim=6371e3,height_dim=R2_dim-R1_dim)
c     *** density and specific heat in the core
      parameter(rhoc_dim=11 000d0, cpc_dim=500d0)
c     *** Bulk Silicate Earth heating 
      parameter(QBSE_dim=4.8e-12)
c     *** gravity
      parameter(g_dim=10d0)
c     *** specific heat
      parameter(cp_dim=1200d0)
c     *** scale for mantle viscosity
      parameter(eta0_dim=1e22)
c     *** year in seconds
      parameter(year_dim=365.25*3600*24)
c     *** tscale_dim translates Byr to non-dimensional units
      parameter(tscale_dim=rkappa_dim*year_dim*1e9/
     v                     (height_dim*height_dim))
c     *** velocity scale (m/s)
      parameter(velocity_scale=rkappa_dim/height_dim)
c     *** pressure scale (Pa)
      parameter(pressure_scale=rkappa_dim*eta0_dim/
     v          (height_dim*height_dim))
      parameter(Ts_dim=273)

      common /dimensional/ DeltaT_dim,Tso_dim,dtout_d,tmax_d
