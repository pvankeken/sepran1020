c *************************************************************
c *   CHECKBOUNDS
c *
c *   Make sure the tracers don't get advected past the mesh boundaries.
c *   PvK 2000
c *************************************************************
      subroutine checkbounds(ichoice,xm,ym)
      implicit none
      integer ichoice
      real*8 xm,ym,r,cost,sint,th
      include 'ccc.inc'
      include 'crminmax.inc'
      include 'bound.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)

      if (ichoice.eq.0) then 
        r = sqrt(xm*xm+ym*ym)
        if (r.gt.radius_max+2e-7) then
           cost = ym/r
           if (xm.gt.0) then
              th = acos(cost)
           else
              th = 2*pi - acos(cost)
           endif
           write(6,'(''modify 1: '',4f10.7,$)') xm,ym,r,th
           xm = xm - (r-radius_max+eps_tracer_bound)*sin(th)
           ym = ym - (r-radius_max+eps_tracer_bound)*cos(th)
           r = sqrt(xm*xm+ym*ym)
           write(6,'(3x,3f10.7)') xm,ym,r
        endif

        if (r.lt.(radius_min-2e-7)) then
           cost = ym/r
           if (xm.gt.0) then
              th = acos(cost)
           else
              th = 2*pi - acos(cost)
           endif
           write(6,'(''modify 2: '',4f12.9,$)') xm,ym,r,radius_min
           xm = xm + (radius_min-r+eps_tracer_bound)*sin(th)
           ym = ym + (radius_min-r+eps_tracer_bound)*cos(th)
           r = sqrt(xm*xm+ym*ym)
           write(6,'(3f12.9)') r,xm,ym
        
        endif
      endif

c     *** for the half geometry
      if (half) then    
         if (xm.lt.0) then
c           write(6,*) 'half: xm<0:',xm,ym
            xm=eps_tracer_bound
         endif
      else if (quart) then
c        *** and for the quart ...
         if (xm.lt.0) xm=eps_tracer_bound
         if (ym.lt.0) ym=eps_tracer_bound
      else if (frac.eq.0.125) then
c        *** for an eight section
         if (xm.lt.0) xm=eps_tracer_bound
         if (xm.ge.ym) xm = ym-eps_tracer_bound
      endif

      return
      end
