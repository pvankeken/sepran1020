c *************************************************************
c *   FLUSHDATAFILE
c *
c *   Write real data (t, Nu, vrms etc.) to compress.data
c *************************************************************
      subroutine flushdatafile
      implicit none
      include 'perealdata.inc'
      include 'SPcommon/cmachn'
      integer no,i,k,nrec,kmax

      kmax=4

      open(66,file='compress.data',form='unformatted', 
     v     access='direct',
     v     recl=2*lenwor)
      read(66,rec=1) no,nrec
      close(66)

      if (nrec.lt.15) then
         write(6,*) 'PERROR(flushdatafile): nrec < 15'
         write(6,*) 'increase to at least 15 in main program'
         call instop
      endif
      open(66,file='compress.data',form='unformatted',
     v     access='direct',
     v     recl=nrec*lenwor)
      write(66,rec=1) no+nodata,nrec
      do i=1,nodata
         write(66,rec=no+i+1) 
     v        tr(i),vr(i),
     v        q_ca(1,i),q_ca(2,i),q_ca(3,i),t_bota(i),
     v        veloc1(i),veloc2(i),
     v        heatava(i),phiava(i),
     v        avt_a(1,i),avt_a(2,i),avt_a(3,i)
      enddo

      open(66,file='degas.data',form='unformatted', 
     v     access='direct',
     v     recl=2*lenwor)
      read(66,rec=1) no,nrec
      close(66)

      if (nrec.lt.35) then
         write(6,*) 'PERROR(flushdatafile): nrec < 35'
         write(6,*) 'increase to at least 35 in main program'
         write(6,*) 'for file degas.data'
         call instop
      endif
      open(66,file='degas.data',form='unformatted',
     v     access='direct',
     v     recl=nrec*lenwor)
      write(66,rec=1) no+nodata,nrec
      do i=1,nodata
c        write(6,*) 'kmax=',kmax,
c    v       nconta(i),(ndegasa(k,i),a238loss(k,i),
c    v       a235loss(k,i),a232loss(k,i),aHe3loss(k,i),aHe4loss(k,i),
c    v       aK40loss(k,i),aAr40loss(k,i),k=1,kmax)
         write(66,rec=no+i+1) 
     v       nconta(i),
     v       ( ndegasa(k,i),a238loss(k,i),
     v        a235loss(k,i),a232loss(k,i),
     v        aHe3loss(k,i),aHe4loss(k,i),
     v        aK40loss(k,i),aAr40loss(k,i),k=1,kmax)
      enddo
      nodata = 0
      close(66)

      return
      end
