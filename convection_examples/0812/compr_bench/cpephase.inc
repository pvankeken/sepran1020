c     *** /CPEPHASE/
c     *** Ra       - Thermal Rayleigh number
c     *** Rb       - Phase Bouyancy numbers
c     *** Di       - Dissipation number
c     *** gamma    - CC slope phase change
c     *** gRbRa    - gamma(1)*Rb/Ra
c     *** g2RbRaDi = gamma(1)*gRbRa*Di
c     *** phz0     - depth phase change (at T=0)
c     *** phdz     - depth interval of phase change
c     *** nph      - number of phase changes in model
      real*8 Ra,Rb,Di,gamma,phz0,phdz,pht0,gRbRa,g2RbRaDi
      real*8 gamma_d,phz0_d,phdz_d,pht0_d
      real*8 drho_rel,DiRa,Grueneisen,DiG
      integer nph,iadiabat
      common /cpephase/ Ra,Rb(10),Di,Grueneisen,gamma(10),
     v                 gRbRa,g2RbRaDi,DiRa,phz0(10),phdz(10),
     v                 pht0(10),drho_rel(10),DiG,nph,iadiabat
      common /cpephase_dim/ gamma_d(10),phz0_d(10),phdz_d(10),pht0_d(10)
