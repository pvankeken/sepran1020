c *************************************************************
c *   READCHEM
c *
c *   PvK 970419
c *************************************************************
      subroutine readchem(chemreal,chemmark,ichem,fname)
      implicit none
      integer ichem
      real*8 chemmark(ichem,*)
      real*4 chemreal(*)
      character*80 fname
      integer no
      include 'tracer.inc'
      include 'SPcommon/ctimen'
      include 'degas.inc'
      integer ip,i,ntot,jchem,nchem
      real*8 x,y

      if (itracoption.eq.2) then
         write(6,*) 'PERROR(readchem): not suited for the '
         write(6,*) 'markerchain method'
         call instop
      endif
 


      U238  = U238_0 *(1 - exp ( -rl238 * t * tscale )  )
      U235  = U235_0 *(1 - exp ( -rl235 * t * tscale )  )
      Th232 = Th232_0*(1 - exp ( -rl232 * t * tscale )  )
      K40 = K40_0*(1 - exp ( -rl40 * t * tscale )  )
      He4 = 8*(U238_0-U238) + 7*(U235_0-U238) + 6*(Th232_0-Th232)
      Ar40 = K40_eff*K40

      open(88,file=fname,form='unformatted')
      read(88) ndist,(ntrac(idist),idist=1,ndist),nchem

      if (ichem.gt.nchem) then
c        *** Probably an incorrect file
         write(6,*) 'PERROR(readchem): ichem > nchem'
         write(6,*) '  ichem,nchem   : ',ichem,nchem
      endif

      ntot=0
      do idist=1,ndist
         ntot=ntot+ntrac(idist)
      enddo


c     ip=0
      do jchem=1,ichem
         do idist=1,ndist
            read(88) (chemreal(ip+i),i=1,ntrac(idist))
            ip = ip+2*ntrac(idist)
         enddo
         do i=1,ntot
            chemmark(jchem,i) = chemreal(i)
         enddo
      enddo
      close(88)


      return
      end
