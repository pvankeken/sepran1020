c *************************************************************
c *   NusseltwT
c *   Compute volume average Nu = 1 + <wT>; find upward velocity
c *   and temperature in each point; place in array user and use
c *   volint to integrate.
c *
c *   PvK 012611
c *************************************************************
      subroutine nusseltwT(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v               iuser,user,NuwT)
      implicit none 
      integer kmesh1(*),kprob1(*),isol1(*),kmesh2(*),kprob2(*),isol2(*)
      integer iuser(*)
      real*8 user(*),NuwT
   
      include 'pecof900.inc'
      include 'ccc.inc'
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))
      integer indprf1,nphys1,npoint1,ipusol1,ipusol2
      integer nunkp1,inidgt,iniget,ipkprf1,ihelp
      real*8 volint

      npoint1 = kmesh1(8)
      call ini050(isol1(1),'nusseltwT-isol1') 
      call ini050(isol2(1),'nusseltwT-isol2') 
      ipusol1 = inidgt(isol1(1))
      ipusol2 = inidgt(isol2(1))
      nunkp1 = kprob1(4)
      indprf1 = kprob1(19)
      if (indprf1 /= 0) then
         call ini050(indprf1,'nusseltwT-indprf')
         ipkprf1=iniget(indprf1)
      endif
      call nusseltwT001(npoint1,buffr(ipusol1),buffr(ipusol2),
     v          nunkp1,indprf1,ibuffr(ipkprf1),user(6))

!     *** Set up integration
      iuser(1)=100
      iuser(2)=1
      iuser(3)=0
      iuser(4)=0
      iuser(5)=0
      iuser(6)=7
      iuser(7)=intrule900
      iuser(8)=icoor900
      iuser(9)=0
      iuser(10)=2001
      iuser(11)=6

      NuwT=1+volint(0,1,1,kmesh1,kprob1,isol1,iuser,user,
     v            user,ihelp)/volume

      end

      subroutine nusseltwT001(npoint,usol1,usol2,nunkp,
     v             indprf,kprobf,user)
      implicit none
      integer npoint,nunkp,indprf,kprobf
      real*8 usol1(*),usol2(*),user(*)
      integer i,j1,j2

      if (indprf == 0) then
         do i=1,npoint
            j2 = (i-1)*nunkp+2
            user(i) = usol1(j2)*usol2(i)
         enddo
      else
         do i=1,npoint
            j1=kprobf(i)+1
            j2=j1+1
            user(i) = usol1(j2)*usol2(i)
         enddo
      endif

      end
