c *************************************************************
c *   PREDCOORT
c *
c *   Explicit Euler step for tracer/markerchain advance during
c *   the predictor step.
c *
c *   coornewm := coormark + velmark*tstep
c *
c *   Information about the number of tracers/markers is stored
c *   in tracer.inc
c *   
c *   This version combines the cylindrical and cartesian box versions
c *************************************************************
      subroutine predcoort(coormark,velmark,coornewm)
      implicit none
      real*8 coormark(*),velmark(*),coornewm(*)
      include 'ccc.inc'

      if (cyl) then
         call predcoortcyl(coormark,velmark,coornewm)
      else
         call predcoortcart(coormark,velmark,coornewm)
      endif

      return
      end
  

c *************************************************************
c *   PREDCOORT
c *
c *   predcoor, adapted for use with tracers 
c *
c *   PvK 960214
c *
c *   PvK 970411: modified for cylindrical geometry.
c *   PvK 073104: (re)adapted for markerchain
c *************************************************************
      subroutine predcoortcyl(coormark,velmark,coornewm)
      implicit none
      real*8 coormark(2,*),velmark(2,*),coornewm(2,*)
      include 'SPcommon/ctimen'
      include 'tracer.inc'
      integer i,j,ntot,nmark,ip
      real*8 xm,ym,r,th,pi,cost
      parameter(pi=3.1415926 535898)
      include 'ccc.inc'
      include 'crminmax.inc'
      include 'elem_topo.inc'
      include 'c1mark.inc'
      real*8 thetal,costh,sinth,vr,vth,u,v
      integer ip1,ip2
     
      if (itracoption.eq.2) then
c        *** markerchain method
         nmark=0
         do ichain=1,nochain
            nmark = nmark+imark(ichain) 
         enddo
         ntot=nmark
      else 
         ntot=0
         do idist=1,ndist
            ntot=ntot+ntrac(idist)
         enddo
      endif

      do i = 1,ntot
         xm = coormark(1,i)
         ym = coormark(2,i)
         r = sqrt(xm*xm+ym*ym)

         if (r.lt.rtop_threshold.and.r.gt.rbot_threshold) then 
c           *** particle is sufficiently far away from the
c           *** top and bottom boundary to use Cartesian coordinates

            xm = xm+tstep*velmark(1,i)
            ym = ym+tstep*velmark(2,i)

         else

c           *** particle is deemed too close to top or bottom boundary;
c           *** advect particle in cylindrical coordinates such that
c           *** the rigid rotation of the surface is taken into
c           *** account in an accurate manner.
            thetal = acos(ym/r)
            u = velmark(1,i)
            v = velmark(2,i)
            costh = cos(thetal)
            sinth = sin(thetal)
            vth = costh*u - sinth*v
            vr  = sinth*u + costh*v
            thetal = thetal + tstep*vth/r
            r      = r      + tstep*vr
            r = min(r,radius_max)
            r = max(r,radius_min)
            xm = r*sin(thetal)
            ym = r*cos(thetal)

         endif

         call checkbounds(0,xm,ym)
         coornewm(1,i) = xm
         coornewm(2,i) = ym
      enddo

      if (itracoption.eq.2) then
c     *** make sure markers at boundaries stay on boundary
         if (quart) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = ip+1
               coornewm(1,ip1) = 0
c              *** y coordinate of last tracer should be zero
               ip2 = ip+nmark
               coornewm(2,ip2) = 0
               ip = ip+nmark
            enddo
         else if (half) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = ip+1
               coornewm(1,ip1) = 0
c              *** x coordinate of last tracer should be zero
               ip2 = ip+nmark
               coornewm(1,ip2) = 0
               ip = ip+nmark
            enddo
         else if (eighth) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = ip+1
               coornewm(1,ip1) = 0
c              *** x and y coordinates of last tracer should be equal
               ip2 = ip+nmark
               coornewm(1,ip2) = coornewm(2,ip2)
               ip = ip+nmark
            enddo
         endif
      endif

      return
      end

c *************************************************************
c *   PREDCOORTCART
c *
c *   PvK 10-8-89
c *************************************************************
      subroutine predcoortcart(coormark,velmark,coornewm)
      implicit none
      real*8 coormark(*),velmark(*),coornewm(*)

      include 'SPcommon/ctimen'
      include 'tracer.inc'
      include 'pexcyc.inc'
      include 'c1mark.inc'
      integer ntot,nmark,i,j,ip1,ip2,ip
    
      if (itracoption.eq.2) then
         nmark=0
         do ichain=1,nochain
            nmark = nmark + imark(ichain)
         enddo
         ntot = nmark
      else
         ntot = 0
         do idist=1,ndist
            ntot = ntot+ntrac(idist)
         enddo
      endif
      do i = 1,ntot
         ip1 = 2*i-1
         ip2 = 2*i
         coornewm(ip1) = coormark(ip1) + tstep*velmark(ip1)
         coornewm(ip2) = coormark(ip2) + tstep*velmark(ip2)
         call checkbounds_cart(coornewm(ip1),coornewm(ip2))
      enddo

      if (itracoption.eq.2) then
c        *** make sure markers at boundaries stay at boundary
         ip=0
         do ichain=1,nochain
            nmark=imark(ichain)
            ip1 = ip+1
            coornewm(2*ip1-1) = xcmax
            ip2 = ip+nmark
            coornewm(2*ip2-1) = xcmin
            ip=ip+nmark
         enddo
      endif

      return
      end
