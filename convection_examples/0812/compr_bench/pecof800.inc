      integer ichtime
      logical variable_Ra
      real*8 wavenumber,amppert,Ra_max
      integer metupw,intrule,icoorsystem,iqtype
      common /pecof800/ Ra_max,wavenumber,amppert,ichtime,metupw,
     v    intrule,icoorsystem,iqtype,qwithrho,qnondim,Nu_with_Tbar,
     v    variable_Ra
      logical qwithrho,qnondim,Nu_with_Tbar
      real*8 q_layer,totdens,q_layer_d
      common /peheatprod/ q_layer(10),totdens,q_layer_d(10)
      real*8 heat_flux_in
      integer numnat800
      common /heatfluxbc/ heat_flux_in,numnat800
      
