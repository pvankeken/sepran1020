c *************************************************************
c *   MARK4
c *
c *   Update tracers/markerchain using 4th order RK for the
c *   corrector step.
c *   Old velocity is stored in velmark; isolnew and isolav
c *   contain the velocity at t+dt and t+0.5*dt. 
c *
c *   This version combines the cylindrical and cartesian box geometry
c *  
c *   PvK 071505
c *************************************************************
      subroutine mark4(kmesh,kprob,isolnew,isolav,user)
      implicit none
      integer kmesh(*),kprob(*),isolnew(*),isolav(*)
      real*8 user
      include 'ccc.inc'
  
      if (cyl) then
         call mark4cyl(kmesh,kprob,isolnew,isolav,user)
      else 
         call mark4cart(kmesh,kprob,isolnew,isolav,user)
      endif
    
      return
      end

c *************************************************************
c *   MARK4cyl
c *
c *   Transport markers using 4th order Runge Kutta method.
c *
c *   kmesh          i    Mesh definition for velocity
c *   kprob          i    Problem definition for velocity
c *   isolnew        i    Predicted velocity at t=t0+dt
c *   isolav         i    Mid-point velocity at t=t0+0.5*dt
c *   user           s    Scratch area to temporarily store velocity info
c *   velmark        i    Old velocity of markers at t=t0
c *   coormark       i    Old position of markers at t=t0
c *   coornewm       io   At input: predicted position of markers
c *                                 at t=t1=t0+dt
c *                       At output: corrected position of markers at t=t1
c *
c *   PvK 101000: adapted from mark4 for use with cylindrical coordinates
c *
c *   PvK 073004: adapted to work with markerchain (itracoption=2)
c *************************************************************
      subroutine mark4cyl(kmesh,kprob,isolnew,isolav,user)
      implicit none
      integer kmesh(*),kprob(*),isolnew(*),isolav(*)
      real*8 user(*)

      integer ibuffr
      common ibuffr(1)
      real*8 pi
      parameter(pi=3.1415926 535898)

      include 'SPcommon/ctimen'
      include 'pexcyc.inc'
      include 'petrac.inc'
      include 'tracer.inc'
      include 'ccc.inc'
      include 'crminmax.inc'
      include 'elem_topo.inc'
      include 'c1mark.inc'
      common /pedebug/ debug
      logical debug

      integer nmark,i,npoint,nelem,ikelmc,ikelmi,iel,ip,j,ip1,ip2
      integer ikelmo,nelgrp
      real*8 rk1x,rk2x,rk3x,rk4x,rk1y,rk2y,rk3y,rk4y
      real*8 xm,ym,factor,u,v,xn(6),yn(6),th
      real*8 shapef(6),un(6),vn(6),cost,r,vx,vy,const(10)
      integer nodno(6),ntot,iniget,inidgt,iconst(10)
      real*8 vth,vr,theta_init,r_init,thetal,costh,sinth
      real*8 rk1th,rk2th,rk3th,rk4th,rk1r,rk2r,rk3r,rk4r
      real*4 second,t00,t01

      t00 = second()
c     *** Prepare for interpolation of velocity (see tdetvel.f)
      call ini050(kmesh(23),'mark4cyl: coordinates')
      call ini050(kmesh(17),'mark4cyl: nodalpoints')
      call ini050(kmesh(29),'mark4cyl: kmesh part o')
      npoint = kmesh(8)
      nelem  = kmesh(9)
      nelgrp = kmesh(5)
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      ikelmo = iniget(kmesh(29))
      if (10+4*npoint.gt.user(1)) then
         write(6,*) 'PERROR(mark4cyl): user is too small'
         write(6,*) 'user(1): ',user(1)
         write(6,*) 'needed: ',10+4*npoint
         call instop
      endif

c     *** Both the averaged velocity at t=t+0.5*dt and the
c     *** predicted velocity at t=t+dt are needed
      factor=1d0
      call pecopy(2,isolav,user,kmesh,kprob,10,factor)
      call pecopy(3,isolav,user,kmesh,kprob,10+npoint,factor)
      call pecopy(2,isolnew,user,kmesh,kprob,10+2*npoint,factor)
      call pecopy(3,isolnew,user,kmesh,kprob,10+3*npoint,factor)

      if (itracoption.eq.2) then
c        *** ntot = total number of markers
         nmark=0
         do ichain=1,nochain
            nmark=nmark+imark(ichain)
         enddo 
         ntot = nmark
      else
c        *** ntot = total number of tracers
         ntot=0
         do idist=1,ndist
            ntot=ntot+ntrac(idist)
         enddo
      endif

c     *** loop over tracers/markers
      do j=1,ntot
         ip1 = 2*j-1
         ip2 = 2*j
         xm = coormark(ip1)
         ym = coormark(ip2)
         r = sqrt(xm*xm+ym*ym)

         if (r.lt.rtop_threshold.and.r.gt.rbot_threshold) then
c           *** particle is sufficiently far away from top and 
c           *** bottom boundary to use Cartesian coordinates
c           *** for particle tracing

c           *** Calculate estimate for mid-point
            rk1x = tstep*velmark(ip1)
            rk1y = tstep*velmark(ip2)
            xm = coormark(ip1) + 0.5*rk1x
            ym = coormark(ip2) + 0.5*rk1y

c           *** Find velocity at mid-point at t=t+0.5*dt
            call checkbounds(1,xm,ym)
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,
     v                    npoint,nelem,nelgrp,u,v,iel,user) 
            rk2x = tstep*u
            rk2y = tstep*v
            xm = coormark(ip1) + 0.5*rk2x
            ym = coormark(ip2) + 0.5*rk2y

c           *** find velocity at corrected mid-point at t=t+0.5*dt
            call checkbounds(1,xm,ym)
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,
     v                       npoint,nelem,nelgrp,u,v,iel,user)
c           *** Find position of marker at t=t+dt
            rk3x = tstep*u
            rk3y = tstep*v
            xm = coormark(ip1) + rk3x
            ym = coormark(ip2) + rk3y

c           *** Find velocity in new marker position at t=t+dt
            call checkbounds(1,xm,ym)
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,npoint,nelem, 
     v                nelgrp,u,v,iel,user(2*npoint+1))
            rk4x = tstep*u
            rk4y = tstep*v

c           *** Combine all the estimates in the 4th order Runge Kutta step
            xm  = coormark(ip1)+ rk1x/6d0 + rk2x/3d0 +
     v                            rk3x/3d0 + rk4x/6d0
            ym  = coormark(ip2)+ rk1y/6d0 + rk2y/3d0 +
     v                            rk3y/3d0 + rk4y/6d0
            call checkbounds(1,xm,ym)

        else

c           *** use cylindrical coordinates for tracing of particles
c           *** near the top surface
            thetal = acos(ym/r)
            theta_init = thetal
            r_init     = r

            u   = velmark(ip1)
            v   = velmark(ip2)
            costh = cos(thetal)
            sinth = sin(thetal)
            vth = (costh*u - sinth*v)/r
            vr  = sinth*u + costh*v

            rk1th = tstep*vth
            rk1r  = tstep*vr
c           *** Calculate estimate for mid-point
            thetal = theta_init + 0.5*rk1th
            r     =  r_init     + 0.5*rk1r
            r = min(r,radius_max)
            r = max(r,radius_min)
c           *** convert to cartesian
            costh = cos(thetal)
            sinth = sin(thetal)
            xm = r*sinth
            ym = r*costh
c           *** find new velocity in Cartesian coordinates
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,
     v                    npoint,nelem,nelgrp,u,v,iel,user) 
c           *** convert to cylindrical
            costh = cos(thetal)
            sinth = sin(thetal)
            vth = (costh*u - sinth*v)/r
            vr  = sinth*u + costh*v
c           *** advance tracer to new position
            rk2th = tstep*vth
            rk2r  = tstep*vr
            thetal = theta_init + 0.5*rk2th
            r     =  r_init     + 0.5*rk2r
            r = min(r,radius_max)
            r = max(r,radius_min)
c           *** convert new position to Cartesian
            costh = cos(thetal)
            sinth = sin(thetal)
            xm = r*sinth
            ym = r*costh
c           *** find velocity at corrected mid-point at t=t+0.5*dt
c           *** in Cartesian
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,
     v                       npoint,nelem,nelgrp,u,v,iel,user)
c           *** Find position of marker at t=t+dt
c           *** convert to cylindrical
            vth = (costh*u - sinth*v)/r
            vr  = sinth*u + costh*v
            rk3th = tstep*vth
            rk3r  = tstep*vr
            thetal = theta_init + rk3th
            r     =  r_init     + rk3r
            r = min(r,radius_max)
            r = max(r,radius_min)
c           *** convert new position to Cartesian
            costh = cos(thetal)
            sinth = sin(thetal)
            xm = r*sinth
            ym = r*costh
c           *** Find velocity in new marker position at t=t+dt
            call velintmarkc(xm,ym,ikelmc,ikelmi,ikelmo,npoint,nelem, 
     v                nelgrp,u,v,iel,user(2*npoint+1))
c           *** convert to cylindrical
            vth = (costh*u - sinth*v)/r
            vr  = sinth*u + costh*v
            rk4th = tstep*vth
            rk4r  = tstep*vr

c           *** Combine all the estimates in the 4th order Runge Kutta step
            thetal  = theta_init + rk1th/6d0 + rk2th/3d0 +
     v                            rk3th/3d0 + rk4th/6d0
            r   =  r_init + rk1r/6d0 + rk2r/3d0 +
     v                      rk3r/3d0 + rk4r/6d0
            r = min(r,radius_max)
            r = max(r,radius_min)
c           *** final coordinates in Cartesian
            xm = r*sin(thetal)
            ym = r*cos(thetal)

         endif

         coornewm(ip1) = xm
         coornewm(ip2) = ym

      enddo

c     *** markers have been updated, information in /petrac/ ielemmark is out of date
      xi_eta_stored = 0
c     write(6,*) 'cpu(mark4cyl): ',second()-t00

      if (itracoption.eq.2) then
c     *** make sure markers at boundaries stay on boundary
         if (quart) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = 2*ip+1
               coornewm(ip1) = 0
c              *** y coordinate of last tracer should be zero
               ip2 = 2*(ip+nmark)
               coornewm(ip2) = 0
               ip = ip+nmark
            enddo
         else if (half) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = 2*ip+1
               coornewm(ip1) = 0
c              *** x coordinate of last tracer should be zero
               ip2 = 2*(ip+nmark)-1
               coornewm(ip2) = 0
               ip = ip+nmark
            enddo
         else if (eighth) then
            ip=0
            do ichain=1,nochain
               nmark = imark(ichain)
c              *** x coordinate of first tracer should be zero
               ip1 = 2*ip+1
               coornewm(ip1) = 0
c              *** x and y coordinates of last tracer should be equal
               ip2 = 2*(ip+nmark)-1
               coornewm(ip2) = coornewm(ip2+1)
               ip = ip+nmark
            enddo
         endif
      endif

      return
      end

c *************************************************************
c *   MARK4cart
c *
c *   Transport markers using 4th order Runge Kutta method.
c *
c *   kmesh          i    Mesh definition for velocity
c *   kprob          i    Problem definition for velocity
c *   isolnew        i    Predicted velocity at t=t0+dt
c *   isolav         i    Mid-point velocity at t=t0+0.5*dt
c *   user           s    Scratch area to temporarily store velocity info
c *   velmark        i    Old velocity of markers at t=t0
c *   coormark       i    Old position of markers at t=t0
c *   coornewm       io   At input: predicted position of markers
c *                                 at t=t1=t0+dt
c *                       At output: corrected position of markers at t=t1
c *************************************************************
      subroutine mark4cart(kmesh,kprob,isolnew,isolav,user)
      implicit none
      integer kmesh(*),kprob(*),isolnew(*),isolav(*)
      real*8 user(*)

      include 'SPcommon/ctimen'
      include 'pexcyc.inc'
      include 'petrac.inc'
      include 'c1mark.inc'
      include 'tracer.inc'

      integer nmark,i,npoint,ikelmc,ikelmi,iel,ip,j,ip1,ip2
      real*8 rk1x,rk2x,rk3x,rk4x,rk1y,rk2y,rk3y,rk4y
      real*8 xm,ym,factor,u,v,xn(6),yn(6)
      real*8 shapef(6),un(6),vn(6)
      integer nodno(6),iniget,inidgt,nelem,ntot

      call pefilxy(2,kmesh,kprob,isolnew)
c     *** Prepare for interpolation of velocity (see tdetvel.f)
      call ini050(kmesh(23),'mark4cart: coordinates')
      call ini050(kmesh(17),'mark4cart: nodalpoints')
      npoint = kmesh(8)
      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      nelem = kmesh(9)

c     *** Both the averaged velocity at t=t+0.5*dt and the
c     *** predicted velocity at t=t+dt are needed
      factor=1d0
      call pecopy(2,isolav,user,kmesh,kprob,10,factor)
      call pecopy(3,isolav,user,kmesh,kprob,10+npoint,factor)
      call pecopy(2,isolnew,user,kmesh,kprob,10+2*npoint,factor)
      call pecopy(3,isolnew,user,kmesh,kprob,10+3*npoint,factor)

      if (itracoption.eq.2) then
         nmark=0
         do ichain=1,nochain
            nmark = nmark + imark(ichain)
         enddo
         ntot = nmark
      else
         ntot = 0
         do idist=1,ndist
            ntot = ntot + ntrac(idist)
         enddo
      endif

      do j=1,ntot
         ip1 = 2*j-1
         ip2 = 2*j

         rk1x = tstep*velmark(ip1)
         rk1y = tstep*velmark(ip2)
c        *** Calculate estimate for mid-point
         xm = coormark(ip1) + 0.5*rk1x
         ym = coormark(ip2) + 0.5*rk1y

c        *** Find average velocity at mid-point at t=t+0.5*dt
         call checkbounds_cart(xm,ym)
         call velintmark(xm,ym,ikelmc,ikelmi,npoint,u,v,iel,nelem,user) 
         rk2x = tstep*u
         rk2y = tstep*v
         xm = coormark(ip1) + 0.5*rk2x
         ym = coormark(ip2) + 0.5*rk2y

c        *** find average velocity at corrected mid-point at t=t+0.5*dt
         call checkbounds_cart(xm,ym)
         call velintmark(xm,ym,ikelmc,ikelmi,npoint,u,v,iel,nelem,user)

c        *** Find position of marker at t=t+dt
         rk3x = tstep*u
         rk3y = tstep*v
         xm = coormark(ip1) + rk3x
         ym = coormark(ip2) + rk3y
c        *** Find velocity in new marker position at t=t+dt
         call checkbounds_cart(xm,ym)
         call velintmark(xm,ym,ikelmc,ikelmi,npoint,
     v             u,v,iel,nelem,user(2*npoint+1))
         rk4x = tstep*u
         rk4y = tstep*v

c        *** Combine all the estimates in the 4th order Runge Kutta step
         coornewm(ip1) = coormark(ip1)+ rk1x/6d0 + rk2x/3d0 +
     v                         rk3x/3d0 + rk4x/6d0
         coornewm(ip2) = coormark(ip2)+ rk1y/6d0 + rk2y/3d0 +
     v                         rk3y/3d0 + rk4y/6d0
c        *** Make sure the markers stay in the computational domain
         call checkbounds_cart(coornewm(ip1),coornewm(ip2))
      enddo

      if (itracoption.eq.2) then
c        *** make sure markers at boundaries stay at boundary
         ip=0
         do ichain=1,nochain
            nmark=imark(ichain)
            ip1 = ip+1
            coornewm(2*ip1-1) = xcmax
            ip2 = ip+nmark
            coornewm(2*ip2-1) = xcmin
            ip=ip+nmark
         enddo
      endif


      return
      end

      subroutine checkbounds_cart(xm,ym)
      implicit none
      real*8 xm,ym

      include 'pexcyc.inc'
 
      xm = max(xm,xcmin)
      xm = min(xm,xcmax)
      ym = max(ym,ycmin)
      ym = min(ym,ycmax)
 
      return
      end
