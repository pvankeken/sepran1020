      subroutine elm903 ( coor, elemmt, elemvc, elemms, iuser, user,
     +                    index1, index3, index4, vecold, islold,
     +                    vecloc, wrk1, symm )
! ======================================================================
!
!        programmer    Guus Segal
!        version  2.9  date 14-11-2007 Extension with diagonal velocity m matrix
!        version  2.8  date 19-04-2007 Upwind for convection
!        version  2.7  date 03-01-2007 New call to el0903
!        version  2.6  date 04-08-2006 Debug statements
!        version  2.5  date 13-02-2006 New call to elvisc
!        version  2.4  date 30-03-2005 Correction mass matrix
!        version  2.3  date 28-02-2005 Fortran 90
!        version  2.2  date 10-11-2004 Jump in case of error
!        version  2.1  date 19-05-2003 New call to elm800basefn
!        version  2.0  date 07-05-2003 Extra parameter symm
!        version  1.8  date 03-03-2003 New call to el2005
!        version  1.7  date 16-01-2003 One call for basis functions
!        version  1.6  date 08-11-2002 New call to el0903
!
!   copyright (c) 1996-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element matrix and vector for the  navier-stokes equations
!     incompressible flow, using the integrated method (Taylor-Hood)
!     Two and three-dimensional elements
!     ELM903 is a help subroutine for subroutine BUILD (TO0050)
!     it is called through the intermediate subroutine elmns2
!     So:
!     BUILD
!     TO0050
!       -  Loop over elements
!          -  ELMMEC
!             - ELM903
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  3 node triangle
!         4 node triangle
!         6 node triangle
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/cellog'
      include 'SPcommon/cinforel'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemmt(*), elemvc(*), coor(*), user(*),
     +                 elemms(*), vecold(*), vecloc(*), wrk1(*)
      integer iuser(*), index1(*), index3(*), index4(*), islold(5,*)
      logical symm

!     coor     i     array of length ndim x npoint containing the co-ordinates
!                    of the nodal points with respect to the global numbering
!                    x  = coor (1,i);  y  = coor (2,i);  z  = coor (3,i);
!                     i                 i                 i
!     elemms   o     Element mass matrix to be computed (Diagonal matrix)
!     elemmt   o     Element matrix to be computed
!                    At this moment the element is assumed to be zero
!     elemvc   o     Element vector to be computed
!     index1   i     Array of length inpelm containing the point numbers
!                    of the nodal points in the element
!     index3   i     Two-dimensional integer array of length NUMOLD x NINDEX
!                    containing the positions of the "old" solutions in array
!                    VECOLD with respect to the present element
!                    For example VECOLD(INDEX3(i,j)) contains the j th
!                    unknown with respect to the i th old solution vector.
!                    The number i refers to the i th vector corresponding to
!                    IVCOLD in the call of SYSTM2 or DERIVA
!     index4   i     Two-dimensional integer array of length NUMOLD x INPELM
!                    containing the number of unknowns per point accumulated
!                    in array VECOLD with respect to the present element.
!                    For example INDEX4(i,1) contains the number of unknowns
!                    in the first point with respect to the i th vector stored
!                    in VECOLD.
!                    The number of unknowns in the j th point with respect to
!                    i th vector in VECOLD is equal to
!                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold   i     User input array in which the user puts information
!                    of all preceding solutions
!                    Integer array of length 5 x numold
!     iuser    i     Integer user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     symm    i/o    Indicates whether the matrix symmetric (true) or not
!     user     i     Real user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     vecloc         Work array in which all old solution vectors for the
!                    integration points are stored
!     vecold   i     In this array all preceding solutions are stored, i.e.
!                    all solutions that have been computed before and
!                    have been carried to system or deriva by the parameter
!                    islold in the parameter list of these main subroutines
!     wrk1           work array of length 13473
!                    contents:
!                    Starting address    length   Name:
!                    1                   32*27    ARRAY
!                    ipugs               3*27     UGAUSS
!                    ipdudx              9*27     DUDX
!                    ipdphidx            27*27*3  DPHIDX
!                    ipphixi             27*27*3  PHIKSI for elp633
!                    ipetef              27       ETHA_EFFECTIVE
!                    ipseci              27       SECOND_INVARIANT
!                    ipdetd              27       DERIVATIVE of
!                                                 SECOND_INVARIANT
!                    ipsp                81*4     SP_matrix (Gradient P)
!                    ipdiv               81*4     DIV_matrix (Divergence matrix)
!                    ippp                8*8      Pressure matrix
!                    ipwork              27*31    Work space
!                    ipelem              81*81    Copy of part of element matrix
!                    ipelvc              81*3     Copy of part of element vector
!                    ippsi               8*27     Linear basis function psi
!                    Total length: 14212
!                    The subarrays contain the following contents:
!                    ARRAY:
!                    array in which the values of the coefficients
!                    in the integration points are stored in the sequence:
!                         2D                  3D
!                    1:   rho              rho
!                    2:   omega            omega
!                    3:   f1               f1
!                    4:   f2               f2
!                    5:                    f3
!                    6:   eta/E            eta/E
!                    7:   rho in nodal points (mcont=1 only)
!                    Each coefficient requires exact m (is number of
!                    integration points) positions
!                    DPHIDX:
!                    Array of length inpelm x m x ndim containing the
!                    derivatives of the basis functions in the sequence:
!                    d phi / dx = dphidx (i,k,1);  d phi / dy = dphidx (i,k,2);
!                         i                             i
!                    d phi / dz = dphidx (i,k,3)
!                         i
!                      in node k
!                    WORK
!                    General work space
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer isub, isup, jelp, jtime, ipugs, ippp, iflipflop,
     +        ipwork, ipphixi, mconv, modelv, ipdudx, ipsp, ipetef,
     +        ipseci, ipdetd, ipelem, ipelvc, mcont, ipdiv, iptran,
     +        iptrnp, ipelcp, iprho, nveloc, ipmass, iprhoc,
     +        imesh, ipunew, ipdudxnw, ipunewgs, mdiv, mcoefconv,
     +        modelrhsd, ioldm, ipphi, signdiv, ipkappa, ipfdiv,
     +        ipcoeffgrad, ipcoeffdiv, ipetaspecial, ipetha, ipomega,
     +        ipwork1, ipgoertler, ipcconv, ippsiupw,
     +        ipx, ipw, ipxgauss, ipuold, ipuoldm, ipdphidx,
     +        ipqmat, ippsi, ippsix, ippsiksi, iseqvel(81), iseqpres(8),
     +        irow(27), icol(27), i, j, k, ipviscrho
      double precision cn, clamb, thetdt, usolcheck(35),
     +                 rhsdiv(8), tang(1), rhsdcheck(35)
      logical debug, second, check, extradebug
      character (len=10) text
      save isub, isup, jelp, jtime, ipwork, ipphixi, ipugs,
     +     mconv, modelv, cn, clamb, ipdudx, ipsp, ipetef,
     +     ipseci, ipdetd, thetdt, ipelem, ipelvc, mcont, ipdiv, iptrnp,
     +     ipelcp, iprho, nveloc, iptran, ipmass, iprhoc, imesh,
     +     ipunew, ipdudxnw, ipunewgs, mdiv, mcoefconv, ippp, modelrhsd,
     +     ioldm, ipphi, ippsiupw, ipdphidx, ipviscrho,
     +     ipx, ipw, ipxgauss, ipuold, ipuoldm, iseqvel, iseqpres,
     +     ipqmat, ippsi, ippsix, ippsiksi

!     check          Indicates if checks must be performed or not
!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     extradebug     If true extra debugging statements are carried out
!     i              Counting variable
!     icol           Help array to store column numbers
!     iflipflop      Work array to prevent flip-flop behaviour in non-
!                    linear upwind
!                    The status of the element with respect to upwind is
!                    stored in this array
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     ipcconv        Starting address of array cconv
!     ipcoeffdiv     Starting address of array coeffdiv
!     ipcoeffgrad    Starting address of array coeffgrad
!     ipdetd         Starting address in array work of detdsc
!     ipdiv          Starting address in array work of div
!     ipdphidx       Starting address in array work of dphidx
!     ipdudx         Starting address in array work of dudx
!     ipdudxnw       Starting address of du0dx
!     ipelcp         Starting address of copy of element
!                    matrix
!     ipelem         Starting address in array work of copy of element matrix
!     ipelvc         Starting address in array work of copy of element vector
!     ipetaspecial   Starting address of array etaspecial
!     ipetef         Starting address in array work of etheff
!     ipetha         Starting address of array etha
!     ipfdiv         Starting address in array work of fdiv
!     ipgoertler     Starting address of array goertler
!     ipkappa        Starting address in array work of kappa
!     ipmass         Starting address in array work of mass matrix before
!                    transformation
!     ipomega        Starting address of array omega
!     ipphi          Starting address of array phi
!     ipphixi        Starting address in array work of phiksi
!     ippp           Starting address in array work of pressure matrix
!     ippsi          Starting address in array work of psi
!     ippsiksi       Starting address in array work of psiksi
!     ippsiupw       Starting address of psiupw
!     ippsix         Pointer to dpsi/dx
!     ipqmat         Starting address of array qmat
!     iprho          Starting address in array work of rho
!     iprhoc         Starting address in array work of rho multiplied by w
!     ipseci         Starting address in array work of secinv
!     ipsp           Starting address in array work of sp
!     iptran         Starting address of trans with respect
!                    to div
!     iptrnp         Starting address of trans with respect
!                    to grad
!     ipugs          Starting address in array work of ugauss
!     ipunew         Starting address of unew (u0)
!     ipunewgs       Starting address of u0 in Gauss points
!     ipuold         Starting address of solution array
!     ipuoldm        Starting address of solution array in prior time step
!     ipviscrho      Starting address of array viscosity/rho
!     ipw            Starting address of weight vector w
!     ipwork         Starting address in array work of work array
!     ipwork1        Starting adress of array WORK1
!     ipx            Starting adress of array x
!     ipxgauss       Starting adress of array xgauss
!     irow           Help array to store row numbers
!     iseqpres       Contains the sequence numbers of the pressure in the
!                    element vector
!     iseqvel        Array containing the positions of the velocities in
!                    the element vector in the sequence required by el2005
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     j              Counting variable
!     jelp           Indication of the type of basis function subroutine must
!                    be called by ELM100.
!     jtime          Integer parameter indicating if the mass matrix for the
!                    time-dependent equation must be computed (>0) or not ( = 0)
!                    Possible values:
!                      0:  no mass matrix
!                      1:  diagonal mass matrix with constant coefficient
!                      2:  diagonal mass matrix with variable coefficient
!                     11:  full mass matrix with constant coefficient
!                     12:  full mass matrix with variable coefficient
!                    101:  refers to the special possibility of the theta
!                          method directly applied
!                    102:  refers to the special possibility of the theta
!                          method directly applied with variable rho
!     k              Counting variable
!     mcoefconv      Defines how the convective term is treated:
!                    0: standard case
!                    1: special case: the convective term is defined as
!                          c_conv_1 u du/dx + c_conv_2 v du/dy
!                          c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!                       Not applicable with penalty function method
!                    4: the continuity equation contains a pressure term.
!     mconv          Defines the treatment of the convective terms
!                    Possible values:
!                    0: convective terms are skipped (Stokes flow)
!                    1: convective terms are linearized by Picard iteration
!                    2: convective terms are linearized by Newton iteration
!                    3: convective terms are linearized by the incorrect Picard
!                       iteration
!                    4: convective terms are linearized by successive
!                       substitution
!     mdiv           Defines if divergence right-hand side is used (1) or
!                    not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     nveloc         Number of velocity parameters
!     rhsdcheck      Help parameter to store a right-hand side vector for
!                    checking purposes
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     second         Indicates if second derivative term is important in
!                    case of upwinding
!     signdiv        Multiplication sign of "continuity equations"
!                    if -1 the stokes matrix is symmetric
!                    if 1 all eigenvalues are in the positive half plane
!     tang           In this array the tangential vector is stored
!                    tang(j,i) contains the j th component of the
!                    tangential vector in the i-th integration point
!     text           String variable containing some text
!     thetdt         1/(theta dt) for the case of the theta method
!                    immediately applied
!     usolcheck      Help parameter to store a solution vector for
!                    checking purposes
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0903         Initialize parameters for subroutines ELD/M/I903
!     EL1004         Compute gradient and divergence matrix (Taylor-Hood)
!     EL1008         Fill complete matrix for Navier-Stokes (Taylor-Hood)
!     ELFILLSOL01    Fill solution vector for an element (testing purposes)
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM800UPWBASE  Compute the modified basis functions psi due to upwinding
!     ELM900ADDRESS  Compute starting addresses of subarrays in array
!     ELM900ADDRESS1 Compute starting addresses of subarrays in array
!     ELM900COEFFS   Fill variable coefficients in coefficients array
!                    Fill velocity from prior iteration if necessary
!     ELM900MASSMAT  Fill element mass matrix for the incompressible
!                    Navier-Stokes equations
!     ELM900MATRIX   Fill element matrix for the incompressible Navier-Stokes
!                    equations
!     ELM900RHSD     Compute right-hand side vector for Navier-Stokes element
!     ELM903UPW      Update stiffness matrix with upwind term due to viscosity
!                    Update gradient matrix with upwind term
!     ELMASSDIAG     Copy components-wise defined diagonal element mass matrix
!                    into diagonal element mass matrix
!     ELMATMULT      Matrix vector multiplication for a full element matrix
!     ELVISC         Compute effective viscosity depending on the viscosity
!                    model
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRSUB         Error messages
!     PRININ         print 1d integer array
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
!     PRINRL1_IND    Print parts of a full element matrix by indicating rows and
!                    columns
!     PRINRL2        Print a real two-dimensional array
!     TODIVVEC       Divide 2 vectors components wise, i.e. y_i = x1_i / x2_i
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  information about the time-integration
!         Possible values:
!         0: if imas in BUILD=0 then time-independent flow
!            else the mass matrix is built according to imas
!         1: The theta method is applied directly in the following way:
!            the mass matrix divided by theta delta is added to the stiffness
!            matrix
!            the mass matrix divided by theta delta and multiplied by u(n) is
!            added to the right-hand-side vector
!     2:  type of viscosity model used (modelv) as well as given stress
!         tensor as right-hand side accordinag to
!         modelv + 1000*model_rhsd
!         Possible values for modelv
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!         Possible values for model_rhsd:
!         0:    if there is a given stress tensor the reference to this
!               tensor is stored in position 19
!         1:    It is supposed that there are 6 given stress tensor components
!               stored in positions 19-24
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!         2: Polar co-ordinates
!     5:  Information about the convective terms (mconv)
!         and continuity equation (mcont) according to mconv+10*mcont
!         Possible values for mconv:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!         Possible values for mcont:
!         0: Standard situation, the continuity equation is div u = 0
!         1: A special continuity equation div ( rho u ) = 0, which rho
!            variable is solved. This is not the standard continuity equation
!            for incompressible flow. In fact we have a time-independent
!            compressible flow description.
!            This possiblility can not be applied with the penalty function
!            approach
!         4: the continuity equation contains a pressure term.
!     6-20: Information about the equations and solution method
!     6:  -
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-16: not yet used
!    17:  kappa  Parameter for the pressure term in the continuity equation
!                (MCONT=4)
!    18:  fdiv   Right-hand side for continuity equation
!    19:  stress If model_rhsd = 0 then
!                Given stress tensor used in the right-hand side vector
!                At this moment it is supposed that the stress is defined
!                per element and that in each element all components are
!                constant
!                If model_rhsd = 1 then
!                The six components of the stress tensors are stored in
!                positions 19 to 24 in the order: xx, yy, zz, xy, yz, zx
!                This option can not be used in cooporation with the
!                dimensionless coefficients stored in positions 20-41
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2873   upwind not yet implemented
! **********************************************************************
!
!                       PSEUDO CODE
!
!    if ( first call ) then
!       initialize standard parameters
!    compute basis functions
!    fill old solutions if necessary
!    if ( matrix ) then
!       fill the viscosity in the corresponding array
!       Compute the viscous terms
!       if ( mconv>0 ) then
!          compute convective terms
!       Compute the divergence matrix
!       Compute the pressure matrix
!       Compute the penalty contribution
!    if ( vector ) then
!       Compute contribution of body forces
!       if ( mconv=2 ) then
!          compute contribution of convective terms
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm903' )
      debug = .false.
      extradebug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elm903'
         write(irefwr,1) 'ielem', ielem
  1      format ( a, 1x, (10i6) )

      end if
      if ( ierror/=0 ) go to 1000

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         icheld = 0
         call el0903 ( iuser, user, jelp, jtime, cn, clamb,
     +                 mconv, mcont, modelv, isub, isup, wrk1,
     +                 thetdt, nveloc, iseqvel, iseqpres, mdiv,
     +                 modelrhsd, index4 )
         if ( ierror/=0 ) go to 1000
         if ( debug ) then

!        --- debug information

            call prinin ( iseqvel, ndim*n, 'iseqvel' )
            call prinin ( iseqpres, npsi, 'iseqpres' )
            write(irefwr,1) 'metupw', metupw

         end if

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

!        --- Compute starting addresses in array wrk1

         call elm900address ( ipphi, ipugs, mcont, iprho, ipdudx,
     +                        ipphixi, ipetef, ipseci, ipdetd, ipsp,
     +                        ipdiv, ippp, ipwork, ipelem, ipelcp,
     +                        wrk1, ipdudxnw, ipelvc,
     +                        ipmass, iprhoc, iptran, iptrnp, ipunew,
     +                        ipunewgs, ippsiupw, ipdphidx,
     +                        ipx, ipw, ipxgauss, ipuold, ipuoldm,
     +                        ipqmat, ippsi, ippsix, ippsiksi,
     +                        ipviscrho )
         if ( debug ) then
            write(irefwr,1) 'ipsp, ipdiv, ippp', ipsp, ipdiv, ippp
            write(irefwr,1) 'ippsi, ippsix, ippsiksi',
     +                       ippsi, ippsix, ippsiksi
            write(irefwr,1) 'ippsiupw, ipviscrho', ippsiupw, ipviscrho
         end if  ! ( debug )

      end if  ! ( ifirst==0 )

      do i = 1, 8
         rhsdiv(i) = 0d0
      end do

      signdiv = -1

!     --- Compute some starting addresses in wrk1

      call elm900address1 ( ipkappa, ipfdiv, ipcoeffgrad,
     +                      ipcoeffdiv, ipetaspecial, ipwork,
     +                      ipwork1, ipomega, ipetha,
     +                      ipgoertler, ipcconv )

!     --- compute basis functions and weights for numerical integration

      if ( jelp<9 .or. jelp>10 .and. jelp<17 ) jdiag = jdiag+10

      call elm800basefn ( coor, wrk1(ipx), wrk1(ipw), wrk1(ipdphidx),
     +                    wrk1(ipxgauss), wrk1(ipphi), index1,
     +                    wrk1(ipqmat), tang, jelp, wrk1(ipphixi),
     +                    wrk1(ippsi), wrk1(ippsix), wrk1(ippsiksi) )
      if ( extradebug ) then
         call prinrl ( wrk1(ipw), m, 'w' )
         call prinrl1 ( wrk1(ipphi), m, n, 'phi' )
         call prinrl1 ( wrk1(ipdphidx), m, n, 'dphi/dx' )
         call prinrl1 ( wrk1(ipdphidx+m*n), m, n, 'dphi/dy' )
         call prinrl1 ( wrk1(ippsi), m, npsi, 'psi' )
         call prinrl1 ( wrk1(ippsix), m, npsi, 'dpsi/dx' )
         call prinrl1 ( wrk1(ippsix+m*npsi), m, npsi, 'dpsi/dy' )
      end if  ! ( debug )

      if ( jelp<9 .or. jelp>10 .and. jelp<17 ) jdiag = jdiag-10
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      call elm900coeffs ( vecold, wrk1(ipuoldm), wrk1, wrk1(ipdphidx),
     +                    index3, iseqvel, ioldm, index4, ipphi, ipugs,
     +                    ipdudx, ipunew, ipdudxnw, ipunewgs,
     +                    wrk1(ipuold), mcont, ipwork, isub, isup,
     +                    iprho, imesh, iuser, user, vecloc, wrk1(ipx),
     +                    wrk1(ipxgauss) )

!     --- Compute the divergence and gradient matrix

      call el1004 ( wrk1(ipsp), wrk1(ipdiv), wrk1(ippsi),
     +              wrk1(ipdphidx), wrk1(ipphi), wrk1(ipw),
     +              wrk1(ipxgauss), mcont, wrk1(iprho),
     +              wrk1(ippp), wrk1(ipkappa), rhsdiv,
     +              wrk1(ipfdiv), mdiv )

      if ( extradebug ) then

!     --- extra debug

         call prinrl2 ( wrk1(ipsp), ndimlc*n, npsi, 'Grad-matrix' )
         call prinrl1 ( wrk1(ipdiv), npsi, ndimlc*n, 'Div-matrix' )

      end if  ! ( extradebug )

      if ( modelv/=5 ) then

!     --- Compute effective viscosity in case of modelv # 5

         call elvisc ( modelv, wrk1(ipetha), wrk1(ipetef), cn, clamb,
     +                 wrk1(ipxgauss), wrk1(ipugs), wrk1(ipdudx),
     +                 wrk1(ipseci), wrk1(ipdetd), wrk1(ipwork), vecloc,
     +                 maxunk, numold )
         if ( extradebug ) write(irefwr,*) 'after elvisc'

      end if  ! ( modelv/=5 )

      if ( metupw>0 ) then

!     --- Upwind method, compute upwind basis functions
!         First compute viscosity/rho, which is the parameter eps

         call todivvec ( wrk1(ipviscrho), wrk1(ipetef), wrk1(iprho), m )
         second = .true.
         call elm800upwbase ( metupw, jelp, wrk1(ipviscrho),
     +                        wrk1(ipugs), wrk1(ipphi), wrk1(ippsiupw),
     +                        wrk1(ipx), second, wrk1(ipdphidx),
     +                        wrk1(ipdudx), iflipflop, wrk1(ippsix) )
         if ( extradebug ) then

!        --- extra debug

            call prinrl1 ( wrk1(ippsiupw), m, n, 'psi_upw' )

         end if  ! ( extradebug )

      end if  ! ( metupw>0 )

!     --- Fill parts of matrix if necessary

      if ( matrix ) then

!     --- matrix = true    compute element matrix
!         fill the viscosity in array etaeff

         call elm900matrix ( wrk1(ipelem), wrk1(ipdphidx), wrk1(ipphi),
     +                       wrk1(ipxgauss), wrk1(ipetaspecial),
     +                       wrk1(ipw), wrk1(ipwork), wrk1(ipwork1),
     +                       wrk1(ipetef), mcont, wrk1(iprho),
     +                       wrk1(ipomega), mconv, imesh,
     +                       wrk1(ipcconv), wrk1(ipugs), wrk1(ipunewgs),
     +                       wrk1(ipunew), modelv, symm, mcoefconv,
     +                       nveloc, wrk1(ipdudxnw), wrk1(ipdudx),
     +                       wrk1(ipgoertler), wrk1(ipkappa),
     +                       wrk1(ippsiupw) )
         if ( extradebug ) then

!        --- extra debug

            call prinrl1 ( wrk1(ipelem), ndimlc*n, ndimlc*n,
     +                     'stiffness matrix' )

         end if  ! ( extradebug )

      end if

      if ( vector .and. notvec==0 ) then

!     --- vector = true, compute element vector

         call elm900rhsd ( wrk1(ipelvc), wrk1, wrk1(iprhoc),
     +                     wrk1(ipphi), wrk1(2*m+1), wrk1(ipwork),
     +                     wrk1(ipwork+n), wrk1(9*m+1), wrk1(ipcconv),
     +                     wrk1(ipugs), wrk1(ipdudx), wrk1(ipgoertler),
     +                     wrk1(ipkappa), wrk1(ipdetd), wrk1(ipw),
     +                     wrk1(ipdphidx), cn, mconv, jtime, mcoefconv,
     +                     mcont, modelv, modelrhsd, wrk1(ipseci),
     +                     wrk1(ipxgauss), wrk1(ippsiupw) )
         if ( extradebug ) then

!        --- extra debug

            call prinrl ( wrk1(ipelvc), ndimlc*n, 'rhs' )

         end if  ! ( extradebug )

      end if

      if ( metupw>0 ) then

!     --- Upwind method, extend gradient matrix and stiffness matrix
!         with upwind terms

         call elm903upw ( wrk1(ipsp), wrk1(ipphi), wrk1(ippsiupw),
     +                    wrk1(ippsix), wrk1(ipw) )

         if ( extradebug ) then

!        --- extra debug

            call prinrl2 ( wrk1(ipsp), ndimlc*n, npsi, 'Grad-matrix/2' )

         end if  ! ( extradebug )

      end if  ! ( metupw>0 )

      if ( jtime>0 ) then

!     --- jtime>0, i.e. the mass matrix must be computed
!         First compute the mass matrix for one velocity component
!         The non-diagonal mass matrix for this component is stored in
!         work positions ipwork - ipwork+n*n-1
!         The diagonal matrix in work pos. ipwork+n*n - ipwork+n*n+n-1

         if ( metupw>0 ) then

!        --- Upwind not yet implemented

            call errchr ( 'Mass matrix', 1 )
            call errsub ( 2873, 0, 0, 1 )

         end if  ! ( metup==0 )

         call elm900massmat ( elemms, wrk1, wrk1(ipwork), wrk1(ipw),
     +                        wrk1(ipphi), wrk1(ipmass),
     +                        wrk1(ipelem), wrk1(ipuoldm), wrk1(ipelvc),
     +                        thetdt, wrk1(ipuold), jtime, mconv,
     +                        wrk1(ipdphidx), wrk1(ipdudx),
     +                        wrk1(ipgoertler), wrk1(ipkappa), index1,
     +                        index3, index4, mcont, wrk1(ipugs),
     +                        vecold, wrk1(ipxgauss), ioldm, iseqvel )
         if ( extradebug ) write(irefwr,*) 'after elm900massmat'

      end if

!     --- Construct final matrix and vector from various parts
!         Eliminate centroid if necessary

      call el1008 ( elemmt, wrk1(ipelem), wrk1(ipsp), wrk1(ipdiv),
     +              elemvc, wrk1(ipelvc), iseqvel, iseqpres,
     +              wrk1(ippp), mcont, rhsdiv, signdiv )
      if ( extradebug ) write(irefwr,*) 'after el1008'

!     --- Store diagonal of element mass matrix into elemmt
!         from position icount^2+1

      call elmassdiag ( elemmt(icount**2+1), wrk1(ipw+m), inpelm,
     +                  iseqvel )

1000  call erclos ( 'elm903' )

      if ( debug ) then

!     --- debug = true

         write(irefwr,*) 'element ', ielem, '  itype ',itype
         call prinrl(elemvc,icount,'elemvc')
         call prinrl1(elemmt,icount,icount,'elemmt')
         if ( notmas==0 ) then
            if ( imas==1 ) then
               call prinrl(elemms,icount,'elemms')
            else
               call prinrl1(elemms,icount,icount,'elemms')
            end if  ! ( imas==1 )
         end if  ! ( notmas==0 )
         check = .false.
         if ( check ) then

!        --- Check is true, Print subarrays

            do i = 1, ndim
               do k = 1, n
                  irow(k) = iseqvel(i+ndim*(k-1))
               end do
               do j = 1, ndim
                  do k = 1, n
                     icol(k) = iseqvel(j+ndim*(k-1))
                  end do
                  write(text,1010) i, j
1010              format('S_',2i1)
                  call prinrl1_ind ( elemmt, icount, icount, irow, icol,
     +                               n, n, text )
               end do
               write(text,1020) i
1020           format('L^t_',i1)
               call prinrl1_ind ( elemmt, icount, icount, irow,
     +                            iseqpres, n, npsi, text )
               write(text,1030) i
1030           format('L_',i1)
               call prinrl1_ind ( elemmt, icount, icount, iseqpres,
     +                            irow, npsi, n, text )

            end do
            call prinrl1_ind ( elemmt, icount, icount, iseqpres,
     +                         iseqpres, npsi, npsi, 'Zero-mat' )

         end if
         write(irefwr,*) 'End elm903'

      end if
      check = .false.
      if ( check ) then

!     --- Check is true, extra debugging

         call elfillsol01 ( 1, usolcheck, iseqvel, iseqpres, ndim,
     +                      n, npsi )
         call elmatmult ( elemmt, usolcheck, rhsdcheck, icount )
         call prinrl ( rhsdcheck, icount, 'rhsdcheck' )

      end if

      end
