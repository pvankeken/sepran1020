c *************************************************************
c *   TEMPTOGRID
c *
c *   Determine temperature in the regular grid nodal points
c *   iel(NXEL,NYEL) in /pielgrid/ contains element numbers
c *   from kmesh1
c *
c *   See velintmarkc() for a description of the interpolation logic
c *
c *   PvK 990908
c *************************************************************
      subroutine temptogrid(kmesh1,kprob1,kmesh2,kprob2,isol2,user, 
     v        temp)
      implicit none
      integer nth,nr
      integer kmesh1(*),kprob1(*),kmesh2(*),kprob2(*),isol2(*)
      real*8 user(*),temp(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      include 'ccc.inc'
      include 'bound.inc'
      include 'averages.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)

      integer ikelmc,ikelmi,npoint,i,j,k,iel,ifound
      real*8 xm,ym,xn(6),yn(6),shapef(6),tn(6),done,rl(3)
      integer nodno(6),imissed,nodlin(6),isub,ikelmo,nelem,nelgrp
      real*8 r,th,temperature,dth,dr,un(6),vn(6)
      integer ith,ir,iniget,inidgt,ip
      logical guess_first
      real*8 xi,eta
      integer ifirst
      save ifirst
      data ifirst/0/
 

      imissed = 0
      ifound = 0
      call ini050(kmesh1(23),'temptogrid: coordinates')
      call ini050(kmesh1(17),'temptogrid: nodal points')
      call ini050(kmesh1(29),'temptogrid: kmesho')
      npoint = kmesh1(8)
      nelem = kmesh1(9)
      if (periodic) nelem = kmesh1(kmesh1(21))
      nelgrp = kmesh1(5)
      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
      ikelmo = iniget(kmesh1(29))

      done = 1d0
      call pecopy(0,isol2,user,kmesh2,kprob2,10,done)

      nth = nth_output
      nr  = nr_output
      dth = dth_output
      dr  = dr_output

c     *** frac=1 corresponds to 360 degrees = 2 pi
      dth = 2*frac*pi/nth
      dr = (r2-r1)/nr
      if (ifirst.eq.0) then
        write(6,'(''TEMPTOGRID: dth, dr  = '',2f12.5)') dth,dr
        write(6,'(''            th range = '',2f12.5)') 0,nth*dth
        write(6,'(''             r range = '',2f12.5)') r1,r1+dr*nr
        ifirst=1
      endif

      guess_first=.false.
      do ir=1,nr
        do ith=1,nth
          r = r1 + (ir-0.5)*dr
          th = (ith-0.5)*dth
          xm = r*sin(th)
          ym = r*cos(th)
c ************************************************************
c *       Find the element in which (xm,ym) lies
c *       Look it up in the table 
c ************************************************************
          call pedeteltrac(2,xm,ym,ikelmc,ikelmi,ikelmo,nodno,nodlin,
     v       rl,xn,yn,un,vn,user,iel,npoint,nelem,imissed,ifound,
     v       nelgrp,shapef,xi,eta,guess_first,'temptogrid') 

c         *** Determine interpolation functions shapef for quadratic
c         *** triangle
c         call detshape(xn,yn,xm,ym,shapef)
          temperature=0
          do k=1,6
             temperature  = temperature + shapef(k)*un(k)
          enddo
c         write(6,*) 'xm,ym: ',xm,ym,un(1),shapef(1),temperature
          ip = (ith-1)*nth+ir
          temp(ip) = temperature
        enddo
      enddo

c     write(6,*) 'Missed about ',imissed,' points'
      if (imissed.gt.0.01*nth*nr) then
         write(6,*) 'PWARN(temptogrid): interpolation of temperature'
         write(6,*) '  is inefficient for > 1% of the grid points'
         write(6,*) '  Time to rethink the mesh gridding strategy?'
         write(6,*) '  imissed = ',imissed
      endif
      return
      end
  
