      subroutine inifil ( ifile )
c ======================================================================
c
c        programmer   Guus Segal
c        version 6.7    date   20-04-1999 Inlcude commons
c        version 6.6    date   10-02-1994 new comments
c        version 6.5    date   22-09-1993 Extension with readonly for VAX
c        version 6.4    date   25-12-1992 Make files 1 and 4 read-only
c        version 6.3    date   27-11-1992 Extension with named file 3
c        version 6.2    date   17-10-1992 Extension with menu message file
c        version 6.1    date   30-10-1991 Possibility to check existence of
c                                         file without error message
c        version 6.0    date   03-11-1990 Complete redesign because of the
c                                         introduction of the SEPRAN environment
c                                         file
c        version 5.3    date   28-08-1990 SEPPATH replaced by SPHOME
c        version 5.2    date   26-07-1990 Test for existance file 2
c        version 5.1    date   02-07-1990 Extension for IBM PC 386 version
c        version 5.0    date   12-05-1990 Reduction to one parameter
c        version 4.0    date   28-10-1989 Complete revision
c
c   copyright (c) 1982-1999  "Ingenieursbureau SEPRA"
c   permission to copy or distribute this software or documentation
c   in hard copy or soft copy granted only by written license
c   obtained from "Ingenieursbureau SEPRA".
c   all rights reserved. no part of this publication may be reproduced,
c   stored in a retrieval system ( e.g., in memory, disk, or core)
c   or be transmitted by any means, electronic, mechanical, photocopy,
c   recording, or otherwise, without written permission from the
c   publisher.
c
c ********************************************************************
c
c                         DESCRIPTION
c
c    The task of INIFIL is to open all files (except for standard input and
c    output if these files have unit number 5 and 6, or have no name)
c    Which files are opened depends on the parameter ifile
c    By a repeated number of calls more files may be opened
c
c      The subroutine is machine dependent.
c      In order to get one of the other machine dependent versions, it is
c      sufficient to remove comments in the first columns and to set
c      comments in the actual version
c
c      For example to get the VAX-version replace cvax by 4 spaces, etc.
c
c      Available versions:
c
c      IBM                   cibm
c      Cyber     NOS/VE      cybr
c      Apollo                capo
c      Standard Unix         cuni and cunx
c      Vax       VMS         cvax
c      Harris                char
c      HP 9000   Unix        cuni and chpu
c      CRAY                  cray
c      IBM PC 386            cipc
c      ALLIANT   Concentrix  cuni and cali
c      Convex    Unix        cuni and ccon
c
c ********************************************************************
c
c                      KEYWORDS
c
c    machine_dependent
c    open_file
c ********************************************************************
c
c                      INPUT / OUTPUT    PARAMETERS
c
      implicit none
      integer ifile

c     ifile    i   Formal file number of file to be opened. In many practical
c                  situations this is also the actual file number
c                  Possible values for ifile are:
c                  1:  Standard element input file (iref1)
c                  2:  Standard backing storage file (iref2), reset information
c                 -2:  Standard backing storage file (iref2), old file
c                  3:  Standard scratch file (iref3)
c                  4:  Standard error message file (irefer)
c                  5:  Standard input file
c                  6:  Standard output file
c                 10:  File containing the mesh (iref10)  (meshoutput)
c                 17:  Standard menu message file (iref17)
c                 73:  File containing information of the problem (iref73)
c                      (sepcomp.inf)
c                 74:  File containing the solutions (iref74) (sepcomp.out)
c                  With respect to the files 10, 73 and 74 a
c                  negative value of ifile denotes that the file must already
c                  exist
c                  If the file does not exist an error message is given.
c                  If ierror = -1, ierror is set to 1 and control is given back
c                  to the calling subroutine , otherwise the program is stopped
c                  With respect to the files 1 4 and 17 a
c                  positive value of ifile denotes that the file must already
c                  exist and is not changed, a negative value means that the
c                  file may be new
c ********************************************************************
c
c
c                   COMMON BLOCKS
c
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/cfile2'
      include 'SPcommon/cfile3'
      include 'SPcommon/cstand'
      include 'SPcommon/cmachn'
      include 'SPcommon/cmacht'
      include 'bstore.inc'

c ********************************************************************
c
c                       LOCAL PARAMETERS
c
c
      integer nr, nr1, nr4, i, ios, iref
      character * 50 name
      character * 10 facc, fstat
      logical check

c    check     Indication if file exists (true) or not (false)
c    facc      File access parameter
c    fstat     File status parameter
c    i         Counting variable
c    ios       IOSTAT status variable to be used for error messages
c    iref      File reference number
c    name      Name of file
c    nr        Record length of a direct access file in machine dependent
c              quantites ( bytes, words )
c    nr1       Record length of direct access file 1 in machine dependent
c              quantites ( bytes, words )
c    nr4       Record length of direct access file 4 in machine dependent
c              quantites ( bytes, words )
c ********************************************************************
c
c             SUBROUTINES CALLED
c
c     INSTOP  Stop the program
c ********************************************************************
c
c                    ERROR MESSAGES
c
c   -
c ********************************************************************
c
c                    I/O
c
c    none
c ********************************************************************
c
c                    PSEUDO CODE
c
c   trivial
c ======================================================================
c

c     write(6,*) 'inifil: ',ifile,namef2
      if ( abs(ifile).eq.1 ) then

c     --- Open the standard element input file

         irecp1=0
         irecp2=0

c        --- The record length is stored in nr1

         nr1 = nrec1 * lenwor

         if ( ifile.gt.0 ) then

c        --- old file, i.e. the file must be opened readonly

            fstat = 'old'

         else

c        --- old or new file, i.e. call by makef1

            fstat = 'unknown'

         end if

         if ( namef1(1:1).eq.' ' ) then

c        --- namef1 is blank, file = without name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then

c           --- old file, no update is allowed

               inquire ( unit=iref1, exist=check )
               if ( .not. check ) then
                  write(*,100) ' File 1 (standard element file)', iref1
100               format( a,' with reference number:', i3,
     +                    ' does not exist'//
     +                    ' execution terminated' )
                  stop
               end if
            end if
            open ( iref1, access='direct', recl=nr1, iostat=ios,
     +             status=fstat )
            if ( ios.ne.0 ) then
               write(*,110) ' File 1 (standard element file)',iref1,ios
110            format( ' Error in opening of', a,
     +                 ' with reference number:', i3/
     +                 ' IOSTATUS parameter has value', i6//
     +                 ' execution terminated' )
               stop

            end if

         else

c        --- namef1 is not blank, file has name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then
               inquire ( file=namef1, exist=check )
               if ( .not. check ) then
                  write(*,120) namef1
120               format( ' File ', a,
     +                    ' does not exist'//
     +                    ' execution terminated' )
                  stop
               end if
               open ( iref1, file=namef1, access='direct', recl=nr1,
cvax       +                readonly,
     +                iostat=ios, status=fstat )
            else
               open ( iref1, file=namef1, access='direct', recl=nr1,
     +                iostat=ios, status=fstat )
            end if

            if ( ios.ne.0 ) then
               write(*,130) namef1, ios
130            format( ' Error in opening of file ', a/
     +                 ' IOSTATUS parameter has value', i6//
     +                 ' execution terminated' )
               stop
            end if

         end if

      else if ( abs(ifile).eq.2 ) then

c     --- |ifile| = 2   Open file sepranback

c         define file 2
c         The record length is stored in nr

         nr = nrec2 * lenwor

         namef2 = f2name
         if ( namef2(1:1).eq.' ' ) then

c        --- namef2 is blank, file = without name

            if ( ifile.lt.0 ) then

c           --- ifile = -2, file must exist

               inquire ( unit=iref2, exist=check )
               if ( .not. check ) then
                  write(*,100) ' File 2 (SEPRAN backing storage file)',
     +                         iref2
                  call instop
               end if

            end if

            open ( iref2, access='direct', recl=nr, iostat = ios )
            if ( ios.ne.0 ) then
               write(*,110) ' File 2 (SEPRAN backing storage file)',
     +                      iref2, ios
               call instop
            end if

         else

c        --- namef2 is not blank, file has name

            if ( ifile.eq.-2 ) then

c           --- ifile = -2, file must exist

               inquire ( file=namef2, exist=check )
               if ( .not. check ) then
                  write(*,120) namef2
                  call instop
               end if

            end if

            namef2 = f2name
c           write(6,*) 'Open file 2: ',namef2
            open ( iref2, file=namef2, access='direct', recl=nr,
     +             iostat=ios)
            if ( ios.ne.0 ) then
               write(*,130) namef2, ios
               call instop
            end if

         end if

         if ( ifile.eq.2 ) then

c        --- ifile = 2, hence initialise cfile2

            inew   = 1
            ifree2 = 2 + 999 / nrec2
            do 200 i = 1,1000
               iwork3 (i) = 0
200         continue

         end if

      else if ( ifile.eq.3 ) then

c     --- Open file 3  (Scratch file)

         nr = nrec3 * lenwor

         if ( namef3(1:1).ne.' ' ) then

c        --- Scratch file has name

            if ( namef3(1:3).eq.'tmp' .and. namef3(4:4).eq.' ' ) then

c           --- Name of scratch file is tmp
c               The name will become /tmp/tmp.SEPxxxxxx,
c               where xxxxxx is the process id


               namef3(1:12) = '/tmp/tmp.SEP'
               namef3(13:18) = pidsep(1:6)


            else if ( namef3(1:7).eq.'usr/tmp' ) then

c           --- Name of scratch file is tmp/usr
c               The name will become /usr/tmp/tmp.SEPxxxxxx,
c               where xxxxxx is the process id


               namef3(1:16) = '/usr/tmp/tmp.SEP'
               namef3(17:22) = pidsep(1:6)

            end if

            open ( iref3, access='direct', file = namef3, recl = nr )

         else

c        --- standard version

            open ( iref3, status='scratch', access='direct', recl=nr )

         end if

         ifree3=1

      else if ( abs(ifile).eq.4 ) then

c     --- Open the error message file
c         The record length is stored in nr4

         nr4 = 80
         nrec4 = nr4 / lenwor
         if ( lenwor.eq.1 ) then

c        --- lenwor = 1, record length in words instead of bytes

            nr4 = 80 / numchr
            nrec4 = nr4

         else if ( lenwor.eq.3 ) then

c        --- lenwor = 3, Harris computer

            nr4 = 81
            nrec4 = 27

         end if

         if ( ifile.gt.0 ) then

c        --- old file, i.e. the file must be opened readonly

            fstat = 'old'

         else

c        --- old or new file, i.e. call by makef4

            fstat = 'unknown'

         end if

         if ( namef4(1:1).eq.' ' ) then

c        --- namef4 is blank, file = without name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then
               inquire ( unit=irefer, exist=check )
               if ( .not. check ) then
                  write(*,100) ' File 4 (error message file)', irefer
                  stop
               end if
               open ( irefer, access='direct', recl=nr4, iostat=ios,
cvax     +                  readonly,
     +                status=fstat )
            else
               open ( irefer, access='direct', recl=nr4, iostat=ios,
     +                status=fstat )
            end if
            if ( ios.ne.0 ) then
               write(*,110) ' File 4 (error message file)',irefer,ios
               stop
            end if

         else

c        --- namef4 is not blank, file has name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then
               inquire ( file=namef4, exist=check )
               if ( .not. check ) then
                  write(*,120) namef4
                  stop
               end if
            end if

            open ( irefer, file=namef4, access='direct', recl=nr4,
     +             iostat=ios, status=fstat )
            if ( ios.ne.0 ) then
               write(*,130) namef4, ios
               stop
            end if

         end if

      else if ( ifile.eq.5 ) then

c     --- Open the input file, if irefre not equal to 5, or namere has value

         if ( irefre.ne.5 .and. namere(1:1).eq.' ' ) then

c        --- namere is blank, file = without name
c            The file must already exist

            inquire ( unit=irefre, exist=check )
            if ( .not. check ) then
               write(*,100) ' Standard input file', irefre
               call instop
            end if
            open ( irefre, iostat=ios )
            if ( ios.ne.0 ) then
               write(*,110) ' Standard input file',irefre,ios
               call instop
            end if

         else if ( namere(1:1).ne.' ' ) then

c        --- namere is not blank, file has name

            inquire ( file=namere, exist=check )
            if ( .not. check ) then
               write(*,120) namere
               call instop
            end if

            open ( irefre, file=namere, iostat=ios )
            if ( ios.ne.0 ) then
               write(*,130) namere, ios
               call instop
            end if

         end if

      else if ( ifile.eq.6 ) then

c     --- Open the output file, if irefwr not equal to 6, or namewr has value
c         Define status parameter and access parameter depending on spappn
c         and the type of computer

         facc  = 'sequential'
         fstat = 'unknown'
         if ( spappn(1:1).eq.'y' .or. spappn(1:1).eq.'Y' ) then

c        --- File must be opened in append mode

cipc            fstat = 'append'
            facc  = 'append'

         end if

         if ( irefwr.ne.6 .and. namewr(1:1).eq.' ' ) then

c        --- namewr is blank, file = without name
c            append mode is not possible

            open ( irefwr, iostat=ios )
            if ( ios.ne.0 ) then
               write(*,110) ' Standard output file',irefwr,ios
               call instop
            end if

         else if ( namewr(1:1).ne.' ' ) then

c        --- namewr is not blank, file has name

            open ( unit=irefwr, file=namewr, access=facc, status=fstat,
     +             iostat=ios)
            if ( ios.ne.0 ) then
               write(*,130) namewr, ios
               call instop
            end if

         end if

      else if ( abs(ifile).eq.17 ) then

c     --- Open the menu message file
c         The record length is stored in nr4

         nr4 = 80
         nrec4 = nr4 / lenwor
         if ( lenwor.eq.1 ) then

c        --- lenwor = 1, record length in words instead of bytes

            nr4 = 80 / numchr
            nrec4 = nr4

         else if ( lenwor.eq.3 ) then

c        --- lenwor = 3, Harris computer

            nr4 = 81
            nrec4 = 27

         end if

         if ( ifile.gt.0 ) then

c        --- old file, i.e. the file must be opened readonly

            fstat = 'old'

         else

c        --- old or new file, i.e. call by makef17

            fstat = 'unknown'

         end if

         if ( namems(1:1).eq.' ' ) then

c        --- namems is blank, file = without name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then
               inquire ( unit=irefer, exist=check )
               if ( .not. check ) then
                  write(*,100) ' File 17 (menu message file)', iref17
                  stop
               end if
            end if
            open ( iref17, access='direct', recl=nr4, iostat=ios,
     +             status=fstat )
            if ( ios.ne.0 ) then
               write(*,110) ' File 17 (menu message file)',iref17,ios
               stop
            end if

         else

c        --- namems is not blank, file has name
c            If ifile > 0, the file must already exist

            if ( ifile.gt.0 ) then
               inquire ( file=namems, exist=check )
               if ( .not. check ) then
                  write(*,120) namems
                  stop
               end if
            end if

            open ( iref17, file=namems, access='direct', recl=nr4,
     +             iostat=ios, status=fstat )
            if ( ios.ne.0 ) then
               write(*,130) namems, ios
               stop
            end if

         end if

      else

c     --- ifile = 10 (meshoutput), 73 (sepcomp.inf) or 74 (sepcomp.out)

         if ( abs(ifile).eq.10 ) then

            name = name10
            iref = iref10

         else if ( abs(ifile).eq.73 ) then

            name = name73
            iref = iref73

         else if ( abs(ifile).eq.74 ) then

            name = name74
            iref = iref74

         end if

         if ( ifile.lt.0 ) then

c        --- ifile < 0, file must exist

            if ( name(1:1).eq.' ' ) then

c           --- File has no name

               inquire ( unit=iref, exist=check )
               if ( .not. check ) then
                  write(*,100) ' File ', iref
                  if ( ierror.eq.-1 ) then
                     ierror = 1
                     go to 1000
                  else
                     call instop
                  end if
               end if

            else

c           --- File has a name

               inquire ( file=name, exist=check )
               if ( .not. check ) then
                  write(*,120) name
                  if ( ierror.eq.-1 ) then
                     ierror = 1
                     go to 1000
                  else
                     call instop
                  end if
               end if

            end if

         end if

         if ( iref.gt.0 ) then

c        --- iref > 0:  formatted

            if ( name(1:1).eq.' ' ) then

c           --- open file without name

               open ( unit = iref )

            else

c           --- open file with name

               open ( unit = iref, file = name )

            end if

         else

c        --- iref < 0, unformatted

            if ( name(1:1).eq.' ' ) then

c           --- open file without name

               open ( unit = -iref, form = 'unformatted' )

            else

c           --- open file with name

               open ( unit = -iref, file = name, form = 'unformatted'
capo     +                ,recl = 1000000
     +              )

            end if

         end if

      end if
      ierror = 0
1000  nerror = 0
      end
