      real*4 vr,gnub,gnut,tr,rcput,veloc1,veloc2,q_ca,t_bota,heatproda 
      real*4 phiava,avt_a,heatava
      integer nodata,ndegasa,nconta
      common /perealdata/ vr(1000),gnub(1000),gnut(1000),tr(1000),
     v                    rcput(1000),veloc1(1000),veloc2(1000),
     v                    q_ca(3,1000),avt_a(3,1000),
     v                    t_bota(1000),heatproda(2,1000), 
     v                    phiava(1000),heatava(1000),
     v                    nodata,ndegasa(4,1000),nconta(1000) 
      real*4 a238loss,a235loss,a232loss,aHe3loss,aHe4loss
      real*4 aK40loss,aAr40loss 
      common /pemoredata/ a238loss(4,1000),a235loss(4,1000),
     v      a232loss(4,1000),aHe3loss(4,1000),aHe4loss(4,1000),
     v      aK40loss(4,1000),aAr40loss(4,1000)
