c *************************************************************
c *   IELFROMGRID
c *
c *   Determine the element that contains (x,y). IELTOGRID
c *   should have been used to store element numbers in the
c *   equidistant array iel(nxel,nyel).
c *
c *   If the correct element is found the element & interpolation info
c *   is returned in ielem, nodno, xn, yn, phiq, xi, eta.
c *
c *   /pieltogrid/
c *      dxel  - grid spacing in x
c *      dyel  - grid spacing in y
c *      iel   - array containing element numbers 
c *      nxel  - x-dimension of iel
c *      nyel  - y-dimension of iel
c *
c *   PvK 970411
c *************************************************************
      subroutine ielfromgrid(ichois,x,y,kmeshc,coor,
     v          nodno,xn,yn,iel_now,ielem,phiq,xi,eta)
      implicit none
      integer ichois,ielem,kmeshc(*),nodno(*),iel_now(*)
      real*8 x,y,coor(2,*),xn(*),yn(*),phiq(*)
      logical out

      include 'ccc.inc'
      include 'pieltogrid.inc'
      include 'elem_topo.inc'
      integer i,j,inpelm,ip,ix1,ix2,iy1,iy2
      integer ith0,ith3,ir0,ir3
      integer isub,nodlin(3)
      logical checkinelem
      real*8 rl(3),xi,eta
 
c     *** estimate location in regular grid based on (x,y)
      ix1 = (x+xoff)/dxel + 1
      if (ix1.eq.nxel) then
         ix1=ix1-1
      endif
      ix2 = ix1+1
      iy1 =(y+yoff)/dyel + 1
      if (iy1.eq.nyel) then
         iy1=iy1-1
      endif
      iy2 = iy1 + 1

c     *** find the elements indicated by iel
      iel_now(1) = iel(ix1,iy1)
      iel_now(2) = iel(ix2,iy1)
      iel_now(3) = iel(ix2,iy2)
      iel_now(4) = iel(ix1,iy2)
c     *** Check for duplicates
      if (iel_now(2).eq.iel_now(1)) then
         iel_now(2)=0
      endif
      if (iel_now(3).eq.iel_now(1).or.iel_now(3).eq.iel_now(2)) then 
         iel_now(3)=0
      endif
      if (iel_now(4).eq.iel_now(1).or.iel_now(4).eq.iel_now(2).or.
     v       iel_now(4).eq.iel_now(3)) then 
         iel_now(4)=0
      endif


c     *** test coordinates to see which element 
      inpelm=6
      i=1
100   continue
          if (iel_now(i).gt.0) then
             ip = (iel_now(i)-1)*inpelm
             do j=1,inpelm
                nodno(j) = kmeshc(ip+j)
                xn(j) = coor(1,nodno(j))
                yn(j) = coor(2,nodno(j))
             enddo
             if (curved_elem(iel_now(i))) then
                out = .not.checkinelem(3,xn,yn,x,y,nodno,nodlin,rl,
     v                xi,eta,phiq,isub)
             else
                out = .not.checkinelem(1,xn,yn,x,y,nodno,nodlin,rl,
     v                xi,eta,phiq,isub)
             endif
          else
             out = .true.
          endif
          if (out) then
             if (i.lt.4) then
                i=i+1
                goto 100
             else
c               write(6,*) 'PERROR(ielfromgrid): search failed'
c               write(6,*) 'Something must be wrong in array iel' 
c               write(6,*) 'Perhaps the elements are too small for'
c               write(6,*) 'current discretization? '
c               write(6,'(''x,y: '',4f8.3)') x,y
c               write(6,'(''iel_now: '',4i8)') (iel_now(i),i=1,4)
                ielem = -1
             endif
          else
             ielem=iel_now(i)
             return
          endif

      return
      end
