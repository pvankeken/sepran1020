c *************************************************************
c *   TRACHEAT
c *
c *   Calculate heat production based on tracer distribution 
c *   in nodal points. 
c *   Parameters:
c *        kmesh1   i  mesh definition for velocity
c *        kprob1   i  problem definition for velocity
c *        coormark i  array containing tracers
c *        idens   i  vector containing density of tracers
c *        iheat   o  vector containing heat production
c * 
c *   
c *   TRACDENS should have been called before tracheat to 
c *   contain the updated tracer density
c *
c *   PvK 080300
c *************************************************************
      subroutine tracheat(kmesh1,kprob1,coormark,chemmark,q_layer,
     v                    idens,iheat) 
      implicit none
      integer kmesh1(*),kprob1(*),idens(*),iheat(*)
      real*8 coormark(*),chemmark(*),q_layer(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence(buffr(1),ibuffr(1))

      include 'tracer.inc'
      include 'ccc.inc'

      integer ikelmc,ikelmi,npoint,ntot,i,j,k,iel,ifound
      real*8 xm,ym,xn(6),yn(6),shapef(6),un(6),vn(6),u,v,done
      integer nodno(6),imissed,inidgt,iniget,isub,nelem,ikelmo
      integer findsubelem,icorrect,ipdens,index,nodlin(3),ipheat,nelgrp 
      real*8 r,th,rl(3),xl(3),yl(3),elemheat(3),tracdens04,user(1)
      logical guess_first
      real*8 xi,eta

      npoint = kmesh1(8)
      nelem = kmesh1(9)
      nelgrp = kmesh1(5)
      if (periodic) nelem = kmesh1(kmesh1(21))

c     *** check if idens and iheat are created correctly
      if (iheat(1).eq.0) then
         write(6,*) 'PERROR(tracheat): array iheat does not exist'
         call instop
      endif
      if (iheat(2).ne.115) then
          write(6,*) 'PERROR(tracheat): vector heat exists'
          write(6,*) 'but is not of type VECTR: ',iheat(2)
          call instop
      endif
      if (iheat(4).ne.1) then
          write(6,*) 'PERROR(tracheat): vector heat exists'
          write(6,*) 'and is of type VECTOR'
          write(6,*) 'but IVEC is wrong: ',iheat(4)
          call instop
      endif
      if (iheat(5).ne.npoint) then
          write(6,*) 'PERROR(tracheat): vector heat exists'
          write(6,*) 'and is of type VECTOR with IVEC=1'
          write(6,*) 'but NUMDEGFD is not NPOINT: ',iheat(5)
          call instop
      endif
      if (idens(1).eq.0) then
         write(6,*) 'PERROR(tracheat): array idens does not exist'
         call instop
      endif
      if (idens(2).ne.115) then
          write(6,*) 'PERROR(tracheat): vector dens exists'
          write(6,*) 'but is not of type VECTR: ',idens(2)
          call instop
      endif
      if (idens(4).ne.1) then
          write(6,*) 'PERROR(tracheat): vector dens exists'
          write(6,*) 'and is of type VECTOR'
          write(6,*) 'but IVEC is wrong: ',idens(4)
          call instop
      endif
      if (idens(5).ne.npoint) then
          write(6,*) 'PERROR(tracheat): vector dens exists'
          write(6,*) 'and is of type VECTOR with IVEC=1'
          write(6,*) 'but NUMDEGFD is not NPOINT: ',idens(5)
          call instop
      endif

      call ini050(iheat(1),'tracheat: heat')
      call ini050(idens(1),'tracheat: dens')

      call ini050(kmesh1(23),'tracheat: coordinates')
      call ini050(kmesh1(17),'tracheat: nodal points')
      call ini050(kmesh1(29),'tracheat: kmesh part o')

      ikelmc = iniget(kmesh1(17))
      ikelmi = inidgt(kmesh1(23))
      ikelmo = iniget(kmesh1(29))
      ipdens = inidgt(idens(1))
      ipheat = inidgt(iheat(1))

c     *** Set vectors to zero
      call tracdens02(buffr(ipheat),npoint)
      call tracdens02(buffr(ipdens),npoint)

      if (itracoption.eq.2) then
         write(6,*) 'PERROR(tracheat): not yet implemented for ' 
         write(6,*) 'markerchain method'
         call instop
      endif
      ntot = 0
      do idist=1,ndist
         ntot = ntot+ntrac(idist)
      enddo

      imissed = 0
      ifound = 0
      do i=1,ntot
         xm = coormark(2*i-1)
         ym = coormark(2*i)
c *************************************************************
c *      Find the element in which (xm,ym) lies
c *      Look it up in the table 
c *************************************************************
         guess_first=.false.
         call pedeteltrac(0,xm,ym,ikelmc,ikelmi,ikelmo,nodno,nodlin,
     v        rl,xn,yn,un,vn,user,iel,npoint,nelem,imissed,ifound,
     v        nelgrp,shapef,xi,eta,guess_first,'tracheat')

c        *** Now that we have the right element, subdivide it into
c        *** four linear ones and find correct subtriangle
         isub = findsubelem(xn,yn,xm,ym,xl,yl,rl,nodno,nodlin)

         do k=1,3
            elemheat(k) = q_layer(1)
         enddo

c        *** Add element density to vector
         call tracdens01(buffr(ipdens),nodlin,rl)

c        *** Add element heat production to vector
         call tracheat01(buffr(ipheat),nodlin,rl,elemheat)

      enddo

c     *** Scale heatproduction by tracer density
      do i=1,npoint
         if (buffr(ipdens+i-1).gt.0) then
         buffr(ipheat+i-1) = buffr(ipheat+i-1)/buffr(ipdens+i-1)
         endif
      enddo

c     write(6,*) 'Missed about ',imissed,' points'
      if (imissed.gt.0.01*ntot) then
         write(6,*) 'PWARN(tracheat): interpolation of velocity'
         write(6,*) '  is inefficient for more than 1% of the tracers'
         write(6,*) '  Time to rethink the mesh gridding strategy?'
         write(6,*) '  imissed = ',imissed
      endif

      return
      end

      subroutine tracheat01(dens,nodlin,rl,elemheat)
      implicit none
      real*8 dens(*),rl(*),elemheat(*)
      integer nodlin(*)
      integer k

      do k=1,3
         dens(nodlin(k)) = dens(nodlin(k)) + rl(k)*elemheat(k)
      enddo

      return
      end
  

