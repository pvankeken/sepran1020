c *************************************************************
c *   CORCOORT
c *
c *   Corrects the position of tracers
c *   Adapted from corcoor to be used with tracers.
c *
c *   PvK 960214
c *************************************************************
      subroutine corcoort(coormark,coornewm,velmark,velnewm)
      implicit none
      real*8 coormark(2,*),velmark(2,*),velnewm(2,*),coornewm(2,*)
      include 'SPcommon/ctimen'
      include 'tracer.inc'
      integer i,k,ntot
      include 'ccc.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)
      real*8 r,th,xm,ym,cost
      include 'crminmax.inc'

      write(6,*) 'PERROR(corcoort): obsolete, use RK methods'
      call instop

      ntot=0
      do idist=1,ndist
         ntot = ntot+ntrac(idist)
      enddo
      do i=1,ntot
         xm = coormark(1,i)
         ym = coormark(2,i)
         xm = xm+0.5*tstep*(velmark(1,i)+velnewm(1,i))
         ym = ym+0.5*tstep*(velmark(2,i)+velnewm(2,i))

         r = sqrt(xm*xm+ym*ym)
         if (r.gt.radius_max) then
c           write(6,*) 'adapted because of radius_max: ',xm,ym,r
            cost = ym/r
            if (xm.gt.0) then
               th = acos(cost)
            else
               th = 2*pi - acos(cost)
            endif
            xm = xm - (r-radius_max+1d-6)*sin(th)
            ym = ym - (r-radius_max+1d-6)*cos(th)
         endif

         if (r.lt.radius_min) then
c           write(6,*) 'adapted because of radius_min: ',xm,ym,r
            cost = ym/r
            if (xm.gt.0) then
               th = acos(cost)
            else
               th = 2*pi - acos(cost)
            endif
            xm = xm + (radius_min-r+1d-6)*sin(th)
            ym = ym + (radius_min-r+1d-6)*cos(th)
         endif

c        *** for the half geometry
         if (half) then
            if (xm.lt.0) xm=0
         endif

         coormark(1,i) = xm
         coormark(2,i) = ym
      enddo

      return
      end
