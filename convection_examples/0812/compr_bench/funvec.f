c *************************************************************
c *   FUNCVEC
c *
c *   PvK 082808
c *************************************************************
      subroutine funvec(rinvec,reavec,nvec,coor,outvec)
      implicit none
      real*8 rinvec(*),reavec(*),coor(*),outvec(*)
      integer nvec
      include 'cpephase.inc'
      real*8 y,adiabat

      y = coor(2)
      if (iadiabat.eq.2) then
         outvec(1) = reavec(1)+adiabat(y)-adiabat(1d0)
      else if (iadiabat.eq.3) then
         outvec(1) = reavec(1)+adiabat(y)
      endif

      end

