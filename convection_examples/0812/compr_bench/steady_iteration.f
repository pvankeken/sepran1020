c *************************************************************
c *   STEADY_ITERATION
c *
c *   PvK 071000
c *************************************************************
      subroutine steady_iteration(iuser,user)
      implicit none
      integer iuser(*)
      real*8 user(*)
   
      integer ibuffr
      common ibuffr(1)
      include 'mysepar.inc'
      include 'c1visc.inc'
      include 'pesteady.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'pecof900.inc'
      include 'bound.inc'

      real*4 second
      real*4 cpustart,cpunow,t10,t11
      integer iinbld(10),matrm(5),irhs1(5),iall,ivisdip(5)
      integer ielhlp,iinder(10),npoint,DONE,ikelmi,iqtype
      integer iinvec(20),ihelp,ibp
      real*8 phiav,heatdummy,rinvec(20),vrms,vrms2
      parameter(DONE=1d0)
      save iinbld,matrm,irhs1,ivisdip,iinder

      niter=0
      cpustart=second()
100   continue
        niter=niter+1
   
        call copyvc(isol1,islol1)
c       call phifromsepran(kmesh1,kprob1,isol1,ivisdip)

c       **** Heat equation
        call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v          isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user)
        call bmheat(kmesh2,kprob2,intmt2,iuser,user,
     v               isol2,islol2,matr2)
c       **** Relaxation
        if (relax.ne.0) then
           call algebr(3,0,islol2,isol2,isol2,kmesh2,kprob2,
     v               1-relax,relax,0,0,ip)
        endif

        call stokes(1,iuser,user)

        call intermediate_output(kmesh1,kmesh2,kprob1,kprob2,
     v     isol1,isol2,islol1,islol2,iheat,idens,ivisc,isecinv,
     v     iuser,user)

        cpunow = second()
        if (niter.lt.nsteadymax.and.dif.gt.eps) goto 100

      write(6,*) 'before compr/bmout: ',iuser(1),user(1)
      call bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,isol3,
     v             iuser,user)

      return
      end
