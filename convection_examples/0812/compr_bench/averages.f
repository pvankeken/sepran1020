      subroutine averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,user,
     v                    nth_output,nr_output,nout)
      implicit none
      integer kmesh1(*),kmesh2(*),kprob1(*),kprob2(*),isol2(*),isol1(*)
      integer nth_output,nr_output,nout,npoint
      real*8 user(*)
      include 'ccc.inc'

      npoint = kmesh2(8)
      if (cyl) then
         call temptogrid(kmesh1,kprob1,kmesh2,kprob2,isol2,
     v                   user,user(10+npoint),nth_output,nr_output)
         call averages_cyl(kmesh2,kprob2,isol2,user(10+npoint),nout)
      else
         call pefilxy(1,kmesh1,kprob1,isol1)
         call averages_cart(kmesh2,kprob2,isol2,user(10+npoint),nout)
      endif

      return
      end

c *************************************************************
c *   AVERAGES_CYL
c *   Compute and output radial averages of T, eta.
c *   Then computer average T (and average T in upper/lower layer
c *   if rmid is defined).
c *   Should be used after temptogrid has been called
c * 
c *   PvK 990908
c *************************************************************
      subroutine averages_cyl(kmesh2,kprob2,isol2,temp,noutput) 
      implicit none
      integer kmesh2(*),kprob2(*),isol2(*),noutput
      real*8 temp(*)
      include 'ccc.inc'
      include 'bound.inc'
      include 'vislo.inc'
      include 'coolcore.inc'
      include 'averages.inc'
      include 'c1visc.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)
      real*8 r,th,avetemp(500),avevis(500),dth,dr,x,y,pefvis
      real*8 secsqrdummy,viscosity,r1l,r0l,davT,temperature
      real*8 avevistop,avevisbot,avetempbot,avetemptop,temp_mid
      integer ir,ith,ival,ivalfind1,nrmid,ip,nth,nr
      character*80 fname

      nr  = nr_output
      nth = nth_output
      dth = dth_output
      dr  = dr_output
      if (nr.gt.500) then
         write(6,*) 'PERROR(averages) nr > 500: ',nr
         call instop
      endif
      if (noutput.ge.0) then 
         write(fname,'(''averages/avetemp.'',i3.3)') noutput
         open(68,file=fname)
         write(68,*) 2,nr+2
         if (itypv.gt.0) then
           write(fname,'(''averages/avevis.'',i3.3)') noutput
           open(67,file=fname)
           write(67,*) 2,nr+2
         endif
      endif
      do ir=1,nr
         avetemp(ir) = 0
         avevis(ir) = 0
         avevisbot = 0
         avevistop = 0
         r = r1 + (ir-0.5)*dr
         do ith=1,nth
           th = (ith-0.5)*dth
            x = r*sin(th)
            y = r*cos(th)
           ival=1
           ip = (ith-1)*nth+ir
           temperature = temp(ip)
           if (ivl) ival = ivalfind1(x,y)
           viscosity = pefvis(x,y,temperature,ival,secsqrdummy)
           avetemp(ir) = avetemp(ir) + temperature*dth
           avevis(ir) = avevis(ir) + viscosity*dth
         enddo
         avetemp(ir) = avetemp(ir)/(nth*dth)
         avetemptop = 0d0
         avetempbot = t_bot
         avevis(ir) = avevis(ir)/(nth*dth)
         avevistop = avevistop/(nth*dth)
         avevisbot = avevisbot/(nth*dth)
      enddo
c     *** bottom layer
      r = r1
      th = 0
      x = r*sin(th)
      y = r*cos(th) 
      ival=1
      if (ivl) ival = ivalfind1(x,y)
      viscosity = pefvis(x,y,t_bot,ival,secsqrdummy)
      avevisbot = viscosity
c     *** top layer
      r = r2
      th = 0
      x = r*sin(th)
      y = r*cos(th) 
      ival=1
      if (ivl) ival = ivalfind1(x,y)
      viscosity = pefvis(x,y,0d0,ival,secsqrdummy)
      avevistop = viscosity

      if (noutput.ge.0) then
         if (itypv.gt.0) then
            write(67,*) avevisbot,r1
            do ir=1,nr
              write(67,*) avevis(ir),r1+(ir-0.5)*dr
            enddo
            write(67,*) avevistop,r2
            close(67)
         endif
         write(68,*) avetempbot,r1
         do ir=1,nr
           write(68,*) avetemp(ir),r1+(ir-0.5)*dr
         enddo
         write(68,*) avetemptop,r2
         close(68)
      endif


      if (axi) return

c *************************************************************
c *   FIND AVERAGE T
c *   Use cylindrical version of trapezoid rule.
c *   T is defined in the middle of interval.
c *
c *   <<T>> =      int pi <T(r)> r dr
c *                ------------------
c *                   int pi r dr
c *************************************************************
      
c     *** first half interval
      r0l = r1
      r1l = r1+0.5*dr
      davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)*
     v             t_bot/(0.5*dr) 
      davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(1)/(0.5*dr)
      avT = pi*davT
c     write(6,'(''r0l: '',i5,5e15.7)') 0,r0l,r1l,r1l-r0l,davT,avT
      do ir=1,nr-1
         r0l = r1 + (ir-0.5)*dr
         r1l = r1 + (ir+0.5)*dr
         davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)*
     v             avetemp(ir)/dr 
         davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(ir+1)/dr 
         avT = avT + pi*davT
c        write(6,'(''r0l: '',i5,5e15.7)') ir,r0l,r1l,r1l-r0l,davT,avT
      enddo
c     *** last half interval
      r0l = r2-0.5*dr
      r1l = r2
      davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)*
     v             avetemp(nr)/(0.5*dr) 
      davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemptop/(0.5*dr) 
      avT = avT + pi*davT
c     write(6,'(''r0l: '',i5,5e15.7)') nr,r0l,r1l,r1l-r0l,davT,avT
      avT = avT / (pi*frac*(r2**2-r1**2))
c     write(6,*) 'average T: ',avT


c *************************************************************
c *   Compute average T in upper/lower layer if necessary
c *
c *   R1               Rmid              R2
c *   |                  |                |
c *     |---|---|---|---|-++|+++|+++|+++|
c *     1   2   3     nrmid 
c *************************************************************
      if (rmid.gt.0) then
         nrmid = (rmid-(r1+0.5*dr))/dr+1
c        *** temperature at Rmid
         temp_mid = avetemp(nrmid)+(avetemp(nrmid+1)-avetemp(nrmid))/dr*
     v         (rmid-(nrmid+0.5)*dr)
c        *** Lower layer
c        *** First half interval
         r0l = r1
         r1l = r1+0.5*dr
         davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)* 
     v               t_bot/(0.5*dr) 
         davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(1)/(0.5*dr)
         avTl = davT*pi
c        write(6,'(''r0l: '',i5,5e15.7)') 0,r0l,r1l,r1l-r0l,davT,avTl
         do ir=1,nrmid-1
           r0l = r1 + (ir-0.5)*dr
           r1l = r1 + (ir+0.5)*dr
           davT =(1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)* 
     v             avetemp(ir)/dr
           davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(ir+1)/dr 
           avTl = avTl + pi*davT
c        write(6,'(''r0l: '',i5,5e15.7)') ir,r0l,r1l,r1l-r0l,davT,avTl
         enddo
         r0l = r1 + (nrmid-0.5)*dr
         r1l = rmid
         if (r1l-r0l.gt.0) then
          davT =(1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)* 
     v             avetemp(nrmid)/(r1l-r0l)
          davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             temp_mid/(r1l-r0l)
          avTl = avTl + pi*davT
         endif
c        write(6,'(''r0l: '',i5,5e15.7)') ir,r0l,r1l,r1l-r0l,davT,avTl

c        *** upper layer
c        write(6,*) 'Upper layer'
         r0l = rmid
         r1l = r1 + (nrmid+0.5)*dr
         if (r1l-r0l.gt.0) then
          davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)*
     v             temp_mid/(r1l-r0l)
          davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(nrmid)/(r1l-r0l)
          avTu = pi*davT
         endif
c        write(6,'(''r0l: '',i5,5e15.7)') ir,r0l,r1l,r1l-r0l,davT,avTu
         do ir=nrmid+1,nr-1
           r0l = r1 + (ir-0.5)*dr
           r1l = r1 + (ir+0.5)*dr
           davT =(1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)* 
     v             avetemp(ir)/dr 
           davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemp(ir+1)/dr 
           avTu = avTu + pi*davT
c        write(6,'(''r0l: '',i5,5e15.7)') ir,r0l,r1l,r1l-r0l,davT,avTu
         enddo
c        *** last half interval
         r0l = r2-0.5*dr
         r1l = r2
         davT = (1d0/6d0*r1l**3-1d0/2*r0l*r0l*r1l+1d0/3d0*r0l*r0l*r0l)*
     v             avetemp(nr)/(0.5*dr) 
         davT = davT + 
     v      (1d0/6d0*r0l**3-1d0/2*r0l*r1l*r1l+1d0/3d0*r1l*r1l*r1l)*
     v             avetemptop/(0.5*dr) 
         avTu = avTu + pi*davT
c        write(6,'(''r0l: '',i5,5e15.7)') nr,r0l,r1l,r1l-r0l,davT,avTu

c        write(6,*) 'avT: ',avT
c        write(6,*) 'avT: ',(avTu+avTl)/(pi*frac*(r2**2-r1**2))
         avTl = avTl / (pi*frac*(rmid**2-r1**2))
         avTu = avTu / (pi*frac*(r2**2-rmid**2)) 
c        write(6,*) 'avTu: ',avTu
c        write(6,*) 'avTl: ',avTl
      endif

      return
      end
      
c *************************************************************
c *   AVERAGES_CART
c *   
c *************************************************************
      subroutine averages_cart(kmesh2,kprob2,isol2,temp,noutput)
      implicit none
      integer kmesh2(*),kprob2(*),isol2(*),noutput
      real*8 temp(*)
      include 'pexcyc.inc'
      include 'c1visc.inc'
      include 'vislo.inc'
      include 'averages.inc'
      real*8 avetemp(NXCYCMAX),avevis(NXCYCMAX)
      real*8 DONE
      parameter(DONE=1d0)
      character*80 fname
      integer i,ix,iy,ip,it1,it2,ival,ivalfind
      real*8 y1,y2,x1,x2,vis1,vis2,secsqrdummy,x,y,pefvis

      secsqrdummy=0
      if (noutput.ge.0) then
         write(fname,'(''averages/avetemp.'',i3.3)') noutput
         open(68,file=fname)
         if (itypv.gt.0) then
           write(fname,'(''averages/avevis.'',i3.3)') noutput
           open(67,file=fname)
         endif
      endif

      call pecopy(0,isol2,temp,kmesh2,kprob2,1,DONE)


      do iy=1,ny
         avetemp(iy)=0
         avevis(iy)=0
         y = yc(iy)
         do ix=1,nx-1
            x1=xc(ix)
            x2=xc(ix+1) 
            it1 = (iy-1)*nx+ix
            it2 = (iy-1)*nx+ix+1
            avetemp(iy) = avetemp(iy)+
     v        0.5*(x2-x1)*(temp(it1)+temp(it2))
            if (itypv.gt.0) then
              if (ivl) ival=ivalfind(y)
              vis1 = pefvis(x,y,temp(it1),ival,secsqrdummy)
              vis2 = pefvis(x,y,temp(it2),ival,secsqrdummy)
              avevis(iy) = avevis(iy)+
     v          0.5*(x2-x1)*(vis1+vis2)
            endif
         enddo
         avetemp(iy)=avetemp(iy)/(xcmax-xcmin)
         avevis(iy)=avevis(iy)/(xcmax-xcmin)
      enddo

      if (noutput.ge.0) then
         do i=1,ny
            write(68,*) avetemp(i),yc(i)
         enddo
         close(68)
         if (itypv.gt.0) then
            do i=1,ny
               write(67,*) avevis(i),yc(i)
            enddo
            close(67)
         endif
      endif

      avT = 0
      do i=1,ny-1
         y1=yc(i)
         y2=yc(i+1)
         avT = avT + 0.5*(y2-y1)*(avetemp(i)+avetemp(i+1))
      enddo

      return
      end

