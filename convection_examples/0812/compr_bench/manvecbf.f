      subroutine manvecbf ( ibuffr, buffer, iinvec, rinvec, invec1,
     +                      invec2, iresvc, kmesh, kprob )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.10 date 19-03-2007 Extension for mapping to type 116
!        version  1.9  date 29-10-2003 Extension with mapping to vectors
!                                      defined per element
!        version  1.8  date 30-10-2002 Extension of iinvec
!        version  1.7  date 23-09-2002 New call to to0099
!        version  1.6  date 21-09-2002 Extra options 47/48/49
!        version  1.5  date 26-09-2001 Extra options 44/45/46
!        version  1.4  date 12-11-2000 Extra option function of vector
!        version  1.3  date 04-09-2000 Extra option limit
!        version  1.2  date 02-01-2000 New call to to0035 (skip element groups)
!        version  1.1  date 12-08-1999 New call to erclmn
!        version  1.0  date 10-04-1999
!
!   copyright (c) 1999-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     General subroutine for manipulations with SEPRAN arrays
!
! **********************************************************************
!
!                       KEYWORDS
!
!     algebraic_manipulation
!     manipulation
!     vector
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cconst'
      include 'SPcommon/cinout'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer iinvec(*), invec1(*), invec2(*), kmesh(*), kprob(*),
     +        iresvc(*), ibuffr(*)
      double precision rinvec(*), buffer(*)

!   buffer   i/o  Real buffer array in which all large data is stored
!   ibuffr   i/o  Integer buffer array in which all large data is stored
!   iinvec    i   In this integer input array it is indicated what type of
!                 manipulations must be preformed by subroutine manvecbf.
!                 For some cases IINVEC acts both as input and
!                 as output array.
!                 The entries of IINVEC must be filled by the user as follows:
!                 1  Number of entries of IINVEC that are filled by the user.
!                    IINVEC(1) must be at least equal to 2.
!                    For positions that are not filled by the user (except the
!                    positions 1 and 2) defaults are used.
!                 2  ICHOICE  General choice parameter indicating what type of
!                             algebraic manipulations have to be carried out.
!                             ichoice must always have been given a value by
!                             the user.
!                    Description of the possible values of ichoice
!                       1     Average: c = average (u1)
!                       2     Extract value of one degree of freedom in one
!                             point from u1.
!                             ISPEC defines if this is a node (0 (default))
!                             or a user point (1)
!                       3     Minimum and maximum value of u1 or | u1 |.
!                       4     Inner product:  c = ( u1 , u2 ) .
!                       5     || u1 ||  or || u1 - u2 ||
!                      26     Subtraction of constant:  u out = u1 - c
!                      27     Linear combination:  u out = c1 u1 +  c2 u2
!                      28     Put value of one degree of freedom in one point
!                             into  u out
!                      29     Modulus of complex vector.
!                      30     Phase of complex vector.
!                      31     User defined function of two SEPRAN vectors:
!                             u out =  f ( u1 , u2 ) .
!                      32     User defined function of  n  SEPRAN vectors and
!                             the co-ordinates:
!                             u out =  f ( u1 , u2 , + . + , u n, x ) .
!                      33     Length of vector composed by components:
!                             u out (i) = sqrt ( u1 (i) ** 2 + u2 (i) ** 2 )
!                             or
!                             u out (i) = sqrt(u1(i)**2 + u2(i)**2 + u3(i)**2)
!                      34     u out (i) = conjg( u1(i) )
!                      35     SEPRAN function FUNADA of two SEPRAN vectors:
!                             u out =  f ( u1 , u2 ) .
!                      36     Put degree of freedom IDEGFD into a vector
!                             consisting of one unknown in each point
!                             It is supposed that the original vector has the
!                             degree of freedom in each point
!                             ISPEC defines if this is a node (0 (default))
!                             or a user point (1)
!                      37     Real part of complex vector
!                      38     Imaginary part of complex vector
!                      39     Linear combination:  u out = c1 u1 +  c2 u2
!                             with constraint: u out(i)>=rinvec(3)
!                      40     Componentwise product of two vectors:
!                             u out(i) = c1 u1(i) u2(i)
!                      41     Point-wise inner-product of two vectors:
!                             u out(i) = sum_j u1(i)_j + u2(i)_j
!                             if idegfd>0, only the first ndegfd components
!                      42     Limit u to values between rinvec(1) and rinvec(2)
!                      43     uout(i) = function of vec1(i)
!                             The type of function is stored in ispec
!                             Possible values:
!                             1: log
!                      44     uout(i) is vector with 1 component in the
!                             points where vec1(i,.) is defined
!                             vec1 must be a vector with ndim components
!                             uout(i) = a . vec1(i)
!                             The vector a must be stored in rinvec(1..ndim)
!                             The type of output vector must be defined in
!                             ISPEC (0=solution vector, >0 vector of special
!                             structure)
!                      45     uout(i) is vector with ndim components in the
!                             points where vec1(i) is defined
!                             vec1 must be a vector with 1 component
!                             uout(i) = vec1(i) a
!                             The vector a must be stored in rinvec(1..ndim)
!                             The type of output vector must be defined in
!                             ISPEC (0=solution vector, >0 vector of special
!                             structure)
!                      46     uout = u - u.n n
!                             The input vector must be a vector with ndim
!                             components defined on a curve (2d) or a
!                             surface (3d) only
!                             The normal on this curve or surface is computed
!                             and the normal component of u in the n direction
!                             is subtracted from u, thus making u a vector
!                             in the curve or surface only.
!                             ISPEC must contain the curve or surface number
!                      47     uout = u.n n
!                             See 46, however, in this case the normal velocity
!                             is computed
!                      48     uout(i) is vector with ndim components in the
!                             points where vec1(i) is defined
!                             vec1 must be a vector with ndim components
!                             vec2 must be a vector
!                             uout(i) = vec1_idegfd(i) vec2_idegfd(i) e_idegfd
!                             The output vector has the same structure as
!                             vec1
!                      49     uout(i) is vector with 1 component in the
!                             points where vec1(i,.) is defined
!                             vec1 and vec2 must be vectors with ndim components
!                             uout(i) = vec1_idegfd1(i) vec2_idegfd2(i)-
!                                       vec2_idegfd1(i) vec1_idegfd2(i)
!                             The type of output vector must be defined in
!                             ISPEC (0=solution vector, >0 vector of special
!                             structure)
!                      51     U out is a map of the SEPRAN vector u1:, i.e.
!                             the SEPRAN vector u1 is transformed to a new
!                             structure defined by the parameters IPROB and
!                             ISPEC (=IVEC). If ISPEC=0 the output vector is of
!                             type 110, otherwise it is of type 115.
!                             Restriction: the new and old vector must
!                             correspond to the same number of nodes and the
!                             number of degrees of freedom per node must be
!                             the same for both vectors.
!                             So in fact Uout is a rearranged u1
!                      52     Map vector of type 110 or 115 onto vector of type
!                             116, i.e. map a vector defined per node onto
!                             a vector defined per element
!                             Line elements and connection elements are skipped
!                             At this moment the mapping is restricted to
!                             linear triangles only
!                 3  IDEGFD   Depending on the value of IDEGFD, the algebraic
!                             manipulations will be performed with respect to
!                             all degrees of freedom of the vectors (IDEGFD=0),
!                             with all internal degrees of freedom i.e. not
!                             with the essential boundary conditions
!                             (IDEGFD=-1), or with the IDEGFD th  degree of
!                             freedom only (IDEGFD>0).
!                             Default value: IDEGFD = 0
!                 4  IPOINT   Nodal point number.
!                             Default value: IPOINT = 1
!                 5  ISPEC    The use of ISPEC depends on the VALUE of ichoice.
!                             Default value: Depending on ichoice.
!                 6  ICHNRM   The use of ICHNRM depends on the VALUE of ichoice.
!                             Default value: ICHNRM = 3
!                 7  ISKIP    Depending on the value of ISKIP element groups
!                             must be skipped in the computation (ISKIP>20)
!                             or not (ISKIP=0).
!                             If ISKIP>20, the value of ISKIP denotes the
!                             starting position in array IINVEC where
!                             information concerning the elements to be skipped
!                             is stored.
!                             In that case it is necessary that the user has
!                             filled the positions IINVEC(ISKIP) , + . + ,
!                             ISKIP(ISKIP+ nelgrp -1), where  nelgrp
!                             denotes the number of element groups. Only
!                             element groups defined in the mesh are taken into
!                             account, so boundary element groups are not
!                             considered as special class. ISKIP in the range
!                             1<ISKIP<10 is not allowed.
!                             If IINVEC(ISKIP+ ielgrp -1) = 0, element group
!                             ielgrp  must not be skipped, if
!                             IINVEC(ISKIP+ ielgrp -1) = 1, the element group
!                             must be skipped.
!                             Default value: ISKIP = 0
!                 8  IDEGFD1  First degree of freedom in a point.
!                             Default value: IDEGFD1 = 1
!                 9  IDEGFD2  Second degree of freedom in a point.
!                             Default value: IDEGFD1 = 2
!                10  IDEGFD3  Third degree of freedom in a point.
!                             Default value: IDEGFD1 = 3
!                11  IPROB    Problem number
!                             Default value: IPROB = 1
!                12  IVEC1    Sequence number of input vector 1 (only used for
!                             error messages)
!                             Default value: IVEC1 = 1
!                13  IVEC2    Sequence number of input vector 2 (only used for
!                             error messages)
!                             Default value: IVEC2 = 2
!   invec1    i   Integer SEPRAN array of length 5 corresponding to a solution
!                 vector or a vector of special structure.
!                 For some possibilities of ichoice, u1 is not only an input
!                 vector but may be also output vector.
!                 In some cases INVEC1 corresponds to more than one vector
!                 ( u1 , u2 , + . + , u n  )
!                 In that case INVEC1 is supposed to be a two-dimensional
!                 array of size  5 x alpha  with  alpha>=n
!                 INVEC1(1:5,1: alpha ).
!                 In INVEC1( i,j ), ( i =1, 2, .. , 5) information about
!                 the  j th input vector must be stored (1<=j<=n ).
!                 This may be done for example by copying the contents of
!                 other SEPRAN arrays into INVEC1.
!   invec2    i   Integer SEPRAN array of length 5 corresponding to a solution
!                 vector or a vector of special structure.
!                 Restriction: At this momemt INVEC2 must have exactly the same
!                 structure as INVEC1 if it is used.
!                 The corresponding arrays must be both real or both complex.
!   iresvc    o   Integer SEPRAN array of length 5 in which the output of the
!                 algebraic manipulation will be stored.
!                 In  u out  only the  IDEGFD sup th  degree of freedom in the
!                 nodal points gets a value when IDEGFD>0.
!                 The other degrees of freedom are copied from  u1 .
!   kmesh     i   Standard SEPRAN array containing information of the mesh
!   kprob     i   Standard SEPRAN array containing information of the problem
!   rinvec    i   Double precision input and output array. The contents of
!                 RINVEC depend on the contents of IINVEC.
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer iconst(4), ichoice, idegfd(3), iskip, ipiskip
      double precision timebefore
      logical debug

!     debug          If true debug statements are carried out otherwise
!                    they are not
!     ichoice        Choice parameter indicating what type of function
!                    is chosen by the user.  ichoice  is derived from IINVEC(2)
!     iconst         In- and output integer array for subroutine TO0035
!                    containing constants for the manipulation depending
!                    on ichoice
!     idegfd         In this array the degrees of freedom that are requested for
!                    the special purpose must be stored. iconst is neccessary
!                    for subroutine TO0035 and contains a part of IINVEC
!     ipiskip        Starting address of array iskip
!     iskip          Indication if element groups must be skipped (>9) or
!                    not (0)
!     timebefore     Time at entrance of subroutine
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLMN         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERSETTIME      Get the CPU time at start of subroutine
!     PRININ         print 1d integer array
!     TO0035         Algebraic manipulations for vectors of type 110, 115
!                    and 116
!     TO0044         Translate output from TO0035 form to manvecbf form
!     TO0061         Check input and translate to form suitable for subroutine
!                    TO0035
!     TO0099         Map SEPRAN vector of one structure into another structure
!     TO0106         Algebraic manipulations for vectors of type 119
!     TO0107         Algebraic manipulations (min/max of vector)
!     TO0108         Algebraic manipulations (length of vector or extract one
!                    unknown)
!     TO0109         Algebraic manipulations (function of several vectors)
!     TO0110         Algebraic manipulations (special possibilities)
!     TOMANVEC44     Perform vector manipulations for subroutine manvec in case
!                    ichoice = 44/45
!     TOMANVEC46     Perform vector manipulations for subroutine manvec in case
!                    ichoice = 46
!     TOMANVEC48     Perform vector manipulations for subroutine manvec in case
!                    ichoice = 48
!     TOMANVEC49     Perform vector manipulations for subroutine manvec in case
!                    ichoice = 49
!     TOMANVEC52     Perform vector manipulations for subroutine manvec in case
!                    ichoice = 52
! **********************************************************************
!
!                       I/O
!
!    none
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'manvecbf' )
      if ( itime>0 ) call ersettime ( timebefore )

      debug = .false.

      if ( debug ) then

!     --- debug information

         write(irefwr,*) 'Debug information from manvecbf'
         call prinin ( iinvec, iinvec(1), 'iinvec' )
  1      format ( a, 1x, (10i6) )

      end if

!     --- Check input and translate to suitable form for subroutine TO0035

      write(6,*) 'manvecbf iinvec: ',iinvec(1),iinvec(2),iinvec(3)
      call to0061 ( iinvec, invec1, invec2, kmesh, kprob, iconst,
     +              ichoice, idegfd, iskip )

      if ( debug ) then

!     --- debug information

         call prinin ( iinvec, 11, 'iinvec' )
         write(irefwr,1) 'ichoice, iconst, idegfd(1), iskip',
     +                    ichoice, iconst(1), idegfd(1), iskip

      end if
      if ( ierror==0 ) then

!     --- Perform actual manipulations

         if ( ichoice==4 .or. ichoice==5 .or.
     +        ichoice>=15 .and. ichoice<=16 .and. iconst(1)==10 )
     +      then

!        --- Value in one point or maximum and corresponding node

            call to0110 ( ibuffr, buffer, ichoice, idegfd, invec1,
     +                    invec2, kmesh, kprob, rinvec, iconst )

         else if ( ichoice==11 .or. ichoice==12 ) then

!        --- Compute minimum or maximum of vector

            call to0107 ( ibuffr, buffer, ichoice, idegfd, invec1,
     +                    kmesh, kprob, rinvec )

         else if ( ichoice==14 .or. ichoice==21 .or. ichoice==26 )
     +      then

!        --- Compute length of vector or vector consisting of one unknown

            call to0108 ( ibuffr, buffer, ichoice, idegfd, invec1,
     +                    invec2, iresvc, kmesh, kprob, iconst )

         else if ( ichoice==17 ) then

!        --- vec3 is a function of vec1(.,i)

            call to0109 ( ibuffr, buffer, idegfd, invec1, iresvc,
     +                    kmesh, kprob, rinvec, iconst )

         else if ( ichoice==20 ) then

!        --- Map array of one structure onto another structure

            call to0099 ( ibuffr, buffer, invec1, iresvc, kmesh, kprob,
     +                    iconst, idegfd )

         else if ( ichoice==52 ) then

!        --- Map vector defined per node onto vector defined per element

            call tomanvec52 ( ibuffr, buffer, invec1, iresvc, kmesh,
     +                        kprob )

         else if ( ichoice==44 .or. ichoice==45 ) then

!        --- Map array of one structure onto another structure
!            Contract or expand with vector

            call tomanvec44 ( ibuffr, buffer, invec1, iresvc, kmesh,
     +                        kprob, iconst, ichoice, rinvec )

         else if ( ichoice>=46 .and. ichoice<=47 ) then

!        --- Subtract normal component

            call tomanvec46 ( ibuffr, buffer, invec1, iresvc, kmesh,
     +                        kprob, iconst, ichoice )

         else if ( ichoice==48 ) then

!        --- Create special vector with same structure as ivec1

            call tomanvec48 ( ibuffr, buffer, invec1, invec2, iresvc,
     +                        kmesh, kprob, iconst )

         else if ( ichoice==49 ) then

!        --- Create special vector with same structure as ivec1

            call tomanvec49 ( ibuffr, buffer, invec1, invec2, iresvc,
     +                        kmesh, kprob, iconst )

         else if ( invec1(2)==119 ) then

            call to0106 ( ibuffr, buffer, ichoice, idegfd, invec1,
     +                    invec2, iresvc, kmesh, rinvec, iconst )

         else

            if ( iskip<=0 ) then

!           --- iskip = 0, do not skip element groups

               ipiskip = 1

            else

!           --- iskip>0, skip element groups

               ipiskip = iskip

            end if
            call to0035 ( ibuffr, buffer, ichoice, idegfd, invec1,
     +                    invec2, iresvc, kmesh, kprob, rinvec, iconst,
     +                    iskip, iinvec(ipiskip) )

         end if

         if ( ierror/=0 ) go to 1000

!        --- Translate output from ICONST to IINVEC

         call to0044 ( iinvec, iconst )

      end if

1000  call erclmn ( 'manvecbf', timebefore )

      end
