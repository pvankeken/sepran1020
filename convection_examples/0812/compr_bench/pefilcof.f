c *************************************************************
c *   PEFILCOF
c * 
c *   Generic routine for specifying coefficients for the Stokes
c *   and heat equation in mantle convection applications.
c *   
c *   Updated to conform with element 903
c *   PvK 071705
c *************************************************************
      subroutine pefilcof(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      implicit none
      integer isol2(*),iuser(*),kmesh1(*),kmesh2(*),kprob1(*)
      integer kprob2(*),isol1(*),islol1(5,*),ichois,iheat(*),idens(*) 
      integer ivisc(*),isecinv(*)
      real*8 user(*)
      include 'ccc.inc'
 
      if (cyl) then
         call pefilcof_cyl(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      else 
         call pefilcof_cart(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      endif
    
      return
      end

c *************************************************************
c *   PEFILCOF_CYL
c *
c *   Modified for ichois=1 Stokes equation. Use modelv=103
c *
c *   PvK 981108/040900
c *************************************************************
      subroutine pefilcof_cyl(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      implicit none
      integer isol2(*),iuser(*),kmesh1(*),kmesh2(*),kprob1(*)
      integer kprob2(*),isol1(*),islol1(5,*),ichois,iheat(*),idens(*) 
      integer ivisc(*),isecinv(*)
      real*8 user(*),work(100),eps,f2

      integer iwork(4,100),itime,modelv,npoint,iincop(6),i
      integer iinpri(5),icurvs(5),iprhocp,k,nparm,j,inputcr(10)
      integer ishape1,kelmf1
      real*8 rinputcr(10),format
      character*80 text
      
      include 'c1visc.inc'
      include 'peparam.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'solutionmethod.inc'
      real*8 DONE
      parameter(DONE=1d0)

      if (compress) then
         if (.not.tala) then
            write(6,*) 'PERROR(pefilcof_cyl): not suited for'
            write(6,*) 'ALA just yet. '
            call instop
         endif
      endif

      if (ichois.eq.1) then
c        *** Momentum equation type 900 only

c        *** clear work/iwork
         do i=1,100
            do k=1,4
              iwork(k,i) = 0
            enddo
            work(i) = 0d0
         enddo
         do i=1,95
            iuser(5+i)=0
         enddo
         kelmf1 = kmesh1(20)
         ishape1 = kmesh1(kelmf1)


c        *** Coefficients for element 900
c        *** First five, integer info
         modelv=  103
         itime = 0
         iwork(1,1) = itime
         iwork(1,2) = modelv
         if (ishape1.eq.3) then
            iwork(1,3) = intrule900 
         else
            iwork(1,3) = intrule900 + 100*interpol900
         endif
         iwork(1,4) = icoor900
         iwork(1,5) = mcontv
         if (axi.and.icoor900.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif
         if (.not.axi.and.icoor900.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif

         if (isolmethod.eq.0) then
c           *** penaltyfunction parameter
            eps = 1d-6
         else
            eps = 0
         endif
         f2     = 1d0

c        *** 6: epsilon
         iwork(1,6) = 0
          work(6) = eps
c        *** 7: rho
         if (compress) then
            iwork(1,7) = 3
         else 
            iwork(1,7) = 0
             work(7)   = 1d0
         endif
c        *** 8: omega
c        *** 9: f1. First dof of third vector in islold
c        *** This contains only the thermal (+phase change) components
c        *** The chemical buoyancy is added in elm900_pvk.
         iwork(1,9) = 2003
         iwork(2,9) = 3
         iwork(3,9) = 1
c        *** 10: f2. Second dof of third vector in islold.
c        *** This contains only the thermal (+phase change) components
c        *** The chemical buoyancy is added in elm900_pvk.
         iwork(1,10) = 2003
         iwork(2,10) = 3
         iwork(3,10) = 2
c        *** 11: f3
         iwork(1,11) = 0
          work(11) = 0d0
c        *** 12: eta (specified through fnv003)
         iwork(1,12) = 0
         work(12)   = 1d0

         nparm=12


c        *** copy temperature in first dof of 2nd vector islold
         call cofcopy(0,islol1(1,2),isol2,kmesh1,kmesh2,
     v          kprob1,kprob2,DONE)
c        *** copy f1+f2 into 3rd vector in islold
         call f12copy(islol1(1,3),isol2,kmesh1,kmesh2,
     v          kprob1,kprob2)
         call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1)

       else if (ichois.eq.2) then


c *************************************************************
c *      TEMPERATURE EQUATION
c *************************************************************
         if (axi.and.icoorsystem.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (.not.axi.and.icoorsystem.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (Di.gt.0.and.itypv.ne.0) then
c              write(6,*) 'determine viscosity for temperature'
               call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,
     v                isol2,ivisc,isecinv,iuser,user)
         endif
         call coef800(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                ivisc,iheat,idens,iuser,user)

       else if (ichois.eq.3) then

         do i=6,100
            iuser(i)=0
         enddo
c        *** Element group 1
         iuser(6)=10
c        *** Boundary element group 1
c        iuser(7)=30

c        *** itime, modelv, numint, icoor, mcont
         iuser(10)=0
         iuser(11)=1
         iuser(12)=intrule
         iuser(13)=1
         iuser(14)=0
c        eps rho omega f1 f2 f3 eta 
         iuser(15)=-6
         iuser(16)=-7
         iuser(17)=0
         iuser(18)=0
         iuser(19)=0
         iuser(20)=0
         iuser(21)=-7
          user(6)=1d-6
          user(7)=1d0

c        *** Boundary element information
c        iuser(30)=2
c        iuser(31)=0
c        iuser(32)=intrule
        

       endif

       return
       end

c *************************************************************
c *   PEFILCOF_CART
c *
c *   Modified for ichois=1 Stokes equation. Use modelv=103
c *
c *   PvK 981108/040900
c *************************************************************
      subroutine pefilcof_cart(ichois,kmesh1,kmesh2,kprob1,kprob2,
     v                    isol1,isol2,islol1,iheat,
     v                    idens,ivisc,isecinv,iuser,user) 
      implicit none
      integer isol2(*),iuser(*),kmesh1(*),kmesh2(*),kprob1(*)
      integer kprob2(*),isol1(*),islol1(5,*),ichois,iheat(*),idens(*) 
      integer ivisc(*),isecinv(*)
      real*8 user(*),work(100),eps,f2

      integer iwork(4,100),itime,modelv,npoint,iincop(6),i
      integer iinpri(5),icurvs(5),iprhocp,k,nparm,j,inputcr(10)
      real*8 rinputcr(10),format
      character*80 text
      integer ishape1,kelmf1
      
      include 'c1visc.inc'
      include 'peparam.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'solutionmethod.inc'
      real*8 DONE
      parameter(DONE=1d0)

      if (ichois.eq.1) then
c        *** Momentum equation type 900 only

c        *** clear work/iwork
         do i=1,100
            do k=1,4
              iwork(k,i) = 0
            enddo
            work(i) = 0d0
         enddo
         do i=1,95
            iuser(5+i)=0
         enddo


c        *** Coefficients for element 900
c        *** First five, integer info
         kelmf1 = kmesh1(20)
         ishape1 = kmesh1(kelmf1)
         modelv=  103
         itime = 0
         iwork(1,1) = itime
         iwork(1,2) = modelv
         if (ishape1.eq.3) then
            iwork(1,3) = intrule900 
         else
            iwork(1,3) = intrule900 + 100*interpol900
         endif
         iwork(1,4) = icoor900
         iwork(1,5) = mcontv
         if (axi.and.icoor900.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif
         if (.not.axi.and.icoor900.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            call instop
         endif

         if (isolmethod.eq.0) then
c           *** penaltyfunction parameter
            eps = 1d-6
         else
            eps = 0
         endif
         f2     = 1d0

c        *** 6: epsilon
         iwork(1,6) = 0
          work(6) = eps
c        *** 7: rho
         if (compress) then
            iwork(1,7) = 3
         else 
            iwork(1,7) = 0
             work(7)   = 1d0
         endif
c        *** 8: omega
c        *** 10: f2. First dof of third vector in islold.
c        *** This contains only the thermal (+phase change) components
c        *** The chemical buoyancy is added in elm900_pvk.
         iwork(1,10) = 2003
         iwork(2,10) = 3
         iwork(3,10) = 2
c        *** 11: f3
         iwork(1,11) = 0
          work(11) = 0d0
c        *** 12: eta (specified through fnv003)
         iwork(1,12) = 0
         work(12)   = 1d0

         nparm=12


c        *** copy temperature in first dof of 2nd vector islold
         call cofcopy(0,islol1(1,2),isol2,kmesh1,kmesh2,
     v        kprob1,kprob2,DONE)
c        *** copy f2 into 3rd vector in islold
         call f2copy(islol1(1,3),isol2,isol1,kmesh1,kmesh2,
     v               kprob1,kprob2)
         call fil103(1,1,iuser,user,kprob1,nparm,iwork,work,kmesh1)

       else if (ichois.eq.2) then


c *************************************************************
c *      TEMPERATURE EQUATION
c *************************************************************
         if (axi.and.icoorsystem.ne.1) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for axisymmetric geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (.not.axi.and.icoorsystem.ne.0) then
            write(6,*) 'PERROR(pefilcof): make sure icoor is '
            write(6,*) 'correct for cylindrical geometry'
            write(6,*) 'axi = ',axi
            write(6,*) 'icoorsystem = ',icoorsystem
            call instop
         endif
         if (Di.gt.0.and.itypv.ne.0) then
c              write(6,*) 'determine viscosity for temperature'
               call coefvis(kmesh1,kprob1,isol1,kmesh2,kprob2,
     v                isol2,ivisc,isecinv,iuser,user)
         endif
         call coef800(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,
     v                ivisc,iheat,idens,iuser,user)

       else if (ichois.eq.3) then

         do i=6,100
            iuser(i)=0
         enddo
c        *** Element group 1
         iuser(6)=10
c        *** Boundary element group 1
c        iuser(7)=30

c        *** itime, modelv, numint, icoor, mcont
         iuser(10)=0
         iuser(11)=1
         iuser(12)=intrule
         iuser(13)=1
         iuser(14)=0
c        eps rho omega f1 f2 f3 eta 
         iuser(15)=-6
         iuser(16)=-7
         iuser(17)=0
         iuser(18)=0
         iuser(19)=0
         iuser(20)=0
         iuser(21)=-7
          user(6)=1d-6
          user(7)=1d0

c        *** Boundary element information
c        iuser(30)=2
c        iuser(31)=0
c        iuser(32)=intrule
        

       endif

       return
       end

c *************************************************************
c *   cofcopy
c *
c *   Copy first dof of isol2 into first dof of isol1
c *
c *   Note: as of 1995 or so, solution vectors are not renumbered
c *   anymore...
c *************************************************************
      subroutine cofcopy(ichois,isol1,isol2,kmesh1,kmesh2,
     v               kprob1,kprob2,factor)
      implicit none
      integer ichois,isol1(5),isol2(5),kmesh1(*), kmesh2(*)
      integer kprob1(*),kprob2(*)
      real*8 factor
 
      integer ibuffr
      common ibuffr(1) 
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      real*8 buffer(1)
      equivalence( ibuffr(1),buffer(1))
      include 'ccc.inc'

      integer indprf1,indprf2,nunkp,ikmeshc
      integer ikmeshi,nelem1,ipuslol1
      integer ipusol1,ipusol2,iniget,inidgt,npoint1,npoint2
      integer nunkp1,nunkp2,ipkprf1,nphys1

      if (kprob1(21).ne.0.and.kprob2(21).ne.0) then
         write(6,*) 'PERROR(cofcopy): kprobh <> 0!' 
         write(6,*) ' Should not happen anymore!'
         call instop
      endif


      indprf1 = kprob1(19)
      indprf2 = kprob2(19) 
      nunkp1 = kprob1(4)
      nphys1 = kprob1(33)
      nunkp2 = kprob2(4)
      npoint1 = kmesh1(8)      
      nelem1 = kmesh1(9)      
      npoint2 = kmesh2(8)      
      if (periodic) nelem1 = kmesh1(kmesh1(21))
c     write(6,*) 'nphys1: ',nphys1,nunkp1,nunkp2
 
      if (indprf1.ne.0) then
c        *** Variable number of dofs: probably ishape=7 for
c        *** integrated method. Use only first 2 dofs for copy
         call ini050(indprf1,'cofcopy: indprf1')
         ipkprf1 = iniget(indprf1)
      endif

      if (indprf2.ne.0) then
         write(6,*) 'PERROR(cofcopy): not suited for variable numbers '
         write(6,*) '  of dofs in heat equation'
         write(6,*) 'indprf2: ',indprf2
         call instop 
      endif

      if (indprf1.eq.0) then
        if ((nunkp1.ne.2.and.nunkp1.ne.3).or.nunkp2.ne.1) then
           write(6,*) 'PERROR(cofcopy): number of dofs incorrect' 
           write(6,*) 'nunkp1 should be 2 or 3, but is: ',nunkp1
           write(6,*) 'nunkp2 should be 1     , but is: ',nunkp2
           call instop
        endif
      endif

      call ini050(isol1(1),'cofcopy: isol1')
      call ini050(isol2(1),'cofcopy: isol2')

      ipusol1 = inidgt(isol1(1))
      ipusol2 = inidgt(isol2(1))

      call cofcop01(buffer(ipusol1),buffer(ipusol2),
     v              indprf1,ibuffr(ipkprf1),
     v              npoint1,nunkp1,nunkp2,factor) 

      return
      end

c *************************************************************
c *   COFCOP01
c *   Copy temperature from sol2 to sol1. Sol1 is used in 
c *   the specification of the coefficients.
c *   PvK 9906
c *************************************************************
      subroutine cofcop01(sol1,sol2,indprf1,kprobf1,
     v              npoint,nunkp1,nunkp2,factor)
      implicit none
      real*8 sol1(*),sol2(*)
      integer npoint,nunkp1,nunkp2,indprf1,kprobf1(*)
      real*8 factor
      integer i,j,j1,j2

      if (indprf1.eq.0) then
        do i=1,npoint
           j1 = (i-1)*nunkp1 + 1
           sol1(j1) = factor*sol2(i)
        enddo
      else 
        do i=1,npoint
           j1 = kprobf1(i) + 1
           sol1(j1) = factor*sol2(i)
        enddo
      endif

      return
      end
            
c *************************************************************
c *   f12copy
c *
c *   Copy f1 and f2 into into first/second dof of isol1
c *
c *   Updated to conform to element 903
c *************************************************************
      subroutine f12copy(isol1,isol2,kmesh1,kmesh2,kprob1,kprob2)
      implicit none
      integer isol1(5),isol2(5),kmesh1(*),kmesh2(*)
      integer kprob1(*),kprob2(*)
 
      integer ibuffr
      common ibuffr(1) 
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      real*8 buffer(1)
      equivalence( ibuffr(1),buffer(1))
      include 'ccc.inc'

      integer nunkp,ikmeshc
      integer ikmeshi,nelem1
      integer ipusol1,ipusol2,iniget,inidgt,npoint1,npoint2
      integer nunkp1,nunkp2,ipkprf1,nphys1,kelmi,ipkelmi,indprf1

      npoint1 = kmesh1(8)      
      nelem1 = kmesh1(9)      
      if (periodic) nelem1 = kmesh1(kmesh1(21))
      kelmi = kmesh1(23)
 
      call ini050(isol1(1),'f12copy: isol1')
      call ini050(isol2(1),'f12copy: isol2')
      ipusol1 = inidgt(isol1(1))
      ipusol2 = inidgt(isol2(1))

      nunkp1 = kprob1(4)
      indprf1 = kprob1(19)
      if (indprf1.ne.0) then
         call ini070(indprf1)
         ipkprf1 = iniget(indprf1)
      endif

      call ini050(kelmi,'f12copy: kelmi')
      ipkelmi = inidgt(kelmi)

      call f12cop01(buffer(ipusol1),buffer(ipusol2),
     v              indprf1,ibuffr(ipkprf1),
     v              buffr(ipkelmi),npoint1,nunkp1)

      return
      end

c *************************************************************
c *   F12COP01
c *   Copy f1+f2 into 1st and 2nd dof of sol1. Sol1 is used in 
c *   the specification of the coefficients.
c *   sol2 contains the temperature.
c *   PvK 050200
c *************************************************************
      subroutine f12cop01(sol1,sol2,indprf1,kprobf1,coor,npoint,nunkp)
      implicit none
      real*8 sol1(*),sol2(*),coor(2,*)
      integer npoint,indprf1,kprobf1(*),nunkp
      real*8 x,y,pefbuoy1
      integer i,j,j1,j2

      if (indprf1.eq.0) then
        do i=1,npoint
           x=coor(1,i)
           y=coor(2,i)
           j1 = (i-1)*2 + 1
           sol1(j1) = pefbuoy1(1,x,y,sol2(i))
           j1 = j1+1
           sol1(j1) = pefbuoy1(2,x,y,sol2(i))
c          write(6,'(''F: '',4f12.3)') x,y,sol1(j1-1),sol1(j1)
        enddo
      else
        do i=1,npoint
           x=coor(1,i)
           y=coor(2,i)
           j1 = kprobf1(i) + 1 
           sol1(j1) = pefbuoy1(1,x,y,sol2(i))
           j1 = j1+1
           sol1(j1) = pefbuoy1(2,x,y,sol2(i))
        enddo
      endif

      return
      end

            
c *************************************************************
c *   f2copy
c *
c *   Copy f2 into into first/second dof of islol1
c *   For Cartesian grids.
c *
c *   Note: as of 1995 or so, solution vectors are not renumbered
c *   anymore...
c *************************************************************
      subroutine f2copy(islol1,isol2,isol1,kmesh1,kmesh2,kprob1,kprob2)
      implicit none
      integer isol1(5),isol2(5),kmesh1(*),kmesh2(*),islol1(5)
      integer kprob1(*),kprob2(*)
 
      integer ibuffr
      common ibuffr(1) 
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
      real*8 buffer(1)
      equivalence( ibuffr(1),buffer(1))
      include 'ccc.inc'

      integer nunkp,ikmeshc
      integer ikmeshi,nelem1,ipuslol1
      integer ipusol1,ipusol2,iniget,inidgt,npoint1,npoint2
      integer nunkp1,nunkp2,nphys1,kelmi,ipkelmi
      integer indprf1,ipkprf1

      npoint1 = kmesh1(8)      
      nelem1 = kmesh1(9)      
      if (periodic) nelem1 = kmesh1(kmesh1(21))
      kelmi = kmesh1(23)
 
      call ini050(isol1(1),'f2copy: isol1')
      call ini050(islol1(1),'f2copy: islol1')
      call ini050(isol2(1),'f2copy: isol2')
      ipusol1 = inidgt(isol1(1))
      ipuslol1 = inidgt(islol1(1))
      ipusol2 = inidgt(isol2(1))

      nunkp1 = kprob1(4)
      indprf1 = kprob1(19)
c     write(6,*) 'indprf1 = ',indprf1
      if (indprf1.ne.0) then
c        ** variable number of dofs; probably element 903
         call ini050(indprf1,'f2copy: kprob F')
         ipkprf1 = iniget(indprf1)
      endif

      call ini050(kelmi,'f2copy: kelmi')
      ipkelmi = inidgt(kelmi)

      call f2cop01(buffer(ipuslol1),buffer(ipusol2),buffr(ipusol1),
     v              indprf1,ibuffr(ipkprf1),
     v              buffr(ipkelmi),npoint1,nunkp1)

      return
      end

c *************************************************************
c *   F2COP01
c *   Copy f2 into 1st dof of slol1. Slol1 is used in 
c *   the specification of the coefficients.
c *   sol2 contains the temperature.
c *   sol1 contains the velocity and pressure (used in ALA).
c *   PvK 050200/082708
c *************************************************************
      subroutine f2cop01(slol1,sol2,sol1,indprf1,kprobf1,coor,
     v            npoint,nunkp1)
      implicit none
      real*8 sol1(*),sol2(*),coor(2,*),slol1(*)
      integer npoint,indprf1,kprobf1(*),nunkp1
      real*8 x,y,pefbuoy,pressure
      integer i,j,j1,j2

c     write(6,*) 'f2cop01: indprf1 = ',indprf1
      if (indprf1.eq.0) then
c       *** number of degrees of freedom is constant
        do i=1,npoint
           x=coor(1,i)
           y=coor(2,i)
c          *** f1 sits in position of first dof in slol1
           j1 = (i-1)*nunkp1 + 1
           slol1(j1) = 0d0
c          *** f2 sits in position of second dof in slol1
           j1 = j1+1
c          *** pressure is in the third dof in sol1.
           pressure = sol1(j1+1)
           slol1(j1) = pefbuoy(x,y,sol2(i),pressure)
c          write(6,10) x,y,sol2(i),pressure,slol1(j1)
        enddo
      else
c       *** number of degrees of freedom is not constant; use kprobf
        do i=1,npoint
           x=coor(1,i)
           y=coor(2,i)
           j1 = kprobf1(i) + 1
           slol1(j1) = 0d0
           j1 = j1+1
           pressure = sol1(j1+1)
           slol1(j1) = pefbuoy(x,y,sol2(i),pressure)
c          write(6,11) x,y,sol2(i),pressure,slol1(j1)
        enddo
      endif
10    format('pefbuoy constant: ',5f15.3)
11    format('pefbuoy not constant: ',5f15.3)
           
      return
      end

c *************************************************************
c *   COEF800
c *
c *   Define coefficients for heat equation: implementation of
c *   phase changes through the extended Boussinesq approximation
c *
c *   PvK 950222 (modified from AB)
c *   PvK 970610 For incorporation in PHCYL
c *************************************************************
      subroutine coef800(kmesh1,kprob1,isol1,kmesh2,kprob2,isol2,  
     v    ivisc,iheat,idens,iuser,user)
      implicit none
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (buffr(1),ibuffr(1))

      integer kmesh2(*),kprob2(*),kmesh1(*),kprob1(*)
      integer iuser(*),isol1(*),isol2(*),ivisc(*),iheat(*),idens(*) 
      real*8 user(*)
 
      real*8 DZERO,DONE
      parameter (DZERO = 0d0, DONE = 1d0)
      include 'cpephase.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'depth_thermodyn.inc'
      include 'ccc.inc'
      include 'petrac.inc'
 
      integer npoint,lniuser,lnuser,nelgrp2,ikelmi
      integer ihorvel,ivervel,ibeta,irhocp,iright,itemp
      integer i,ichois,icheld,ix,jdegfd,ivec,iphi(5),ielhlp
      integer iinvec(5),iresvc,iinder(10),ivisdip(5)
      real*8 rinvec(5),phimin,phimax,phiav,funccf,adiabat
      integer iuser_dum,inidgt

      save ivisdip,iphi

c     **** prepare viscous dissipation if necessary - make sure to do this 
c     **** before filling arrays iuser/user!
      if (Di.gt.0) then
c        *** Calculate viscous dissipation 
         call phifromgrad(kmesh1,kprob1,isol1,ivisdip)
      endif

      npoint = kmesh2(8)
      lniuser = iuser(1)
      lnuser  =  user(1)
      do i=6,lniuser
         iuser(i) = 0
      enddo
      do i=2, lnuser
         user(i) = DZERO
      enddo

c     *** define:   a11,a12,a22,u,v,b,f
      nelgrp2 = kmesh2(5)
      iuser(2)=(nelgrp2+numnat800)
      if (nelgrp2 .ne. 1 .and. .not.periodic) then
         write(6,*) 'PERROR(coef800): nelgrp2 > 1: ',nelgrp2
         write(6,*) 'but periodic .eq. ',periodic
         call instop
      end if
      if (nelgrp2 .ne. 2 .and. periodic) then
         write(6,*) 'PERROR(coef800): nelgrp2 <> 2: ',nelgrp2
         write(6,*) 'but periodic .eq. ',periodic
         call instop
      end if
      if (numnat800.lt.0.or.numnat800.gt.1) then
         write(6,*) 'PERROR(coef800): numnat800 <> 0,1: ',numnat800
         call instop
      endif

 
c     *** Element group 1
      iuser(6) = 8
c     **** if needed:
c     *** Boundary element group 1
c     write(6,*) 'numnat800 : ',numnat800
      if (numnat800.eq.1) iuser(7) = 40

c     *** pointers to nodal point information on: u,w,beta,f,rhocp,T
      ihorvel = 10
      ivervel = 10+npoint
      ibeta   = 10+2*npoint
      iright  = 10+3*npoint
      irhocp  = 10+4*npoint
      itemp   = 10+5*npoint
      if (10+6*npoint-1.gt.lnuser) then
         write(6,*) 'PERROR(coef800): Length of array user too small'
         write(6,*) 'Declared length: ',lnuser
         write(6,*) 'Required length: ',10+6*npoint-1
         call instop
      endif

c     *** type integer info.
c     ( 1) - not yet used
      iuser(8) = 0
c     ( 2) - type of upwinding 
      iuser(9) = metupw
c     ( 3) - type of numerical integration
      iuser(10) = intrule
c     ( 4) - type of coordinate system (0=Cartesian; 1=Axisymmetric)
c     write(6,*) 'icoorsystem: ',icoorsystem
      iuser(11) = icoorsystem
c     ( 5) - not yet used
      iuser(12) = 0

c     *** type real info.

c     (6-11) Thermal conductivity
      if (idiftype.eq.0) then
          iuser(13) = -6
          iuser(16) = -6
      else
c         *** function of depth
c         write(6,*) 'diffusivity through funccf(4)',
c    v       funccf(4,r1,0d0,0d0),funccf(4,r2,0d0,0d0)
          iuser(13) = 4
          iuser(16) = 4
      endif
      user(6)=1d0

c     (12) - horizontal advection velocity 
      iuser(19) = 2001
      iuser(20) = ihorvel
c     (13) - vertical advection velocity 
      iuser(21) = 2001
      iuser(22) = ivervel
c     (14) - u 3, ignored in 2-D case
      iuser(23) = 0

c     (15) - beta, adiab compression term 
      iuser(24) = 2001
      iuser(25) = ibeta

c     (16) - heat production
      iuser(26) = 2001
      iuser(27) = iright

c     (17) - rho*cp 
      iuser(28) = 2001
      iuser(29) = irhocp

c     (18-20) - not yet used - corresp. entries of iuser are zero

c     (40-52) Boundary element information; type 2
c     *** type, not used, integration rule, coordinate system, not used
      if (numnat800.eq.1) then
        iuser(40) = 2
        iuser(41) = 0
        iuser(42) = intrule
        iuser(43) = icoorsystem
        iuser(44) = 0
c       *** sigma 
        iuser(45) = 0
c       *** h
        iuser(46) = -8
         user(8) = heat_flux_in
c       (6-11) Thermal conductivity
        if (idiftype.eq.0) then
            iuser(47) = -6
            iuser(48) = 0
            iuser(49) = 0
            iuser(50) = -6
            iuser(51) = 0
            iuser(51) = 0
        else
c           *** function of depth
            iuser(47) = 4
            iuser(48) = 0
            iuser(49) = 0
            iuser(50) = 4
            iuser(51) = 0
            iuser(51) = 0
        endif
      endif
c       *** End of boundary element input

c     *** Prepare info on coefficients per nodal point in user
c     *** Copy information into user:
c     *** Velocity components
      call pecopy(2,isol1,user,kmesh1,kprob1,ihorvel,DONE)
      call pecopy(3,isol1,user,kmesh1,kprob1,ivervel,DONE)
c     do i=1,npoint
c        write(6,'(''u,v: '',i5,2f8.3)') i,user(ihorvel+i-1),
c    v           user(ivervel+i-1)
c     enddo
c     *** Temperature
      call pecopy(0,isol2,user,kmesh1,kprob2,itemp,DONE)


c *************************************************************
c *   *** Copy viscous dissipation into user, multiply by Di/Ra
c *   *** (or initialize if Di=0)
c *************************************************************
      call ini050(kmesh1(23),'coef800: coor')
      ikelmi = inidgt(kmesh1(23))

      if (iqtype.ne.0) then
c        *** Determine radiogenic heatproduction in nodal points
         call findheatgen(kmesh1,kprob1,coormark,chemmark,
     v        idens,iheat) 
      endif
c     *** copy heat production and viscous dissipation into user
      call pecophi(ivisdip,user,iright,npoint,DiRa,
     v      iqtype,buffr(ikelmi),ivisc,iheat)

      if (cyl) then
       call coef8_2_cyl(npoint,user(ihorvel),user(ivervel),user(ibeta),
     v             user(iright),user(irhocp),user(itemp),buffr(ikelmi)) 
      else
       call coef8_2_cart(npoint,user(ihorvel),user(ivervel),user(ibeta),
     v             user(iright),user(irhocp),user(itemp),buffr(ikelmi)) 
      endif
   

      return
      end

c *************************************************************
c *   COEF8_2_cyl
c *
c *   Modified for cylindrical/axisymmetric version
c *       Change y->r
c *       Change vervel -> radial velocity
c *   PvK 1997/230400
c *************************************************************
      subroutine coef8_2_cyl(N,horvel,vervel,beta,right,rhocp,temp,coor)
      implicit none 
      integer N
      real*8 horvel(N),vervel(N),beta(N),right(N),rhocp(N)
      real*8 coor(2,N),temp(N)
      include 'cpephase.inc'
      real*8 tratio,bigGamma,dGdpi,alphas,rhocps,rho
      real*8 x,y,funccf,prespi,glRbRa,gl2RbRaDi,r,z
      integer i,iph
      dimension glRbRa(10),gl2RbRaDi(10)
      include 'ccc.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)
      real*8 th,cost,radial_velocity,yold,sint
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'
      include 'pecof900.inc'
      
      if (nph.gt.2) then
         write(6,*) 'PERROR(coef8_2): nph > 2'
         write(6,*) 'nph = ',nph
         call instop
      endif
      do iph=1,nph
        glRbRa(iph) = gamma(iph)*Rb(iph)/Ra 
        gl2RbRaDi(iph) = gamma(iph)*Di*glRbRa(iph)
        if (phdz(iph).le.0.or.phdz(iph).ge.1) then
           write(6,*) 'PERROR(coef8_2): phdz <= 0 or phdz >= 1'
           write(6,'(''phdz('',i2,'') = '',f12.3)') iph,phdz(iph)
           call instop
        endif
      enddo
    
      yold=99
      do i=1,N
         x = coor(1,i)
         y = coor(2,i)
         r = sqrt(x*x+y*y)
         cost = y/r
         sint = x/r
         if (x.ge.0) then
            th = acos(cost)
         else
            th = 2*pi - acos(cost)
         endif
         radial_velocity = horvel(i)*sin(th) + vervel(i)*cos(th)

c        radial_velocity = horvel(i)*sint+vervel(i)*cost
         dGdpi = 0d0
         if (ialphatype.eq.1) then
            alphas = funccf(5,x,y,z)
         else
            alphas = 1d0
         endif
         if (compress) then
            rho = funccf(3,x,y,z)
            rhocps = rho
         else
            rho    = 1d0
            rhocps = 1d0
         endif
         do iph=1,nph
c          *** make sure r2-r1=1 (for cylindrical/axisymmetric case)
           prespi = (r2-r) - phz0(iph) - gamma(iph)*(temp(i)-pht0(iph))
           tratio = temp(i) + Ts_dim/DeltaT_dim
           bigGamma = 0.5 + 0.5*tanh(prespi/phdz(iph))
c          dGdpi = dGdpi + 2d0/phdz(iph)*bigGamma*(1-bigGamma)
           dGdpi = 2d0/phdz(iph)*bigGamma*(1-bigGamma)
c          if (y.ne.yold.and.iph.eq.1) then
c          write(6,'(''Prespi: '',i5,6f8.3)') iph,y,1-y-phz0(iph),
c    v        prespi,dGdpi,bigGamma,temp(i)
c           yold=y
c          endif

           alphas = alphas + glRbRa(iph)*dGdpi
           rhocps = rhocps + gl2RbRaDi(iph)*dGdpi*tratio
         enddo
c        *** Note: use formulation from Christensen and Yuen, 1985

         beta(i)   = rho*alphas*Di*radial_velocity
c        *** contribution of T0 to the right hand side vector
         right(i)  = right(i)  - 
     v           rho*Di*alphas*radial_velocity* Ts_dim/DeltaT_dim
         rhocp(i)  = rhocps
c        if (nint(th*1000).eq.1571) then
c          write(6,'(10f8.2)') x,y,r,th,beta(i),right(i),rhocp(i),
c    v        alphas,radial_velocity,horvel(i)
c        endif
      enddo
          
      return
      end

c *************************************************************
c *   COEF8_2_CART
c *
c *   PvK 1997/230400
c *************************************************************
      subroutine coef8_2_cart(N,horvel,vervel,beta,
     v                 right,rhocp,temp,coor)
      implicit none 
      integer N
      real*8 horvel(N),vervel(N),beta(N),right(N),rhocp(N)
      real*8 coor(2,N),temp(N)
      include 'cpephase.inc'
      real*8 tratio,bigGamma,dGdpi,alphas,rhocps,rho
      real*8 x,y,funccf,prespi,glRbRa,gl2RbRaDi,r,z
      integer i,iph
      dimension glRbRa(10),gl2RbRaDi(10)
      include 'ccc.inc'
      real*8 pi
      parameter(pi=3.1415926 535898)
      real*8 th,cost,radial_velocity,yold,sint,adiabat,d2adiabatdz2
      include 'depth_thermodyn.inc'
      include 'dimensional.inc'
      include 'pecof900.inc'
      
      if (nph.gt.2) then
         write(6,*) 'PERROR(coef8_2): nph > 2'
         write(6,*) 'nph = ',nph
         call instop
      endif
      do iph=1,nph
        if (Ra.eq.0) then
           glRbRa(iph)=0
        else
           glRbRa(iph) = gamma(iph)*Rb(iph)/Ra 
        endif
        gl2RbRaDi(iph) = gamma(iph)*Di*glRbRa(iph)
        if (phdz(iph).le.0.or.phdz(iph).ge.1) then
           write(6,*) 'PERROR(coef8_2): phdz <= 0 or phdz >= 1'
           write(6,'(''phdz('',i2,'') = '',f12.3)') iph,phdz(iph)
           call instop
        endif
      enddo
    
      yold=99
      do i=1,N
         x = coor(1,i)
         y = coor(2,i)
         radial_velocity = vervel(i)
         dGdpi = 0d0
         if (ialphatype.eq.1) then
            alphas = funccf(5,x,y,z)
         else
            alphas = 1d0
         endif
         if (compress) then
            rho = funccf(3,x,y,z)
            rhocps = rho
         else
            rho    = 1d0
            rhocps = 1d0
         endif
         do iph=1,nph
c          *** make sure r2-r1=1 (for cylindrical/axisymmetric case)
           prespi = (1-y) - phz0(iph) - gamma(iph)*(temp(i)-pht0(iph))
           tratio = temp(i) + Ts_dim/DeltaT_dim
           bigGamma = 0.5 + 0.5*tanh(prespi/phdz(iph))
           dGdpi = 2d0/phdz(iph)*bigGamma*(1-bigGamma)

           alphas = alphas + glRbRa(iph)*dGdpi
           rhocps = rhocps + gl2RbRaDi(iph)*dGdpi*tratio
         enddo
c        *** Note: use formulation from Christensen and Yuen, 1985

         beta(i)   = rho*alphas*Di*radial_velocity
c        *** add contribution of compressional work to the right hand side vector
c        *** Take into account T0 if necessary
           right(i)  = right(i)  - 
     v           rho*Di*alphas*radial_velocity* 
     v           (Tso_dim-adiabat(1d0))/DeltaT_dim
         if (addnabla2Tbar) then
c           *** add effect of diffusion along adiabat
            right(i)  = right(i)  + d2adiabatdz2(y)
         endif
         rhocp(i)  = rhocps
c        if (nint(th*1000).eq.1571) then
c          write(6,'(10f8.2)') x,y,r,th,beta(i),right(i),rhocp(i),
c    v        alphas,radial_velocity,horvel(i)
c        endif
      enddo
          
      return
      end

