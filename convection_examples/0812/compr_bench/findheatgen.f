c *************************************************************
c *   FINDHEATGEN
c *
c *   Determine radiogenic heatproduction in nodal points
c *
c *   kmesh,kprob  i    Mesh and problem definition
c *   coormark     i    positions of the tracers
c *   chemmark     i    Info about chemistry in the tracers
c *   idens        o    vector of special structure containing 
c *                     density of the tracers in the nodal points
c *                     (only if iqtype=3)
c *   iheat        o    vector of special structure containing
c *                     heatprodcction in the nodal points
c *
c *   PvK 080600
c *************************************************************
      subroutine findheatgen(kmesh,kprob,coormark,chemmark,
     v           idens,iheat) 
      implicit none
      integer kmesh(*),kprob(*),idens(*),iheat(*)
      real*8 coormark(2,*),chemmark(*)

      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1) 
      equivalence(buffr(1),ibuffr(1))

      include 'ccc.inc'
      include 'pecof800.inc'
      include 'pecof900.inc'
      include 'SPcommon/ctimen'
      include 'degas.inc'
      integer ipheat,ipdens,inidgt,npoint,nelem,i
      integer ikelmc,ikelmi,iniget
      real*8 heatbse

      npoint = kmesh(8)
      nelem = kmesh(9)
      if (periodic) nelem = kmesh(kmesh(21))

c     *** initiate or create iheat
      if (iheat(1).eq.0) then
         write(6,*) 'PERROR(findheatgen): iheat does not exist'
         call instop
      endif
      if (iheat(2).ne.115) then
         write(6,*) 'PERROR(findheatgen): vector heat exists'
         write(6,*) 'but is not of type VECTR: ',iheat(2)
         call instop
      endif
      if (iheat(4).ne.1) then
         write(6,*) 'PERROR(findheatgen): vector heat exists'
         write(6,*) 'and is of type VECTOR'
         write(6,*) 'but IVEC is wrong: ',iheat(4)
         call instop
      endif
      if (iheat(5).ne.npoint) then
         write(6,*) 'PERROR(findheatgen): vector heat exists'
         write(6,*) 'and is of type VECTOR with IVEC=1'
         write(6,*) 'but NUMDEGFD is not NPOINT: ',iheat(5)
         call instop
      endif
  
      call ini050(iheat(1),'findheatgen: heat')

      call ini050(kmesh(23),'findheatgen: coordinates')
      call ini050(kmesh(17),'findheatgen: nodal points')

      ikelmc = iniget(kmesh(17))
      ikelmi = inidgt(kmesh(23))
      ipheat = inidgt(iheat(1))

      if (iqtype.eq.0) then
c        *** heatproduction is zero
          do i=1,npoint
             buffr(ipheat+i-1)=0d0
          enddo
      else if (iqtype.eq.1) then
c        *** heatproduction is constant
         do i=1,npoint
             buffr(ipheat+i-1)=q_layer(1)
         enddo
      else if (iqtype.eq.2) then
c        *** heatproduction is layer dependent
         call findheat01(buffr(ikelmi),q_layer,buffr(ipheat),npoint) 
      else if (iqtype.eq.3) then
c        *** determine tracer density and corresponding heatproduction
         call tracheat(kmesh,kprob,coormark,chemmark,q_layer,
     v         idens,iheat) 
      else if (iqtype.eq.4) then
c        *** BSE time-dependent heatproduction 
         call heatfromthuk(t*tscale,heatbse)
         do i=1,npoint
            buffr(ipheat+i-1) = heatbse
         enddo
      endif   

      if (qwithrho.or.mcontv.ge.10) then
c        *** multiply with local density (<rho>==1)
         call findheat02(buffr(ikelmi),buffr(ipheat),npoint)
      endif

      return
      end

c *************************************************************
c *   FINDHEAT01
c *   Layer dependent heatproduction
c *   PvK 080600
c ************************************************************* 
      subroutine findheat01(coor,q_layer,heat,npoint) 
      implicit none
      real*8 coor(2,*),q_layer(*),heat(*)
      integer npoint
      real*8 x,y
      integer ival,ivalfind1,i

      do i=1,npoint
         x = coor(1,i)
         y = coor(2,i)
         ival = ivalfind1(x,y)
         heat(i) = q_layer(ival)
      enddo
 
      return
      end

c *************************************************************
c *   FINDHEAT02
c *   PvK 080700
c *************************************************************
      subroutine findheat02(coor,heat,npoint)
      implicit none
      real*8 coor(2,*),heat(*)
      integer npoint,i
      real*8 x,y,z,funccf,rho
      
      do i=1,npoint
         x=coor(1,i)
         y=coor(2,i)
         rho = funccf(3,x,y,z)
         heat(i)=rho*heat(i)
      enddo
         
      return
      end
 
c *************************************************************
c *   HEATFROMTHUK
c *
c *   Determine BSE heating at time TDIM (in Byr). Non-dimensionalize
c *   using dimensional values in dimensional.inc
c *   Assume t=0 corresponds to 4 Byr ago.
c *
c c   PvK 081000
c *************************************************************
      subroutine heatfromthuk(tdim,heat)
      implicit none
      real*8 tdim,heat
      include 'dimensional.inc'
      include 'degas.inc'
      real*8 I235_0,I238_0,I232_0,I40_0
      real*8 C235_0,C238_0,C232_0,C40_0
      real*8 H235_0,H238_0,H232_0,H40_0
      real*8 tdim0
      parameter(I235_0=0.0071,I238_0=0.9928,I232_0=1.0,I40_0=0.000119)
      parameter(C235_0=0.0202e-6,C238_0=0.0202e-6,C232_0=0.0764e-6,
     v            C40_0=0.0235e-2)
      parameter(H235_0=5.69e-4,H238_0=9.47e-5,H232_0=2.63e-5,
     v            H40_0=2.95e-5)
      parameter(tdim0=4.0)
      integer i
      real*8 heat_dim

     
      heat_dim = (R2_dim-R1_dim)*(R2_dim-R1_dim)/
     v              (cp_dim*rkappa_dim*DeltaT_dim)
      heat = I235_0*C235_0*H235_0*exp(rl235*(tdim0-tdim)) + 
     v       I238_0*C238_0*H238_0*exp(rl238*(tdim0-tdim)) +
     v       I232_0*C232_0*H232_0*exp(rl232*(tdim0-tdim)) +
     v       I40_0 *C40_0 *H40_0 *exp(rl40 *(tdim0-tdim)) 
      heat = heat*heat_dim

      end
