c ***************************************************
c *   PEVRMS
c *
c *   Calculates u*u + v*v per nodal point from the
c *   solution vector and places the result in array user.
c *
c *   For element 900/903 with fixed number of degrees of 
c *   freedom (e.g., 900 with ISHAPE=6, 903 with ISHAPE=3)
c *   or for variable (e.g., 903 with ISHAPE=4)
c *
c *   PvK, 17-4-89/930107/970105/
c *   PvK 190700
c *****************************************************************
      subroutine pevrms9(isol,kmesh,kprob,iuser,user)
      implicit none
      integer isol(*),kmesh(*),kprob(*),iuser(*)
      real*8 user(*)
      include 'pecof900.inc'
      integer ibuffr
      common ibuffr(1)
      real*8 buffr(1)
      equivalence (buffr(1),ibuffr(1))
      integer indprf,nphys,npoint,ipusol
      integer nunkp,inidgt,iniget,ipkprf
      

      npoint = kmesh(8)
      call ini050(isol(1),'pevrms9: isol')
      ipusol = inidgt(isol(1))
      nunkp = kprob(4)
      indprf = kprob(19)
      if (indprf.ne.0) then
         call ini050(indprf,'pevrms9: indprf')
         ipkprf = iniget(indprf)
      endif
      call pevrms9_01(npoint,buffr(ipusol),nunkp,
     v                indprf,ibuffr(ipkprf),user(6))

c     *** this is new (and annoying)
      iuser(1) = 100
      iuser(2) = 1
      iuser(3) = 0
      iuser(4) = 0
      iuser(5) = 0
      iuser(6) = 7
      iuser(7) = intrule900
      iuser(8) = icoor900
      iuser(9) = 0
      iuser(10) = 2001
      iuser(11) = 6

      return
      end

      subroutine pevrms9_01(npoint,usol,nunkp,indprf,kprobf,user) 
      implicit none
      integer npoint,nunkp,indprf,kprobf(*)
      real*8 usol(*),user(*)
      integer j1,j2,i

      if (indprf.eq.0) then
         do i=1,npoint
            j1 = (i-1)*nunkp+1
            j2 = (i-1)*nunkp+2
             user(i) = usol(j1)*usol(j1)+usol(j2)*usol(j2)
         enddo
      else 
         do i=1,npoint
            j1=kprobf(i)+1
            j2=j1+1
            user(i) = usol(j1)*usol(j1)+usol(j2)*usol(j2)
         enddo
      endif
 
      return
      end
