      subroutine eld905 ( coor, elemvc, iuser, user, index1, index3,
     +                    index4, vecold, islold, vecloc, work )
! ======================================================================
!
!        programmer    Guus Segal
!        version  1.0  date 13-05-2005
!
!   copyright (c) 2005-2005  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element derivative vector and weights for Navier-Stokes
!     equations solved by pressure correction in combination with
!     Taylor-Hood
!     Two and three dimensional elements
! **********************************************************************
!
!                       KEYWORDS
!
!     derivative
!     element_vector
!     navier_stokes_equation
!     pressure_correction
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cactl'
      include 'SPcommon/celiar'
      include 'SPcommon/celint'
      include 'SPcommon/cconst'
      include 'SPcommon/cinforel'
      double precision x, w, xgauss, phi, dum, uold
      common /celwrk/ x(81), w(27), xgauss(81), phi(729),
     +                uold(81), dum(501)
      save /celwrk/

!                     /celwrk/
!          Work common block to store local arrays
!          CELWRK contains exactly 1500 real positions, the last 500
!          are reserved for the ELP subroutines
!          The following parameters are used in CELWRK
!     dum            500 positions reserved for the ELP subroutines
!     phi            Array of length inpelm x m containing the values of the
!                    basis functions in the Gauss points in the sequence:
!                           k
!                    phi (xg ) = phi (i,k)
!                       i
!                    Array phi is only filled when ifirst = 0 and gauss
!                    integration is applied
!     uold           Array to store the old solution in the nodal points
!     x              array of length inpelm x 2 containing the x and
!                    y-coordinates of the nodal points with respect to the
!                    local numbering in the sequence:
!                    x  = x (i,1);  y  = x (i,2);
!                     i              i
!     xgauss         Array of length m x 2 containing the co-ordinates of
!                    the gauss points.  Array xgauss is only filled when Gauss
!                    integration is applied.
!                    xg  = xgauss (i,1);  yg  = xgauss (i,2);
!                      i                    i
!     w              array of length m containing the weights for the
!                    numerical integration
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemvc(*), coor(*), user(*),
     +                 vecold(*), vecloc(*), work(*)
      integer iuser(*), index1(*), index3(numold,*), index4(*),
     +        islold(5,*)

!     coor           i    array of length ndim x npoint containing the
!                         co-ordinates of the nodal points with respect to the
!                         global numbering
!     elemvc         o    Element vector to be computed
!     index1         i    Array of length inpelm containing the point numbers
!                         of the nodal points in the element
!     index3         i    Two-dimensional integer array of length NUMOLD x
!                         NINDEX containing the positions of the "old"
!                         solutions in array
!                         VECOLD with respect to the present element
!                         For example VECOLD(INDEX3(i,j)) contains the j th
!                         unknown with respect to the i th old solution vector.
!                         The number i refers to the i th vector corresponding
!                         to IVCOLD in the call of SYSTM2 or DERIVA
!     index4         i    Two-dimensional integer array of length NUMOLD x
!                         INPELM containing the number of unknowns per point
!                         accumulated in array VECOLD with respect to the
!                         present element.
!                         For example INDEX4(i,1) contains the number of
!                         unknowns in the first point with respect to the
!                         i th vector stored in VECOLD.
!                         The number of unknowns in the j th point with respect
!                         to i th vector in VECOLD is equal to
!                         INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold         i    User input array in which the user puts information
!                         of all preceding solutions
!                         Integer array of length 5 x numold
!     iuser          i    Integer user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     user           i    Real user array to pass user information from
!                         main program to subroutine. See STANDARD PROBLEMS
!     vecloc         i    Work array in which all old solution vectors for the
!                         integration points are stored
!     vecold         i    In this array all preceding solutions are stored, i.e.
!                         all solutions that have been computed before and
!                         have been carried to system or deriva by the parameter
!                         islold in the parameter list of these main subroutines
!     work                work array of length 163 + 238m**3 + 6m + 15n**2 + 10n
!                         contents:
!                         Starting address    length   Name:
!                         1                   7*27     ARRAY
!                         ipugs               3*27     UGAUSS
!                         ipdudx              9*27     DUDX
!                         ipdpdx              27*27*3  DPHIDX
!                         ipphix              27*27*3  PHIKSI for elp633
!                         ipetef              27       ETHA_EFFECTIVE
!                         ipseci              27       SECOND_INVARIANT
!                         ipdetd              27       DERIVATIVE of
!                                                      SECOND_INVARIANT
!                         ipsp                81*4     SP_matrix (Gradient P)
!                         ipdiv               81*4     DIV_matrix (Div matrix)
!                         ipwork              27*31    Work space
!                         Total length: 6558
!                         The subarrays contain the following contents:
!                         ARRAY:
!                         array in which the values of the coefficients
!                         in the integration points are stored in the sequence:
!                              2D                  3D
!                         1:   rho              rho
!                         2:   omega            omega
!                         3:   f1               f1
!                         4:   f2               f2
!                         5:                    f3
!                         6:   eta              eta
!                         Each coefficient requires exact m (is number of
!                         integration points) positions
!                         DPHIDX:
!                         Array of length inpelm x m x ndim containing the
!                         derivatives of the basis functions in the sequence:
!                         d phi / dx_l = dphidx (i,k,l)
!                           in node k
!                         WORK
!                         General work space
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer i, isub, isup, jelp, jtime, ipdpdx, ipugs,
     +        ipwork, ipphix, mconv, modelv, ipdudx, ipsp, ipetef,
     +        ipseci, ipdetd, jcheld, mult, mcont, ipdiv,
     +        nveloc, idummy(1), iseqpres, nsave, mdiv, modelrhsd,
     +        ipfdiv, ipkappa, ippp, ippsi, ippsiksi, ippsix, iprho
      double precision qmat(3,27), cn, clamb, tang(27), rhsdiv(8)
      logical debug
      save isub, isup, jelp, jtime, ipdpdx, ipwork, ipphix, ipugs,
     +     mconv, modelv, cn, clamb, ipdudx, ipsp, ipetef,
     +     ipseci, ipdetd, mult, mcont, jcheld,
     +     ipdiv, nveloc, iseqpres,
     +     ipfdiv, ipkappa, ippp, ippsi, ippsiksi, ippsix, iprho

!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     i              Counting variable
!     idummy         Dummy parameter
!     ipdetd         Starting address in array work of detdsc
!     ipdiv          Starting address in array work of div (2D only)
!     ipdpdx         Starting address in array work of dphidx
!     ipdudx         Starting address in array work of dudx
!     ipetef         Starting address in array work of etheff
!     ipfdiv         Starting address in array work of fdiv
!     ipkappa        Starting address in array work of kappa
!     ipphix         Starting address in array work of phiksi
!     ippp           Starting address in array work of pressure matrix
!     ippsi          Starting address in array work of psi
!     ippsiksi       Starting address in array work of psiksi
!     ippsix         Pointer to dpsi/dx
!     iprho          Starting address in array work of rho
!     ipseci         Starting address in array work of secinv
!     ipsp           Starting address in array work of sp
!     ipugs          Starting address in array work of ugauss
!     ipwork         Starting address in array work of work array
!     iseqpres       Contains the sequence numbers of the pressure in the
!                    element vector
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     jcheld         if ( icheld<= 10 ) then icheld else icheld-20
!     jelp           Indication of the type of basis function subroutine must
!                     be called by ELM100.
!     jtime          Integer parameter indicating if the mass matrix for the
!                    heat equation must be computed (>0) or not ( = 0)
!                    If jtime = 1, then the coefficient for the mass matrix is
!                    constant, otherwise (jtime = 2) it is variable
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!     mconv          see USER INPUT in subroutine ELM600
!     mdiv           Defines if divergence right-hand side is used (1)
!                    or not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second
!                          invariant
!                    102:  user provided model depending on the second
!                          invariant,
!                          the co-ordinates and the velocity
!     mult           Multiplication factor for the number of unknowns per point
!     nsave          Help parameter to save the value of n
!     nveloc         Number of velocity parameters
!     qmat           transformation matrix global -> local coordinates
!                    in case of a 3D element
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     tang           In this array the tangential vector is stored
!                    tang(j,i) contains the j th component of the
!                    tangential vector in the i-th integration point
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0905         Perform initialization for element subroutine type 905
!     EL1004         Compute gradient and divergence matrix (Taylor-Hood)
!     EL1005         fill of derivatives and weights in elemvc and elemwg
!                    with respect to subroutine ELD250
!     EL1007         Fill element vector for vertices only
!     EL1014         Restrict values of u, x and dudx to centroid
!     EL2005         Compute a local vector and its derivatives from a known
!                    vector
!     EL3009         Fill series of old solutions in array vecloc
!     EL4917         Compute special derivatives for Navier-Stokes
!     ELFLR7         Fill variable coefficients in array ARRAY
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELMATMULTRECTP Multiply transpose of rectangular matrix div of size
!                    (nrow x ncol) by vector of size nrow to get vector of
!                    size ncol
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     INICHKARLEN    Check length of array and give error message 550 of
!                    array is too small
!     INSTOP         Stop the program
!     PRESGRAD       Computes the pressure gradient in the 2d case
!     PRESGRAD3D     Computes the pressure gradient in the 3d case
!     PRINRL         Print 1d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  ISEQ:  Sequence number of vector in VECOLD/ISLOLD from which UOLD and
!         derivatives must be computed.
!         If 0 or 1 the first vector is used
!         If more vectors are needed, these vectors are supposed to be stored
!         sequentially, where the first one is indicated by ISEQ
!     2:  type of viscosity model used (modelv)
!         Possible values:
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!     5:  Information about the convective terms (mconv)
!         Possible values:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!     6-20: Information about the equations and solution method
!     6:  -
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1, the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-20: not yet used
!     Remark:  the coefficients 2-20 are only used in the case of icheld = 6
!              or 11, i.e. computation of the stress tensor
!              In the case icheld = 7 and an extended quadratic triangle
!              also all parameters are used
!     icheld                    Cartesian            Cylindrical/Polar
!       1                        du        / d
!                                  jdegfd     ix
!       2                        grad u               2D/Cyl: (r,z)
!                                i.e. du1/dx1,        dur/dr, duz/dr,
!                                du2/dx1, ...         dur/dz, duz/dz
!                                                     3D/Cyl: (r,z,phi)
!                                                     dur/dr, duz/dr, duphi/dr
!                                                     dur/dr, duz/dz, duphi/dz
!                                                     dur/rdphi-uphi/r,
!                                                     duz/rdphi,duphi/rdphi+ur/r
!                                                     2D/Pol: (r,phi)
!                                                     dur/dr, duz/dr,
!                                                     dur/rdphi-uphi/r,
!                                                     duphi/rdphi+ur/r
!       3                        - grad u
!       4                        div u
!       5                        curl u                  Cylinder co-or
!                     2D:       du2/dx1-du1/dx2      2D:   duz/dr-dur/dz
!                     3D:       du3/dx2-du2/dx3      3D:   duz/rdphi-duphi/dz
!                               du1/dx3-du3/dx1       duphi/dr+(uphi-dur/dphi)/r
!                               du2/dx1-du1/dx2            dur/dz-duz/dr
!                                                         Polar co-or
!                                                     duphi/dr+(uphi-dur/dphi)/r
!                             | sigma x |                    | sigma r     |
!       6                     | sigma y |                    | sigma z     |
!                     sigma = | sigma z |            sigma = | sigma theta |
!                             | tau xy  |                    | tau rz      |
!                             | tau yz  |                    | tau zphi    |
!                             | tau zx  |                    | tau phir    |
!       7             pressure p
!       8             elongation
!                        ndim = 2, jcart # 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(1)*u(2)*(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        e = (u(1)**2*gradu(1,1) + u(2)**2*gradu(2,2) +
!                             u(3)**2*gradu(3,3) + u(1)*u(2)*(gradu(1,2) +
!                             gradu(2,1)) + u(1)*u(3)*(gradu(1,3)+gradu(3,1)) +
!                             u(2)*u(3)*(gradu(2,3)+gradu(3,2)))/||u||*2
!       9             deformation
!                        ndim = 2, jcart # 4:
!                        g = (2u(1)u(2)(-gradu(1,1)+gradu(2,2)) +
!                             u(1)**2-u(2)**2(gradu(1,2)+gradu(2,1)))/||u||*2
!                        ndim = 3 or jcart = 4:
!                        g1 = gradu(1,2) + gradu(2,1)
!                        g2 = gradu(2,3) + gradu(3,2)
!                        g3 = gradu(3,1) + gradu(1,3)
!      10             sqrt(secinv)
!      11             viscous dissipation: 1/2 * t:A1
!      21-31:  See 1-11, however, now defined in the vertices instead of the
!              nodal points
!      21-31:  See 1-11, however, now defined in the vertices instead of the
!      54:     The right-hand side for the pressure equation (div u, psi_i)
!              is computed in case of pressure correction
!     Structure of output arrays:
!     Solution array:  ndim unknowns per point (IVEC=0)
!     Arrays of special structure:
!     IVEC = 1:   1 unknowns per point
!     IVEC = 2:   2 unknowns per point
!     IVEC = 3:   3 unknowns per point
!     IVEC = 4:   6 unknowns per point
!     IVEC = 5:   ndim unknowns per point
!     IVEC = 6:   1 unknowns per vertex
!     IVEC = 7:   2 unknowns per vertex
!     IVEC = 8:   3 unknowns per vertex
!     IVEC = 9:   6 unknowns per vertex
!     IVEC =10:   ndim unknowns per vertex
!     IVEC =11:   1/3/6 unknowns per point
!     IVEC =12:   0/1/3 unknowns per point
!     IVEC =13:   1/4/9 unknowns per point
!     IVEC =14:   1/3/6 unknowns per vertex
!     IVEC =15:   0/1/3 unknowns per vertex
!     IVEC =16:   1/4/9 unknowns per vertex
!     ICHELD                 IVEC
!       1                      1
!       2                     13
!       3                     13
!       4                      1
!       5                     12
!       6                      4
!       8                      1
!       9                     12
!      10                      1
!      11                      1
!      21                      6
!      22                     16
!      23                     16
!      24                      6
!      25                     15
!      26                      9
!      28                      6
!      29                     15
!      30                      6
!      31                      6
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     if ( ifirst=0 ) then
!        Define element independent quantities
!        Compute pointers in array WORK
!     Compute basis functions and related quantities
!     Fill old solution into array UOLD and if necessary the derivatives
!     Compute variable coefficients if necessary
!     Compute derived quantities
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'eld905' )
      debug = .false.

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         call el0905 ( iuser, user, jelp, jtime, cn, clamb,
     +                 mconv, mcont, modelv, isub, isup, work,
     +                 dum(1), nveloc, mdiv, modelrhsd, iseqpres )

!        --- Fill array ich (COMMON CELIAR) for preceding solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

!        --- Length of elemwg(*) to be filled

         if ( icheld<=11 ) then
            jcheld = icheld
         else if ( icheld<=16 ) then
            jcheld = icheld-10
         else if ( icheld==17 ) then
            jcheld = 11
         else if ( icheld<=19 ) then
            jcheld = icheld-10
         else if ( icheld==20 ) then
            jcheld = 7
         else if ( icheld<=31 ) then
            jcheld = icheld-20
         else
            jcheld = icheld-40
         end if
         if ( jcheld==1 .or. jcheld==4 .or. jcheld==7 .or.
     +        jcheld==8 .or. jcheld==10 .or. jcheld==11 ) then

!        --- icheld = 1, 4, 6, 8, 10 or 11
!            1 unknown per point

            mult = 1

         else if ( jcheld==2 .or. jcheld==3 ) then

!        --- icheld = 2 or 3
!            ndim**2 unknowns per point

            mult = ndim**2

         else if ( jcheld==6 ) then

!        --- icheld = 6
!            lower triangle of tensor

            mult = 6

         else if ( ndim==2 ) then

!        --- icheld = 5 or 9 and ndim = 2,
!            one unknown per point

            mult = 1

         else

!        --- icheld = 5 or 9 and ndim = 3,
!            three unknowns per point

            mult = 3

         end if

!        --- Fill pointers in array work

         if ( mcont==1 .and. mod(jdiag,10)/=1 .and.
     +       ( ind(1)>0 .or. ist(1)>0 ) ) then
            iprho = 1+6*m
         else
            iprho = 1
         end if
         ipkappa = 1+7*m
         ipfdiv = 1+8*m

         ipugs  = 1 + 32*m+n
         ipdudx = ipugs  + ndim*m
         ipdpdx = ipdudx + ndim**2*m
         ipphix = ipdpdx + ndim*m*n
         ipetef = ipphix + ndim*n*m
         ipseci = ipetef + m
         ipdetd = ipseci + m
         ipsp   = ipdetd + m
         if ( mcont==0 ) then
            ipdiv = ipsp
         else
            ipdiv = ipsp + n*npsi*ndim
         end if

!        --- set pointer for pressure matrix

         if ( mcont==4 ) then
            ippp = ipdiv + n*npsi*ndim
            ipwork = ippp + (npsi)**2
         else
            ippp = ipdiv
            ipwork = ippp + n*npsi*ndim
         end if
         ippsi = ipwork+(ndim*n)**2
         ippsix = ippsi+m*npsi
         ippsiksi = ippsix+m*npsi*n

!        --- Check array length as defined in elmgrinf

         call inichkarlen (  ippsiksi+m*npsi*ndim, 27316, 'work' )

      end if

!     --- compute basis functions and weights for numerical integration

      jdiag = jdiag+10
      call  elm800basefn ( coor, x, w, work(ipdpdx), xgauss, phi,
     +                     index1, qmat, tang, jelp, work(ipphix),
     +                     work(ippsi), work(ippsix), work(ippsiksi) )
      jdiag = jdiag-10
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold if necessary

      iseqin = 1
      if ( ind(50)==1 .or. ind(50)==3 ) then
         call el2005 ( vecold, uold, work(ipdudx), phi, work(ipdpdx),
     +                 index3, idummy, work(ipugs), iseqin, index4 )
      end if

      if ( ind(50)==2 .or. ind(50)==3 )
     +   call el3009 ( vecold, index3, index4, vecloc,
     +                 phi, n, work(ipwork) )

!     --- Fill variable coefficients if necessary

      do i = isub, isup

         if ( ind(i)>0 .or. ist(i)>0 ) then

!        --- Coefficient is variable

            if ( mcont==1 .and. i==1 .and. jdiag/=1 ) jdiag = 1
            call elflr7 ( i, user, work(1+(i-1)*m), index1, x, xgauss,
     +                    m, n, phi, vecold, work(ipwork), 1, index1,
     +                    index3, index4, numold, iuser, work(ipugs),
     +                    vecloc, maxunk )

         end if

      end do

!     --- Fill elemvc, contents depends on icheld

      if ( icheld==54 .or. icheld==55 ) then

!     --- icheld = 54, 55 special case, in which the divergence matrix is
!         multiplied by the old velocity or the gradient matrix by the
!         old pressure
!         This option is meant for pressure correction only
!         First compute the divergence and gradient matrix

         call el1004 ( work(ipsp), work(ipdiv), work(ippsi),
     +                 work(ipdpdx), phi, w, xgauss, mcont, work(iprho),
     +                 work(ippp), work(ipkappa), rhsdiv, work(ipfdiv),
     +                 mdiv )

         if ( icheld==54 ) then

!        --- icheld = 54, Matrix vector multiplication Lu

            do i = 1, icount
               uold(i) = vecold(index3(iseqin,i))  ! refill uold in standard
                                                   ! sequence u_1, v_1, u_2, ...
            end do  ! i = 1, icount
            jadd = 0
            call elmatmultrectp ( work(ipdiv), uold, elemvc, icount,
     +                            npsi, jadd )    ! L u
         else

!        --- icheld = 55, compute Gp

            do i = 1, npsi
               uold(i) = vecold(index3(iseqin,i))  ! put pressure in uold
            end do  ! i = 1, icount
            if ( debug ) call prinrl ( uold, npsi, 'pold' )
            if ( ndimlc==2 ) then
               call presgrad ( uold, work(ippsix), elemvc )
            else
               call presgrad3d ( uold, work(ippsix), elemvc )
            end if
         end if  ! ( icheld==54 ) then

      else if ( icheld<=5 .or. icheld>=31 .and.icheld<=35 ) then

!     --- 1<=icheld<=5 or 31<=icheld<=35,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, uold, work(ipdudx),
     +                 x, mult, elemvc, m )

      else if ( icheld>=43 .and. icheld<=47 ) then

!     --- 43<=icheld<=47,
!         fill elemvc by derivatives

         call el1005 ( jcheld, ix, jdegfd, work(ipugs), work(ipdudx),
     +                 xgauss, mult, elemvc, m )

      else if ( icheld<=11 .or. icheld>35 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation

         call el4917 ( jcheld, modelv, work(1+5*m), work(ipetef), cn,
     +                 clamb, xgauss, uold, work(ipdudx), work(ipseci),
     +                 work(ipwork), mult, elemvc, m, vecloc,
     +                 maxunk, numold, m , mcont)

      else if ( icheld>20 .and. icheld<=25 ) then

!     --- 20<icheld<=25 fill elemvc by derivatives,
!         First fill in all nodes

         call el1005 ( icheld-20, ix, jdegfd, uold, work(ipdudx),
     +                 x, mult, work(ipwork), m )

!        --- Copy in vertices

         call el1007 ( work(ipwork), elemvc, mult, ishape )

      else if ( icheld>11 .and. icheld<=15 ) then

!     --- 12<=icheld<=15
!         fill elemvc by derivatives
!         First set n equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, work(ipdudx), x, ishape )
         m = 1
         call el1005 ( jcheld, ix, jdegfd, uold, work(ipdudx),
     +                 x, mult, elemvc, nsave )

!        --- reset n

         m = nsave

      else if ( icheld<=20 ) then

!     --- Compute stress tensor, elongation, shear rate, second invariant or
!         viscous dissipation
!         First set n equal to 1 and compute the arrays u, x and dudx
!         in centroid

         nsave = m
         call el1014 ( uold, work(ipdudx), x, ishape )
         m = 1
         call el4917 ( jcheld, modelv, work(1+5*nsave), work(ipetef),
     +                 cn, clamb, xgauss, uold, work(ipdudx),
     +                 work(ipseci), work(ipwork), mult, elemvc, m,
     +                 vecloc, maxunk, numold, nsave , mcont)

!        --- reset m

         m = nsave

      else

!     --- Compute stress tensor, elongation, shear rate or second invariant
!         First fill in all nodes

         call el4917 ( icheld-20, modelv, work(1+5*m), work(ipetef), cn,
     +                 clamb, xgauss, uold, work(ipdudx), work(ipseci),
     +                 work(ipwork+mult*n), mult, work(ipwork),
     +                 n-jtrans, vecloc, maxunk, numold, m , mcont)

!        --- Copy in vertices

         call el1007 ( work(ipwork), elemvc, mult, ishape )

      end if

1000  if ( ierror/=0 ) call instop

      call erclos ( 'eld905' )

      end
