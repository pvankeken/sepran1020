      subroutine elm900 ( coor, elemmt, elemvc, elemms, iuser, user,
     +                    index1, index3, index4, vecold, islold,
     +                    vecloc, wrk1, symm )
! ======================================================================
!
!        programmer    Guus Segal/Jaap van der Zanden
!        version  2.22 date 14-11-2007 Extension with diagonal velocity m matrix
!        version  2.21 date 31-08-2007 New call to el0900
!        version  2.20 date 19-04-2007 Upwind for convection
!        version  2.19 date 08-02-2006 Replace part of body by subroutines
!        version  2.18 date 16-07-2004 Correction rhside in case of penalty +
!                                      non-zero divergence
!        version  2.17 date 23-02-2004 Extra parameters w, x, wrk1(ipxgauss)
!        version  2.16 date 13-02-2004 Extra debug statements
!        version  2.15 date 04-01-2004 Debug statements
!        version  2.14 date 04-07-2003 Remove common celwrk
!        version  2.13 date 19-05-2003 New call to elm800basefn
!        version  2.12 date 07-05-2003 Do not make symm false for 901/2
!        version  2.11 date 16-04-2003 Use elm800basefn instead of elp routines
!        version  2.10 date 03-03-2003 New call to el2005
!
!   copyright (c) 1993-2007  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     Fill element matrix and vector for the  navier-stokes equations
!     incompressible flow, using the penalty method or the integrated method
!     Two and three-dimensional elements
!     ELM900 is a help subroutine for subroutine BUILD (TO0050)
!     it is called through the intermediate subroutine elmns2
!     So:
!     BUILD
!     TO0050
!       -  Loop over elements
!          -  ELMMEC
!             - ELM900
!    The subroutine is able to handle the following element shapes and cases:
!    2D:  4 node quadrilateral (Types 900, 925)
!         5 node quadrilateral (types 901, 926 integrated method)
!         6 node triangle (Types 900, 925 and 400, 402, 404 and 406)
!         penalty method
!         This element is treated internally as a seven-point element
!         Type 400: Cartesian, 402 Polar, 404 Axi-symmetric,
!              406: Axi-symmetric with swirl
!         7 node triangle (Type 901,902)
!         integrated method
!         901: elimination of centroid
!         902: no elimination
!         9 node quadrilateral (Type 900, penalty method
!                               Type 901 integrated method)
!    3D: 8 node or 27 node brick (Types 900 and 410)
!         penalty method
!        27 node brick (Type 902)
!         integrated method
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cmcdpi'
      include 'SPcommon/cconst'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/cellog'
      include 'SPcommon/cinforel'
      include 'SPcommon/celwrk'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision elemmt(*), elemvc(*), coor(*), user(*),
     +                 elemms(*), vecold(*), vecloc(*), wrk1(*)
      integer iuser(*), index1(*), index3(*), index4(*), islold(5,*)
      logical symm

!     coor     i     array of length ndim x npoint containing the co-ordinates
!                    of the nodal points with respect to the global numbering
!                    x  = coor (1,i);  y  = coor (2,i);  z  = coor (3,i);
!                     i                 i                 i
!     elemms   o     Element mass matrix to be computed (Diagonal matrix)
!     elemmt   o     Element matrix to be computed
!                    At this moment the element is assumed to be zero
!     elemvc   o     Element vector to be computed
!     index1   i     Array of length inpelm containing the point numbers
!                    of the nodal points in the element
!     index3   i     Two-dimensional integer array of length NUMOLD x NINDEX
!                    containing the positions of the "old" solutions in array
!                    VECOLD with respect to the present element
!                    For example VECOLD(INDEX3(i,j)) contains the j th
!                    unknown with respect to the i th old solution vector.
!                    The number i refers to the i th vector corresponding to
!                    IVCOLD in the call of SYSTM2 or DERIVA
!     index4   i     Two-dimensional integer array of length NUMOLD x INPELM
!                    containing the number of unknowns per point accumulated
!                    in array VECOLD with respect to the present element.
!                    For example INDEX4(i,1) contains the number of unknowns
!                    in the first point with respect to the i th vector stored
!                    in VECOLD.
!                    The number of unknowns in the j th point with respect to
!                    i th vector in VECOLD is equal to
!                    INDEX4(i,j) - INDEX4(i,j-1)  (j>1)
!     islold   i     User input array in which the user puts information
!                    of all preceding solutions
!                    Integer array of length 5 x numold
!     iuser    i     Integer user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     symm    i/o    Indicates whether the matrix symmetric (true) or not
!     user     i     Real user array to pass user information from
!                    main program to subroutine. See STANDARD PROBLEMS
!     vecloc         Work array in which all old solution vectors for the
!                    integration points are stored
!     vecold   i     In this array all preceding solutions are stored, i.e.
!                    all solutions that have been computed before and
!                    have been carried to system or deriva by the parameter
!                    islold in the parameter list of these main subroutines
!     wrk1           work array of length 163 + 238m**3 + 6m + 15n**2 + 10n
!                    contents:
!                    Starting address    length   Name:
!                    1                  32*27     ARRAY
!                    ipugs               3*27     UGAUSS
!                    ipdudx              9*27     DUDX
!                    ipphixi              27*27*3  PHIKSI for elp633
!                    ipetef              27       ETHA_EFFECTIVE
!                    ipseci              27       SECOND_INVARIANT
!                    ipdetd              27       DERIVATIVE of
!                                                 SECOND_INVARIANT
!                    ipsp                81*4     SP_matrix (Gradient P)
!                    ipdiv               81*4     DIV_matrix (Divergence matrix)
!                    ippp                4*4      Pressure matrix
!                    ipwork              27*31    Work space
!                    ipelem              81*81    Copy of part of element matrix
!                    ipelvc              81*3     Copy of part of element vector
!                    iptran              12*2     Transformation matrix based
!                                                 on div
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    iptrnp              12*2     Transformation matrix based
!                                                 on sp
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    ipmass              4*7*7    Mass matrix in case of a
!                                                 transformation
!                                                 (2D only, does not contribute
!                                                  to total length, since this
!                                                  length is maximized for 3d)
!                    ipunew               3*27     Unew
!                    ipunewgs             3*27     Unew_gauss
!                    ipdudxnw             9*27     DUDX_unew
!                    Total length: 14353
!                    The subarrays contain the following contents:
!                    ARRAY:
!                    array in which the values of the coefficients
!                    in the integration points are stored in the sequence:
!                         2D                  3D
!                    1:   rho              rho
!                    2:   omega            omega
!                    3:   f1               f1
!                    4:   f2               f2
!                    5:                    f3
!                    6:   eta/E            eta/E
!                    7:   rho in nodal points (mcont=1 only)
!                    7:   G_L              G_L (mcont=2 only)
!                    8:   kappa            kappa
!                    9:   fdiv             fdiv
!                   10:   fstress          fstress
!                   11:   c_div_1
!                   12:   c_div_2
!                   13:   -
!                   14:   c_grad
!                   15:   c_conv_1
!                   16:   c_conv_2
!                   17:   c_conv_3
!                   18:   c_conv_4
!                   19:   -
!                   20:   -
!                   21:   -
!                   22:   -
!                   23:   -
!                   24:   c_visc_1
!                   25:   c_visc_2
!                   26:   c_visc_3
!                   27:   c_visc_4
!                   28:   -
!                   29:   -
!                   30:   -
!                   31:   -
!                   32:   -
!                    Each coefficient requires exactly m (is number of
!                    integration points) positions
!                    DPHIDX:
!                    Array of length inpelm x m x ndim containing the
!                    derivatives of the basis functions in the sequence:
!                    d phi / dx = dphidx (i,k,1);  d phi / dy = dphidx (i,k,2);
!                         i                             i
!                    d phi / dz = dphidx (i,k,3)
!                         i
!                      in node k
!                    WORK
!                    General work space
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer isub, isup, jelp, jtime, ipugs, ippp, iflipflop,
     +        ipwork, ipphixi, mconv, modelv, ipdudx, ipsp, ipetef,
     +        ipseci, ipdetd, ipelem, ipelvc, mcont, ipdiv, iptran,
     +        iptrnp, ipelcp, iprho, nveloc, ipmass, npres, iprhoc,
     +        imesh, ipunew, ipdudxnw, ipunewgs, mdiv, mcoefconv,
     +        modelrhsd, ioldm, ipphi, signdiv, ipkappa, ipfdiv,
     +        ipcoeffgrad, ipcoeffdiv, ipetaspecial, ipetha, ipomega,
     +        ipwork1, ipgoertler, ipcconv, ippsiupw, ipviscrho,
     +        ipx, ipw, ipxgauss, ipuold, ipuoldm, ipdphidx,
     +        ipqmat, ippsi, ippsix, ippsiksi, ipenalty
      double precision penalp, cn, clamb, thetdt,
     +                 rhsdiv(4), rhsdivtr(3), tang(1)
      logical debug, second
      save isub, isup, jelp, jtime, ipwork, ipphixi, ipugs,
     +     mconv, modelv, penalp, cn, clamb, ipdudx, ipsp, ipetef,
     +     ipseci, ipdetd, thetdt, ipelem, ipelvc, mcont, ipdiv, iptrnp,
     +     ipelcp, iprho, nveloc, iptran, ipmass, npres, iprhoc, imesh,
     +     ipunew, ipdudxnw, ipunewgs, mdiv, mcoefconv, ippp, modelrhsd,
     +     ioldm, ipphi, ippsiupw, ipdphidx, ipviscrho,
     +     ipx, ipw, ipxgauss, ipuold, ipuoldm,
     +     ipqmat, ippsi, ippsix, ippsiksi, ipenalty

!     clamb          Parameter lambda with respect to viscosity model
!     cn             Power in power law model
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     iflipflop      Work array to prevent flip-flop behaviour in non-
!                    linear upwind
!                    The status of the element with respect to upwind is
!                    stored in this array
!     imesh          Sequence number of mesh velocity in input vector
!                    If 0 no mesh velocity is present
!     ioldm          Sequence number of velocity at end of previous time
!                    step in input vector.
!                    If 0 no iterative procedure is adopted for dynamical
!                    analyses.
!     ipcconv        Starting address of array cconv
!     ipcoeffdiv     Starting address of array coeffdiv
!     ipcoeffgrad    Starting address of array coeffgrad
!     ipdetd         Starting address in array wrk1 of detdsc
!     ipdiv          Starting address in array wrk1 of div
!     ipdphidx       Starting address of dphidx
!     ipdudx         Starting address in array wrk1 of dudx
!     ipdudxnw       Starting address in array wrk1 of du0dx
!     ipelcp         Starting address in array wrk1 of copy of element matrix
!     ipelem         Starting address in array wrk1 of copy of element matrix
!     ipelvc         Starting address in array wrk1 of copy of element vector
!     ipenalty       Indicates whether a penalty term must be added to
!                    the velocity part (1) or not (0)
!     ipetaspecial   Starting address of array etaspecial
!     ipetef         Starting address in array wrk1 of etheff
!     ipetha         Starting address of array etha
!     ipfdiv         Starting address of array fdiv
!     ipgoertler     Starting address of array goertler
!     ipkappa        Starting address of array kappa
!     ipmass         Starting address in array wrk1 of mass matrix before
!                    transformation
!     ipomega        Starting address of array omega
!     ipphi          Starting address in array wrk1 of phi
!     ipphixi        Starting address in array wrk1 of phiksi
!     ippp           Starting address in array wrk1 of pressure matrix
!     ippsi          Starting address of psi
!     ippsiksi       Starting address of psiksi
!     ippsiupw       Starting address of psiupw
!     ippsix         Pointer to dpsi/dx
!     ipqmat         Starting address of array qmat
!     iprho          Starting address in array wrk1 of rho
!     iprhoc         Starting address in array wrk1 of rho multiplied by w
!     ipseci         Starting address in array wrk1 of secinv
!     ipsp           Starting address in array wrk1 of sp
!     iptran         Starting address in array wrk1 of trans with respect to
!                    div
!     iptrnp         Starting address in array wrk1 of trans with respect to
!                    grad
!     ipugs          Starting address in array wrk1 of ugauss
!     ipunew         Starting address in array wrk1 of unew (u0)
!     ipunewgs       Starting address in array wrk1 of u0 in Gauss points
!     ipuold         Starting address of solution array
!     ipuoldm        Starting address of solution array in prior time step
!     ipviscrho      Starting address of array viscosity/rho
!     ipw            Starting address of weight vector w
!     ipwork         Starting address in array wrk1 of work array
!     ipwork1        Starting adress of array WORK1
!     ipx            Starting adress of array x
!     ipxgauss       Starting adress of array xgauss
!     isub           Integer help parameter for do-loops to indicate the
!                    smallest coefficient number that is dependent on space
!     isup           Integer help parameter for do-loops to indicate the
!                    largest coefficient number that is dependent on space
!     jelp           Indication of the type of basis function subroutine must
!                    be called by ELM100. Possible vaues:
!                     1:  Subroutine ELP610  (Linear line element)
!                     2:  Subroutine ELP611  (Quadratic line element)
!                     3:  Subroutine ELP620  (Linear triangle)
!                     4:  Subroutine ELP624  (Extended quadratic triangle)
!                     5:  Subroutine ELP621  (Bilinear quadrilateral)
!                     6:  Subroutine ELP623  (Biquadratic quadrilateral)
!                    11:  Subroutine ELP630  (Linear tetrahedron)
!                    12:  Subroutine ELP631  (Quadratic tetrahedron)
!                    13:  Subroutine ELP632  (Trilinear brick)
!                    14:  Subroutine ELP633  (Triquadratic brick)
!    jtime           Integer parameter indicating if the mass matrix for the
!                    time-dependent equation must be computed (>0) or not ( = 0)
!                    Possible values:
!                      0:  no mass matrix
!                      1:  diagonal mass matrix with constant coefficient
!                      2:  diagonal mass matrix with variable coefficient
!                     11:  full mass matrix with constant coefficient
!                     12:  full mass matrix with variable coefficient
!                    101:  refers to the special possibility of the theta
!                          method directly applied
!                    102:  refers to the special possibility of the theta
!                          method directly applied with variable rho
!     mcoefconv      Defines how the convetive term is treated:
!                    0: standard case
!                    1: special case: the convective term is defined as
!                          c_conv_1 u du/dx + c_conv_2 v du/dy
!                          c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     mcont          Defines the type of continuity equation
!                    Possible values:
!                    0: incompressible flow
!                    1: compressible flow: div (rho u ) = 0
!                       Not applicable with penalty function method
!                    2: Goertler equations
!                    3: Special form of NS equations
!                    4: the continuity equation contains a pressure term.
!     mconv          Defines the treatment of the convective terms
!                    Possible values:
!                    0: convective terms are skipped (Stokes flow)
!                    1: convective terms are linearized by Picard iteration
!                    2: convective terms are linearized by Newton iteration
!                    3: convective terms are linearized by the incorrect Picard
!                       iteration
!                    4: convective terms are linearized by successive
!                       substitution
!     mdiv           Defines if divergence right-hand side is used (1) or
!                    not (0)
!     modelrhsd      Defines how the stress tensor is treated in the rhs
!                    Possible values:
!                    0:    if there is a given stress tensor the reference
!                          to this tensor is stored in position 19
!                    1:    It is supposed that there are 6 given stress
!                          tensor components stored in positions 19-24
!     modelv         Defines the type of viscosity model
!                    Possible values:
!                      1:  newtonian liquid
!                      2:  power law liquid
!                      3:  carreau liquid
!                      4:  plastico viscous liquid
!                     10:  simplified newtonian liquid, with constant viscosity
!                    100:  user provided model depending on grad u
!                    101:  user provided model depending on the second invariant
!                    102:  user provided model depending on the second invariant
!                          the co-ordinates and the velocity
!     npres          Number of pressure parameters
!     nveloc         Number of velocity parameters
!     penalp         Penalty parameter
!     rhsdiv         Divergence right-hand side (if mdiv=1)
!     rhsdivtr       Transformed divergence right-hand side (if mdiv=1)
!     second         Indicates if second derivative term is important in
!                    case of upwinding
!     signdiv        Multiplication sign of "continuity equations"
!                    if -1 the stokes matrix is symmetric
!                    if 1 all eigenvalues are in the positive half plane
!     tang           Dummy
!     thetdt         1/(theta dt) for the case of the theta method immediately
!                    applied
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     EL0900GEN      Performs the initializations for subroutine ELM900, ELI900
!                    or ELD900 in the case that IFIRST = 0
!     EL2107         Eliminate the centroid point for the incompressible
!                    Navier- Stokes equations in case of an extended quadratic
!                    triangle
!     ELIND0         Fill array ICH in COMMON CELIAR for preceding solutions
!     ELM800BASEFN   Compute the basis functions phi, its derivatives and other
!                    related quantities for various element shapes
!     ELM800UPWBASE  Compute the modified basis functions psi due to upwinding
!     ELM900ADDRESS  Compute starting addresses of subarrays in array
!     ELM900ADDRESS1 Compute starting addresses of subarrays in array
!     ELM900CHECK    Check element matrix/vector by matix vector multiplication
!     ELM900COEFFS   Fill variable coefficients in coefficients array
!                    Fill velocity from prior iteration if necessary
!     ELM900DIVGRAD  Compute the divergence and gradient matrix for
!                    Navier-Stokes
!                    Incompressible equations
!     ELM900MASSMAT  Fill element mass matrix for the incompressible
!                    Navier-Stokes equations
!     ELM900MAT      Create element matrix for Navier-Stokes equations
!                    using separate submatrices
!     ELM900MATRIX   Fill element matrix for the incompressible Navier-Stokes
!                    equations
!     ELM900RHSD     Compute right-hand side vector for Navier-Stokes element
!     ELM900TRANS    Compute the transformation matrix
!                    Compute the velocity in the centroid and the derivative
!                    of u
!     ELM900VEC      Create element vector for Navier-Stokes equations
!                    using separate subvectors
!     ELVISC         Compute effective viscosity depending on the viscosity
!                    model
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERRSUB         Error messages
!     PRINRL         Print 1d real vector
!     PRINRL1        Print 2d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is:
!     1:  information about the time-integration
!         This position consists of 2 parts:  jtime_old + 10*ioldm
!         Possible values for jtime_old
!         0: if imas in BUILD=0 then time-independent flow
!            else the mass matrix is built according to imas
!         1: The theta method is applied directly in the following way:
!            the mass matrix divided by theta delta is added to the stiffness
!            matrix
!            the mass matrix divided by theta delta and multiplied by u(n) is
!            added to the right-hand-side vector
!         ioldm indicates the sequence number of velocity at end of previous
!               time step in input vector.
!               If 0 no iterative procedure is adopted for dynamical
!               analyses.
!     2:  type of viscosity model used (modelv) as well as given stress
!         tensor as right-hand side according to
!         modelv + 1000*model_rhsd
!         Possible values for modelv
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!         5:    special case: the viscous term is defined as
!               c_visc_1 d^2 u /dx^2 + c_visc_2 d^2 u /dy^2
!               c_visc_3 d^2 v /dx^2 + c_visc_4 d^2 v /dy^2
!        10:    special case: Newtonian fluid, with constant viscosity
!               incompressible
!               The viscous part is written as eta laplace u,
!               hence the continuity equation is used to simplify
!               the expression
!               The natural boundary conditions are different from the
!               standard ones.
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant, the
!               co-ordinates and the velocity
!         Possible values for model_rhsd:
!         0:    if there is a given stress tensor the reference to this
!               tensor is stored in position 19
!         1:    It is supposed that there are 6 given stress tensor components
!               stored in positions 19-24
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Consists of 2 parts icoor and ipenalty according to
!         icoef4 = icoor + 100*ipenalty
!         icoor:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!         2: Polar co-ordinates
!         ipenalty:  Information of extra penalty term
!         0:  Standard method
!         1:  Extra penalty term added to velocity part
!             This is used for Arbitrary Lagrangian iteration method
!     5:  Information about the convective terms (mconv)
!         and continuity equation (mcont) according to
!         mconv+10*mcont+100*mcoef_conv
!         Possible values for mconv:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!         4: convective terms are linearized by successive substitution
!         Possible values for mcont:
!         0: Standard situation, the continuity equation is div u = 0
!         1: A special continuity equation div ( rho u ) = 0, which rho
!            variable is solved. This is not the standard continuity equation
!            for incompressible flow. In fact we have a time-independent
!            compressible flow description.
!            This possiblility can not be applied with the penalty function
!            approach
!         2: The so-called Goertler equations are solved
!         3: The continuity equation is equal to:
!            c_div_1 du/dx +  c_div_2 dv/dy = 0
!            The gradient of the pressure is multiplied by c_grad
!         4: the continuity equation contains a pressure term.
!         Possible values for mcoef_conv:
!         0: standard convective terms
!         1: special case: convective terms are defined by:
!            c_conv_1 u du/dx + c_conv_2 v du/dy
!            c_conv_3 u dv/dx + c_conv_4 v dv/dy
!     6-50: Information about the equations and solution method
!     6:  Parameter eps for the penalty function method
!         Also used for arbitrary Lagrangian approach
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  eta (if modelv = 1,10 the dynamic viscosity
!                          2, the parameter eta_n
!                          3, the parameter eta_c
!                          4, the parameter eta_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15:  imesh_velocity   (integer, refers to sequence number of mesh-velocity)
!                          If unequal to zero the convective term is adapted by
!                          -u_mesh * grad(u)
!                In case of the Goertler equations imesh_velocity refers to
!                the velocity field U_0
!    16:  G_L    Parameter for the Goertler equations
!    17:  kappa  Parameter for the Goertler equations
!                Parameter for the pressure term in the continuity equation
!                (MCONT=4)
!    18:  fdiv   Right-hand side for continuity equation
!    19:  stress If model_rhsd = 0 then
!                Given stress tensor used in the right-hand side vector
!                At this moment it is supposed that the stress is defined
!                per element and that in each element all components are
!                constant
!                If model_rhsd = 1 then
!                The six components of the stress tensors are stored in
!                positions 19 to 24 in the order: xx, yy, zz, xy, yz, zx
!                This option can not be used in cooporation with the
!                dimensionless coefficients stored in positions 20-41
!    20:  c_div_1
!    21:  c_div_2
!    22:  -
!    23:  c_grad
!    24-32:  c_conv 1 ... 9
!    33-41:  c_visc 1 ... 9
! **********************************************************************
!
!                       ERROR MESSAGES
!
!    2873   upwind not yet implemented
! **********************************************************************
!
!                       PSEUDO CODE
!
!    if ( first call ) then
!       initialize standard parameters
!    compute basis functions
!    fill old solutions if necessary
!    if ( matrix ) then
!       fill the viscosity in the corresponding array
!       Compute the viscous terms
!       if ( mconv>0 ) then
!          compute convective terms
!       Compute the divergence matrix
!       Compute the pressure matrix
!       Compute the penalty contribution
!    if ( vector ) then
!       Compute contribution of body forces
!       if ( mconv=2 ) then
!          compute contribution of convective terms
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'elm900' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from elm900'
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      if ( ifirst==0 )  then

!     --- ifirst = 0,  compute element independent quantities

         icheld = 0
         call el0900gen ( iuser, user, jelp, jtime, penalp, cn,
     +                    clamb, mconv, mcont, modelv, isub, isup,
     +                    wrk1, thetdt, nveloc, npres, imesh,
     +                    index4, mdiv, mcoefconv, modelrhsd, ioldm,
     +                    ipenalty )

!        --- Fill array ich (COMMON CELIAR) for prior solutions

         call elind0 ( index4, islold, numold, isub, isup, iuser )

!        --- Compute starting addresses in array wrk1

         npsi = ndim+1
         call elm900address ( ipphi, ipugs, mcont, iprho, ipdudx,
     +                        ipphixi, ipetef, ipseci, ipdetd, ipsp,
     +                        ipdiv, ippp, ipwork, ipelem, ipelcp,
     +                        wrk1, ipdudxnw, ipelvc,
     +                        ipmass, iprhoc, iptran, iptrnp, ipunew,
     +                        ipunewgs, ippsiupw, ipdphidx, ipx, ipw,
     +                        ipxgauss, ipuold, ipuoldm, ipqmat,
     +                        ippsi, ippsix, ippsiksi, ipviscrho )

      end if
      if ( debug )
     +   write(irefwr,1) 'modelv, metupw', modelv, metupw

!     --- Set some constants
!         First multiplication factor for continuity equation
!            in case -1, the matrix is symmetric but has
!            positive and negative eigenvalues
!            In case 1, the matrix is non-symmetric but all
!            eigenvalues are in the positive half plane

      signdiv = -1

!     --- Compute some starting addresses in wrk1

      call elm900address1 ( ipkappa, ipfdiv, ipcoeffgrad,
     +                      ipcoeffdiv, ipetaspecial, ipwork,
     +                      ipwork1, ipomega, ipetha,
     +                      ipgoertler, ipcconv )

!     --- compute basis functions and weights for numerical integration

      call elm800basefn ( coor, wrk1(ipx), wrk1(ipw), wrk1(ipdphidx),
     +                    wrk1(ipxgauss), wrk1(ipphi), index1,
     +                    wrk1(ipqmat), tang, jelp, wrk1(ipphixi),
     +                    work, work, work )
      if ( ierror/=0 ) go to 1000

!     --- Fill variable coefficients if necessary
!         First fill uold and uoldm (for dyn. iterative analyses) if necessary

      call elm900coeffs ( vecold, wrk1(ipuoldm), wrk1, wrk1(ipdphidx),
     +                    index3, index1, ioldm, index4, ipphi, ipugs,
     +                    ipdudx, ipunew, ipdudxnw, ipunewgs,
     +                    wrk1(ipuold), mcont, ipwork, isub, isup,
     +                    iprho, imesh, iuser, user, vecloc, wrk1(ipx),
     +                    wrk1(ipxgauss) )

!     --- Compute the divergence and gradient matrix
!         Store in sp and div
!         pp contains the pressure matrix

      call elm900divgrad ( wrk1(ipsp), wrk1(ipdiv), wrk1(ipdphidx),
     +                     wrk1(ippp), wrk1(ipphi), wrk1(ipw),
     +                     wrk1(ipx), wrk1(ipxgauss), mcont,
     +                     wrk1(iprho), wrk1(ipkappa),
     +                     wrk1(ipwork), rhsdiv, wrk1(ipfdiv), mdiv,
     +                     ishape, wrk1(ipcoeffgrad), wrk1(ipcoeffdiv) )

      if ( jtrans==1 ) then

!     --- Compute the transformation matrix
!         Compute the velocity in the centroid and the derivative of u

         call elm900trans ( wrk1(iptran), wrk1(ipdiv), mdiv, rhsdivtr,
     +                      rhsdiv, wrk1(iptrnp), wrk1(ipsp),
     +                      wrk1(ipuold), wrk1(ipdudx), wrk1(ipphi),
     +                      wrk1(ipdphidx), wrk1(ipugs), mcont, mconv,
     +                      modelv, .false. )

      end if

      if ( modelv/=5 ) then

!     --- Compute effective viscosity in case of modelv # 5

         call elvisc ( modelv, wrk1(ipetha), wrk1(ipetef), cn, clamb,
     +                 wrk1(ipxgauss), wrk1(ipugs), wrk1(ipdudx),
     +                 wrk1(ipseci), wrk1(ipdetd), wrk1(ipwork), vecloc,
     +                 maxunk, numold )

      end if  ! ( modelv/=5 )

      if ( metupw>0 ) then

!     --- Upwind method, compute upwind basis functions

         second = .true.
         call elm800upwbase ( metupw, jelp, wrk1(ipetha),
     +                        wrk1(ipugs), wrk1(ipphi), wrk1(ippsiupw),
     +                        wrk1(ipx), second, wrk1(ipdphidx),
     +                        wrk1(ipdudx), iflipflop, wrk1(ippsix) )

      end if  ! ( metupw>0 )

!     --- if ( debug ) call prinrl ( wrk1(ipetef), 4, 'etaeff/2' )

!         --- Fill parts of matrix if necessary

      if ( matrix ) then

!     --- matrix = true    compute element matrix

         call elm900matrix ( wrk1(ipelem), wrk1(ipdphidx), wrk1(ipphi),
     +                       wrk1(ipxgauss), wrk1(ipetaspecial),
     +                       wrk1(ipw), wrk1(ipwork), wrk1(ipwork1),
     +                       wrk1(ipetef), mcont, wrk1(iprho),
     +                       wrk1(ipomega), mconv, imesh,
     +                       wrk1(ipcconv), wrk1(ipugs), wrk1(ipunewgs),
     +                       wrk1(ipunew), modelv, symm, mcoefconv,
     +                       nveloc, wrk1(ipdudxnw), wrk1(ipdudx),
     +                       wrk1(ipgoertler), wrk1(ipkappa),
     +                       wrk1(ippsiupw) )

      end if

      if ( vector .and. notvec==0 ) then

!     --- vector = true

         call elm900rhsd ( wrk1(ipelvc), wrk1, wrk1(iprhoc),
     +                     wrk1(ipphi), wrk1(2*m+1), wrk1(ipwork),
     +                     wrk1(ipwork+n), wrk1(9*m+1), wrk1(ipcconv),
     +                     wrk1(ipugs), wrk1(ipdudx), wrk1(ipgoertler),
     +                     wrk1(ipkappa), wrk1(ipdetd), wrk1(ipw),
     +                     wrk1(ipdphidx), cn, mconv, jtime, mcoefconv,
     +                     mcont, modelv, modelrhsd, wrk1(ipseci),
     +                     wrk1(ipxgauss), wrk1(ippsiupw) )

      end if  ! ( vector .and. notvec==0 )

      if ( jtime>0 ) then

!     --- jtime>0, i.e. the mass matrix must be computed
!         First compute the mass matrix for one velocity component
!         The non-diagonal mass matrix for this component is stored in
!         wrk1 positions ipwork - ipwork+n*n-1
!         The diagonal matrix in wrk1 pos. ipwork+n*n - ipwork+n*n+n-1

         if ( metupw>0 ) then

!        --- Upwind not yet implemented

            call errchr ( 'Mass matrix', 1 )
            call errsub ( 2873, 0, 0, 1 )

         end if  ! ( metup==0 )

         call elm900massmat ( elemms, wrk1, wrk1(ipwork), wrk1(ipw),
     +                        wrk1(ipphi), wrk1(ipmass),
     +                        wrk1(ipelem), wrk1(ipuoldm), wrk1(ipelvc),
     +                        thetdt, wrk1(ipuold), jtime, mconv,
     +                        wrk1(ipdphidx), wrk1(ipdudx),
     +                        wrk1(ipgoertler), wrk1(ipkappa), index1,
     +                        index3, index4, mcont, wrk1(ipugs),
     +                        vecold, wrk1(ipxgauss), ioldm, index1 )

      end if

      if ( matrix ) then

!     --- Create element matrix for Navier-Stokes equations
!         using separate submatrices

         call  elm900mat ( elemmt, wrk1(ipelcp), wrk1(ipelem),
     +                     wrk1(iptran), wrk1(iptrnp), wrk1(ipsp),
     +                     wrk1(ipdiv), wrk1(ippp), penalp, mcont,
     +                     wrk1(ipw), wrk1(ipx), wrk1(ipxgauss),
     +                     signdiv, npres, nveloc, ipenalty )

      end if

      if ( itype/=901 .and. jtrans==1 .and. imas==2 ) then

!     --- Eliminate centroid in case of 2D quadratic elements
!         The resulting matrix is stored in elemms

          call el2107 ( elemms, wrk1(ipmass), wrk1(iptran),
     +                  wrk1(iptrnp) )

      end if
      if ( vector ) then

!     --- Copy element vector in elemvc

         call elm900vec ( elemvc, wrk1(ipelvc), wrk1(iptrnp), mdiv,
     +                    wrk1(ipelem), rhsdivtr, wrk1(ipsp), npres,
     +                    rhsdiv, signdiv, wrk1(ipw), wrk1(ipx),
     +                    wrk1(ipxgauss), penalp )

      end if

1000  call erclos ( 'elm900' )

      if ( debug ) then

!     --- debug = true

         write(irefwr,*) 'element ', ielem, ' itype ',itype,
     +                   ' jtime ', jtime
         call prinrl(elemvc,icount,'elemvc')
         call prinrl1(elemmt,icount,icount,'elemmt')
         if ( jtime>0 ) then

!        --- jtime>0, mass matrix

             if ( imas==1 ) then

!            --- imas = 1, diagonal mass matrix

               call prinrl(elemms,icount,'elemms')

             else

!            --- imas = 2, non-diagonal mass matrix

                call prinrl1(elemms,icount,icount,'elemms')

             end if
         end if

         call elm900check ( wrk1(ipx), elemmt, npres, nveloc, elemvc )
         write(irefwr,*) 'End elm900'

      end if

      end
