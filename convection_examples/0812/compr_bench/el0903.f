      subroutine el0903 ( iuser, user, jelp, jtime, cn, clamb,
     +                    mconv, mcont, modelv, isub, isup, array,
     +                    thetdt, nveloc, iseqvel, iseqpres, mdiv,
     +                    modelrhsd )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.3  date 30-03-2005 Correction mass matrix
!        version  3.2  date 10-11-2004 Jump in case of error
!        version  3.1  date 16-01-2003 Extension for linear prisms
!        version  3.0  date 08-11-2002 Extra parameters mdiv, modelrhsd
!        version  2.3  date 01-05-2002 Remove some error messages
!        version  2.2  date 23-08-2001 New call to elusr3
!        version  2.1  date 03-09-2000 Extension for scalars
!        version  2.0  date 10-02-1999 New parameter list
!        version  1.3  date 27-03-1998 Do not set ncomp in case of icheli
!        version  1.2  date 02-05-1997 Corrections with respect to
!                                      derivative
!        version  1.1  date 23-01-1997 Extra error message itime=1, tstep=0
!        version  1.0  date 21-12-1996
!
!   copyright (c) 1996-2005  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy,
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     This subroutine performs the initializations for subroutine ELM903 or
!     ELD903 in the case that IFIRST = 0
!     The parameters in the common blocks celint, celiar and celp
!     get a start value
!     Array array is initialized with the values of the coefficients
!     If this subroutine is changed, also adapt el0260
! **********************************************************************
!
!                       KEYWORDS
!
!     element_matrix
!     element_vector
!     navier_stokes_equation
!     initialization
! **********************************************************************
!
!                       MODULES USED
!
      implicit none
! **********************************************************************
!
!                       COMMON BLOCKS
!
      include 'SPcommon/cellog'
      include 'SPcommon/cactl'
      include 'SPcommon/celint'
      include 'SPcommon/celiar'
      include 'SPcommon/cconst'
      include 'SPcommon/celp'
      include 'SPcommon/ctimen'
      include 'SPcommon/cinforel'
      include 'SPcommon/cmcdpi'

! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision user(*), array(*), cn, clamb, thetdt
      integer iuser(*), jelp, jtime, isub, isup, mconv, modelv,
     +        mcont, nveloc, iseqvel(*), iseqpres(*), mdiv, modelrhsd

!     array          o       Real work array in which the values of the
!                            coefficients in the integration points are stored
!                            in the sequence:
!                            In the case of eld903 or elm903:
!                                 2D                  3D
!                            1:   rho              rho
!                            2:   omega            omega
!                            3:   f1               f1
!                            4:   f2               f2
!                            5:                    f3
!                            6:   eta              eta
!                            7:   rho in nodal points (mcont=1 only)
!                            In the case of eli903:
!                            1:   f
!     clamb          o     Parameter lambda with respect to viscosity model
!     cn             o     Power in power law model
!     iseqpres       o     Contains the sequence numbers of the pressure in the
!                          element vector
!     iseqvel        o     Array containing the positions of the velocities in
!                          the element vector in the sequence required by el2005
!     isub           o     Integer help parameter for do-loops to indicate the
!                          smallest coefficient number that is dependent on
!                          space
!     isup           o     Integer help parameter for do-loops to indicate the
!                          largest coefficient number that is dependent on space
!     iuser          i     Integer user array to pass user information from
!                          main program to subroutine. See STANDARD PROBLEMS
!     jelp           o     Indication of the type of basis function subroutine
!                          must be called by ELM100. Possible vaues:
!                           1:  Subroutine ELP610  (Linear line element)
!                           2:  Subroutine ELP611  (Quadratic line element)
!                           3:  Subroutine ELP620  (Linear triangle)
!                           4:  Subroutine ELP622  (Quadratic triangle)
!                           5:  Subroutine ELP621  (Bilinear quadrilateral)
!                           6:  Subroutine ELP623  (Biquadratic quadrilateral)
!                          11:  Subroutine ELP630  (Linear tetrahedron)
!                          12:  Subroutine ELP631  (Quadratic tetrahedron)
!                          13:  Subroutine ELP632  (Trilinear brick)
!                          14:  Subroutine ELP633  (Triquadratic brick)
!     jtime          o     Integer parameter indicating if the mass matrix for
!                          the time-dependent equation must be computed
!                          (>0) or not ( = 0)
!                          Possible values:
!                            0:  no mass matrix
!                            1:  diagonal mass matrix with constant coefficient
!                            2:  diagonal mass matrix with variable coefficient
!                           11:  full mass matrix with constant coefficient
!                           12:  full mass matrix with variable coefficient
!                          101:  refers to the special possibility of the theta
!                                method directly applied
!                          102:  refers to the special possibility of the theta
!                                method directly applied with variable rho
!     mcont          o     Defines the type of continuity equation
!                          Possible values:
!                          0: incompressible flow
!                          1: compressible flow: div (rho u ) = 0
!     mconv          o     Defines the treatment of the convective terms
!                          Possible values:
!                          0: convective terms are skipped (Stokes flow)
!                          1: convective terms are linearized by Picard
!                             iteration
!                          2: convective terms are linearized by Newton
!                             iteration
!                          3: convective terms are linearized by the incorrect
!                             Picard iteration
!                          4: convective terms are linearized by successive
!                             substitution
!     mdiv           o     Defines if divergence right-hand side is used (1)
!                          or not (0)
!     modelrhsd      o    Defines how the stress tensor is treated in the rhs
!                         Possible values:
!                         0:    if there is a given stress tensor the reference
!                                  to this tensor is stored in position 19
!                         1:    It is supposed that there are 6 given stress
!                                  tensor components stored in positions 19-24
!     modelv         o     Defines the type of viscosity model
!                          Possible values:
!                             1:  newtonian liquid
!                             2:  power law liquid
!                             3:  carreau liquid
!                             4:  plastico viscous liquid
!                           100:  user provided model depending on grad u
!                           101:  user provided model depending on the second
!                                 invariant
!                           102:  user provided model depending on the second
!                                 invariant,
!                                 the co-ordinates and the velocity
!                           103:  user provided model depending on the second
!                                 invariant,
!                                 the co-ordinates, the velocity, and old
!                                 solutions
!     nveloc         o     Number of velocity parameters
!     thetdt         o     1/(theta dt) for the case of the theta method
!                          immediately applied
!     user           i     Real user array to pass user information from
!                          main program to subroutine. See STANDARD PROBLEMS
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer ncoef, i, j, isubcf, kchois, mrule, jcheld, ishift
      double precision etha
      logical funchk, compress, debug

!     compress       If true the pressure must be computed
!     debug          If true debug statements are carried out otherwise
!                    they are not
!     etha           Viscosity parameter etha
!     funchk         Indicates whether there are coefficients that must be
!                    evaluated by funcc3 (true) or not (false)
!     i              Counting variable
!     ishift         Shift in coefficient sequence number
!     isubcf         Under bound for coefficients check
!     j              Counting variable
!     jcheld         Value of icheld corrected for different storage schemes
!     kchois         Indication what type of parameters must be computed
!                    Possible values:
!                    0:  Standard method, the calling subroutine is ELM903
!                        It is supposed that an element matrix must be computed
!                    1:  Subroutine ELD903 calls this subroutine, only
!                        derivatives must be computed
!                    2:  Subroutine ELI903 calls this subroutine, only
!                        integrals must be computed
!     mrule          Type of integration rule as given by the user
!     ncoef          Number of coefficients in the equation
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
      integer inirepscal

!     EL0900CONS     Set some constants for subroutines el0900/3/5
!     EL0900INIT     Make some initialization for subroutines el0900/3/5
!                    depending on icheld
!     ELCOMM         Initializes the commons celp and celint with standard
!                    values
!     ELUSR3         Extracts information concerning the coefficients from IUSER
!                    and USER.
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     ERRCHR         Put character in error message
!     ERREAL         Put real in error message
!     ERRINT         Put integer in error message
!     ERRSUB         Error messages
!     INIREPSCAL     Corrects integer value in case of scalars
!     INSTOP         Stop the program
!     PRININ         print 1d integer array
!     PRINLG         Print and checks length of user arrays
!     PRINRL         Print 1d real vector
! **********************************************************************
!
!                       I/O
!
!     The user provides the coefficients through the arrays IUSER and USER
!     Usually they are filled by FILCOF
!     The sequence is for the Navier-Stokes equations is:
!     1:  information about the time-integration
!         Possible values:
!         0: if imas in BUILD=0 then time-independent flow
!            else the mass matrix is built according to imas
!         1: The theta method is applied directly in the following way:
!            the mass matrix divided by theta delta is added to the stiffness
!            matrix
!            the mass matrix divided by theta delta and multiplied by u(n) is
!            added to the right-hand-side vector
!     2:  type of viscosity model used (modelv) as well as given stress
!         tensor as right-hand side accordinag to
!         modelv + 1000*model_rhsd
!         Possible values for modelv
!         0,1:  newtonian liquid
!         2:    power law liquid
!         3:    carreau liquid
!         4:    plastico viscous liquid
!       100:    user provided model depending on grad u
!       101:    user provided model depending on the second invariant
!       102:    user provided model depending on the second invariant,
!               velocity vector and co-ordinates
!         Possible values for model_rhsd:
!         0:    if there is a given stress tensor the reference to this
!               tensor is stored in position 19
!         1:    It is supposed that there are 6 given stress tensor components
!               stored in positions 19-24
!     3:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     4:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-ordinates
!         1: Axi-symmetric co-ordinates
!     5:  Information about the convective terms (mconv)
!         and continuity equation (mcont) according to mconv+10*mcont
!         Possible values for mconv:
!         0: convective terms are skipped (Stokes flow)
!         1: convective terms are linearized by Picard iteration
!         2: convective terms are linearized by Newton iteration
!         3: convective terms are linearized by the incorrect Picard
!            iteration
!         Possible values for mcont:
!         0: Standard situation, the continuity equation is div u = 0
!         1: A special continuity equation div ( rho u ) = 0, which rho
!            variable is solved. This is not the standard continuity equation
!            for incompressible flow. In fact we have a time-independent
!            compressible flow description.
!     6-20: Information about the equations and solution method
!     6:  -
!     7:  density rho
!     8:  angular velocity
!     9:  F1  (Body force in 1-direction)
!    10:  F2  (Body force in 2-direction)
!    11:  F3  (Body force in 3-direction)
!    12:  etha (if modelv = 1, the dynamic viscosity
!                           2, the parameter etha_n
!                           3, the parameter etha_c
!                           4, the parameter etha_pv)
!    13:  n (if modelv = 2-4, the parameter n in the power of the model)
!    14:  lambda (if modelv = 3, the parameter lambda in the viscosity model
!                 if modelv = 4, the parameter s in the viscosity model)
!    15-18: not yet used
!    19:  stress If model_rhsd = 0 then
!                Given stress tensor used in the right-hand side vector
!                At this moment it is supposed that the stress is defined
!                per element and that in each element all components are
!                constant
!                If model_rhsd = 1 then
!                The six components of the stress tensors are stored in
!                positions 19 to 24 in the order: xx, yy, zz, xy, yz, zx
!                This option can not be used in cooporation with the
!                dimensionless coefficients stored in positions 20-41
!     In the case of a call by INTEGR:
!     1:  Type of numerical integration
!         Possible values:
!         0: default value, defined by element
!     2:  Type of co-ordinate system
!         Possible values:
!         0: Cartesian co-orindates
!         1: Axi-symmetric co-orindates
!         2: Polar co-orindates
!     3:  Not yet used (must be 0)
!     4:  f
! **********************************************************************
!
!                       ERROR MESSAGES
!
!      42   Array IUSER(.) has value which is too small
!     238   Omega unequal to zero, whereas axi symmetric elements are used
!     988   itime = 1 in combination with tstep<=0 not allowed
!    1303   Only constants are allowed for this coefficient
!    1305   Coefficient has wrong value
!    1306   Coefficient has wrong value
!    1430   The viscosity parameter eta is non-positive
!    1541   The combination imas = 1 and ishape=4 is not permitted
!    1766   Type of integration rule incorrect
!    1767   Type of co-ordinates incorrect
!    1771   Shape number not yet implemented
!    2024   Option has not been implemented for 3d
! **********************************************************************
!
!                       PSEUDO CODE
!
!   trivial
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
!     --- Fill some standard values in the common blocks celp and celint

      call eropen ( 'el0903' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from el0903'
         write(irefwr,100) 'imas', imas
100      format ( a, 1x, (10i6) )
         call prinin ( iuser, 50, 'iuser' )
         call prinrl ( user, 20, 'user' )

      end if
      if ( ierror/=0 ) go to 1000

!     --- initialize parameters in commons

      call elcomm

!     --- initialize some other parameters

      call el0900cons ( jtime, jconsf, ncomp, ndim, kchois,
     +                  modelv, modelrhsd, mcont, mrule, jcheld,
     +                  icheld, compress )

!     --- extra initializations

      jind = 0
      n = inpelm
      jtrans = 0
      jelp = ishape
      nvert = n
      ishift = 0

!     --- Compute kchois

      call el0900init ( icheld, ncoef, compress, itype, jcheld,
     +                  ielgrp, ix, ndim, jdegfd, ncomp, jderiv,
     +                   kchois, icount, inpelm )

      if ( debug ) write(irefwr,100) 'ncoef, kchois', ncoef, kchois

!     --- Fill the arrays IND, MST and ARRAY with information of the
!         coefficients

      write(6,*) 'iuser/user: '
      do i=1,20
         write(6,*) i,iuser(i),user(i)
      enddo
      if ( ncoef>0 ) then

         ipos = iuser ( ielgrp + 5 )
         write(6,*) 'el0903: ipos, ielgrp ',ipos,ielgrp
         if ( ipos<7 .or. ipos>100000 ) then
            call errint ( ielgrp+5, 1 )
            call errint ( ipos, 2 )
            call errint ( ielem, 3 )
            call errint ( itype, 4 )
            call errint ( ielgrp, 5 )
            call errsub ( 42, 5, 0, 0 )
            go to 1000
         end if

         nuser = 0

         if ( kchois==0 ) then

!        --- Standard situation, Compute the parameters jtime, irule, jcart,
!                                                       mconv, modelv

            jtime = inirepscal ( iuser(ipos) )
        write(6,*) 'el0903: jtime,iuser(ipos): ',ipos,iuser(ipos),jtime
            ipos = ipos + 1
            ishift = ishift+1
            if ( imas==0 ) notmas = 1
            if ( jtime==0 ) then

               jtime = 1+(imas-1)*10

            else

               jtime = 101
               if ( tstep<=0d0 ) then

!              --- jtime>0 and tstep=0

                  call erreal ( tstep, 1 )
                  call errsub ( 988, 0, 1, 0 )

               else

                  thetdt = 1d0 / (theta * tstep)

               end if

            end if
            if ( imas==1 .and. ishape==4 ) then

!           --- error ishape=4 may not be combined with a diagonal mass
!               matrix

               call errint ( ishape, 1 )
               call errsub ( 1541, 1, 0, 0 )

            end if

            modelv = inirepscal ( iuser(ipos) )
            modelrhsd = modelv/1000
            modelv = mod(modelv,1000)
            ipos = ipos + 1
            ishift = ishift+1
            if ( modelv<0 .or. modelv>5 .and. modelv<100 .or.
     +           modelv>103 ) then
               call errint ( 2, 1 )
               call errint ( itype, 2 )
               call errint ( ielgrp, 3 )
               call errint ( ielem, 4 )
               call errint ( modelv, 5 )
               call errchr ( 'modelv', 1 )
               call errsub ( 1305, 5, 0, 1 )
            end if
            modelv = max ( modelv, 1 )
            if ( modelrhsd<0 .or. modelrhsd>1 ) then
               call errint ( 2, 1 )
               call errint ( itype, 2 )
               call errint ( ielgrp, 3 )
               call errint ( ielem, 4 )
               call errint ( modelrhsd, 5 )
               call errchr ( 'modelrhsd', 1 )
               call errsub ( 1305, 5, 0, 1 )
            end if
            if ( icheld>0 .and. jcheld/=6 .and.
     +           jcheld/=11 ) then
               if ( ndim==3 .or. .not. compress ) modelv = 0
            end if

         else if ( kchois==1 ) then
            ipos = ipos + 2
            ishift = ishift+2
         end if

         if ( kchois>=0 .and. kchois<=2 ) then

!        --- Call by BUILD or INTEGR, Compute the parameters jcart and irule

            mrule = inirepscal ( iuser(ipos) )
            ipos = ipos + 1
            ishift = ishift+1
            if ( mrule<0 .or. mrule>4 ) then
               call errint ( mrule, 1 )
               call errint ( 3, 2 )
               call errint ( ielem, 3 )
               call errint ( itype, 4 )
               call errint ( ielgrp, 5 )
               call errsub ( 1766, 5, 0, 0 )
            end if

            jcart = inirepscal ( iuser(ipos) )
            ipos = ipos + 1
            ishift = ishift+1
            if ( jcart<0 .or. jcart>2 ) then
               call errint ( jcart, 1 )
               if ( kchois==0 ) then
                  call errint ( 4, 2 )
               else
                  call errint ( 2, 2 )
               end if
               call errint ( ielem, 3 )
               call errint ( itype, 4 )
               call errint ( ielgrp, 5 )
               call errsub ( 1767, 5, 0, 0 )
            end if
            jcart = jcart + 1
            if ( kchois==2 ) then
               ipos = ipos + 1
               ishift = ishift+1
            end if

         end if

         if ( kchois==0 ) then

!        --- call by BUILD

            mconv = inirepscal ( iuser(ipos) )
            mcont = mconv/10
            mconv = mconv-10*mcont
            if ( mconv<0 .or. mconv>3 .or. mcont>4 ) then

!           --- mconv<0 or mconv>3 or mcont>4
!               Such values are not allowed

               call errint ( 5, 1 )
               call errint ( itype, 2 )
               call errint ( ielgrp, 3 )
               call errint ( ielem, 4 )
               call errint ( mconv+10*mcont, 5 )
               call errchr ( 'mconv', 1 )
               call errsub ( 1305, 5, 0, 1 )

            end if
            ipos = ipos + 1
            ishift = ishift+2
            call elusr3 ( 1, array, iuser, user, ishift )

         end if

      end if

!     --- Set parameters depending on the shape numbers

      if ( mrule>0 ) then
         irule = mrule
      else if ( ishape==3 .and. icheld==0 .or. ishape==10 ) then
         irule = 2
      else if ( ishape==5 .and. icheld==0  .or. ishape==9 .or.
     +          ishape==13 .or. ishape==17 .or.
     +          ishape==4 .and. imas>1 ) then
         irule = 3
      else if ( ishape==11 .and. icheld==0 .or. ishape==18 ) then
         irule = 4
      else if ( ncoef==0 ) then
         irule = 1
      else if ( jcart>1 .and. compress ) then
         irule = 3
      else
         irule = 1
      end if
      if ( irule/=1 .or. (ishape==3 .or. ishape==11 .or.
     +     ishape==13) .and. icheld==0 .or.
     +     ishape>=17 .or. ishape==10 ) jdiag=2
      if ( ishape==3 .or. ishape==10 ) then

!     --- extended linear triangle

         if ( icheld==0 ) then
            n = 4
            if ( ishape==3 ) jtrans = 1
            jelp = 10
         else
            jdercn = 0
         end if
         do i = 1, 4
            do j = 1, 2
               iseqvel((i-1)*2+j) = (i-1)*3+j
            end do
         end do
         do i = 1, 3
            iseqpres(i) = 3*i
         end do

      else if ( ishape==5 .or. ishape==9 ) then

!     --- extended linear quadrilateral

         if ( icheld==0 ) then
            n = 5
            if ( ishape==5 ) jtrans = 1
            jelp = 9
         else
            jdercn = 0
         end if
         do i = 1, 5
            do j = 1, 2
               iseqvel((i-1)*2+j) = (i-1)*3+j
            end do
         end do
         do i = 1, 4
            iseqpres(i) = 3*i
         end do
         call errint ( ishape, 1 )
         call errint ( ielem, 2 )
         call errint ( itype, 3 )
         call errint ( ielgrp, 4 )
         call errsub ( 1771, 4, 0, 0 )

      else if ( ishape==4 ) then

!     --- quadratic triangle

         do j = 1, 2
            do i = 1, 3
               iseqvel((i-1)*4+j) = (i-1)*5+j
               iseqvel((i-1)*4+2+j) = (i-1)*5+3+j
            end do
         end do
         iseqpres(1) = 3
         iseqpres(2) = 8
         iseqpres(3) = 13

      else if ( ishape==6 ) then

!     --- quadratic quadrilateral

         do j = 1, 2
            do i = 1, 4
               iseqvel((i-1)*4+j) = (i-1)*5+j
               iseqvel((i-1)*4+2+j) = (i-1)*5+3+j
            end do
            iseqvel(17) = 21
            iseqvel(18) = 22
         end do
         iseqpres(1) = 3
         iseqpres(2) = 8
         iseqpres(3) = 13
         iseqpres(4) = 18

      else if ( ishape==11 .or. ishape==18 ) then

!     --- extended linear tetrahedron

         if ( icheld==0 ) then
            n = 5
            if ( ishape==11 ) jtrans = 1
            jelp = 18
         else
            jdercn = 0
         end if
         do i = 1, 5
            do j = 1, 3
               iseqvel((i-1)*3+j) = (i-1)*4+j
            end do
         end do
         do i = 1, 4
            iseqpres(i) = 4*i
         end do

      else if ( ishape==13 .or. ishape==17 ) then

!     --- extended linear hexahedron

         if ( icheld==0 ) then
            n = 9
            if ( ishape==13 ) jtrans = 1
            jelp = 17
         else
            jdercn = 0
         end if
         do i = 1, 9
            do j = 1, 3
               iseqvel((i-1)*3+j) = (i-1)*4+j
            end do
         end do
         do i = 1, 8
            iseqpres(i) = 4*i
         end do
         call errint ( ishape, 1 )
         call errint ( ielem, 2 )
         call errint ( itype, 3 )
         call errint ( ielgrp, 4 )
         call errsub ( 1771, 4, 0, 0 )

      else if ( ishape==12 ) then

!     --- quadratic tetrahedron

         do j = 1, 3
            do i = 1, 3
               iseqvel((i-1)*6+j) = (i-1)*7+j
               iseqvel((i-1)*6+3+j) = (i-1)*7+4+j
            end do
         end do
         do i = 19, 30
            iseqvel(i) = i+3
         end do
         iseqpres(1) = 4
         iseqpres(2) = 11
         iseqpres(3) = 18
         iseqpres(4) = 34

      else if ( ishape==14 ) then

!     --- quadratic hexahedron

         do j = 1, 3
            do i = 1, 2
               iseqvel((i-1)*6+j) = (i-1)*7+j
               iseqvel((i-1)*6+18+j) = (i-1)*7+20+j
               iseqvel((i-1)*6+54+j) = (i-1)*7+58+j
               iseqvel((i-1)*6+72+j) = (i-1)*7+78+j
            end do
            iseqvel(3+j) = 4+j
            iseqvel(21+j) = 24+j
            iseqvel(57+j) = 62+j
            iseqvel(75+j) = 82+j
         end do
         do i = 10, 18
            iseqvel(i) = i+2
         end do
         do i = 28, 54
            iseqvel(i) = i+4
         end do
         do i = 64, 72
            iseqvel(i) = i+6
         end do
         do i = 1, 2
            iseqpres(1+(i-1)*4) = 4 +58*(i-1)
            iseqpres(2+(i-1)*4) = 11+58*(i-1)
            iseqpres(3+(i-1)*4) = 24+58*(i-1)
            iseqpres(4+(i-1)*4) = 31+58*(i-1)
         end do

      else if ( ishape==21 .and. icheld>0 ) then

!     --- Linear prism in case of derivatives

         do i = 1, 6
            do j = 1, 3
               iseqvel((i-1)*3+j) = (i-1)*4+j
            end do
         end do
         do i = 1, 6
            iseqpres(i) = 4*i
         end do

      else

!     --- shape number not yet available

         call errint ( ishape, 1 )
         call errint ( ielem, 2 )
         call errint ( itype, 3 )
         call errint ( ielgrp, 4 )
         call errsub ( 1771, 4, 0, 0 )

      end if
      m = n
      if ( jelp==3 .or. jelp==5 ) then
         if ( irule==2 ) then
            m = 1
         else if ( irule==3 ) then
            m = 4
         end if
      else if ( jelp==4 .or. jelp==10 ) then
         if ( irule==2 ) then
            m = 4
         else if ( irule==3 ) then
            m = 7
         end if
      else if ( jelp==9 ) then
         if ( irule==3 ) then
            m = 4
         else if ( irule==4 ) then
            m = 9
         end if
      else if ( ishape==6 ) then
         if ( irule==2 ) then
            m = 4
         else if ( irule==3 ) then
            m = 9
         end if
      else if ( jelp==11 ) then
         if ( irule==2 ) then
            m = 1
         else if ( irule==3 ) then
            m = 4
         else if ( irule==4 ) then
            m = 5
         end if
      else if ( jelp==18 ) then
         if ( irule==3 ) then
            m = 4
         else if ( irule==4 ) then
            m = 5
         end if
      else if ( ishape==13 .or. ishape==17 ) then
         m = 8
      else if ( ishape==14 ) then
         if ( irule==2 ) then
            m = 8
         else if ( irule==3 ) then
            m = 27
         end if
      end if

      mn = m * n
      iphi=1
      igausp = 1
      if ( ishape==4 .or. ishape==10 ) then
         nvert = 3
      else if ( ishape==6 .or. ishape==9 .or. ishape==12 .or.
     +          ishape==18 ) then
         nvert = 4
      else if ( ishape==14 .or. ishape==17 ) then
         nvert = 8
      end if
      nveloc = n*ndim
      npsi = nvert

!     --- Clear array ARRAY

      do i = 1, 32*m
         array(i) = 0d0
      end do

      do i = 1 , ncoef
         call elusr3 ( i, array(1+(i-1)*m), iuser, user, i+ishift )
      end do
      ishift = ishift+ncoef

!     --- Test coefficients

      if ( ind(1)==0 ) then

!     --- rho has not been given a value, set rho equal to 1

         do i = 1, m
            array(i) = 1d0
         end do

      else if ( ind(1)>0 .and. icheld==0 ) then

!     --- rho not constant

         if ( jtime>0 ) jtime = jtime + 1

      end if
      if ( jcart==2 .and. ind(2)/=0 ) then

!     --- omega#0, whereas axi-symmetric elements are used

         call errint ( ielgrp, 1 )
         call errint ( ielem, 2 )
         call errsub ( 238, 2, 0, 0 )
         ind(2) = 0

      end if

      if ( ind(6)<=0 .and. (icheld==0 .or. mod(icheld,10)==6 .or.
     +     mod(icheld,20)==11 .or. icheld==41) .and. modelv<100 )
     +   then

!     --- etha is constant, test whether etha is positive

         etha = array(1+5*m)
         if ( etha<=0d0 ) then

!        --- etha<=0

            call erreal ( etha, 1 )
            call errint ( itype, 1 )
            call errint ( 12, 2 )
            call errint ( ielgrp, 3 )
            call errint ( ielem, 4 )
            call errsub ( 1430, 4, 1, 0 )

         end if

      end if

!     --- Read extra parameters depending on modelv

      if ( modelv>1 .and. modelv<5 ) then

!     --- power law type fluid

         ishift = ishift+1
         call elusr3 ( 7, array(1+6*m), iuser, user, ishift )
         if ( ind(7)>0 ) then

!        --- cn is given as function instead of coefficient

            call errint ( 13, 1 )
            call errint ( itype, 2 )
            call errint ( ielgrp, 3 )
            call errint ( ielem, 4 )
            call errchr ( 'cn', 1 )
            call errsub ( 1303, 4, 0, 1 )

         end if
         cn = array(1+6*m)
         if ( cn<0d0 .or. cn>5.0d0 ) then

!        --- cn out of range

            call errint ( 13, 1 )
            call errint ( itype, 2 )
            call errint ( ielgrp, 3 )
            call errint ( ielem, 4 )
            call erreal ( cn, 1 )
            call errchr ( 'cn', 1 )
            call errsub ( 1306, 4, 1, 1 )

         end if

      else if ( kchois/=2 .and. ncoef<=20 ) then

         ipos = ipos+1

      end if

      if ( modelv>2 .and. modelv<5 ) then

!     --- power law type fluid with two parameters

         ishift = ishift+1
         call elusr3 ( 7, array(1+6*m), iuser, user, ishift )
         if ( ind(7)>0 ) then

!        --- clamb is given as function instead of coefficient

            call errint ( 14, 1 )
            call errint ( itype, 2 )
            call errint ( ielgrp, 3 )
            call errint ( ielem, 4 )
            if ( modelv==3 ) then
               call errchr ( 'lambda', 1 )
            else
               call errchr ( 's', 1 )
            end if
            call errsub ( 1303, 4, 0, 1 )

         end if
         clamb = array(1+6*m)
         if ( clamb<0d0 ) then

!        --- clamb out of range

            call errint ( 13, 1 )
            call errint ( itype, 2 )
            call errint ( ielgrp, 3 )
            call errint ( ielem, 4 )
            call erreal ( clamb, 1 )
            if ( modelv==3 ) then
               call errchr ( 'lambda', 1 )
            else
               call errchr ( 's', 1 )
            end if
            call errsub ( 1306, 4, 1, 1 )

         end if

      else if ( kchois/=2 .and. ncoef<=20 ) then

         ipos = ipos+1

      end if

!     --- Allow reading of coefficients for RHS of continuity equation,
!         stress-RHS, and KAPPA
!         Skip over imesh

      if ( kchois/=2 .and. ncoef<=20 ) ipos = ipos+1

      if ( mcont==4 ) then

!     --- continuity equation with pressure term
!         Increase ISHIFT with 2, as ICOEF15 is stepped over

         ishift = ishift+4
         do i = 7, 8
            call elusr3 ( i, array(1+(i-1)*m), iuser, user, ishift-7+i )
         end do
         ncoef = 8

      else if ( kchois/=2 .and. ncoef<=20 ) then

         ipos = ipos+2

      end if

!     --- Right-hand side for continuity equation

      if ( ncoef>=6 .and. ncoef<=20 ) then
         ishift = ishift+2
         i = 9
         call elusr3 ( i, array(1+(i-1)*m), iuser, user, ishift )
      end if
      if ( ind(9)/=0 ) then

!     --- The right-hand side for the continuity equation is not equal to zero

         mdiv = 1
         ncoef = 9

      else

         mdiv = 0

      end if

!     --- Given stress tensor for the right-hand side

      if ( ncoef>=6 .and. ncoef<=20 ) then

         if ( modelrhsd>=1 ) then
            isup = 15
         else
            isup = 10
         end if
         do i = 10, isup

            call elusr3 ( i, array(1+(i-1)*m), iuser, user, 9+i )

            if ( ind(i)/=0 ) then

!           --- Given stress tensor for the right-hand side

               ncoef = i
               if ( isup==10 .and. (ndim==3 .or. jcart>2) ) then

!              --- Not yet implemented

                  call errsub ( 2024, 0, 0, 0 )

               end if

            end if

         end do  ! i = 10, isup

      end if

      if ( ncoef>0 ) then

!     --- Test array lengths, number of nodes and number of degrees of freedom

         call prinlg ( 'iuser', ipos-1, user, iuser )
         call prinlg ( 'user', -nuser, user, iuser )

      end if

!     --- Check which parameters depend on the space co-ordinates
!         Compute isub and isup

      isubcf = 1
      if ( icheld>0 ) isubcf = 6

      do i = isubcf , ncoef
         isub = i

!        --- If ind(i)>0 then paramater i corresponds to a function

         if ( ind(i)>0 ) goto 170

      end do

      isub = ncoef + 1

170   isup = ncoef
      do i = ncoef, isub , -1
         isup = i
         if ( ind(i)>0 ) goto 190
      end do

!     --- Check for double parameters (functions) to avoid calls for
!         function subroutines
!         ist is used to store the references for the double parameters
!         Set ind(50)

190   do i = isub, isup
         ist(i) = 0
      end do

      if ( icheld>0 ) then

!     --- Compute derivative quantities

         ind(50) = 1
         jderiv = 1

      else if ( icheld==-1 .or. icheld==-2 .or. icheld==-6 ) then

!     --- icheld = -1 or -2, do not compute derivatives

         ideriv = 0
         jderiv = 0

      else if ( icheld<-2 ) then

!     --- icheld<-2, derivatives should be computed

         jderiv = -icheld+3

      else if ( mconv>0 .or. modelv>1 .or. jtime>0 ) then

!     --- the convective terms must be taken into account and/or the
!         viscosity model depends on the gradient of the velocity and/or
!         the theta method must be applied

         ind(50) = 1
         jderiv = 1

      else

!     --- Stokes flow with Newtonian viscosity model

         ind(50) = 0

      end if

      funchk = .false.
      do i = isub, isup
         if ( ind(i)>0 .and. ind(i)<2000 ) then

!        --- only the function calls are checked

            do j = i+1,isup
               if ( ind(j)==ind(i) ) then
                  ist(j) = i
                  ind(j) = 0
               end if
            end do
            igausp = 1
         end if
         if ( ind(i)>1000 .and. ind(i)<=2000 ) ind(50) = 1
         if ( ind(i)>10000 ) funchk = .true.
         if ( ind(i)>1000 .and. ind(i)<=2000 .or. ind(i)>10000 )
     +        igausp=1
      end do

      if ( icheld<-1 .or. icheld>0 ) ind(50) = 1
      if ( funchk .or. modelv==103 ) ind(50) = ind(50) + 2

      if ( ierror/=0 ) call instop

1000  call erclos ( 'el0903' )
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End el0903'

      end if

      end
