c *************************************************************
c *   COMPR
c *
c *   Convection in axisymmetric spherical geometry or cylindrical geometry.
c *   Simple driver program that calls initialization and
c *   solution routines for steady state or time-dependent convection.
c *
c *   PvK 072100/030904
c * 
c *   PvK 072904
c *   Modified to use markerchain for buoyancy rather than volumetric tracers.
c *
c *   PvK 071405
c *   Adapted to do both cylindrical/spherical and cartesian geometry
c *************************************************************
      program compr
      implicit none
      integer NBUFDEF,NUM,NPMAX
c     *** NBUFDEF : size of main memory buffer (ibuffr/buffr)
c     *** NPMAX   : maximum number of points in the mesh
c     *** NUM     : size of user() array, based on NPMAX
      parameter(NBUFDEF=500 000 000,NPMAX=500 000,NUM=7*NPMAX+15)

      real*8 user(NUM)
      integer iuser(100)

c     *** COMMON DECLARATIONS
      integer ibuffr
      common ibuffr(NBUFDEF)

      include 'pesteady.inc'
      include 'tracer.inc'
 
      data user(1),iuser(1)/NUM,100/

      call compr_start(iuser,user,NBUFDEF,NPMAX)

      if (steady) then
         call steady_iteration(iuser,user)
      else 
         call time_integration(iuser,user)
      endif

      end

c *************************************************************
c *   STEADY_ITERATION
c *
c *   PvK 071000
c *************************************************************
      subroutine steady_iteration(iuser,user)
      implicit none
      integer iuser(*)
      real*8 user(*)
   
      integer ibuffr
      common ibuffr(1)
      include 'mysepar.inc'
      include 'c1visc.inc'
      include 'pesteady.inc'
      include 'cpephase.inc'
      include 'ccc.inc'
      include 'pecof900.inc'
      include 'bound.inc'

      real*4 second
      real*4 cpustart,cpunow,t10,t11
      integer iinbld(10),matrm(5),irhs1(5),iall,ivisdip(5)
      integer ielhlp,iinder(10),npoint,DONE,ikelmi,iqtype
      integer iinvec(20),ihelp,ibp,ip
      real*8 phiav,heatdummy,rinvec(20),vrms,vrms2,p,q
      parameter(DONE=1d0)
      save iinbld,matrm,irhs1,ivisdip,iinder

      niter=0
      cpustart=second()
100   continue
        niter=niter+1
   
        call copyvc(isol1,islol1)
c       call phifromsepran(kmesh1,kprob1,isol1,ivisdip)
        call phifromgrad(kmesh1,kprob1,isol1,ivisdip)

c       **** Heat equation
        call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v          isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user)
        call bmheat(kmesh2,kprob2,intmt2,iuser,user,
     v               isol2,islol2,matr2)
        if (relax.ne.0) then
           call algebr(3,0,islol2,isol2,isol2,kmesh2,kprob2,
     v              1d0-relax,relax,p,q,ip)
        endif
        call stokes(1,iuser,user)

        call intermediate_output(kmesh1,kmesh2,kprob1,kprob2,
     v     isol1,isol2,islol1,islol2,iheat,idens,
     v     ivisc,isecinv,iuser,user)

        cpunow = second()
        if (niter.lt.nsteadymax.and.dif.gt.eps) goto 100

1000  continue
      write(6,*) 'before compr/bmout: ',iuser(1),user(1)
      call bmout(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v             isol3,iuser,user)

      return
      end

c *************************************************************
c *   TIME_INTEGRATION
c *
c *   Do time integration of dynamical equations
c *
c *   Do for NOUT steps
c *      tout = tout+dtout
c *      While (t<tout)
c *          compute CFL time step 
c *          Integrate over new time step
c *          output intermediate information
c *      end
c *      Output plots, write solution to file etc.
c *   Done
c *
c *   PvK 071900
c *************************************************************
      subroutine time_integration(iuser,user)
      implicit none
      integer iuser(*)
      real*8 user(*)

c     *** NMAX: maximum number of iterations in the time integration loop
      integer NMAX 
      parameter(NMAX=10 000)
 
      integer ibuffr(1)
      common ibuffr
      real*8 buffr(1)
      equivalence(ibuffr(1),buffr(1))
 
c     **** COMMON DECLARATIONS
      include 'SPcommon/cbuffr'
      include 'SPcommon/cmacht'
      include 'SPcommon/ctimen'
c     *** /mysepar/ most sepran arrays (kmesh/kprob etc).
      include 'mysepar.inc'
c     *** /petrac/ tracer logic
      include 'petrac.inc'
c     *** /tracer/ more tracer logic
      include 'tracer.inc'
c     *** /degas/ specification of degassing zones
      include 'degas.inc'
c     *** /cont/ specification of continent formation zones
      include 'pecont.inc'
c     *** /pecofx00/ specification of coefficients for problem 800 and 900
      include 'pecof900.inc'
      include 'pecof800.inc'
c     *** /powerlaw/ specification of powerlaw coefficients
      include 'powerlaw.inc'
c     *** /peiter/ logic for integration
      include 'peiter.inc'
c     *** /cpix/ information on pixel grid for raster output
      include 'cpix.inc'
c     *** /c1visc/ viscosity information
      include 'c1visc.inc'
c     *** /vislo/ more viscosity information
      include 'vislo.inc'
c     *** /petime/ time integration info
      include 'petime.inc'
c     *** /cpephase/ phase transformation info
      include 'cpephase.inc'
c     *** /depth_thermodyn/ variable alpha, kappa
      include 'depth_thermodyn.inc'
c     *** /dimensional/ info about scaling to dimensional parameters
      include 'dimensional.inc'
c     *** /compression/ information on compressibility
      include 'compression.inc'
c     *** /coolcore/ use thermal cooling of the core?
      include 'coolcore.inc'
c     *** /averages/ info on computing averages for output
      include 'averages.inc'
c     *** /perealdata/ storage for real*4 data
      include 'perealdata.inc'
c     *** /ccc/ /bound/ boundary and curve info
      include 'ccc.inc'
      include 'bound.inc'
c     *** /solutionmethod/ type of solution method (direct/iterative)
      include 'solutionmethod.inc'
c     *** /cimage/ info on pixel grid
      include 'cimage.inc'
c     *** /penoniter/ info on non-Newtonian iteration
      include 'penoniter.inc'
c     *** /colimage/ more image info
      include 'colimage.inc'
c     *** /bstore/ name of backingstorage file
      include 'bstore.inc'
      include 'csky.inc'

c     *** LOCAL VARIABLES
      real second,t00,t01,t11,t10,cpu
      real*8 tnew,contln(10),format,yfaccn,t_d
      integer niter,inout,iall,irhs2(5),matrm(5),jsmoot,numarr
      integer inbetween,ibp,imoved,npoint,ielhlp,ipoint,icurv,iout
      real*8 rmax,subdif,anorm,rmax2,p,q,veloc(20),veloc_d(20)
      logical output
      character*80 fimage,command,rname
      integer iremarkstep,nq_a
      real*8 q_a(20),q_a_d(20),new_Ra
      

      save irhs2,matrm,iremarkstep

      niter = 0
      iremarkstep=0
      t=0
      cpu=0
      nitermax = NMAX
      inout = 1
      inbetween = 0
      imoved = 0
      npoint = kmesh1(8)

      t00 = second()
      tout = t + dtout
      output = .false.

      write(6,*) 'Start time integration '
      call copyvc(isol1,islol1(1,1))
      call stokes(1,iuser,user)
      call nusselt(kmesh2,kprob2,isol2,iuser,user,q_a,nq_a,q_a_d)
            write(6,'(''       conductive solution q_a        : '',
     v                      3f8.2)') q_a(1),q_a(2),q_a(3)


      if (ifollowdegas.gt.0.and.vardegaszones) call var_degaszones(0)
100   continue
         niter = niter+1
         
c        *** calculate time step; pefilcof stores velocity in user
c        *** which then is used in pedtcf.
         call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v              isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user)
         call pedtcf(kmesh2,user,dtcfl)
c        *** based time step on CFL criterion
         tstep = dtcfl * tfac
c        *** make sure new time doesn't exceed output limits or maximum time
         tnew = t+tstep
         if (tnew.ge.tout) then
            tstep = tout-t
            t = tout
            output = .true.
         else if (tnew.ge.tmax) then
            tstep = tmax-t
            t = tmax
            output=.true.
         else
            t = tnew
            output = .false.
         endif
         if (variable_Ra) then
            Ra=min(Ra_max,new_Ra(t))
c           write(6,'(''changed Ra to '',4e15.4)') t,Ra
         endif
c        write(6,*) 'tstep: ',tstep,tnew

c        *** predict position of markers
         if (itracoption.ne.0) then
            t10 = second()
            call tracvel(kmesh1,kprob1,isol1,user,1)
            call predcoort(coormark,velmark,coornewm)
            t11 = second()
c           write(6,*) 'cpu(tracvelc+predcoort): ',t11-t10
         endif

c        *** Predict temperature field
         if (Ra.gt.0) then
            t10 = second()
            call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v               isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user) 
            call ieulh(kmesh2,kprob2,intmt2,isol2,islol2,user,
     v                 iuser,matr2,matrm,irhs2)

c sky
c           *** implement "Lenardic filter"
            if (sky_filter) call skyfilter(kmesh2,isol2)
            t11 = second()
         endif

         if (ncor.eq.0) then
c           *** copy or regrid marker coordinates from coornewm to coormark
            if (itracoption.eq.1) call copcoor(1)
            if (itracoption.eq.2) call remarker

         else if (ncor.eq.1) then

           nsub(1)=0
c          *** Compute velocity that is consistent with current T
           t10 = second()
           call copyvc(isol1,islol1(1,1))
           call stokes(2,iuser,user)
           t11 = second()
           if (itracoption.ge.1) then
c             *** correct position of markers using 4th order RK
              call algebr(3,0,isol1,islol1,islol1,kmesh1,kprob1,
     v           0.5d0,0.5d0,p,q,ipoint)
              call mark4(kmesh1,kprob1,isol1,islol1,user)
c             *** copy or regrid marker coordinates from coornewm to coormark
              if (itracoption.eq.1) call copcoor(1)
              if (itracoption.eq.2) call remarker
           endif

c          *** correct temperature field
           if (Ra.gt.0) then
              t10 = second()
              call pefilcof(2,kmesh1,kmesh2,kprob1,kprob2,isol1,
     v               isol2,islol2,iheat,idens,ivisc,isecinv,iuser,user)
              call corrh(kmesh2,kprob2,intmt2,isol2,islol2,
     v              user,iuser,matr2,matrm,irhs2)

c             *** implement "Lenardic filter"
              if (sky_filter) call skyfilter(kmesh2,isol2)
              t11 = second()
           endif
         endif

c        *** update velocity using updated temperature (and tracers if necessary)
         nsub(2)=0
         t10=second()
         call stokes(1,iuser,user)
         t11 = second()
         if (ifollowchem.ne.0) then
            call updatechem(coormark,chemmark,kmesh2,kprob2,isol2,
     v             nchem,NTRACMAX)
         endif

c        *** update t_bot if thermal evolution is simulated
         if (cool_core) call coolcore(kmesh2,kprob2,isol2,iuser,user)

c        *** intermediate output
         call time_int_output(kmesh1,kmesh2,kprob1,kprob2,
     v        isol1,isol2,islol1,islol2,iheat,idens,icompwork,
     v        ivisc,isecinv,iuser,user) 

         if (output) then
c           *** 'large' output only at each output time interval
c           *** Make image file
            inbetween=inbetween+1
            t_d = t/tscale_dim
            write(6,'(''  output at time='',e15.7,''('',e15.7,'' Byr)'',
     v        '' of image'',i4)') t,t_d,inout
            write(fimage,'(''images/temp.'',i5.5)') inout

            call image(kmesh2,kprob2,isol2,fimage)

            if (inbetween.ge.nbetween) then
            write(6,'(''  output at time='',e15.7,''('',e15.7,'' Byr)'',
     v        '' of plot '',i4)') t,t_d,inout/nbetween
              call peplotit(kmesh1,kmesh2,kprob1,kprob2,
     v              isol1,isol2,islol1,
     v              ivisc,iheat,isecinv,iuser,user,inout/nbetween) 
              write(f2name,'(''solutions/sol.'',i3.3)') inout/nbetween
              namef2=f2name
              call openf2(.true.)
              call writbs(0,1,numarr,'temp',isol2,kprob2)
              call writb1
              if (itracoption.ge.1) then
                 call tracerout(coormark,coorreal,inout/nbetween)
              endif
              if (itracoption.eq.2) then
                 iout=-1
                 write(rname,'(''tracerpix.'',i3.3)') inout/nbetween
                 call mardiv(coormark,rname,iout)
              endif
              if (ifollowchem.ne.0) then
                 call chemout(chemmark,coorreal,nchem,inout/nbetween)
              endif
              call GMTout(kmesh2,kprob2,isol2,inout/nbetween)

              if (rav_output) then
                call averages(kmesh1,kmesh2,kprob1,kprob2,isol1,isol2,
     v                       user,nth_output,nr_output,inout/nbetween)
              endif

              close(2)
              inbetween = 0
            endif

            f2name='restart.back'
            namef2=f2name
            call openf2(.true.)
            call writbs(0,1,numarr,'temp',isol2,kprob2)
            call writbs(0,2,numarr,'velo',isol1,kprob1)
            call writb1
            close(2)


            tout=t+dtout
            inout=inout+1
            output=.false.
            if (t.lt.tmax) 
     v        write(6,*) 'next output at time t=',tout,tout/tscale_dim 

c           call flushdatafile

c           *** At the end of every IMOVECONTth output time step:
c           *** move the continent formation zone.
            if (imovecont.ge.1.and.
     v           itracoption.ge.1.and.ncontzone.ge.1) then
               imoved = imoved + 1
               if ((imoved/imovecont).eq.1) then
                  call movecont(dtout)
                  imoved = 0
               endif
            endif
            if (ifollowdegas.gt.0.and.vardegaszones) 
     v               call var_degaszones(1)


         endif

      if (t.lt.tmax) then
         goto 100
      else
         write(6,*) 'Time integration stopped because ',
     v      't>tmax: ',t.ge.tmax
      endif

      return
      end

      real*8 function new_Ra(t)
      implicit none
      real*8 t
      new_Ra = 1e4*exp(100*t)
      return
      end

