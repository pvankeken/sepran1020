program stokes
call sepcom(NBUFDEF)

end program stokes

real(kind=8) function funccf(ichoice,x,y,z)
implicit none
integer :: ichoice
real(kind=8) :: x,y,z
real(kind=8), parameter :: pi=3.1415926538_8

funccf=1d3*(1-y+0.1*cos(pi*x)*sin(pi*y))

end function funccf
