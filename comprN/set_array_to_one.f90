subroutine set_array_to_one(npoint,sepranvector)
implicit none
integer,intent(in) :: npoint
real(kind=8),intent(inout) :: sepranvector(*)
integer :: i

sepranvector(1:npoint) = 1e0_8

end subroutine set_array_to_one
