! PvK July 2024
subroutine read_namelist_input
use sepmodulecomio
use control
use coeff
use geometry
use convparam
implicit none
integer :: open_status,jtypv,iiqtype,iiiqtype
namelist /comprN_nml/ isolmethod9,jtypv,intrule900,intrule800,interpol900,interpol800,icoor800,icoor900, &
     & mcontv,penalty_parameter,cyl,Ra,wavel_perturb,ampini_perturb,itype_stokes,irestart,Tstartfile,UVstartfile, &
     & eps_convergence,b_eta,c_eta,relax,Di,nsteady_max,r1,r2,tosi15,tosi15_case,steady,sigma_y,eta_star,read_velocity, &
     & itop,ibottom,isideboundary,ipetsc8,ipetsc9,isolmethod8,isolmethod9,imatrix8,imatrix9,axi,cyl,nph,tfac,dtout_d,tmax_d, &
     & ncor,ibench_type,krestart,iiqtype,q_layer_d,insulbot,delta1K

krestart=-1
itype_stokes=900 ! set penalty function method as default 
isolmethod9=0 ! direct solution method
itypv=0 ! isoviscou
intrule900=0 ! let sepran decide on integration rule
intrule800=0
interpol900=0 ! no interpolation P2->P1
interpol800=0
icoor800=0 ! Cartesian coordinates
icoor900=0
mcontv=0 ! Boussinesq
penalty_parameter=1e-6_8 ! penalty function parameter
cyl=.false. ! Cartesian 
print_node=.true.  ! make sure to modify this in parallel
Ra=1e4  
wavel_perturb = -1.0_8 ! default: makes wavelength twice the size of the box
ampini_perturb=0.05_8
Tstartfile='T_start.nf'
UVstartfile='UV_start.nf'
eps_convergence=1e-4_8
relax=0.0_8
Di=0.0_8
nsteady_max=20
r1=0.0_8
r2=1.0_8
steady=.true.
read_velocity=.false.
itop=3
ibottom=1
isideboundary=2
axi=.false.
cyl=.false.
ibench_type=0
tfac=-0.5
dtout_d=0.01
tmax_d=1
ncor=1
iiqtype=0
delta1K=.false. ! if true scale temperature by 1K
Kequivalent=.false. ! if true temperature is numerically equivalent to dimensional T in K
deltaT_dim=3000.0_8

itypv=0
jtypv=0
ivl=.false.
ivt=.false.
ivn=.false.
tosi15=.false.
tosi15_case=0
tackley=.false.
b_eta=0.0_8
c_eta=0.0_8
sigma_y=0
eta_star=0
ipetsc8=0
ipetsc9=0
imatrix8=2
imatrix9=1
isolmethod8=0
isolmethod9=0
q_layer_d=0.0_8
q_layer=0.0_8

open(lu_nml,file='comprN.nml',iostat=open_status)
if (open_status /= 0) then
   if (print_node) write(irefwr,*) 'PERROR(readname_list): error on opening comprN.nml: ',open_status
   call instop
endif
read(lu_nml,NML=comprN_nml)
close(lu_nml)

if (jtypv>0) then
   itypv=jtypv-(jtypv/100)*100
   tackley=(jtypv>=100)
else
   itypv=jtypv
   tackley=.false.
endif

qwithrho=(iiqtype/100==1)
iiiqtype=iiqtype-(iiqtype/100)*100
qnondim=(iiiqtype/10)==1
iqtype=iiqtype-(iiqtype/100)*100-(iiiqtype/10)*10
if (iqtype == 2 .and. cyl) then
   if (print_node) then
     write(irefwr,*) 'Need to modify code for layer dependent heating'
     write(irefwr,*) ' in cyl '
   endif
   stop
endif
if (iqtype < 0 .or. iqtype > 5) then
   if (print_node) then
     write(irefwr,*) 'PERROR(compr_start): iqtype has incorrect value'
     write(irefwr,*) ' iqtype = ',iqtype
   endif
   stop
endif

if (iqtype==1) then
   if (qnondim) then
      q_layer=q_layer_d
   else
      q_layer=q_layer_d*QBSE_DIM*height_dim*height_dim/(cp_dim*rkappa_dim*deltaT_dim)
   endif
endif

if (print_node) write(irefwr,920) iqtype,qwithrho,qnondim,q_layer_d(1),q_layer(1)






!write(6,*) 'penalty_parameter: ',penalty_parameter
920   format('iqtype ................................... ',i10,/, 'Multiply heatproduction with rho(z)? ..... ',L10,/, &
     &       'Is q_layer nondimensional? ............... ',L10,/,'q_layer_d/q_layer .......................... ',2f8.3:)
910   format('nlay ..................................... ',i8,/,8x, 'zint_d ................................... ',10e10.3:)
911   format(8x,'r_zint ................................... ',10f10.3:)
1112  format(/,'Tracer option .................... ',i10,/, 'Number of tracer distributions.... ',i12,/, &
     &         'Choice for following chemicals.... ',i12,/, 'Distance between particles ....... ',f12.7,/, &
     &         'Minimum distance between particles ',f12.7,/, 'Take compressiblity into account?  ',L12)
1115  format(/'Extended Boussinesq parameters: ',/, '    Di    ..................... ',f8.3,/, &
     &        '    Grueneisen parameter ...... ',f8.3,/, &
     &        '    deltaT_dim ................ ',f8.3,/, &
     &        '    Number of phase changes ... ',i8)

end subroutine read_namelist_input
