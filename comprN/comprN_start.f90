! PvK July 2024
subroutine comprN_start()
use sepmodulekmesh
use sepmoduleoldrouts
use sepmodulevecs
use sepran_arrays
use control
use convparam
use coeff
use geometry
use mparallel
use mtime
implicit none
integer :: allocate_status,num_user_here,inputcr(10)=0
real(kind=8) :: rinputcr(10)=0.0_8,u1lc(10)=0.0_8
integer :: ielhlp=0,iu1lc(10)=0,commat_in(20)

! Check whether MPI parallelism is used outside of Sepran
call check_parallel

cpustart=second()
t1=cpustart

! set defaults for parameters in fillcoef8/900 and update by namelist input
call read_namelist_input()

! few sanity checks and limitations compared to comprP
! Note - before sepstr 'print_node' and 'irefwr' are not yet defined
if (axi) then
   write(6,*) 'PERROR(comprN_start): not suited for axi'
   call instop
endif

if (mcontv /= 0) then
   write(6,*) 'PERROR(comprN_start): not yet suited for compressible convection',mcontv
   call instop
endif


   

! start sepran, read mesh & problem information, set up structure of matrices
call sepstr(kmesh,kprob,intmat)
if (myid==0) irefwr=6

if (cyl) then
   if (print_node) then
      write(irefwr,*) '*******************************'
      write(irefwr,*) '*    CYLINDRICAl GEOMETRY     *'
      write(irefwr,*) '*******************************'
   endif
else
   if (print_node) then
      write(irefwr,*) '*******************************'
      write(irefwr,*) '*     CARTESIAN GEOMETRY      *'
      write(irefwr,*) '*******************************'
   endif
endif

read_velocity=.true.
if (krestart<0) then
   irestart=krestart
else
  if (krestart>=1000) read_velocity=.false.
  irestart = krestart - 1000*(krestart/1000)
endif
restart = (irestart==1).or.(irestart==2)

tmaxp = tmax_d * tscale_dim
dtoutp = dtout_d * tscale_dim
if (print_node) then
   write(irefwr,'(''Dimensional model time (Byr) : '',f15.7)') tmax_d
   write(irefwr,'(''Non-dimensional model time   : '',f15.7)') tmaxp
   write(irefwr,'(''Dimensional output time (Byr): '',f15.7)') dtout_d
   write(irefwr,'(''Non-dimensional output time  : '',f15.7)') dtoutp
endif
 


! create viscosity vector after temperature has been initialized
rinputcr=0.0_8
inputcr(1)=7
inputcr(2)=1 ! number of vectors to be created
inputcr(3)=2 ! ICHVC type of vector (2=special structure)
inputcr(4)=1 ! IPROB
inputcr(5)=1 ! IVEC type of special structure (1= one dof per nodal point)
inputcr(6)=0 ! ICOMPL
inputcr(7)=0 ! IFILL (0=create vector and set values to zero)
call creatv(kmesh,kprob,ivisc,inputcr,rinputcr)

! set up iadia, idens, iviscplast, ivisclin just for compatibility with future extension / merger with comprP
!inputcr(4)=2 ! iprob
 call creatv(kmesh,kprob,isecinv,inputcr,rinputcr)
 call creatv(kmesh,kprob,iviscplast,inputcr,rinputcr)
 call creatv(kmesh,kprob,ivisclin,inputcr,rinputcr)
 call creatv(kmesh,kprob,iplasticity,inputcr,rinputcr)
u1lc(1) = 0
 u1lc(1) = 0d0
! ichcrv = ichcr + (iprob-1)*1000
call creavc(0,1,1,isol(1),kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,isol(2),kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,iphi,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,iphi2,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,ivisdip2,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,ivisdip,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,iadia,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,ipress,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,iheat,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)
call creavc(0,1001,1,icompwork,kmesh,kprob,iu1lc,u1lc,iu1lc,u1lc)


! clearly I don't understand the creatv manpage because this doesn't work:
!inputcr(7)=3 ! IFILL, use next two positions to indicate what
!inputcr(8)=0 ! prescribe all degrees of freedom
!inputcr(9)=-1 ! fill with constant value of rinputcr(-1)
!inputcr(10)=0 ! use all nodes
!rinputcr(1)=1.0_8
!so just fill with zeros and then adjust
call creatv(kmesh,kprob,idens,inputcr,rinputcr)
call creatv(kmesh,kprob,irho,inputcr,rinputcr)
call creatv(kmesh,kprob,ialpha,inputcr,rinputcr)
call set_array_to_one(npoint,ks(idens)%sol)
call set_array_to_one(npoint,ks(irho)%sol)
call set_array_to_one(npoint,ks(ialpha)%sol)


call determine_geometry()

call initialize_temp_vel()

! set up space for coefficients in user_here
num_user_here=10+7*npoint
allocate(user_here(num_user_here),stat=allocate_status)
if (allocate_status /= 0) then
   write(6,*) 'PERROR(simple): error in allocating user_here'
   call instop
endif
iuser_here(1)=NUSERMAX
user_here(1)=num_user_here

! set b.c. for both problems
call presdf(kmesh,kprob,isol)

! Redefine matrix structure  and create intmat2
commat_in=0
commat_in(1)=14  ! number of entries in commat_in
commat_in(2)=imatrix9 ! row compact storage for petsc (55=row compact; 56=row compact, no sepran mat)
commat_in(3)=0
commat_in(4)=0
commat_in(5)=1  ! iprob = problem number 
commat_in(13)=ipetsc9  ! use_petsc=petsc (2=mumps)
if (print_node) write(irefwr,'(''commat: '',14i5)') commat_in(1:14)
call matstruc(commat_in,kmesh,kprob,intmat(1))
commat_in(1)=14  ! number of entries in commat_in
commat_in(2)=imatrix8 ! row compact storage for petsc (55=row compact; 56=row compact, no sepran mat)
commat_in(3)=0
commat_in(4)=0
commat_in(5)=2  ! iprob = problem number 
commat_in(13)=ipetsc8  ! use_petsc=petsc (2=mumps)
if (print_node) write(irefwr,'(''commat: '',14i5)') commat_in(1:14)
call matstruc(commat_in,kmesh,kprob,intmat(2))

end subroutine comprN_start

