subroutine determine_geometry()
use sepmodulecomio
use control
use coeff
use sepran_arrays
use geometry
use tracers
implicit none
integer :: iuserh(100),ihelp,pvk_ishape
real(kind=8) :: userh(100),volint

if (cyl) then
   if (print_node) write(irefwr,*) 'PERROR(determine_geometry): not yet suited for cyl'
   call instop
endif
iuserh=0
 userh=0.0_8
iuserh(1)=100
 userh(1)=1.0_8*100

iuserh(2)=1
iuserh(6)=7
iuserh(8)=icoor800
iuserh(9)=0
iuserh(10)=-6
 userh(6)=1.0_8
volume=volint(0,1,1,kmesh,kprob,isol(2),iuserh,userh,ihelp)

if (.not.cyl) then
  pvk_ishape=2
  call pefilxy(pvk_ishape)
  rlampix = (xcmax-xcmin)/(ycmax-ycmin)
  ! default perturbation has half wavelength of the width of the box
  if (wavel_perturb<0) wavel_perturb=rlampix
! write(6,*) 'wavel_perturb=',wavel_perturb
! call instop
  frac=1d0
  if (print_node) then
     write(irefwr,'(''icoorsystem            : '',2i10)') icoorsystem, icoor900
     write(irefwr,'(''volume                 : '',f8.4)') volume
     write(irefwr,'(''surface area           : '',2f8.4)') surf(1),surf(2)
     write(irefwr,'(''rlampix, dx_two_elem   : '',2f8.4)') rlampix,dx_two_elements
     write(irefwr,'(''wavel_perturb          : '',f8.4)') wavel_perturb
  endif


endif !cyl

if (print_node) then
   call system('mkdir -p GMT PLOTS VTK solutions Tracers')
endif

end subroutine determine_geometry

