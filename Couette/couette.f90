! Find solution to Couette flow with V(z=0)=V0 and V(z=L)=0 as boundary conditions using a simple shooting method
! 
! tau = mu du/dy  or  du     tau
!                     -- = - --- leads to FD stencil  u(i+1)-u(i) = - dz * tau/mu(i+1/2)
!                     dz      mu
!
! tau is constant in the channel
!
! If mu=1e21 Pa.s, channel depth L=300 km, and top velocity V0=5 cm/yr then tau=mu*V0/L=5.28 MPa  
!
! PvK February 16 2022
program couette
implicit none
real(kind=8), parameter :: L=300e3,V0=0.05/(365.24*24*3600) ! depth of asthenosphere, SI velocity
real(kind=8) :: tau ! stress in channel
integer, parameter :: N=301 ! number of grid points
real(kind=8),dimension(N,3) :: Vz   ! estimated velocity profiles
real(kind=8) :: dz=1e3 ! spacing in km
real(kind=8) :: mu(N-1),T(N-1),T0,T1,Ea,R ! viscosity and temperature at midpoints of FD intervals
integer :: Niter,i,j,ierr,ifactor
character(len=120) :: fname

! First find tau in constant viscosity channel
mu=1e21 ! in Pa.s
ierr=0
call find_tau(mu,V0,Vz,tau,dz,N,ierr)
if (ierr /= 0) then
   write(6,*) 'find_tau failed with error: ',ierr
else
   write(6,*) tau
endif
open(9,file='v_mu_constant.dat') 
do i=1,N
   write(9,*) (i-1)*dz/L,Vz(i,2)/V0
enddo
close(9)


! Now define viscosity using T&S 2nd edition (7-140)
T0=800+273
T1=1300+273
R=8.3144 ! gas constant
do ifactor=10,30,10
   Ea=ifactor*R*T0  ! Ea/(R*T0)=10
   do i=1,N-1
      T(i)=T0+(T1-T0)*(i-0.5)*dz/L
      mu(i)=1e21*exp(Ea/(R*T0)*(T0/T(i)-1))
   enddo
   write(6,*) T(1),T(N-1)
   write(6,*) mu(1),mu(N-1)
   ierr=0
   call find_tau(mu,V0,Vz,tau,dz,N,ierr)
   write(fname,'(''v_ERT0_'',i2.2,''.dat'')') ifactor
   open(9,file=fname)
   do i=1,N
      write(9,*) (i-1)*dz/L,Vz(i,2)/V0
   enddo
   close(9)
enddo
end program couette


subroutine find_tau(mu,V0,Vz,tauM,dz,N,ierr)
implicit none
integer,intent(in) :: N
real(kind=8),intent(in) :: mu(N),V0,dz
real(kind=8),intent(out) :: tauM,Vz(N,3)
integer,intent(inout) :: ierr
integer :: Niter,j
real(kind=8) :: tau1,tau2

! extreme end member values 
tau1=1e1 ! in Pa
tau2=1e10 ! in Pa
Niter=0
do
   Niter=Niter+1
   tauM=0.5*(tau1+tau2)
   ! find velocity profile for extreme and mid-point estimate
   call find_vz(N,dz,tau1,mu,V0,Vz(1,1))
   call find_vz(N,dz,tau2,mu,V0,Vz(1,3))
   ! Since Vz(N) should be 0 use bisection to figure out new interval
   call find_vz(N,dz,tauM,mu,V0,Vz(1,2))
   if (Vz(N,1)*Vz(N,2)<=0.0_8) then
      ! solution lies between tau1 and tauM
      tau2=tauM
   else
      tau1=tauM
   endif
   write(6,'(i5,e15.7,3f12.3)') Niter,tauM,(Vz(N,j)*365.24*24*3600*100,j=1,3)
   if (Niter>1000) then
      ierr=1
      exit
   else if (tau2-tau1<1e1) then
      tauM=0.5*(tau2+tau1)
      exit
   endif
enddo


end subroutine find_tau



subroutine find_vz(N,dz,tau,mu,V0,Vz)
implicit none
integer,intent(in) :: N
real(kind=8),intent(in) :: dz,tau,V0,mu(N-1)
real(kind=8),intent(out) :: Vz(*)
integer :: i
real(kind=8) :: vI,vI1 ! velocity at current grid point and one below

! integrate from top down
Vz(1)=V0
do i=1,N-1
   vz(i+1) = vz(i) - dz*tau/mu(i)
enddo

end subroutine find_vz
