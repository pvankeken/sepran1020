program mwhu
implicit none
integer(kind=4),parameter :: n=4,m=1,lda=m+1
real(kind=8) :: S_sp(lda,n),f_sp(n),u_sp(n)
integer(kind=4) :: j,i,info,i1,i2,k,k1,itime,kpivot(lda),m2
integer(kind=4),parameter :: mu=1,ml=1,lda2=2*mu+ml+1
real(kind=8) :: S_sp2(lda2,n)


S_sp=0.0_8
f_sp=0.0_8
do j=1,n
   write(6,'(''j = '',i3)') j
   i1=max(1,j-m)
   do i=i1,j
      k = i-j+m+1
      write(6,*) i,j,k
      if (i==j) then
         if (j<n) then
            S_sp(k,j)=2.0_8
         else 
            S_sp(k,j)=1.0_8
         endif
      else if (abs(i-j)==1) then
         S_sp(k,j)=-1.0_8
      endif
   enddo
enddo   

do j=1,n
   write(6,'(2e15.7)') (S_sp(k,j),k=1,lda)
enddo
! set up rhsd vector
f_sp(1:n-1)=-1
f_sp(n)=-0.5
u_sp=f_sp

call dpbfa(S_sp,lda,n,m,info)
write(6,*) 'info: ',info
do j=1,n
   write(6,'(2e15.7)') (S_sp(k,j),k=1,lda)
enddo

call dpbsl(S_sp,lda,n,m,u_sp)

do j=1,n
   write(6,'(e15.7)') u_sp(j)
enddo

! Now with non SPD solver
! dsifa/dsisl are for symmetric indefinite
! apparently there is no linpack routine for factorization of symmetric banded matrix
! use general band matrix routine instead (efficient).
!!!!call dsifa(S_sp,lda,n,kpivot,info)
!!!!call dsisl(S_sp,lda,n,kpivot,u_sp)
S_sp2=0.0_8
m2=ml+mu+1
do j=1,n
   write(6,'(''j = '',i3)') j
   ! see magic code in linpack comments or user's guide
   i1=max(1,j-mu)
   i2=min(n,j+ml)
   do i=i1,i2
      k = i-j+m2
      write(6,*) i,j,k
      if (i==j) then
         if (j<n) then
            S_sp2(k,j)=2.0_8
         else 
            S_sp2(k,j)=1.0_8
         endif
      else if (abs(i-j)==1) then
         S_sp2(k,j)=-1.0_8
      endif
   enddo
enddo   

u_sp=f_sp
call dgbfa(S_sp2,lda2,n,ml,mu,kpivot,info)
write(6,*) 'info,m2: ',info,m2
do j=1,n
   write(6,'(3e15.7,i5)') (S_sp2(k,j),k=1,m2),kpivot(n)
enddo
call dgbsl(S_sp2,lda2,n,ml,mu,kpivot,u_sp,0)
do j=1,n
   write(6,'(e15.7)') u_sp(j)
enddo


end program mwhu
