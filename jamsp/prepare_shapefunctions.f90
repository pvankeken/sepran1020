! PvK November 2021
! Adjusted from fortran77 code written 1993-1996
! Could be made a bit more elegant by use of generic functions
subroutine prepare_shapefunctions
use geometry
implicit none
integer :: l,k,m
! shapefunctions and first & second derivatives in 1D
real(kind=8),dimension(NSPAN) :: sx,sy,sx1,sy1,sx2,sy2
real(kind=8) :: xi,eta

do l=1,ng1d
   do k=1,ng1d
     m = k + (l-1)*ng1d
     xi = xg(k)
     eta = xg(l)
     ! form shape function in this Gauss point
     call shap0(sx,xi,NSPAN)
  enddo
enddo
end subroutine prepare_shapefunctions

