subroutine check_gauss_integration
use geometry
implicit none
real(kind=8) :: integral_estimate,integral_value
integer :: i

! on interval [-1,1]
integral_value=f_int(1.0_8)-f_int(-1.0_8)
integral_estimate=0.0_8
do i=1,ng1d
   !write(6,*) i,wg(i),xg(i)
   integral_estimate=integral_estimate+wg(i)*f_to_be_int(xg(i))
enddo

write(6,'(''integration test with N-th order Gauss quadrature, N='',i4,3e15.7)') ng1d,integral_value,integral_estimate, &
    & (integral_estimate-integral_value)/integral_value

contains
  real(kind=8) function f_to_be_int(x)
  implicit none
  real(kind=8),intent(in) :: x
  f_to_be_int=(x+0.5_8)**3+(x-0.5_8)**2+x
  end function f_to_be_int

  real(kind=8) function f_int(x)
  implicit none
  real(kind=8),intent(in) :: x
  f_int=0.25_8*(x+0.5_8)**4+1.0_8/3*(x-0.5_8)**3+0.5*x*x
  end function f_int


end subroutine check_gauss_integration
