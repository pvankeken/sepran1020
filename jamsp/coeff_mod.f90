module coeff
! Rayleigh number
real(kind=8) :: Ra
! Maximum number of Picard iterations, relaxation factor, Picard convergence criterion
integer :: maxiter
real(kind=8) :: relax,Picard_epsilon
!real(kind=8),allocatable,dimension(:,:) :: Sp
!real(kind=8),allocatable,dimension(:) :: fp

end module coeff
