module geometry
! aspect ratio of the box
real(kind=8) :: aspectratio=1.0_8
! number of nodal points in x,y; number of elements in x and y, number of non-zero spline coefficients
integer :: nx=11,ny=11,nex=10,ney=10,nspx=9,nspy=9
real(kind=8),parameter :: pi = 3.14159265358979323846_8, twopi=2.0_8*pi


!  Gauss integration parameter
!  number of Gauss points for 1D integration & number of elements a spline function spans
integer, parameter :: NG1DMAX=4,NSPAN=4
integer :: ng1d=3,ngauss
! xgN contain local coordinates and wgN containts the weights for N-point Gauss integration
! use 2D arrays for all for programming logic. Values are set in spgaus()
real(kind=8) :: xg1(1),wg1(1),xg2(2),wg2(2),xg3(3),wg3(3),xg4(4),wg4(4)
! Gauss points and weights for chosen integration rule + values adjusted to element [0,1]
real(kind=8), dimension(NG1DMAX) :: xg,wg,xg1d,w1d

! finite element mesh
integer :: ncoor=0,nelem=0
real(kind=8), dimension(:,:),allocatable :: coor
! nodal points in x and y
real(kind=8), dimension(:), allocatable :: xc,yc
real(kind=8) :: xcmin,xcmax,ycmin,ycmax,dxgrid,dygrid

! coefficients describing size of matrix in 2D
integer :: nunknown=0,mbw=0,lda=0,nmat=0
! same but now in 1D
integer :: nunknown1d=0,mbw1d=0,lda1d=0,nmat1d=0
! stiffness matrix
real(kind=8),dimension(:,:),allocatable :: S_sp
! righthandside and solution vectors; bspline coefficients
real(kind=8),dimension(:),allocatable :: f_sp,u_sp,b_sp

contains

  real(kind=8) function f0x(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then 
     ! element is outside span of this shapefunction
     f0x=0
     return
  endif
  call shap0(y,x,NSPAN)
  if (no==1) then
     ! modify for Dirichlet b.c.
     if (iel == 1) then
        f0x = y(2)-y(4)   
     else 
        f0x = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        f0x = y(3)-y(1)
     else
        f0x = y(iel-no+2)
     endif
     return
  endif
  f0x = y(iel-no+2)
   
  end function f0x

  real(kind=8) function df1x(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  !write(6,'(''df1x: '',2i5,2f12.3,$)') no,iel,x
  if (no-2 >= iel .or. iel >= no+3) then 
     ! element is outside span of this shapefunction
     df1x=0
     !write(6,*) 'outside'
     return
  endif

  !y(1:4)=-99.0_8
  call shap1(y,x,NSPAN)
  if (no==1) then
     if (iel==1) then   
        df1x = y(2)-y(4)
     else
        df1x = y(iel+1)
     endif
     !write(6,*) 'iel1: ',df1x
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        df1x = y(3)-y(1)
     else
        df1x = y(iel-no+2)
     endif
     !write(6,*) 'ielN: ',df1x
     return
  endif
  df1x = y(iel-no+2)
  !write(6,*) df1x,iel-no+2
   
  end function df1x

  real(kind=8) function df2x(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then 
     ! element is outside span of this shapefunction
     df2x=0
     return
  endif

  call shap2(y,x,NSPAN)
  !y(1:4)=-999.0_8
  if (no==1) then
     if (iel==1) then   
        df2x = y(2)-y(4)
     else
        df2x = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        df2x = y(3)-y(1)
     else
        df2x = y(iel-no+2)
     endif
     return
  endif
  df2x = y(iel-no+2)
  end function df2x

  real(kind=8) function df3x(no,iel,x,nex)
  implicit none
  integer,intent(in) :: no,iel,nex
  integer,parameter  :: NSPAN=4
  real(kind=8),intent(in) :: x
  real(kind=8),dimension(NSPAN) :: y

  if (no-2 >= iel .or. iel >= no+3) then 
     ! element is outside span of this shapefunction
     df3x=0
     return
  endif

  call shap3(y,x,NSPAN)
  if (no==1) then
     if (iel==1) then   
        df3x = y(2)-y(4)
     else
        df3x = y(iel+1)
     endif
     return
  endif
  if (no==nex-1) then
     if (iel==nex) then
        df3x = y(3)-y(1)
     else
        df3x = y(iel-no+2)
     endif
     return
  endif
  df3x = y(iel-no+2)
  end function df3x

  ! shap0 = shape functions for 1D bicubic splines defined on 4 intervals of [-1,1] 
  ! shapN = their N-th derivatives 
  subroutine shap0(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: xi2 ,oneminusx
  oneminusx=1-xi
  y(1) = xi*xi*xi
  y(2) = 1+3*xi+3*xi*xi-3*xi*xi*xi
  y(3) = 1+3*oneminusx+3*oneminusx*oneminusx-3*oneminusx*oneminusx*oneminusx
  y(4) = oneminusx*oneminusx*oneminusx
  !! ? y(1:4) = y(1:4)*0.1
  end subroutine shap0

  subroutine shap1(y,xi,N)
  implicit none
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: xi2 ,oneminusx
  oneminusx=1-xi
  y(1) = 3*xi*xi
  y(2) = 3+6*xi       -9*xi*xi
  y(3) = -3-6*oneminusx+9*oneminusx*oneminusx
  y(4) = -3*oneminusx*oneminusx
  !y(1) = 3*xi2
  !y(2) = -9*xi2+6*xi+3
  !y(3) = 9*xi2-12*xi
  !y(4) = -3*(1.0_8-xi)*(1.0_8-xi)
  !! ? y(1:4) = 0.1_8*y(1:4)
  !y(1:4) = -99.0_8
  end subroutine shap1

  subroutine shap2(y,xi,N)
  implicit none 
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y
  real(kind=8) :: oneminusx

  oneminusx=1-xi
  y(1) = 6*xi
  y(2) = 6       -18*xi
  y(3) = +6-18*oneminusx
  y(4) = 6*oneminusx
  !y(1) = 6*xi
  !y(2) = -18*xi+6.0_8
  !y(3) = 18*xi+6.0_8
  !y(4) = 6.0_8-6*xi
  !! ? y(1:4) = y(1:4)*0.1_8

  end subroutine shap2

  subroutine shap3(y,xi,N)
  implicit none 
  real(kind=8),intent(in) :: xi
  integer,intent(in) :: n
  real(kind=8),dimension(n) :: y

  y(1) = 6.0_8
  y(2) = -18.0_8
  y(3) = 18.0_8
  y(4) = -6.0_8
  !! ? y(1:4) = y(1:4)*0.1_8

  end subroutine shap3




end module geometry
