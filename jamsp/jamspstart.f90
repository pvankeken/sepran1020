subroutine jamspstart()
use coeff
use geometry
use control
implicit none
integer :: i,j,ip

namelist /jamspnl/ aspectratio,nx,ny,ng1d,Ra,relax,maxiter,Picard_epsilon

open(9,file='jam.nml')
read(9,NML=jamspnl)
close(9)

! few sanity checks
if (ng1d>NG1DMAX) then
   write(6,*) 'ERROR(jamspstart): ng1d>NG1DMAX: ',ng1d,NG1DMAX
   call instop
endif
call spgauss

call check_gauss_integration


! define coordinates and set up look up grids
ncoor=nx*ny
allocate(coor(2,ncoor),xc(nx),yc(ny))
! boundaries of the domain
xcmin=0.0_8
ycmin=aspectratio
ycmin=0.0_8
ycmax=1.0_8
! number of elements in x and y
nex=nx-1
ney=ny-1
! number of non-zero spline coefficients
nspx=nex-1
nspy=ney-1
! spacing of finite element grid (nice and constant)
dxgrid=aspectratio/nex
dygrid=1.0_8/ney
nelem=nex*ney
! set up coordinates
do i=1,nx
   xc(i)=(i-1)*dxgrid
enddo
do j=1,ny
      yc(j)=(j-1)*dygrid
enddo
ip=0
do j=1,ny
   do i=1,nx
     coor(1,ip+i)=xc(i)
     coor(2,ip+i)=yc(j)
   enddo
   ip=ip+nx
enddo

write(irefwr,'(''set up finite element grid'')')
write(irefwr,'(''    nx by ny nodal points: '',2i5)') nx,ny
write(irefwr,'(''    nodal point spacing  : '',2e12.3)') dxgrid,dygrid
if (ny<=11.and.nx<=11) then
   write(irefwr,'(''xc : '',11f6.2)') (xc(i),i=1,nx)
   write(irefwr,'(''yc : '',11f6.2)') (yc(j),j=1,ny)
   write(irefwr,'(''coordinates: '')') 
   ip=0
   do j=1,ny
      write(irefwr,'(11(2f5.1,3x),:)') (coor(1,ip+i),coor(2,ip+i),i=1,nx)
      ip=ip+nx
   enddo
endif
   

! define matrix structure
! since we have homogeneous Dirichlet b.c.'s we only have to worry about the unknowns in the interior
nunknown=nspx*nspy
! bandwidth of matrix
mbw = 3*(ny-1)+3
lda = mbw+1
! size of stiffness matrix
nmat=lda*nunknown

! First solve 1D Poisson equation
!nunknown1d=nspx
!mbw1d=3
!lda1d=mbw1d+1
!nmat1d=lda1d*nunknown1d
!allocate(S_sp(lda,nunknown1d),f_sp(nunknown1d),u_sp(nunknown1d),b_sp(nunknown1d))


! set up shapefunctions based on bicubic splines
call prepare_shapefunctions()

call test_shapefunctions()

call Poisson1D()

!deallocate(S_sp,f_sp,u_sp,b_sp)


end subroutine jamspstart


! evaluate a specific 1D spline in the Gauss points
subroutine test_shapefunctions
use geometry
use control
implicit none
integer :: iel,k,jx
real(kind=8) :: xi,xstart,x,df1
character(len=80) :: fname

do jx=1,nex-1 
  write(fname,'(''spline_'',i2.2)') jx
  open(9,file=fname)
  do iel=1,nex
     xstart=(iel-1)*dxgrid
     do k=1,11
        xi= (k-1)*0.1
        x = xstart + xi*dxgrid
        write(9,'(5f15.5)') x,f0x(jx,iel,xi,nex),df1x(jx,iel,xi,nex),df2x(jx,iel,xi,nex),df3x(jx,iel,xi,nex)
     enddo
  enddo
  close(9)
enddo

end subroutine test_shapefunctions

! real(kind=8) function f0(ispline,x,nex)
! use geometry
! implicit none
! integer, intent(in) :: ispline, nex ! spline number & number of elements
! real(kind=8), intent(in) :: x ! horizontal coordinate
! real(kind=8) :: xmm2,xmm1,xm,xmp1,xmp2
! 
! xm=x
! xmm1=xm-dxgrid
! xmm2=xmm1-dxgrid
! xmp1=xm+dxgrid
! xmp2=xmp2+dxgrid
! 
! xi=(x-xmm2)/dxgrid
! y(1)=xi*xi*xi
! xi=(x-
! 
! 
! 
