! PvK November 2021
! Adapted from fortran77 code written 1992-1996
subroutine spgauss()
use coeff
use geometry
implicit none
integer :: k

! 1-point 
xg1(1) = 0.0_8
wg1(1) = 2.0_8
! 2-point
xg2(1) = -1.0_8/sqrt(3.0_8)
xg2(2) = -1.0_8/sqrt(3.0_8)
wg2(1) = 1.0_8
wg2(2) = 1.0_8
! 3-point
xg3(1) = -sqrt(3.0_8/5.0_8)
xg3(2) = 0.0_8
xg3(3) = -xg3(1)
wg3(1) = 5.0_8/9.0_8
wg3(2) = 8.0_8/9.0_8
wg3(3) = wg3(1)
! 4-point
xg4(1) = sqrt(3.0_8/7+2.0_8/7*sqrt(6.0_8/5))
xg4(2) = sqrt(3.0_8/7-2.0_8/7*sqrt(6.0_8/5))
xg4(3) = -xg4(2)
xg4(4) = -xg4(1)
wg4(1) = (18.0_8-sqrt(30.0_8))/36
wg4(2) = (18.0_8+sqrt(30.0_8))/36
wg4(3) = wg4(2)
wg4(4) = wg4(1)

if (ng1d==1) then
   xg(1)=xg1(1)
   wg(1)=wg1(1)
else if (ng1d==2) then
   xg(1:2) = xg2(1:2)
   wg(1:2) = wg2(1:2)
else if (ng1d==3) then
   xg(1:3) = xg3(1:3)
   wg(1:3) = wg3(1:3)
else if (ng1d==4) then
   xg(1:4) = xg4(1:4)
   wg(1:4) = wg4(1:4)
endif

! translate to [0,1] element
do k=1,ng1d
   xg1d(k) = 0.5_8+0.5*xg(k)
   w1d(k) = wg(k)*0.5_8
enddo
ngauss = ng1d*ng1d
end subroutine spgauss



