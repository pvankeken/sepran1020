      subroutine to0006 ( icnt, relemmt )
! ======================================================================
!
!        programmer    Guus Segal
!        version  3.0  date 24-07-2015 Use sepmodulebuild
!        version  2.0  date 06-12-2008 New parameter list
!        version  1.4  date 18-10-2000 Correction for coinciding dof's
!
!   copyright (c) 1983-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!     add element matrix to large matrix
!     real profile matrix
! **********************************************************************
!
!                       KEYWORDS
!
!     matrix
!     real
!     assembly
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulematstr
      use sepmodulekprob
      use sepmodulebuild
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      integer, intent(out) :: icnt
      double precision, intent(in) :: relemmt(icount,icount)

!     icnt           o    Number of non-prescribed unknowns in element
!     relemmt        i    Element matrix
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer :: i, imat, inusol, jnusol, j, l
      double precision, pointer :: rmat(:)

!     i              Counting variable
!     imat           Position in array rmat
!     inusol         Position of unknown in array USOL
!     j              Counting variable
!     jnusol         Position of unknown in array USOL
!     l              General loop variable
!     rmat           matrix S_ii
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRININ         print 1d integer array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     if ( jmethod=1 ) then
!        For i := 1 (1) icnt do
!           For j := 1 (1) i do
!              Compute position in rmat
!              Add contribution of element matrix to rmat
!     else ( jmethod=2 ) then
!        For i := 1 (1) icnt do
!           For j := 1 (1) icnt do
!              Compute position in rmat
!              Add contribution of element matrix to rmat
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0006' )
      debug = .false. .and. ioutp>=0
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0006'
         write(irefwr,1) 'jmethod, icount, nrusol', &
                          jmethod, icount, nrusol
         call prinin ( indexren, icount , 'indexren' )
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      rmat => Smat%S_ii

      icnt = 0
      if ( jmethod==1 ) then

!     --- jmethod = 1   symmetrical case

         do i = 1, icount

            inusol=indexren(i)
            if ( inusol<=nrusol ) then

!           --- Non-prescribed unknown

               icnt = icnt+1

!              --- Off diagonal elements

               do j = 1, i-1

                  jnusol=indexren(j)
                  if ( jnusol<=nrusol ) then

!                 --- Non-prescribed unknown

                     l=max(inusol, jnusol)
                     imat=irowsii(l)-abs(jnusol-inusol)
                     rmat(imat)=rmat(imat)+relemmt(j,i)

!                    --- correction for the case that degrees of freedom
!                        collapse
!                        This is not possible for a standard SEPRAN case but
!                        may happen when a user provides his own numbering

                     if ( inusol==jnusol ) &
                        rmat(imat)=rmat(imat)+relemmt(j,i)

                  end if  ! ( jnusol<=nrusol )

               end do  ! j = 1, i-1

!              --- Diagonal element

               imat=irowsii(inusol)
               rmat(imat)=rmat(imat)+relemmt(i,i)

            end if  ! ( inusol<=nrusol )

         end do  ! i = 1, icount

      else

!     --- jmethod = 2   non-symmetrical case

         do i = 1, icount

            inusol=indexren(i)
            if ( inusol<=nrusol ) then

!           --- Non-prescribed unknown

               icnt = icnt+1

               do j= 1, icount

                  jnusol=indexren(j)
                  if ( jnusol<=nrusol ) then

!                 --- Non-prescribed unknown

                     l=max(inusol, jnusol)
                     imat=irowsii(l)-inusol+jnusol
                     rmat(imat)=rmat(imat)+relemmt(j,i)

                  end if  ! ( jnusol<=nrusol )

               end do  ! j= 1, icount

            end if  ! ( inusol<=nrusol )

         end do  ! i = 1, icount

      end if  ! ( jmethod==1 )

1000  if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0006'

      end if  ! ( debug )
      call erclos ( 'to0006' )

      end
