      subroutine to0051
! ======================================================================
!
!        programmer    Guus Segal
!        version 14.0  date 24-07-2015 Use sepmodulebuild
!        version 13.2  date 21-04-2015 New definition intmat
!        version 13.1  date 14-08-2014 Extension with parallel petsc
!
!   copyright (c) 1984-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!   add element matrix to large matrix, element vector to large vector and
!   element mass matrix to mass matrix (diagonal or not)
!   Warning: the multiplication factor in periodical boundary conditions
!            has only been programmed for real matrices
! **********************************************************************
!
!                       KEYWORDS
!
!     assembly
!     diagonal_matrix
!     mass_matrix
!     matrix
!     vector
! **********************************************************************
!
!                       MODULES USED
!
      use sepmoduleintmat
      use sepmodulebuild
      use sepmodulekprob
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      logical :: sym
      integer :: i
      type (intmatrix), pointer :: IM

!     IM             Refers to intmt(intmat)
!                    they are not
!     i              General loop variable
!     sym            If true the matrix is stored as a symmetric matrix
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     TOASSEMBLECMAT Assemble one element matrix in the large matrix
!                    Complex matrix
!     TOASSEMBLECVEC Assemble one element vector in the large vector
!                    Complex vector
!     TOASSEMBLEMAT  Assemble one element matrix in the large matrix
!                    real matrix
!     TOASSEMBLEMMAT Assemble one element matrix in the mass matrix
!     TOASSEMBLEVEC  Assemble one element vector in the large vector
!                    Real vector
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!    With respect to periodical boundary conditions the following technique
!    is applied:
!    The unknowns are splitted into the categories i, l, r and b
!    meaning internal, left per. bc., right per. bc. and prescribed bc.
!    The matrix vector equation becomes:
!    | S    S    S    S   |  | u  |    | f  |
!    |  ii   il   ir   ib |  |  i |    |  i |
!    |                    |  |    |    |    |
!    | S    S    S    S   |  | u  |    | f  |
!    |  li   ll   lr   lb |  |  l |    |  l |
!    |                    |  |    | =  |    |
!    | S    S    S    S   |  | u  |    | f  |
!    |  ri   rl   rr   rb |  |  r |    |  r |
!    |                    |  |    |    |    |
!    | S    S    S    S   |  | u  |    | f  |
!    |  bi   bl   br   bb |  |  b |    |  b |
!    Since the prescribed boundary conditions are not used in the matrix, &
!    the last row is skipped
!    Using the boundary condition  u = a u  + c and adding equation 3
!                                   r     l
!    multiplied by a to equation 2, the equations reduce to:
!    S  u  + ( S   + a S  ) u  =  f  - S  u  - c S
!     ii i      il      ir   l     i    ib b      ir
!                                                2
!    (S  + a S  ) u  + ( S   + a ( S   + S  ) + a S  ) u  = &
!      li     ri   i      ll        lr    rl       rr   l
!    f  - S  u  + a f  - a S  u - c S  - ac S
!     l    lb        r      rb b     lr      rr
!    Hence before creating the matrix the rows and columns of the
!    element matrix corresponding to the "right" periodical boundary
!    conditions are multiplied by a
!    The same is done for the right-hand side
!    When creating the matrices for the boundary conditions, the columns
!    of the element matrix are divided by a
!                    PSEUDO CODE
!   trivial
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0051' )
      debug = .false.
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0051'
         write(irefwr,*) 'matrix, vector ', matrix, vector
         write(irefwr,1) 'notmas, imas, idefcr', notmas, imas, idefcr
         write(irefwr,1) 'nrusol', nrusol
  1      format ( a, 1x, (10i6) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      IM => intmt(intmat)
      sym = IM%symmetric

!     --- add element matrix

      if ( compl ) then

!     --- Complex case
!         add element matrix

         call toassemblecmat

!        --- add element vector

         call toassemblecvec

      else

!     --- Real case
!         add element matrix

         call toassemblemat ( sym )

!        --- add element vector

         call toassemblevec

      end if  ! ( compl )

!     --- add element mass matrix

      call toassemblemmat

1000  if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0051'

      end if
      call erclos ( 'to0051' )

      end
