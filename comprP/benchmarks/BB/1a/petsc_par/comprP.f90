! Solve convection equations with petsc
! This approach follows closely what we use in the production codes (e.g., comprS)

! PG=Programmer's Guide (ta.twi.tudelft.nl/NW/sepran/pg.pdf)
! SP=Standard Problems Guide (ta.twi.tudelft.nl/NW/sepran/sp.pdf)

! PvK August 2017
! PvK October 2017: expanded for parallel computing
! PvK June 2019: complete rewrite using sepran 0619
program comprP
use sepmodulecomio  ! provides lu's for reading & writing
use sepmodulecpack  ! parallel computing interface
use sepran_arrays_interface  ! access to kmesh, kprob, isol, etc. and interfaces to subroutines
use control
use dtm_elem  ! controls building of pressure mass matrix
implicit none

call comprP_start()

if (steady) then
   call steady_iteration
else 
   write(6,*) 'write time_integration!' 
   call instop
endif

end program comprP 

