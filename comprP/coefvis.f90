! COEFVIS
! Make array that contains viscosity in the nodal points
! PvK 070819 adapted for 0619
subroutine coefvis
use sepmodulecomio
use sepmodulevecs
use sepmodulekmesh
use sepmoduleoldrouts
use sepran_arrays
use coeff
use control
implicit none
interface
   subroutine coefv2(ndim,npoint,coor,temp,adia,eta,etalin,etaplast,secinv,rho)
   integer,intent(in) :: ndim,npoint
   real(kind=8),intent(in) :: coor(ndim,npoint),temp(npoint),adia(npoint),secinv(npoint),rho(npoint)
   real(kind=8),intent(out) :: eta(npoint),etalin(npoint),etaplast(npoint)
   end subroutine coefv2
end interface
integer :: iuserlc(100),inpcre(10)
real(kind=8) :: userlc(100),rincre(10)
integer :: ix=0,ielhlp=0,jdegfd=0

!if (print_node) pedebug=.true.

!pedebug=.true.
if (pedebug) write(irefwr,*) 'in coefvis: ',ivisc,ivisclin,iviscplast
pedebug=.false.
!call instop

!!! PvK Jan 19 2022
!!!! use iuserlc/userlc rather than iuser_here ; see getvisdip
iuserlc=0
userlc=0
iuserlc(1)=100
iuserlc(2)=1
 userlc(1)=100.0_8
if (ivisc==0) then
   if (print_node) then
      write(irefwr,*) 'PERROR(coefvis): ivisc=0'
   endif
   call instop
endif
if (ivn.or.tackley.or.tosi15) then
   iuserlc(1)=100
   iuserlc(2)=1
   iuserlc(3:5)=0
   iuserlc(6)=7
   iuserlc(7)=0 ! type of NS equation
   iuserlc(8)=1 ! type of constitutive equation
   iuserlc(9)=0 ! type of interpolation
   iuserlc(10)=icoor900 ! type of coordinate system
   iuserlc(11)=mcontv ! compressibility formulation
   iuserlc(12)=-7 ! eps
   iuserlc(13)=-8 ! rho
   iuserlc(18)=-9 ! eta
   iuserlc(15:100)=0
    userlc(2:5)=0.0_8
    if (itype_stokes==903) then
       userlc(6)=0.0_8
    else
       userlc(6)=penalty_parameter
    endif
    userlc(8)=1.0_8
    userlc(9)=1.0_8
   call deriva(2,10,ix,jdegfd,1,isecinv,kmesh1,kprob1,isol1,isol1,iuserlc,userlc,ielhlp)
   pedebug=.false.
   if (pedebug) then
      write(irefwr,*) 'secinv sum: ',anorm(1,1,1,kmesh1,kprob1,isecinv,isecinv,ielhlp)
      write(irefwr,*) 'secinv max: ',anorm(1,3,1,kmesh1,kprob1,isecinv,isecinv,ielhlp)
      write(irefwr,*) 'uvel   max: ',anorm(1,3,1,kmesh1,kprob1,isol1,isol1,ielhlp)
      write(irefwr,*) 'wvel   max: ',anorm(1,3,2,kmesh1,kprob1,isol1,isol1,ielhlp)
   endif
   pedebug=.false.
else 
   isecinv=1 ! needs to be something different from 0..
endif

if (idens==0) idens=1

!if (pedebug) write(irefwr,*) 'isol2: ',isol2
call sepactsolbf1(isol2)
call sepgetmeshinfo(ndim,npoint)
!pedebug=.true.
if (pedebug) write(irefwr,*) 'isol2: ',isol2,iadia,ivisc,isecinv,idens
pedebug=.false.
call coefv2(ndim,npoint,coor,ks(isol2)%sol,ks(iadia)%sol,ks(ivisc)%sol,ks(ivisclin)%sol,ks(iviscplast)%sol, &
          & ks(isecinv)%sol,ks(idens)%sol)
!write(6,*) 'coefvis',output_vislin,output_visplast,ks(ivisc)%sol(1),ks(ivisclin)%sol(1),ks(iviscplast)%sol(1)

end subroutine coefvis

subroutine coefv2(ndim,npoint,coor,temp,adia,eta,etalin,etaplast,secinv,rho)
use sepmodulecomio
use sepmoduleoldrouts
use geometry
use control
use coeff
implicit none
integer,intent(in) :: ndim,npoint
real(kind=8),intent(in) :: coor(ndim,npoint),temp(npoint),adia(npoint),secinv(npoint),rho(npoint)
real(kind=8),intent(out) :: eta(npoint),etalin(npoint),etaplast(npoint)
real(kind=8) :: etamin,etamax,temp_a,temp_l,secsqr,pefvis,x,y
integer :: i,ival,ivalfind,ipl,ipp
real(kind=8) :: vislmin,vislmax,vispmin,vispmax

!if (print_node) pedebug=.true.
etamin=5e9
etamax=0
vislmin=1e9
vislmax=-1e9
vispmin=1e9
vispmax=-1e9
ipl=0
ipp=0
eta=0.0_8
etaplast=0.0_8
etalin=0.0_8

!write(irefwr,*) 'ivl: ',ivl,nlay,r_zint(1:nlay)
if (itypv==0) then
   eta(1:npoint)=1.0_8
else if (.not.tackley.and..not.tosi15) then
   ival=1
   secsqr=0
   do i=1,npoint
      x = coor(1,i)
      y = coor(2,i)
      if (ivl) ival = ivalfind(x,y)
      temp_l=temp(i)
      temp_a=adia(i)
      secsqr=secinv(i)*secinv(i)
      eta(i) = pefvis(x,y,temp_l,temp_a,ival,secsqr)
   enddo
else
   ! tackley or tosi
   ival=1
   secsqr=0
   do i=1,npoint
      x = coor(1,i)
      y = coor(2,i)
      if (ivl) ival = ivalfind(x,y)
      temp_l=temp(i)
      temp_a=adia(i)
      secsqr=secinv(i)*secinv(i)
      eta(i) = pefvis(x,y,temp_l,temp_a,ival,secsqr)
      etalin(i) = etalinh
      etaplast(i) = etaplasth
!     if (output_vislin) then
!        eta(i)=etalinh
!        if (i==1.and.print_node) write(irefwr,*) 'etalinh = ',etalinh
!        ipl=ipl+1
!        vislmin=min(vislmin,etalinh)
!        vislmax=max(vislmax,etalinh)
!     endif
!     if (output_visplast) then
!        eta(i)=etaplasth
!        if (i==1.and.print_node) write(irefwr,*) 'etaplasth = ',etaplasth
!        ipp=ipp+1
!        vispmin=min(vispmin,etaplasth)
!        vispmax=max(vispmax,etaplasth)
!     endif
   enddo
!  if (print_node) then
!     if (output_vislin) then
!        write(irefwr,*) 'COEFVIS ipl: ',ipl,vislmin,vislmax
!     endif
!     if (output_visplast) then
!        write(irefwr,*) 'COEFVIS ipp: ',ipp,vispmin,vispmax
!     endif
!  endif
    
     
endif
      
end subroutine coefv2

