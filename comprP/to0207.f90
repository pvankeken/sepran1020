      subroutine to0207 ( relemmt )
! ======================================================================
!
!        programmer    Bas van Rens (based on to0207)
!        version  5.0  date 24-07-2015 Use sepmodulebuild
!        version  4.0  date 21-04-2015 New parameter list
!        version  3.1  date 11-04-2015 extra debug
!
!   copyright (c) 1996-2015  "Ingenieursbureau SEPRA"
!   permission to copy or distribute this software or documentation
!   in hard copy or soft copy granted only by written license
!   obtained from "Ingenieursbureau SEPRA".
!   all rights reserved. no part of this publication may be reproduced,
!   stored in a retrieval system ( e.g., in memory, disk, or core)
!   or be transmitted by any means, electronic, mechanical, photocopy
!   recording, or otherwise, without written permission from the
!   publisher.
! **********************************************************************
!
!                       DESCRIPTION
!
!    add element matrix to large matrix part 2, 3, 4
!    i.e. construct not only Sup but also Spu and Spp
!    Hence ibcmat must be 1
!    (corresponding to boundary conditions)
! **********************************************************************
!
!                       KEYWORDS
!
!     element
!     matrix
!     assembly
!     essential_boundary_condition
!     reaction_force
! **********************************************************************
!
!                       MODULES USED
!
      use sepmodulematstr
      use sepmodulebuild
      use sepmodulekprob
      implicit none
!
! **********************************************************************
!
!                       INPUT / OUTPUT PARAMETERS
!
      double precision, intent(in) :: relemmt(icount,icount)

!     relemmt        i    element matrix
! **********************************************************************
!
!                       LOCAL PARAMETERS
!
      integer :: i, inusol, jtel, j, jnusol, &
                 imat, ktel, k, knusol
      double precision, pointer :: rmat(:)

!                    they are not
!     i              General loop variable
!     imat           entry of the large matrix you want to add the element
!                    matrix entry of Sup(j,i) to
!     inusol         Global sequence number of prescribed variable with respect
!                    to USOL
!     j              General loop variable
!     jnusol         Global sequence number of prescribed variable with respect
!                    to USOL
!     jtel           Local sequence number of prescribed variable with respect
!                    to indexren
!     k              General loop variable
!     knusol         global seguence number of local free degree of freedom j
!     ktel           Global sequence number of prescribed degree of freedom, &
!                    where the first prescribed d.o.f. has sequence number 1
!     rmat           matrix S_ip
! **********************************************************************
!
!                       SUBROUTINES CALLED
!
!     ERCLOS         Resets old name of previous subroutine of higher level
!     EROPEN         Produces concatenated name of local subroutine
!     PRININ         print 1d integer array
! **********************************************************************
!
!                       I/O
!
! **********************************************************************
!
!                       ERROR MESSAGES
!
! **********************************************************************
!
!                       PSEUDO CODE
!
!     For i := 1 (1) icount-icnt do
!        inusol := global seguence number of local precribed degree of freedom i
!        itel := local sequence number of i in element
!        Fill array iwork with information of the free unknowns coupled to
!        inusol
!        For k := 1 (1) icnt do
!           knusol := global seguence number of local free degree of freedom k
!           imat := position of element (inusol, knusol) in matrix
!           rmat(imat) := rmat(imat)+relemmt(index(k),itel)
!           rmat(imat+leng) := rmat(imat+leng) + relemmt(itel,index(k))
!        Fill array iwork with information of the prescribed dof's coupled to
!        inusol
!        For j := 1 (1) icnt1 do
!           jnusol := global sequence number of local prescr. dof j
!           imat  := position of element (inusol,jnusol)
!           rmat(imat) := rmat(imat)+ relemmt(jtel,itel)
! **********************************************************************
!
!                       DATA STATEMENTS
!
! ======================================================================
!
      call eropen ( 'to0207' )
      debug = .false. .and. ioutp>=0
      if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'Debug information from to0207'
         write(irefwr,1) 'nrusol, icount', nrusol, icount
         call prinin ( indexren, icount, 'indexren' )
  1      format ( a, 1x, (10i9) )

      end if  ! ( debug )
      if ( ierror/=0 ) go to 1000

      S_ip => Smat%S_ip
      S_pi => Smat%S_pi
      S_pp => Smat%S_pp

!     --- for every prescribed d.o.f

      do i = 1, icount

!     --- inusol := global sequence number of local
!         prescribed degree of freedom i

         inusol = indexren(i)
         if ( inusol<=nrusol ) go to 200  ! only prescribed unknowns

!        --- fill iwork with addresses in rmat of all free degrees of
!            freedom coupled to inusol

         ktel = inusol-nrusol
         do k = irowsip(ktel)+1, irowsip(ktel+1)
            iwork(icolsip(k)) = k
         end do

!        --- for every free d.o.f coupled to the d.o.f. i
!            construct Sup and Spu

         do k = 1, icount

            knusol = indexren(k)
            if ( knusol>nrusol ) go to 100   ! only free degrees of freedom
            imat = iwork(knusol)

!           --- add the entry to the large matrix

            S_ip(imat) = S_ip(imat)+relemmt(i,k)
            S_pi(imat) = S_pi(imat)+relemmt(k,i)

100      end do  ! k = 1, icount

!        --- fill iwork with addresses in rmat of all prescribed
!            degrees of freedom coupled to inusol

         jtel = inusol-nrusol
         do j = irowsp(jtel)+1, irowsp(jtel+1)
            iwork(icolsp(j)) = j
         end do

!        --- for every prescribed d.o.f coupled to the d.o.f. i
!            construct Spp

         do j = 1, icount

!        --- jnusol := global seguence number of local
!            precribed degree of freedom
!            jtel := local sequence number of j in element

            jnusol = indexren(j)
            if ( jnusol<=nrusol ) go to 150   ! only prescribed deg of freedom
            imat = iwork(jnusol)

!           --- add the entry to the large matrix

            S_pp(imat) = S_pp(imat)+relemmt(j,i)

150      end do  ! j = 1, icount

200   end do  ! i = 1, icount

1000  if ( debug ) then

!     --- Debug information

         write(irefwr,*) 'End to0207'

      end if  ! ( debug )
      call erclos ( 'to0207' )

      end
